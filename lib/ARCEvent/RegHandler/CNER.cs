﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCEvent
{
    [Class(Description =
        "CNER = Create New Entity Registration.\r\n" +
        "\r\n" +
        "General model for creating a new entity.\r\n" +
        "\r\n" +
        "TODO: Improve performance by more strong typing and caching.\r\n" +
        "\r\n" +
        "Note similarity with -" + nameof(SSPR) + "-, we could in principle just use that one " +
        "and when processing just accept that no value is defined.\r\n" +
        "\r\n" +
        "Note how this Registration does not have to implement -" + nameof(IRegHandlerTAERTE.TryApplyEventResultToEntity) + "- " +
        "because the actual existence of the event itself will be enough for for instance -" + nameof(PCollectionES) + "- to create the entity.\r\n" +
        "\r\n" +
        "TODO: This Registration is difficult (impossible) to change through another Registration, for instance with -" + nameof(RegVerb) + "-.-" + nameof(RegVerb.UNDO) + "-.\r\n" +
        "TODO: or just with another -" + nameof(RegVerb.DO) + "-.\r\n" +
        "TODO: The reason is that the Registration makes -" + nameof(PCollectionES) + "- create an Event-collection for the identity given.\r\n" +
        "TODO: and there is currently no mechanism for deleting it afterwards.\r\n" +
        "TODO. SOLUTION: Solve this through -" + nameof(EventProcessor.TryStoreAndDistributeEvent) + "-.\r\n" +
        "" +
        "\r\n" +
        "See also -" + nameof(CNERP) + "-."
    )]
    public class CNER : PExact<CNERP>, IRegHandler, IRegHandlerTGRI
    {
        private static readonly int capacity = Enum.GetNames(typeof(CNERP)).Length - 1;
        public CNER() : base(capacity) { }

        //public static CNER Create(Type entityType, IK entityKey) => Create(new ParentProcessId(entityType, entityKey));

        public static CNER Create(EntityTypeAndKey recipientId)
        {
            var retval = new CNER();
            retval.SetPVDirect(CNERP.RecipientId, recipientId);
            return retval;
        }

        public bool TryGetRecipientId(out EntityTypeAndKey recipient, out string errorResponse) =>
                TryGetPVDirect<EntityTypeAndKey>(CNERP.RecipientId, out recipient, out errorResponse);
            
    }

    public class CNERCollection : PCollection
    {

    }

    [Enum(
        Description = "Describes class -" + nameof(CNER) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum CNERP
    {
        __invalid,

        [PKType(Type = typeof(EntityTypeAndKey))]
        RecipientId,
    }
}
