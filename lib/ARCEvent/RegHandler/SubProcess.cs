﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCEvent
{
    [Class(Description =
        "Contains information about a single sub process instance.\r\n" +
        "\r\n" +
        "The sub processes are either created from a -" + nameof(Schema) + "- as a generic -" + nameof(Process) + "- (configurable) " +
        "or as an -" + nameof(IRegHandler) + "- (defined in C#).\r\n" +
        "\r\n" +
        "In the latter case -" + nameof(EXEcuteR) + "- is used to start the \r\n" +
        "actual \"execution\" (of the -" + nameof(IRegHandlerTGTE.TryGetTransientEvents) + "-) of the super process.\r\n" +
        "(in the former case this is not relevant because -" + nameof(Process) + "- " +
        "does not implement -" + nameof(IRegHandlerTGTE) + "-.)\r\n" +
        "\r\n" +
        "If a sub process is defined, and the parent process is a -" + nameof(IRegHandler) + "- other than -" + nameof(Process) + "-, " +
        "then -" + nameof(IRegHandler.DefaultEventStatus) + "- would typically be -" + nameof(EventStatus.WAIT_FOR_EXEC) + "-.\r\n" +
        "and the super process will usually have a 'SubProcess' collection of " +
        "the sub processes which are built up through the standard -" + nameof(Snapshot) + "- generating mechanism of -" + nameof(PCollectionES) + "-.\r\n" +
        "(for the configurable -" + nameof(Process) + "- the sub processes would be stored in -" + nameof(ProcessP.SubProcess) + "-.)\r\n" +
        "\r\n" +
        "Correspondingly, the individual super processes must be stored within a -" + nameof(PCollectionES) + "- in order to support " +
        "this -" + nameof(Snapshot) + "- mechanism. Other -" + nameof(IRegHandler) + "-s " +
        "are ordinarily stored within a -" + nameof(PCollection) + "- (but do not have to be stored at all actually)." +
        "\r\n" +
        "TODO: Consider renaming into SubProcessTuple or similar.\r\n" +
        "\r\n" +
        "This class is immutable.\r\n"
    )]
    public class SubProcess
    {
        public Type RegHandlerType { get; private set; }

        [ClassMember(Description =
            "This is the -" + nameof(RegTime.SerialNumber) + "- of the -" + nameof(Reg) + "- under which the corresponding " +
            "-" + nameof(RegHandler) + "- belongs.\r\n" +
            "Used by -" + nameof(ProcessController) + "- in order to construct an editable schema of process with its sub processes.\r\n" +
            "(in order to know how to make a hierarchical presentation).\r\n" +
            "\r\n" +
            "NOTE: SerialNumber PLUS RegHandler is very close to IKIP.\r\n" +
            "NOTE: (TODO:?) We could create a generic IKIP like IKIP<IRegHandler> for such purposes.\r\n" +
            "NOTE: Or maybe a generic IKIP with TWO generic parameters like IKIP<RegId, IRegHandler>\r\n" +
            "\r\n" +
            "TODO: Consider turning into RegId instead... But think about memory consequences because currently\r\n" +
            "TODO: (as of Dec 2021) we will not reuse the object (PCollectionES has no access to it).\r\n"
        )]
        public long SerialNumber { get; private set; }

        //[ClassMember(Description =
        //    "TODO: BUG: We can not store the object here if the actual sub process is itself updated through\r\n" +
        //    "TODO: BUG: the snapshot mechanism (if the actual sub process resides within PCollectionES and not PCollection.\r\n" +
        //    "TODO: BUG: This is because the object will become stale, meaning that when the super process\r\n" +
        //    "TODO: BUG. (or especially -" + nameof(ProcessController) + "-) needs to\r\n" +
        //    "TODO: BUG: look into it in order to get the sub processes of the sub process, they are not there."
        //)]
        //public IRegHandler RegHandler { get; private set; }

        //public SubProcess(long serialNumber, IRegHandler regHandler)
        //{
        //    SerialNumber = serialNumber;
        //    RegHandler = regHandler;
        //}

        public SubProcess(long serialNumber, Type regHandlerType)
        {
            SerialNumber = serialNumber;
            RegHandlerType = regHandlerType;
        }

        [ClassMember(Description =
            "Attempts to construct a strongly typed list of the given -" + nameof(IRegHandler) + "- sub type " +
            "from the List of -" + nameof(SubProcess) + "- given.\r\n" +
            "\r\n" +
            "Those whose type do not match the T type parameter are ignored.\r\n" +
            "(this corresponds to -" + nameof(PKUIAttributeP.SubProcessType) + "- being a 'many' property).\r\n" +
            "\r\n" +
            "Note that regTime is not present as parameter.\r\n" +
            "TODO: As of Dec 2021 it has not been tought through how querying in -" + nameof(RegTime) + "- against\r\n" +
            "TODO: sub processes will work out (it is possibly just not relevant to think about).\r\n" +
            "\r\n" +
            "TODO: The handling of types and collection types here look a little strange.\r\n" +
            "TODO: One could possibly rewrite the method a little bit, maybe also split it into two,\r\n" +
            "TODO: one for getting a single RegHandler, one for getting multiple.\r\n" +
            "TODO:\r\n" +
            "NOTE: On might believe that a common method for getting a collection (PCollection or PCollectionES)\r\n" +
            "NOTE: would be a good idea, but it is probably not a good idea since TryGetP works differently between them."
        )]
        public static bool TryGetRegHandlers<T>(IP dataStorage, EventTime eventTime, List<SubProcess> subProcesses, long minimumCountRequired, out List<T> regHandlers, out string errorResponse) where T : IRegHandler
        {
            var entityType = typeof(T);
            if (!IP.AllIPDerivedEntityCollectionClassesDict.TryGetValue(typeof(T), out var collectionType))
            {
                if (typeof(T).Equals(typeof(IRegHandler)) && subProcesses.Count == 1 &&
                    IP.AllIPDerivedEntityCollectionClassesDict.TryGetValue(subProcesses[0].RegHandlerType, out collectionType)
                )
                {
                    /// HACK: Made to accommodate <see cref="ProcessController"/>

                    /// OK, caller just wants a generic collection.
                    entityType = subProcesses[0].RegHandlerType;
                }
                else
                {
                    regHandlers = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse = "-" + nameof(TryGetRegHandlers) + "-: " +
                        "Corresponding collection type for " + typeof(T).ToStringShort() + " was not found.\r\n" +
                        "Expected to find '" + typeof(T).ToStringShort() + "Collection'.";
                    return false;

                }
            }
            regHandlers = new List<T>();
            if (typeof(PCollectionES).IsAssignableFrom(collectionType))
            {
                // Use snapshot mechanism
                if (!dataStorage.TryGetCollection(entityType, out var collection, out errorResponse))
                {
                    regHandlers = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse = "-" + nameof(TryGetRegHandlers) + "-: " + errorResponse;
                    return false;
                }
                foreach (var subProcess in subProcesses)
                {
                    if (!(typeof(T).IsAssignableFrom(subProcess.RegHandlerType)))
                    {
                        continue;
                    }
                    if (!collection.TryGetP(eventTime, regTime: null, new IKLong(subProcess.SerialNumber), out var snapshot, out errorResponse))
                    {
                        regHandlers = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        errorResponse = "-" + nameof(TryGetRegHandlers) + "-: " + errorResponse;
                        return false;
                    }
                    if (!(snapshot.Entity is T t))
                    {
                        throw new DatastructureException(
                            "Found in collection of type " + collectionType + " an entity not of type " +
                            typeof(T) + " but of type " + snapshot.Entity.GetType()
                        );

                    }
                    regHandlers.Add(t);
                }
            }
            else
            {
                // Use "ordinary" mechanism

                if (!dataStorage.TryGetP<IP>(IKType.FromType(entityType), out var collection, out errorResponse))
                {
                    /// This actually qualifies for a <see cref="DatastructureException"/>...
                    regHandlers = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse = "-" + nameof(TryGetRegHandlers) + "-: " +
                        "Collection for " + collectionType.ToStringShort() + " not found.\r\nDetails: " + errorResponse;
                    return false;
                }

                foreach (var subProcess in subProcesses)
                {
                    if (!(typeof(T).IsAssignableFrom(subProcess.RegHandlerType)))
                    {
                        continue;
                    }
                    if (!collection.TryGetP<T>(new IKLong(subProcess.SerialNumber), out var regHandler, out errorResponse))
                    {
                        regHandlers = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        errorResponse = "-" + nameof(TryGetRegHandlers) + "-: " + errorResponse;
                        return false;
                    }
                    //if (!(regHandler is T t))
                    //{
                    //    throw new DatastructureException(
                    //        "Found in collection of type " + collectionType + " an entity not of type " +
                    //        typeof(T) + " but of type " + regHandler.GetType()
                    //    );

                    //}
                    regHandlers.Add(regHandler);
                }
            }
            if (regHandlers.Count < minimumCountRequired)
            {
                errorResponse = "-" + nameof(TryGetRegHandlers) + "-: " +
                    "Found only " + regHandlers.Count + " " + nameof(regHandlers) + " while " + nameof(minimumCountRequired) + " is " + minimumCountRequired;

            }
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString() => RegHandlerType.ToStringVeryShort() + " " + SerialNumber;
    }
}
