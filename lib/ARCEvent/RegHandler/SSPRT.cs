﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCEvent
{
    [Class(
        Description =
        "Strongly typed variant of -" + nameof(SSPR) + "-.\r\n" +
        "\r\n" +
        "See -" + nameof(SSPR) + "- for general explanation.\r\n" +
        "\r\n" +
        "Has higher performance and can set more complex values.\r\n" +
        "\r\n" +
        "Only to be used for transient events (events not root of -" + nameof(ARConcepts.TransientEventTree) + "-), " +
        "in other words, events that do not have to be serialized.\r\n" +
        "(in other words, can only be created from C# code).\r\n" +
        "\r\n" +
        "Does not serialize / deserialize well (if at all).\r\n" +
        "\r\n" +
        "TODO: This class was made generic in Jan 2022 in an attempt to increase performance related to use of\r\n" +
        "TODO: -PExact.TrySetPVDirect- (using enum keys direct).\r\n" +
        "TODO: This did not necessarily lead to any improvements though, meaning that having this as a generic class\r\n" +
        "TODO: is probably strictly not necessary.\r\n" +
        "\r\n" +
        "TODO: This class was changed to implementing PReadOnly instead of PExact in Feb 2022 in an attempt to increase performance " +
        "TOOD: but no significant change in performance was detected.\r\n" +
        "\r\n" +
        "TODO: Consider renaming into SSPTR for better consistency.\r\n" +
        "\r\n" +
        "TODO: Consider making common base class for SSPRT and SSPR, and move TryGetEntityType, TryGetEntityKey to there " +
        "TODO: enabling querying with QueryExpressionTransient against both at the same time.\r\n" +
        "\r\n" +
        "TODO: Make more use of (new, from Jan 2022) high-performance ...PVDirect-methods in PExact.\r\n" +
        "\r\n" +
        "See also -" + nameof(SSPRP) + "-.\r\n"
    )]
    public class SSPRT<TPropertyKeyEnum> : PReadOnly, IRegHandler, IRegHandlerTGRI, IRegHandlerTAERTE where TPropertyKeyEnum : struct, Enum
    {

        public EntityTypeAndKey RecipientId { get; set; }
        public TPropertyKeyEnum PropertyKey { get; set; }
        [ClassMember(Description =
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-." +
            "\r\n"
        )]
        public bool TryGetPropertyKey(out IP retval, out string errorResponse)
        {
            retval = PK.FromEnum(PropertyKey);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public IP Value { get; private set; }
        [ClassMember(Description =
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-." +
            "\r\n"
        )]
        public bool TryGetValue(out IP retval, out string errorResponse)
        {
            retval = Value;
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Note: There is no specific reason for constructor being private. The practical consequence is forcing use of Create-methods.\r\n"
        )]
        private SSPRT(EntityTypeAndKey recipientId, TPropertyKeyEnum propertyKey, IP value)
        {
            RecipientId = recipientId;
            PropertyKey = propertyKey;
            Value = value;
        }

        public override IEnumerator<IKIP> GetEnumerator() => throw new NotImplementedException(
            "This would be quite easy to implement, use 'yield new IKIP ...' for each of RecipientId, PropertyKey and Value"
        );


        public static SSPRT<TPropertyKeyEnum> Create<TValue>(Type entityType, IK entityKey, TPropertyKeyEnum propertyKey, TValue value) where TValue : notnull =>
            Create(new EntityTypeAndKey(entityType, entityKey), propertyKey, value is IP ip ? ip : new PValue<TValue>(value));

        public static SSPRT<TPropertyKeyEnum> Create<TValue>(EntityTypeAndKey recipientId, TPropertyKeyEnum propertyKey, TValue value) where TValue : notnull =>
            Create(recipientId, propertyKey, value is IP ip ? ip : new PValue<TValue>(value));

        public static SSPRT<TPropertyKeyEnum> Create(EntityTypeAndKey recipientId, TPropertyKeyEnum key, IP value)
        {
            var retval = new SSPRT<TPropertyKeyEnum>(recipientId, key, value);
            return retval;
        }

        public bool TryGetRecipientId(out EntityTypeAndKey recipientId, out string errorResponse)
        {
            recipientId = RecipientId;
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public bool TryApplyEventResultToEntity(IP entity, out string errorResponse) =>

            // Old approach, before implementing PReadOnly
            //// This is a possible validation that can be done, but it affects performance, so it is therefore left out
            ////IP.TryGetPV<EntityTypeAndKey>(SSPRTP.RecipientId, out var recipientId, out errorResponse) &&
            ////InvalidObjectTypeException.TryAssertEquals(entity, recipientId.EntityType, out errorResponse, () => "Type of entity given now does not match recipientId.EntityType") &&
            ////
            //TryGetPVDirect<PK>(SSPRTP.PropertyKey, out var propertyKey, out errorResponse) &&
            //IP.TryGetP(SSPRTP.Value, out var value, out errorResponse) &&
            //entity.TrySetP(propertyKey, value, out errorResponse);

            entity.TrySetP(PropertyKey, Value, out errorResponse);


        [ClassMember(Description =
            "Serializes to a format that is human friendly but not necessarily understood by -" + nameof(IRegHandler.TryParse) + "-\r\n" +
            "\r\n" +
            "(This class is not meant to be serialized / deserialized).\r\n" +
            "\r\n" +
            "Explanation: In contrast to -" + nameof(SSPR) + "-, -" + nameof(SSPRT<TPropertyKeyEnum>) + "- " +
            "does not serialize (with the standard -" + nameof(IRegHandler.Serialize) + "-) in a human friendly manner " +
            "because of -" + nameof(RecipientId) + "- being a complex type.\r\n" +
            "TODO: Ascertain that this still holds as of Feb 2022 (and changing to use PReadOnly.\r\n" +
            "(in addition this method does also take into account -" + nameof(PValueEmpty) + "- showing it as [Empty].\r\n"
        )]
        public string Serialize() =>

            //var retval = new StringBuilder();
            //retval.Append(GetType().ToStringVeryShort());
            //IP.GetPV<EntityTypeAndKey>(SSPRTP.RecipientId).Use(r =>
            //    retval.Append("/" + r.EntityType.ToStringVeryShort() + "/" + r.Key.ToString())
            //);
            //retval.Append("/" + IP.GetP(SSPRTP.PropertyKey).ToString()); // PK is IP, so we can ask for P not PV
            ///// TODO: Add serialize hint for object.ToString at call to create
            //var value = IP.GetP(SSPRTP.Value);

            //retval.Append("/" + (value is PValueEmpty ? "[Empty]" : (value.GetV<string>())));
            //return retval.ToString();

            GetType().ToStringVeryShort() + "/" +
            RecipientId.EntityType.ToStringVeryShort() + "/" +
            RecipientId.Key.ToString() + "/" +
            PropertyKey.ToString() + "/" +
            (Value is PValueEmpty ? "[Empty]" : (Value.GetV<string>()));


        [ClassMember(Description =
            "Returns -" + nameof(EntityTypeAndKey.EntityType) + "- part of -" + nameof(RecipientId) + "-.\r\n" +
            "\r\n" +
            "Makes for easier querying with for instance -" + nameof(QueryExpressionTransient) + "-, and enables " +
            "mixing of -" + nameof(SSPR) + "- and -" + nameof(SSPRT<TPropertyKeyEnum>) + "- in queries.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetEntityType(out PValue<Type> retval, out string errorResponse)
        {
            //if (!IP.TryGetPV<EntityTypeAndKey>(SSPRTP.RecipientId, out var recipientId, out errorResponse))
            //{
            //    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            //    return false;
            //}
            retval = new PValue<Type>(RecipientId.EntityType);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Returns -" + nameof(EntityTypeAndKey.Key) + "--part of -" + nameof(RecipientId) + "-.\r\n" +
            "\r\n" +
            "Makes for easier querying with for instance -" + nameof(QueryExpressionTransient) + "-, and enables " +
            "mixing of -" + nameof(SSPR) + "- and -" + nameof(SSPRT<TPropertyKeyEnum>) + "- in queries.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetEntityKey(out PValue<IK> retval, out string errorResponse)
        {
            //if (!IP.TryGetPV<EntityTypeAndKey>(SSPRTP.RecipientId, out var recipient, out errorResponse))
            //{
            //    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            //    return false;
            //}
            retval = new PValue<IK>(RecipientId.Key);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "See -" + nameof(QueryExpressionTransient.TryGetRegIdStatic) + "- for documentation.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetRegIdFromTransientId(IP dataStorage, IK thisKey, out PValue<RegId> retval, out string errorResponse) =>
            QueryExpressionTransient.TryGetRegIdStatic(dataStorage, thisKey, out retval, out errorResponse);

        [ClassMember(Description =
            "See -" + nameof(QueryExpressionTransient.TryGetEventIdStatic) + "- for documentation.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetEventIdFromTransientId(IP dataStorage, IK thisKey, out PValue<EventId> retval, out string errorResponse) =>
            QueryExpressionTransient.TryGetEventIdStatic(dataStorage, thisKey, out retval, out errorResponse);
    }

    public class SSPRTCollection : PCollection
    {

    }

    //[Enum(
    //    Description = "Describes class -" + nameof(SSPRT<TPropertyKeyEnum>) + "-.",
    //    AREnumType = AREnumType.PropertyKeyEnum
    //)]
    //public enum SSPRTP
    //{
    //    __invalid,

    //    [PKType(
    //        Type = typeof(EntityTypeAndKey),
    //        IsObligatory = true
    //    )]
    //    RecipientId,

    //    [PKType(
    //         Description =
    //            "In principle it should not be necessary to demand PK here. IK should suffice.\r\n" +
    //            "but it is not possible to use IK because IK does not implement -" + nameof(ITypeDescriber) + "-",
    //        Type = typeof(PK),
    //        IsObligatory = true
    //    )]
    //    PropertyKey,

    //    [PKType(
    //        Description =
    //            "We would like to use typeof(IP) but -" + nameof(IKIP.TryAssertTypeIntegrity) + "- will not allow it.\r\n" +
    //            "This is possibly an unnecessary restriction, but typef(object) works anyway.",
    //        Type = typeof(object),
    //        IsObligatory = true
    //    )]
    //    Value
    //}
}
