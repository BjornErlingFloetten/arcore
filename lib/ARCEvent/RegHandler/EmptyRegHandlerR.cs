﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCEvent
{

    [Class(Description =
        "The -" + nameof(IRegHandler) + "- used for -" + nameof(RegVerb) + "-s " +
        "like -" + nameof(RegVerb.UNDO) + "- which do not require a registration handler.\r\n" +
        "\r\n" +
        "Convenience class in order not to have to content with possible null value for " +
        "-" + nameof(RegP.RH) + "- / -" + nameof(Event.RegHandler) + "-.\r\n" +
        "\r\n" +
        "See also -" + nameof(EmptyRegHandlerRP) + "-.\r\n"
    )]
    public class EmptyRegHandlerR : PRich, IRegHandler // , IRegHandlerTGRI
    {
        //public bool TryGetRecipientId(out EntityTypeAndKey recipientId, out string errorResponse)
        //{
        //    throw new NotImplementedException();
        //}

        private EmptyRegHandlerR() { }

        public static EmptyRegHandlerR Singleton = new EmptyRegHandlerR();
    }

    public class EmptyRegHandlerRCollection : PCollection
    {

    }

    [Enum(
        Description = "Describes class -" + nameof(EmptyRegHandlerR) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum EmptyRegHandlerRP
    {
        __invalid
    }
}
