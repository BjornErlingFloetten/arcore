﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

/// <summary>
/// Contains the following:
/// <see cref="ARCEvent.IRegHandler"/>
/// <see cref="ARCEvent.IRegHandlerTGRI"/>
/// <see cref="ARCEvent.IRegHandlerTGTE"/>
/// <see cref="ARCEvent.IRegHandlerTATPP"/>
/// <see cref="ARCEvent.RegHandler"/>
/// <see cref="ARCEvent.IRegHandlerTAERTE"/>
/// <see cref="ARCEvent.RegHandlerP"/>
/// </summary>
namespace ARCEvent
{

    [Class(
        Description =
            "Classes implementing this interface serve two functions:\r\n" +
            "\r\n" +
            "1) As a model describing a registration / process.\r\n" +
            "\r\n" +
            "2) The actually handling of -" + nameof(Event) + "-s resulting from an -" + nameof(Reg) + "-, " +
            "that is the actual execution of the process\r\n" +
            "\r\n" +
            "Belongs inside -" + nameof(Reg) + "- as -" + nameof(RegP.RH) + "-, and inside -" + nameof(Event) + "- as -" + nameof(EventP.RH) + "-.\r\n" +
            "\r\n" +
            "This interface describes three major steps when processing an event:\r\n" +
            "\r\n" +
            "1) -" + nameof(TryGetRecipientId) + "-: Returns the identification of the entity for which this Event (Registration) should be stored.\r\n" +
            "Usually done through -" + nameof(IRegHandlerTGRI.TryGetRecipientId) + "-.\r\n" +
            "\r\n" +
            "2) -" + nameof(IRegHandlerTGTE.TryGetTransientEvents) + "-: Calculates the consequence of this Event (Registration), " +
            "based on internal values and queries performed against the given data storage.\r\n" +
            "Note separate interface, since not all models generates transient events.\r\n" +
            "\r\n" +
            "3) - " + nameof(IRegHandlerTATPP.TryAttachToParentProcess) + "-. " +
            "TODO: ADD TEXT.\r\n" +
            "\r\n" +
            "4) -" + nameof(IRegHandlerTAERTE.TryApplyEventResultToEntity) + "-: Applies the result of this Event (Registration) " +
            "to the given entity.\r\n" +
            "\r\n" +
            "Step 1) and 2) are performed at once when an event is generated.\r\n" +
            "Step 2) is repeated whenever retroactive changes have been done in the -" + nameof(ARConcepts.EventStream) + "-.\r\n" +
            "Step 3) and 4) are performed whenever a -" + nameof(Snapshot) + "- has to be updated.\r\n" +
            "\r\n" +
            "Some long term idea for this interface:\r\n" +
            "Dec 2021: Introduce TTL (Time-to-live) to indicate when Registrations may be deleted from the -" + nameof(ARConcepts.RegStream) + "-.\r\n" +
            "\r\n" +
            "Note how the serialized format of a Registration, as given by -" + nameof(Serialize) + "-  +" +
            "is quite similar to the format understood by -" + nameof(RSController) + "-\r\n" +
            "It is very compact by nature, much more compact than -" + nameof(ARConcepts.PropertyStream) + "- " +
            "and probably also much more compact than JSON.\r\n" +
            "\r\n" +
            "See also -" + nameof(CNER) + "- and -" + nameof(SSPR) + "-."
    )]
    public interface IRegHandler : IP
    {

        /// Replaced by <see cref="PKUIAttributeP.SubProcessType"/>
        //[ClassMember(Description =
        //    "The sub process involved with this Registration.\r\n" +
        //    "It is (by default) assumed that there may be multiple instances of the given sub process type-\r\n" +
        //    "\r\n" +
        //    "See also -" + nameof(ARConcepts.SubProcesses) + "-.\r\n" +
        //    "\r\n" +
        //    "TODO: Consider turning this into a -" + nameof(BaseAttribute) + "-, in order to describe\r\n" +
        //    "TODO: the IRegHandlers through the general IP mechanism (for documentation purposes for instance)."
        //)]
        //Type? SubProcess => null;        

        [ClassMember(Description =
            "The default status for the event when created by -" + nameof(EventProcessor) + "-.\r\n" +
            "\r\n" +
            "This could for instance be set to -" + nameof(EventStatus.BLOCKED_DEFAULT) + "- " +
            "for Registrations that require a follow up -" + nameof(RegVerb.ALLOW) + "- before they are processed.\r\n" +
            "\r\n" +
            "TODO: Consider turning this into a -" + nameof(BaseAttribute) + "-, in order to describe\r\n" +
            "TODO: the IRegHandlers through the general IP mechanism (for documentation purposes for instance).\r\n" +
            "\r\n" +
            "TODO: We can not currently (Dec 2021) combine -" + nameof(EventStatus.BLOCKED_DEFAULT) + "- or similar with\r\n" +
            "TODO: -" + nameof(EventStatus.WAIT_FOR_EXEC) + "-."
        )]
        EventStatus DefaultEventStatus => EventStatus.OK;

        [ClassMember(Description =
            "Returns the identification of the entity for which this event (action) should be stored.\r\n" +
            "(entityType describes which collection to use)." +
            "\r\n" +
            "Checks first for -" + nameof(ProcessP.ParentProcessId) + "- and, if not found, checks if the given " +
            "-" + nameof(IRegHandler) + "- instance implements -" + nameof(IRegHandlerTGRI.TryGetRecipientId) + "- " +
            "and if so, calls -" + nameof(IRegHandlerTGRI.TryGetRecipientId) + "-.\r\n" +
            "\r\n" +
            "Typically called by -" + nameof(EventProcessor) + "- when it receives new events in order to know " +
            "where to store the event (which -" + nameof(PCollectionES) + "- and to where within that collection " +
            "(see -" + nameof(PCollectionES.TryStoreEvent) + ").\r\n" +
            "\r\n" +
            "This entity is again the one that will be passed to " +
            "-" + nameof(IRegHandlerTATPP.TryAttachToParentProcess) + "- and\r\n" +
            "-" + nameof(IRegHandlerTAERTE.TryApplyEventResultToEntity) + "-.\r\n" +
            "\r\n" +
            "If the implementing class does not implement " +
            "-" + nameof(IRegHandlerTAERTE) + "- (-" + nameof(IRegHandlerTAERTE.TryApplyEventResultToEntity) + "-) but only\r\n" +
            "-" + nameof(IRegHandlerTGTE) + "- (-" + nameof(IRegHandlerTGTE.TryGetTransientEvents) + "-) " +
            "then it really does not matter which identification is returned here\r\n" +
            "but some identification MUST be returned in order to have a place " +
            "to store the -" + nameof(Event) + "- resulting from an -" + nameof(Reg) + "-.\r\n" +
            "\r\n" +
            "TODO: If the identification changes for the \"same\" event, then we might have a problem with generation of snapshots.\r\n" +
            "TOOD: Example:\r\n" +
            "TODO: '15:29/DO/AddOrder/Customer68/ (this gets SerialNumber 42)\r\n" +
            "TOOD: '42/DO/AddOrder/Customer72/\r\n" +
            "TODO: This is the \"same\" event (because SerialNumber is identical), but they\r\n" +
            "TODO: get stored in two different places, meaning that -" + nameof(PCollectionES) + "-\r\n" +
            "TODO: when generating a snapshot, will call -" + nameof(IRegHandlerTAERTE.TryApplyEventResultToEntity) + "- in BOTH cases.\r\n" +
            "TODO: HOWEVER: If none of them implement -" + nameof(IRegHandlerTAERTE.TryApplyEventResultToEntity) + "- this does not have\r\n" +
            "TODO: any consequence (apart from the uglieness of it all).\r\n" +
            "TODO: And -" + nameof(EventProcessor.TryStoreAndDistributeEvent) + "- will have " +
            "TODO: deleted the transient events for the first one when the second one arrives.\r\n" +
            "\r\n"
        )]
        public static bool TryGetRecipientId(IRegHandler regHandler, out EntityTypeAndKey recipient, out string errorResponse)
        {
            // Note that the actual enum used to store this value is not necessarily 'ProcessP'
            // TODO: Fix use against PExact which will not accept a PK with another type of enum now
            // TODO: (but which will accept IKString.FromString('ParentProcessId'), only that it will be very inefficient).
            if (regHandler.TryGetPV<ParentProcessId>(ProcessP.ParentProcessId, out var parentProcessId))
            {
                recipient = new EntityTypeAndKey(parentProcessId.RegHandlerType, parentProcessId.Key);
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
            /// Note: If would be tempting to check for <see cref="IRegHandlerTGRI"/> FIRST, because
            /// probably most <see cref="IRegHandler"/>s will implement it, but for many it is NOT important
            /// where they are stored (because they do not implement <see cref="IRegHandlerTAERTE"/>) but
            /// IT IS important that they are stored within a super process if they are a sub process
            /// so therefor we must check for <see cref="IRegHandlerTGRI"/> last instead.
            if (regHandler is IRegHandlerTGRI tgri)
            {
                return tgri.TryGetRecipientId(out recipient, out errorResponse);
            }

            recipient = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            errorResponse =
                    "-" + nameof(ProcessP.ParentProcessId) + "- not found, " +
                    "and type -" + regHandler.GetType().ToStringVeryShort() + "- " +
                    "does not implement -" + nameof(IRegHandlerTGRI) + "-";
            return false;
        }

        [ClassMember(Description = "Convenience method supporting 'fluent' coding pattern in -" + nameof(ARCEvent) + "-")]
        public bool TryCastTo<TRegHandlerSubType>(out TRegHandlerSubType subType, out string errorResponse) where TRegHandlerSubType : IRegHandler
        {
            if (!(this is TRegHandlerSubType temp))
            {

                subType = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Caller expected " + GetType().ToStringShort() + " to implement " + typeof(TRegHandlerSubType) + " but it does not.";
                return false;
            }
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            subType = temp;
            return true;
        }

        public static string Translate(string s, params string[] args)
        {
            try
            {
                return string.Format(Translator(s), args);
            }
            catch (Exception ex)
            {
                /// TODO: Move this functionality somewhere else, maybe into <see cref="ARCQuery.Translations"/>
                throw new ARCQuery.TranslationException(
                    "An exception occurred when translating string\r\n" +
                    "\r\n" +
                    "'" + s + "'\r\n" +
                    "\r\n" +
                    "with " + args.Length + " parameters.\r\n" +
                    "\r\n" +
                    "The translated string is\r\n" +
                    "\r\n" +
                    "'" + Translator(s) + "'.\r\n" +
                    "\r\n" +
                    "Typical cause: Translated string contains errors with parameters like {0}, {1} and so on.\r\n",
                    inner: ex
               );
            }
        }

        [ClassMember(Description =
            "The application lifetime Translator.\r\n" +
            "\r\n" +
            "Should be set at application startup.\r\n" +
            "\r\n" +
            "Note that the choice of language mechanism is application specific, not user specific"
        )]
        public static Func<string, string> Translator = s => s;

        private static HashSet<Type>? allRegHandlerDerivedTypes = null;
        [ClassMember(Description =
            "All types implementing -" + nameof(IRegHandler) + "-."
        )]
        public static HashSet<Type> AllIRegHandlerDerivedTypes => allRegHandlerDerivedTypes ??= new Func<HashSet<Type>>(() =>
        {
            UtilCore.AssertCurrentlyStartingUp(); // TODO: Do we need this assertion? Probably not
            var retval = new HashSet<Type>();
            UtilCore.Assemblies.SelectMany(a => a.GetTypes()).Where(t =>
            {
                if (!typeof(IRegHandler).IsAssignableFrom(t)) return false;
                if (t.IsGenericTypeDefinition) return false; /// Typically: [TODO: Add example her, if any)
                if (t.IsAbstract) return false; /// Typically: [TODO: Add example her, if any)
                if (t.IsInterface) return false; /// Excludes <see cref="IRegHandler"/> itself
                return true;
            }).ForEach(t =>
            {
                if (!retval.Add(t)) throw new InvalidTypeException(t, "Already contained in " + nameof(retval) + "\r\n");
            });
            return retval;
        })();

        public static Dictionary<string, Type>? _allIRegHandlerDerivedTypesShorthandNamesDict = null;
        [ClassMember(Description =
            "Key is the short hand representation of an -" + nameof(IRegHandler) + "-'s Type, value is the actual Type\r\n" +
            "\r\n" +
            "Example 1: Key EXER points to value -" + nameof(EXEcuteR) + "-.\r\n" +
            "Example 2: Key RUTR points to value -" + nameof(RunMacroR) + "-.\r\n" +
            "\r\n" +
            "In addition, keys are also added for the full name representation (from -" + nameof(ARCCore.Extensions.ToStringVeryShort) + "-).\r\n" +
            "\r\n" +
            "TODO: Rename into something better, compared to -" + nameof(AllIRegHandlerDerivedTypesTypeDict) + "-."
        )]
        public static Dictionary<string, Type> AllIRegHandlerDerivedTypesShorthandNamesDict = _allIRegHandlerDerivedTypesShorthandNamesDict ??= new Func<Dictionary<string, Type>>(() =>
        {
            UtilCore.AssertCurrentlyStartingUp(); // TODO: Do we need this assertion? Probably not
            var retval = new Dictionary<string, Type>();
            AllIRegHandlerDerivedTypes.ForEach(t =>
            {
                new List<string> {
                    t.ToStringVeryShort(), // TODO: Add more representation here if needed.
                    string.Join("", t.ToStringVeryShort().Where(a => a == char.ToUpper(a))) // PascalCase
                }.ForEach(name =>
                {
                    if (!retval.TryAdd(name, t))
                    {
                        if (t.Equals(retval[name]))
                        {
                            // The type consists of only capital letters, therefore the collision. Ignore.                           
                        }
                        else
                        {
                            throw new InvalidTypeException(t,
                                "The two Registration handler types " + t.ToString() + " and " + retval[name].ToString() + " " +
                                "both resolve to the same name, or short name, '" + name + "'.\r\n" +
                                "\r\n" +
                                "Possible resolution: Change the name of one of the types."
                            );
                        }
                    }
                });
            });
            return retval;
        })();

        public static Dictionary<Type, string>? _allIRegHandlerDerivedTypesTypeDict = null;
        [ClassMember(Description =
            "Key is the type of an -" + nameof(IRegHandler) + "-, Value is the short hand representation of an -" + nameof(IRegHandler) + "-'s Type.\r\n" +
            "\r\n" +
            "Example 1: Key -" + nameof(EXEcuteR) + "- points to value EXER .\r\n" +
            "Example 2: Key -" + nameof(RunMacroR) + "- points to value RMR.\r\n" +
            "\r\n" +
            "TODO: Rename into something better, compared to -" + nameof(AllIRegHandlerDerivedTypesShorthandNamesDict) + "-."
        )]
        public static Dictionary<Type, string> AllIRegHandlerDerivedTypesTypeDict = _allIRegHandlerDerivedTypesTypeDict ??= new Func<Dictionary<Type, string>>(() =>
        {
            UtilCore.AssertCurrentlyStartingUp(); // TODO: Do we need this assertion? Probably not
            var retval = new Dictionary<Type, string>();
            AllIRegHandlerDerivedTypes.ForEach(t =>
                retval.Add(t, string.Join("", t.ToStringVeryShort().Where(a => a == char.ToUpper(a)))) // PascalCase);
            );
            return retval;
        })();

        /// <param name="regVerb">Used in order to interpret <see cref="PKUIAttributeP.NotObligatoryForPLAN"/></param>
        [ClassMember(Description =
            "Attempts to parse (deserialize) an API request to an -" + nameof(IRegHandler) + "- in a standardized manner, " +
            "ensuring that all -" + nameof(PKTypeAttributeP.IsObligatory) + "- values are set and " +
            "returning an error response if superfluous parameters are given.\r\n" +
            "\r\n" +
            "The given apiRequest parameter should start with the name of the actual -" + nameof(IRegHandler) + "- to use.\r\n" +
            "Example: If the API request is like 'RS/DO/now/RunMacroR/...' the parameter passed to this method should be equivalent to " +
            "'RunMacroR/...'.\r\n" +
            "\r\n" +
            "The apiRequest parameter should be in unencoded format.\r\n" +
            "\r\n" +
            "The parameters are assumed to be in the same order as the corresponding -" + nameof(PK) + "- enums.\r\n" +
            "\r\n" +
            "See also -" + nameof(PSPrefix.dtr) + "-, -" + nameof(ICmdHandler.TryParse) + "- and -" + nameof(IP.TryParseDtr) + "-.\r\n" +
            "(code in -" + nameof(ICmdHandler.TryParse) + "- and -" + nameof(IP.TryParseDtr) + "- is quite similar to code here)."
        )]
        public static bool TryParse(RegVerb regVerb, List<string> apiRequest, out IRegHandler regHandler, out string errorResponse)
        {
            if (apiRequest.Count == 0)
            {
                regHandler = EmptyRegHandlerR.Singleton;
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;

                //regHandler = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                //errorResponse = "No registration model name given";
                //return false;
            }
            var regName = apiRequest[0];
            if (!AllIRegHandlerDerivedTypesShorthandNamesDict.TryGetValue(regName, out var type))
            {
                regHandler = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "No Reg (Registration) matching '" + regName + "' was found.";
                return false;
            }
            regHandler = (IRegHandler)System.Activator.CreateInstance(type);
            
            // Apply values in order
            var pks = PK.GetAllPKForEntityType(type).ToList(); // ToList makes for easier iteration
            var parameters = apiRequest.Skip(1).ToList(); // Make comparing easier (same indices)
            var i = 0;
            for (i = 0; i < parameters.Count && i < pks.Count; i++)
            {
                // TODO: Skip if blank and not IsObligatory
                var parameterValue = parameters[i];
                var pk = pks[i];
                if (string.IsNullOrEmpty(parameterValue) && !(
                        (
                            regVerb == RegVerb.PLAN && // TODO: Consider renaming into OnlyObligatoryforDO and compare with != DO here instead.
                            pk.TryGetA<PKUIAttribute>(out var pkui) &&
                            pkui.IP.GetPV<bool>(PKUIAttributeP.NotObligatoryForPLAN, defaultValue: false)
                        ) ||
                        pk.GetA<PKTypeAttribute>().IP.GetPV<bool>(PKTypeAttributeP.IsObligatory, defaultValue: false)
                    )
                )
                {
                    continue; // Blank and not obligatory, OK
                }

                // TODO: Is this check actually relevantæ? Do we refer to Reg Handlers in Reg Handlers?
                if (pk.Type == typeof(Type) && IRegHandler.AllIRegHandlerDerivedTypesShorthandNamesDict.TryGetValue(parameterValue, out var temp))
                {
                    /// <see cref="PKTypeAttribute"/> only knows about types in <see cref="UtilCore.TryGetTypeFromStringFromCache"/>
                    /// It does not know about shorthand representations of types, therefore change back to 'full' name
                    parameterValue = temp.ToStringVeryShort();
                }

                if (!pk.TryCleanParseAndPack(parameterValue, out var property, out errorResponse))
                {
                    regHandler = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                // TODO: Link to full documentation
                    errorResponse =
                        "Illegal value (" + parameterValue + ") given for parameter -" + pk.__enum.GetType().ToStringVeryShort() + "-.-" + pk.__enum + "-\r\n" +
                        "(Parameter no " + (i + 1) + " of " + parameters.Count + " given / " + pks.Count + " possible (counted from one))\r\n" +
                        "Details:\r\n" + errorResponse + "\r\n\r\n" +
                        (pk.IP.TryGetPV<string>(BaseAttributeP.Description, out var description) ?
                            ("Description of parameter:\r\n----------" + description) : ""
                        );
                    return false;
                }
                regHandler.SetP(new IKIP(pk, property));
            }

            if (parameters.Count > i)
            {
                regHandler = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                // TODO: Link to full documentation
                errorResponse =
                    "Excess parameters given for Reg (Registration) -" + type.ToStringShort() + "-.\r\n" +
                    "The excess parameters are: " + string.Join(", ", parameters.Skip(i).Select(r => "'" + r + "'"));
                return false;
            }
            if (pks.Count > i && pks.Skip(i).Any(pk => pk.GetA<PKTypeAttribute>().IsObligatory))
            {
                regHandler = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse =
                    "Missing obligatory parameters for Reg (Registration) -" + type.ToStringShort() + "-.\r\n" +
                    "The missing parameters are:\r\n" + string.Join("\r\n", pks.Skip(i).Where(pk => pk.GetA<PKTypeAttribute>().IsObligatory).Select(pk =>
                        "-----------\r\n-" + pk.__enum.GetType().ToStringVeryShort() + "-.-" + pk.__enum.ToString() + "-" + (pk.IP.TryGetPV<string>(BaseAttributeP.Description, out var description) ?
                        ("\r\n----------" + description) : ""
                    )));
                return false;
            }

            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Serializes to a format understood by -" + nameof(TryParse) + "-.\r\n" +
            "\r\n" +
            "The format should also be safe to put into an HTML page (no HTMLEncode should be needed), " +
            "or use as an URL request for sending to -" + nameof(RSController) + "- (no URLEncode should be needed).\r\n" +
            "\r\n" +
            "Note how the serialized format of a Reg (Registration), as given by -" + nameof(Serialize) + "- " +
            "is quite similar to the format understood by -" + nameof(RSController) + "-\r\n" +
            "It is very compact by nature, much more compact than -" + nameof(ARConcepts.PropertyStream) + "- " +
            "and probably also much more compact than JSON.\r\n" +
            "\r\n"
        )]
        public string Serialize()
        {
            if (this is EmptyRegHandlerR) return "";
            var pks = PK.GetAllPKForEntityType(GetType()).ToList(); // ToList makes for easier iteration
            var retval = new StringBuilder();
            retval.Append(GetType().ToStringVeryShort() + "/");
            for (var i = 0; i < pks.Count; i++)
            {
                if (!TryGetPV<string>(pks[i], out var strValue))
                {
                    // Skip this value
                }
                else
                {
                    // TODO: Verify correctness of encoding here
                    retval.Append(IKCoded.FromUnencoded(IKString.FromString(strValue)).Encoded.
                        // Relax encoding somewhat
                        Replace("0x0020", " ")
                    );
                }
                if (i < pks.Count - 1)
                {
                    retval.Append("/");
                }
            }
            var strRetval = retval.ToString();
            while (strRetval.EndsWith("/"))
            {
                // Remove trailing slashes. These would create problems with later parsing because they would indicate the
                // presence of a value.
                strRetval = strRetval[0..^1];
            }

            return strRetval;
        }
    }

    public interface IRegHandlerTGRI : IRegHandler
    {

        bool TryGetRecipientId(out EntityTypeAndKey recipient, out string errorResponse);

        [ClassMember(Description =
            "Helper method for internal use.\r\n" +
            "\r\n" +
            "TODO: Clean up name / location of this method, or qualifier used to access it.\r\n" +
            "TODO: Make static for instance.\r\n"
        )]
        public bool TryGetRecipientIdInternal<TId, TPropertyKeyEnum>(Type type, TPropertyKeyEnum key, out EntityTypeAndKey recipientId, out string errorResponse) where TId : IK where TPropertyKeyEnum : struct
        {
            if (!TryGetPV<TId>(key, out var entityId, out errorResponse))
            {
                recipientId = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            recipientId = new EntityTypeAndKey(type, entityId);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

    }
    public interface IRegHandlerTGTE : IRegHandler
    {
        [ClassMember(Description =
            "Calculates the consequence of this Event (Registration), based on internal values and queries performed against the given data storage.\r\n" +
            "\r\n" +
            "Typically called by -" + nameof(EventProcessor) + "- when it receives new events or when it has to spool back in the event stream.\r\n" +
            "\r\n" +
            "A desired pattern is assumed to be that \r\n" +
            "Events (Registrations) generates either one or more -" + nameof(SSPRT<TPropertyKeyEnum>) + "- or\r\n" +
            "other transient Events that in turn generate -" + nameof(SSPRT<TPropertyKeyEnum>) + "- transient Events\n" +
            "in order for all branches of the resulting -" + nameof(ARConcepts.TransientEventTree) + "- ending in a -" + nameof(SSPRT<TPropertyKeyEnum>) + "-.\r\n" +
            "\r\n" +
            "This will make it very easy to ascertain the effects of an -" + nameof(Reg) + "- / -" + nameof(Event) + "- because\r\n" +
            "-" + nameof(SSPRT<TPropertyKeyEnum>) + "- is able to describe itself in a consistent easy-to-understand manner.\r\n" +
            "\r\n" +
            "In other words, this would mean that an Event (Registration) must EITHER\r\n" +
            "A) Generate transient Events here OR " +
            "B) Implement -" + nameof(IRegHandlerTAERTE) + "- (-" + nameof(IRegHandlerTAERTE.TryApplyEventResultToEntity) + "-).\r\n" +
            "\r\n" +
            "NOTE: Although the implementing method gets access to the dataStorage, it is meaningless for it\r\n" +
            "NOTE: to WRITE to the storage, this would probably mean writing\r\n" +
            "TODO. to an ephemereal entity contained within a -" + nameof(Snapshot) + "-.\r\n" +
            "NOTE: Writing must instead be done in the 'next step', -" + nameof(IRegHandlerTAERTE.TryApplyEventResultToEntity) + "-.\r\n" +
            "\r\n" +
            // TODO: Delete commented out comment.K RegTime was removed as parameter on 8 Feb 2022.
            //"NOTE: See -" + nameof(Event.CreateTransientEvent) + "- for explanation of parameter regTime here.\r\n" +
            //"NOTE: (the implementing class MUST NOT use this parameter for other purposes than calling -" + nameof(Event.CreateTransientEvent) + "-, " +
            //"NOTE: for instance it MUST NOT call -" + nameof(PCollectionESExtension.TryGetEntityInCollection) + "- with this parameter).\r\n" +
            //"TODO: Maybe it would have been better to put the responsbility of creating transient outside OUTSIDE of IRegHandler.\r\n" +
            "\r\n" +
            "TODO: Note change 8 Feb 2022, returning List<IRegHandler> instead of List<Event> from TryGetTransientEvents.\r\n" +
            "TODO: Consider renaming method name and also parameter transientEvents correspondingly."
        )]
        bool TryGetTransientEvents(IP dataStorage, Event _event, out List<IRegHandler> transientEvents, out string errorResponse);
    }


    [Class(Description =
        "TATPP = TryAttachToParentProcess.\r\n" +
        "\r\n" +
        "TODO: The interface -" + nameof(IRegHandlerTATPP) + "- is probably superfluous " +
        "TODO: and can be replaced with one static method in -" + nameof(IRegHandler) + "-.\r\n" +
        "TODO: (meaning that current implementing classes do not have to implement " +
        "TODO: -" + nameof(IRegHandlerTATPP.TryAttachToParentProcess) + "- at all.\r\n" +
        "\r\n" +
        "See methods\r\n" +
        "-" + nameof(TryAttachToParentProcessFromIRegHandlerSubProcess) + "-.\r\n" +
        "-" + nameof(TryAttachToParentProcessFromSubProcessOfProcess) + "-.\r\n" +
        "\r\n" +
        "See also -" + nameof(PKUIAttributeP.SubProcessType) + "-."
    )]
    public interface IRegHandlerTATPP : IRegHandler
    {
        [ClassMember(Description =
            "Implementing class would call one of\r\n" +
            "-" + nameof(TryAttachToParentProcessFromIRegHandlerSubProcess) + "- or \r\n" +
            "-" + nameof(TryAttachToParentProcessFromSubProcessOfProcess) + "-.\r\n" +
            "\r\n"
        )]
        public bool TryAttachToParentProcess(long serialNumber, IP entity, out string er);

        [ClassMember(Description =
            "Adds \"this\" object as a new -" + nameof(SubProcess) + "- to parents list of sub processes " +
            "(to -" + nameof(ProcessP.SubProcess) + "-).\r\n" +
            "\r\n" +
            "This method IS ONLY relevant for -" + nameof(IRegHandler) + "- being a sub process of another " +
            "-" + nameof(IRegHandler) + "-.\r\n" +
            "For other cases, see -" + nameof(TryAttachToParentProcessFromSubProcessOfProcess) + "-.\r\n" +
            "\r\n" +
            "The parameter serialNumber is needed by -" + nameof(ProcessController) + "- in order to construct schema of " +
            "sub processes."
        )]
        public bool TryAttachToParentProcessFromIRegHandlerSubProcess<TEntity, TPropertyKeyEnum>(
            TPropertyKeyEnum subProcessKey, long serialNumber, IP entity, out string er) where TPropertyKeyEnum : struct, Enum
        {
            if (
                !InvalidObjectTypeException.TryAssertEquals(entity, typeof(TEntity), out er, () => "EntityType does not match Entity given now") ||
                !(
                    entity.TryGetPV<List<SubProcess>>(subProcessKey, out var lines) ||
                    entity.TrySetPV(subProcessKey, lines = new List<SubProcess>(), out er)
                )
            )
            {
                return false;
            }
            lines.Add(new SubProcess(serialNumber, regHandlerType: this.GetType()));
            er = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        /// <param name="parentProcessIdOfChild">
        /// This would be <see cref="ProcessP.ParentProcessId"/> for <see cref="Process"/>es having a parent process.
        /// </param>
        [ClassMember(Description =
            "Adds \"this\" object as a new -" + nameof(SubProcess) + "- to the parent -" + nameof(Process) + "-.\r\n" +
            "\r\n" +
            "This method is relevant for all -" + nameof(Process) + "-es having a parent process, and for all " +
            "-" + nameof(IRegHandler) + "- which (in each individual case) have set a generic parent process id.\r\n" +
            "(but which may in other cases have been \"started directly\".)\r\n" +
            "\r\n" +
            "This method IS NOT relevant for -" + nameof(IRegHandler) + "-s being a sub process of another " +
            "-" + nameof(IRegHandler) + "-. For that case see -" + nameof(TryAttachToParentProcessFromIRegHandlerSubProcess) + "-.\r\n" +
            "\r\n" +
            "The parameter serialNumber is needed by -" + nameof(ProcessController) + "- in order to construct schema of " +
            "sub processes."
        )]
        public bool TryAttachToParentProcessFromSubProcessOfProcess<TPropertyKeyEnum>(
            TPropertyKeyEnum parentProcessIdOfChild, long serialNumber, IP entity, out string er) where TPropertyKeyEnum : struct, Enum
        {
            // NOTE: It would be tempting to get rid of the TPropertyKeyEnum type parameter 
            // NOTE: (get rid of parentProcessIdOfChild) because it will always be "ParentProcessId"
            // NOTE: (that is, we could have looked up with ContaintsKey("ParentProcessId") here instead).
            // NOTE: BUT, it would make it more difficult in a future performance increase scenario
            // NOTE: where we want to implement ContainsKey direct in PExact, and lookup directly
            // NOTE: in its List<object> with the enum value as index.
            if (!((IP)this).ContainsKey(parentProcessIdOfChild))
            {
                /// This is normal because:
                /// 1) For <see cref="Process"/>: Not every process has a parent (there has to be a root process)
                /// 2) For <see cref="IRegHandler"/>: Not every IRegHandler has a parent, it may be "started" directly.
                er = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
            if (
                !InvalidObjectTypeException.TryAssertEquals(entity, typeof(Process), out er,
                    () => "This method is only relevant for applying results to a -" + nameof(Process) + "- \"parent\".") ||
                !(
                    entity.TryGetPV<List<SubProcess>>(ProcessP.SubProcess, out var subProcesses) ||
                    entity.TrySetPV(ProcessP.SubProcess, subProcesses = new List<SubProcess>(), out er)
                )
            )
            {
                return false;
            }
            subProcesses.Add(new SubProcess(serialNumber, regHandlerType: this.GetType()));
            er = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }
    }

    [Class(Description =
        "TAERTE = -" + nameof(TryApplyEventResultToEntity) + "-.\r\n" +
        "\r\n" +
        "This interface should \"ideally\" only be implemented by -" + nameof(SSPR) + "- and -" + nameof(SSPRT<TPropertyKeyEnum>) + "-.\r\n" +
        "\r\n" +
        "The reason for this is that if all leaf nodes in the -" + nameof(ARConcepts.TransientEventTree) + "- " +
        "end up as -" + nameof(SSPR) + "- / -" + nameof(SSPRT<TPropertyKeyEnum>) + "- " +
        "then it is always clearly stated what changes a given -" + nameof(Reg) + "- / -" + nameof(Event) + "- entails.\r\n" +
        "(because what happens inside -" + nameof(IRegHandlerTAERTE.TryApplyEventResultToEntity) + "- is then clearly understood " +
        "as SSPR / SSPRT clearly communicates this)\r\n" +
        "\r\n" +
        "A registration handler implementing this interface can not take part as a -" + nameof(SubProcess) + "- of another process " +
        "because... TODO: Insert why.\r\n"
    )]
    public interface IRegHandlerTAERTE : IRegHandler
    {
        /// <param name="serialNumber">
        /// Needed by <see cref="ProcessController"/> in order to construct schema of 
        /// sub processes. See <see cref="TryApplyEventResultToEntityFromIRegHandlerSubProcess"/> for how it is used.
        /// (It is not needed in itself for applying results)."
        /// </param>
        [ClassMember(Description =
            "Apply the result of this Event (Registration) to the given entity.\r\n" +
            "\r\n" +
            "Typically called by -" + nameof(PCollectionES) + "- when building a -" + nameof(Snapshot) + "-.\r\n" +
            "\r\n" +
            "Note default interface method here, since not all Events / Registrations actually store data back into an entity.\r\n" +
            "Instead they generate transient events like -" + nameof(SSPR) + "-, -" + nameof(SSPRT<TPropertyKeyEnum>) + "- " +
            "which \"ideally\" should be the only Registrations writing to an entity making for easier reading of logs." +
            "\r\n" +
            "Note: It is common to encounter situations where it would be desireable to have a 'dataStorage' parameter " +
            "to this method alongside with the 2D stream position (-" + nameof(EventTime) + "- + -" + nameof(RegTime) + "-).\r\n" +
            "However, adding such parameter would make for very dangerous situations because using them could lead to new " +
            "requests for -" + nameof(Snapshot) + "-s which again could call this method (that is, a loop).\r\n"
        )]
        bool TryApplyEventResultToEntity(IP ip, out string errorResponse);
    }

    [Class(Description =
            "'Dummy'-class, not necessarily in use. See -" + nameof(RegHandlerP) + "- for more information. " +
            "Created because of the concept of " + nameof(EnumAttributeP.CorrespondingClass) + " which is strictly enforced.\r\n" +
            "See instead -" + nameof(IRegHandler) + "-.\r\n"
        )]
    public class RegHandler : PRich
    {
        public RegHandler() => throw new NotImplementedException("This class (" + GetType().ToStringShort() + ") is not expected to be used. If assumption turns out to be wrong, just delete this constructor.\r\n");
    }

    [Enum(
        Description =
            "Describes meta data for any class implementing -" + nameof(IRegHandler) + "-.\r\n" +
            "\r\n" +
            "This values may be attempted stored with each -" + nameof(IRegHandler) + "-.\r\n" +
            "But note that there is no guarantee that all of them can store 'any' kind of value " +
            "(compare -" + nameof(PRich) + "- to -" + nameof(PExact<TPropertyKeyEnum>) + "- for instance).\r\n" +
            "\r\n" +
            "Not necessarily stored for every instance of -" + nameof(IRegHandler) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum RegHandlerP
    {
        __invalid,

        [PKType(
            Description =
                "Pointer to corresponding -" + nameof(Reg) + "- which this -" + nameof(Event) + "- resulted from.\r\n" +
                "Used for report / debugging purposes, not necessary in itself for -" + nameof(ARConcepts.EventSourcing) + "-.\r\n" +
                "\r\n" +
                "TODO: Consider adding an interface IHasRegId signifying whether we can store this value or not.",
            Type = typeof(RegId)
        )]
        RegId,

        [PKType(
            Description =
                "Pointer to corresponding -" + nameof(Event) + "- created as a result of this -" + nameof(Reg) + "-.\r\n" +
                "Used for report / debugging purposes, not necessary in itself for -" + nameof(ARConcepts.EventSourcing) + "-.\r\n" +
                "\r\n" +
                "TODO: Consider adding an interface IHasEventId signifying whether we can store this value or not.",
            Type = typeof(EventId)
        )]
        EventId,
    }
}
