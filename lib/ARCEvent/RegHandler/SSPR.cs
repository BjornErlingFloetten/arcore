﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCEvent
{

    [Class(Description =
        "SSPR = Set Single Property Registration.\r\n" +
        "\r\n" +
        "A standard general model for setting a single property in the 'ordinary' -" + nameof(ARConcepts.PropertyStream) + "- manner.\r\n" +
        "\r\n" +
        "See also -" + nameof(SSPRT<TPropertyKeyEnum>) + "- which serve the same purpose but has better performance and can handle more complex models.\r\n" +
        "\r\n" +
        "This model servies two purposes:\r\n" +
        "1) To have a general mechanism for enabling changes in the -" + nameof(ARConcepts.EventStream) + "- through an API, " +
        "without having to write specific instances of -" + nameof(IRegHandler) + "-.\r\n" +
        "\r\n" +
        "2) To make a changelog for how other -" + nameof(IRegHandler) + "- make changes to entities (to snapshots).\r\n" +
        "(instead of writing directly in the C# code \"Account.NewValue = xxxx\", and thereby HIDING the fact that something\r\n" +
        "was changed, it is better to create an instance of this class.)\r\n" +
        "\r\n" +
        "Purpose 2) Means that all branches of the -" + nameof(ARConcepts.TransientEventTree) + "- generated by -" + nameof(IRegHandler) + "- " +
        "should (as far as possible) end up with a -" + nameof(SSPR) + "-, " +
        "thereby creating what in effect becomes a log of changes to any entity, and their cascading effects on other entities.\r\n" +
        "(in other words, SSPRT (and -" + nameof(SSPRT<TPropertyKeyEnum>) + "-) should ideally be the only registration handlers implementing " +
        "-" + nameof(IRegHandlerTAERTE) + "-.)\r\n" +
        "\r\n" +
        "TODO: Be careful about how this relates to -" + nameof(ARConcepts.PropertyStream) + "-.\r\n" +
        "TODO: We should not create something that duplicates that concept.\r\n" +
        "\r\n" +
        "TODO: Create alternative SMPA, setting multiple properties for a single entity.\r\n" +
        "TODO: This will increase performance because parsing of entity type only has to be done once.\r\n" +
        "TODO: It should also improve logging by clearly stating what changes were done as an atomic operation.\r\n" +
        "\r\n" +
        "See also -" + nameof(SSPRT<TPropertyKeyEnum>) + "-, -" + nameof(CNER) + "- and -" + nameof(SSPRP) + "-."
    )]
    public class SSPR : PExact<SSPRP>, IRegHandler, IRegHandlerTGRI, IRegHandlerTAERTE
    {
        private static readonly int capacity = Enum.GetNames(typeof(SSPRP)).Length - 1;
        public SSPR() : base(capacity) { }

        [ClassMember(Description =
            "NOTE: Avoid using this 'constructor' from C# code.\r\n" +
            "\r\n" +
            "This method should only be used when original -" + nameof(IRegHandler) + "- originates from a serialized format.\r\n" +
            "\r\n" +
            "Use -" + nameof(SSPRT<TPropertyKeyEnum>) + "-.-" + nameof(SSPRT<TPropertyKeyEnum>.Create) + "- instead which will have much better performance."
        )]
        public static SSPR Create(EntityTypeAndKey recipientId, string propertyKey, string value)
        {
            var retval = new SSPR();
            if (
                !retval.IP.TrySetP(SSPRP.RecipientId, new PValue<EntityTypeAndKey>(recipientId), out var errorResponse) ||
                !retval.IP.TrySetP(SSPRP.PropertyKey, new PValue<string>(propertyKey), out errorResponse) ||
                !retval.IP.TrySetPV(SSPRP.Value, value, out errorResponse)
            )
            {
                throw new ArgumentException(errorResponse);
            }

            return retval;
        }

        public bool TryGetRecipientId(out EntityTypeAndKey recipientId, out string errorResponse) =>
            TryGetPVDirect<EntityTypeAndKey>(SSPRP.RecipientId, out recipientId, out errorResponse);

        public bool TryApplyEventResultToEntity(IP entity, out string errorResponse) =>

            /// Note: This validation can be left out (as done in <see cref="SSPRT{TPropertyKeyEnum}"/>) but since SSPR is drastically 
            /// less type and most probably often will be used outside of something covered by a unit test,
            /// it is considered best to have this check here.
            IP.TryGetPV<EntityTypeAndKey>(SSPRP.RecipientId, out var recipientId, out errorResponse) &&
            InvalidObjectTypeException.TryAssertEquals(entity, recipientId.EntityType, out errorResponse, () => "Type of entity given now does not match recipientId.EntityType") &&

            // TODO: Consider caching of operations here (TryGetFromTypeAndFieldName and TryParseAndPack)

            IP.TryGetPV<string>(SSPRP.PropertyKey, out var propertyKey, out errorResponse) &&
            PK.TryGetFromTypeAndFieldName(recipientId.EntityType, propertyKey, out var pk, out errorResponse) &&
            IP.TryGetPV<string>(SSPRP.Value, out var value, out errorResponse) &&
            pk.TryCleanParseAndPack(value, out var ip, out errorResponse) &&
            // entity.TrySetP(IKString.FromString(propertyKey), ip, out errorResponse); // Bug-fix 17.01.2022, use pk found above.
            entity.TrySetP(pk, ip, out errorResponse);
    }

    public class SSPRCollection : PCollection
    {
    }

    [Enum(
        Description = "Describes class -" + nameof(SSPR) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum SSPRP
    {
        __invalid,

        [PKType(
            Type = typeof(EntityTypeAndKey),
            IsObligatory = true
        )]
        RecipientId,

        /// NOTE: We can not set IK as type here, because it does not "implement" <see cref="ITypeDescriber.EnrichKeyMethodName"/>.
        // [PKType(Type = typeof(IK))]
        /// See also <see cref="SSPRT"/>
        [PKType(Type = typeof(string), IsObligatory = true)]
        PropertyKey,

        [PKType(Type = typeof(string), IsObligatory = true)]
        Value
    }
}