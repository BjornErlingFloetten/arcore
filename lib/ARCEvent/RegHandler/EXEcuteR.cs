﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCEvent
{
    [Class(Description =
        "EXEcuteR is the registration handler that \"starts the execution\" of another -" + nameof(IRegHandler) + "-s " +
        "with -" + nameof(EventStatus.WAIT_FOR_EXEC) + "-.\r\n" +
        "(A registration handler with -" + nameof(SubProcess) + "-es).\r\n" +
        "\r\n" +
        "The reason we need this is that we have a chicken-and-egg problem with registration handlers with -" + nameof(EventStatus.WAIT_FOR_EXEC) + "-, " +
        "they must be defined before their sub processes, but they can not do -" + nameof(IRegHandlerTGTE.TryGetTransientEvents) + "- " +
        "with their original -" + nameof(EventTime) + "-, because then they will not have been populated with data from their sub processes.\r\n" +
        "\r\n" +
        "Instead -" + nameof(EventProcessor) + " 'waits' for this registration handler, -" + nameof(EXEcuteR) + "- to call their -" + nameof(IRegHandlerTGTE.TryGetTransientEvents) + "- " +
        "and \"pass it off as its own\".\r\n" +
        "\r\n" +
        "See also -" + nameof(SubProcess) + "- and -" + nameof(EXEcuteRP) + "-.\r\n"
    )]
    public class EXEcuteR : PRich, IRegHandler, IRegHandlerTGRI, IRegHandlerTGTE
    {
        public bool TryGetRecipientId(out EntityTypeAndKey recipientId, out string errorResponse)
        {
            // Note how we are VERY patient with input data here.
            // We could as well have used IP.GetPV, which would result in an exception being thrown instead of returning FALSE
            if (
                !IP.TryGetPV<Type>(EXEcuteRP.RegHandlerType, out var regHandlerType, out errorResponse) ||
                !IP.TryGetPV<IKLong>(EXEcuteRP.RegHandlerId, out var regHandlerId, out errorResponse)
            )
            {
                recipientId = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            recipientId = new EntityTypeAndKey(regHandlerType, regHandlerId);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "This is a special variant, we call -" + nameof(IRegHandlerTGTE.TryGetTransientEvents) + "- " +
            "for the -" + nameof(IRegHandler) + "- that we are \"starting\" or \"executing\", and return its " +
            "transient events as our own"
        )]
        public bool TryGetTransientEvents(IP dataStorage, Event _event, out List<IRegHandler> transientEvents, out string errorResponse)
        {
            if (
                !IP.TryGetPV<Type>(EXEcuteRP.RegHandlerType, out var regHandlerType, out errorResponse) ||
                !IP.TryGetPV<IKLong>(EXEcuteRP.RegHandlerId, out var regHandlerId, out errorResponse) ||
                !dataStorage.TryGetEntityInCollection<IRegHandler>(_event.EventTime, regTime: null, regHandlerType, regHandlerId, out var regHandler, out errorResponse) ||
                !regHandler.TryCastTo<IRegHandlerTGTE>(out var tgte, out errorResponse) ||
                !tgte.TryGetTransientEvents(dataStorage, _event, out transientEvents, out errorResponse)
            )
            {
                transientEvents = null!;
                return false;
            }
            return true;
        }

    }

    public class EXEcuteRCollection : PCollection
    {

    }

    [Enum(
    Description = "Describes class -" + nameof(EXEcuteR) + "-.",
    AREnumType = AREnumType.PropertyKeyEnum
)]
    public enum EXEcuteRP
    {
        __invalid,

        [PKType(
            Type = typeof(Type), IsObligatory = true
        )]
        RegHandlerType,

        [PKType(
            Type = typeof(IKLong), IsObligatory = true
        )]
        RegHandlerId
    }
}
