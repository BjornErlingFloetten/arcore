﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCEvent
{
    [Class(Description =
        "A process is a collection of -" + nameof(IRegHandler) + "- defined from a -" + nameof(Schema) + "-.\r\n" +
        "\r\n" +
        "That is, it is user configurable in contrast to -" + nameof(IRegHandler) + "-s coded in C#.\r\n" +
        "\r\n" +
        "A process may consist of multiple -" + nameof(SubProcess) + "-es.\r\n" +
        "\r\n" +
        "Note how this class does not implement -" + nameof(IRegHandlerTGTE) + "-.\r\n" +
        "\r\n" +
        "See also -" + nameof(ProcessP) + "-."
    )]
    public class Process : PRich, IRegHandler, IRegHandlerTGRI, IRegHandlerTATPP
    {

        [ClassMember(Description =
            "Convenience method stating minimum information information necessary in order to have a meaningful Process object"
        )]
        public static Process Create(SchemaId schemaId, Type entityType)
        {
            var retval = new Process();
            if (
                !retval.IP.TryAddPV(ProcessP.SchemaId, schemaId, out var errorResponse) ||
                !retval.IP.TryAddPV(ProcessP.EntityType, entityType, out errorResponse)
            )
            {
                throw new ArgumentException(errorResponse);
            }
            return retval;
        }

        public bool TryGetRecipientId(out EntityTypeAndKey recipientId, out string errorResponse)
        {
            if (
                !IP.TryGetPV<string>(ProcessP.EntityKey, out var entityKey, out errorResponse)
             )
            {
                recipientId = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            recipientId = new EntityTypeAndKey(
                IP.TryGetPV<Type>(ProcessP.EntityType, out var type) ? type : typeof(Process),
                IKString.FromString(entityKey)
            );
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Only relevant when this -" + nameof(Process) + "- is a sub process of another Process in some hierarchical structure " +
            "of sub processes.\r\n" +
            "\r\n" +
            "In other words, NOT relevant for root Processes.\r\n"
        )]
        public bool TryAttachToParentProcess(long serialNumber, IP entity, out string errorResponse)
        {
            if (IP.TryGetPV<Type>(ProcessP.EntityType, out var entityType) && !typeof(Process).Equals(entityType))
            {
                // TODO. Text below is work in progress.
                // 
                /// This process is probably the root prosess of some hierarchical structure of processes now.
                /// 
                /// It would be meaningless to apply result now because Recipient (as given in <see cref="TryGetRecipientId"/>
                /// can be anything, meaning it has no knowledge / no mechanism for knowing about various 
                /// (<see cref="Schema"/>-based) sub processes
                /// 
                /// It is (anyway) in general only relevant for sub processes to apply themselves to a
                /// parent process (to <see cref="ProcessP.SubProcess"/> in order for the parent to keep track of its sub processes.
                /// There is no mechanism (as of Dec 2021) with which a sub prosess can transfer other information.
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
            /// We now assume that the entity given is a Process (see generic type parameter)
            /// and therefore that <see cref="ProcessP.EntityType"/> is either typeof(Process) or not given at all
            return ((IRegHandlerTATPP)this).TryAttachToParentProcessFromIRegHandlerSubProcess<Process, ProcessP>(ProcessP.SubProcess, serialNumber, entity, out errorResponse);
        }

        [ClassMember(Description =
            "Alternative ('override') to -" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "-.\r\n" +
            "\r\n" +
            "Creates HTML in the \"ordinary\" manner, and also HTML for using -" + nameof(ProcessController) + "-.\r\n" +
            "\r\n" +
            "NOTE: The mere existence of this method is not sufficient in order to avoid " +
            "-" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- being called.\r\n" +
            "(extension method will be chosen anyway if instance is of type -" + nameof(IP) + "-.\r\n" +
            "See " + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- for more details about this issue.\r\n"
        )]
        public string ToHTMLSimpleSingle(bool prepareForLinkInsertion = false, List<IKCoded>? linkContext = null)
        {
            try
            {
                REx.Inc(); /// Use guard here, it is too easy to mix up how <see cref="ARCDoc.Extensions.ToHTMLSimpleSingle"/> is called.
                return
                    (
                        (
                            !IP.TryGetPV<string>(ProcessP.SchemaId, out var schemaId) ||
                            !IP.TryGetPV<RegId>("RegId", out var regId) // "RegId" was stored if possible, no guarantee that it exists
                        ) ?
                        "" : // Not possible to create edit link
                        (
                        "<h1>Process " + schemaId + "</h1>" +
                        "<a href=\"/Process/" + schemaId + "/" + regId + "\">Edit</a>&nbsp;&nbsp;"
                        )
                    ) +
                    ARCDoc.Extensions.ToHTMLSimpleSingleInternal(this, prepareForLinkInsertion, linkContext);
            }
            finally
            {
                REx.Dec();
            }
        }

    }


    [Class(Description =
        "Sub class of -" + nameof(PCollectionES) + "- because we need to be able to " +
        "keep track of -" + nameof(ProcessP.SubProcess) + "-es as they are added or \"deleted\"."
    )]
    public class ProcessCollection : PCollectionES
    {
    }

    [Enum(
        Description = "Describes class -" + nameof(Process) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum ProcessP
    {
        __invalid,

        [PKType(
            Description = "The -" + nameof(Schema) + "- describing how this Process is performed.",
            Type = typeof(SchemaId),
            IsObligatory = true
        )]
        SchemaId,

        [PKType(
            Description =
                "The parent -" + nameof(Process) + "- to which this Process belongs (under which this Process is a child / sub process).\r\n" +
                "\r\n" +
                "TODO: THIS KEY IS NOT USED FOR Process AS OF DEC 2021.\r\n" +
                "TODO: IT ONLY SERVES AS A PLACEHOLDER. Move somewhere else.\r\n" +
                "TODO: Create something called ParentProcessIdName with value 'ParentProcessId' or similar.\r\n" +
                "TODO: Or just create an IKString with value 'ParentProcessid'.\r\n" +
                "TOOO: At the same time, ensure that ALL PK defined within ARCEvent with ParentProcessId as TYPE\r\n" +
                "TODO: are CALLED ParentProcessId.\r\n" +
                "\r\n" +
                "TODO: ParentProcessId is the desired key that we want to use, but we need to initialize a process\r\n" +
                "TODO: with only EntityType being known, therefore we also have EntityType and EntityKey here.\r\n" +
                "\r\n",
            Type = typeof(ParentProcessId)
        )]
        [PKRel(ForeignEntity = typeof(Process))]
        ParentProcessId,

        [PKType(
            Description =
                "The entity type under which this registration handler will be stored.\r\n" +
                "\r\n" +
                "Note: 'Ideally' this should be unnecessary to include here, because -" + nameof(SchemaId) + "- can lead to this " +
                "information (to -" + nameof(SchemaP) + "-.-" + nameof(SchemaP.EntityType) + "-), " +
                "but this will not help -" + nameof(Process) + "-.-" + nameof(Process.TryGetRecipientId) + "- " +
                "because it does not get access to DataStorage (it can not use -" + nameof(ProcessP.SchemaId) + "- " +
                "to look up the -" + nameof(Schema) + "- when it does not have DataStorage).\r\n" +
                "\r\n" +
                "Anyway, -" + nameof(SchemaP.EntityType) + "- can be different from -" + nameof(ProcessP.EntityType) + "- " +
                "when 'this' process is a sub process of another root process (-" + nameof(SchemaP.EntityType) + "- would then " +
                "point to the entity type for which to store the process IF it had been a root process instead, while " +
                "-" + nameof(ProcessP.EntityType) + "- would be typeof(Process) " +
                "(or not given at all (because assumed to be typeof(Process))\r\n" +
                "\r\n" +
                "NOTE: EntityType is probably only interesting for root processes\r\n" +
                "NOTE: (in order for -" + nameof(Process.TryGetRecipientId) + "- to indicate where to store the Event).\r\n" +
                "NOTE: For sub processes it is assumed to always be\r\n" +
                "NOTE: set to typeof(Process).\r\n" +
                "NOTE: (and correspondingly, typeof(Process) is therefore the default value)",
            Type = typeof(Type),
            DefaultValue = typeof(Process),
            IsObligatory = true
        )]
        EntityType,

        [PKType(
            Description =
                "The key for the entity under which this registration handler will be stored.\r\n" +
                "\r\n" +
                "NOTE: We can not set IK as type here, because it does not \"implement\" -" + nameof(ITypeDescriber.EnrichKeyMethodName) + "-.\r\n" +
                "[PKType(Type = typeof(IK))]\r\n" +
                "\r\n" +
                "TODO: Consider implementing -" + nameof(ITypeDescriber.EnrichKeyMethodName) + "-, letting it return IKString.\r\n",
            Type = typeof(string),
            IsObligatory = true
        )]
        EntityKey,

        [PKType(
            Description =
                "The sub processes of a process.\r\n" +
                "\r\n" +
                "Note that for -" + nameof(IRegHandler) + "- based processes, with sub processes, the property key for " +
                "all the sub processes (also if they are of various types) must be named exact 'SubProcess').\r\n" +
                "\r\n" +
                "This is for instance related to how -" + nameof(ProcessController) + "- (-RegHandlerCreateSubProcesses-) " +
                "fetches list of sub processes.\r\n" +
                "\r\n" +
                "Note how -" + nameof(PKUIAttributeP.SubProcessType) + "- is a 'many' collection precisely in order to support this.\r\n",
            Type = typeof(SubProcess), Cardinality = Cardinality.WholeCollection
        )]
        SubProcess
    }

    [Class(Description =
        "Strong typing of primary keys reduces risk of mix-ups in the code.\r\n" +
        "\r\n" +
        "Usually identical to -" + nameof(RegTime.SerialNumber) + "-.\r\n"
    )]
    public class ProcessId : IKLong
    {

        public ProcessId(long value) : base(value) { }

        // Parse / TryParse according to AgoRapide standard. Looks like overkill of course in our case
        public static new ProcessId Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidProcessIdException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse + "\r\n");
        public static bool TryParse(string value, out ProcessId retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out ProcessId retval, out string errorResponse)
        {
            if (string.IsNullOrEmpty(value))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = "Null or Empty value (" + value + ")";
                return false;
            }
            if (!long.TryParse(value, out var valueAsLong))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = "Invalid as long.";
                return false;
            }
            retval = new ProcessId(valueAsLong);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
            return true;
        }


        [ClassMember(Description = "'Implementing' -" + nameof(ITypeDescriber) + "-.")]
        public static new void EnrichKey(PK k) =>
            k.SetValidatorAndParser(new Func<string, ParseResult>(value =>
                TryParse(value, out var retval, out var errorResponse) ?
                    ParseResult.CreateOK(retval) :
                    ParseResult.CreateError(errorResponse)
            ));

        public class InvalidProcessIdException : ApplicationException
        {
            public InvalidProcessIdException(string message) : base(message) { }
            public InvalidProcessIdException(string message, Exception inner) : base(message, inner) { }
        }
    }


    [Class(Description =
        "The process instance under which a sub process belongs.\r\n" +
        "\r\n" +
        "This will often correspond to enum key -" + nameof(ProcessP.ParentProcessId) + "-.\r\n" +
        "\r\n" +
        "For C# coded sub -" + nameof(IRegHandler) + "-s (those which can not be root processes, for instance because " +
        "they are simple models), the actual type of the enum used will be different, " +
        "but the name of the key should always be 'ParentProcessId'.\r\n" +
        "\r\n" +
        "Note: -" + nameof(ParentProcessId) + "- and -" + nameof(EntityTypeAndKey) + "- are quite similar " +
        "but not similar enough to be merged into a single class."
    )]
    public class ParentProcessId : ITypeDescriber
    {

        public Type RegHandlerType { get; private set; }

        public IK Key { get; private set; }

        public ParentProcessId(Type regHandlerType, long serialNumber) : this(regHandlerType, new IKLong(serialNumber)) { }
        public ParentProcessId(Type regHandlerType, IK key)
        {
            RegHandlerType = regHandlerType;
            Key = key;
        }

        public override string ToString() => RegHandlerType.ToStringVeryShort() + " " + Key.ToString();

        // Parse / TryParse according to AgoRapide standard. Looks like overkill of course in our case
        public static ParentProcessId Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidParentProcessIdException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse + "\r\n");
        public static bool TryParse(string value, out ParentProcessId retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out ParentProcessId retval, out string errorResponse)
        {
            if (string.IsNullOrEmpty(value))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = "Null or Empty value (" + value + ")";
                return false;
            }
            var t = value.Split(" ");
            if (t.Length != 2)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 

                // errorResponse = "Not two elements separated by hyphen / minus sign ('-') but " + t.Length + " (value '" + value + "').";

                // Note: There is nothing wrong with splitting on first space only, and accepting spaces in key here.
                errorResponse = "Not two elements separated by a space (' ') but " + t.Length + " (value '" + value + "').";
                return false;
            }

            if (!IRegHandler.AllIRegHandlerDerivedTypesShorthandNamesDict.TryGetValue(t[0], out var regHandlerType))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = "Type '" + t[0] + "' does not resolve to a -" + nameof(IRegHandler) + "- type (value '" + value + "').";
                return false;
            }

            if (!long.TryParse(t[1], out var serialNumber))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = "Invalid serial number '" + t[1] + "' (value '" + value + "').";
                return false;
            }
            retval = new ParentProcessId(regHandlerType, serialNumber);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
            return true;
        }


        [ClassMember(Description = "'Implementing' -" + nameof(ITypeDescriber) + "-.")]
        public static void EnrichKey(PK k) =>
            k.SetValidatorAndParser(new Func<string, ParseResult>(value =>
                TryParse(value, out var retval, out var errorResponse) ?
                    ParseResult.CreateOK(retval) :
                    ParseResult.CreateError(errorResponse)
            ));

        public class InvalidParentProcessIdException : ApplicationException
        {
            public InvalidParentProcessIdException(string message) : base(message) { }
            public InvalidParentProcessIdException(string message, Exception inner) : base(message, inner) { }
        }
    }

}