﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Text;
using ARCCore;
using System.Linq;

/// <summary>
/// Contains the following:
/// <see cref="ARCEvent.MacroAttribute"/>
/// <see cref="ARCEvent.MacroAttributeCollection"/>
/// <see cref="ARCEvent.MacroAttributeP"/>
/// <see cref="ARCEvent.MacroStatus"/>
/// <see cref="ARCEvent.MacroType"/>
/// <see cref="ARCEvent.TCRed"/>
/// <see cref="ARCEvent.TCRedCollection"/>
/// <see cref="ARCEvent.TCRedP"/>
/// <see cref="ARCEvent.TCGreen"/>
/// <see cref="ARCEvent.TCGreenCollection"/>
/// <see cref="ARCEvent.TCGreenP"/>
/// <see cref="ARCEvent.TCBlue"/>
/// <see cref="ARCEvent.TCBlueCollection"/>
/// <see cref="ARCEvent.TCBlueP"/>
/// </summary>
namespace ARCEvent
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    [Class(Description =
        "Describes a group of actions to be performed against the API.\r\n" +
        "\r\n" +
        "The actions (as a group) may be either:\r\n" +
        "\r\n" +
        "1) A collection of -" + nameof(ARConcepts.RegStream) + "- lines " +
        "(" + nameof(MacroAttributeP.RegStream) + "), often with some assertions like -" + nameof(AssertR) + "-, " +
        "together with some -" + nameof(MacroAttributeP.QuerySuggestions) + "- to use afterwards " +
        "in order to analyze result of the Macro.\r\n" +
        "These will be processed by -" + nameof(RSController.TryParseRegistration) + "- and " +
        "-" + nameof(RSController.TryStoreRegistrationAndStoreAndDistributeEvent) + "-.\r\n" +
        "\r\n" +
        "2) A collection of -" + nameof(ARConcepts.PropertyStream) + "- lines.\r\n" +
        "(only relevant when -" + nameof(MacroAttributeP.MacroType) + "- is -" + nameof(MacroType.Translation) + "-.)\r\n" +
        "These will be processed by -" + nameof(PropertyStreamLine) + "-.-" + nameof(PropertyStreamLine.TryParseAndStore) + "-.\r\n" +
        "\r\n" +
        "Note how -" + nameof(MacroType) + "-.-" + nameof(MacroType.BugReport) + "- (which describes bug reports) and " +
        "-" + nameof(MacroType) + "-.-" + nameof(MacroType.UnitTest) + "- are actually two sides of the same thing.\r\n"
    )]
    public class MacroAttribute : BaseAttribute
    {
        [ClassMember(Description =
            "The maximum number of lines that " +
            "-" + nameof(MacroAttributeP.RegStream) + "- or -" + nameof(MacroAttributeP.QuerySuggestions) + "- may contain.\r\n" +
            "\r\n" +
            "This number is related to how -" + nameof(ToHTMLSimpleSingle) + "- produces identifications.\r\n" +
            "It may in principle be set to any number but an extremely high number will have a performance hit.\r\n" +
            "\r\n" +
            "See also -" + nameof(InsertIds) + "-."
        )]
        public const long MaxLineCount = 20;

        [ClassMember(Description = "See -" + nameof(MacroAttributeP.Name) + "-.")]
        public string? Name { get; set; }

        [ClassMember(Description = "See -" + nameof(MacroAttributeP.MacroType) + "-.")]
        public MacroType MacroType { get; set; }

        [ClassMember(Description = "See -" + nameof(MacroAttributeP.MacroTimeHint) + "-.")]
        public MacroTimeHint MacroTimeHint { get; set; }

        [ClassMember(Description =
            "Note how gets added more strongly typed (as List<IKString>) to -" + nameof(MacroAttributeP.Dependencies) + "-.\r\n" +
            "\r\n" +
            "See -" + nameof(MacroAttributeP.Dependencies) + "-."
        )]
        public string[]? Dependencies { get; set; }

        [ClassMember(Description = "See -" + nameof(MacroAttributeP.RegStream) + "-.")]
        public string? RegStream { get; set; }

        [ClassMember(Description = "See -" + nameof(MacroAttributeP.QuerySuggestions) + "-.")]
        public string? QuerySuggestions { get; set; }


        public override void Initialize()
        {
            if (Name != null)
            {
                IP.AddPV(MacroAttributeP.Name, Name);
            }

            if (MacroType == MacroType.__invalid)
            {
                MacroType = MacroType.UnitTest;
            }
            IP.AddPV(MacroAttributeP.MacroType, MacroType);

            if (MacroTimeHint == MacroTimeHint.__invalid)
            {
                MacroTimeHint = MacroTimeHint.Now;
            }
            IP.AddPV(MacroAttributeP.MacroTimeHint, MacroTimeHint);

            if (Dependencies != null && Dependencies.Length > 0)
            {
                IP.AddPV(MacroAttributeP.Dependencies, Dependencies.Select(d => IKString.FromCache(d)).ToList());
            }

            if (RegStream != null)
            {
                IP.AddPV(MacroAttributeP.RegStream, RegStream);
                IP.AddPV(MacroAttributeP.LOC, (long)RegStream.Split("\r\n").Count());
            }

            if (QuerySuggestions != null)
            {
                IP.AddPV(MacroAttributeP.QuerySuggestions, QuerySuggestions);
            }
            base.Initialize();
        }

        [ClassMember(Description =
            "Alternative ('override') to -" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "-.\r\n" +
            "\r\n" +
            "Creates HTML in the \"ordinary\" manner, and also HTML for the -" + nameof(ARConcepts.TransientEventTree) + "-\r\n" +
            "\r\n" +
            "NOTE: The mere existence of this method is not sufficient in order to avoid " +
            "-" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- being called.\r\n" +
            "(extension method will be chosen anyway if instance is of type -" + nameof(IP) + "-.\r\n" +
            "See " + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- for more details about this issue.\r\n"
        )]
        public string ToHTMLSimpleSingle(bool prepareForLinkInsertion = false, List<IKCoded>? linkContext = null)
        {
            try
            {
                REx.Inc(); /// Use guard here, it is too easy to mix up how <see cref="ARCDoc.Extensions.ToHTMLSimpleSingle"/> is called.
                var baseId = EventProcessor.GetCurrentSerialNumber() + 1;
                var htmlElementId = baseId;
                var toRoot = string.Join("", Enumerable.Repeat("../", (linkContext?.Count + 1) ?? 0));
                return
                    (!IP.TryGetPV<string>(MacroAttributeP.ErrorResponse, out var errorResponse) ? "" : (
                        "<p style=\"color:red\">ErrorResponse:<br>\r\n" + WebUtility.HtmlEncode(errorResponse).Replace("\r\n", "<br>\r\n") + "</p>"
                    )) +
                    "<p><a href=\"" + toRoot + "RS/DO/now/RunMacroR/" + IKCoded.FromUnencoded(Key) + "\">Execute all</a></p>\r\n" +
                    (!IP.TryGetPV<string>(MacroAttributeP.RegStream, out var regStream) ? "" : (
                        "<p>-" + nameof(MacroAttributeP.RegStream) + "-<p>\r\n" +
                        "<table><tr><th>" +
                        "<span title=\"" +
                            "When creating new Macros, use this string in order to (in later Registration stream lines), " +
                            "to refer to this Registration\">" +
                        "Id</span></th><th>" +
                        "Registration</th><th>" +
                        "<span title=\"" +
                            "Exec will take into account changes you made in the textbox.\r\n" +
                            "DO will execute the original Registration, not taking into account eventual changes made." +
                        "\">" +
                        "Execute</span></th></tr>\r\n" +
                        string.Join("", regStream.Split("\r\n").Where(s => !string.IsNullOrEmpty(s)).Select(s =>
                        {
                            if (IsComment(s))
                            {
                                return
                                    "<tr><td>&nbsp;</td><td>" +
                                    WebUtility.HtmlEncode(s) +
                                    "</td>&nbsp;<td>" +
                                    "</td></tr>\r\n";
                            }
                            htmlElementId += 1;
                            s = InsertIds(baseId, s);
                            return "<tr><td>" +
                                "{id+" + (htmlElementId - baseId - 1) + "}</td><td>" +
                                "<input id=\"" + htmlElementId + "\" type=\"text\" size=80 value=\"" + s + "\"/>" +
                                "</td><td>" +
                                "<input type=\"submit\" value=\"Exec\" onclick=\"" +
                                    "location.href = '/RS/' + document.getElementById('" + htmlElementId + "').value;\"/>&nbsp;" +
                                "<a href=\"/RS/" + s + "\">DO</a><br>" +
                                "</td></tr>\r\n";
                        })) +
                        "</table>\r\n"
                    )) +
                    (!IP.TryGetPV<string>(MacroAttributeP.QuerySuggestions, out var querySuggestions) ? "" : (
                        "<p>-" + nameof(MacroAttributeP.QuerySuggestions) + "-</p>\r\n" +
                        "<table><tr>" +
                            "<th><span title=\"" +
                                "Use links in left column if you did 'Execute all' at top of this page " +
                                "(this will have displaced all id's by one because RunMacroR itself counts as an event)" +
                                "\">All" +
                            "</span></th>" +
                            "<th><span title=\"" +
                                "Use links in right column if you clicked the single 'DO's for each item " +
                                "in the Registration stream" +
                                "\">Single DO" +
                            "</span></th>" +
                            "</tr>\r\n" +
                        string.Join("", querySuggestions.Split("\r\n").Where(s => !string.IsNullOrEmpty(s)).Select(s =>
                        {
                            if (IsComment(s))
                            {
                                return "<tr><td>&nbsp;</td><td>&nbsp;</td><td>" + WebUtility.HtmlEncode(s) + "</td></tr>";
                            }
                            return "<tr><td>" +
                                "<a href=\"" + toRoot + InsertIds(baseId + 1, s) + "\">All</a></td><td>" +
                                "<a href=\"" + toRoot + InsertIds(baseId, s) + "\">DO</a></td><td>" +
                                WebUtility.HtmlEncode(InsertIds(baseId, s)) + "</td></tr>\r\n";
                        })) +
                        "</table>"
                    )) +
                    "<br><hr><br>\r\n" +
                    ARCDoc.Extensions.ToHTMLSimpleSingleInternal(this, prepareForLinkInsertion, linkContext);
            }
            finally
            {
                REx.Dec();
            }
        }

        [ClassMember(Description =
            "Returns TRUE if a single line " +
            "(from -" + nameof(MacroAttributeP.RegStream) + "- or -" + nameof(MacroAttributeP.QuerySuggestions) + "-) " +
            "is a comment (indicated by '//', '#' or '--')."
        )]
        public static bool IsComment(string line) =>
            // NOTE: If better performance is required one could compare with just first char 
            // NOTE: "return (switch) (line[0]) ..."
            string.IsNullOrEmpty(line) || line.StartsWith("//") || line.StartsWith("#") || line.StartsWith("--");

        [ClassMember(Description =
            "Insert id's into single line " +
            "(from -" + nameof(MacroAttributeP.RegStream) + "- or -" + nameof(MacroAttributeP.QuerySuggestions) + "-) " +
            "based on the given baseId.\r\n" +
            "\r\n" +
            "Will insert a maxium of -" + nameof(MaxLineCount) + "- id's."
        )]
        public static string InsertIds(long baseId, string line)
        {
            line = line.Replace("{id}", baseId.ToString());
            for (var i = 1; i < MaxLineCount; i++)
            {
                line = line.Replace("{id+" + i + "}", (baseId + i).ToString());
            }
            return line;
        }

        private static List<MacroAttribute>? _allMacroAttributesAsList = null;
        public static List<MacroAttribute> AllMacroAttributesAsList = _allMacroAttributesAsList ??= new Func<List<MacroAttribute>>(() =>
        {
            var retval = new List<MacroAttribute>();
            /// Misguided idea. We must be able to "put" Macros anywhere in code.
            /// (Although what we are operating with in a Macro is the <see cref="IRegHandler"/>s, 
            /// the operations themselves may actually test something quite different)
            /// IRegHandler.AllIRegHandlerDerivedTypes.ForEach(type =>

            /// Better approach, allow Macros on any IP derived object
            /// TODO:
            /// TODO: Even better approach, allow Macros on ANY class            
            /// TOOD:
            IP.AllIPDerivedTypesInludingGenericAndAbstract.ForEach(type =>
                GetCustomAttributes(type, typeof(MacroAttribute), inherit: false).ForEach(a =>
                {
                    var macroAttribute = (MacroAttribute)a;
                    if (macroAttribute.Name == null)
                    {
                        throw new NullReferenceException(
                            "-" + nameof(MacroAttribute.Name) + "- not set for " + a.GetType() + " defined for " + type.ToString() + "\r\n" +
                            "\r\n" +
                            "Details (" + nameof(MacroAttribute.RegStream) + "):\r\n-----------------\r\n" + macroAttribute.RegStream +
                            "\r\n------------------\r\n" +
                            "Details (" + nameof(MacroAttribute.QuerySuggestions) + "):\r\n-----------------\r\n" + macroAttribute.QuerySuggestions
                        );
                    }

                    macroAttribute.IP.AddPV(MacroAttributeP.AssemblyName, type.Assembly.GetName().Name);
                    if (type.Namespace != null) macroAttribute.IP.AddPV(MacroAttributeP.MacroNamespace, type.Namespace);
                    macroAttribute.IP.AddPV(MacroAttributeP.ClassType, type);

                    macroAttribute.Initialize();

                    retval.Add(macroAttribute);
                })
            );
            return retval;
        })();


        [ClassMember(Description = "Returns a Key that is usable for storage and identification of this instance.")]
        public IK Key => IKString.FromCache(
            IP.GetPV<Type>(MacroAttributeP.ClassType).ToStringVeryShort() + "_" +
            IP.GetPV<string>(MacroAttributeP.Name, "")); // Name does not have to defined.

        public static Dictionary<IK, MacroAttribute>? _allMacroAttributesAsDict = null;
        public static Dictionary<IK, MacroAttribute> AllMacroAttributesAsDict = _allMacroAttributesAsDict ??= new Func<Dictionary<IK, MacroAttribute>>(() =>
        {
            var retval = new Dictionary<IK, MacroAttribute>();
            AllMacroAttributesAsList.ForEach(a =>
            {
                var key = a.Key;
                if (!retval.TryAdd(key, a))
                {
                    // TODO: Text below is a bit misleading now that we include ClassType above in name.
                    throw new MacroAttributeException(
                        "Duplicate Key (" + key + ") used for " + a.GetType() + "\r\n" +
                        "\r\n" +
                        "Explanation: The Type of the Macro plus the " + nameof(MacroAttribute.Name) + " property " +
                        "is used for identification of each Macro " +
                        "and therefore has to be unique.\r\n" +
                        "\r\n" +
                        // TODO: Meaningless text, class is probably the same now.
                        "First usage as attribute for class " + a.IP.GetPV<Type>(MacroAttributeP.ClassType) + "\r\n" +
                        "Second usage as attribute for class " + retval[key].IP.GetPV<Type>(MacroAttributeP.ClassType) + "\r\n"
                    );
                }
            });
            return retval;
        })();

        public class MacroAttributeException : ApplicationException
        {
            public MacroAttributeException(string message) : base(message) { }
            public MacroAttributeException(string message, Exception inner) : base(message, inner) { }
        }
    }

    public class MacroAttributeCollection : PCollection { }

    [Enum(
        Description = "Describes class -" + nameof(MacroAttribute) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum MacroAttributeP
    {
        __invalid,

        #region Properties that will not change in the course of a Macro's lifetime

        Name,

        [PKType(
            Description = "This value will always be set, with default value being -" + nameof(ARCEvent.MacroType.UnitTest) + "-.\r\n",
            Type = typeof(MacroType),
            DefaultValue = ARCEvent.MacroType.UnitTest
        )]
        MacroType,

        [PKType(
            Description =
                "This value will always be set, with default value being -" + nameof(ARCEvent.MacroTimeHint.Now) + "-.\r\n" +
                "\r\n" +
                "For Macros not operating in 'now' time it is critical for performance that they are marked " +
                "with the appropriate hint.\r\n" +
                "r\n" +
                "Note: One way of discovering if in a given collection some Macros are wrongly marked with 'now' " +
                "(or has wrongly 'now' as default value) " +
                "is to run the collection multiple times and check whether -" + nameof(RegP.CostTryStoreEvent) + "- increases " +
                "for each run, signifying that spool back is occurring",
            Type = typeof(MacroTimeHint),
            DefaultValue = ARCEvent.MacroTimeHint.Now
        )]
        MacroTimeHint,

        [PKType(
            Description =
                "Other Macros (-" + nameof(MacroAttribute) + "-) that this Macro depends on\r\n" +
                "\r\n" +
                "Note: One could possibly believe that dependencies could just be exported from Macro to Macro as simple " +
                "strings to be added to -" + nameof(RegStream) + "- but that would make it more difficult to calculate the " +
                "{id+xxx} values in each individual Macro because it would depend on number of new id's (number of events) " +
                "generated by the Macro on which is depended upon.\r\n" +
                "\r\n" +
                "Note: Preferred type here would have been typeof(IK), but that is not allowed because it does not support ITypeDescriber.",
            Type = typeof(IKString), Cardinality = Cardinality.WholeCollection
        )]
        Dependencies,

        [PKType(Description =
            "The stream of -" + nameof(IRegHandler) + "- commands constituting the Macro.\r\n" +
            "This also includes the necessary assertions, such as -" + nameof(AssertR) + "-.\r\n" +
            "\r\n" +
            "A literal string like {id}, {id+1}, {id+2} and so on will by -" + nameof(MacroAttribute.ToHTMLSimpleSingle) + "- " +
            "be replaced with the corresponding number based on -" + nameof(EventProcessor.GetCurrentSerialNumber) + "-.\r\n" +
            "\r\n" +
            "TODO: Introduce comments (lines starting with // or #)"
        )]
        RegStream,

        [PKType(
            Description = "Number of Lines of code in -" + nameof(RegStream) + "-.",
            Type = typeof(long)
        )]
        LOC,

        [PKType(Description =
            "Suggestions for queries (for human consumption), typically through -" + nameof(ARComponents.ARCQuery) + "-, to use in order to " +
            "analyze the Macro and its results.\r\n" +
            "\r\n" +
            "TODO: Introduce comments (lines starting with // or #)"
        )]
        QuerySuggestions,

        [PKType(Description =
            "The assembly in which this Macro resides."
        )]
        AssemblyName,

        [PKType(
            Description = "The namespace for -" + nameof(ClassType) + "-.",
            Type = typeof(Type)
        )]
        MacroNamespace,

        [PKType(
            Description = "The actual class type that we are a -" + nameof(MacroAttribute) + "- for",
            Type = typeof(Type)
        )]
        ClassType,

        #endregion

        #region Properties that will change in the course of a Macro's lifetime

        [PKType(
            Description = "-" + nameof(EventProcessor.GetCurrentSerialNumber) + "- at start of last execution of Macro.",
            Type = typeof(long)
        )]
        SerialNumberAtStart,

        [PKType(
            Description =
                "NOTE: -" + nameof(MacroStatus) + "-.-" + nameof(ARCEvent.MacroStatus.OK) + "- " +
                "does not necessaryily imply no -" + nameof(ErrorResponse) + "- (or the other way around). " +
                "This depends instead on -" + nameof(MacroType) + "-.",
            Type = typeof(MacroStatus)
        )]
        MacroStatus,

        [PKType(Description =
            "NOTE: -" + nameof(MacroStatus) + "-.-" + nameof(ARCEvent.MacroStatus.OK) + "- " +
            "does not necessaryily imply no -" + nameof(ErrorResponse) + "- (or the other way around). " +
            "This depends instead on -" + nameof(MacroType) + "-."
        )]
        ErrorResponse

        #endregion

    }

    public enum MacroStatus
    {
        __invalid,
        NEW,
        OK,
        ERROR

    }

    [Enum(
        Description =
            "Hint about ordering for execution of macros (-" + nameof(MacroAttribute) + "-).\r\n" +
            "\r\n" +
            "Running through all macros would take a very long time if they haphazardly entered items into " +
            "the event timeline, therefore this hint.\r\n" +
            "\r\n" +
            "Macros running at -" + nameof(MacroTimeHint.bot) + "- (assumed no other evens exist) or -" + nameof(MacroTimeHint.Now) + "- are " +
            "the quickest to execute because they will never require any spool back by the event engine.\r\n" +
            "\r\n" +
            "-" + nameof(RunMacroR) + "- uses this hint to run Macros in the order given here.\r\n"
    )]
    public enum MacroTimeHint
    {
        __invalid,
        [EnumMember(Description = "bot = 'beginning of time'")]
        bot,
        Past,
        Now,
        Future,
        [EnumMember(Description =
            "eot = 'end of time'.\r\n" +
            "\r\n" +
            "Currently not possible to use. We can only QUERY at eot, not store anything there (except exactly one event)"
        )]
        eot
    }

    // Some classes (Red, Green and Blue) that can be used for unit testing

    public class TCRed : PRich
    {
    }
    public class TCRedCollection : PCollectionES
    {
    }

    [Enum(
        Description = "Describes class -" + nameof(TCRed) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum)]
    public enum TCRedP
    {
        __invalid,
        First,
        Second,
        Third
    }

    public class TCGreen : PRich
    {
    }
    public class TCGreenCollection : PCollectionES
    {
    }
    [Enum(
        Description = "Describes class -" + nameof(TCGreen) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum)]
    public enum TCGreenP
    {
        __invalid,
        First,
        Second,
        Third
    }

    [Enum(
        Description = "Describes class -" + nameof(TCBlue) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum)]
    public enum TCBlueP
    {
        __invalid,
        First,
        Second,
        Third
    }

    public class TCBlue : PRich
    {
    }

    public class TCBlueCollection : PCollectionES
    {
    }
}
