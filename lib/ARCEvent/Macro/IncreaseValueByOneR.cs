﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCEvent
{

    [Class(Description =
        "Increases the given value by one (sets to one if not found).\r\n" +
        "\r\n" +
        "See also -" + nameof(IncreaseValueByOneRP) + "-."
    )]
    public class IncreaseValueByOneR : PExact<IncreaseValueByOneRP>, IRegHandler, IRegHandlerTGRI, IRegHandlerTGTE
    {
        private static readonly int capacity = Enum.GetNames(typeof(IncreaseValueByOneRP)).Length - 1;
        public IncreaseValueByOneR() : base(capacity) { }

        public bool TryGetRecipientId(out EntityTypeAndKey recipientId, out string errorResponse) =>
            IP.TryGetPV<EntityTypeAndKey>(IncreaseValueByOneRP.RecipientId, out recipientId, out errorResponse);

        public bool TryGetTransientEvents(IP dataStorage, Event _event, out List<IRegHandler> transientEvents, out string errorResponse)
        {
            if (
                !IP.TryGetPV<EntityTypeAndKey>(IncreaseValueByOneRP.RecipientId, out var recipient, out errorResponse) ||
                !IP.TryGetPV<string>(IncreaseValueByOneRP.PropertyKey, out var propertyKey, out errorResponse) ||
                !dataStorage.TryGetEntityInCollection<IP>(_event.EventTime, regTime: null, recipient.EntityType, recipient.Key, out var entity, out errorResponse)
            )
            {
                transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }

            transientEvents = new List<IRegHandler>
            {
                // SSPRT would be more efficient probably,
                // but after it was changed to a Generic class, we are unable to use it since we do not have the generic type parameter here.
                //SSPRT.Create(new ParentProcessId(entityType, IKString.FromString(entityKey)), pk, 
                //    new PValue<string>((entity.TryGetPV<long>(pk, out var existingValue, out _) ? (existingValue+1) : 1L).ToString())
                //)

                SSPR.Create(recipient, propertyKey,                    
                    // Convert propertyKey to IKString in order to avoid wrong overload being chosen.
                    (entity.TryGetPV<long>(IKString.FromString(propertyKey), out var existingValue, out _) ? 
                        (existingValue+1) : 
                        1L).ToString()
                )

            };

            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

    }

    public class IncreaseValueByOneRCollection : PCollection
    {

    }

    [Enum(
        Description = "Describes class -" + nameof(IncreaseValueByOneR) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum IncreaseValueByOneRP
    {
        __invalid,

        [PKType(
            Type = typeof(EntityTypeAndKey),
            IsObligatory = true
        )]
        RecipientId,

        //[PKType(Type = typeof(Type), IsObligatory = true)]
        //EntityType,

        //[PKType(IsObligatory = true)]
        //EntityKey,

        [PKType(IsObligatory = true)]
        PropertyKey,
    }
}