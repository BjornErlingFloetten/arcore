﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCEvent
{
    [Class(Description =
        "Provokes creation of a snapshot.\r\n" +
        "\r\n" +
        "As of Dec 2021 only probable use is for unit testing -" + nameof(ARConcepts.EventSourcing) + "- " +
        "(testing -" + nameof(EventProcessor) + "- and -" + nameof(PCollectionES) + "-).\r\n" +
        "\r\n" +
        // TODO: Delete commented out TODO (fixed)
        //"TODO: Consider distinguishing between recipient-entity and snapshot-entity, just like\r\n" +
        //"TODO: -" + nameof(ConditionalCreateTransientEventA) + "- disinguishes between -" + nameof( recipient-entity and transient-event entity.\r\n" +
        //"\r\n" +
        "See also -" + nameof(ProvokeSnapshotCreationRP) + "-."
    )]
    public class ProvokeSnapshotCreationR : PRich, IRegHandler, IRegHandlerTGRI, IRegHandlerTGTE
    {
        public bool TryGetRecipientId(out EntityTypeAndKey recipientId, out string errorResponse) =>
            IP.TryGetPV<EntityTypeAndKey>(ProvokeSnapshotCreationRP.RecipientId, out recipientId, out errorResponse);

        public bool TryGetTransientEvents(IP dataStorage, Event _event, out List<IRegHandler> transientEvents, out string errorResponse)
        {
            if (
                !IP.TryGetPV<EntityTypeAndKey>(ProvokeSnapshotCreationRP.SnapshotId, out var snapshotId, out errorResponse) ||
                !dataStorage.TryGetCollection(snapshotId.EntityType, out var entityCollection, out errorResponse) ||
                !entityCollection.TryGetP(
                    IP.TryGetPV<EventTime>(ProvokeSnapshotCreationRP.EventTime, out var eventTime, out _) ? eventTime : EventTime.NowAsQuery(),
                    IP.TryGetPV<RegTime>(ProvokeSnapshotCreationRP.RegTime, out var regTimeTE, out _) ? regTimeTE : null,
                    snapshotId.Key,
                    out var _,
                    out errorResponse
                )
            )
            {
                transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }

            transientEvents = new List<IRegHandler>()
            {
            };

            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }
    }

    public class ProvokeSnapshotCreationRCollection : PCollection { }
    [Enum(
        Description = "Describes class -" + nameof(ProvokeSnapshotCreationR) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]

    public enum ProvokeSnapshotCreationRP
    {
        __invalid,

        [PKType(
            Description = "Defaults to 'now', see -" + nameof(ARCEvent.EventTime.NowAsQuery) + "-,",
            Type = typeof(EventTime)
        )]
        EventTime,

        [PKType(
            Description = "Defaults to NULL",
            Type = typeof(RegTime)
        )]
        RegTime,

        [PKType(
            Description =
                "It is important for this to be something else than -" + nameof(SnapshotId) + "- " +
                "in order not to influence what is being tested.",
            Type = typeof(EntityTypeAndKey),
            IsObligatory = true
        )]
        RecipientId,

        //[PKType(
        //     Description =
        //        "Recipient entity type for which this Registration will be stored (important to not be -" + nameof(SEntityType) +"- " +
        //        "in order for test not to influence on what is being tested).",
        //     Type = typeof(Type),
        //     IsObligatory = true
        // )]
        //REntityType,

        //[PKType(
        //    Description =
        //        "Recipient entity key for which this Registration will be stored (important to not be -" + nameof(SEntityType) + "- " +
        //        "in order for test not to influence on what is being tested).",
        //    Type = typeof(string),
        //    IsObligatory = true
        //)]
        //REntityKey,

        [PKType(
            Description =
                "It is important for this to be something else than -" + nameof(RecipientId) + "- " +
                "in order not to influence what is being tested.",
            Type = typeof(EntityTypeAndKey),
            IsObligatory = true
        )]
        SnapshotId,

        //[PKType(
        //    Type = typeof(Type),
        //    IsObligatory = true
        //)]
        //SEntityType,

        //[PKType(
        //    Type = typeof(string),
        //    IsObligatory = true
        //)]
        //SEntityKey,
    }
}
