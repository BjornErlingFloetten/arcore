﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCEvent
{
    [Class(Description =
        "Makes an assertion against the given entity at the specified -" + nameof(EventTime) + "-, -" + nameof(RegTime) + "- " +
        "point in the -" + nameof(ARConcepts.PropertyStream2D) + "-\r\n" +
        "\r\n" +
        "If -" + nameof(AssertRP.PropertyKey) + "- is not given then assertion is done against the whole entity. " +
        "Relevant -" + nameof(AssertType) + "- would then be -" + nameof(AssertType.Exist) + "-, -" + nameof(AssertType.NotExist) + "-.\r\n" +
        "\r\n" +
        "If -" + nameof(AssertRP.Value) + "- is not given then relevant -" + nameof(AssertType) + "- " +
        "would also be -" + nameof(AssertType.Exist) + "-, -" + nameof(AssertType.NotExist) + "-.\r\n" +
        "\r\n" +
        "TODO: BUG: As of Dec 2021 assertion against existence of entity is not possible because -" + nameof(TryGetRecipientId) + "- will\r\n" +
        "TODO: BUG: automatically create the entity.\r\n" +
        "TODO: BUG:\r\n" +
        "TODO: BUG: SOLUTION. Either:\r\n" +
        "TODO: BUG:\r\n" +
        "TODO: BUG: 1) Change -" + nameof(PCollectionES) + " (TryGenerateAndStoreSnapshotAsRelevant) so as to\r\n" +
        "TODO: BUG: NOT return an entity if no events are found (BUT, this is a breaking change, result of -" + nameof(CNER) + "- would\r\n" +
        "TODO: BUG: for instance not work as expected).\r\n" +
        "TODO: BUG:\r\n" +
        "TODO: BUG: or\r\n" +
        "TODO: BUG:\r\n" +
        "TODO: BUG: 2) Add recipient EntityType and EntityKey here (which would make use of this registration handler more cumbersome).\r\n" +
        "TODO: BUG:\r\n" +
        "TODO: BUG: Alternative 1) is probably the preferred one, CNER has limited use anyway.\r\n" +
        "\r\n" +
        "See also -" + nameof(AssertRP) + "-.\r\n"
    )]
    public class AssertR : PRich, IRegHandler, IRegHandlerTGRI, IRegHandlerTGTE
    {
        public bool TryGetRecipientId(out EntityTypeAndKey recipientId, out string errorResponse) =>
            IP.TryGetPV<EntityTypeAndKey>(AssertRP.RecipientId, out recipientId, out errorResponse);

        public bool TryGetTransientEvents(IP dataStorage, Event _event, out List<IRegHandler> transientEvents, out string errorResponse)
        {
            if (
                !IP.TryGetPV<AssertType>(AssertRP.AssertType, out var assertType, out errorResponse) ||
                !IP.TryGetPV<EntityTypeAndKey>(AssertRP.RecipientId, out var recipient, out errorResponse) ||
                !dataStorage.TryGetCollection(recipient.EntityType, out var entityCollection, out errorResponse)
            )
            {
                transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }

            if (IP.TryGetPV<string>(AssertRP.Value, out var value, out errorResponse))
            {
                // Assert against value
                if (
                    !entityCollection.TryGetP(
                        IP.TryGetPV<EventTime>(AssertRP.EventTime, out var eventTime, out _) ? eventTime : EventTime.NowAsQuery(),
                        IP.TryGetPV<RegTime>(AssertRP.RegTime, out var regTimeTE, out _) ? regTimeTE : null,
                        recipient.Key,
                        out var snapshot,
                        out errorResponse
                    ) ||
                    /// TODO: Change from TryGetPV to using <see cref="ARCQuery.Extensions.TryGetP"/> instead
                    /// TODO: in order to use <see cref="ARCQuery.CompoundKey"/>s.
                    !IP.TryGetPV<string>(AssertRP.PropertyKey, out var propertyKey, out errorResponse) ||
                    !snapshot.Entity.TryGetPV<string>(IKString.FromString(propertyKey), out var compareValue, out errorResponse)
                )
                {
                    errorResponse = "Assert against value '" + value + "' failed.\r\nDetails: " + errorResponse;
                    transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return false;
                }
                switch (assertType)
                {
                    case AssertType.Equal:
                        if (value != compareValue)
                        {
                            errorResponse = "NOT EQUAL: Found value '" + compareValue + "' not equal to required value '" + value + "'.";
                            transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            return false;
                        }
                        break;
                    case AssertType.NotEqual:
                        if (value == compareValue)
                        {
                            errorResponse = "EQUAL: Found value '" + compareValue + "' equal to required value '" + value + "'.";
                            transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            return false;
                        }
                        break;
                    default:
                        errorResponse =
                                "Assert against value '" + value + "' failed.\r\n" +
                                "Reason: Invalid -" + nameof(AssertType) + "-: " + assertType + ".\r\n" +
                                "Resolution: Use one of -" + nameof(AssertType.Equal) + "- or -" + nameof(AssertType.NotEqual) + ".";
                        break;

                }
            }
            else if (IP.TryGetPV<string>(AssertRP.PropertyKey, out var propertyKey, out errorResponse))
            {
                // Assert against existence of key
                if (
                    !entityCollection.TryGetP(
                        IP.TryGetPV<EventTime>(AssertRP.EventTime, out var eventTime, out _) ? eventTime : EventTime.NowAsQuery(),
                        IP.TryGetPV<RegTime>(AssertRP.RegTime, out var regTimeTE, out _) ? regTimeTE : null,
                        recipient.Key,
                        out var snapshot,
                        out errorResponse
                    )
                )
                {
                    errorResponse = "Assert against existence of property key '" + propertyKey + "' failed.\r\nDetails: " + errorResponse;
                    transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return false;
                }
                switch (assertType)
                {
                    case AssertType.Exist:
                        if (!snapshot.Entity.ContainsKey(propertyKey))
                        {
                            errorResponse = "NOT EXISTS: Did not find property key '" + propertyKey + "'.";
                            transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            return false;
                        }
                        break;
                    case AssertType.NotExist:
                        if (snapshot.Entity.ContainsKey(propertyKey))
                        {
                            errorResponse = "EXISTS: Found property key '" + propertyKey + "'.";
                            transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            return false;
                        }
                        break;
                    default:
                        errorResponse =
                                "Assert against existence of property key '" + propertyKey + "' failed.\r\n" +
                                "Reason: Invalid -" + nameof(AssertType) + "-: " + assertType + ".\r\n" +
                                "Resolution: Use one of -" + nameof(AssertType.Equal) + "- or -" + nameof(AssertType.NotEqual) + ".";
                        break;
                }
            }
            else
            {
                // Assert against existence of entity
                switch (assertType)
                {
                    case AssertType.Exist:
                        if (
                            !entityCollection.TryGetP(
                                IP.TryGetPV<EventTime>(AssertRP.EventTime, out var eventTime, out _) ? eventTime : EventTime.NowAsQuery(),
                                IP.TryGetPV<RegTime>(AssertRP.RegTime, out var regTimeTE, out _) ? regTimeTE : null,
                                recipient.Key,
                                out var snapshot,
                                out errorResponse
                            )
                        )
                        {
                            errorResponse = "NOT EXISTS: Did not find entity '" + recipient.Key + "'.";
                            transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            return false;
                        }
                        break;
                    case AssertType.NotExist:
                        if (
                            entityCollection.TryGetP(
                                IP.TryGetPV<EventTime>(AssertRP.EventTime, out eventTime, out _) ? eventTime : EventTime.NowAsQuery(),
                                IP.TryGetPV<RegTime>(AssertRP.RegTime, out regTimeTE, out _) ? regTimeTE : null,
                                recipient.Key,
                                out snapshot,
                                out errorResponse
                            )
                        )
                        {
                            errorResponse = "EXISTS: Found entity key '" + recipient.Key + "'.";
                            transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            return false;
                        }
                        break;
                    default:
                        errorResponse =
                                "Assert against existence of entity '" + recipient.Key + "' failed.\r\n" +
                                "Reason: Invalid -" + nameof(AssertType) + "-: " + assertType + ".\r\n" +
                                "Resolution: Use one of -" + nameof(AssertType.Equal) + "- or -" + nameof(AssertType.NotEqual) + ".";
                        break;
                }
            }

            transientEvents = new List<IRegHandler>
            {
            };

            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

    }

    public class AssertRCollection : PCollection
    {

    }

    [Enum(
        Description =
            "Describes class -" + nameof(AssertR) + "-.\r\n" +
            "\r\n" +
            "See -" + nameof(SSPRP) + "- for reason why 'string' is mostly used as type here.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum AssertRP
    {
        __invalid,

        [PKType(
            Type = typeof(AssertType),
            IsObligatory = true
        )]
        AssertType,

        [PKType(
            Description = "Defaults to 'now', see -" + nameof(ARCEvent.EventTime.NowAsQuery) + "-,",
            Type = typeof(EventTime)
        )]
        EventTime,
        
        [PKType(
            Description = "Defaults to NULL",
            Type = typeof(RegTime)
        )]
        RegTime,

        [PKType(
            Type = typeof(EntityTypeAndKey),
            IsObligatory = true
        )]
        RecipientId,

        //[PKType(
        //    Type = typeof(Type),
        //    IsObligatory = true
        //)]
        //EntityType,

        //[PKType(
        //    Type = typeof(string),
        //    IsObligatory = true
        //)]
        //EntityKey,

        [PKType(
            Description =
                "Not relevant for -" + nameof(ARCEvent.AssertType) + "- - " + nameof(ARCEvent.AssertType.Exist) + "- / -" + nameof(ARCEvent.AssertType.NotExist) + "- " +
                "against entity key only.",
            Type = typeof(string),
            IsObligatory = false
        )]
        PropertyKey,

        [PKType(
            Description =
                "Not relevant for -" + nameof(ARCEvent.AssertType) + "- - " + nameof(ARCEvent.AssertType.Exist) + "- / -" + nameof(ARCEvent.AssertType.NotExist) + "- " +
                "against -" + nameof(PropertyKey) + "-.",
            Type = typeof(string),
            IsObligatory = false
        )]
        Value
    }

    [Enum(
        Description = "The type of assertion that is to be performed.\r\n",
        AREnumType = AREnumType.OrdinaryEnum
    )]
    public enum AssertType
    {
        __invalid,
        Equal,
        NotEqual,
        Exist,
        NotExist,
        LT,
        LTE,
        GTE,
        GT,
    }

}
