﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCEvent
{
    [Class(Description =
        "Will create a -" + nameof(SSPR) + "- transient event if parameter -" + nameof(ConditionalCreateTransientEventRP) + "-.-" + 
        nameof(ConditionalCreateTransientEventRP.Create) + "- is TRUE.\r\n" +
        "\r\n" +
        "As of Dec 2021 only probable use is for unit testing -" + nameof(ARConcepts.EventSourcing) + "- " +
        "(testing -" + nameof(EventProcessor) + "- and -" + nameof(PCollectionES) + "-).\r\n" +
        "\r\n" +
        "See also -" + nameof(ConditionalCreateTransientEventRP) + "-."
    )]
    public class ConditionalCreateTransientEventR : PRich, IRegHandler, IRegHandlerTGRI, IRegHandlerTGTE
    {
        public bool TryGetRecipientId(out EntityTypeAndKey recipientId, out string errorResponse) =>
            IP.TryGetPV<EntityTypeAndKey>(ConditionalCreateTransientEventRP.RecipientId, out recipientId, out errorResponse);

        public bool TryGetTransientEvents(IP dataStorage, Event _event, out List<IRegHandler> transientEvents, out string errorResponse)
        {
            if (!IP.GetPV<bool>(ConditionalCreateTransientEventRP.Create, defaultValue: false))
            {
                transientEvents = new List<IRegHandler>();
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }

            if (
                !IP.TryGetPV<EntityTypeAndKey>(ConditionalCreateTransientEventRP.TEId, out var teId, out errorResponse) ||
                !IP.TryGetPV<string>(ConditionalCreateTransientEventRP.TEPropertyKey, out var propertyKey, out errorResponse) ||
                !IP.TryGetPV<string>(ConditionalCreateTransientEventRP.TEValue, out var value, out errorResponse)
            )
            {
                transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }

            transientEvents = new List<IRegHandler>
            {
                SSPR.Create(teId, propertyKey, value)
            };

            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }
    }

    public class ConditionalCreateTransientEventRCollection : PCollection { }

    [Enum(
        Description =
            "Describes class -" + nameof(ConditionalCreateTransientEventR) + "-.\r\n" +
            "\r\n" +
            "See -" + nameof(SSPRP) + "- for reason why 'string' is mostly used as type here.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum ConditionalCreateTransientEventRP
    {
        __invalid,

        [PKType(
            Description = "If FALSE then no transient event will be created.",
            Type = typeof(bool),
            IsObligatory = true
        )]
        Create,

        [PKType(
            Description =
                "It is important for this to be something else than -" + nameof(TEId) + "- " +
                "in order not to influence what is being tested.",
            Type = typeof(EntityTypeAndKey),
            IsObligatory = true
        )]
        RecipientId,

        //[PKType(
        //    Description = "Recipient entity type for which this Registration will be stored.",
        //    Type = typeof(Type),
        //    IsObligatory = true
        //)]
        //REntityType,

        //[PKType(
        //    Description = "Recipient entity key for which this Registration will be stored.",
        //    Type = typeof(string),
        //    IsObligatory = true
        //)]
        //REntityKey,

        [PKType(
            Description =
                "It is important for this to be something else than -" + nameof(RecipientId) + "- " +
                "in order not to influence what is being tested.",
            Type = typeof(EntityTypeAndKey),
            IsObligatory = true
        )]
        TEId,

        //[PKType(
        //    Description = "Transient event entity type. Copied into -" + nameof(SSPRP.EntityType) + "-.",
        //    Type = typeof(Type)
        //)]
        //TEEntityType,

        //[PKType(
        //    Description = "Transient event entity key. Copied into -" + nameof(SSPRP.EntityKey) + "-.",
        //    Type = typeof(string)
        //)]
        //TEEntityKey,

        [PKType(
            Description = "Transient event property key. Copied into -" + nameof(SSPRP.PropertyKey) + "-.",
            Type = typeof(string)
        )]
        TEPropertyKey,

        [PKType(
            Description = "Transient event property value. Copied into -" + nameof(SSPRP.Value) + "-.",
            Type = typeof(string)
        )]
        TEValue
    }
}