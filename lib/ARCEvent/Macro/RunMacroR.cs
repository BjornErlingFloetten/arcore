﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using ARCCore;
using ARCAPI;
using System.Linq;

/// <summary>
/// Contains the following:
/// <see cref="ARCEvent.RunMacroR"/>
/// <see cref="ARCEvent.RunMacroRCollection"/>
/// <see cref="ARCEvent.RunMacroRP"/>
/// <see cref="ARCEvent.MacroExecutionResult"/>
/// <see cref="ARCEvent.MacroExecutionResultCollection"/>
/// <see cref="ARCEvent.MacroExecutionResultP"/>
/// </summary>
namespace ARCEvent
{

    [Class(Description =
        "Runs the specified Macro (-" + nameof(MacroAttribute) + "-).\r\n" +
        "\r\n" +
        "Observes error conditions on multiple levels:\r\n" +
        "1) Whether -" + nameof(Reg) + "- is accepted by -" + nameof(RSController) + "- at all (for instance whether parsing succeeded).\r\n" +
        "2) Whether the generated -" + nameof(Reg) + "- ends up with -" + nameof(RegStatus) + "-.-" + nameof(RegStatus.ERROR) + "-.\r\n" +
        "3) Whether any events generated (root events or events in the -" + nameof(ARConcepts.TransientEventTree) + "-) " +
        "ends up with -" + nameof(EventStatus) + "-.-" + nameof(EventStatus.ERROR) + "-.\r\n" +
        "\r\n" +
        "Draws a final conclusion, whether to report -" + nameof(EventStatus) + "-.-" + nameof(EventStatus.ERROR) + "- or not.\r\n" +
        "What is considered an ERROR depends on -" + nameof(MacroType) + "-:\r\n" +
        "\r\n" +
        "Some examples of this are:\r\n" +
        "\r\n" +
        "-" + nameof(MacroType.UnitTest) + "-: Macro ending with ERROR will result in an ERROR.\r\n" +
        "\r\n" +
        "-" + nameof(MacroType.BugReport) + "-: Macro NOT ending with ERROR will result in an ERROR " +
        "(type should instead be changed to -" + nameof(MacroType.UnitTest) + "- because bug has then apparently been resolved).\r\n" +
        "\r\n" +
        "-" + nameof(MacroType.FeatureRequest) + "-: Macro NOT ending with ERROR will result in an ERROR " +
        "(type should instead be changed to -" + nameof(MacroType.UnitTest) + "- because feature has then apparently been implemented).\r\n" +
        "\r\n" +
        "TODO: Check of this affects lock functionality (search for 'lock' in code for this class).\r\n" +
        "\r\n" +
        "See also -" + nameof(RunMacroRP) + "-."
    )]
    public class RunMacroR : PRich, IRegHandler, IRegHandlerTGRI, IRegHandlerTGTE
    {

        private readonly static ConcurrentDictionary<IK, DateTime> _alreadyRunDataInits = new ConcurrentDictionary<IK, DateTime>();

        [ClassMember(Description =
        "The -" + nameof(ParentProcessId.Key) + "- part of -" + nameof(ParentProcessId) + "- as returned by " +
        "-" + nameof(TryGetRecipientId) + "-.\r\n" +
        "\r\n" +
        "Note: This is a bit dubious, caching identification returned by -" + nameof(TryGetRecipientId) + "- " +
        "and use that one in -" + nameof(TryGetTransientEvents) + "-.\r\n" +
        "But the consequence of any 'failure' would probably be limited to some confusing ids."
    )]
        IKLong? recipientIdKey = null;

        public bool TryGetRecipientId(out EntityTypeAndKey recipientId, out string errorResponse)
        {
            recipientIdKey = new IKLong(EventProcessor.GetCurrentSerialNumber());
            recipientId = new EntityTypeAndKey(typeof(MacroExecutionResult), recipientIdKey);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Executes tests as close to possible as how the individual -" + nameof(IRegHandler) + "-s would have been executed 'for real'"
        )]
        public bool TryGetTransientEvents(IP dataStorage, Event _event, out List<IRegHandler> transientEvents, out string errorResponse)
        {
            if (!dataStorage.TryGetP<IP>(IKType.FromType(typeof(MacroAttribute)), out var globalMacroCollection, out errorResponse))
            {
                transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "\r\nDetails: " + nameof(globalMacroCollection) + " not found";
                return false;
            }

            // TODO: This code turned a bit complex as more and more functionality was added.

            // ----------------------------------------------------------------------------------
            // STEP 1: Find either a single test to execute, or start with global list of all tests
            // ----------------------------------------------------------------------------------
            IP macroCollection;
            MacroType macroTypeToConsider;
            if (
                IP.TryGetPV<string>(RunMacroRP.MacroId, out var macroId)
            )
            {
                // Variant 1, a specific Macro is specified
                if (!globalMacroCollection.TryGetP<MacroAttribute>(macroId, out var macro, out errorResponse))
                {
                    transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return false;
                }
                macroCollection = new PRich();
                macroCollection.AddP(new IKIP(macroId, macro));
                macroTypeToConsider = macro.MacroType;
            }
            else
            {
                // Variant 2, MacroType is specified
                macroCollection = globalMacroCollection;

                if (
                  // TODO: Replace this with general QueryExpression for selection of which Macro to run
                  // Note now one MUST chose a MacroType, but MacroTimeHint is optional to chooose.
                  !IP.TryGetPV<MacroType>(RunMacroRP.MacroType, out macroTypeToConsider, out errorResponse)
                )
                {
                    transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return false;
                }
            }

            // ----------------------------------------------------------------------------------
            // STEP 2: Eliminate according to Macro type and to time hint (if given)
            // (and order by time hint)
            // ----------------------------------------------------------------------------------

            var macroTimeHint = IP.GetPV<MacroTimeHint>(RunMacroRP.MacroTimeHint, MacroTimeHint.__invalid);

            var macros = macroCollection.
                Where(ikip =>
                    ikip.P.GetPV<MacroType>(MacroAttributeP.MacroType) == macroTypeToConsider &&
                    (
                        macroTimeHint == MacroTimeHint.__invalid ||
                        ikip.P.GetPV<MacroTimeHint>(MacroAttributeP.MacroTimeHint) == macroTimeHint
                    )).
                OrderBy(ikip =>
                    // Sort according to time hint in order to have as little spool back as possible in event processor.

                    // NOTE: The whole purpose of MacroTimeHint is defeated if a collection of Macros are run
                    // NOTE: multiple times within the same application lifetime.
                    // NOTE: On the other hand, doing just that is an easy way to test the performance of the event engine.
                    (long)ikip.P.GetPV(MacroAttributeP.MacroTimeHint, MacroTimeHint.Now)
                ).
                Select(
                    ikip => ikip.P.As<MacroAttribute>()
                    // TODO: Delete commented out code (replaced by IP.As<T>)
                    //ikip => ikip.P as MacroAttribute ?? throw new InvalidObjectTypeException(ikip.P, typeof(MacroAttribute),
                    //    "Found object in " + nameof(macroCollection) + " (with key " + ikip.Key + ") " +
                    //    "not of type " + typeof(MacroAttribute) + " but " + ikip.P.GetType().ToString() + "\r\n"
                    //)
                ).
                ToList();

            // ----------------------------------------------------------------------------------
            // STEP 3: Add dependencies
            // ----------------------------------------------------------------------------------

            // TODO: Algorithm is O(n^2). TODO: Improve on this.

            var dependenciesToAdd = new Dictionary<IK, MacroAttribute>();
            foreach (var macro in macros)
            {
                if (macro.IP.TryGetPV<List<IKString>>(MacroAttributeP.Dependencies, out var dependencyKeys))
                {
                    foreach (var dependencyKey in dependencyKeys)
                    {
                        if (!macros.Any(u => dependencyKey.Equals(u.Key)) && !dependenciesToAdd.ContainsKey(dependencyKey))
                        {
                            // Dependency is not already included and has not in list of dependenciesToAdd
                            if (!globalMacroCollection.TryGetP<MacroAttribute>(dependencyKey, out var dependency, out errorResponse))
                            {
                                transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                                errorResponse = "\r\n" +
                                    "Details: -" + nameof(MacroAttribute) + "- " + macro.Key + " " +
                                    "has -" + nameof(MacroAttributeP.Dependencies) + "- " +
                                    "of which dependency " + dependencyKey + " was not found.\r\n";
                                return false;
                            }
                            dependenciesToAdd.Add(dependencyKey, dependency);
                        }
                    }
                }
            }
            macros.InsertRange(0, dependenciesToAdd.Values);

            // ----------------------------------------------------------------------------------
            // STEP 4: Exclude data init that has already been run (Within application lifetime)
            // ----------------------------------------------------------------------------------

            // Now how datainit that fails will never be repeated now within application lifetime
            macros = macros.Where(macro =>
                !
                (
                    MacroType.DataInit.Equals(macro.IP.GetPV<MacroType>(MacroAttributeP.MacroType, MacroType.__invalid)) &&
                    !_alreadyRunDataInits.TryAdd(macro.Key, UtilCore.DateTimeNow)
                )
            ).ToList();

            // ----------------------------------------------------------------------------------
            // STEP 5: Run the actual tests
            // ----------------------------------------------------------------------------------

            ARCQuery.Translations? t = null; /// Only for <see cref="MacroType.Translation"/>
            IP? dt = null; // TODO: This will probably be set to the same object as incoming parameter dataStorage, so we could set it here anway
            EventCollection? eventCollection = null;
            RegCollection? regCollection = null;

            var startTime = UtilCore.DateTimeNow;
            var countTotal = 0L;
            var countFailure = 0L;
            var countSuccess = 0L;


            foreach (var macro in macros)
            {
                macro.IP.SetPV(MacroAttributeP.SerialNumberAtStart, EventProcessor.GetCurrentSerialNumber());

                var macroType = macro.IP.GetPV<MacroType>(MacroAttributeP.MacroType);

                countTotal += 1;

                if (!macro.IP.TryGetPV<string>(MacroAttributeP.RegStream, out var regStream, out var errorResponse2))
                {
                    macro.IP.SetPV(MacroAttributeP.MacroStatus, MacroStatus.ERROR);
                    macro.IP.SetPV(MacroAttributeP.ErrorResponse, errorResponse2);
                    countFailure += 1;
                    continue;
                }

                var failureIsSuccess = (macroType) switch
                {
                    // TODO: Maybe make "failureIsSuccess" into an extension method on MacroType, could be useful in other cases too.
                    MacroType.DataInit => false,
                    MacroType.UnitTest => false,
                    MacroType.BugReport => true,
                    MacroType.FeatureRequest => true,
                    MacroType.DevDemo => false, // In general it does not matter for this
                    MacroType.TODO => false, // In general it does not matter for this
                    MacroType.Translation => false,
                    _ => throw new InvalidEnumException(macroType)
                };

                var setMacroStatus = new Action<MacroAttribute>(macro =>
                {
                    if (failureIsSuccess)
                    {
                        macro.IP.SetPV(MacroAttributeP.MacroStatus, MacroStatus.OK);
                        countSuccess += 1;
                    }
                    else
                    {
                        macro.IP.SetPV(MacroAttributeP.MacroStatus, MacroStatus.ERROR);
                        countFailure += 1;
                    }
                });

                macro.IP.SetPV(MacroAttributeP.MacroStatus, MacroStatus.NEW);
                macro.IP.SetP(MacroAttributeP.ErrorResponse, PValueEmpty.Singleton);

                var baseId = EventProcessor.GetCurrentSerialNumber() + 1;

                foreach (var s in regStream.Split("\r\n"))
                {
                    var strRegistration = s;
                    if (MacroAttribute.IsComment(strRegistration))
                    {
                        continue;
                    }
                    strRegistration = MacroAttribute.InsertIds(baseId, strRegistration);

                    var timeBefore = UtilCore.DateTimeNow;

                    if (macroType == MacroType.Translation)
                    {
                        // Special case for translations

                        // Abandoned approach, using PropertyStreamLine.TryParseAndStore
                        /// Syntax is the one output by <see cref="ARCQuery.TranslationSingle.TryGetPS"/>
                        /// (This syntax is longer than the one output by <see cref="ARCQuery.TranslationSingle.TryGetMacro"/>
                        /// and therefore this approach was not chosen)
                        //if (!PropertyStreamLine.TryParseAndStore(GlobalDataStorage.Storage, strRegistration, out errorResponse2))
                        //{
                        //    macro.IP.SetPV(MacroAttributeP.ErrorResponse,
                        //        "-" + nameof(PropertyStreamLine.TryParseAndStore) + "-: " +
                        //        "-" + nameof(Reg) + "-: '" + strRegistration + "'\r\nDetails:\r\n" + errorResponse2
                        //    );
                        //    setMacroStatus(macro);
                        //    break;
                        //}

                        // Chosen approach
                        /// Syntax is the one output by <see cref="ARCQuery.TranslationSingle.TryGetMacro"/>
                        /// This syntax is short (compared to <see cref="ARCQuery.TranslationSingle.TryGetPS"/>)
                        /// but makes for longer code here.
                        /// Since this code will be edited very seldom, while the translation data will be edited frequently, 
                        /// this is assumed to be a quite acceptable approach.
                        var keyValue = strRegistration.Split(" = ");
                        if (keyValue.Length != 2)
                        {
                            macro.IP.SetPV(MacroAttributeP.ErrorResponse,
                                "Invalid syntax, not 2 items \"{key} = {value}\" but " + keyValue.Length + " items."
                            );
                            setMacroStatus(macro);
                            break;
                        }
                        if (t is null)
                        {
                            if (!GlobalDataStorage.Storage.TryGetP<ARCQuery.Translations>(PSPrefix.t.ToString(), out var temp2, out errorResponse2))
                            {
                                macro.IP.SetPV(MacroAttributeP.ErrorResponse,
                                    "-" + nameof(ARCQuery.Translations) + "- not found (as 't'). Details:\r\n" + errorResponse2
                                );
                                setMacroStatus(macro);
                                break;
                            }
                            t = temp2;
                        }
                        if (!t.IP.TrySetP(
                            IKString.FromCache("_" + PropertyStreamLine.Decode(keyValue[0])),
                            new ARCQuery.TranslationSingle(PropertyStreamLine.Decode(keyValue[1]), isMissing: false),
                            out errorResponse2
                        ))
                        {
                            macro.IP.SetPV(MacroAttributeP.ErrorResponse,
                                "-" + nameof(IP.TrySetP) + "-: '" + strRegistration + "'\r\nDetails:\r\n" + errorResponse2
                            );
                            setMacroStatus(macro);
                            break;
                        }
                    }
                    else if (strRegistration.StartsWith(nameof(PSPrefix.dtr) + "/"))
                    {
                        /// Special case for <see cref="PSPrefix.dtr"/>

                        if (!IP.TryParseDtr(strRegistration, out var ikip, out errorResponse))
                        {
                            macro.IP.SetPV(MacroAttributeP.ErrorResponse,
                                "-" + nameof(IP.TryParseDtr) + "-: " +
                                "'" + strRegistration + "'\r\nDetails:\r\n" + errorResponse2
                            );
                            setMacroStatus(macro);
                            break;
                        }
                        /// TODO: Implement something similar to <see cref="PropertyStreamLine.TryParseAndStore"/>
                        /// TODO: for a one-liner here instead of all this code.
                        /// TODO: (check both Program.cs and RunMacroR.cs)
                        if (!dataStorage.TryGetP<IP>(IKType.FromType(ikip.P.GetType()), out var collection))
                        {
                            macro.IP.SetPV(MacroAttributeP.ErrorResponse,
                                "Collection not found for " + ikip.P.GetType().ToStringVeryShort() + " originating from '" + strRegistration + "'\r\n" +
                                "Details:\r\n" + errorResponse2
                            );
                            setMacroStatus(macro);
                            break;
                        }
                        // NOTE: AddP is maybe better as it will check for duplicates.
                        // TODO: Decide whether to allow duplicates or not here.
                        if (!collection.TrySetP(ikip, out errorResponse))
                        {
                            macro.IP.SetPV(MacroAttributeP.ErrorResponse,
                                "Unable to store " + ikip.ToString() + " originating from '" + s + "'.\r\n" +
                                "Details:\r\n" + errorResponse2
                            );
                            setMacroStatus(macro);
                            break;
                        }
                    }
                    else if (strRegistration.StartsWith(nameof(PSPrefix.dt) + "/"))
                    {
                        if (!PropertyStreamLine.TryParseAndStore(dataStorage, strRegistration[3..], out errorResponse))
                        {
                            macro.IP.SetPV(MacroAttributeP.ErrorResponse,
                                "-" + nameof(PropertyStreamLine.TryParseAndStore) + "-: " +
                                "'" + strRegistration + "'\r\nDetails:\r\n" + errorResponse2
                            );
                            setMacroStatus(macro);
                            break;
                        }
                    }
                    else
                    {
                        /// All others are assumed to be <see cref="IRegHandler"/>, use <see cref="RSController"/> for parsing and storing.

                        // Allow for a somewhat relaxed encoding
                        strRegistration = strRegistration.Replace(" ", "0x0020");

                        /// Note that for this method there is no need to split up into <see cref="RSController.TryParseRegistration"/> and
                        /// <see cref="RSController.TryStoreRegistrationAndStoreAndDistributeEvent"/>
                        /// because we are within a Write lock context anyway. But for other users of these methods it is an advantage
                        /// with the split in order to hold a Write lock for as short time as possible.
                        if (!RSController.TryParseRegistration(
                            GlobalDataStorage,

                            // Note: If we ever are to run unit tests multiple times, this operation can be cached somewhere
                            strRegistration.Split("/").Select(s => IKCoded.FromEncoded(s).Unencoded.ToString()).ToList(),

                            persistedRegTime: null,
                            out var reg, out var regHandler, out var regTimeA,
                            out errorResponse2, ref dt, ref eventCollection, ref regCollection, out var regHandlerCollection))
                        {
                            macro.IP.SetPV(MacroAttributeP.ErrorResponse,
                                "-" + nameof(RSController.TryParseRegistration) + "-: " +
                                "-" + nameof(Reg) + "-: '" + strRegistration + "'\r\nDetails:\r\n" + errorResponse2
                            );
                            setMacroStatus(macro);
                            break;
                        }
                        if (dt == null || eventCollection == null || regCollection == null || regHandlerCollection == null)
                        {
                            throw new NullReferenceException("dt == null || regCollection == null || eventCollection == null || regHandlerCollection == null");
                        }

                        /// Note hack in <see cref="EventProcessor.TryStoreAndDistributeEvent"/>
                        /// which has been made in order to accommodate this method.
                        /// But we still must ensure that we do not execute AS a Macro.

                        if (regHandler is RunMacroR)
                        {
                            macro.IP.SetPV(MacroAttributeP.ErrorResponse,
                                "It is illegal to attempt to execute -" + nameof(RunMacroR) + "- within a Macro.\r\n" +
                                "This could very easily lead to infinite recursion " +
                                "(if the same -" + nameof(MacroType) + "- is chosen for instance)."
                            );
                            setMacroStatus(macro);
                            break;
                        }

                        if (!RSController.TryStoreRegistrationAndStoreAndDistributeEvent(
                            GlobalDataStorage,
                            timeBefore, reg, regHandler, regTimeA, out var _event2,
                            out errorResponse2, eventCollection, regCollection, regHandlerCollection
                        ))
                        {
                            macro.IP.SetPV(MacroAttributeP.ErrorResponse,
                                "-" + nameof(RSController.TryStoreRegistrationAndStoreAndDistributeEvent) + "-: " +
                                "-" + nameof(Reg) + "-: '" + strRegistration + "'\r\nDetails:\r\n" + errorResponse2
                            );
                            setMacroStatus(macro);
                            break;
                        }
                    }
                }

                if (macro.IP.GetPV<MacroStatus>(MacroAttributeP.MacroStatus) == MacroStatus.NEW)
                {
                    // No errors / "success as failure" occurred, mark as OK
                    if (failureIsSuccess)
                    {
                        macro.IP.SetPV(MacroAttributeP.MacroStatus, MacroStatus.ERROR);
                        macro.IP.SetPV(MacroAttributeP.ErrorResponse,
                            "[No failure occurred, but expected one since 'failure is success' for " + macroType + "]");
                        countFailure += 1;
                    }
                    else
                    {
                        macro.IP.SetPV(MacroAttributeP.MacroStatus, MacroStatus.OK);
                        countSuccess += 1;
                    }
                }
            }

            if (recipientIdKey is null)
            {
                throw new NullReferenceException(nameof(recipientIdKey) + ": Should have been set at call to -" + nameof(TryGetRecipientId) + "-.");
            }

            var now = UtilCore.DateTimeNow;
            transientEvents = new List<IRegHandler>
            {
                SSPRT<MacroExecutionResultP>.Create(typeof(MacroExecutionResult), recipientIdKey, MacroExecutionResultP.StartTime, startTime),
                SSPRT<MacroExecutionResultP>.Create(typeof(MacroExecutionResult), recipientIdKey, MacroExecutionResultP.EndTime, now),
                SSPRT<MacroExecutionResultP>.Create(typeof(MacroExecutionResult), recipientIdKey, MacroExecutionResultP.ElapsedTime, now-startTime),
                SSPRT<MacroExecutionResultP>.Create(typeof(MacroExecutionResult), recipientIdKey, MacroExecutionResultP.MacroType, macroTypeToConsider),
                SSPRT<MacroExecutionResultP>.Create(typeof(MacroExecutionResult), recipientIdKey, MacroExecutionResultP.CountTotal, countTotal),
                SSPRT<MacroExecutionResultP>.Create(typeof(MacroExecutionResult), recipientIdKey, MacroExecutionResultP.CountSuccess, countSuccess),
                SSPRT<MacroExecutionResultP>.Create(typeof(MacroExecutionResult), recipientIdKey, MacroExecutionResultP.CountFailure, countFailure)
            };

            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;

        }

        // Abandoned approach
        //finally
        //{
        //    REx.Dec();
        //}

        [ClassMember(Description = "TODO: Find some better initialization here.")]
        public static ARCAPI.DataStorage GlobalDataStorage { get; set; } = null!;
    }

    public class RunMacroRCollection : PCollection { }

    [Enum(
        Description =
            "Describes class -" + nameof(RunMacroR) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum RunMacroRP
    {
        __invalid,

        [PKType(Description =
            "Leave out this if you instead want to run multiple Macros specified by -" + nameof(MacroType) + "- and -" + nameof(MacroTimeHint) + "-"
        )]
        MacroId,

        [PKType(Type = typeof(MacroType))]
        MacroType,

        [PKType(Type = typeof(MacroTimeHint))]
        MacroTimeHint
    }

    [Class(Description =
        "Contains result from -" + nameof(RunMacroR) + "-.\r\n" +
        "\r\n" +
        "See also -" + nameof(MacroExecutionResultP) + "-.\r\n"

    )]
    public class MacroExecutionResult : PRich
    {

    }

    public class MacroExecutionResultCollection : PCollectionES
    {
    }

    [Enum(
        Description = "Describes class -" + nameof(MacroExecutionResult) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum MacroExecutionResultP
    {
        __invalid,
        [PKType(Type = typeof(DateTime))]
        StartTime,
        [PKType(Type = typeof(DateTime))]
        EndTime,
        [PKType(Type = typeof(TimeSpan))]
        ElapsedTime,
        [PKType(Type = typeof(MacroType))]
        MacroType,
        [PKType(Type = typeof(long))]
        CountTotal,
        [PKType(Type = typeof(long))]
        CountFailure,
        [PKType(Type = typeof(long))]
        CountSuccess
    }
}
