﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCEvent
{
    [Enum(
        AREnumType = AREnumType.OrdinaryEnum
    )]
    public enum MacroType
    {
        __invalid,

        [EnumMember(Description =
            "Marker for a Macro (often a unit test) that should be implemented.\r\n" +
            "\r\n" +
            "Use this when you have just written the code and know all the intricacies that should be tested " +
            "but do not want to write the actual tests yet.\r\n" +
            "Since the details are often forgotten after a while it is a good idea to write them down at once, " +
            "hence -" + nameof(TODO) +"- here."
        )]
        TODO,

        [EnumMember(Description =
            "Generation of selected sample data for testing.\r\n" +
            "\r\n" +
            "The corresponding Macro sets up data points for the purpose of either:\r\n" +
            "\r\n" +
            "1) Being example data to use in order to test the application.\r\n" +
            "\r\n" +
            "or\r\n" +
            "\r\n" +
            "2) Set up a customer specific configuration.\r\n" +
            "\r\n" +
            "For 1), having good sample data to test an application with is often an issue, sometimes leading to using production databases " +
            "for testing which is not always an optimal approach.\r\n" +
            "\r\n" +
            "-" + nameof(DataInit) + "- on the other hand is envisaged as a more specific human friendly way " +
            "of introducing specially created sample data in the application.\r\n" +
            "\r\n" +
            "Note than in general ordinary -" + nameof(UnitTest) + "-s will potentially also generate some useful data to experiment further with " +
            "(running all unit test will populate the database (or litter it according to viewpoint) with a huge amount of data which may " +
            "(or may not) be useful for further experiements).\r\n" +
            "\r\n" +
            "-" + nameof(DataInit) + "- test will probably also function as ordinary unit tests because they should not fail when executed."
        )]
        DataInit,

        [EnumMember(Description =
            "The corresponding Macro is an ordinary unit test.\r\n" +
            "\r\n" +
            "Note that on could also for unit tests actually being integration tests because of -" + nameof(ARCEvent) +"-s " +
            "compact all-in-one architecture. The test can be run in any environment for instance because they are " +
            "always available.\r\n"
        )]
        UnitTest,

        [EnumMember(Description =
            "Signifies that the corresponding Macro describes a known bug " +
            "(that is, it is known that the test will end up in some error state)\r\n" +
            "\r\n" +
            "-" + nameof(BugReport) + "- are 'supposed' to fail (end up with an -" + nameof(MacroAttributeP.ErrorResponse) + "-).\r\n" +
            "In other words, it is OK to commit code to a source control system even though " +
            "-" + nameof(MacroAttributeP.ErrorResponse) + "- is set for these tasts.\r\n" +
            "Correspondingly, a -" + nameof(BugReport) + " test which does not end up in some error state, should be taken " +
            "as a sign not to commit the current code " +
            "(if the bug has actually been fixed -" + nameof(BugReport) + "- can just be changed into -" + nameof(UnitTest) + "-).\r\n" +
            "\r\n" +
            "This is useful for submitting bug reports. The bug is described as the sequence of -" + nameof(Reg) + "-s that leads up " +
            "to the bug. After the bug issue has been resolved, " +
            nameof(BugReport) + " - is changed into - " + nameof(UnitTest) +
            "turning the bug report into an ordinary unit test guarding against future regressions.\r\n" +
            "\r\n" +
            "This approach saves the developer from creating a new unit test after fixing a reported bug, and gives the reporter a guarantee " +
            "that the bug is fixed 'forever' since there is now a unit test guarding against its reappearance.\r\n" +
            "\r\n" +
            "This approach also makes it possible to 'ignore' existing unit test by marking them as 'BugReport'.\r\n" +
            "The is useful if despite of regressions, it is still desireable to put the application into the production environment.\r\n" +
            "The fact that some unit tests are 'ignored' is then clearly stated in the (easily generated) documentation for known bugs " +
            "(simple query like \"dt/MacroAttribute/WHERE MacroType = 'BugReport'\")."
        )]
        BugReport,

        [EnumMember(Description =
            "Note: A feature request can be ANY -" + nameof(ARConcepts.RegStream) + "-, syntactically correct or not.\r\n" +
            "It does not matter on what level an ERROR occurs as observed by -" + nameof(RunMacroR) + "-, " +
            "the acceptance criteria (that the feature has been implementd) is simply that no ERROR occurs.\r\n" +
            "\r\n" +
            "As with -" + nameof(BugReport) + "-, when then feature has been implemented, -" + nameof(FeatureRequest) + "- " +
            "is changed into -" + nameof(UnitTest) + "-, guaranteeing for the future that the feature will remain permanent " +
            "(guarding against regressions).\r\n" +
            "\r\n" +
            "This concept is probably somewhat equivalent to Test Driven Development (TDD), although the author has " +
            "(as of December 2021) not studied it extensively.\r\n" +
            "\r\n" +
            "TODO: Implement some more support for this. A status field like REJECTED for instance is probably needed."
        )]
        FeatureRequest,

        [EnumMember(Description =
            "Demonstration of features to be shared between developers.\r\n" +
            "\r\n" +
            "Could for instance contain common gotchas that all developers should be aware of.\r\n" +
            "\r\n" +
            "These tests may either succeed or fail, they are made for educational purposes without any specific " +
            "requirements to how they are interpreted."
        )]
        DevDemo,

        [EnumMember(Description =
            "Translations.\r\n" +
            "\r\n" +
            "These are implicit understood by -" + nameof(RunMacroR) +"- to be stored through -" + nameof(PropertyStreamLine.TryParseAndStore) + "- " +
            "(instead of -" + nameof(RSController) +"-).\r\n" +
            "\r\n" +
            "See -" + nameof(ARCQuery.Translations) + "-."
        )]
        Translation,

        Ignore
    }
}
