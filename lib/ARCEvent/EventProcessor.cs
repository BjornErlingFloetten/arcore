﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using ARCCore;
using System.Linq;

namespace ARCEvent
{

    [Class(Description =
        "TODO: Add some statistics, like  _lastEventsSeen.Count and last time clean up was done of the list.\r\n" +
        "TODO: Maybe create some history for such operations too.\r\n" +
        "TOOD: \r\n" +
        "TODO: Add total cost at startup also.\r\n" +
        "\r\n" +
        "See also -" + nameof(EventProcessorP) + "-."
    // See below more information, search for "TODO: Add a "cost"-property"
    )]
    [Macro(
        Name = "BUG 2021-12-22: Recalculation, wrong snapshot is used.",
        MacroType = MacroType.BugReport,
        MacroTimeHint = MacroTimeHint.Past,
        Description =
            "This is a simpler explanation of BUG 2021-12-18:\r\n" +
            "\r\n" +
            "Event A is -" + nameof(IncreaseValueByOneR) + "- for some entity, calculating value to 1\r\n" +
            "Event B is inserted in Event stream BEFORE Event A.\r\n" +
            "Event A must therefore be recalculated, but when it requests the entity snapshot, the snapshot already (wrongly) contains the value 1, " +
            "and therefore Event A wrongly calculates 2.\r\n" +
            "Only AFTER Event A stores the new value will the old snapshot be deleted.\r\n" +
            "\r\n",
        RegStream = @"
DO/now/IncreaseValueByOneR/TCGreen {id}/First
DO/now/ProvokeSnapshotCreationR///TCRed {id}/TCGreen {id}
// Some random event at an earlier point in the timeline, triggering recalculation
DO/Yesterday/CNER/TCRed {id}
DO/now/AssertR/Equal///TCGreen {id}/First/1
",
        QuerySuggestions = @"
ES/dt/now//TCRed/{id}
"
    )]
    [Macro(
        Name = "BUG 2021-12-18: Transient event disappear, snapshot not deleted.",
        MacroType = MacroType.BugReport,
        Description =
            "It is not sufficient in -" + nameof(MarkTransientEventsAsReplacedRecursively) + "-\r\n" +
            "to mark with -" + nameof(Event.ReplacedRegTime) + "-, we must also inform recipient that it has to\r\n" +
            "delete any snapshots at or newer than event time, because in case the transient event is not regenerated\r\n" +
            "again, the snapshots may be invalid.\r\n" +
            "This functionality may be put in either -" + nameof(EventProcessor) + "- or\r\n" +
            "-" + nameof(PCollectionES) + "- may do some checks when asked for a snapshot.\r\n" +
            "\r\n" +
            "For the time being the bug is left as-is for demonstration purposes.\r\n" +
            "\r\n" +
            "Update 2021-12-22: See BUG 2021-12-22 which describes this in an even simpler manner.\r\n",
        RegStream = @"
DO/now/ConditionalCreateTransientEventR/True/TCRed {id}/TCGreen {id}/First/ThisValueShouldDisappearFromFinalSnapshot
DO/now/ProvokeSnapshotCreationR///TCRed {id}/TCGreen {id}
DO/{id}/ConditionalCreateTransientEventR/False/TCRed {id}/TCGreen {id}
DO/now/AssertR/NotExist///TCGreen {id}/First
",
        QuerySuggestions = @"
ES/dt/now//TCGreen/{id+1}
"
    )]
    public class EventProcessor : PConcurrent
    {
        private static long _currentSerialNumber = 0;
        /// TODO: Move somewhere else. Serial number is that of Registration. Move to <see cref="RSController"/> for instance
        public static long GetNextSerialNumber() =>
            System.Threading.Interlocked.Increment(ref _currentSerialNumber);
        public static long GetCurrentSerialNumber() => _currentSerialNumber;

        [ClassMember(Description =
            "Should be called at end of application startup, that is, after all persisted registrations have been deserialized from disk " +
            "and the highest current serial number has been ascertained.\r\n"
        )]
        public static void SetSerialNumber(long highestPersistedSerialNumber)
        {
            UtilCore.AssertCurrentlyStartingUp();
            _currentSerialNumber = highestPersistedSerialNumber;
        }

        private readonly IP _dataStorage;

        // private static Dictionary<EventTime, Event> _allEventsSeen = new Dictionary<EventTime, Event>();

        [ClassMember(Description =
            "Collection of earlier events seen.\r\n" +
            "\r\n" +
            "Necessary in order to regenerate all transient events when events arrive \"out-of-order\" " +
            "\r\n" +
            "The size of this list decides how far back in time it is possible to insert events \"out-of-order\".\r\n" +
            "\r\n" +
            "The list is always kept in sorted order (sorted by increasing -" + nameof(EventTime) + "-)."
        )]
        private static List<Event> _lastEventsSeen = new List<Event>();

        [ClassMember(Description =
            "This exposure is useful for high-performance iterating of events in sorted order, but its value is " +
            "somewhat constricted because number of elements in the list is limited (currently hardcoded to between 100 and 150 thousand elements)." +
            "\r\n" +
            "Note how events are usually also stored under the ordinary 'dataStorage' mechanism by -" + nameof(RSController) + "-.\r\n"
        )]
        public static List<Event> LastEventsSeen => _lastEventsSeen;

        [ClassMember(Description = "" +
            "The first event seen, either the first arriving at application startup (when deserializing from disk)\r\n" +
            "or later event arriving before this event.\r\n" +
            "\r\n" +
            "Used to interpret action to take when spooling back past start of -" + nameof(_lastEventsSeen) + "-."
        )]
        private static Event? _firstEventEverSeen = null;

        // private EventTime _lastEventTime = EventTime.MinValue;

        // private readonly IP _eventCollection;

        [ClassMember(Description =
            "The parameter dataStorage would probably be equivalent to the -" + nameof(PSPrefix.dt) + "- part of some " +
            "global application lifetime storage."
        )]
        public EventProcessor(IP dataStorage)
        {
            _dataStorage = dataStorage;
            // _eventCollection = _dataStorage.GetP(IKType.FromType(typeof(Event)));
        }

        [ClassMember(Description =
            "Creates an -" + nameof(Event) + "- from the given -" + nameof(Reg) + "- object.\r\n" +
            "Stores it with the relevant collection.\r\n" +
            "\r\n" +
            "Returns TRUE if generation and storing of the resulting -" + nameof(Event) + "- succeeded.\r\n" +
            "If FALSE is returned, the out-parameter _event MAY or MAY NOT be set, but if set it will not be " +
            "'active', that is, it will never be taken into consideration / processed.\r\n" +
            "\r\n" +
            "In other words, if TRUE is returned the actual -" + nameof(IRegHandlerTGTE.TryGetTransientEvents) + "- operation may still have " +
            "failed, and you have to look up the -" + nameof(Event.EventStatus) + "- and -" + nameof(Event.ErrorResponse) + "- " +
            "for information about that."
        )]
        public bool TryStoreAndDistributeEvent(RegId regId, Reg reg, out Event _event, out string errorResponse)
        {
            // TODO: Add overload to TryGetP with generic type parameter for type of Property desired.
            if (!reg.IP.TryGetP<IRegHandler>(RegP.RH, out var regHandler, out errorResponse))
            {
                _event = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }

            //if (!(temp is IRegHandler regHandler))
            //{
            //    // We could just as well have thrown an exception here.
            //    errorResponse = "Invalid type for -" + nameof(RegP.RH) + "-: " + temp.GetType().ToString();
            //    _event = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            //    return false;
            //}

            EntityTypeAndKey? recipientId = null;
            if (regHandler is EmptyRegHandlerR)
            {
                // Wait with finding recipient id

                /// This is typically <see cref="RegVerb.UNDO"/> replacing an earlier event.
                /// And since EmptyRegHandlerR does not give RecipientId we have to wait instead
                /// until we have found the reg handler that it replaces, and use 
                /// the recipient id of that one.

                // Note that one could believe that EmptyRegHandlerR is not necessary to store at all, 
                // because it contains no data.
                // But it has to be sent to the relevant PCollectionES in order for
                // any snapshots based on the old event to be discarded.
            }
            else if (!IRegHandler.TryGetRecipientId(regHandler, out recipientId, out errorResponse))
            {
                errorResponse = "-" + nameof(IRegHandler.TryGetRecipientId) + "-: " + errorResponse;
                _event = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }


            var eventStatus = regHandler.DefaultEventStatus;
            var regVerb = reg.IP.GetPV<RegVerb>(RegP.RegVerb);
            if (regVerb == RegVerb.BLOCK)
            {
                eventStatus = EventStatus.BLOCKED_USER;
            }

            // TODO: Create a ToRootEvent method in IRegHandler.
            _event = new Event(
                regId,
                reg.IP.GetPV<RegTime>(RegP.RegTime),
                reg.IP.GetPV<EventTime>(RegP.EventTime),
                regVerb,
                regHandler,
                eventStatus
            );

            if (!TryFindInsertPosition(_event, out var insertPosition, out errorResponse))
            {
                return false;
            };

            /// Note that insertPosition == 0 is only allowed by <see cref="TryFindInsertPosition"/>
            /// if new event is at "beginning-of-time", so check below does not risk missing something
            if (insertPosition > 0 && _lastEventsSeen[insertPosition - 1].EventTime == _event.EventTime)
            {
                /// This is the "same" event, just with a new <see cref="RegVerb"/> or similar.
                var oldEvent = _lastEventsSeen[insertPosition - 1];

                if (recipientId == null)
                {
                    // regHandler is EmptyRegHandlerR, and we therefore had to wait until now to use recipient id of old reg handler.
                    if (!IRegHandler.TryGetRecipientId(oldEvent.RegHandler, out recipientId, out errorResponse))
                    {
                        errorResponse = "-" + nameof(IRegHandler.TryGetRecipientId) + "- (for old event, being replaced now): " + errorResponse;
                        _event = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        return false;
                    }

                }
                if (oldEvent.RegHandler.GetType().Equals(typeof(EmptyRegHandlerR)) || regHandler.GetType().Equals(typeof(EmptyRegHandlerR)))
                {
                    /// This would typically be <see cref="RegVerb.DO"/> replaced with <see cref="RegVerb.UNDO"/>, or the other way around.
                }
                else if (!oldEvent.RegHandler.GetType().Equals(_event.RegHandler.GetType()))
                {
                    // Check that object types match
                    errorResponse =
                        "Cannot replace event with another event of another type.\r\n" +
                        "Old event -" + oldEvent.RegHandler.GetType().ToStringShort() + "- " +
                        "can not be replaced with -" + _event.RegHandler.GetType().ToStringShort() + "-.\r\n" +
                        "\r\n" +
                        "Old registration handler: " + oldEvent.RegHandler.Serialize() + "\r\n" +
                        "New registration handler: " + _event.RegHandler.Serialize() + "\r\n" +
                        "\r\n" +
                        "Possible resolution: -" + nameof(RegVerb.UNDO) + "- old event, and create new event.\r\n" +
                        "\r\n" +
                        "TODO: This is not thought through, but it is probably best to be restrictive initially\r\n" +
                        "TODO: and instead loosen this up later if any need for this should emerge.\r\n";
                    return false;
                };
                // TODO: There is some overlap here between responsibilities of EventProcessor and PCollectionES 
                // TODO: when that one generates snapshots.
                // TODO: If could for instance be nice to not have to repeat the Registration just for a UNDO for instance,
                // TODO: as we are forced to do now.

                // The old event is now irrelevant since it is replaced with a new one.
                oldEvent.ReplacedRegTime = _event.RegTime;
                MarkTransientEventsAsReplacedRecursively(_event.RegTime, oldEvent);
            }

            if (recipientId == null)
            {
                throw new NullReferenceException(
                    "recipientId. TODO: Probably replace this exception with errorResponse and return false.\r\n" +
                    "Should have been found from old event which is being replaced by this event."
                );
            }
            else
            {
                if (!_dataStorage.TryGetCollection(recipientId.EntityType, out var collection, out errorResponse))
                {
                    errorResponse = nameof(PCollectionESExtension.TryGetCollection) + " (for " + recipientId.EntityType.ToStringShort() + "): " + errorResponse;
                    _event = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return false;
                }

                if (!collection.TryStoreEvent(recipientId.Key, _event, out errorResponse))
                {
                    return false;
                }
            }

            // Insert into list at appropriate position
            _lastEventsSeen.Insert(insertPosition, _event);

            if (_event.RegHandler is RunMacroR)
            {
                return TryGenerateAndDistributeTransientEventsRecursively(_event, _event.RegTime, out errorResponse);

                /// Skip the rest. 
                /// 
                /// If we attempt to execute the rest, all kind of problems occur because
                /// new events will be generated (this code will be reentered) as a consequence 
                /// of calling <see cref="RunMacroR.TryGetTransientEvents"/> which again will call here.
                ///
                /// Note that we can skip the rest because we simply assume that <see cref="RunMacroR"/> in itself
                /// does not "change the future".
                /// 
                /// Note that it is tempting to also skip the insertion above, but that would mess up 
                /// calculating / use of <see cref="_firstEventEverSeen"/>
            }


            // Delete ALL transient events that have been generated after the event-time
            // (note how the new event is included here, it makes for easier to follow code)
            //
            // The reason for this is that the transient events may contain setting of property values
            // that will change due to earlier changes. Even the generation of transient events themselves
            // may change now.
            //
            // Note that this becomes a hugely expensive process if the difference in time now is big
            // (or rather, if many events must be reprocessed)
            // Especially if these events have a need for up-to-date snapshots in order to "decide" what to do.
            for (
                var i = insertPosition; // (note how the new event is included here (we do not use 'insertPosition+1'), makes loop similar to loop below
                i < _lastEventsSeen.Count;
                i++
            )
            {

                var e = _lastEventsSeen[i];

                if (e.RegHandler is RunMacroR)
                {
                    continue; // Skip, see rationale above
                }

                MarkTransientEventsAsReplacedRecursively(_event.RegTime, e);
                // Note: Do not fall for the temptation of combining this for-loop with the one below
                // it would lead to deletion of transient events just generated.
            }

            /// HACK: Hack in order to accommodate <see cref="RunMacroR"/>, whose 
            /// HACK: <see cref="RunMacroR.TryGetTransientEvents"/> will call this method again
            /// HACK: leading to "i" iterating PAST the current Count, as <see cref="_lastEventsSeen"/> 
            /// HACK: gets new items inserted.
            /// HACK: Therefore, take a copy of the value
            var _lastEventsSeenCount = _lastEventsSeen.Count;

            string? errorResponseToReturn = null;
            for (
                var i = insertPosition; // (now we MUST include the new event being stored)
                i < _lastEventsSeenCount;
                i++
            )
            {
                // Wrong approach, do not change the output parameter:
                // _event = _lastEventsSeen[i];
                var e = _lastEventsSeen[i];

                if (!(e.ReplacedRegTime is null))
                {
                    continue;
                }

                if (e.RegHandler is RunMacroR)
                {
                    continue; // Skip, see rationale above
                }

                if (!TryGenerateAndDistributeTransientEventsRecursively(e, _event.RegTime, out errorResponse))
                {
                    // TODO: Delete commented out TODO:
                    ////// TODO:
                    ////// TODO: Note that this is a bit odd.
                    ////// TODO: The event gets an ERROR status, but metod below returns with result TRUE anyway
                    ////// TODO: This will result in confusing results when running Macros for instance, 
                    ////// TODO: the Macro itself will return as OK, but events will be marked with ERROR.
                    ////// TODO: We could at least have returned ERROR if "i" now points at "insertPosition".

                    if (i == insertPosition)
                    {
                        // Let method return FALSE below
                        // For the event being stored now it would be correct to let method return FALSE
                        // (for failure with other events (that are now recalculating) returning TRUE is probably best, 
                        // because it is "not this event's fault" that later events fails.
                        // The reason for returning FALSE now is that for instance unit tests would otherwise have reported
                        // OK, even though a failure occurred (or actually any other Registration too).
                        errorResponseToReturn = errorResponse;
                    }
                    e.EventStatus = EventStatus.ERROR;
                    e.ErrorResponse = errorResponse;
                }
                else
                {
                    e.EventStatus = EventStatus.OK;
                    e.ErrorResponse = null; // Set to null because maybe it was something different before.            
                }
            }

            if (_lastEventsSeen.Count > 150000)
            {                             
                // Clean up in order for List not to grow too big.
                // TODO: Introduce a configuration value for this. 
                _lastEventsSeen = _lastEventsSeen.Skip(100000).ToList();
            }

            if (errorResponseToReturn != null)
            {
                // Should we not set 
                // firstEvent.EventStatus = EventStatus.ERROR;
                // here?
                // NO, probably not
                errorResponse = errorResponseToReturn;


                // One possible approach
                // Negative consequence: Bug reports will not execute correctly, because they expect an error response to occur...
                // return true; // Return TRUE anyway, in order for event to be stored.
                // error response will most probably be ignored now?

                // Chosen approach
                return false;
            }

            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        private bool TryFindInsertPosition(Event _event, out int insertPosition, out string errorResponse)
        {
            if (_firstEventEverSeen == null)
            {
                _firstEventEverSeen = _event;
            }

            if (_lastEventsSeen.Count > 0 && _event.EventTime < _lastEventsSeen[^1].EventTime)
            {
                // We must spool back

                var i = _lastEventsSeen.Count - 1;
                while (i >= 0 && _lastEventsSeen[i].EventTime > _event.EventTime)
                {
                    i--;
                }
                i++;
                if (i == 0)
                {
                    if (
                        ReferenceEquals(_lastEventsSeen[i], _firstEventEverSeen)
                    )
                    {
                        // OK because new event is at 'beginning-of-time', and we have all events in memory
                        // (the first event in List is equal to the first we ever saw)
                        // 
                        // This is useful for debugging, going back to the 'beginning-of-time', 
                        // in a production system this issue is probably little relevant (we will not have all events in memory here anyway)
                        _firstEventEverSeen = _event;
                    }
                    else
                    {
                        insertPosition = default; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        errorResponse =
                            "Unable to spool back to eventTime " + _event.EventTime + "\r\n" +
                            "because have only cached last events back to " + _lastEventsSeen[0].EventTime + ".\r\n" +
                            "Note: If some sorting optimization is running in the background this problem may sort itself out " +
                            "automatically in the future at application restart, as the problematic event " +
                            "slowly bubbles further back to an earlier position in the sorted persisted -" + nameof(ARConcepts.RegStream) + "- .\r\n" +
                            "\r\n";
                        return false;
                    }
                }
                insertPosition = i;
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
            else
            {
                // Add to end of list
                insertPosition = _lastEventsSeen.Count;
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
        }

        [ClassMember(Description =
            "See -" + nameof(Event.ReplacedRegTime) + "- for documentation.\r\n" +
            "\r\n" +
            "Note that the -" + nameof(Event.TransientEvents) + "- (the -" + nameof(ARConcepts.TransientEventTree) + "-) are not deleted."
        )]
        private void MarkTransientEventsAsReplacedRecursively(RegTime regTime, Event _event)
        {
            try
            {
                REx.Inc();
                if (_event.TransientEvents != null)
                {
                    foreach (var e in _event.TransientEvents)
                    {
                        /// Note how we do not bother with finding where the event was stored
                        /// Marking as deleted is enough and makes for easier debugging
                        /// <see cref="PCollectionES"/> will skip deleted events anyway when constructing snapshot
                        e.ReplacedRegTime = regTime;
                        MarkTransientEventsAsReplacedRecursively(regTime, e);
                    }
                }
            }
            finally
            {
                REx.Dec();
            }
        }

        public bool TryGenerateAndDistributeTransientEventsRecursively(Event firstEvent, RegTime regTime, out string errorResponse)
        {
            try
            {
                REx.Inc();

                /// Misguided, it is quite natural to be called multiple times and the existing transient event tree
                /// does not need to be deleted by <see cref="MarkTransientEventsAsReplacedRecursively"/>                
                //if (_firstEvent.TransientEvents != null)
                //{
                //    throw new NotNullReferenceException(
                //        nameof(Event.TransientEvents) + ": " + _firstEvent.ToString() + ".\r\n" +
                //        "\r\n"
                //    );
                //}
                /// Instead we do like this, in case call to <see cref="IRegHandler.TryGetTransientEvents"/> fails
                /// (because that would leave us with a strange state, an ERROR condition at root-event level but with an old <see cref="ARConcepts.TransientEventTree"/>).
                firstEvent.TransientEvents = null;

                firstEvent.LastCallToTryGenerateAndDistributeTransientEventsRecursively = UtilCore.DateTimeNow;

                if (firstEvent.EventStatus == EventStatus.WAIT_FOR_EXEC)
                {
                    /// Event has sub processes and must wait for <see cref="EXEcuteR"/> to "execute" it (to call <see cref="IRegHandler.TryGetTransientEvents"/>)
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
                }

                if (!(firstEvent.RegHandler is IRegHandlerTGTE tgte))
                {
                    // registration handler does not generate transient events at all
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
                }
                // Note change 8 Feb 2022, returning List<IRegHandler> instead of List<Event> from TryGetTransientEvents
                if (!tgte.TryGetTransientEvents(_dataStorage, firstEvent, out var transientRegHandlers, out errorResponse))
                {
                    errorResponse = "-" + tgte.GetType().ToStringShort() + "-: -" + nameof(IRegHandlerTGTE.TryGetTransientEvents) + "-: " + errorResponse;
                    return false;
                }
                var transientEvents = transientRegHandlers.Select(a => firstEvent.CreateTransientEvent(a, regTime)).ToList();
                firstEvent.TransientEvents = transientEvents;

                // Note: TRUE will be returned regardless of what happens below now
                // (regardless of errors occurring for transient events, they will get their own error status instead)
                // In other words, an ERROR state within a transient event does not propagate up to the root event level.
                foreach (var _event in transientEvents)
                {
                    if (!IRegHandler.TryGetRecipientId(_event.RegHandler, out var recipient, out errorResponse))
                    {
                        _event.EventStatus = EventStatus.ERROR;
                        _event.ErrorResponse = errorResponse;
                        continue;
                    }
                    if (
                        !_dataStorage.TryGetCollection(recipient.EntityType, out var collection, out errorResponse) ||
                        !collection.TryStoreEvent(recipient.Key, _event, out errorResponse)
                    )
                    {
                        _event.EventStatus = EventStatus.ERROR;
                        _event.ErrorResponse = errorResponse;
                        continue;
                    }
                    if (!TryGenerateAndDistributeTransientEventsRecursively(_event, regTime, out errorResponse))
                    {
                        _event.EventStatus = EventStatus.ERROR;
                        _event.ErrorResponse = errorResponse;
                        continue;
                    }
                    _event.EventStatus = EventStatus.OK;
                    _event.ErrorResponse = null;
                }
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
            finally
            {
                REx.Dec();
            }
        }

        public class EventProcessorException : ApplicationException
        {
            public EventProcessorException(string message) : base(message) { }
            public EventProcessorException(string message, Exception inner) : base(message, inner) { }
        }
    }

    [Enum(
        AREnumType = AREnumType.PropertyKeyEnum,
        Description = "Describes class -" + nameof(EventProcessor) + "-."
    )]
    public enum EventProcessorP
    {
        __invalid,
        [PKType(
            Description =
                "TODO: Not implemented as of Dec 2021\r\n" +
                "\r\n" +
                "The time point in the -" + nameof(ARConcepts.EventStream) + "- around which most Registrations are expected to take place.\r\n" +
                "\r\n" +
                "Used for optimizing storing of events, avoiding O(n) inserts in List.\r\n" +
                "May also be used for avoiding regeneration of transient events in the future or similar (TODO: Not implemented)\r\n" +
                "\r\n" +
                "If not set then 'now' is assumed.\r\n",
            Type = typeof(DateTime)
        )]
        OptimizationTime,

        [PKType(
            Description =
                "Number of events currently held in memory.\r\n" +
                "\r\n" +
                "The number will decide how far back in time it is possible to insert new -" + nameof(Reg) + "-.\r\n" +
                "\r\n" +
                "TODO: Not in use as of Dec 2021 (hardcoded instead)",
            Type = typeof(long)
        )]
        LastEventsSeenCount,
    }
}
