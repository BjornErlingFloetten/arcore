﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using ARCAPI;
using ARCCore;
using ARCQuery;

namespace ARCEvent {

    [Class(Description =
        "TODO: This class is probably not needed. Move -" + nameof(TryParseDateTime) + "- into -" + nameof(UtilEvent) + "- instead."
    )]
    public class BaseEventController : BaseController {

        [ClassMember(Description =
            "Attempts to construct a DateTime object in a user friendly manner.\r\n" +
            "\r\n" +
            "Accepts 'bot' (beginning of time), 'now' and 'eot' (end of time) as values.\r\n" +
            "\r\n" +
            "Does also utilize -" + nameof(ValueComparerDateTime) + "- in order to support expressions like Today, Tomorrow, TwoDaysAgo and so on.\r\n" +
            "\r\n" +
            "TODO: Make something simpler than -" + nameof(ValueComparerDateTime) + "- which can give a point in time only."
        )]
        public static bool TryParseDateTime(DateTime now, string strTime, out DateTime dateTime, out string errorResponse) {

            if ("bot".Equals(strTime)) {
                dateTime = DateTime.MinValue;
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
            else if ("now".Equals(strTime))
            {
                // TODO: Introduce this as parameter,                 
                dateTime = now;
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
            else if ("eot".Equals(strTime)) {
                dateTime = DateTime.MaxValue;
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            } else if (UtilCore.DateTimeTryParse(strTime, out dateTime)) {
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            } else if (ValueComparerDateTime.TryParse(strTime, out var vc, out errorResponse)) {
                // TODO: MAYBE make something simpler than ValueComparerDateTime which can give a point in time only
                // TODO: The class is a bit overkill for what we need here
                // TODO: On the other hand, it works quite well as it is now...
                dateTime = vc.StartPeriod;
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            } else {
                dateTime = default; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse =
                    "Invalid time given (" + strTime + "). Use \"now\" for instance.\r\n" +
                    "Details (in case you tried to specify a -" + nameof(ValueComparerDateTime) + "-): " + errorResponse;
                return false;
            }
        }
    }
}
