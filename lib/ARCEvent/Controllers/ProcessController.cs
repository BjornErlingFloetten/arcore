﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using ARCCore;
using ARCDoc;
using ARCAPI;
using System.Linq;

namespace ARCEvent
{
    [Class(Description =
        "For -" + nameof(ResponseFormat.HTML) + "-: Attempts to produce a workable HTML interface for editing processes.\r\n" +
        "\r\n" +
        "A process is either a -" + nameof(Process) + "- based on some -" + nameof(Schema) + "-, " +
        "or a C# coded -" + nameof(IRegHandler) + "-.\r\n" +
        "A process may have -" + nameof(SubProcess) + "-es again, that is, it is inherently a hierarchical structure as seen by " +
        "this class.\r\n" +
        "A process may be edited at any level in this hierarchical structure.\r\n" +
        "\r\n" +
        "TODO: For -" + nameof(ResponseFormat.JSON) + "-: Delivers necessary information to client in order for client to generate a " +
        "user interface.\r\n" +
        "\r\n" +
        "The code herewithin must be considered a sketch of what is possible. It should probably not be used in a production system, " +
        "but only be used for testing and debugging.\r\n" +
        "\r\n" +
        "TODO: Before adding to this code, clean up use of Tuples, make into corresponding struct's with tighter definitions.\r\n" +
        "TODO: And clean up code in general, it is too difficult to follow now.\r\n" +
        "\r\n" +
        "TODO: IMPORTANT! idDiscriminiator must be turned into a ref-parameter in order to support branching.\r\n" +
        "TODO: IMPORTANT! Or even better, complement with 'depth' parameter in order to position elements in UI " +
        "TOOD: IMPORTANT! (move each level down one step to the right).\r\n" +
        "\r\n" +
        "TODO: Rename xxxxCreate into Createxxx (or maybe not?)\r\n" +
        "\r\n" +
        "TODO: Some ideas:\r\n" +
        "Let individual registration handlers communicate what information must be presented to the user in order to use that registration handler.\r\n" +
        "That is, information that the user must possess BEFORE entering data into the registration handler's model. What data is required " +
        "will also vary as SOME data is entered into the model. For instance when EventTime has been added, the data presented " +
        "can then be based on the situation at the event time."
    )]
    public class ProcessController : BaseController
    {

        private const long WidthRegVerb = 100;
        private const long WidthEventTime = 150;
        private const long WidthField = 170;
        [ClassMember(Description = "Margin in order to give space for explanation about obligatory or not values.")]
        private const long WidthFieldMargin = 20;
        private const long WidthSave = 60;

        [ClassMember(Description =
            "Note: For client offline scenarios these values can probably not be sent to server.\r\n" +
            "because they would have changed their meaning when it arrives at the server " +
            "(potentially several hours or even days later)\r\n" +
            "\r\n" +
            "Note: Some general thoughts about online / offline issues.\r\n" +
            "Client could store locally all changes and ask user to review as soon as a network connection is made again.\r\n" +
            "The client could ASK THE SERVER for how this representation should be presented to the user.\r\n"
        )]
        public List<string> EventTimeValues = new Func<List<string>>(() =>
        {
            var retval = new List<string>
            {
                "now",
                "15MinutesAgo",
                "30MinutesAgo",
                "45MinutesAgo",
                "60MinutesAgo",
                "OneHourAgo",
                "TwoHoursAgo",
                "ThreeHoursAgo",
                "FourHoursAgo",
                "Yesterday",
                "TwoDaysAgo",
                "ThreeDaysAgo",
                "Today",
                "Tomorrow",
                "InTwoDays",
                "InThreeDays",
                "InFourDays",
                "InFiveDays",
                "InSixDays",
                "InSevenDays",
                "bot"
            };
            return retval;
        })();

        // NOTE / TODO: Due to bug (as of May 2020) in ClassMemberAttribute as used by documentation, do not add a ClassMemberAttribute here. Document with ClassAttribute instead
        public override ContentResult APIMethod(
            ResponseFormat format,
            int jsonDepth,
            List<IKCoded> request,
            string? postData,
            DataStorage dataStorage
        )
        {
            if (format == ResponseFormat.JSON)
            {
                throw new NotImplementedException(format.ToString() + " is not implemented (if machine readable response is not needed, just use HTML instead)");
            }

            if (request.Count > 0 && string.IsNullOrEmpty(request[^1].ToString()))
            {
                request = request.Take(request.Count - 1).ToList();
            }

            // Note that only HTML is supported currently.
            // TODO: Consider offering JSON also, or some combination of JSON, HTML and Javascript.

            var htmlResult = new Func<string>(() =>
            {
                try
                {
                    dataStorage.Lock.EnterReadLock();

                    if (!dataStorage.Storage.TryGetP<IP>(PSPrefix.dt.ToString(), out var dt, out var errorResponse))
                    {
                        return "<p>" +
                           "ERROR: 'dt'-part of data storage not found.\r\n" +
                           "Details: " + WebUtility.HtmlEncode(errorResponse) + ".\r\n" +
                       "</p>";
                    }

                    var schemaCollection = dt.GetCollection<SchemaCollection>(() => new SchemaCollection()); // Tolerate no schemas at all

                    /// TODO: Add ongoing processes. For instance all <see cref="IRegHandler"/>s with <see cref="IRegHandler.DefaultEventStatus"/>
                    /// TODO: <see cref="EventStatus.WAIT_FOR_EXEC"/> where corresponding <see cref="EXEcuteR"/> has not been run yet.
                    /// TODO: Or all Events with <see cref="RegVerb.PLAN"/> and so on.
                    /// TODO: Or (even better) create some PLANNING Controller that shows everything in "progress" and things that need to be done.
                    switch (request.Count)
                    {
                        case 0:
                            // Initial stage, choose something do to
                            return
                                "<p>Choose a process:</p>" +
                                "<table><tr><th>Process name</th><th>Description</th></tr>\r\n" +
                                // TODO: Sort into groups (put into attribute), maybe with some 
                                // TODO: opening / closing <span>'s
                                string.Join("", schemaCollection.
                            /// Filter out schemas which constitute sub processes.
                            /// We would like to be able to "start" these also directly but there is currently (Dec 2021)
                            /// no mechanism for storing them (see <see cref="Process.TryGetRecipientId"/>)
                            Where(ikip => ikip.P.TryGetPV<Type>(SchemaP.EntityType, out var type) && !typeof(Process).IsAssignableFrom(type)).
                                    OrderBy(ikip => ikip.Key.ToString()).
                                    Select(ikip =>
                                        "<tr><td>" +
                                        "<a href=\"" + IKCoded.FromUnencoded(ikip.Key) + "\">" +
                                            WebUtility.HtmlEncode(ikip.Key.ToString()) +
                                        "</a>" +
                                        "</td><td>" +
                                        (!ikip.P.TryGetPV<string>("Name", out var name) ? "" :
                                            ("&nbsp;" + WebUtility.HtmlEncode(name))
                                        ) +
                                        (!ikip.P.TryGetPV<List<string>>(SchemaP.SubProcessType, out var subProcessTypes) ? "" :
                                            ("&nbsp;With sub processes " + string.Join(", ", subProcessTypes.Select(p => WebUtility.HtmlEncode(p))))
                                        ) +
                                        "</td></tr>\r\n"
                                    )
                                ) +
                                "<hr>\r\n" +
                                string.Join("", IRegHandler.AllIRegHandlerDerivedTypesTypeDict.
                                    OrderBy(e => e.Key.ToStringVeryShort()).
                                    Select(e =>
                                        "<tr><td>" +
                                            "<a href=\"" + e.Value + "\">" + e.Key.ToStringVeryShort() + "</a></td><td>" +
                                            (!ClassAttribute.GetAttribute(e.Key).IP.TryGetPV<string>(BaseAttributeP.Description, out var d) ? "&nbsp;" :
                                               // TODO: Add Span for rest of text. Already done in Documentator?
                                               WebUtility.HtmlEncode(d[..Math.Min(d.Length, 100)])
                                            ) +
                                        "</td></tr>\r\n"
                                    )
                                ) +
                                "</table>\r\n";
                        case 1:
                        case 2:
                            // Initial stage, fill in blank schema

                            // TODO: We do not have to bother with toRoot in a URL context. We can just start new URLs with '/' instead.
                            var toRoot = string.Join("", Enumerable.Repeat("../", request.Count + 1));

                            var schemaId = request[0].Unencoded.ToString();
                            var existingRegId = request.Count == 2 ? request[1].Unencoded : null;

                            /// There is a chicken and egg problem in the way that we build up the handling below
                            /// It should be valid to query Process/Process/{processId} here, but then schemaId will not
                            /// be valid at call to <see cref="Schema.TryGetIRegHandlerBasedProcessOrSchemaBasedProcess"/> below
                            /// Therefore we make a special case for "Process" here.
                            if (
                                nameof(Process).Equals(schemaId) &&
                                !(existingRegId is null) &&
                                dt.TryGetEntityInCollection<Process>(EventTime.EOT, regTime: null, existingRegId, out var process, out _) &&
                                process.IP.TryGetPV<SchemaId>(ProcessP.SchemaId, out var temp3))
                            {
                                // Now we have a "valid" schemaId to use below
                                schemaId = temp3.ToString();
                            }

                            if (!Schema.TryGetIRegHandlerBasedProcessOrSchemaBasedProcess(dt, schemaId, out var regHandlerType, out errorResponse))
                            {
                                return "<p>" + WebUtility.HtmlEncode(errorResponse) + "</p>";

                            }

                            (IK key, Reg value)? existingReg = null;

                            if (request.Count == 2)
                            {
                                if
                               (
                                   /// Misguided approach, <see cref="Reg"/>s are not stored as <see cref="PCollectionES"/> 
                                   /// (they are immutable, so there is no need)
                                   /// (but note the SOME of the <see cref="IRegHandler"/>s (those having sub processes) 
                                   /// ARE stored in <see cref="PCollectionES"/>
                                   //!dt.TryGetCollection<RegCollection>(out var regCollection, out errorResponse) ||
                                   //!regCollection.TryGetP<Reg>(existingRegId ?? throw new NullReferenceException(nameof(existingRegId)), out var temp, out errorResponse)

                                   // Correct approach
                                   !dt.TryGetP<IP>(IKType.FromType(typeof(Reg)), out var regCollection, out errorResponse) ||
                                   !regCollection.TryGetP<Reg>(existingRegId ?? throw new NullReferenceException(nameof(existingRegId)), out var temp, out errorResponse)
                               )
                                {
                                    return
                                       "<p>" +
                                           WebUtility.HtmlEncode("ERROR: Unable to find " + nameof(Reg) + " '" + request[1].Unencoded + "'") +
                                       "</p>\r\n" +
                                       "<p>" +
                                           "One possible explanation (if the database has grown huge) is that it is an old -" + nameof(Reg) + "- " +
                                           "which simply has been purged in order to save memory." +
                                       "/p>\r\n" +
                                       "<p>Details: " + WebUtility.HtmlEncode(errorResponse) + "</p>\r\n";
                                }
                                else
                                {
                                    existingReg = (existingRegId, temp);
                                }
                            }

                            (IK? key, IRegHandler value)? existingRegHandler = null;
                            if (existingRegId == null)
                            {
                                // No existing registration handler
                                if (!(regHandlerType.schemaId is null) && !(regHandlerType.schema is null))
                                {
                                    existingRegHandler = (key: null, value: regHandlerType.schema.ToProcess(regHandlerType.schemaId));
                                }
                            }
                            else if (
                                 IP.AllIPDerivedEntityCollectionClassesDict.TryGetValue(regHandlerType.subProcessType, out var collectionType) &&
                                 !(typeof(PCollectionES).IsAssignableFrom(collectionType))
                            )
                            {
                                /// Look in ordinary collection (most probably a <see cref="PCollection"/>)
                                /// The registration handler does not have sub processes
                                if (
                                    dt.TryGetP<IP>(IKType.FromType(regHandlerType.subProcessType), out var regHandlerPCollection) &&
                                    regHandlerPCollection.TryGetP<IRegHandler>(existingRegId, out var temp2)
                                )
                                {
                                    existingRegHandler = (key: existingRegId, value: temp2);
                                }
                            }
                            else if (
                               /// Look in <see cref="PCollectionES"/> and ask for <see cref="Snapshot"/> at EOF (end-of-time)
                               // 
                               // The registration handler probably has sub processes and therefore needs to reside within
                               // a PCollectionES 
                               //
                               // (Asking at EOF is necessary because if some of the sub processes are in the future, they 
                               // would not show up in the resulting "form" that vi generate now.)
                               dt.TryGetCollection(regHandlerType.subProcessType, out var regHandlerPCollectionES, out errorResponse) &&
                               regHandlerPCollectionES.TryGetP(EventTime.EOT, regTime: null, existingRegId, out var snapshot, out errorResponse) &&
                               snapshot.Entity is IRegHandler temp2
                           )
                            {
                                existingRegHandler = (key: existingRegId, value: temp2);
                            }
                            else
                            {
                                return "<p>ERROR: " +
                                   "Unable to find existing registration handler (" + regHandlerType.subProcessType.ToStringVeryShort() + "/" + existingRegId + ").\r\n" +
                                   "Possible cause: " + WebUtility.HtmlEncode(errorResponse) + ".\r\n" +
                                   // TODO: Change code above in order to be able to give out details here.
                                   //"Details:\r\n" +
                                   //(regHandlerCollection is null ? "" : ("Found " + nameof(regHandlerCollection) + " of type " + regHandlerCollection.GetType() + ".\r\n")) +
                                   //(snapshot is null ? "" : (
                                   //    "Found " + nameof(snapshot) + " of type " + snapshot.GetType().ToStringVeryShort() + ".\r\n" +
                                   //    "Found snapshot entity of type " + snapshot.Entity.GetType().ToStringVeryShort() + ".\r\n"
                                   //)) +
                                   "</p>\r\n";
                            }

                            var idDiscriminator = 0L;
                            return
                               "<p>" +
                                   "<a href=\"/Process/\">Choose another process</a>" +
                                   (
                                       request.Count < 2 ? "" :
                                       (
                                           "&nbsp;&nbsp;<a href=\"/Process/" + IKCoded.FromUnencoded(IKString.FromString(schemaId.ToString())) + "\">Repeat this process</a>"
                                       )
                                   ) +
                                   (existingRegId is null ? "" :
                                       (
                                           "&nbsp;&nbsp;View as Registration <a href=\"/RQ/dt/Reg/" + existingRegId + "\">" + existingRegId + "</a>\r\n"
                                       )
                                   ) +
                                   (
                                       (
                                           existingRegHandler is null ||
                                           !existingRegHandler.Value.value.TryGetPV<ParentProcessId>(ProcessP.ParentProcessId, out var parentProcessId)
                                       ) ? "" :
                                       (
                                           "&nbsp;&nbsp;Edit parent process <a href=\"/Process/" +
                                           parentProcessId.RegHandlerType.ToStringVeryShort() + "/" + parentProcessId.Key + "\">" + parentProcessId + "</a>\r\n"
                                       )
                                   ) +
                               "</p>\r\n" +
                               "<h1>" + (!(regHandlerType.schema is null) ?
                                   regHandlerType.schema.IP.GetPV<string>(SchemaP.Name) :
                                   regHandlerType.subProcessType.ToStringVeryShort()
                               ) + "</h1>\r\n" +
                               RegHandlerCreate(dt, toRoot, existingReg, regHandlerType, existingRegHandler, ref idDiscriminator, skipHeader: false);
                        default:
                            return
                               "<p>ERROR: Invalid number of parameters, zero, one (process name) "+
                               "or two (process name and identification) needed</p>";
                    }
                }
                finally
                {
                    dataStorage.Lock.ExitReadLock();
                }
            })();

            /// We are now outside of any locking context. 
            /// (the main reason for operating without locking now is that <see cref="DocLinkCollection.InsertLinks"/> is hugely expensive,
            /// but in general as much code as possible should of course be executed outside of a locking context)
            /// 
            // Any code following from here should NOT depend on data structures that may change.
            // (therefore any evaluations on changing datastructures have been prepared already in 'htmlResponse')

            var retval = htmlResult;

            if (dataStorage.DocLinks != null)
            {
                retval = dataStorage.DocLinks.InsertLinks(toDoc: "/RQ", contentHTML: retval);
            }

            return ContentResult.CreateHTMLOK(UtilDoc.GenerateHTMLPage(retval));
        }

        private string RegHandlerCreate(
            IP dt,
            string toRoot,
            (IK key, Reg value)? existingReg,
            (SchemaId? schemaId, Schema? schema, Type type) regHandlerType,
            (IK? key, IRegHandler value)? existingRegHandler,
            ref long idDiscriminator,
            bool skipHeader
        )
        {
            try
            {
                REx.Inc(); /// Guard because we are recursively called by <see cref="RegHandlerCreateSubProcesses"/>

                // TODO: Careful with sharing this. It has variants (include IsObligatory or not, PopulateBy or not)

                /// Note how <see cref="RegHandlerCreateSubmitWithAssert"/> does the same evaluation 
                /// (as done here below) in order to decide whether fields exists or not 
                /// (whether to look for document.getElementById('...').value or send as blank field)

                var allPk = regHandlerType.type.Equals(typeof(Process)) ?

                    // We have to specificially remove 'SubProcess' here because 
                    // PKUIAttributeP.SubProcessType can not be supported by Process as we do not know the type
                    PK.GetAllPKForEntityType(regHandlerType.type).Where(pk => ((ProcessP)pk.__enum) != ProcessP.SubProcess).ToList() :

                    PK.GetAllPKForEntityType(regHandlerType.type).Where(pk =>
                        !pk.TryGetA<PKUIAttribute>(out var pkui) || !pkui.IP.ContainsKey(PK.FromEnum(PKUIAttributeP.SubProcessType))
                    ).ToList();

                var tableWidth = WidthRegVerb + WidthEventTime + WidthSave + allPk.Count * WidthField;
                var retval =
                    (skipHeader ? "" : DescriptionCreate(regHandlerType)) +
                    "<table style=\"table-layout:fixed;width:" + tableWidth + "px;max-width:" + tableWidth + "px;min-width:" + tableWidth + "px;\">" +
                    (skipHeader ? "" : HeaderRowCreate(regHandlerType.type, allPk)) +
                    // Value row
                    // =====================================================
                    "<tr><td style=\"width:" + WidthRegVerb + "px;\">" +
                    RegCreateSelectForRegVerb(existingReg?.value, idDiscriminator) +
                        "</td><td style=\"width:" + WidthEventTime + "px;\">\r\n" + // Default = 'DO'
                    RegCreateSelectForEventTime(existingReg, idDiscriminator) + "</td>\r\n" + // Default = 'now'
                    RegHandlerCreateTableWithFields(dt, regHandlerType.type,
                        existingRegHandler?.value,
                        allPk, idDiscriminator
                    ) + "<td style=\"width:" + WidthSave + "px;\">\r\n" +
                    RegHandlerCreateSubmitWithAssert(toRoot, regHandlerType.type, idDiscriminator) + "\r\n" +
                    "</td></tr>\r\n" +
                    "</table>";

                if (existingReg is null || existingRegHandler is null)
                {
                    // We are unable to create sub processes now
                }
                else
                {
                    idDiscriminator += 1;
                    retval += RegHandlerCreateSubProcesses(
                           dt,
                           toRoot,
                           parentReg: existingReg.Value,
                           parentRegHandlerType: regHandlerType,
                           parentRegHandler: (existingReg.Value.key, existingRegHandler.Value.value),
                           ref idDiscriminator
                       ) + "\r\n";
                }
                return retval;
            }
            finally
            {
                REx.Dec();
            }
        }

        private string DescriptionCreate((SchemaId? schemaId, Schema? schema, Type type) regHandlerType) =>
            !(regHandlerType.schema is null) ?

                ("<hr><p><b>" + regHandlerType.schema.IP.GetPV<string>(SchemaP.Name, regHandlerType.schemaId?.ToString() ??
                    throw new ArgumentNullException(nameof(regHandlerType.schemaId) + ": Should always be set together with schema")
                    ) +
                "</b></p>\r\n"
                ) :

                (
                    "<hr><p><b>-" + regHandlerType.type.ToStringVeryShort() + "-</b></p>\r\n" +
                    (
                    !ClassAttribute.GetAttribute(regHandlerType.type).IP.TryGetPV<string>(BaseAttributeP.Description, out var d) ? "" : (
                        // Show first line only
                        "<p>" + WebUtility.HtmlEncode(d.Split("\r\n")[0]) + "</p>\r\n"
                    )
                )
            );

        private string HeaderRowCreate(Type regHandlerType, List<PK> allPk) =>
            "<tr><th style=\"width:" + WidthRegVerb + "px;\">" +
            nameof(RegVerb) + "</th><th style=\"width:" + WidthEventTime + "px;\">" +
            nameof(EventTime) + "</th>" +
            string.Join("", allPk.Select(pk =>
            {
                var helpTextHTML = pk.GetA<PKTypeAttribute>().GetHelptextHTML();
                return string.IsNullOrEmpty(helpTextHTML) ?
                    ("<th style=\"width:" + WidthField + "px;\">" + pk.ToString() + "</th>") :
                    ("<th style=\"width:" + WidthField + "px;\"><span title=\"" + helpTextHTML + "\">" + pk.ToString() + "</span></th>");
            })) + "<th style=\"width:" + WidthSave + "px;\">" +
            "Save</th></tr>\r\n";

        private string RegCreateSelectForRegVerb(Reg? existingReg, long idDiscriminator)
        {
            var defaultRegVerb = existingReg != null ?
                existingReg.IP.GetPV<RegVerb>(RegP.RegVerb, defaultValue: RegVerb.DO) :
                RegVerb.DO;

            return
                "<select " +
                    "name=\"" + nameof(RegVerb) + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "\" " +
                    "id=\"" + nameof(RegVerb) + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "\" " +
                    "style=\"width:" + (WidthRegVerb - 10) + "px;\"" +
                ">\r\n" +
                string.Join("", UtilCore.EnumGetMembers<RegVerb>().Select(at =>
                    "<option " +
                    "value=\"" + at + "\"" +
                    (at != defaultRegVerb ? "" : " selected") +
                    ">" + at + "</option>\r\n"
                )) +
                "</select>\r\n";
        }

        private string RegCreateSelectForEventTime((IK key, Reg value)? existingReg, long idDiscriminator)
        {
            var x = existingReg?.key;

            return existingReg is null ?
                (
                    "<select " +
                        "name=\"" + nameof(EventTime) + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "\" " +
                        "id=\"" + nameof(EventTime) + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "\" " +
                        "style=\"width:" + (WidthEventTime - 10) + "px;\"" +
                    ">\r\n" +
                    string.Join("", EventTimeValues.Select(et =>
                        "<option value=\"" + et + "\">" + et + "</option>\r\n"
                    )) +
                    "</select>\r\n"
                ) :
                (
                    "<input " +
                        "name=\"" + nameof(EventTime) + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "\" " +
                        "id=\"" + nameof(EventTime) + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "\" " +
                        "type=\"hidden\" " +
                        "value=\"" + existingReg.Value.key + "\" " +
                    //"style=\"width:" + (WidthEventTime - 10) + "px;\"" +
                    //"readonly" +
                    "/>\r\n" +

                    "<span title=\"" +
                        "RegId: " + existingReg.Value.key + ". " +
                        "This Registration can be modified but its EventTime can not be changed" +
                    "\"><label>" + existingReg.Value.value.EventTime.ToString() +
                    "</span></label>\r\n"
                ); ;
        }

        private string RegHandlerCreateSubProcesses(
            IP dt,
            string toRoot,
            (IK key, Reg value) parentReg,
            (SchemaId? schemaId, Schema? schema, Type type) parentRegHandlerType,
            (IK? key, IRegHandler value) parentRegHandler,
            ref long idDiscriminator
        )
        {
            var retval = new StringBuilder();

            var subProcessesFromCSharpCode =
                PK.GetAllPKForEntityType(parentRegHandler.value.GetType()).SelectMany(pk =>
                (!pk.TryGetA<PKUIAttribute>(out var pkui) || !pkui.IP.TryGetPV<List<Type>>(PKUIAttributeP.SubProcessType, out var subProcessTypes)) ?
                    new List<(SchemaId? schemaId, Schema? schema, Type subProcessType)>() :
                    subProcessTypes.Select(subProcessType => (schemaId: (SchemaId?)null, schema: (Schema?)null, subProcessType))
                ).ToList();

            var subProcessesFromSchema = parentRegHandlerType.schema is null ? // From user configurable schema
                    new List<(SchemaId? schemaId, Schema? schema, Type subProcessType)>() :
                    parentRegHandlerType.schema.GetSubProcesses(dt);

            if (subProcessesFromCSharpCode.Count > 0 && subProcessesFromSchema.Count > 0)
            {
                throw new ProcessControllerException(
                    "subProcessesFromCSharpCode.Count > 0 && subProcessesFromSchema.Count > 0\r\n" +
                    "\r\n" +
                    "This is not necessarily a problem, just that it was made an assumption that sub processes would\r\n" +
                    "arise EITHER from one place OR the other.\r\n" +
                    "\r\n" +
                    "Possible resolution: Just remove this assertion from the code.\r\n"
                );
            }

            var allSubProcesses = subProcessesFromCSharpCode.Concat(subProcessesFromSchema).ToList();

            var foundExistingSubProcess = false;

            // Note: Algorithm here is kind of O(n^2) (two for loops), but with very small numbers of n.
            foreach (var subProcessType in allSubProcesses)
            {
                var thisIsFirstExistingSubProcess = true;

                var existingSubProcesses = new StringBuilder();

                var subProcesses = parentRegHandler.value.GetPV(ProcessP.SubProcess, new List<SubProcess>()).
                    // Show most recent at top
                    OrderByDescending(subProcess => subProcess.SerialNumber).
                    ToList();

                foreach (var subProcess in subProcesses)
                {
                    // Find existing registration and decide if belongs to this shema at all
                    var existingRegId = new RegId(subProcess.SerialNumber);

                    //var temp = dt.GetP(IKType.FromType(typeof(Reg))).GetP(existingRegId);
                    //// TODO: Improve on default interface methods in order to avoid code like this
                    //var existingReg = temp as Reg ?? throw new InvalidObjectTypeException(temp, typeExpected: typeof(Reg));


                    // TODO: Make this fail more gracefully if Registration is not found (use GetP instead of TryGetP here)
                    // TODO: Remember that the RegCollection is not "meant" to be available back to beginning of time.
                    // TDOO: We need a Registration object in order to EDIT schema, but should have to need it in order to only view it
                    // TODO: (so we should be able to do without one here)
                    var existingReg = dt.GetPV<RegCollection>(IKType.FromType(typeof(Reg))).IP.GetPV<Reg>(existingRegId);
                    var regHandler = existingReg.IP.GetP<IRegHandler>(RegP.RH);
                    if (!(subProcessType.schemaId is null)) // Compare schemaId with schemaId
                    {
                        //InvalidObjectTypeException.AssertEquals(typeof(Process), regHandler.GetType(), () =>
                        //    "subProcessType defines a schemaId, expected process to be of type Process"
                        //);
                        if (!(regHandler is Process process))
                        {
                            throw new InvalidObjectTypeException(regHandler,
                                "subProcessType defines a schemaId, expected process to be of type Process");
                        }
                        if (!(process.IP.GetPV<SchemaId>(ProcessP.SchemaId) == subProcessType.schemaId))
                        {
                            // Do not include this
                            continue;
                        }
                    }
                    else // Compare regHandler's Type
                    {
                        if (regHandler is Process)
                        {
                            throw new InvalidObjectTypeException(regHandler,
                                "subProcessType does NOT define a schemaId, expected process to NOT be of type Process");
                        }

                        if (!subProcess.RegHandlerType.Equals(subProcessType.subProcessType))
                        {
                            // Do not include this
                            continue;
                        }
                    }

                    // This method was designed for getting multiple sub processes but
                    // it works OK for getting a single one also with a little assertion afterwards.
                    if (!SubProcess.TryGetRegHandlers<IRegHandler>(
                        dt,
                        // (Asking at EOF is necessary because if some of the sub processes are in the future, they 
                        // would not show up in the resulting "form" that vi generate now)
                        EventTime.EOT,
                        new List<SubProcess> { subProcess },
                        minimumCountRequired: 0, // NOTE: Value is really "1", but we want to construct a better error message below.
                        out var tempList,
                        out var errorResponse
                    ) ||
                        tempList.Count != 1)
                    {
                        existingSubProcesses.Append("<p>ERROR: " + WebUtility.HtmlEncode(
                            "Sub process " + subProcess.RegHandlerType.ToString() + " with key " + subProcess.SerialNumber + " " +
                            "was not found.\r\n" +
                            (tempList == null || tempList.Count == 1 ? "" : ("\r\nDetails: Found " + tempList.Count + " registration handlers instead of 1")) +
                            (errorResponse == null ? "" : ("\r\nDetails: " + errorResponse))
                        ) + "</p>\r\n");
                        continue;
                    }

                    var skipHeader = !thisIsFirstExistingSubProcess || (
                        // We assume that of sub processes that are IRegHandler, multiple will always be relevant
                        // That is, a suggestion for inserting a new IRegHandler wil be generated, mening that
                        // a header will be generated ABOVE what is produced now, meaning that we can skip headers.
                        subProcessType.schema is null
                    );


                    thisIsFirstExistingSubProcess = false;

                    idDiscriminator += 1;

                    existingSubProcesses.Append(
                        RegHandlerCreate(dt, toRoot,
                            existingReg: (existingRegId, existingReg),
                            regHandlerType: subProcessType,
                            existingRegHandler: (existingRegId, tempList[0]),
                            ref idDiscriminator,
                            skipHeader
                        )
                    );
                }

                if (existingSubProcesses.Length > 0)
                {
                    foundExistingSubProcess = true;
                }

                if (!(subProcessType.schema is null) && existingSubProcesses.Length > 0)
                {
                    // Assume that of sub processes that are Shemas (Processes), only one will be needed.

                    /// (TODO: Improve Schema definition, explain if optional, exact one, 1..n or 0..n is relevant).
                    /// This could be a Struct which replaces the existing <see cref="SchemaP.Process"/> string type today.
                    /// The Cardinality could look like One, ZtOne, ZtN, 1tN for instance and maybe also Exclusive
                }
                else
                {
                    // Assume that of sub processes that are IRegHandler, multiple will always be relevant

                    /// (TODO: Improve on <see cref="PKUIAttributeP.SubProcessType"/>, 
                    /// Explain if optional, exact one, 1..n or 0..n is relevant).
                    /// For instance a new field, SubProcessTypeCardinality, same as for Schema above.
                    idDiscriminator += 1;
                    retval.Append(RegHandlerCreateSubProcess(dt, toRoot, parentReg, parentRegHandler, subProcessType, idDiscriminator));
                }

                retval.Append(existingSubProcesses);

            }

            if (foundExistingSubProcess) // Add link for completing sub processes.
            {
                if (!(parentRegHandlerType.schema is null))
                {
                    /// There is little meaning in doing <see cref="EXEcuteR"/> for a schema based process
                    /// because <see cref="Process"/> does not implement <see cref="IRegHandlerTGTE"/>
                }
                else
                {
                    retval.Insert(index: 0,
                        "<span title=\"" + WebUtility.HtmlEncode(
                            "'Completes' the sub processes by running " + nameof(EXEcuteR) + ".\r\n" +
                            "\r\n" +
                            "This will call " + nameof(IRegHandlerTGTE.TryGetTransientEvents) + " on the super process " +
                            "(" + parentRegHandler.value.GetType().ToStringVeryShort() + ") which until now have had " +
                            nameof(EventStatus) + "." + nameof(EventStatus.WAIT_FOR_EXEC) + " because list of sub processes " +
                            "have not been available."
                        ) + "\">\r\n" +
                        "<p><a href=\"" + toRoot + "RS/DO/now/" + nameof(EXEcuteR) + "/" +
                            parentRegHandler.value.GetType().ToStringVeryShort() + "/" + parentReg.key + "\">" +
                            "Complete sub processes</a>" +
                        "</p>\r\n" +
                        "</span>\r\n"
                    );
                }
            }

            return retval.ToString();
        }
        private string RegHandlerCreateSubProcess(
            IP dt,
            string toRoot,
            (IK key, Reg value)? parentReg,
            (IK? key, IRegHandler value) parentRegHandler,
            (SchemaId? schemaId, Schema? schema, Type type) subProcessType,
            long idDiscriminator
        )
        {
            if (parentRegHandler.key == null) throw new ArgumentNullException("parentRegHandler.key==null");

            InvalidTypeException.AssertAssignable(subProcessType.type, typeof(IRegHandler), () =>
                parentRegHandler.value.GetType().ToStringShort() + " defines a -" + nameof(SubProcess) + "- " +
                "(" + subProcessType.type.ToStringShort() + ") " +
                "which is not of type -" + nameof(IRegHandler) + "-");

            // Assume that can have multiple sub processes
            // TODO: Add some TryCreate for IP in general here with better error checking
            // TODO: (search for all use of System.Activator in AgoRapide)
            var subProcessRegHandler = subProcessType.schema != null ?
                (
                    subProcessType.schema.ToProcess(
                        subProcessType.schemaId ?? throw new ArgumentNullException("subProcessType.schemaId must always be set together with schema"),
                        createSubProcess: true,
                        entityKey: parentRegHandler.key
                    )
                ) :
                (IRegHandler)System.Activator.CreateInstance(subProcessType.type);

            // TODO: Delete commented out code
            ///// Example: The parentIdKey will for Process be <see cref="ProcessP.ParentProcessId"/>
            ///// For "ordinary" <see cref="RegHandler"/>s the parent key must be strictly named according to the same pattern.
            ///// For instance for "OrderLine" there must be an "OrderLineP.ParentOrderId" key to set.
            //var parentIdKey = IKString.FromCache("Parent" + parentRegHandler.value.GetType().ToStringVeryShort() + "Id");

            var parentIdKey = IKString.FromCache(nameof(ProcessP.ParentProcessId));

            // TODO: This assertion is of little value of subProcessRegHandler is of a permissible
            // TOOD: IP implementation like PRich that can store "any" value.
            if (!subProcessRegHandler.TrySetPV(
                parentIdKey,                 
                ParentProcessId.Parse( // We can not use constructor directly because an integer key is required.
                    IRegHandler.AllIRegHandlerDerivedTypesTypeDict.GetValue(parentRegHandler.value.GetType(), () =>
                        // TODO: Implement Get and TryGet on IRegHandler instead for this.
                        "Shorthand representation of type " + parentRegHandler.value.GetType() + " not found"
                    ) + " " + 
                    parentRegHandler.key 
                ),
                out var errorResponse
            ))
            {
                // TODO: Maybe change into exception?
                // TODO. First make sure that this is not some user caused problem (schema definition)
                return "<p>ERROR: " +
                    "Unable to complete creation of sub process " + subProcessRegHandler.GetType().ToStringVeryShort() +
                    " for " + parentRegHandler.GetType().ToStringVeryShort() + " because the sub process does not " +
                    "support setting of field " + parentIdKey +
                "</p>";
            }

            // NOTE how we do not have the key here yet!!! Only 'subProcessRegHandler'
            return
                RegHandlerCreate(dt, toRoot, null,
                    subProcessType.schema != null ?
                        (subProcessType.schemaId, subProcessType.schema, subProcessRegHandler.GetType()) :
                        (null, null, subProcessRegHandler.GetType()),
                    (key: null, value: subProcessRegHandler), ref idDiscriminator, skipHeader: false);
        }

        [ClassMember(Description = "NOTE: This does not include <table>, header row or </table>")]
        private string RegHandlerCreateTableWithFields(
            IP dt,
            Type regHandlerType,
            IRegHandler? existingRegHandler,
            List<PK> allPk,
            long idDiscriminator
        )
        {
            return
                // Input row
                // =====================================================
                string.Join("", allPk.Select(pk =>
                {
                    return "<td style=\"width:" + WidthField + "px;\">" +

                        // <input> or <select> for value
                        // =====================================================
                        new Func<string>(() =>
                        {
                            var pkToString = pk.ToString();

                            if (
                                nameof(ProcessP.ParentProcessId).Equals(pkToString) &&
                                existingRegHandler != null &&
                                existingRegHandler.TryGetPV<ParentProcessId>(ProcessP.ParentProcessId, out var parentProcessId)
                            )
                            {
                                return FieldCreateReadonlyInputForParentProcessId(parentProcessId, idDiscriminator);
                            }

                            if (regHandlerType.Equals(typeof(Process)))
                            {
                                if (existingRegHandler == null) throw new NullReferenceException(
                                      nameof(existingRegHandler) + ": Must always be set for " + nameof(Process));
                                if (!(pk.__enum is ProcessP processP)) throw new InvalidObjectTypeException(pk.__enum, typeof(ProcessP));

                                    /// TODO: After introduction of <see cref="ParentProcessId"/> this is superfluous now
                                    /// TODO: at least for sub processes.
                                    return (processP) switch
                                {
                                        // These two fields are special case
                                        ProcessP.EntityType => FieldCreateReadonlyInputForProcessEntityType(existingRegHandler, idDiscriminator),
                                    ProcessP.EntityKey => FieldCreateSelectForIds(
                                        dt.GetCollection(existingRegHandler.GetPV<Type>(ProcessP.EntityType)),
                                        pk, existingRegHandler, idDiscriminator
                                    ),
                                        // The other fields can be shown as standard, but readonly
                                        _ => FieldCreateStandardTextbox(pk, existingRegHandler, idDiscriminator, readOnly: true)
                                };
                            }

                            if (pk.Type == typeof(Type))
                            {
                                return FieldCreateSelectForType(pk, existingRegHandler, idDiscriminator);
                            }

                            if (pk.Type.IsEnum)
                            {
                                return FieldCreateSelectForEnum(pk, existingRegHandler, idDiscriminator);
                            }

                            if (
                                pkToString.EndsWith("Id") &&
                                (
                                    !pk.TryGetA<PKUIAttribute>(out var pkui) ||
                                    pkui.DoNotOfferIds == false
                                ) &&
                                (
                                    IP.AllIPDerivedTypesDict.TryGetValue(pkToString[..^2], out var tId) ||
                                    (
                                        pk.TryGetA<PKRelAttribute>(out var pkRel) &&
                                        (tId = pkRel.ForeignEntity) != null
                                    )
                                ) &&
                                dt.TryGetCollection(tId, out var collection, out _)
                                )
                            {
                                return FieldCreateSelectForIds(collection, pk, existingRegHandler, idDiscriminator);
                            }

                                // We can not offer any help with input, show ordinary textbox
                                return FieldCreateStandardTextbox(pk, existingRegHandler, idDiscriminator);
                        })() +

                        // Required
                        // =====================================================
                        // TODO: Distinguish between required for DO and required for PLAN
                        // TODO: And add some explanation (span or similar)
                        (pk.GetA<PKTypeAttribute>().IP.GetPV<bool>(PKTypeAttributeP.IsObligatory, defaultValue: false) ? "*" : "&nbsp;") +

                        "</td>";

                }));
        }

        private string FieldCreateReadonlyInputForParentProcessId(ParentProcessId parentProcessId, long idDiscriminator) =>
            "<input " +
                "name=\"" + nameof(ProcessP.ParentProcessId) + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "\" " +
                "id=\"" + nameof(ProcessP.ParentProcessId) + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "\" " +
                "type=\"text\" " +
                // Note that value is required, GetPV will throw exception now if not found.
                "value=\"" + parentProcessId.ToString() + "\" " +
                "readonly " +
                "style=\"width:" + (WidthField - WidthFieldMargin) + "px;\"" +
            ">\r\n";

        private string FieldCreateReadonlyInputForProcessEntityType(IRegHandler existingRegHandler, long idDiscriminator) =>
            "<input " +
                "name=\"" + nameof(ProcessP.EntityType) + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "\" " +
                "id=\"" + nameof(ProcessP.EntityType) + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "\" " +
                "type=\"text\" " +
                // Note that value is required, GetPV will throw exception now if not found.
                "value=\"" + existingRegHandler.GetPV<Type>(ProcessP.EntityType).ToStringVeryShort() + "\" " +
                "readonly " +
                "style=\"width:" + (WidthField - WidthFieldMargin) + "px;\"" +
            ">\r\n";

        private string FieldCreateSelectForType(PK pk, IRegHandler? existingRegHandler, long idDiscriminator)
        {
            var pkToString = pk.ToString();
            var defaultType = (existingRegHandler != null && existingRegHandler.TryGetPV<Type>(pk, out var temp)) ?
                temp.ToStringVeryShort() :
                null;

            return
                // Return <select> with relevant types
                "<select " +
                    "name=\"" + pkToString + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "\" " +
                    "id=\"" + pkToString + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "\" " +
                    "style=\"width:" + (WidthField - WidthFieldMargin) + "px;\"" +
                ">\r\n" +
                "<option value=\"\"></option>\r\n" +
                string.Join("", IP.AllIPDerivedTypes.Where(t =>
                        !t.Assembly.GetName().Name.StartsWith("ARC") && // Exclude types in AgoRapide (ARCore assemblies) // TODO: Create some better defined mechanism for this.
                        !"Root".Equals(t.ToStringVeryShort()) && // TODO: Root is not well defined anywhere (it is typically what is returned by "RQ/" by convention though)
                        !typeof(IRegHandler).IsAssignableFrom(t) &&  // It is probably meaningless to include registration handlers in registration handlers.
                        !t.ToStringVeryShort().EndsWith("Collection") &&
                        !t.IsAbstract &&
                        !t.IsInterface
                    ).Select(t =>
                    {
                        var tToStringVeryShort = t.ToStringVeryShort();
                        return "<option " +
                            "value=\"" + tToStringVeryShort + "\"" +
                            (tToStringVeryShort != defaultType ? "" : " selected") +
                            ">" + tToStringVeryShort +
                        "</option>\r\n";
                    })) +
                "</select>\r\n";
        }

        private string FieldCreateSelectForEnum(PK pk, IRegHandler? existingRegHandler, long idDiscriminator)
        {
            var pkToString = pk.ToString();
            var defaultEnum = (existingRegHandler != null && existingRegHandler.TryGetPV<string>(pk, out var temp)) ? temp : null;

            return
                // Return <select> with relevant types
                "<select " +
                    "name=\"" + pkToString + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "\" " +
                    "id=\"" + pkToString + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "\" " +
                    "style=\"width:" + (WidthField - WidthFieldMargin) + "px;\"" +
                ">\r\n" +
                "<option value=\"\"></option>\r\n" +
                string.Join("", UtilCore.EnumGetMembers(pk.Type).Select(e => e.ToString()).
                    Select(e =>
                    {
                        return "<option " +
                            "value=\"" + e + "\"" +
                            (e != defaultEnum ? "" : " selected") +
                            ">" + e +
                        "</option>\r\n";
                    })) +
                "</select>\r\n";
        }

        private string FieldCreateSelectForIds(IP collection, PK pk, IRegHandler? existingRegHandler, long idDiscriminator)
        {
            var pkToString = pk.ToString();
            var defaultId = existingRegHandler == null || !existingRegHandler.TryGetPV<string>(pk, out var temp, out var errorResponse) ? null : temp;

            return
                // Return <select> with all possible ids
                "<select " +
                    "name=\"" + pkToString + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "\" " +
                    "id=\"" + pkToString + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "\" " +
                    "style=\"width:" + (WidthField - WidthFieldMargin) + "px;\"" +
                ">\r\n" +
                "<option value=\"\"></option>\r\n" +
                // TODO: Make id selection smarter, restrict to a subset of entities for instance.
                string.Join("", collection.OrderBy(ikip => ikip.Key.ToString()).Select(ikip =>
                    "<option " +
                    "value=\"" + ikip.Key.ToString() + "\"" +
                    (ikip.Key.ToString() != defaultId ? "" : " selected") +
                    ">" +
                        // TODO: Do not shown Key if Name is also found.
                        ikip.Key.ToString() +
                        (
                            !ikip.P.TryGetPV<string>("Name", out var name) ? "" :
                            // By convention we assume that "Name" is the property that should be shown here
                            (" " + WebUtility.HtmlEncode(name))
                        ) +
                    "</option>\r\n"
                )) +
                "</select>\r\n";
        }

        private string FieldCreateStandardTextbox(PK pk, IRegHandler? existingRegHandler, long idDiscriminator, bool readOnly = false) =>
            "<input" +
                " name=\"" + pk.ToString() + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "\"" +
                " id=\"" + pk.ToString() + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "\"" +
                " type=\"text\"" +
                ((existingRegHandler == null || !existingRegHandler.TryGetPV<string>(pk, out var temp)) ? "" :
                    // TODO: BUG: SECURITY: Do some proper encoding here, including quotation marks
                    // TODO: BUG: SECURITY: We must encode to HTML, and after that do 0xabcd encoding.
                    " value=\"" + temp + "\""
                ) +
                (!readOnly ? "" : " readonly") +
                " style=\"width:" + (WidthField - WidthFieldMargin) + "px;\"" +
            ">\r\n";

        private string RegHandlerCreateSubmitWithAssert(string toRoot, Type regHandlerType, long idDiscriminator)
        {

            return
            "<input type=\"submit\" value=\"Save\" onclick=\"\r\n" +
                // Assert that all obligatory values are filled in
                string.Join("", PK.GetAllPKForEntityType(regHandlerType).Select(pk =>
                    // NOTE: If IsObligatory is combined with PKUIAttributeP.SubProcessType, this will fail.
                    (!pk.TryGetA<PKTypeAttribute>(out var pktype) || !pktype.IP.GetPV(PKTypeAttributeP.IsObligatory, false)) ? "" :
                    (
                        "if (document.getElementById('" + pk.ToString() + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "').value == '') {\r\n" +
                            "alert('Value " + pk.ToString() + " is missing');\r\n" +
                            "return;\r\n" +
                        "}\r\n"
                    )
                )) +
                "location.href = location.href + '/" + toRoot + "RS/' +\r\n" +
                "document.getElementById('" + nameof(RegVerb) + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "').value + '/' +\r\n" +
                "document.getElementById('" + nameof(EventTime) + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "').value + '/' +\r\n" +
                "'" + regHandlerType.ToStringVeryShort() + "'" +
                string.Join("", PK.GetAllPKForEntityType(regHandlerType).Select(pk =>
                    (
                        /// This must correspond to keys generated in <see cref="RegHandlerCreate"/>
                        (pk.TryGetA<PKUIAttribute>(out var pkui) && pkui.IP.ContainsKey(PK.FromEnum(PKUIAttributeP.SubProcessType))) ||
                        (regHandlerType.Equals(typeof(Process)) && ((ProcessP)pk.__enum) == ProcessP.SubProcess)
                    ) ?
                        /// These will have been skipped by <see cref="RegHandlerCreateTableWithFields"/>
                        /// Send as empty fields.
                        " + '/'\r\n" :
                        // TODO: Add 0xabcd encoding here from some Javascript library
                        (" + '/' + \r\ndocument.getElementById('" + pk.ToString() + (idDiscriminator == 0 ? "" : ("_" + idDiscriminator)) + "').value")
                )) +
                ";\r\n" +
                "\"/>";
        }

        public class ProcessControllerException : ApplicationException
        {
            public ProcessControllerException(string message) : base(message) { }
            public ProcessControllerException(string message, Exception inner) : base(message, inner) { }
        }
    }
}
