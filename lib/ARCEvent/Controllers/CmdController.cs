﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using ARCAPI;
using System.Linq;
using System.Net;
using ARCDoc;

namespace ARCEvent
{
    public class CmdController : BaseController
    {
        // NOTE / TODO: Due to bug (as of May 2020) in ClassMemberAttribute as used by documentation, do not add a ClassMemberAttribute here. Document with ClassAttribute instead
        public override ContentResult APIMethod(
            ResponseFormat format,
            int jsonDepth,
            List<IKCoded> request,
            string? postData,
            DataStorage dataStorage
        )
        {
            var timeBefore = DateTime.UtcNow;

            if (format == ResponseFormat.JSON)
            {
                throw new NotImplementedException(format.ToString() + " is not implemented (if machine readable response is not needed, just use HTML instead)");
            }

            if (format == ResponseFormat.HTML)
            {
                var htmlResponse = new Func<string>(() =>
                {
                    Cmd cmd;
                    ICmdHandler cmdHandler;
                    IP? dt = null;
                    CmdCollection? cmdCollection = null;
                    IP? cmdHandlerCollection = null;
                    try
                    {
                        dataStorage.Lock.EnterReadLock();
                        if (!TryParseCommand(
                            dataStorage,
                            request.Select(r => r.Unencoded.ToString()).ToList(),
                            out cmd, out cmdHandler, out var errorResponse,
                            ref dt, ref cmdCollection, out cmdHandlerCollection
                        ))
                        {
                            return "<h1>ERROR</h1><p>-" +
                               nameof(TryParseCommand) + "-: " +
                               WebUtility.HtmlEncode(errorResponse).Replace("\r\n", "<br>\r\n") +
                            "</p>";
                        }
                    }
                    finally
                    {
                        dataStorage.Lock.ExitReadLock();
                    }

                    if (dt == null || cmdCollection == null || cmdHandlerCollection == null)
                    {
                        throw new NullReferenceException("dt == null || cmdCollection == null || cmdHandlerCollection == null");
                    }

                    try
                    {
                        dataStorage.Lock.EnterWriteLock();
                        // TODO: Serialize to disk also

                        if (!TryStoreAndExecuteCommand(dataStorage, timeBefore, cmd, cmdHandler, out var errorResponse, cmdCollection, cmdHandlerCollection))
                        {
                            var retval =
                                "<h1>ERROR</h1>\r\n" +
                                "<p>-" +
                                   nameof(TryStoreAndExecuteCommand) + "-: " +
                                   WebUtility.HtmlEncode(errorResponse).Replace("\r\n", "<br>\r\n") +
                                "</p>" +

                                "<hr><p>Command: <a href=\"/RQ/dt/Cmd/{TODO: Insert key here}\">{TODO: Insert key here}</a></p>\r\n" +

                                cmd.ToHTMLSimpleSingle(prepareForLinkInsertion: true, request);

                            return retval;
                        }

                        // NOTE: Generation of text below does not require a write lock against the database
                        // NOTE: A read lock would be sufficient.
                        // NOTE: In other words, we could release the write lock before creating text below.
                        // NOTE: The point is probably not very relevant when we introduce JSON responses (which will be most common)
                        // NOTE: because they will be much more simpler (just returning and id-number or two).
                        return
                              (
                                  "<h1>Command execute OK</h1>\r\n" +
                                  "<p>" +
                                     "Command: <a href=\"/RQ/dt/Cmd/{TODO: Insert key here}\">{TODO: Insert key here}</a>&nbsp;&nbsp;\r\n" +
                                  "</p>\r\n" +
                                  "<hr>\r\n"
                              ) +
                              "<hr>\r\n" +
                              cmd.ToHTMLSimpleSingle(prepareForLinkInsertion: true, request);
                    }
                    finally
                    {
                        dataStorage.Lock.ExitWriteLock();
                    }
                })();


                /// We are now outside of any locking context. 
                /// (the main reason for operating without locking now is that <see cref="DocLinkCollection.InsertLinks"/> is hugely expensive,
                /// but in general as much code as possible should of course be executed outside of a locking context)
                /// 
                // Any code following from here should NOT depend on data structures that may change.
                // (therefore any evaluations on changing datastructures have been prepared already in 'htmlResponse')

                var retval = htmlResponse;

                if (dataStorage.DocLinks != null)
                {
                    retval = dataStorage.DocLinks.InsertLinks(toDoc: "/RQ", contentHTML: retval);
                }

                return ContentResult.CreateHTMLOK(UtilDoc.GenerateHTMLPage(retval));
            }

            throw new InvalidEnumException(format);
        }

        [ClassMember(Description =
            "Attempts to parse the given request into an -" + nameof(Cmd) + "- (Command) object.\r\n" +
            "\r\n" +
            "Uses -" + nameof(ICmdHandler) + "-.-" + nameof(ICmdHandler.TryParse) + "-.\r\n" +
            "\r\n" +
            "TODO: Could probably be moved to the Cmd-class instead.\r\n" +
            "\r\n" +
            "The convoluted syntax is due to need for high performance and also to avoid write-lock until as late as possible.\r\n" +
            "\r\n" +
            "Note use of ref-parameters in case calling method makes multiple calls.\r\n"
        )]
        public static bool TryParseCommand(
            DataStorage dataStorage,
            List<string> request,
            out Cmd cmd,
            out ICmdHandler cmdHandler, // Included in Cmd, but makes for quicker access when separate
            out string errorResponse,
            ref IP? dt,
            ref CmdCollection? cmdCollection,
            out IP cmdHandlerCollection
        )
        {
            if (!(dataStorage.Lock.IsReadLockHeld || dataStorage.Lock.IsWriteLockHeld))
            {
                // Note: Requirement for read lock applies to the "whole" method, not only finding cmdCollection below
                // because we peek inside existing Command object
                throw new CmdControllerException(
                    "!!(dataStorage.Lock.IsReadLockHeld || dataStorage.Lock.IsWriteLockHeld).\r\n" +
                    "\r\n" +
                    "Resolution: Ensure that the thread has acquired a read lock before calling this method."
                );
            }

            if (dt == null || cmdCollection == null)
            {
                if (!dataStorage.Storage.TryGetP<IP>(IKString.FromCache(PSPrefix.dt.ToString()), out dt, out errorResponse))
                {
                    cmd = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    cmdHandler = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    cmdHandlerCollection = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return false;
                }

                /// TOOD: Make configurable whether to store in Command collection afterwards.
                /// TODO: The collection is a "nice-to-have" thing, not essential to the functioning of ... TOOD: Complete sentence here
                /// TODO: Alternatively, create some mechanism for only keeping recent Commands in the collection, for debug purposes.
                if (!dt.TryGetP<CmdCollection>(IKType.FromType(typeof(Cmd)), out cmdCollection, out errorResponse))
                {
                    cmd = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    cmdHandler = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    cmdHandlerCollection = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return false;
                }
            }

            if (request.Count < 2) // [TODO: CHECK IF CORRECT]
            {
                errorResponse = "Too few parameters (" + request.Count + "), at least 2 is required [TODO: CHECK IF CORRECT]";
                cmd = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                cmdHandler = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                cmdHandlerCollection = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }

            if (!ICmdHandler.TryParse(request, out cmdHandler, out errorResponse))
            {
                errorResponse = "Invalid model. Details:\r\n" + errorResponse;
                cmd = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                cmdHandlerCollection = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }

            if (!dt.TryGetP(IKType.FromType(cmdHandler.GetType()), out cmdHandlerCollection))
            {
                // TODO: Note exception here, but errorResponse for the other collections above. Make more similar.
                throw new DatastructureException(
                    "Collection for type " + cmdHandler.GetType() + " not initialized. Should have been defined at application startup.");
            }

            // Ensure that all components involved in parsing have to same idea of what 'now' means
            var now = UtilCore.DateTimeNow;

            cmd = new Cmd();
            cmd.IP.SetPV(CmdP.CmdTime, now);
            // TODO: Reconsider if necessary now that we also link to the regHandler in its own collection.
            cmd.IP.SetP(CmdP.CH, cmdHandler);
            cmd.IP.SetPV(CmdP.CmdStatus, CmdStatus.NEW);

            return true;
        }

        [ClassMember(Description =
            "Attempts to store and execute the given -" + nameof(Cmd) + "- (Command) through " +
            "-" + nameof(CmdProcessor.TryExecuteCmd) + "-.\r\n"
        )]
        public static bool TryStoreAndExecuteCommand(
            DataStorage dataStorage,
            DateTime timeBefore,
            Cmd cmd,
            ICmdHandler cmdHandler,
            out string errorResponse,
            IP cmdCollection,
            IP cmdHandlerCollection
        )
        {
            if (!dataStorage.Lock.IsWriteLockHeld)
            {
                throw new CmdControllerException(
                    "!dataStorage.Lock.IsWriteLockHeld.\r\n" +
                    "\r\n" +
                    "Resolution: Ensure that the thread has acquired a write lock before calling this method."
                );
            }

            var cmdId = "xxx: TODO:";

            cmdCollection.AddP(cmdId, cmd);

            // TODO: Implement CmdHandlerP like RegHandlerP or not?
            // cmdHandler.TryAddPV(CmdHandlerP.CmdId, cmdId, out _); /// Ignore if fails, we have no guarantees here, it may be a <see cref="PExact{EnumType}"/> implementation for instance.

            // TODO: Use this or not?
            // TODO: Is RegId the correct type here?
            cmd.IP.SetPV(IKString.FromCache(cmdHandler.GetType().ToStringVeryShort() + "Id"), cmdId);

            /// TODO: Implement use of indexing here
            /// TODO: Use <see cref="IP.TrySetPP"/> for registerering all keys within regHandler.
            cmdHandlerCollection.AddP(cmdId, cmdHandler);

            // Note that the "distribute" part will not affect whether TRUE or FALSE is returned now.
            var ok = CmdProcessor.TryExecuteCmd(cmdId, cmd, out errorResponse);

            cmd.IP.SetPV(CmdP.CostTime, DateTime.UtcNow - timeBefore);
            
            if (!ok)
            {
                cmd.IP.SetPV(CmdP.CmdStatus, CmdStatus.ERROR);
                cmd.IP.SetPV(CmdP.ErrorResponse, errorResponse);

                errorResponse = "-" + nameof(CmdProcessor.TryExecuteCmd) + "-: " + errorResponse;
                return false;
            }

            cmd.IP.SetPV(CmdP.CmdStatus, CmdStatus.OK);
            return true;
        }

        public class CmdControllerException : ApplicationException
        {
            public CmdControllerException(string message) : base(message) { }
            public CmdControllerException(string message, Exception inner) : base(message, inner) { }

        }
    }
}
