﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using ARCCore;
using ARCDoc;
using ARCAPI;
using ARCQuery;
using System.Linq;

namespace ARCEvent
{

    [Class(Description =
        "Operates on the -" + nameof(Reg) + "- (Registration) side (the \"input\" side) of the API.\r\n" +
        "\r\n" +
        "Packs a given -" + nameof(IRegHandler) + "- into an -" + nameof(Reg) + "-\r\n" +
        "(for persisting in the -" + nameof(ARConcepts.RegStream) + "-) and sends it to -" + nameof(EventProcessor) + "- " +
        "for entering into the -" + nameof(ARConcepts.EventStream) + "-.\r\n" +
        "\r\n" +
        "You can use the ordinary -" + nameof(RQController) + "- for querying against the generated -" + nameof(Reg) + "- afterwards.\r\n" +
        "\r\n" +
        "TODO: Add JSON output."
    )]
    [Macro(
        Name = "01. Past",
        MacroType = MacroType.UnitTest,
        MacroTimeHint = MacroTimeHint.Past,
        RegStream = @"
DO/20DaysAgo/CNER/TCRed {id}
DO/20DaysAgo/SSPR/TCRed {id}/First/Apple
DO/19DaysAgo/SSPR/TCRed {id}/First/Orange
DO/18DaysAgo/SSPR/TCRed {id}/First/Banana
DO/Yesterday/SSPR/TCRed {id}/First/Rotten
// TODO: Currently unable to specify query at 'end-of-timeperiod'
// TODO: Instead we must query at start of next period.
DO/now/AssertR/NotExist/20DaysAgo//TCRed {id}
DO/now/AssertR/Equal/19DaysAgo//TCRed {id}/First/Apple
DO/now/AssertR/Equal/18DaysAgo//TCRed {id}/First/Orange
DO/now/AssertR/Equal/17DaysAgo//TCRed {id}/First/Banana
DO/now/AssertR/Equal/Today//TCRed {id}/First/Rotten
",
        QuerySuggestions = @"
// Tip: Scroll backwards and forwards in EventTime and RegTime in order to see how this entity was constructed
ES/dt/now/now/TCRed/{id}
"
    )]
    [Macro(
        Name = "02. Future",
        MacroType = MacroType.UnitTest,
        MacroTimeHint = MacroTimeHint.Future,
        RegStream = @"
DO/Tomorrow/CNER/TCRed {id}
DO/Tomorrow/SSPR/TCRed {id}/First/Apple
DO/In2Days/SSPR/TCRed {id}/First/Orange
DO/In3Days/SSPR/TCRed {id}/First/Banana
DO/In20Days/SSPR/TCRed {id}/First/Rotten
// TODO: Currently unable to specify query at 'end-of-timeperiod'
// TODO: Instead we must query at start of next period.
DO/now/AssertR/NotExist/Today//TCRed {id}
DO/now/AssertR/Equal/In2Days//TCRed {id}/First/Apple
DO/now/AssertR/Equal/In3Days//TCRed {id}/First/Orange
DO/now/AssertR/Equal/In4Days//TCRed {id}/First/Banana
DO/now/AssertR/Equal/In21Days//TCRed {id}/First/Rotten
",
        QuerySuggestions = @"
// Tip: Scroll backwards and forwards in EventTime and RegTime in order to see how this entity was constructed
ES/dt/now/now/TCRed/{id}
"
    )]
    [Macro(
        Name = "03. DO and UNDO",
        MacroType = MacroType.UnitTest,
        RegStream = @"
DO/now/CNER/TCRed Red_{id}
DO/now/SSPR/TCRed Red_{id}/First/Dark
DO/now/AssertR/Equal///TCRed Red_{id}/First/Dark

// Hack: UNDO Assertion before undoing First = Dark below, in order to avoid ERROR in Event log.
UNDO/{id+2}

UNDO/{id+1}
DO/now/AssertR/NotExist///TCRed Red_{id}/First

// Hack: UNDO Assertion before setting First = Light below, in order to avoid ERROR in Event log.
UNDO/{id+5}

DO/{id+1}/SSPR/TCRed Red_{id}/First/Light
DO/now/AssertR/Equal///TCRed Red_{id}/First/Light
",
        QuerySuggestions = @"
"
    )]
    public class RSController : BaseEventController
    {

        // NOTE / TODO: Due to bug (as of May 2020) in ClassMemberAttribute as used by documentation, do not add a ClassMemberAttribute here. Document with ClassAttribute instead
        public override ContentResult APIMethod(
            ResponseFormat format,
            int jsonDepth,
            List<IKCoded> request,
            string? postData,
            DataStorage dataStorage
        )
        {

            var timeBefore = DateTime.UtcNow;

            if (format == ResponseFormat.JSON)
            {
                throw new NotImplementedException(format.ToString() + " is not implemented (if machine readable response is not needed, just use HTML instead)");
            }

            if (format == ResponseFormat.HTML)
            {
                var htmlResponse = new Func<string>(() =>
                {
                    Reg reg;
                    IRegHandler regHandler;
                    RegTime regTime;
                    IP? dt = null;
                    EventCollection? eventCollection = null;
                    RegCollection? regCollection = null;
                    IP? regHandlerCollection = null;
                    try
                    {
                        dataStorage.Lock.EnterReadLock();
                        if (!TryParseRegistration(
                            dataStorage, 
                            request.Select(r => r.Unencoded.ToString()).ToList(), 
                            persistedRegTime: null,
                            out reg, out regHandler, out regTime, out var errorResponse, 
                            ref dt, ref eventCollection, ref regCollection, out regHandlerCollection
                        ))
                        {
                            return "<h1>ERROR</h1><p>-" +
                               nameof(TryParseRegistration) + "-: " +
                               WebUtility.HtmlEncode(errorResponse).Replace("\r\n", "<br>\r\n") +
                            "</p>";
                        }
                    }
                    finally
                    {
                        dataStorage.Lock.ExitReadLock();
                    }

                    if (dt == null || eventCollection == null || regCollection == null || regHandlerCollection == null)
                    {
                        throw new NullReferenceException("dt == null || eventCollection == null || regCollection == null || regHandlerCollection == null");
                    }

                    try
                    {
                        dataStorage.Lock.EnterWriteLock();
                        // TODO: Serialize to disk also

                        if (!TryStoreRegistrationAndStoreAndDistributeEvent(dataStorage, timeBefore, reg, regHandler, regTime, out var _event, out var errorResponse, eventCollection, regCollection, regHandlerCollection))
                        {
                            var retval =
                                "<h1>ERROR</h1>\r\n" +
                                "<p>-" +
                                   nameof(TryStoreRegistrationAndStoreAndDistributeEvent) + "-: " +
                                   WebUtility.HtmlEncode(errorResponse).Replace("\r\n", "<br>\r\n") +
                                "</p>" +

                                "<hr><p>Registration: <a href=\"/RQ/dt/Reg/" + regTime.SerialNumber + "\">" + regTime.SerialNumber + "</a></p>\r\n" +

                                reg.ToHTMLSimpleSingle(prepareForLinkInsertion: true, request);

                            return retval;
                        }

                        // NOTE: Generation of text below does not require a write lock against the database
                        // NOTE: A read lock would be sufficient.
                        // NOTE: In other words, we could release the write lock before creating text below.
                        // NOTE: The point is probably not very relevant when we introduce JSON responses (which will be most common)
                        // NOTE: because they will be much more simpler (just returning and id-number or two).
                        return
                              (
                                  "<h1>Registration OK</h1>\r\n" +
                                  "<p>" +
                                     "<a href=\"/Process/" + regHandler.GetType().ToStringVeryShort() + "/" + regTime.SerialNumber + "\">Edit</a>&nbsp;&nbsp;\r\n" +
                                     "Registration: <a href=\"/RQ/dt/Reg/" + regTime.SerialNumber + "\">" + regTime.SerialNumber + "</a>&nbsp;&nbsp;\r\n" +
                                     "Event: <a href=\"/RQ/dt/Event/" + regTime.SerialNumber + "\">" + regTime.SerialNumber + "</a>&nbsp;&nbsp;\r\n" +

                                  // This was a bit overkill as it is repated below by _event.ToHTMLSimpleSingle and reg.ToHTMLSimpleSingle
                                  //"All earlier edits:&nbsp;&nbsp;\r\n" +
                                  //"<a href=\"/RQ/dt/Reg/WHERE EventTime = '" + _event.EventTime.ToString() + "'/ORDER BY RegId DESC\">As Registrations</a>&nbsp;&nbsp;\r\n" +
                                  //"<a href=\"/RQ/dt/Event/WHERE EventTime = '" + _event.EventTime.ToString() + "'/ORDER BY RegId DESC\">As Events</a>&nbsp;&nbsp;\r\n" +

                                  "</p>\r\n" +

                                  // TODO: Delete commented out code
                                  // "<p style=\"color:red\">Note: Any links below do probably not work. Query with RQ instead of AS:</p>\r\n" +

                                  "<hr>\r\n"
                              ) +
                              _event.ToHTMLSimpleSingle(prepareForLinkInsertion: true, request) +
                              "<hr>\r\n" +
                              reg.ToHTMLSimpleSingle(prepareForLinkInsertion: true, request);
                    }
                    finally
                    {
                        dataStorage.Lock.ExitWriteLock();
                    }
                })();


                /// We are now outside of any locking context. 
                /// (the main reason for operating without locking now is that <see cref="DocLinkCollection.InsertLinks"/> is hugely expensive,
                /// but in general as much code as possible should of course be executed outside of a locking context)
                /// 
                // Any code following from here should NOT depend on data structures that may change.
                // (therefore any evaluations on changing datastructures have been prepared already in 'htmlResponse')

                var retval = htmlResponse;

                if (dataStorage.DocLinks != null)
                {
                    retval = dataStorage.DocLinks.InsertLinks(toDoc: "/RQ", contentHTML: retval);
                }

                return ContentResult.CreateHTMLOK(UtilDoc.GenerateHTMLPage(retval));
            }

            throw new InvalidEnumException(format);
        }

        [ClassMember(Description =
            "Convenience method, combining -" + nameof(TryParseRegistration) + "- and -" + nameof(TryStoreRegistrationAndStoreAndDistributeEvent) + "- " +
            "NOT recommended for use when high performance is needed due to " +
            "unnecessary long period of write lock.\r\n" +
            "\r\n" +
            "TODO: Maybe not implement after all."
        )]
        public bool TryParseAndStoreRegistrationAndStoreAndDistributeEvent()
        {
            throw new NotImplementedException();
        }

        /// <param name="persistedRegTime">Only relevant at application startup when reading persisted data from disk</param>
        [ClassMember(Description =
            "Attempts to parse the given request into an -" + nameof(Reg) + "- (Registration) object.\r\n" +
            "\r\n" +
            "Uses -" + nameof(IRegHandler) + "-.-" + nameof(IRegHandler.TryParse) + "-.\r\n" +
            "\r\n" +
            "TODO: Could probably be moved to the Reg-class instead.\r\n" +
            "\r\n" +
            "The convoluted syntax is due to need for high performance and also to avoid write-lock until as late as possible.\r\n" +
            "\r\n" +
            "Note use of ref-parameters in case calling method makes multiple calls.\r\n" +
            "\r\n" +
            "TODO: In order to support client with unreliable network connection:\r\n" +
            "TODO: Add some UID for unique identifications of request.\r\n" +
            "TODO: If found, then use the UID for looking up recent Action objects stored (see -" + nameof(TryStoreRegistrationAndStoreAndDistributeEvent) + "-) " +
            "TODO: and if found, use THAT Registration's serial number here.\r\n" +
            "TODO: DO NOT (preferable) store the UID permanently, due to memory concerns. " +
            "TOOD: Only store for some hours or similar (or store within application lifetime)."
        )]
        public static bool TryParseRegistration(
            DataStorage dataStorage,
            List<string> request,
            RegTime? persistedRegTime,
            out Reg reg,
            out IRegHandler regHandler, // Included in Reg, but makes for quicker access when separate
            out RegTime regTime,        // Included in Reg, but makes for quicker access when separate
            out string errorResponse,
            ref IP? dt,
            ref EventCollection? eventCollection,
            ref RegCollection? regCollection,
            out IP regHandlerCollection
        )
        {
            if (!(dataStorage.Lock.IsReadLockHeld || dataStorage.Lock.IsWriteLockHeld))
            {
                // Note: Requirement for read lock applies to the "whole" method, not only finding regCollection and eventCollection below
                // because we peek inside existing Registration object
                throw new RSControllerException(
                    "!!(dataStorage.Lock.IsReadLockHeld || dataStorage.Lock.IsWriteLockHeld).\r\n" +
                    "\r\n" +
                    "Resolution: Ensure that the thread has acquired a read lock before calling this method."
                );
            }

            if (dt == null || eventCollection == null || regCollection == null)
            {
                if (!dataStorage.Storage.TryGetP<IP>(IKString.FromCache(PSPrefix.dt.ToString()), out dt, out errorResponse))
                {
                    reg = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    regHandler = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    regTime = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    regHandlerCollection = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return false;
                }

                /// TOOD: Make configurable whether to store in Event collection afterwards.
                /// TODO: The collection is a "nice-to-have" thing, not essential to the functioning of <see cref="ARConcepts.EventSourcing"/>
                /// TODO: (however, it is essential for <see cref="ProcessController"/>
                /// TODO: Alternatively, create some mechanism for only keeping recent Events in the collection, for debug purposes.
                if (!dt.TryGetP<EventCollection>(IKType.FromType(typeof(Event)), out eventCollection, out errorResponse))
                {
                    reg = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    regHandler = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    regTime = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    regHandlerCollection = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return false;
                }

                /// TOOD: Make configurable whether to store in Registration collection afterwards.
                /// TODO: The collection is a "nice-to-have" thing, not essential to the functioning of <see cref="ARConcepts.EventSourcing"/>
                /// TODO: Alternatively, create some mechanism for only keeping recent Registrations in the collection, for debug purposes.
                if (!dt.TryGetP<RegCollection>(IKType.FromType(typeof(Reg)), out regCollection, out errorResponse))
                {
                    reg = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    regHandler = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    regTime = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    regHandlerCollection = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return false;
                }
            }

            if (request.Count < 2)
            {
                errorResponse = "Too few parameters (" + request.Count + "), at least 2 is required (RegVerb and EventTime).";
                reg = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                regHandler = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                regTime = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                regHandlerCollection = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }

            if (!UtilCore.EnumTryParse<RegVerb>(request[0], out var regVerb, out errorResponse))
            {
                reg = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                regHandler = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                regTime = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                regHandlerCollection = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }

            if (!IRegHandler.TryParse(regVerb, request.Skip(2).ToList(), out regHandler, out errorResponse))
            {
                errorResponse = "Invalid model. Details:\r\n" + errorResponse;
                reg = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                regTime = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                regHandlerCollection = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }

            if (!dt.TryGetP(IKType.FromType(regHandler.GetType()), out regHandlerCollection))
            {
                // TODO: Note exception here, but errorResponse for the other collections above. Make more similar.
                throw new DatastructureException(
                    "Collection for type " + regHandler.GetType() + " not initialized. Should have been defined at application startup.");
            }

            var strTime = request[1];

            // Ensure that all components involved in parsing have to same idea of what 'now' means
            var now = UtilCore.DateTimeNow;

            EventTime? eventTime = null;
            if (long.TryParse(strTime, out var lngSerialNumber))
            {
                // Look for existing Registration and use that one's EventTime
                if (!regCollection.TryGetP<Reg>(new IKLong(lngSerialNumber), out var existingReg, out errorResponse))
                {
                    errorResponse =
                        "Existing -" + nameof(Reg) + "- with serial number " + lngSerialNumber + " not found." +
                        "Unable to modify this -" + nameof(Reg) + "- (if it ever existed).\r\n" +
                        "If the -" + nameof(regTime.SerialNumber) + "- was very old " +
                        "this may be due to -" + nameof(Reg) + "-s being purged from memory.\r\n" +
                        "Details: " + errorResponse;
                    reg = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    regTime = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return false;
                }
                if (existingReg.IP.TryGetP<IRegHandler>(RegP.RH, out var existingRegHandler))
                {
                    if (existingRegHandler.GetType().Equals(typeof(EmptyRegHandlerR)) || regHandler.GetType().Equals(typeof(EmptyRegHandlerR)))
                    {
                        /// This would typically be <see cref="RegVerb.DO"/> replaced with <see cref="RegVerb.UNDO"/>, or the other way around.
                    }
                    else
                    {
                        var _newRegHandler = regHandler;
                        // TODO: Change this into setting errorResponse and returning FALSE.
                        InvalidObjectTypeException.AssertEquals(existingRegHandler, regHandler.GetType(), () =>
                            "If you want to modify the effect of an -" + nameof(Reg) + "- with another " +
                            "-" + nameof(RegVerb) + "- then the handlers must match (have the same type).\r\n" +
                            "\r\n" +
                            "The relevant registration handlers are:\r\n" +
                            "Existing: " + (existingRegHandler is IRegHandler r ? r.Serialize() : "[NOT OF TYPE IRegHandler]") + "\r\n" +
                            "New: " + regVerb + " " + _newRegHandler.Serialize()
                        );
                    }
                }
                regTime = persistedRegTime ?? new RegTime(UtilEvent.RoundDateTimeBackwards(now), EventProcessor.GetNextSerialNumber());
                eventTime = existingReg.EventTime;
            }
            else if (TryParseDateTime(now, strTime, out var dtmEventTime, out errorResponse))
            {
                regTime = persistedRegTime ?? new RegTime(UtilEvent.RoundDateTimeBackwards(now), EventProcessor.GetNextSerialNumber());
                eventTime = new EventTime(UtilEvent.RoundDateTimeBackwards(dtmEventTime), regTime.SerialNumber);
            }
            else
            {
                errorResponse =
                    "Invalid -" + nameof(EventTime) + "- specified. Use \"now\" for instance.\r\n" +
                    "Details (in case you tried to specify a -" + nameof(ValueComparerDateTime) + "-): " + errorResponse;
                reg = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                regTime = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }

            reg = new Reg();
            reg.IP.SetPV(RegP.RegTime, regTime);
            reg.IP.SetPV(RegP.EventTime, eventTime);
            reg.IP.SetPV(RegP.RegVerb, regVerb);

            // Removed 31 Jan 2022
            // reg.IP.SetPV(RegP.RegHandlerType, regHandler.GetType());

            // TODO: Reconsider if necessary now that we also link to the regHandler in its own collection.
            reg.IP.SetP(RegP.RH, regHandler);

            reg.IP.SetPV(RegP.RegStatus, RegStatus.NEW);

            return true;
        }

        [ClassMember(Description =
            "Attempts to store the given -" + nameof(Reg) + "- (Registratioin) and store and distribute the given -" + nameof(Event) + "- through " +
            "-" + nameof(EventProcessor.TryStoreAndDistributeEvent) + "-.\r\n" +
            "\r\n" +
            "Note use of -" + nameof(PCollectionES.TryStoreSnapshotDirect) + "- for Registrations residing in " +
            "-" + nameof(PCollectionES) + "- derived collecitons.\r\n"
        )]
        public static bool TryStoreRegistrationAndStoreAndDistributeEvent(
            DataStorage dataStorage,
            DateTime timeBefore,
            Reg reg,
            IRegHandler regHandler,
            RegTime regTime,
            out Event _event,
            out string errorResponse,
            IP eventCollection,
            IP regCollection,
            IP regHandlerCollection
        )
        {
            if (!dataStorage.Lock.IsWriteLockHeld)
            {
                throw new RSControllerException(
                    "!dataStorage.Lock.IsWriteLockHeld.\r\n" +
                    "\r\n" +
                    "Resolution: Ensure that the thread has acquired a write lock before calling this method."
                );
            }

            var tryStoreEventAccessCountBefore = PCollectionES.TryStoreEventAccessCount;
            var requestSnapshotCountBefore = PCollectionES.GlobalRequestSnapshotCount;
            var createNewSnapshotCountBefore = PCollectionES.GlobalCreateNewSnapshotCount;

            var regId = new RegId(regTime.SerialNumber);

            /// Note: On could believe that we do not need both an RegCollection and an EventCollection?                
            /// TODO: Technically the answer is probably yes (no, we do not need both), but practically it is no, 
            /// TODO: because each collection represent an 'axis' in the
            /// TODO: two dimensional <see cref="ARConcepts.EventSourcing"/> space and that distinction is nice to have.
            regCollection.AddP(regId, reg);

            regHandler.TryAddPV(RegHandlerP.RegId, regId, out _); /// Ignore if fails, we have no guarantees here, it may be a <see cref="PExact{EnumType}"/> implementation for instance.

            // TODO: Is RegId the correct type here?
            reg.IP.SetPV(IKString.FromCache(regHandler.GetType().ToStringVeryShort() + "Id"), regId);

            if (regHandlerCollection is PCollectionES)
            {
                /// This registration handler probably defines <see cref="IRegHandler.SubProcess"/> and
                /// therefore is stored within an <see cref="PCollectionES"/>. 
                /// We must wait with storing until corresponding event has been generated.
            }
            else
            {
                /// TODO: Implement use of indexing here
                /// TODO: Use <see cref="IP.TrySetPP"/> for registerering all keys within regHandler.
                regHandlerCollection.AddP(regId, regHandler);
            }


            // Note that the "distribute" part will not affect whether TRUE or FALSE is returned now.
            var ok = EventProcessor.TryStoreAndDistributeEvent(regId, reg, out _event, out errorResponse);

            reg.IP.SetPV(RegP.CostTime, DateTime.UtcNow - timeBefore);
            reg.IP.SetPV(RegP.CostTryStoreEvent, PCollectionES.TryStoreEventAccessCount - tryStoreEventAccessCountBefore);
            reg.IP.SetPV(RegP.CostRequestSnapshot, PCollectionES.GlobalRequestSnapshotCount - requestSnapshotCountBefore);
            reg.IP.SetPV(RegP.CostCreateSnapshot, PCollectionES.GlobalCreateNewSnapshotCount - createNewSnapshotCountBefore);

            if (!ok)
            {
                reg.IP.SetPV(RegP.RegStatus, RegStatus.ERROR);
                reg.IP.SetPV(RegP.ErrorResponse, errorResponse);

                errorResponse = "-" + nameof(EventProcessor.TryStoreAndDistributeEvent) + "-: " + errorResponse;
                return false;
            }

            if (!(regHandlerCollection is PCollectionES pCollectionES))
            {
                // OK, was stored above since stored within an ordinary PCollection or similar.
                // In other words, this is the "ordinary" case (or at least, the "simple" case)

                //throw new DatastructureException(
                //    "Collection for type " + regHandler.GetType() + " is of incorrect type " + regHandlerCollection.GetType().ToString() + " " +
                //    "(it should have been of type " + nameof(PCollectionES) + " or a sub type).");
            }
            else if (!pCollectionES.TryStoreSnapshotDirect(regId, _event, regHandler, out errorResponse))
            {
                // Maybe throw exception here instead?
                errorResponse = nameof(PCollectionES.TryStoreSnapshotDirect) + ": " + errorResponse;
                return false;
            }

            reg.IP.SetPV(RegP.RegStatus, RegStatus.OK);

            /// TOOD: Do we really need both an ActionCollection and an EventCollection?                
            /// TODO: The answer is probably no, but the ActionCollection is nice to have in order to distinguish between
            /// TODO: the two dimensions (EventTime and RegTime) in <see cref="ARConcepts.PropertyStream2D"/>

            var eventId = new EventId(regTime.SerialNumber);
            reg.IP.SetPV(RegP.EventId, eventId);
            /// Ignore if fails, we have no guarantees here, it may be a <see cref="PExact{EnumType}"/> implementation for instance.
            regHandler.TryAddPV(RegHandlerP.EventId, eventId, out _);
            eventCollection.AddP(eventId, _event);

            return true;
        }


        [ClassMember(Description = "TODO: Find some better initialization here.")]
        public static EventProcessor EventProcessor { get; set; } = null!;

        public class RSControllerException : ApplicationException
        {
            public RSControllerException(string message) : base(message) { }
            public RSControllerException(string message, Exception inner) : base(message, inner) { }

        }
    }
}
