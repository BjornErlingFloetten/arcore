﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using ARCCore;
using ARCDoc;
using ARCAPI;
using System.Linq;

namespace ARCEvent
{

    [Class(Description =
        "Presents an auto generated user interface."
    )]
    public class UIController : BaseController
    {        
        [ClassMember(Description =
            "HTML which should be placed on the top of every page that this application returns"
        )]
        public static string SearchComponentHTML = @"
<script>
    async function search(searchType, searchCriteria)
    {

        if (searchCriteria == '') {
            document.getElementById('searchResult').innerHTML = '';
            return;
        }

        document.getElementById('searchResult').innerHTML = '<p>' + searchType + ' searching...</p>';
        (await fetch('https://' + document.location.host + '/UI/' + searchType + '/' + searchCriteria)).text().then(data => {
            var apiResponseParsed = new DOMParser().parseFromString(data, 'text/html');
            document.getElementById('searchResult').innerHTML = apiResponseParsed.getElementById('result').innerHTML;
        });
    } 
</script>
<p>

<span title=""Quick search should work well for both moderately sized datasets and also for huge datasets"">
<b><u>Q</u></b>uick search: <input name=""searchCriteriaQuick"" id=""searchCriteriaQuick"" type=""text"", 
placeholder=""<Quick search>"" accesskey=""Q"" 
onkeyup=""search('Quick', document.getElementById('searchCriteriaQuick').value);"">
</span>

&nbsp;&nbsp;

<span title=""Full search is very time consuming for moderately sized datasets (and maybe even small datasets) due to algorithm being O(n^4) or similar."">
Ful<b><u>l</u></b> search: <input name=""searchCriteriaFull"" id=""searchCriteriaFull"" type=""text"", 
tabindex=""1000000"" 
placeholder=""<Full search>"" accesskey=""L"" 
onkeyup=""search('Full', document.getElementById('searchCriteriaFull').value);"">
</span>

<span title=""Documentation search is limited to documentation only."">
Do<b><u>c</u></b>umentation search: <input name=""searchCriteriaDoc"" id=""searchCriteriaDoc"" type=""text"", 
tabindex=""1000001"" 
placeholder=""<Doc search>"" accesskey=""C"" 
onkeyup=""search('Doc', document.getElementById('searchCriteriaDoc').value);"">
</span>

</p>
<label name=""searchResult"" id=""searchResult""></label>
<hr>

";

        [ClassMember(Description =
            "This can be turned into separate classes UIComponent and UIComponentCollection if the need arises.\r\n" +
            "\r\n +" +
            "Is implicitly understood to contain a string based key in LOWER CASE and a PValue<string> value containing HTML code."
        )]
        public static PRich UIComponents { get; set; } = new PRich();

        public static void AddUIComponent(string key, string value) => AddUIComponent(new IKIP(IKString.FromCache(key), new PValue<string>(value)));

        public static void AddUIComponent(IKIP ikip) => UIComponents.IP.AddP(ikip);

        [ClassMember(Description = "Adds standard UI components")]
        public static void AddStandardUIComponents(IP dataStorage)
        {
            var a = new Action<string, string>((key, value) => UIController.AddUIComponent(key.ToLower(), value));

            a("start process", "<p><a href=\"/Process/\">Process (auto generated user interface)</a></p>");

            UtilCore.EnumGetMembers<MacroType>().ForEach(t =>
                a(
                    "Run Macros of type " + t,
                    "<p>Run Macros of type <a href=\"/RS/DO/now/RunMacroR//" + t + "\">" + t + "</a>&nbsp;(only " +
                    string.Join("", UtilCore.EnumGetMembers<MacroTimeHint>().Select(h =>
                        "<a href=\"/RS/DO/now/RunMacroR//" + t + "/" + h + "\">" + h + "</a>&nbsp;"
                    )) +
                    ")</p>\r\n"
                )
            );

            UtilCore.EnumGetMembers<MacroType>().ForEach(t =>
            {
                a(
                    "View Macros of type " + t,
                    "<p>View Macros of type <a href=\"/RQ/dt/" +
                    "MacroAttribute/WHERE MacroType = '" + t + "'/" +
                    "SELECT AssemblyName, ClassType, Name, MacroTimeHint, MacroStatus, SerialNumberAtStart/" +
                    "ORDER BY MacroAttributeId/" +
                    "TITLE Macros of type " + t + "\">" + t + "</a>" +
                    "</p>\r\n"
                );
                a(
                    "View Macros of type " + t + " with ERROR condition",
                    "<p>View Macros of type <a href=\"/RQ/dt/" +
                    "MacroAttribute/WHERE MacroType = '" + t + "'/WHERE MacroStatus = 'ERROR'/" +
                    "SELECT AssemblyName, ClassType, Name, MacroTimeHint, MacroStatus, SerialNumberAtStart/" +
                    "ORDER BY MacroAttributeId/" +
                    "TITLE Macros of type " + t + " with ERROR condition\">" + t + "</a> " +
                    "with 'ERROR' condition" +
                    "</p>\r\n"
                );
            });

            // TODO: Use literal string prefix (@), and insert slash ('/') when generating HTML
            // TODO: This will enable copy paste of queries directly between C# code and web browser.
            new List<(string url, string text)> {

                (
                    "MacroAttribute/SELECT AssemblyName0x002B'_'0x002BClassType AS Class, MacroType/" +
                    "PIVOT Class BY MacroType/ORDER BY UnitTest DESC/" +
                    "TITLE Macro coverage (unit test coverage)",
                    "Macro coverage (unit test coverage) (count)"
                ),

                (
                    "MacroAttribute/SELECT AssemblyName0x002B'_'0x002BClassType AS Class, MacroType, LOC/" +
                    "PIVOT Class BY MacroType SUM LOC/" +
                    "SELECT *.Int() AS */" +
                    "ORDER BY UnitTest DESC/" +
                    "TITLE Macro coverage (unit test coverage)",
                    "Macro coverage (unit test coverage) (LOC)"
                ),

                (
                    "MacroExecutionResult/SELECT */" +
                    "TITLE MacroExecutionResult",
                    "MacroExecutionResult"
                ),

                /// NOTE: Note that ordering by EventTime does not work. It is currently a string-ordering.
                /// NOTE: This would not matter much in a production system where the serial numbers will mostly
                /// NOTE: have the same number of digits, but for developers and testing it is confusing because
                /// NOTE: going from 1 to 2 digits, from 2 to 3 and so on, messes up the sorting.
                /// TODO: Check if failure is due to <see cref="ARCQuery.QueryExpressionOrderBy"/> (always using string) or 
                /// TODO: <see cref="EventTime"/> missing a IComparer implementation (probably the first cause is the real one).
                /// NOTE: This is not important, we just use <see cref="Event.TryGetEventTimeSortable"/> instead.
                /// 
                /// NOTE: Note that ordering by EventTimeTime would not have been optimal either, because it would not distinguish 
                /// NOTE: between events with same EventTime.

                (
                    "Event/WHERE EventTimeTime = LastYearG/" +
                    "SELECT RegVerb, RegHandler, EventTimeTime.Short() AS Et, RegTimeTime.Short() AS Rt, ReplacedRegTime, EventStatus AS s, " +
                    "ErrorResponse AS er, EventTimeSortable/ORDER BY EventTimeSortable DESC/" +
                    "TITLE Events log, root events only (not including transient events)",
                    "Events log, root events only (not including transient events)."
                ),

                (
                    /// Note filter against ReplacedRegTime because it will be quite natural for (later) events to end up in an ERROR state
                    /// when retroactive changes are being made to the <see cref="ARConcepts.EventStream"/>
                    /// and we want this query to be a query checking for exceptional circumstances, not "normal" occurrences.
                    "Event/WHERE EventTimeTime = LastYearG/" +
                    "TRANSIENT/WHERE ReplacedRegTime EQ NULL/WHERE EventStatus = 'ERROR'/" +
                    "SELECT RegId, RegVerb, RegHandler, EventTimeTime.Short() AS Et, RegTimeTime.Short() AS Rt, ReplacedRegTime, EventStatus AS s, " +
                    "ErrorResponse AS er, EventTimeSortable/ORDER BY EventTimeSortable DESC/THEN BY EventId DESC/" +
                    "TITLE Events log with status 'ERROR', including transient events.",
                    "Events log with status 'ERROR', including transient events (no result is often expected)."
                ),

                (
                    "Reg/WHERE RegTimeTime = LastYearG/" +
                    "SELECT RegVerb AS at, RegHandler, EventTimeTime.Short() AS Et, RegTimeTime.Short() AS Rt, RegStatus AS s, ErrorResponse AS er, " +
                    "CostTime, CostTryStoreEvent AS CostSE, CostRequestSnapshot AS CostRS, CostCreateSnapshot AS CostCS/ORDER BY RegId DESC/" +
                    "TITLE Reg (Registrations) log",
                    "Reg (Registrations) log"
                ),

                (
                    "Reg/WHERE RegTimeTime = LastYearG/" +
                    "WHERE RegStatus = 'ERROR'/SELECT RegVerb AS rt, RegHandler, EventTimeTime.Short() AS Et, RegTimeTime.Short() AS Rt, RegStatus AS s, ErrorResponse AS er, " +
                    "CostTime, CostTryStoreEvent AS CostSE, CostRequestSnapshot AS CostRS, CostCreateSnapshot AS CostCS/ORDER BY RegId DESC/" +
                    "TITLE Reg (Registrations) log with status 'ERROR'",
                    "Reg (Registrations) log with status 'ERROR'"
                ),

                (
                    "SELFCREATE PCollectionESStat/" +
                    "SELECT RequestSnapshotCount AS RSC, CreateNewSnapshotCount AS CNSC, EventsDictCount AS CountE, SnapshotsDictCount AS CountS, " +
                    "EventsTotalCount AS Events, SnapshotsTotalCount AS Snapshots, " +
                    "EventsPerSnapshot.Int() AS EPS, EventsInvalidCount AS EIC, EventsValidCount AS EVC, ValidEventsPerSnapshot.Int() AS VEPS/" +
                    "ORDER BY PCollectionESStatId/" +
                    "TITLE PCollectionESStat (Statistics for all instances of PCollectionES, snapshots and events)",
                    "PCollectionESStat (Statistics for all instances of PCollectionES, snapshots and events).\r\n" +
                    "\r\n" +
                    ARCQuery.QueryExpressionSelect.UrlSegmentMaxLengthWarning
                ),

                (
                    "Process/SELECT SchemaId, EntityType, EntityKey, SubProcess/ORDER BY ProcessId DESC/" +
                    "TITLE All process instances (historic and ongoing). Use /UI/ to start new process.",
                    "All process instances (historic and ongoing). Use /UI/ to start new process."
                ),

                (
                    url:
                        "Event/WHERE EventTimeTime = LastYearG/" +
                        // TODO: Since SSPRT is a generic class, we can not use 'TRANSIENT SSPRT' here
                        // TODO: This is an unnecessary limitation, performance would be better if we could do it.
                        "TRANSIENT/" +
                        "WHERE RH.Type().VeryShort() = 'SSPRT'/" +
                        // TODO: Consider doing ORDER BY and TAKE, before constructing query content below
                        // TODO: in order not to vaste the calculated values.
                        "SELECT EventIdFromEventTime AS EventId, RegVerb, EventTimeTime.Short() AS Et, RegTimeTime.Short() AS Rt, " +
                        "RH.EntityType, RH.EntityKey, RH.PropertyKey, RH.Value, " +
                        "EventStatus AS s, ErrorResponse AS er, EventTimeSortable/" +
                        "ORDER BY EventTimeSortable DESC/THEN BY EventId DESC/" +
                        "TITLE All SSPRT log",
                    text:
                        "All SSPRT log (TODO: Currently not including SSPR)"
                )
            }.ForEach(tuple =>
                a(
                    "view report " + tuple.text,
                    // From 23 Dec 2021, attempt to run without table, only show links
                    "<p>View <a href=\"/RQ/" + nameof(PSPrefix.dt) + "/" + tuple.url + "\">" +
                    System.Net.WebUtility.HtmlEncode(tuple.text) +
                    "</a></p>\r\n"
                )
            );

            IRegHandler.AllIRegHandlerDerivedTypesTypeDict.ForEach(e =>
                a(
                   "Start process " + e.Value + " " + e.Key.ToStringVeryShort(),
                   "<p>Start process <a href=\"/Process/" + e.Value + "\">" + e.Key.ToStringVeryShort() + "</a>" +
                   (!ClassAttribute.GetAttribute(e.Key).IP.TryGetPV<string>(BaseAttributeP.Description, out var d) ? "" :
                       // TODO: Add Span for rest of text. Already done in Documentator?
                       // TODO: Consider insertings links here (if not takes too long time)
                       ("&nbsp;&nbsp;" + WebUtility.HtmlEncode(d[..Math.Min(d.Length, 100)]))
                   ) + "</p>\r\n"
                )
            );

            new List<(string url, string text)> {
                (nameof(PSPrefix.doc),"AgoRapide (ARCore) documentation"),
                (nameof(PSPrefix.app),"Internal application state")
            }.ForEach(tuple =>
                a(
                    "view" + tuple.text,
                    "<p>View <a href=\"/RQ/" + System.Net.WebUtility.UrlEncode(tuple.url) + "\">" +
                    System.Net.WebUtility.HtmlEncode(tuple.text) + "</a></p>\r\n"
                )
            );

            if (dataStorage.TryGetP<IP>(nameof(PSPrefix.dt), out var dt))
            {
                dt.ForEach(ikip =>
                    a(
                        "view all entity entities of type " + ikip.Key.ToString(),
                        "<p>" +
                            "View all <a href=\"/RQ/" + nameof(PSPrefix.dt) + "/" + System.Net.WebUtility.UrlEncode(ikip.Key.ToString()) + "/All/" +
                            "TITLE All " + ikip.Key.ToString() + "\">" +
                            System.Net.WebUtility.HtmlEncode(ikip.Key.ToString()) +
                            "</a>" +
                            "&nbsp;&nbsp;" +
                            "<a href=\"/RQ/" + nameof(PSPrefix.dt) + "/" + System.Net.WebUtility.UrlEncode(ikip.Key.ToString()) + "/SELECT */" +
                            "TITLE All " + ikip.Key.ToString() + " (including EntityMethodKeys)\">" +
                            "Including EntityMethodKeys" +
                            "</a> (see -" + nameof(ARCQuery.EntityMethodKey) + "-)" +
                        "</p>\r\n"
                    )
                 );
            }

            // This is tempting but not practical as of Dec 2021 (too many results are returned)
            //dataStorage[nameof(PSPrefix.doc)][nameof(DocFrag)].ForEach(ikip =>
            //    a(
            //        "view documentation fragment " + ikip.Key.ToString(),
            //        "<p>View documentation fragment <a href=\"/RQ/" + nameof(PSPrefix.dt) + "/doc/" + System.Net.WebUtility.UrlEncode(ikip.Key.ToString()) + "/All\">" +
            //        System.Net.WebUtility.HtmlEncode(ikip.Key.ToString()) +
            //        "</a><p>\r\n"
            //    )
            // );
        }

        // NOTE / TODO: Due to bug (as of May 2020) in ClassMemberAttribute as used by documentation, do not add a ClassMemberAttribute here. Document with ClassAttribute instead
        public override ContentResult APIMethod(
            ResponseFormat format,
            int jsonDepth,
            List<IKCoded> request,
            string? postData,
            DataStorage dataStorage
        )
        {
            // Note that only HTML is supported currently.
            // TODO: Consider offering JSON also, or some combination of JSON, HTML and Javascript.
            var responseCreator = new Func<string, ContentResult>(s =>
            {
                /// Link insertion is not done because it is assumed that the <see cref="UIComponent"/>s 
                /// found contain all necessary links.
                //// Note: Insertion of links is quite resource heavy.
                //return new ContentResult(System.Net.HttpStatusCode.OK, "text/html",
                //    dataStorage.DocLinks?.
                //        InsertLinks(toDoc: string.Join("", Enumerable.Repeat("../", request.Count + 1)) + "RQ", UtilDoc.GenerateHTMLPage(s)) ??
                //        UtilDoc.GenerateHTMLPage(s)
                //);

                return new ContentResult(System.Net.HttpStatusCode.OK, "text/html",
                    UtilDoc.GenerateHTMLPage("<span id=\"result\">" + s + "</span>")
                );
            });

            if (request.Count != 2)
            {
                return responseCreator("<p>Two parameters needed (not " + request.Count + "): Search type ('Quick' / 'Full' / 'Doc') and Search criteria</p>");
            }

            var searchType = request[0].ToString();

            if (string.IsNullOrEmpty(request[1].ToString()))
            {
                return responseCreator("<p>Exactly 1 search criteria not given</p>");
            }

            var searchCriteria = request[1].Unencoded.ToString().ToLower().Split(" ").ToList();

            var isAll = searchCriteria.Count == 1 && (
                "*".Equals(searchCriteria[0]) ||
                "all".Equals(searchCriteria[0]) ||
                "help".Equals(searchCriteria[0])
            );

            try
            {
                dataStorage.Lock.EnterReadLock();

                List<IKIP>? result;

                if (
                    "Doc".Equals(searchType) &&
                    dataStorage.Storage.TryGetP<IP>(PSPrefix.doc.ToString(), out var doc) &&
                    doc.TryGetP<DocFragCollection>(IKType.FromType(typeof(DocFrag)), out var docFragCollection)
                )
                {
                    result = new List<IKIP>();

                    foreach (var isMatch in new List<Func<IKIP, string, bool>>
                    {
                        (ikipDocFrag, s) => // Priority one, fragments where key mathces
                            ikipDocFrag.Key.ToString().ToLower().Contains(s),
                        (ikipDocFrag, s) => // Priority two, text matches (these will show up last in result list)
                            ikipDocFrag.P.TryGetPV<string>(BaseAttributeP.Description, out var description) &&
                            description.ToLower().Contains(s)
                    })
                    {
                        foreach (var ikipDocFrag in docFragCollection)
                        {
                            if (searchCriteria.All(s => isMatch(ikipDocFrag, s)))
                            {
                                result.Add(new IKIP(
                                    ikipDocFrag.Key, // TODO: This does not work with "moreWords" below
                                    new PValue<string>(
                                        "<p><a " +
                                            "href=\"/RQ/doc/DocFrag/" + IKCoded.FromUnencoded(ikipDocFrag.Key) + "\">" +
                                            WebUtility.HtmlEncode(ikipDocFrag.Key.ToString()) + "</a>" +
                                            (!ikipDocFrag.P.TryGetPV<string>(BaseAttributeP.Description, out var description) ? "" :
                                                // TODO: Inefficient code, use string.IndexOf("\r\n").Use(int => ...) instead.
                                                ("&nbsp;&nbsp;" + WebUtility.HtmlEncode(description.Split("\r\n")[0]))
                                            ) +
                                        "</p>\r\n"
                                    )
                                ));
                                if (result.Count > 20)
                                {
                                    goto break_full_search;
                                }
                            }
                        }
                    }

                    goto break_full_search;
                }

                result = isAll ?
                    UIComponents.ToList() :
                    UIComponents.Where(ikip => searchCriteria.All(s => ikip.Key.ToString().Contains(s))).ToList();

                var searchCriteriaIndex1OriginalCase = searchCriteria.Count < 2 ? "" :
                    request[1].Unencoded.ToString().Split(" ")[1];

                if (searchCriteria.Count == 2 &&
                    (searchCriteria[0]) switch   // TODO: Make this smarter, user would expect to be able to write "start process" for instance
                    {
                        "p" => true,
                        "pr" => true,
                        "pro" => true,
                        "proc" => true,
                        "proce" => true,
                        "proces" => true,
                        "process" => true,
                        _ => false,
                    } &&
                    dataStorage.Storage.TryGetP<IP>(PSPrefix.dt.ToString(), out var dt) &&
                    dt.TryGetP<SchemaCollection>(IKType.FromType(typeof(Schema)), out var schemaCollection)
                )
                {
                    // Look for schema containing search criteria in schema id (schema name?)
                    // Note that this is a relatively costly operation compared to lookup in an alreday
                    // created dictionary or similar, but the issue is that schemas may change over time
                    // (There are of course ways of caching this if this should ever become a problem)
                    foreach (var ikip in schemaCollection)
                    {
                        // This will query in the "now" sense of EventTime.
                        if (
                            ikip.Key.ToString().ToLower().Contains(searchCriteria[1]) ||
                            ikip.P.GetPV<string>(SchemaP.Name, "").ToLower().Contains(searchCriteria[1]))
                        {
                            result.Add(new IKIP(
                                ikip.Key,
                                new PValue<string>(
                                    "<p>Start Schema based process " +
                                        "<a href=\"/Process/" + IKCoded.FromUnencoded(ikip.Key) + "\">" +
                                            WebUtility.HtmlEncode(ikip.Key.ToString()) +
                                        "</a>" +
                                        (!ikip.P.TryGetPV<string>("Name", out var name) ? "" :
                                            ("&nbsp;" + WebUtility.HtmlEncode(name))
                                        ) +
                                        (!ikip.P.TryGetPV<List<string>>(SchemaP.SubProcessType, out var subProcessTypes) ? "" :
                                            ("&nbsp;With sub processes " + string.Join(", ", subProcessTypes.Select(p => WebUtility.HtmlEncode(p))))
                                        ) +
                                    "</p>\r\n"
                                )
                            ));

                        }
                    }
                }

                // Check if search for specific entity. If this is the case, offer lookup through RQ and also ES if relevant
                // NOTE: This is very finicky, requiring exact case
                // NOTE: See Search type "Full" below which is probably much better to use
                if (
                    searchCriteria.Count == 2 &&
                    IP.AllIPDerivedTypesDict.TryGetValue(searchCriteria[0], out var type) &&
                    dataStorage.Storage.TryGetP<IP>(PSPrefix.dt.ToString(), out dt) &&
                    dt.TryGetP<IP>(IKType.FromType(type), out var collection) &&
                    collection.TryGetP<IP>(searchCriteriaIndex1OriginalCase, out var entity) // Restore original case of key
                )
                {
                    var strType = type.ToStringVeryShort();
                    result.Add(new IKIP(
                        IKString.FromString(searchCriteria[0]),
                        new PValue<string>(
                            "<p><a " +
                                "href=\"/RQ/dt/" + strType + "/" + IKCoded.FromUnencoded(IKString.FromString(searchCriteriaIndex1OriginalCase)) + "\">" +
                                "RQ/dt/" + strType + "/" + searchCriteriaIndex1OriginalCase + "</a>" +
                            (!entity.TryGetPV<string>("Name", out var name) ? "" :
                                ("&nbsp;" + WebUtility.HtmlEncode(name)
                            ) +
                            "</p>\r\n"
                        )
                    )));

                    // TODO: Consider removing this, it is often quite easy to navigate between RQ / ES view
                    if (collection is PCollectionES) /// This entity can also be requested as a <see cref="Snapshot"/>
                    {
                        result.Add(new IKIP(
                            IKString.FromString(searchCriteria[0]),
                            new PValue<string>(
                                "<p><a " +
                                    "href=\"/ES/dt/now/now/" + strType + "/" + IKCoded.FromUnencoded(IKString.FromString(searchCriteriaIndex1OriginalCase)) + "\">" +
                                    "ES/dt/now/now/" + strType + "/" + WebUtility.HtmlEncode(searchCriteriaIndex1OriginalCase) + "</a> (snapshot)" +
                                (!entity.TryGetPV<string>("Name", out name) ? "" :
                                    ("&nbsp;&nbsp;" + WebUtility.HtmlEncode(name)
                                ) +
                                "</p>\r\n"
                            )
                        )));
                    }
                }

                if (
                    "Full".Equals(searchType) &&
                    dataStorage.Storage.TryGetP<IP>(PSPrefix.dt.ToString(), out dt)
                )
                {
                    // Do full search against ALL entities in data storage
                    // (this is very time consuming, O(n^4)
                    foreach (var ikipCollection in dt)
                    {
                        foreach (var ikipEntity in ikipCollection.P)
                        {
                            if (searchCriteria.All(s =>
                                ikipEntity.Key.ToString().ToLower().Contains(s) ||
                                ikipEntity.P.Any(ikipProperty => ikipProperty.P.GetV<string>(defaultValue: "").ToLower().Contains(s))
                            ))
                            {
                                result.Add(new IKIP(
                                    ikipEntity.Key, // TODO: This does not work with "moreWords" below
                                    new PValue<string>(
                                        "<p><a " +
                                            "href=\"/RQ/dt/" + IKCoded.FromUnencoded(ikipCollection.Key) + "/" + IKCoded.FromUnencoded(ikipEntity.Key) + "\">" +
                                            "RQ/dt/" + WebUtility.HtmlEncode(ikipCollection.Key.ToString()) + "/" + WebUtility.HtmlEncode(ikipEntity.Key.ToString()) + "</a>" +
                                        (!ikipEntity.P.TryGetPV<string>("Name", out var name) ? "" :
                                            ("&nbsp;" + WebUtility.HtmlEncode(name)
                                        ) +
                                        "</p>\r\n"
                                    )
                                )));
                                if (result.Count > 20)
                                {
                                    goto break_full_search;
                                }
                            }
                        }
                    }
                }
            break_full_search:

                if (result.Count == 0)
                {
                    return responseCreator("<p>[NO RESULT. Use '*' to see all keywords]</p>\r\n");
                }

                var moreWords = (isAll ?
                    UIComponents :
                    UIComponents.Where(ikip => searchCriteria.All(s => ikip.Key.ToString().Contains(s)))
                ).SelectMany(ikip =>
                     ikip.Key.ToString().Split(" ")
                ).ToHashSet().Where(s => !searchCriteria.Any(c => c.Equals(s))).OrderBy(s => s).ToList();

                // TODO: Add from recent Registrations, Events, Entities that may match and so on and so on.
                // TODO: Create "outside" defined getter for doing application specific searches.
                return responseCreator(
                    (result.Count <= 12 ? "" : ("<p>Showing 12 of " + result.Count + " results</p>")) +
                    (moreWords.Count == 0 ? "" : ("<p>You may narrow down to: " + WebUtility.HtmlEncode(string.Join(", ", moreWords)) + "</p>")) +
                    string.Join("", result.Take(12).Select(ikip => ikip.P.GetV<string>(defaultValue: "<p>[NO VALUE FOUND]</p>")))
                );
            }
            catch (Exception ex)
            {
                return responseCreator("<p>" + WebUtility.HtmlEncode(UtilCore.GetExeptionDetails(ex)).Replace("\r\n", "<br>\r\n"));
            }
            finally
            {
                dataStorage.Lock.ExitReadLock();
            }
        }
    }

    // This was overkill, we only need one single static PRich for this

    //[Class(Description =
    //    "Describes some URL component that may be starting point for some user Registration based on a general search.\r\n" +
    //    "\r\n" +
    //    "See also -" + nameof(UIComponentP) + "-."
    //)]
    //public class UIComponent : PRich
    //{

    //    public static UIComponent Create(string name, string html)
    //    {
    //        var retval = new UIComponent();
    //        if (
    //            !retval.IP.TrySetPV(UIComponentP.Name, name, out var errorResponse) ||
    //            !retval.IP.TrySetPV(UIComponentP.HTML, html, out errorResponse)
    //        )
    //        {
    //            throw new ArgumentException(errorResponse);
    //        }
    //        return retval;
    //    }
    //}

    //public class UIComponentCollection : PCollection
    //{

    //}

    //[Enum(
    //    Description =
    //        "Describes class -" + nameof(UIComponent) + "-.\r\n" +
    //        "\r\n",
    //    AREnumType = AREnumType.PropertyKeyEnum
    //)]
    //public enum UIComponentP
    //{
    //    __invalid,

    //    Name,

    //    [PKType(Description =
    //        "The HTML code that constitutes this component. " +
    //        "\r\n" +
    //        "This should be very short, ideally one or two lines of text with a couple of HTML links maximum.\r\n" +
    //        "\r\n"
    //    )]
    //    HTML
    //}
}
