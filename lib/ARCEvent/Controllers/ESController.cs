﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Net;
using ARCCore;
using ARCAPI;
using ARCDoc;
using ARCQuery;

namespace ARCEvent
{
    [Class(Description =
        "Operates on the -" + nameof(Event) + "- side (the \"output\" side) of the API.\r\n" +
        "\r\n" +
        "Is similar to the -" + nameof(RQController) + "- but supports also querying with -" + nameof(EventTime) + "- and -" + nameof(RegTime) + "-.\r\n"
    )]
    public class ESController : BaseEventController
    {

        // NOTE / TODO: Due to bug (as of May 2020) in ClassMemberAttribute as used by documentation, do not add a ClassMemberAttribute here. Document with ClassAttribute instead
        public override ContentResult APIMethod(
            ResponseFormat format,
            int jsonDepth,
            List<IKCoded> request,
            string? postData,
            DataStorage dataStorage
        )
        {

            if (format == ResponseFormat.JSON)
            {
                // Note how JSON is treated in a much simpler manner than HTML 
                try
                {
                    dataStorage.Lock.EnterReadLock();
                    return
                        TryGetEntity(dataStorage, request, out var tuple, out var errorResponseHTML) ?
                        ContentResult.CreateJSONOK(jsonDepth, tuple.snapshot) :
                        ContentResult.CreateError(format, errorResponseHTML);
                }
                finally
                {
                    dataStorage.Lock.ExitReadLock();
                }
            }

            if (format == ResponseFormat.HTML)
            {
#pragma warning disable IDE0042 // Deconstruct variable declaration
                var htmlResponse = new Func<(Type? resultType, string html)>(() =>
#pragma warning restore IDE0042 // Deconstruct variable declaration
                {
                    try
                    {
                        dataStorage.Lock.EnterReadLock();
                        if (!TryGetEntity(dataStorage, request, out var tuple, out var errorResponseHTML))
                        {
                            /// TODO: Create more elaborate error pages
                            /// Note that this is somewhat confusing, error response returned now will be sent to <see cref="ContentResult.CreateHTMLOK"/>
                            return (null, errorResponseHTML);
                        }

                        return (
                            tuple.snapshot.Entity.GetType(),

                            /// Extension method belongs here: <see cref="ARCDoc.Extensions.ToHTMLSimpleSingle"/>
                            /// NOTE: Extension method uses reflection in order to call actually implemented method in class if present.
                            tuple.snapshot.ToHTMLSimpleSingle(prepareForLinkInsertion: true, tuple.linkContext) +

                            // NOTE: Not relevant as of Dec 2021, maybe relevant when persisting of data has been added
                            GetTimestampIsOldWarningHTML(dataStorage.Storage)
                        );
                    }
                    finally
                    {
                        dataStorage.Lock.ExitReadLock();
                    }
                })();

                /// We are now outside of any locking context. 
                /// (the main reason for operating without locking now is that <see cref="DocLinkCollection.InsertLinks"/> is hugely expensive,
                /// but in general as much code as possible should of course be executed outside of a locking context)
                /// 
                // Any code following from here should NOT depend on data structures that may change.
                // (therefore any evaluations on changing datastructures have been prepared already in 'htmlResponse')

                var retval = htmlResponse.html;

                if (dataStorage.DocLinks != null)
                {
                    if (retval.Length > 20000)
                    {
                        retval =
                            "<p>Note: Documentation links have not been inserted due to document being longer than 20000 characters in length</p>\r\n" +
                            retval;
                    }
                    else
                    {
                        retval = dataStorage.DocLinks.InsertLinks(toDoc: "/RQ", contentHTML: retval);
                    }
                }

                // TODO: Probably fixed on 1 Jan 2022.
                //retval =
                //    "<p style=\"color:red\">Note: Some of the links below do probably not work. Query with RQ instead of ES for correct link insertion:</p>\r\n" +
                //    retval;

                // Insert link suggestions to the related entity types if relevant
                if (
                    htmlResponse.resultType != null &&
                    ForeignKey.IPRelationsKeysPointingTo.TryGetPV<List<ForeignKey.EntityTypeAndKey>>(htmlResponse.resultType.ToStringVeryShort(), out var relations)
                )
                {
                    retval =
                        (
                            "<p>See related " +
                            string.Join(" ", relations.Select(r =>  // Create something like '../Order/WHERE Customerid = 42'
                                "<a href=\"/RQ/dt/" +
                                WebUtility.UrlEncode(r.EntityType.ToStringVeryShort()) + "/" +
                                "WHERE " +
                                    WebUtility.UrlEncode(r.Key.ToString()) + " = " +
                                    "'" + request[4].Encoded + "'" +
                                "\">" + WebUtility.HtmlEncode(r.OppositeTerm) +
                                "</a>")
                            ) +
                            "</p>\r\n"
                        ) +
                        retval;
                }

                return ContentResult.CreateHTMLOK(UtilDoc.GenerateHTMLPage(retval));
            }

            throw new InvalidEnumException(format);
        }

        [ClassMember(Description =
            "Similar to -" + nameof(TryNavigateToLevelInDataStorage) + "-\r\n" +
            "\r\n" +
            "This method uses a two step approach, and dispenses with the use of -" + nameof(QueryExpression) + "-.\r\n" +
            "\r\n" +
            "The two steps are:\r\n " +
            "1) Use the first two levels to find EntityType and Key, lookup entity (snapshot)\r\n" +
            "2) Navigate further down in in stucture of entity found, as needed (of more than two levels in query).\r\n" +
            "TODO: Implement QueryExpression against -" + nameof(ARConcepts.EventSourcing) + "-."
        )]
        public bool TryGetEntity(DataStorage dataStorage, List<IKCoded> levels, out (List<IKCoded> linkContext, Snapshot snapshot) retval, out string errorResponseHTML, IP? initialPointer = null)
        {

            if (levels.Count != 5)
            {
                // TODO: Make more user friendly
                retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponseHTML = "<p>Exact 5 parameters required ('dt', EventTime, RegTime, EntityType, EntityKey), not " + levels.Count + ".</p>";
                return false;
            }

            // TODO: Make more flexible. This requires use of "dt/" as prefix
            if (!dataStorage.Storage.TryGetP<IP>(levels[0].Unencoded, out var pointer, out var errorResponse))
            {
                retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponseHTML = "<p>" +
                    WebUtility.HtmlEncode("Collection not found: '" + levels[0].Unencoded + "'") + ".\r\n" +
                    "Details: " + errorResponse +
                "</p>";
                return false;
            }

            var strEventTime = levels[1].Unencoded.ToString();
            EventTime? eventTime;
            if ("now".Equals(strEventTime))
            {
                // Include everything in the current "time resolution window" by rounding up to next window and use serial number 0.
                eventTime = EventTime.NowAsQuery();
            }
            else if (!EventTime.TryParse(strEventTime, out eventTime, out errorResponse))
            {
                retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponseHTML = "<p>" + WebUtility.HtmlEncode("Invalid EventTime. Use 'now' for instance.\r\nDetails: " + errorResponse) + "</p>";
                return false;
            }

            var strRegTime = levels[2].Unencoded.ToString();
            RegTime? regTime;
            // TODO: Consider using long as strRegTime, and lookup that Reg in the RegCollection in order to find RegTime.
            if (string.IsNullOrEmpty(strRegTime))
            {
                /// Do NOT use "now" as default her, as that requires much more processing in <see cref="PCollectionES"/>
                regTime = null;
            }
            else if ("now".Equals(strRegTime))
            {
                // Include everything in the current "time resolution window" by rounding up to next window and use serial number 0.
                regTime = new RegTime(UtilEvent.NowAsQuery(), serialNumber: 0);
            }
            else if (!RegTime.TryParse(strRegTime, out regTime, out errorResponse))
            {
                retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponseHTML = "<p>" + WebUtility.HtmlEncode("Invalid RegTime (" + strRegTime + "). Note: Should usually be empty.\r\nDetails: " + errorResponse) + "</p>";
                return false;
            }

            var errorResponseModifier = new Func<string, string>(r =>
            {
                r = GetTimestampIsOldWarningHTML(dataStorage.Storage) + r;
                if (initialPointer != null)
                {
                    // Do not try link insertion because most probably links will not resolve
                }
                else if (dataStorage.DocLinks != null)
                {
                    r = dataStorage.DocLinks.InsertLinks(
                        contentLocation: levels.
                            Take(levels.Count == 0 ? 0 : (levels.Count - 1)).
                            ToList(),
                        contentHTML: r
                    );
                }
                return r;
            });

            if (levels.Count < 2)
            {
                retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponseHTML = errorResponseModifier("<p>Too few parameters. Specify both entity type and key, like Custome/42</p>");
                return false;
            }

            if (!IP.AllIPDerivedTypesDict.TryGetValue(levels[3].Unencoded.ToString(), out var type))
            {
                retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponseHTML = errorResponseModifier("<p>Type '" + levels[3].Unencoded + "' not found</p>");
                return false;
            }

            if (
                !pointer.TryGetCollection(type, out var collection, out errorResponse) ||
                !collection.TryGetP(eventTime, regTime, levels[4].Unencoded, out var snapshot, out errorResponse)
            )
            {
                retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponseHTML = errorResponseModifier("<p>" + WebUtility.HtmlEncode(errorResponse) + "</p>");
                return false;
            }

            // TODO Check validity of this
            retval = (levels.ToList(), snapshot);
            errorResponseHTML = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;

            // TODO: Add step 2, continue looking into entity
        }
    }
}
