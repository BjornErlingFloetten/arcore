﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCEvent
{

    [Class(Description =
        "Methods here may be useful for other libraries too, but we do not want for instance -" + nameof(ARCCore.Extensions) + "- to be overpopulated"
    )]
    public static class Extensions
    {
        [ClassMember(Description =
            "Same function as -" + nameof(Enumerable.FirstOrDefault) + "- but faciliates better fluent expressions.\r\n"
        )]
        public static bool TryGetFirst<T>(this List<T> list, Func<T, bool> predicate, out T first, out string errorResponse, Func<string>? detailer = null)
        {
            var temp = list.FirstOrDefault(predicate);
            if (temp == null)
            {
                first = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Element not found" + detailer.Result("\r\nDetails: ");
                return false;
            }
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            first = temp;
            return true;
        }
    }
}
