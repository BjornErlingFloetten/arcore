﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCEvent
{
    [Class(Description =
        "Represents the type of an entity together with a key for looking up a specific entity.\r\n" +
        "\r\n" +
        "This class is a practical construct created in order to reduce the number of parameters " +
        "needed in various situations. It is for instance used as 'RecipientId' by -" + nameof(IRegHandler) + "- " +
        "(see for instance -" + nameof(IRegHandlerTGRI.TryGetRecipientId) + "-).\r\n" +
        "\r\n" +
        "Note: -" + nameof(ParentProcessId) + "- and -" + nameof(EntityTypeAndKey) + "- are quite similar " +
        "but not similar enough to be merged into a single class.\r\n" +
        "\r\n"
        // TOOD: Delete commented out code (IT IS NOT MUCH SIMILAR)
        //"Note: -" + nameof(ARCQuery.ForeignKey.EntityTypeAndKey) + "- is very similar to this class, but not exact identical.\r\n" +
        //"\r\n"
    )]
    public class EntityTypeAndKey : ITypeDescriber
    {
        public Type EntityType { get; private set; }

        public IK Key { get; private set; }

        public EntityTypeAndKey(Type entityType, IK key)
        {
            EntityType = entityType;
            Key = key;
        }

        public override string ToString() => EntityType.ToStringVeryShort() + " " + Key.ToString();

        // Parse / TryParse according to AgoRapide standard. Looks like overkill of course in our case
        public static EntityTypeAndKey Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidEntityTypeAndKeyException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse + "\r\n");
        public static bool TryParse(string value, out EntityTypeAndKey retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out EntityTypeAndKey retval, out string errorResponse)
        {
            if (string.IsNullOrEmpty(value))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = "Null or Empty value (" + value + ")";
                return false;
            }
            var t = value.Split(" ");
            if (t.Length != 2)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                // Note / TODO: There is nothing wrong with splitting on first space only, and accepting spaces in key here.
                errorResponse = "Not two elements separated by a space (' ') but " + t.Length + " (value '" + value + "').";
                return false;
            }

            if (
                !IP.AllIPDerivedTypesDict.TryGetValue(t[0], out var entityType) &&
                !IRegHandler.AllIRegHandlerDerivedTypesShorthandNamesDict.TryGetValue(t[0], out entityType))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = 
                    "Type '" + t[0] + "' does not resolve to a -" + nameof(IP) + "- or -" + nameof(IRegHandler) + "- type " +
                    "(value '" + value + "').";
                return false;
            }

            retval = new EntityTypeAndKey(entityType, IKString.FromString(t[1]));
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
            return true;
        }


        [ClassMember(Description = "'Implementing' -" + nameof(ITypeDescriber) + "-.")]
        public static void EnrichKey(PK k) =>
            k.SetValidatorAndParser(new Func<string, ParseResult>(value =>
                TryParse(value, out var retval, out var errorResponse) ?
                    ParseResult.CreateOK(retval) :
                    ParseResult.CreateError(errorResponse)
            ));

        public class InvalidEntityTypeAndKeyException : ApplicationException
        {
            public InvalidEntityTypeAndKeyException(string message) : base(message) { }
            public InvalidEntityTypeAndKeyException(string message, Exception inner) : base(message, inner) { }
        }
    }
}