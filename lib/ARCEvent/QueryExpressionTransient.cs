﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ARCCore;
using ARCQuery;

namespace ARCEvent
{

    [Class(Description =
        "Extracts the -" + nameof(ARConcepts.TransientEventTree) + "- for the current result (of -" + nameof(Event) + "-s).\r\n" +
        "\r\n" +
        "Extracts either\r\n" +
        "\r\n" +
        "1) The -" + nameof(IRegHandler) + "-s\r\n" +
        "(Syntax is 'TRANSIENT {comma separated list of registration handler types}' like 'TRANSIENT SSPR,SSPRT')\r\n" +
        "\r\n" +
        "or\r\n" +
        "\r\n" +
        "2) The -" + nameof(Event) + "-s.\r\n" +
        "(Syntax is 'TRANSIENT')\r\n" +
        "\r\n" +
        "In other words, the type of objects being returned is dependent on the syntax used.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class QueryExpressionTransient : QueryExpression
    {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "TRANSIENT {comma separated list of registration handler types} like 'TRANSIENT SSPR,SSPRT'"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionTransient));

        [ClassMember(Description =
            "If NULL then syntax is 'TRANSIENT', that is, we are going to return the -" + nameof(Event) + "-s, not the -" + nameof(IRegHandler) + "-."
        )]
        public List<Type>? RegHandlerTypes { get; private set; }

        public QueryExpressionTransient(List<Type>? regHandlerTypes) => RegHandlerTypes = regHandlerTypes;

        public override QueryExpressionWithSuggestions Execute(Query query)
        {
            // TODO: WHAT TO RETURN? Event OR RegHandler?
            // TODO: Consider root-information... We loose a lot of information now.
            // TODO: Maybe som Tree-extractor query expression...
            var result = new List<IKIP>();
            foreach (var ikip in query.Progress.Result)
            {
                if (!(ikip.P is Event _event))
                {
                    continue;
                }
                var transientSerialNumber = 0L;
                AddTransientEventsMatchingListOfRegHandlersRecursively(_event, ref transientSerialNumber, result);
            };
            query.Progress.Result = result;
            query.Progress.StrCurrentType = RegHandlerTypes?[0].ToStringVeryShort() ?? typeof(Event).ToStringVeryShort();
            return new QueryExpressionWithSuggestions(this);
        }

        private void AddTransientEventsMatchingListOfRegHandlersRecursively(Event _event, ref long transientSerialNumber, List<IKIP> result)
        {
            try
            {
                REx.Inc();

                // TODO: Do we want this restriction at all?
                // TODO: In general it only applies to events NOT in the TransientEventTree 
                // TODO: (that is, events that are not found here anyway)
                // if (!(_event.ReplacedRegTime is null)) return;

                // TODO: Alternative approach, check if holds
                // Removed check 24 Dec 2021, but user must then be aware of problem.
                //if (!(_event.ReplacedRegTime is null))
                //{
                //    throw new DatastructureException("Found event (" + _event.EventTime + " / " + _event.RegHandler.GetType().ToString() + " in -" + nameof(ARConcepts.TransientEventTree) + "- with -" + nameof(EventP.ReplacedRegTime) + "- set");
                //} 

                // TOOD: Do (in general) take into consideration DO, UNDO, PLAN and so on here.
                // TODO: In general, events should have a "does this event count as a result"
                // TODO: Or, rather, this is probably not a concern here, the assertion above may be sufficient.

                if (RegHandlerTypes == null) 
                {
                    // Syntax was 'TRANSIENT'
                    transientSerialNumber++;
                    result.Add(new IKIP(
                        IKString.FromString(
                            _event.EventTime.SerialNumber.ToString(UtilEvent.SerialNumberFormat) + " " + 
                            transientSerialNumber.ToString("000") // Allow for up to 999 transient events.
                         ),
                        _event
                    ));
                }
                else if (RegHandlerTypes.Any(type => type.IsAssignableFrom(_event.RegHandler.GetType())))
                {
                    // Syntax was 'TRANSIENT {list of registration handlers types}'
                    transientSerialNumber++;
                    result.Add(new IKIP(
                        IKString.FromString(
                            _event.EventTime.SerialNumber.ToString(UtilEvent.SerialNumberFormat) + " " +
                            transientSerialNumber.ToString("000") // Allow for up to 999 transient events.
                         ),
                        _event.RegHandler
                    ));
                }
                if (_event.TransientEvents is null) return;
                foreach (var e in _event.TransientEvents)
                {
                    AddTransientEventsMatchingListOfRegHandlersRecursively(e, ref transientSerialNumber, result);
                }
            }
            finally
            {
                REx.Dec();
            }
        }

        public new static QueryExpressionTransient Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryExpressionTransientException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionTransient retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionTransient retval, out string errorResponse)
        {
            value = value.Trim().Replace("  ", " ");
            var t = value.Split(" ");
            if (!"transient".Equals(t[0].ToLower()))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'TRANSIENT' as first word." + SyntaxHelp;
                return false;
            }

            if (t.Length == 1)
            {
                // We are meant to return the Events, not the IRegHandlers
                retval = new QueryExpressionTransient(regHandlerTypes: null);
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }

            if (t.Length != 2)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not two words like 'TRANSIENT {comma separated list of registration handler types}' but " + t.Length + " words." + SyntaxHelp;
                return false;
            }
            var regHandlerTypes = new List<Type>();
            foreach (var strType in t[1].Split(","))
            {
                if (!IRegHandler.AllIRegHandlerDerivedTypesShorthandNamesDict.TryGetValue(strType, out var type))
                {
                    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                    errorResponse = "Type (" + strType + ") is not implementing -" + nameof(IRegHandler) + "-." + SyntaxHelp;
                    return false;
                }
                regHandlerTypes.Add(type);
            }
            if (regHandlerTypes.Count > 1)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                // TODO: Confer with Execute and setting of StrCurrentType
                errorResponse = "Multiple types found (" + string.Join(", ", regHandlerTypes.Select(t => t.ToStringVeryShort())) + "). Only one is currently supported." + SyntaxHelp;
                return false;
            }
            retval = new QueryExpressionTransient(regHandlerTypes);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString() => "TRANSIENT" + (RegHandlerTypes == null ? "" : (" " + string.Join(",", RegHandlerTypes.Select(t => t.ToStringVeryShort()))));

        [ClassMember(Description =
            "Reconstructs -" + nameof(EventId) + "- from a key that was probably constructed by -" + nameof(AddTransientEventsMatchingListOfRegHandlersRecursively) + "-.\r\n" +
            "\r\n" +
            "NOTE: Used by an -" + nameof(IRegHandler) + " which normally will not have a key because it will be a transient event, but if it was put into\r\n" +
            "NOTE: a collection by for instance -" + nameof(QueryExpressionTransient) + "-\r\n" +
            "NOTE: then we assume that an id can be extracted from the key used.\r\n" +
            "\r\n" +
            "NOTE: This is a bit brittle. Depends on how for instance -" + nameof(AddTransientEventsMatchingListOfRegHandlersRecursively) + "-\r\n" +
            "NOTE: constructs ids.\r\n" +
            "\r\n" +
            "Has 'static' in name in order not to be confused with something understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public static bool TryGetEventIdStatic(IP dataStorage, IK thisKey, out PValue<EventId> retval, out string errorResponse)
        {
            // Bug, looking for [1] when correct is [0]
            //var t = thisKey.ToString().Split(" ");
            //if (t.Length >= 2 && long.TryParse(t[1], out var _long))

            if (long.TryParse(thisKey.ToString().Split(" ")[0], out var _long))
            {                
                retval = new PValue<EventId>(new EventId(_long));
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
            retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            errorResponse = "Unable to reconstruct an -" + nameof(EventId) + "- from '" + thisKey.ToString() + "'.";
            return false;
        }

        [ClassMember(Description =
            "Reconstructs -" + nameof(RegId) + "- from a key that was probably constructed by " +
            "-" + nameof(AddTransientEventsMatchingListOfRegHandlersRecursively) + "-.\r\n" +
            "\r\n" +
            "NOTE: Used by an -" + nameof(IRegHandler) + " which normally will not have a key " +
            "NOTE: because it will be a transient event, but if it was put into\r\n" +
            "NOTE: a collection by for instance -" + nameof(QueryExpressionTransient) + "-\r\n" +
            "NOTE: then we assume that an id can be extracted from the key used.\r\n" +
            "\r\n" +
            "NOTE: This is a bit brittle. Depends on how for instance -" + nameof(AddTransientEventsMatchingListOfRegHandlersRecursively) + "-\r\n" +
            "NOTE: constructs ids.\r\n" +
            "\r\n" +
            "TODO: We must probably delete this because of -" + nameof(RunMacroR) + "- and -" + nameof(EXEcuteR) + "- which can give " +
            "TODO: other RegIds for execution of events (other than original RegId that is).\r\n" +
            "\r\n" +
            "Has 'static' in the name (TryGetRegIdStatic) in order not to be confused with something understood by " +
            "-" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public static bool TryGetRegIdStatic(IP dataStorage, IK thisKey, out PValue<RegId> retval, out string errorResponse)
        {
            // Bug, looking for [1] when correct is [0]
            //var t = thisKey.ToString().Split(" ");
            //if (t.Length >= 2 && long.TryParse(t[1], out var _long))

            if (long.TryParse(thisKey.ToString().Split(" ")[0], out var _long))
            {
                retval = new PValue<RegId>(new RegId(_long));
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
            retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            errorResponse = "Unable to reconstruct an -" + nameof(RegId) + "- from '" + thisKey.ToString() + "'.";
            return false;
        }

        public class InvalidQueryExpressionTransientException : ApplicationException
        {
            public InvalidQueryExpressionTransientException(string message) : base(message) { }
            public InvalidQueryExpressionTransientException(string message, Exception inner) : base(message, inner) { }
        }
    }

    /// <summary>
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionTransientExtension
    {
        public static Query Transient(this Query query) =>
            query.ExecuteOne(new QueryExpressionTransient(regHandlerTypes: null));

        public static Query Transient(this Query query, List<Type> regHandlerTypes) =>
            query.ExecuteOne(new QueryExpressionTransient(regHandlerTypes));
    }
}
