﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCEvent
{
    [Class(
        Description =
            "Classes implementing this interface serve two functions:\r\n" +
            "\r\n" +
            "1) As a model describing a command.\r\n" +
            "\r\n" +
            "2) The actually handling of the command, that is the actual execution.\r\n" +
            "\r\n" +
            "Belongs inside -" + nameof(Cmd) + "- as -" + nameof(CmdP.CH) + "-.\r\n" +
            "\r\n" +
            "Note how the serialized format of a Registration, as given by -" + nameof(Serialize) + "-  +" +
            "is quite similar to the format understood by -" + nameof(RSController) + "-\r\n" +
            "It is very compact by nature, much more compact than -" + nameof(ARConcepts.PropertyStream) + "- " +
            "and probably also much more compact than JSON.\r\n"
    )]

    public interface ICmdHandler : IP
    {

        private static HashSet<Type>? allCmdHandlerDerivedTypes = null;
        [ClassMember(Description =
            "All types implementing -" + nameof(ICmdHandler) + "-."
        )]
        public static HashSet<Type> AllICmdHandlerDerivedTypes => allCmdHandlerDerivedTypes ??= new Func<HashSet<Type>>(() =>
        {
            UtilCore.AssertCurrentlyStartingUp(); // TODO: Do we need this assertion? Probably not
            var retval = new HashSet<Type>();
            UtilCore.Assemblies.SelectMany(a => a.GetTypes()).Where(t =>
            {
                if (!typeof(ICmdHandler).IsAssignableFrom(t)) return false;
                if (t.IsGenericTypeDefinition) return false; /// Typically: [TODO: Add example her, if any)
                if (t.IsAbstract) return false; /// Typically: [TODO: Add example her, if any)
                if (t.IsInterface) return false; /// Excludes <see cref="ICmdHandler"/> itself
                return true;
            }).ForEach(t =>
            {
                if (!retval.Add(t)) throw new InvalidTypeException(t, "Already contained in " + nameof(retval) + "\r\n");
            });
            return retval;
        })();

        public static Dictionary<string, Type>? _allICmdHandlerDerivedTypesShorthandNamesDict = null;
        [ClassMember(Description =
            "Key is the short hand representation of an -" + nameof(ICmdHandler) + "-'s Type, value is the actual Type\r\n" +
            "\r\n" +
            "TODO: Create real example here:\r\n" +
            "Example 1: Key xx points to value -" + nameof(IRegHandler) + "-.\r\n" + 
            "\r\n" +
            "In addition, keys are also added for the full name representation (from -" + nameof(ARCCore.Extensions.ToStringVeryShort) + "-).\r\n" +
            "\r\n" +
            "TODO: Rename into something better, compared to -" + nameof(AllICmdHandlerDerivedTypesTypeDict) + "-."
        )]
        public static Dictionary<string, Type> AllICmdHandlerDerivedTypesShorthandNamesDict = _allICmdHandlerDerivedTypesShorthandNamesDict ??= new Func<Dictionary<string, Type>>(() =>
        {
            UtilCore.AssertCurrentlyStartingUp(); // TODO: Do we need this assertion? Probably not
            var retval = new Dictionary<string, Type>();
            AllICmdHandlerDerivedTypes.ForEach(t =>
            {
                new List<string> {
                    t.ToStringVeryShort(), // TODO: Add more representation here if needed.
                    string.Join("", t.ToStringVeryShort().Where(a => a == char.ToUpper(a))) // PascalCase
                }.ForEach(name =>
                {
                    if (!retval.TryAdd(name, t))
                    {
                        if (t.Equals(retval[name]))
                        {
                            // The type consists of only capital letters, therefore the collision. Ignore.                           
                        }
                        else
                        {
                            throw new InvalidTypeException(t,
                                "The two Cmd (Command) Handler types " + t.ToString() + " and " + retval[name].ToString() + " " +
                                "both resolve to the same name, or short name, '" + name + "'.\r\n" +
                                "\r\n" +
                                "Possible resolution: Change the name of one of the types."
                            );
                        }
                    }
                });
            });
            return retval;
        })();

        public static Dictionary<Type, string>? _allICmdHandlerDerivedTypesTypeDict = null;
        [ClassMember(Description =
            "Key is the type of an -" + nameof(ICmdHandler) + "-, Value is the short hand representation of an -" + nameof(ICmdHandler) + "-'s Type.\r\n" +
            "\r\n" +
            "Example 1: Key -" + nameof(ICmdHandler) + "- points to value {TODO: Insert some command name here} .\r\n" +
            "\r\n" +
            "TODO: Rename into something better, compared to -" + nameof(AllICmdHandlerDerivedTypesShorthandNamesDict) + "-."
        )]
        public static Dictionary<Type, string> AllICmdHandlerDerivedTypesTypeDict = _allICmdHandlerDerivedTypesTypeDict ??= new Func<Dictionary<Type, string>>(() =>
        {
            UtilCore.AssertCurrentlyStartingUp(); // TODO: Do we need this assertion? Probably not
            var retval = new Dictionary<Type, string>();
            AllICmdHandlerDerivedTypes.ForEach(t =>
                retval.Add(t, string.Join("", t.ToStringVeryShort().Where(a => a == char.ToUpper(a)))) // PascalCase);
            );
            return retval;
        })();

        [ClassMember(Description =
            "Attempts to parse (deserialize) an API request to an -" + nameof(ICmdHandler) + "- in a standardized manner, " +
            "ensuring that all -" + nameof(PKTypeAttributeP.IsObligatory) + "- values are set and " +
            "returning an error response if superfluous parameters are given.\r\n" +
            "\r\n" +
            "The given apiRequest parameter should start with the name of the actual -" + nameof(ICmdHandler) + "- to use.\r\n" +
            "Example: If the API request is like 'Cmd/{TODO: Insert some command name here}/...' the parameter passed to this method should be equivalent to " +
            "'{TODO: Insert some command name here}/...'.\r\n" +
            "\r\n" +
            "The apiRequest parameter should be in unencoded format.\r\n" +
            "\r\n" +
            "The parameters are assumed to be in the same order as the corresponding -" + nameof(PK) + "- enums.\r\n" +
            "\r\n" +
            "See also -" + nameof(PSPrefix.dtr) + "-, -" + nameof(IRegHandler.TryParse) + "- and -" + nameof(IP.TryParseDtr) + "-.\r\n" +
            "(code in -" + nameof(IRegHandler.TryParse) + "- and -" + nameof(IP.TryParseDtr) + "- is quite similar to code here)."
        )]
        public static bool TryParse(List<string> apiRequest, out ICmdHandler cmdHandler, out string errorResponse)
        {
            if (apiRequest.Count == 0)
            {
                cmdHandler = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "No Cmd model name given";
                return false;
            }
            var cmdName = apiRequest[0];
            if (!AllICmdHandlerDerivedTypesShorthandNamesDict.TryGetValue(cmdName, out var type))
            {
                cmdHandler = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "No Cmd (Command) matching '" + cmdName + "' was found.";
                return false;
            }
            cmdHandler = (ICmdHandler)System.Activator.CreateInstance(type);

            // Apply values in order
            var pks = PK.GetAllPKForEntityType(type).ToList(); // ToList makes for easier iteration
            var parameters = apiRequest.Skip(1).ToList(); // Make comparing easier (same indices)
            var i = 0;
            for (i = 0; i < parameters.Count && i < pks.Count; i++)
            {
                // TODO: Skip if blank and not IsObligatory
                var parameterValue = parameters[i];
                var pk = pks[i];
                if (
                    string.IsNullOrEmpty(parameterValue) && 
                    pk.GetA<PKTypeAttribute>().IP.GetPV<bool>(PKTypeAttributeP.IsObligatory, defaultValue: false)
                )
                {
                    continue; // Blank and not obligatory, OK
                }

                // TODO: Is this check actually relevantæ? Do we refer to Cmd Handlers in Cmd Handlers?
                if (pk.Type == typeof(Type) && ICmdHandler.AllICmdHandlerDerivedTypesShorthandNamesDict.TryGetValue(parameterValue, out var temp))
                {
                    /// <see cref="PKTypeAttribute"/> only knows about types in <see cref="UtilCore.TryGetTypeFromStringFromCache"/>
                    /// It does not know about shorthand representations of types, therefore change back to 'full' name
                    parameterValue = temp.ToStringVeryShort();
                }
                if (!pk.TryCleanParseAndPack(parameterValue, out var property, out errorResponse))
                {
                    cmdHandler = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                // TODO: Link to full documentation
                    errorResponse =
                        "Illegal value (" + parameterValue + ") given for parameter -" + pk.__enum.GetType().ToStringVeryShort() + "-.-" + pk.__enum + "-\r\n" +
                        "(Parameter no " + (i + 1) + " of " + parameters.Count + " given / " + pks.Count + " possible (counted from one))\r\n" +
                        "Details:\r\n" + errorResponse + "\r\n\r\n" +
                        (pk.IP.TryGetPV<string>(BaseAttributeP.Description, out var description) ?
                            ("Description of parameter:\r\n----------" + description) : ""
                        );
                    return false;
                }
                cmdHandler.SetP(new IKIP(pk, property));
            }

            if (parameters.Count > i)
            {
                cmdHandler = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                // TODO: Link to full documentation
                errorResponse =
                    "Excess parameters given for Cmd (Command) -" + type.ToStringShort() + "-.\r\n" +
                    "The excess parameters are: " + string.Join(", ", parameters.Skip(i).Select(r => "'" + r + "'"));
                return false;
            }
            if (pks.Count > i && pks.Skip(i).Any(pk => pk.GetA<PKTypeAttribute>().IsObligatory))
            {
                cmdHandler = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse =
                    "Missing obligatory parameters for Cmd (Command) -" + type.ToStringShort() + "-.\r\n" +
                    "The missing parameters are:\r\n" + string.Join("\r\n", pks.Skip(i).Where(pk => pk.GetA<PKTypeAttribute>().IsObligatory).Select(pk =>
                        "-----------\r\n-" + pk.__enum.GetType().ToStringVeryShort() + "-.-" + pk.__enum.ToString() + "-" + (pk.IP.TryGetPV<string>(BaseAttributeP.Description, out var description) ?
                        ("\r\n----------" + description) : ""
                    )));
                return false;
            }

            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Serializes to a format understood by -" + nameof(TryParse) + "-.\r\n" +
            "\r\n" +
            "The format should also be safe to put into an HTML page (no HTMLEncode should be needed), " +
            "or use as an URL request for sending to -" + nameof(CmdController) + "- (no URLEncode should be needed).\r\n" +
            "\r\n"
        )]
        public string Serialize()
        {
            if (this is EmptyRegHandlerR) return "";
            var pks = PK.GetAllPKForEntityType(GetType()).ToList(); // ToList makes for easier iteration
            var retval = new StringBuilder();
            retval.Append(GetType().ToStringVeryShort() + "/");
            for (var i = 0; i < pks.Count; i++)
            {
                if (!TryGetPV<string>(pks[i], out var strValue))
                {
                    // Skip this value
                }
                else
                {
                    // TODO: Verify correctness of encoding here
                    retval.Append(IKCoded.FromUnencoded(IKString.FromString(strValue)).Encoded.
                        // Relax encoding somewhat
                        Replace("0x0020", " ")
                    );
                }
                if (i < pks.Count - 1)
                {
                    retval.Append("/");
                }
            }
            var strRetval = retval.ToString();
            while (strRetval.EndsWith("/"))
            {
                // Remove trailing slashes. These would create problems with later parsing because they would indicate the
                // presence of a value.
                strRetval = strRetval[0..^1];
            }

            return strRetval;
        }
    }
}
