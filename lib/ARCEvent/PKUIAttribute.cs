﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCEvent
{
    [Class(Description =
        "Gives hints about how to construct a user interface.\r\n" +
        "\r\n" +
        "See also -" + nameof(PKUIAttributeP) + "-.\r\n"
    )]
    public class PKUIAttribute : BasePKAttribute
    {

        [ClassMember(Description = "See -" + nameof(PKUIAttributeP.NotObligatoryForPLAN) + "-.")]
        public bool NotObligatoryForPLAN;

        [ClassMember(Description = "See -" + nameof(PKUIAttributeP.DoNotOfferIds) + "-.")]
        public bool DoNotOfferIds;

        [ClassMember(Description = "See -" + nameof(PKUIAttributeP.SubProcessType) + "-.")]
        public Type[]? SubProcessType;

        public override void Initialize()
        {
            if (NotObligatoryForPLAN) IP.AddPV(PKUIAttributeP.NotObligatoryForPLAN, NotObligatoryForPLAN);
            if (DoNotOfferIds) IP.AddPV(PKUIAttributeP.DoNotOfferIds, DoNotOfferIds);
            if (SubProcessType != null) IP.AddPV(PKUIAttributeP.SubProcessType, SubProcessType.ToList());
            base.Initialize();
        }
    }

    [Enum(
        Description = "Describes class -" + nameof(PKUIAttribute) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum PKUIAttributeP
    {
        __invalid,

        [PKType(
            Description =
                "TODO: Consider alternative name for this property 'OnlyObligatoryForDO'.\r\n" +
                "TODO: (sounds more exact in its meaning).\r\n" +
                "\r\n" +
                "TRUE indicates that for -" + nameof(RegVerb) + "-.-" + nameof(RegVerb.PLAN) + "- this value is not obligatory.\r\n" +
                "\r\n" +
                "Set this property in order to be able to set -" + nameof(PKTypeAttributeP.IsObligatory) + "- for more fields " +
                "than would normally have been possible.\r\n" +
                "\r\n" +
                "Typically used by -" + nameof(IRegHandler.TryParse) + "-",
            Type = typeof(bool)
        )]
        NotObligatoryForPLAN,

        [PKType(
            Description =
                "Relevant for fields ending with 'Id' in which case for instance -" + nameof(ProcessController) + "- would by default " +
                "offer all ids found in the collection of the given type.\r\n" +
                "\r\n" +
                "If the relevant Registration is about CREATING a new entity for instance, then it would be meaningless to offer existing id\r\n" +
                "Other reasons for not offering an id could be that there simply are too many to choose from.",
            Type = typeof(bool)
        )]
        DoNotOfferIds,

        [PKType(
            Description =
                "Relevant for -" + nameof(ProcessP.SubProcess) + "- and corresponding other enum keys with name 'SubProcess'.\r\n" +
                "\r\n" +
                "See -" + nameof(IRegHandlerTATPP) + "-.\r\n" +
                "\r\n" +
                "-" + nameof(ProcessController) + "- interprets the existence of this value as meaning \"no edit\" " +
                "(user editing not relevant).\r\n" +
                "\r\n" +
                "In addition to being UI related this also describes what sub processes a given -" + nameof(IRegHandler) + "- is constituted of.\r\n" +
                "-" + nameof(ProcessController) + "- uses this value to show the relevant sub processes.\r\n" +
                "\r\n",
            Type = typeof(Type), Cardinality = Cardinality.WholeCollection
        )]
        SubProcessType,

        [PKType(Description =
            "TODO: Not implemented. Class implementing IIdCreator which knows which ids are relevant\r\n" +
            "TOOD: (usually based on some specific criteria).\r\n" +
            "TODO: The idea is to specify which ids are relevant for a process."
        )]
        IdCreator
    }
}
