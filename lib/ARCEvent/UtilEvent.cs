﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

/// <summary>
/// Contains the following:
/// <see cref="ARCEvent.UtilEvent"/>
/// <see cref="ARCEvent.DatastructureException"/>
/// </summary>
namespace ARCEvent
{
    public static class UtilEvent
    {

        [ClassMember(Description =
            "Formatting of serial numbers in a manner which allows 'alphabetically' sorting.\r\n" +
            "(like avoiding '11' being sorted before '2').\r\n" +
            "\r\n" +
            "8 digits will work up to 100 million events.\r\n" +
            "\r\n" +
            "Used by:\r\n" +
            "-" + nameof(Event.TryGetEventTimeSortable) + "-,\r\n" +
            "-" + nameof(Reg.TryGetEventTimeSortable) + "-.\r\n" +
            "\r\n" +
            "Set to shorter or longer string according to need."
        )]
        public static string SerialNumberFormat = "00000000";

        private static TimestampResolutionType _timestampResolution = TimestampResolutionType.WholeMinutes;
        [ClassMember(Description =
            "TODO: Currently duplicated as -" + nameof(StreamProcessorP.TimestampResolution) + "- and -" + nameof(UtilEvent.TimestampResolution) + "-.\r\n" +
            "\r\n" +
            "See -" + nameof(StreamProcessorP.TimestampResolution) + "- for explanation.\r\n" +
            "\r\n" +
            "Note different ways of specifying resolution, in -" + nameof(ARCEvent) + "- -" + nameof(TimestampResolutionType) + "- is used " +
            "while in -" + nameof(ARComponents.ARCCore) + "-.-" + nameof(StreamProcessor) + "- TimeSpan is used.\r\n" +
            "TODO: Consider amalgating these two approaches.\r\n" +
            "\r\n" +
            "The resolution should be set to the minimum resolution that a human operator is interested in using when distinguishing -" + nameof(Event) + "-s.\r\n" +
            "For instance if events only have interest to the human operator within a resolution of minutes, then this value can be set to " +
            "-" + nameof(TimestampResolutionType.WholeMinutes) + "-.\r\n" +
            "A value of -" + nameof(TimestampResolutionType.Whole5Minutes) + "- or even higher may even be practical in many cases.\r\n" +
            "\r\n"
        )]
        public static TimestampResolutionType TimestampResolution
        {
            get => _timestampResolution;
            set
            {
                UtilCore.AssertCurrentlyStartingUp();
                _timestampResolution = value;
            }
        }

        [ClassMember(Description =
            "Rounds the given DateTime backwards in time according to the -" + nameof(TimestampResolution) + "- used.\r\n" +
            "\r\n" +
            "Example: With a resolution of -" + nameof(TimestampResolutionType.WholeMinutes) + "- " +
            "a time of '2021-12-07 10:03:42.323' will be changed to '2021-12-07 10:03:00.000' " +
            "which again can be 'safely' displayed as ''2021-12-07 10:03'\r\n" +
            "\r\n" +
            "Used in order to decide meaning of 'now' when adding -" + nameof(Reg) + "-s through -" + nameof(RSController) + "-.\r\n" +
            "\r\n" +
            "Rationale: (given a resolution of -" + nameof(TimestampResolutionType.WholeMinutes) + "-) An -" + nameof(Reg) + "- performed with 'now' inside " +
            "a minute 'window' for instance 42 seconds into the 10:03 window (in other words a Registration added at 10:03:42) should probably " +
            "have 10:43 as -" + nameof(RegTime) + "-.\r\n" +
            "\r\n" +
            "See also -" + nameof(RoundDateTimeForwards) + "-.\r\n"
        )]
        public static DateTime RoundDateTimeBackwards(DateTime dateTime) =>
            (_timestampResolution) switch
            {
                TimestampResolutionType.WholeMilliseconds => new DateTime(dateTime.Ticks - (dateTime.Ticks % 10000)),
                TimestampResolutionType.WholeSeconds => new DateTime(dateTime.Ticks - (dateTime.Ticks % (1000 * 10000))),
                TimestampResolutionType.WholeMinutes => new DateTime(dateTime.Ticks - (dateTime.Ticks % (60 * 1000 * 10000))),
                TimestampResolutionType.Whole5Minutes => new DateTime(dateTime.Ticks - (dateTime.Ticks % (5L * 60 * 1000 * 10000))),
                TimestampResolutionType.Whole15Minutes => new DateTime(dateTime.Ticks - (dateTime.Ticks % (15L * 60 * 1000 * 10000))),
                _ => throw new InvalidEnumException(_timestampResolution)
            };

        [ClassMember(Description =
            "Rounds the given DateTime forwards in time according to the -" + nameof(TimestampResolution) + "- used.\r\n" +
            "\r\n" +
            "Example: With a resolution of -" + nameof(TimestampResolutionType.WholeMinutes) + "- " +
            "a time of '2021-12-07 10:03:42.323' will be changed to '2021-12-07 10:04:00.000' " +
            "which again can be 'safely' displayed as ''2021-12-07 10:04'\r\n" +
            "\r\n" +
            "Used in order to decide meaning of 'now' when querying through -" + nameof(ESController) + "-.\r\n" +
            "\r\n" +
            "Rationale: (given a resolution of -" + nameof(TimestampResolutionType.WholeMinutes) + "-) A query with 'now' inside " +
            "a minute 'window' for instance 42 seconds into the 10:03 window (in other words a query performed at 10:03:42) should probably include all " +
            "events within that window,  that is everything up to (but not including) 10:04 (not including is assured because " +
            "a -" + nameof(EventTime.SerialNumber) + "- of 0 will be used, meaning no events from 10:04 onwards will be inclued).\r\n" +
            "\r\n" +
            "Note: One could of course also instead -" + nameof(RoundDateTimeBackwards) + "- with a serial number of long.MaxValue " +
            "but this would not make for pretty presentation (a huge number would show up in the interface).\r\n" +
            "\r\n" +
            "See also -" + nameof(RoundDateTimeBackwards) + "-.\r\n"
        )]
        public static DateTime RoundDateTimeForwards(DateTime dateTime) =>
            (_timestampResolution) switch
            {
                TimestampResolutionType.WholeMilliseconds => new DateTime(dateTime.Ticks - (dateTime.Ticks % 10000) + (10000)),
                TimestampResolutionType.WholeSeconds => new DateTime(dateTime.Ticks - (dateTime.Ticks % (1000 * 10000)) + (1000 * 10000)),
                TimestampResolutionType.WholeMinutes => new DateTime(dateTime.Ticks - (dateTime.Ticks % (60 * 1000 * 10000)) + (60 * 1000 * 10000)),
                TimestampResolutionType.Whole5Minutes => new DateTime(dateTime.Ticks - (dateTime.Ticks % (5L * 60 * 1000 * 10000)) + (5L * 60 * 1000 * 10000)),
                TimestampResolutionType.Whole15Minutes => new DateTime(dateTime.Ticks - (dateTime.Ticks % (15L * 60 * 1000 * 10000)) + (15L * 60 * 1000 * 10000)),
                _ => throw new InvalidEnumException(_timestampResolution)
            };

        [ClassMember(Description =
            "The value for 'now' which makes sense when finding the time for when a Registration was performed.\r\n" +
            "This is the start of the current timeinterval according to -" + nameof(TimestampResolution) + "-.\r\n" +
            "\r\n" +
            "Example: At time 12:28 with a -" + nameof(TimestampResolution) + "- of -" + nameof(TimestampResolutionType.Whole5Minutes) + "- " +
            "it makes sense to insert Registration / Event at 12:25 in the timeline."
        )]
        public static DateTime NowAsEvent() => UtilEvent.RoundDateTimeBackwards(UtilCore.DateTimeNow);

        [ClassMember(Description =
            "The value for 'now' which makes sense when querying.\r\n" +
            "This is equivalent to the start of the next timeinterval according to -" + nameof(TimestampResolution) + "-.\r\n" +
            "\r\n" +
            "Example: At time 12:28 with a -" + nameof(TimestampResolution) + "- of -" + nameof(TimestampResolutionType.Whole5Minutes) + "- " +
            "it makes sense to query " +
            "for all values less than 12:30 (< 12:30). This will for instance catch all events within the time period of " +
            "[12:25 - 12:30> regardless of their -" + nameof(EventTime.SerialNumber) + "-."
        )]
        public static DateTime NowAsQuery() => UtilEvent.RoundDateTimeForwards(UtilCore.DateTimeNow);

        public enum TimestampResolutionType
        {
            __invalid,
            WholeMilliseconds,
            WholeSeconds,
            WholeMinutes,
            Whole5Minutes,
            Whole15Minutes
        }

        [ClassMember(Description =
            "Example of assertion method which helps with creating more \"fluent\" code.\r\n" +
            "\r\n" +
            "TODO: Find a more suitable home for such methods. Maybe a separate -" + nameof(ARComponents) + "- like ARCAssertions."
        )]
        public static bool TryAssertNotNegative(long value, out string errorResponse, Func<string>? detailer)
        {
            if (value >= 0)
            {
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
            errorResponse = "Number '" + value + "' is negative. " + detailer.Result("Details: ");
            return false;
        }
    }

    [Class(Description =
        "Signifies that some part of the data structure has broken down, presumable because of some " +
        "erroneous processing of events.\r\n"
    )]
    public class DatastructureException : ApplicationException
    {
        public DatastructureException(string message) : base(message) { }
        public DatastructureException(string message, Exception inner) : base(message, inner) { }
    }
}
