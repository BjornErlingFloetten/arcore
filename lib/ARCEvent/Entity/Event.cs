﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using ARCCore;
using ARCQuery;

/// <summary>
/// Contains the following:
/// <see cref="ARCEvent.Event"/>
/// <see cref="ARCEvent.EventP"/>
/// <see cref="ARCEvent.EventTime"/>
/// <see cref="ARCEvent.EventId"/>
/// <see cref="ARCEvent.EventStatus"/>
/// </summary>
namespace ARCEvent
{

    [Class(Description =
        "An Event is an item in the -" + nameof(ARConcepts.EventStream) + "-.\r\n" +
        "\\r\n" +
        "It is always generated as a consequence of a corresponding -" + nameof(Reg) + "-, either directly or as a 'transient' event " +
        "(being part of the -" + nameof(ARConcepts.TransientEventTree) + "-) " +
        "generated through -" + nameof(IRegHandlerTGTE.TryGetTransientEvents) + "- from another Event.\r\n" +
        "\r\n" +
        "This class is designed for high performance and is therefore a hybrid of -" + nameof(IP) + "- / -" + nameof(ARConcepts.PropertyAccess) + "- and conventional.\r\n" +
        "\r\n" +
        "The focus is on high read-performance on the properties for quick on-demand generation of -" + nameof(Snapshot) + "-s.\r\n" +
        "\r\n" +
        "In addition, supporting -" + nameof(ARConcepts.PropertyAccess) + "- makes for easier debugging.\r\n" +
        "\r\n" +
        "TODO: The memory consumption of this class can very easily be reduces significantly.\r\n" +
        "TODO: Use -" + nameof(PReadOnly) + "- instead of -" + nameof(PRich) +"-.\r\n" +
        "TOOD: (see how this has been done for -" + nameof(SSPRT<TPropertyKeyEnum>) + "-).\r\n" +
        "TODO: OR (rather better approach) use -" + nameof(EntityMethodKey) + "- for reading, " +
        "TODO: and store only as traditional C# properties here.\r\n" +
        "\r\n"
    )]
    public class Event : PRich
    { // TODO: Turn into PExact<>, will save some memory when storing

        [ClassMember(Description = "Event is a root event, that is, it is not part of a -" + nameof(ARConcepts.TransientEventTree) + "-.")]
        public bool IsRootEvent { get; private set; }

        private RegTime _regTime;
        public RegTime RegTime
        {
            get => _regTime; /// This is dangerous, outside may set via <see cref="ARConcepts.PropertyAccess"/> instead.
            private set
            {
                _regTime = value;
                IP.SetPV(EventP.RegTime, value);
            }
        }

        private EventTime _eventTime;
        public EventTime EventTime
        {
            get => _eventTime; /// This is dangerous, outside may set via <see cref="ARConcepts.PropertyAccess"/> instead.
            private set
            {
                _eventTime = value;
                IP.SetPV(EventP.EventTime, value);
            }
        }

        private RegVerb _regVerb;
        public RegVerb RegVerb
        {
            get => _regVerb; /// This is dangerous, outside may set via <see cref="ARConcepts.PropertyAccess"/> instead.
            private set
            {
                _regVerb = value;
                IP.SetPV(EventP.RegVerb, value);
            }
        }

        private IRegHandler _regHandler;

        [ClassMember(Description =
            "NOTE: IMPORTANT! For registration handlers having -" + nameof(SubProcess) + "-es, the object stored here " +
            "NOTE: IMPORTANT! is not necessarily up-to-date because such registration handlers must be updated as -" + nameof(Snapshot) + "-.\r\n" +
            "\r\n"
        )]
        public IRegHandler RegHandler
        {
            get => _regHandler; /// This is dangerous, outside may set via <see cref="ARConcepts.PropertyAccess"/> instead.
            private set
            {
                _regHandler = value;
                IP.SetP(EventP.RH, value);
            }
        }

        [ClassMember(Description =
            "The last time at which a call was made to -" + nameof(EventProcessor.TryGenerateAndDistributeTransientEventsRecursively) + "-.\r\n" +
            "\r\n" +
            "Says something about when -" + nameof(EventStatus) + "- and -" + nameof(ErrorResponse) + "- was created.\r\n" +
            "TODO: Change name of this property."
        )]
        public DateTime LastCallToTryGenerateAndDistributeTransientEventsRecursively { get; set; }

        private EventStatus _eventStatus;
        [ClassMember(Description =
            "Related to the last time at which a call was made to -" + nameof(EventProcessor.TryGenerateAndDistributeTransientEventsRecursively) + "-.\r\n"
        )]
        public EventStatus EventStatus
        {
            get => _eventStatus;
            set
            {
                _eventStatus = value;
                IP.SetPV(EventP.EventStatus, value);
            }
        }

        private string? _errorResponse;
        [ClassMember(Description =
            "Related to the last time at which a call was made to -" + nameof(EventProcessor.TryGenerateAndDistributeTransientEventsRecursively) + "-.\r\n"
        )]
        public string? ErrorResponse
        {
            get => _errorResponse;
            set
            {
                _errorResponse = value;
                if (value == null)
                {
                    IP.SetP(EventP.ErrorResponse, PValueEmpty.Singleton);
                }
                else
                {
                    IP.SetPV(EventP.ErrorResponse, value);
                }
            }
        }

        private RegTime? _replacedRegTime;
        [ClassMember(Description =
            "The -" + nameof(RegTime) + "- at which this event " +
            "(either a root event or an event in -" + nameof(ARConcepts.TransientEventTree) + "-) " +
            "was replaced by another event.\r\n" +
            "\r\n" +
            "A root event (an event not in the -" + nameof(ARConcepts.TransientEventTree) + "-) " +
            "is replaced as a consequence of a later event (later in the -" + nameof(RegTime) + "- sense) " +
            "having the exact same -" + nameof(EventTime) + "-.\r\n" +
            "The other event may for instance have a different -" + nameof(RegVerb) + "-, " +
            "like -" + nameof(RegVerb.DO) + "- being followed by -" + nameof(RegVerb.UNDO) + "- " +
            "or it may simply contain different data (a correction to the original event).\r\n" +
            "\r\n" +
            "A transient event (an event in the -" + nameof(ARConcepts.TransientEventTree) + "-) is replaced either\r\n" +
            "1) As a consequence of its root event becoming invalid (as described above), or\r\n" +
            "2) Because of earlier / older events (earlier / older in the -" + nameof(EventTime) + "- sense) " +
            "being inserted into the -" + nameof(ARConcepts.EventStream) + "- " +
            "having the consequence that the root event may now evaluate to different transient events " +
            "(see -" + nameof(IRegHandlerTGTE.TryGetTransientEvents) + "-), or maybe no transient events at all.\r\n" +
            "(Note the last part, the concept \"Replaced\" may for transient events actually mean replaced with 'nothing')." +
            "\r\n" +
            "Note about marking instead of deletion:\r\n" +
            "There are two reasons for only marking as invalid, not deleting for real, because if we had deleted for real then:\r\n" +
            "\r\n" +
            "1) (most important): Navigating to an earlier -" + nameof(RegTime) + "- would not have been possible because\r\n" +
            "transient events would be gone if they were deleted for real.\r\n" +
            "\r\n" +
            "2) It would be too cumbersome to actually find the Event inside -" + nameof(PCollectionES) + "-.\r\n" +
            "If would (as of Dec 2021) be an O(n) operation.\r\n" +
            "\r\n" +
            "This naturally also means that the application will to some degree use more RAM than strictly necessary, " +
            "but only to the degree that changes are made 'retroactively' in the -" + nameof(ARConcepts.EventStream) + "-.\r\n" +
            "At application startup this can for instance be avoided by sorting the -" + nameof(ARConcepts.RegStream) + "- " +
            "by -" + nameof(EventTime) + "- before processing."
        )]
        public RegTime? ReplacedRegTime
        {
            get => _replacedRegTime;
            set
            {
                _replacedRegTime = value;
                IP.SetPV(EventP.ReplacedRegTime, value ?? throw new ArgumentNullException(
                    "It is only allowed to set -" + nameof(ReplacedRegTime) + "- to a non-null value.")
                );
            }
        }

        private List<Event>? _transientEvents;
        [ClassMember(Description =
            "Start of tree structure describing consequences of this event.\r\n" +
            "\r\n" +
            "TODO: This structure could for instance be communicated out for the last couple of events, when" +
            "TODO: a snapshot is being generated, describing how events regarding the snapshot produced also " +
            "TODO: affects other entitites.\r\n" +
            "\r\n" +
            "This structure SHOULD NOT be used for any processing purposes, other than \"nice-to-have\" " +
            "debugging purposes as described above."
        )]
        public List<Event>? TransientEvents
        {
            get => _transientEvents;
            set
            {
                _transientEvents = value;
                if (value == null)
                {
                    IP.SetP(EventP.TransientEvents, PValueEmpty.Singleton);
                }
                else
                {
                    IP.SetPV(EventP.TransientEvents, value);
                }
            }
        }

        /// <param name="regId">
        /// Only relevant for root-events, not events that are in the <see cref="ARConcepts.TransientEventTree"/>.
        /// TODO: Why is that? Why not just always include it?
        /// See also <see cref="TryGetRegIdFromRegTime"/>
        /// </param>
        public Event(RegId? regId, RegTime regTime, EventTime eventTime, RegVerb regVerb, IRegHandler regHandler, EventStatus eventStatus)
        {

            if (!(regId is null))
            {
                IP.SetPV(EventP.RegId, regId); /// Only stored in <see cref="ARConcepts.PropertyAccess"/> manner
                IsRootEvent = true;
            }

            _regTime = regTime;
            RegTime = regTime;                 /// Hybrid, stored both in classic manner and in <see cref="ARConcepts.PropertyAccess"/> manner

            _eventTime = eventTime;
            EventTime = eventTime;                   /// Hybrid, stored both in classic manner and in <see cref="ARConcepts.PropertyAccess"/> manner

            _eventStatus = eventStatus;
            EventStatus = eventStatus;               /// Hybrid, stored both in classic manner and in <see cref="ARConcepts.PropertyAccess"/> manner

            _regVerb = regVerb;
            RegVerb = regVerb;                 /// Hybrid, stored both in classic manner and in <see cref="ARConcepts.PropertyAccess"/> manner

            _regHandler = regHandler;
            RegHandler = regHandler;           /// Hybrid, stored both in classic manner and in <see cref="ARConcepts.PropertyAccess"/> manner

            // Removed 31 Jan 2022
            // IP.SetPV(EventP.RegHandlerType, regHandler.GetType()); /// Only stored in <see cref="ARConcepts.PropertyAccess"/> manner
        }

        [ClassMember(Description =
            "Returns result of -" + nameof(IRegHandler.Serialize) + "-.\r\n" +
            "\r\n" +
            "NOTE: Do NOT confuse this with ordinary -" + nameof(ARConcepts.PropertyAccess) + "- for -" + nameof(RegP.RH) + "-.\r\n" +
            "\r\n" +
            "NOTE: IMPORTANT! For registration handlers having -" + nameof(SubProcess) + "-es, the object stored here " +
            "NOTE: IMPORTANT! is not necessarily up-to-date because such registration handlers must be updated as -" + nameof(Snapshot) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-." +
            "\r\n" +
            "Use this method in order to query like 'Event/SELECT RegHandler' instead of 'Event/SELECT AH' (-" + nameof(EventP.RH) + "-) " +
            "because the latter will not show up very well because of how -" + nameof(IP.TryGetV) + "- is usually implemented.\r\n" +
            "(except for -" + nameof(SSPR) + "- because of -" + nameof(SSPRP.Value) + "-\r\n" +
            "TODO: Check text here, that still applies after Feb 2022 and changing SSPRT to use PReadOnly.\r\n" +
            "and -" + nameof(SSPRT<TPropertyKeyEnum>) + "- because of -" + nameof(SSPRT<TPropertyKeyEnum>.Value) + "-.)\r\n"
        )]
        public bool TryGetRegHandler(out PValue<string> retval, out string errorResponse)
        {
            retval = new PValue<string>(RegHandler.Serialize());
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Parameter regTime would 'normally' correspond to -" + nameof(Event.RegTime) + "- for this event, " +
            "but in case of transient events that are regenerated as a consequence of a correction to another event, " +
            "the regTime of that event should be used instead.\r\n" +
            "This is necessary in order to enable scrolling backwards in time in the -" + nameof(ARConcepts.RegStream) + "-.\r\n" +
            "\r\n" +
            "TODO: THIS NEEDS CLARIFICATION.\r\n" +
            "\r\n" +
            "TODO: If this turns out to be too complex, the parameter regTime can be removed, and one could use RegTime here instead.\r\n" +
            "TODO: A possible scenario is that we must choose between keeping this parameter, and treating events in order of\r\n" +
            "TODO: regTime, with the corresponding performance penalty, or sort events by EventTime and give up RegTime as parameter here.\r\n" +
            "\r\n" +
            "TODO: There is some good potential for reduction in memory consumption if RootEvents and TransientEvents are made into\r\n" +
            "TODO: sub classes of a BaseEvent-class, where TransientEvents can point to the RootEvent for properties like\r\n" +
            "TODO: EventTime, RegVerb and EventStatus. This together with removing RegId for TransientEvent would mean\r\n" +
            "TODO: replacing 4 objects pointers / value types with 1 one object pointer, a potential of 24 bytes pr event.\r\n" +
            "TODO: (possibly much more because the general IP structure (Currently PRich) could be eliminated from TransientEvents,\r\n" +
            "TODO: but it would entail implementing IP in TransientEvent (pointing to RootEvent's IP metods).\r\n" +
            "TODO: One could then use abstract methods in the BaseEvent class, in order for RootEvents and TransientEvents to look\r\n" +
            "TODO: identical to the outside world (TransientEvent.RegVerb for instance would point to RootEvent.RegVerb)\r\n" +
            "TODO: (that is, the whole change could be made \"invisible\" to the outside).\r\n" +
            "TODO:\r\n" +
            "TODO: Note the additional functionality that this will give, linking from transient events to root events, " +
            "TODO: something which is not possible now (Mar 2022).\r\n"
        )]
        public Event CreateTransientEvent(IRegHandler regHandler, RegTime regTime) =>
            new Event(regId: null, regTime, EventTime, RegVerb, regHandler, EventStatus);

        [ClassMember(Description =
            "Alternative ('override') to -" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "-.\r\n" +
            "\r\n" +
            "Creates HTML in the \"ordinary\" manner, and also HTML for the -" + nameof(ARConcepts.TransientEventTree) + "-\r\n" +
            "\r\n" +
            "NOTE: The mere existence of this method is not sufficient in order to avoid " +
            "-" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- being called.\r\n" +
            "(extension method will be chosen anyway if instance is of type -" + nameof(IP) + "-.\r\n" +
            "See " + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- for more details about this issue.\r\n"
        )]
#pragma warning disable IDE0079 // Remove unnecessary suppression
#pragma warning disable IDE0060 // Remove unused parameter
        public string ToHTMLSimpleSingle(bool prepareForLinkInsertion = false, List<IKCoded>? linkContext = null)
#pragma warning restore IDE0060 // Remove unused parameter
#pragma warning restore IDE0079 // Remove unnecessary suppression
        {
            try
            {
                REx.Inc(); /// Use guard here, it is too easy to mix up how <see cref="ARCDoc.Extensions.ToHTMLSimpleSingle"/> is called.
                return
                    "<h1 style=\"font-family:monospace\">" + RegHandler.Serialize().Replace("0x0020", " ") + "</h1>" +
                    (!IP.TryGetPV<RegId>(EventP.RegId, out var regId) ? "" : (
                        "<a href=\"/Process/" + RegHandler.GetType().ToStringVeryShort() + "/" + regId + "\">Edit</a>&nbsp;&nbsp;" +
                        ToInputTextboxWithSubmitButton(regId)
                    )) +
                    "<p>All earlier edits:&nbsp;&nbsp;\r\n" +
                    "<a href=\"/RQ/dt/Reg/WHERE EventTime = '" + EventTime.ToString() + "'/ORDER BY RegId DESC\">As Registrations</a>&nbsp;&nbsp;\r\n" +
                    "<a href=\"/RQ/dt/Event/WHERE EventTime = '" + EventTime.ToString() + "'/ORDER BY RegId DESC\">As Events</a>&nbsp;&nbsp;\r\n" +
                    "</p>\r\n" +
                    TransientEventTreeAsHTML() +
                    ARCDoc.Extensions.ToHTMLSimpleSingleInternal(this, prepareForLinkInsertion, linkContext: null); // Give up on linking
            }
            finally
            {
                REx.Dec();
            }
        }

        [ClassMember(Description = "See -" + nameof(ToHTMLAsTableRow) + "-.")]
        public const string ToHTMLAsTableRowHeading = "<table><tr><th>Event time</th><th>Registration time</th><th>Root</th><th>Event</th><th>Inactive from</th><th>EventStatus</th><th>ErrorResponse</th></tr>";
        [ClassMember(Description =
            "Creates an HTML representation of this event as table row with five columns, " +
            "1: -" + nameof(EventTime) + "-,\r\n" +
            "2: -" + nameof(RegTime) + "-,\r\n" +
            "3) Link to root event (if event is not root in -" + nameof(ARConcepts.TransientEventTree) + "-.\r\n" +
            "4: Some useful HTML representation, either a textbox for editing if root-event or link to root-event if transient event.\r\n" +
            "5: -" + nameof(EventP.ReplacedRegTime) + "- if given.\r\n" +
            "6. -" + nameof(EventP.EventStatus) + "-.\r\n" +
            "7. -" + nameof(EventP.ErrorResponse) + "-.\r\n"
        )]
        public string ToHTMLAsTableRow(List<IKCoded>? linkContext = null)
        {
            return
                "<tr><td>" +
                EventTime + "</td><td>" +
                RegTime + "</td><td>" + (
                    IP.TryGetPV<RegId>(EventP.RegId, out var regId) ?
                    (
                        // This is a "root"-event
                        "&nbsp;</td><td>" + ToInputTextboxWithSubmitButton(regId)
                    ) :
                    (
                        // This is not a "root"-event, link to root-event in order to be able to edit that instead.
                        "<a href=\"/RQ/dt/Event/" + EventTime.SerialNumber + "\">Root</a></td><td>\r\n" +
                        // And show only summary here.
                        RegVerb + " " + RegHandler.Serialize().Replace("0x0020", " ").Use(s => s.Length < 110 ? s :
                                    ("<span title=\"" + s + "\">" + s[..110] + "...</span>\r\n"))
                    )
                ) + "</td><td>" +
                (ReplacedRegTime is null ? "&nbsp;" : ("&nbsp;&nbsp;<span style=\"color:red\">REPLACED BY " + ReplacedRegTime.ToString() + "</span>")) + "</td><td>" +
                EventStatus.ToString() + "</td><td>" +
                (ErrorResponse == null ? "&nbsp;" : WebUtility.HtmlEncode(ErrorResponse)) +
                "</td></tr>\r\n";
        }

        [ClassMember(Description =
            "Generates an HTML INPUT-text with a corresponding INPUT-submit for editing this event.\r\n" +
            "\r\n" +
            "NOTE: After introduction of -" + nameof(ProcessController) + "- this functionality is somewhat superfluous.\r\n" +
            "\r\n"
        )]
        private string ToInputTextboxWithSubmitButton(RegId regId) =>
            // TODO: Add encoding in the browser
            "<input id=\"" + regId + "\" type=\"text\" size=80 value=\"" +
            "RS/" + RegVerb + "/" + regId + "/" + RegHandler.Serialize().Replace("0x0020", " ") +
            "\"/>\r\n" +
            "<input type=\"submit\" value=\"Change\" onclick=\"" +
                "location.href = '/' + document.getElementById('" + regId + "').value;\"/>\r\n";

        [ClassMember(Description = "Note that is always dynamically generated, not cached within the -" + nameof(Event) + "- instance.")]
        public string TransientEventTreeAsHTML()
        {
            var sb = new StringBuilder();
            TransientEventTreeAsHTMLInternalRecursive(this, level: 1, sb);
            return
                "<p>-" + nameof(ARConcepts.TransientEventTree) + "-</p>\r\n" +
                "<table><tr><th>Registration</th><th>Status</th><th>Error response</th><th>-" + nameof(EventP.ReplacedRegTime) + "-</th></tr>\r\n" +
                 sb.ToString() + "\r\n" +
                 "</table>\r\n";
        }

        private void TransientEventTreeAsHTMLInternalRecursive(Event _event, int level, StringBuilder sb)
        {
            try
            {
                REx.Inc();
                sb.Append("<tr><td>" + string.Join("", Enumerable.Repeat("&nbsp;&nbsp;&nbsp;&nbsp;", level)) +
                    // PropertyStreamLine.Decode(_event.RegHandler.Serialize()) + 
                    _event.RegHandler.Serialize().Replace("0x0020", " ").Use(s => s.Length < 130 ? s :
                        ("<span title=\"" + s + "\">" + s[..130] + "...</span>")
                    ) +
                    "</td><td>" +
                    _event.EventStatus.ToString() +
                    "</td><td>" +
                    (_event.ErrorResponse is null ? "&nbsp;" : WebUtility.HtmlEncode(_event.ErrorResponse)) +
                    "</td><td>" +
                    (_event.ReplacedRegTime is null ? "&nbsp;" : _event.ReplacedRegTime.ToString()) +
                    "</td></tr>\r\n"
                );
                if (_event.TransientEvents != null)
                {
                    foreach (var childEvent in _event.TransientEvents)
                    {
                        TransientEventTreeAsHTMLInternalRecursive(childEvent, level + 1, sb);
                    }
                }
            }
            finally
            {
                REx.Dec();
            }
        }

        [ClassMember(Description =
            "Returns -" + nameof(ARCEvent.EventTime.Time) + "- part of -" + nameof(EventTime) + "-.\r\n" +
            "\r\n" +
            "Makes for easier querying like \"Event/WHERE EventTimeTime = Today\" for instance.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetEventTimeTime(out PValue<DateTime> retval, out string errorResponse)
        {
            retval = new PValue<DateTime>(EventTime.Time);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Returns a string representation of -" + nameof(ARCEvent.EventTime) + "- useful for string based sorting.\r\n" +
            "\r\n" +
            "Created because of weakness in -" + nameof(ARCQuery.QueryExpressionOrderBy) + "- which is limited in what types it can sort.\r\n" +
            "\r\n" +
            "Note that a corresponding method for -" + nameof(RegTime) + "- is not necessary because it can be sorted by RegId which " +
            "is again recognized by -" + nameof(ARCQuery.QueryExpressionOrderBy) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetEventTimeSortable(out PValue<string> retval, out string errorResponse)
        {
            retval = new PValue<string>(
                EventTime.Time.ToStringDateAndTime() + " " +
                EventTime.SerialNumber.ToString(UtilEvent.SerialNumberFormat) + " " +
                RegTime.SerialNumber.ToString(UtilEvent.SerialNumberFormat) // Added RegTime.SerialNumber 19 Jan 2022.
            );
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Returns -" + nameof(ARCEvent.RegTime.Time) + "- part of -" + nameof(RegTime) + "-.\r\n" +
            "\r\n" +
            "Makes for easier querying like \"Event/WHERE RegTimeTime = Today\" for instance.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetRegTimeTime(out PValue<DateTime> retval, out string errorResponse)
        {
            retval = new PValue<DateTime>(RegTime.Time);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Relevant when this event is a transient event (in the -" + nameof(ARConcepts.TransientEventTree) + "-) and " +
            "it has been returned from -" + nameof(QueryExpressionTransient) + "-.\r\n" +
            "The 'EventId' then returned is 'contained' by a transient serial number, and is not useful for looking up " +
            "id of root event. Therefore this method.\r\n" +
            "\r\n" +
            "This is conceptually similar to how -" + nameof(QueryExpressionTransient.TryGetEventIdStatic) + "- works.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetEventIdFromEventTime(out PValue<EventId> retval, out string errorResponse)
        {
            // retval = new PValue<EventId>(new EventId(EventTime.SerialNumber));
            retval = new PValue<EventId>(EventTime.ToEventId);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Relevant when this event is a transient event (in the -" + nameof(ARConcepts.TransientEventTree) + "-) because " +
            "then -" + nameof(EventP.RegId) + "- has not been set.\r\n" +
            "TODO: Why is that? Why not just include RegId?\r\n" +
            "\r\n" +
            "This is conceptually similar to how -" + nameof(QueryExpressionTransient.TryGetRegIdStatic) + "- works.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetRegIdFromRegTime(out PValue<RegId> retval, out string errorResponse)
        {
            // retval = new PValue<RegId>(new RegId(RegTime.SerialNumber));
            retval = new PValue<RegId>(RegTime.ToRegId);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString()
        {
            return RegHandler.GetType().ToStringVeryShort();
        }
    }

    public class EventCollection : PCollection
    {

    }

    [Enum(
    Description = "Describes class -" + nameof(Event) + ".",
    AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum EventP
    {
        __invalid,

        [PKType(Type = typeof(EventTime))]
        EventTime,

        [PKType(Type = typeof(RegTime))]
        RegTime,

        [PKType(
            Description =
                "Pointer to corresponding -" + nameof(Reg) + "- which this -" + nameof(Event) + "- resulted from.\r\n" +
                "Used for report / debugging purposes, not necessary in itself for -" + nameof(ARConcepts.EventSourcing) + "-.\r\n" +
                "\r\n" +
                "Note that only set for the root-event in the -" + nameof(ARConcepts.TransientEventTree) + "-, not for the rest.\r\n",
            Type = typeof(RegId), IsObligatory = false
        )]
        RegId,

        [PKType(
            Description = "See -" + nameof(Event.RegVerb) + "- for documentation",
            Type = typeof(RegVerb))]
        RegVerb,

        // Removed 31 Jan 2022
        //[PKType(
        //    Description = "See -" + nameof(RegP.RegHandlerType) + "- for documentation",
        //    Type = typeof(Type))]
        //RegHandlerType,

        [PKType(
            Description =
                "See -" + nameof(RegP.RH) + "- for documentation.\r\n" +
                "\r\n" +
                "NOTE: IMPORTANT! For registration handlers having -" + nameof(SubProcess) + "-es, the object stored here " +
                "NOTE: IMPORTANT! is not necessarily up-to-date because such registration handlers must be updated as -" + nameof(Snapshot) + "-.\r\n" +
                "\r\n" +
                "See also -" + nameof(Event.RegHandler) + "-." +
                "\r\n" +
                "See also -" + nameof(Event.TryGetRegHandler) + "- which returns -" + nameof(IRegHandler.Serialize) + "-.\r\n",
            Type = typeof(IRegHandler)
        )]
        RH,

        [PKType(
            Description = "See -" + nameof(Event.EventStatus) + "- for documentation",
            Type = typeof(EventStatus)
        )]
        EventStatus,

        [PKType(
            Description = "See -" + nameof(Event.ErrorResponse) + "- for documentation"
        )]
        ErrorResponse,

        [PKType(
            Description = "See -" + nameof(Event.TransientEvents) + "- for documentation",
            Type = typeof(Event), Cardinality = Cardinality.WholeCollection
        )]
        TransientEvents,

        [PKType(
            Description = "See -" + nameof(Event.ReplacedRegTime) + "- for documentation",
            Type = typeof(RegTime)
        )]
        ReplacedRegTime,

    }

    [Class(Description =
        "Points to a time on the -" + nameof(ARConcepts.EventStream) + "- \"axis\".\r\n" +
        "\r\n" +
        "The -" + nameof(SerialNumber) + "- comes from the corresponding -" + nameof(ARCEvent.RegTime.SerialNumber) + "-" +
        "its purpose is to sort events \"happening at the same time\" in some logical order, which is assumed to be \r\n" +
        "the order in which they appear in the -" + nameof(ARConcepts.RegStream) + "-.\r\n" +
        "\r\n" +
        "This means that this class actually identifies unique -" + nameof(Event) + "-s, except for " +
        "duplicates with different -" + nameof(RegVerb) + "- like -" + nameof(RegVerb.BLOCK) + "- " +
        "and -" + nameof(RegVerb.ALLOW) + "-." +
        "\r\n" +
        "The use of a serial number means that time points always have an exact meaning, " +
        "regardless of the -" + nameof(StreamProcessorP.TimestampResolution) + "-  used.\r\n" +
        "For instance '07:15 5' means exact time '07:15:00.000 1', so if an event occurs as '07:15:01 2', querying for '07:15 1'\r\n" +
        "gives meaning, there is no ambiguity with regard to what 07:15 means\r\n" +
        "(questions like 'does it mean 07:15 exact or the interval [07:15, 07:16> never arise).\r\n" +
        "\r\n" +
        "Example of how Events are sorted with regard to Registration:\r\n" +
        "\r\n" +
        "Example 1) Two Registrations which become two separate events, " +
        "scrollable back and forth in -" + nameof(EventTime) + "- and -" + nameof(RegTime) + "-:\r\n" +
        "\r\n" +
        "Original -" + nameof(ARConcepts.RegStream) + "-:\r\n" +
        "07:15/DO/SSPR/Customer 42/FirstName/John (executed at 07:21, serial number 1)\r\n" +
        "07:15/DO/SSPR/Customer 42/FirstName/Johnny (executed at 07:22, serial number 2)\r\n" +
        "\r\n" +
        "Resulting -" + nameof(ARConcepts.EventStream) + "- (EventTime, RegTime, RegVerb, RegHandler):\r\n" +
        "07:15 1/07:21 1/DO/SSPR/Customer 42/FirstName/John\r\n" +
        "07:15 2/07:22 2/DO/SSPR/Customer 42/FirstName/Johnny\r\n" +
        "\r\n" +
        "Example 2) Two Registrations which become a single event, the second cancels the first, " +
        "scrollable back and forth in -" + nameof(RegTime) + "- only:\r\n" +
        "\r\n" +
        "Original -" + nameof(ARConcepts.RegStream) + "-:\r\n" +
        "07:15/DO/SSPR/Customer 42/FirstName/John (executed at 07:21, serial number 1)\r\n" +
        "1/DO/SSPR/Customer 42/FirstName/Johnny (executed at 07:22, serial number 2)\r\n" +
        "\r\n" +
        "Note how the second Registration identifies EvenTime as the EventTime of Registration with serial number 1.\r\n" +
        "\r\n" +
        "Resulting -" + nameof(ARConcepts.EventStream) + "- (EventTime, RegTime, RegVerb, RegHandler):\r\n" +
        "07:15 1/07:21 1/DO/SSPR/Customer 42/FirstName/John\r\n" +
        "07:15 1/07:22 2/DO/SSPR/Customer 42/FirstName/Johnny\r\n" +
        "\r\n" +
        "Note how the second event gets stored with the -" + nameof(EventTime) + "- of the original event.\r\n" +
        "When these changes are applied by -" + nameof(PCollectionES) + "- it sees that the event times are identical, and only the " +
        "latest one gets applied.\r\n" +
        "\r\n" +
        "The same principle is used for other -" + nameof(RegVerb) + "-s like -" + nameof(RegVerb.UNDO) + "-.\r\n" +
        "\r\n" +
        "Some special meanings are directly understood by the -" + nameof(TryParse) + "- method:\r\n" +
        "'bot' = Beginning of time, DateTime.MinValue + serial number 0 (currently of little use).\r\n" +
        "'eot' = End of time, DateTime.MaxValue + serial number Long.MaxValue.\r\n" +
        "'now' = The current time (from -" + nameof(UtilCore.DateTimeNow) + "-), rounded UP (forward in time) " +
        "to the nearest -" + nameof(StreamProcessorP.TimestampResolution) + "- interval with serial number 0.\r\n" +
        "\r\n" +
        "NOTE:\r\n" +
        "Since an instance of this class is stored for EVERY -" + nameof(Event) + "-\r\n" +
        "it is tempting to reduce memory usage here (8 bytes per object) by storing the serialnumber within Ticks " +
        "of the Time-object, and restrict Time to only whole seconds.\r\n" +
        "\r\n" +
        "There are 10 000 000 ticks in a second so we could have stored \"SerialNumber % 10 000 000\", \r\n" +
        "however, we must be careful with wrap-around bugs for this module counter.\r\n" +
        "So it is considered too dangerous for the time being.\r\n"
    )]
    public class EventTime : IEquatable<EventTime>, IComparable, ITypeDescriber
    {
        public DateTime Time { get; private set; }

        [ClassMember(Description =
            "Originates from corresponding field in -" + nameof(RegTime) + "-.-" + nameof(RegTime.SerialNumber) + "-."
        )]
        public long SerialNumber { get; private set; }

        [ClassMember(Description = "BOT = Beginning of time")]
        public static EventTime BOT = new EventTime(DateTime.MinValue, 0);
        [ClassMember(Description =
            "TODO: Should probably be split into EOTAsQuery and EOTAs???? in order to distingiush between " +
            "TODO: adding items at EOT and querying at EOT.\r\n" +
            "\r\n" +
            "EOT = End of time, suitable for querying (retrieving all Events in -" + nameof(ARConcepts.EventStream) + "-.\r\n"
        )]
        public static EventTime EOT = new EventTime(DateTime.MaxValue, long.MaxValue);

        public EventTime(DateTime time, long serialNumber)
        {
            Time = time;
            SerialNumber = serialNumber;
        }

        public static bool operator >(EventTime a, EventTime b)
            => (a.Time > b.Time) || (a.Time == b.Time && a.SerialNumber > b.SerialNumber);

        public static bool operator <(EventTime a, EventTime b)
            => (a.Time < b.Time) || (a.Time == b.Time && a.SerialNumber < b.SerialNumber);

        public static bool operator ==(EventTime a, EventTime b)
            => (a.Time == b.Time) && (a.SerialNumber == b.SerialNumber);

        public static bool operator !=(EventTime a, EventTime b)
            => (a.Time != b.Time) || (a.SerialNumber != b.SerialNumber);

        public static bool operator >=(EventTime a, EventTime b)
            => a > b || a == b;

        public static bool operator <=(EventTime a, EventTime b)
            => a < b || a == b;

        public bool Equals(EventTime other) => Time == other.Time && SerialNumber == other.SerialNumber;
        public override bool Equals(object other) => other is EventTime otherAsEeventTime && this == otherAsEeventTime;
        public override int GetHashCode() => (int)(SerialNumber % int.MaxValue);

        [ClassMember(Description =
            "'Smart' way of showing time. Removes unnecessary information.\r\n " +
            "\r\n" +
            "TODO: If this turns out to be too smart (remember, it must also fit together with -" + nameof(TryParse) + "- for instance),\r\n" +
            "TOOD: then we can always make it dumber. The consequence will be more digits presented to the human user of the system however.\r\n" +
            "TODO: NOTE: A better solution is probably to introduce ToStringSmart, and let ToString work against TryParse.\r\n" +
            "\r\n" +
            "Note how uses -" + nameof(FunctionKeyShort) + "-.-" + nameof(FunctionKeyShort.Short) + "-.\r\n" +
            "\r\n" +
            "TODO: Include 'bot' and 'eot'."
        )]
        public override string ToString() =>
            FunctionKeyShort.Short(Time) + " " +
            SerialNumber.ToString();

        [JsonIgnore] // Ignore because information is same as SerialNumber
        public EventId ToEventId => new EventId(SerialNumber);

        public int CompareTo(Object other)
        {
            if (!(other is EventTime otherAsEventTime)) throw new InvalidObjectTypeException(other, typeof(EventTime));
            var retval = Time.CompareTo(otherAsEventTime.Time);
            if (retval != 0) return retval;
            return SerialNumber.CompareTo(otherAsEventTime.SerialNumber);
        }

        [ClassMember(Description = "See -" + nameof(UtilEvent) + "-.-" + nameof(UtilEvent.NowAsQuery) + "- for explanation.\r\n")]
        public static EventTime NowAsQuery() => new EventTime(UtilEvent.NowAsQuery(), 0);

        [ClassMember(Description =
            "bot = 'Beginning of time'\r\n" +
            "\r\n" +
            "This Event time can be used for setting properties that are static / always valid.\r\n" +
            "\r\n" +
            "TODO: Some ideas about handling 'bot' events:\r\n" +
            "TODO: 'bot' events can be persisted in a separate file which is read first at startup.\r\n" +
            "TODO: This will avoid spool-back issues at application startup, this file will be read first,\r\n" +
            "TODO: before the other ones.\r\n" +
            "TOOD: Such a separate file would also make for very easy editing (in an ordinary text-editor) of\r\n" +
            "TOOD: the initial setup for the world being modelled.\r\n" +
            "TODO:\r\n" +
            "TODO: Also, new 'bot' events could be treated as 'now' events memory wise when application is running,\r\n" +
            "TODO: thus avoiding spool back to 'bot' each time one is received.\r\n"
        )]
        public bool IsBot() => Time.Equals(DateTime.MinValue) && SerialNumber.Equals(0L);
        public bool IsEot() => Time.Equals(DateTime.MaxValue) && SerialNumber.Equals(long.MaxValue);

        public static bool TryParse(string value, out EventTime retval, out string errorResponse)
        {
            switch (value)
            {
                case "bot":
                    retval = new EventTime(DateTime.MinValue, serialNumber: 0);
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
                case "eot":
                    retval = new EventTime(DateTime.MaxValue, serialNumber: long.MaxValue);
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
                case "now":
                    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse =
                        "'now' not understood by general parser for -" + nameof(EventTime) + "-.\r\n" +
                        "\r\n" +
                        "Resolution: Try to remove parameter altogether and see if reasonable default will be chosen instead.\r\n" +
                        "\r\n" +
                        "Reason: We are unable to decide here what 'now' means (round backwards or forwards within time period).\r\n" +
                        "For instance, for -" + nameof(RSController) + "- rounding backwards would be logical, while for -" + nameof(ESController) + "- " +
                        "rounding forwards would be logical (see -" + nameof(EventTime.NowAsQuery) + "-).";
                    return false;
                /// TODO: Add parameter to TryParse in order to solve this.
                /// NOTE: Commentd out code below illustrates problem.
                //case "now":
                //    retval = new EventTime(UtilEvent.RoundDateTimeBackwards, serialNumber: 0);
                //    retval = new EventTime(UtilEvent.RoundDateTimeForwards, serialNumber: 0);
                //    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                //    return false;
                default:
                    var t = value.Split(" ");
                    if (t.Length == 1)
                    {
                        if (!ValueComparerDateTime.TryParse(t[0], out var vc, out errorResponse))
                        {
                            retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> ; 
                            errorResponse =
                                "Invalid value, since 1 part tried to parse as -" + nameof(ValueComparerDateTime) + "- " +
                                "with result:\r\n" + errorResponse;
                            return false;
                        }
                        else
                        {
                            // TODO: MAYBE make something simpler than ValueComparerDateTime which can give a point in time only
                            // TODO: The class is a bit overkill for what we need here
                            // TODO: On the other hand, it works quite well as it is now...
                            retval = new EventTime(vc.StartPeriod, serialNumber: 0);
                            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            return true;
                        }
                    }
                    if (t.Length == 2)
                    {
                        /// Allow only time part or only date part to be specified 
                        /// (see correspondingly <see cref="ToString"/> and use of <see cref="FunctionKeyShort.Short"/>)
                        if (t[0].Length >= 3 && t[0][2] == ':')
                        {
                            // Looks like only time part was specified, add date part
                            t = new string[] { UtilCore.DateTimeNow.ToString("yyyy-MM-dd"), t[0], t[1] };
                            // TOOD: This is not a very efficient approach, constructing a string and then parsing it again
                        }
                        else if (t[0].Length >= 5 && t[0][4] == '-')
                        {
                            // Looks like only date part was specified, add time part
                            t = new string[] { t[0], UtilCore.DateTimeNow.ToString("HH:mm:ss.fff"), t[1] };
                            // TOOD: This is not a very efficient approach, constructing a string and then parsing it again
                        }
                    }
                    if (t.Length != 3)
                    {
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> ; 
                        errorResponse = "Not three parts (date, time, serial number), separated by space but " + t.Length;
                        return false;
                    }
                    if (!UtilCore.DateTimeTryParse(t[0] + " " + t[1], out var dateTime))
                    {
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> ; 
                        errorResponse = "Invalid DateTime part (" + t[0] + " " + t[1] + ")";
                        return false;

                    }
                    if (!long.TryParse(t[2], out var serialNumber))
                    {
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> ; 
                        errorResponse = "Invalid SerialNumber part (" + t[2] + ")";
                        return false;
                    }
                    retval = new EventTime(dateTime, serialNumber);
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
            }
        }

        [ClassMember(Description = "'Implementing' -" + nameof(ITypeDescriber) + "-.")]
        public static void EnrichKey(PK k) =>
            k.SetValidatorAndParser(new Func<string, ParseResult>(value =>
                TryParse(value, out var retval, out var errorResponse) ?
                    ParseResult.CreateOK(retval) :
                    ParseResult.CreateError(errorResponse)
            ));
    }

    [Class(Description =
        "Strong typing of primary keys reduces risk of mix-ups in the code.\r\n" +
        "\r\n" +
        "Consists of the -" + nameof(RegTime.SerialNumber) + "- of the -" + nameof(Reg) + "- leading to this event.\r\n" +
        "\r\n" +
        "Not relevant for events in the -" + nameof(ARConcepts.TransientEventTree) + "- (transient events)."
    )]
    public class EventId : IKLong
    {

        public EventId(long value) : base(value) { }

        // Parse / TryParse according to AgoRapide standard. Looks like overkill of course in our case
        public static new EventId Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidEventIdException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse + "\r\n");
        public static bool TryParse(string value, out EventId retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out EventId retval, out string errorResponse)
        {
            if (string.IsNullOrEmpty(value))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = "Null or Empty value (" + value + ")";
                return false;
            }
            if (!long.TryParse(value, out var valueAsLong))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = "Invalid as long.";
                return false;
            }
            retval = new EventId(valueAsLong);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
            return true;
        }


        [ClassMember(Description = "'Implementing' -" + nameof(ITypeDescriber) + "-.")]
        public static new void EnrichKey(PK k) =>
            k.SetValidatorAndParser(new Func<string, ParseResult>(value =>
                TryParse(value, out var retval, out var errorResponse) ?
                    ParseResult.CreateOK(retval) :
                    ParseResult.CreateError(errorResponse)
            ));

        public class InvalidEventIdException : ApplicationException
        {
            public InvalidEventIdException(string message) : base(message) { }
            public InvalidEventIdException(string message, Exception inner) : base(message, inner) { }
        }
    }

    [Enum(AREnumType = AREnumType.OrdinaryEnum)]
    public enum EventStatus
    {
        __invalid,

        [EnumMember(Description =
            "Event has not been attempted processed yet."
        )]
        NEW,

        [EnumMember(Description =
            "Event is blocked because of an explicit request from the user / API client (see -" + nameof(RegVerb.BLOCK) + "-).\r\n" +
            "\r\n" +
            "Event will appear in the -" + nameof(ARConcepts.EventStream) + "- but will not be sent to -" + nameof(IRegHandlerTAERTE.TryApplyEventResultToEntity) + "-."
        )]
        BLOCKED_USER,

        [EnumMember(Description =
            "Event is blocked by default (set by -" + nameof(IRegHandlerTGTE.TryGetTransientEvents) + "-.)" +
            "\r\n" +
            "Event will appear in the -" + nameof(ARConcepts.EventStream) + "- but will not be sent to -" + nameof(IRegHandlerTAERTE.TryApplyEventResultToEntity) + "-."
        )]
        BLOCKED_DEFAULT,

        [EnumMember(Description =
            "Event is blocked because the user doing the Registration do not have the necessary access rights.\r\n" +
            "\r\n" +
            "Event will appear in the -" + nameof(ARConcepts.EventStream) + "- but will not be sent to -" + nameof(IRegHandlerTAERTE.TryApplyEventResultToEntity) + "-.\r\n" +
            "\r\n" +
            "When appearing in the -" + nameof(ARConcepts.TransientEventTree) + "-, this status can be seen as a task " +
            "for a supervisor.\r\n" +
            "The supervisor can then either -" + nameof(RegVerb.ALLOW) + "- or -" + nameof(RegVerb.BLOCK) + "- the Registration.\r\n"
        )]
        BLOCKED_NO_RIGHT,

        [EnumMember(Description =
            "Event is blocked due to there not beeing sufficient information in order to complete the Registration.\r\n" +
            "\r\n" +
            "Event will appear in the -" + nameof(ARConcepts.EventStream) + "- but will not be sent to -" + nameof(IRegHandlerTAERTE.TryApplyEventResultToEntity) + "-.\r\n" +
            "\r\n" +
            "This is an alternative to the API rejecting requests outright.\r\n"
        )]
        BLOCKED_INSUFICCIENT_INFORMATION,

        [EnumMember(Description =
            "The Event must wait for all -" + nameof(SubProcess) + "-es to complete before it can evaluate a total result.\r\n" +
            "\r\n" +
            "See also -" + nameof(EXEcuteR) + "-.\r\n"
        )]
        WAIT_FOR_EXEC,

        OK,

        ERROR
    }
}
