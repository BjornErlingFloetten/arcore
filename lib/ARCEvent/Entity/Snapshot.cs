﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using ARCCore;
using ARCDoc;
using ARCQuery;
using ARCAPI;
using System.Linq;

namespace ARCEvent
{

    [Class(Description =
        "Wrapper around another -" + nameof(IP) + "-.\r\n" +
        "\r\n" +
        "Populated by -" + nameof(PCollectionES) + "-.\r\n" +
        "\r\n" +
        "Note a relatively high memory impact of this wrapper due to storage of a considerable amount of " +
        "convenience properties. These properties do make debugging much easier however.\r\n" +
        "\r\n" +
        "In general the -" + nameof(Snapshot) + "- concept is best suited for an application " +
        "with lots of -" + nameof(Reg) + "-s / -" + nameof(Event) + "-s, but relatively few entities for which " +
        "to create snapshots.\r\n" +
        "\r\n"
    )]
    public class Snapshot : PExact<SnapshotP>
    {
        private static readonly int capacity = Enum.GetNames(typeof(SnapshotP)).Length - 1;
        public Snapshot() : base(capacity) { }

        [ClassMember(Description =
            "Used in conjunction with -" + nameof(PCollectionES.SetEventTimeMonotoneIncreasingGuarantee) + "-.\r\n" +
            "Necessary when -" + nameof(SnapshotP.EventTime) + "- is not sufficient in order to decide whether " +
            "a given snapshot is up-to-date (when, within processing of a single root event, an event is stored, " +
            "a snapshot is generated and then a new event is stored).\r\n" +
            "Ordinary, -" + nameof(PCollectionES.TryStoreEvent) + "- will delete any existing snapshots with identical " +
            "EventTime, but not with -" + nameof(PCollectionES.SetEventTimeMonotoneIncreasingGuarantee) + "-.\r\n" +
            "\r\n" +
            "IMPORTANT: If this value is set, then snapshot can not be used outside of " +
            "-" + nameof(PCollectionES.SetEventTimeMonotoneIncreasingGuarantee) + "- because it may be incomplete " +
            "for its given event time (atomicity within Event time does not necessarily hold.\r\n" +
            "\r\n" +
            "TODO: If becomes permanent, store with ordinary PropertyAccess maybe."
        )]
        public int IndexLastEventIncluded = -1;

        // Convenience getters due to AgoRapide's admittedly somewhat convoluted syntax
        public DateTime CreatedTime => IP.GetPV<DateTime>(SnapshotP.CreatedTime);
        public EventTime EventTime => IP.GetPV<EventTime>(SnapshotP.EventTime);
        // Not always set, do not offer here (or at least mark as nullable and use TryGetPV)
        // public RegTime RegTime => IP.GetPV<RegTime>(SnapshotP.RegTime);
        public IP Entity => IP.GetP<IP>(SnapshotP.Entity);

        private long _accessCount;
        public long AccessCount { get => _accessCount; }
        [ClassMember(Description =
            "Returns number of times this Snapshot has been accessed through -" + nameof(PCollectionES) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetAccessCount(out PValue<long> retval, out string er)
        {
            retval = new PValue<long>(_accessCount);
            er = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }
        [ClassMember(Description =
            "Note that we can not store Access count / Last access through ordinary -" + nameof(ARConcepts.PropertyAccess) + "- " +
            "because it would have entailed writing to a Dictionary under a multithreaded scenario with no write locking.\r\n" +
            "\r\n" +
            "This method will also update value returned by -" + nameof(LastAccess) + "- / -" + nameof(TryGetLastAccess) + "-.\r\n" 
        )]
        public void IncreaseAccessCount()
        {
            System.Threading.Interlocked.Increment(ref _accessCount);
            _lastAccess = UtilCore.DateTimeNow;
        }

        private DateTime _lastAccess;
        public DateTime LastAccess { get => _lastAccess; }
        [ClassMember(Description =
            "Returns number of times this Snapshot has been accessed through -" + nameof(PCollectionES) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetLastAccess(out PValue<DateTime> retval, out string er)
        {
            retval = new PValue<DateTime>(_lastAccess);
            er = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public bool TryGetEntity<T>(out T entity, out string errorResponse) where T : IP
        {
            var e = Entity;
            if (!(e is T temp))
            {
                entity = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Found " + e.GetType().ToStringShort() + ", expected " + typeof(T).ToStringShort();
                return false;
            }
            entity = temp;
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Alternative ('override') to -" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "-.\r\n" +
            "\r\n" +
            "Creates HTML for both the -" + nameof(Snapshot) + "- and the contained -" + nameof(Entity) + "-.\r\n" +
            "and also hints for navigation backwards and forwards in time on the " +
            "-" + nameof(ARConcepts.RegStream) + "- and -" + nameof(ARConcepts.EventStream) + ".\r\n" +
            "\r\n" +
            "NOTE: The mere existence of this method is not sufficient in order to avoid " +
            "-" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- being called.\r\n" +
            "(extension method will be chosen anyway if instance is of type -" + nameof(IP) + "-.\r\n" +
            "See " + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- for more details about this issue.\r\n"
        )]
        public string ToHTMLSimpleSingle(bool prepareForLinkInsertion = false, List<IKCoded>? linkContext = null)
        {
            try
            {
                REx.Inc(); /// Use guard here, it is too easy to mix up how <see cref="ARCDoc.Extensions.ToHTMLSimpleSingle"/> is called.

                // TODO: Links do not work when accessed through ESController, and since Snapshots are not accessible through
                // TODO: RQController, we can ignore linking altogether here.
                // TODO: But, fix original problem with linking.
                prepareForLinkInsertion = false;

                // TODO: Fix this
                // var toRoot = string.Join("", Enumerable.Repeat("../", (Math.Max(0, linkContext?.Count - 1 ?? 0))));

                var now = UtilCore.DateTimeNow;
                // var dateTimeToString = new Func<DateTime, string>(d => d.Date == now.Date ? d.ToString("HH:mm:ss.fff") : d.ToStringDateAndTime());

                // Create link for navigating in the 2D Property Stream
                var linkCreator = new Func<string, string, int, string>((url, text, position) =>
                {
                    if (linkContext == null) return WebUtility.HtmlEncode(text);
                    var copy = linkContext.Select(l => l.Encoded.ToString()).ToList(); // Do not change in-parameter
                    copy[position] = "now".Equals(text) ? "now" : url; // now is fine, and more readable.

                    // return "<a href=\"" + toRoot + string.Join("/", copy.Select(l => l.ToString())) + "\">" +
                    return "<a href=\"/ES/" + string.Join("/", copy.Select(l => l.ToString())) + "\">" +
                        WebUtility.HtmlEncode(text) +
                    "</a>";

                });

                var eventTimes = new SortedDictionary<EventTime, string>
                {
                    {  new EventTime(now, 0), "now" }
                };
                if (IP.TryGetPV<EventTime>(SnapshotP.EventTime, out var et))
                {
                    eventTimes.TryAdd(et, "This");
                }
                if (IP.TryGetPV<EventTime>(SnapshotP.PreviousEventTime, out et))
                {
                    eventTimes.TryAdd(et, "<= Previous");
                }
                if (IP.TryGetPV<EventTime>(SnapshotP.NextEventTime, out et))
                {
                    eventTimes.TryAdd(et, "=> Next ");
                }

                var regTimes = new SortedDictionary<RegTime, string>
                {
                    {  new RegTime(now, 0), "now" }
                };
                if (IP.TryGetPV<RegTime>(SnapshotP.RegTime, out var at))
                {
                    regTimes.TryAdd(at, "This");
                }
                if (IP.TryGetPV<RegTime>(SnapshotP.PreviousRegTime, out at))
                {
                    regTimes.TryAdd(at, "^ Previous");
                }
                if (IP.TryGetPV<RegTime>(SnapshotP.NextRegTime, out at))
                {
                    regTimes.TryAdd(at, "v Next");
                }

                var entityKey = (!(linkContext is null) && linkContext.Count == 5) ?
                    linkContext[4].Unencoded :
                    throw new SnapshotException(
                        "Unable to find the entity key for this snapshot.\r\n" +
                        "The entity key is used in a quite narrow context, getting values for -" + nameof(EntityMethodKey) + "-s.\r\n" +
                        "This should probably be quite easy to fix."
                    );

                return
                    "<hr><p>-" + nameof(Snapshot) + "- information:</p>\r\n" +
                    (!IP.ContainsKey(PK.FromEnum(SnapshotP.RegTime)) ? "" : ("<p style=\"color:red\">" +
                        "NOTE: -" + nameof(SnapshotP.RegTime) + "- was specified in query for this -" + nameof(Snapshot) + "-.\r\n" +
                        "This is an 'expensive' process with no caching of result. " +
                        "Leave out -" + nameof(SnapshotP.RegTime) + "- (set blank) for a faster query." +
                    "<p>\r\n")) +

                    "<p>Access count: " + AccessCount + "</p>\r\n" +

                    /// Note: Setting linkContext to null will create links starting with '/RQ/dt...', that is, they will be correct 
                    /// regardless of this page originating from <see cref="RQController"/> or <see cref="ESController"/>
                    ARCDoc.Extensions.ToHTMLSimpleSingleInternal(this, prepareForLinkInsertion, linkContext: null) +

                    "\r\n\r\n" +
                    "<hr><p>Entity information:</p>\r\n" +

                    Entity.ToHTMLSimpleSingle(prepareForLinkInsertion: true, linkContext) + 

                    // TODO: Find a way of putting these into the JSON generated also
                    (GlobalDataStorageDt is null ? "" :
                    EntityMethodKey.AllEntityMethodKeysForEntityTypeList(Entity.GetType()).Use(list =>
                        list.Count == 0 ? "" : (
                            "<p>-" + nameof(EntityMethodKey) + "-</p>\r\n" +
                            "<table><tr><th>Key</th><th>Value</th></tr>\r\n" +
                            string.Join("", list.Select(entityMethodKey =>
                            {                                
                                var entityMethodKeyAsIk = IKString.FromCache(entityMethodKey.ToString());
                                var helpTextHTML = entityMethodKey.Attribute.GetHelptextHTML();
                                return !entityMethodKey.TryGetPInternal(new IKIP(entityKey, Entity), GlobalDataStorageDt, out var p, out _) ? "" : (
                                    ARCDoc.Extensions.ToHTMLSimpleAsTableRow(new IKIP(entityMethodKeyAsIk, p), strLinkContext: "/TODO:/", prepareForLinkInsertion, 
                                        keyToHTMLSimpleWithTooltip: string.IsNullOrEmpty(helpTextHTML) ? null : (
                                            "<span title=\"" + helpTextHTML + "\">" + WebUtility.HtmlEncode(entityMethodKeyAsIk.ToString()) + "</span>"
                                    )) 
                                );
                            })) +
                            "</table>\r\n"
                        )
                    )) +
                    "<hr><p>Events constituting this entity:</p>\r\n" +
                    Event.ToHTMLAsTableRowHeading +
                    string.Join("\r\n", IP.GetPV<List<Event>>(SnapshotP.Events).Select(e =>  // Note: Do not sort here in any manner, use ordering which snapshot was constructed from
                        e.ToHTMLAsTableRow(linkContext)
                    ).Reverse()) +
                    "</table>\r\n" +
                    "\r\n" +
                    "<hr><p>2D Property Stream navigation</p>\r\n" +
                    "<p>" + string.Join("", Enumerable.Repeat("&nbsp;", 30)) + "-" + nameof(ARConcepts.EventStream) + "-: " +
                    // TODO: This became maybe a bit to complex
                    string.Join("", eventTimes.Select(et => ("This".Equals(et.Value) ? "<b>" : "") + et.Value + ("This".Equals(et.Value) ? "</b>" : "") + " " + linkCreator(
                        et.Key.ToString(), "now".Equals(et.Value) ? "now" : et.Key.ToString(), 1 // One is the position in the URL for EventTime
                    ) + "&nbsp;&nbsp;\r\n")) +
                    linkCreator("eot", "'end-of-time'", 1) +
                    "</p>\r\n" +
                    "<p>-" + nameof(ARConcepts.RegStream) + "- (Registration stream):<br>" +
                    // TODO: This became maybe a bit to complex
                    string.Join("", regTimes.Select(et => linkCreator(
                        et.Key.ToString(), "now".Equals(et.Value) ? "now" : et.Key.ToString(), 2 // Two is the position in the URL for RegTime
                    ) + " " + ("This".Equals(et.Value) ? "<b>" : "") + et.Value + ("This".Equals(et.Value) ? "</b>" : "") + "<br>\r\n")) +
                    // Note: eot / 'end-of-time' is not relevant for Registration.
                    "</p>\r\n" +
                    "<p><span title=\"" + WebUtility.HtmlEncode(
                        // TODO: Put this text to somewhere more well defined.
                        "NOTE: There is an inherent limitation when changing RegTime / 'scrolling' along the RegTime 'axis'.\r\n" +
                        "If a newer Registration generates Transient events with EventTime BEFORE the EventTime of some other Registration, and these Transient events " +
                        "are used by that other Registration as input for new Transient events, then the scrolling will NOT effect this (although it 'should' have)."
                    ) + "\">*Limitations when changing RegTime*</span></p>" +

                    "<hr><p>Events discarded (because replaced with other events):</p>\r\n" +
                    Event.ToHTMLAsTableRowHeading +
                    string.Join("\r\n", IP.GetPV<List<Event>>(SnapshotP.EventsDiscarded).Select(e =>  // Note: Do not sort here in any manner, use ordering which snapshot was constructed from
                        e.ToHTMLAsTableRow(linkContext)
                    ).Reverse()) +
                    "</table>\r\n" +
                    "\r\n"
                    ;
            }
            finally
            {
                REx.Dec();
            }
        }

        [ClassMember(Description = 
            "The -" + nameof(PSPrefix.dt) + "- part of the global data storage.\r\n" +
            "\r\n" +
            "Used in quite a narrow context in order to find values for -" + nameof(EntityMethodKey) + "-s\r\n" +
            "\r\n" +
            "TODO: Find some better initialization here."
        )]
        public static IP? GlobalDataStorageDt = null;

        public class SnapshotException : ApplicationException
        {
            public SnapshotException(string message) : base(message) { }
            public SnapshotException(string message, Exception inner) : base(message, inner) { }
        }
    }


    [Enum(
        // Description = "Describes class -" + nameof(Snapshot<TIPES>) + "-.",
        Description = "Describes class -" + nameof(Snapshot) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum SnapshotP
    {
        __invalid,
        
        /// TODO: DO NOT USE THIS. Will entail writing to Dictionary under a multithreaded scenario with no write locking
        /// TODO: Consider using <see cref="PConcurrent"/> as base for Snapshot, but take into account memory usage 
        /// TODO: Note: If we can assume to have few number of entities (tens of thousands instead of tens of millions), 
        /// TODO: ConcurrentDictionary's memory consumptiom would not be a problem.
        //[PKType(
        //    Description = "The time of the last access of this snapshot.",
        //    Type =typeof(DateTime)
        //)]
        //LastAccessedAt,

        [PKType(Description =
            "Textual description of the process for creating the snapshot.\r\n" +
            "\r\n" +
            "TODO: Not in use as of Dec 2021."
        )]
        Description,

        [PKType(Type = typeof(DateTime))]
        CreatedTime,

        /// Probably unnecessary, see instead <see cref="Snapshot.IndexLastEventIncluded"/>
        //[PKType(
        //    Description =
        //        "Serial number (within event collection for this entity) for last event included in the snapshot.\r\n" +
        //        "\r\n" +
        //        "Note: A global event serial number is probably found within -" + nameof(EventTime) + "-.",
        //    Type = typeof(long)
        //)]
        //LastEntityEventIndex,

        [PKType(
            Description = "The timestamp along the -" + nameof(ARConcepts.EventStream) + "- \"axis\" for which this snapshot is valid.",
            Type = typeof(EventTime)
        )]
        EventTime,

        [PKType(
            Description = "The timestamp of the snapshot upon which this snapshot was based.",
            Type = typeof(EventTime)
        )]
        BaseSnapshotEventTime,

        [PKType(
            Description = "The number of new events that where processed when creating this snapshot (compared to snapshot that we are based on).",
            Type = typeof(long)
        )]
        NewEventsProcessed,

        [PKType(
            Description =
                "The next timestamp along the -" + nameof(ARConcepts.EventStream) + "- \"axis\" when this snapshot will change.\r\n" +
                "Only relevant if snapshot was queried with -" + nameof(RegTime) + "-.\r\n",
            Type = typeof(EventTime)
        )]
        NextEventTime,

        [PKType(
            Description =
                "The previous timestamp along the -" + nameof(ARConcepts.EventStream) + "- \"axis\" when this snapshot was changed.\r\n" +
                "Only relevant if snapshot was queried with -" + nameof(RegTime) + "-.\r\n",
            Type = typeof(EventTime)
        )]
        PreviousEventTime,

        [PKType(
            Description =
                "The timestamp along the -" + nameof(ARConcepts.RegStream) + "- \"axis\" for which this snapshot is valid.\r\n" +
                "Will usually be \"now\". It it assumed that querying for other -" + nameof(RegTime) + "- values is probably only relevant when debugging." +
                "\r\n",
            Type = typeof(RegTime)
        )]
        RegTime,

        [PKType(
            Description =
                "The next timestamp along the -" + nameof(ARConcepts.RegStream) + "- \"axis\" when this snapshot will change.\r\n" +
                "Only relevant if snapshot was queried with -" + nameof(RegTime) + "-.\r\n" +
                "\r\n",
            Type = typeof(RegTime)
        )]
        NextRegTime,

        [PKType(
            Description =
                "The previous timestamp along the -" + nameof(ARConcepts.RegStream) + "- \"axis\" when this snapshot was changed.\r\n" +
                "Only relevant if snapshot was queried with -" + nameof(RegTime) + "-.\r\n" +
                "\r\n",
            Type = typeof(RegTime)
        )]
        PreviousRegTime,

        [PKType(
            Description =
                "Errors encountered when processing events for this snapshot.\r\n" +
                "\r\n" +
                "Consists of events with error responses, that is, events that we can not trust to have " +
                "been processes correctly.",
            Type = typeof(string), Cardinality = Cardinality.WholeCollection)]
        ErrorResponses,

        [PKType(
            Description =
                "All events leading up to this snapshot.\r\n" +
                "Probably mostly used for debugging",
            Type = typeof(Event), Cardinality = Cardinality.WholeCollection
        )]
        Events,

        [PKType(
            Description =
                "All events discarded when constructing this snapshot\r\n" +
                "(because replaced with another event with the exact same -" + nameof(EventTime) +"-).\r\n" +
                "Does not include events in the future.\r\n" +
                "Probably mostly used for debugging",
            Type = typeof(Event), Cardinality = Cardinality.WholeCollection
        )]
        EventsDiscarded,

        [PKType(
            Description = "The actual value of the snapshot.",
            Type = typeof(IP)
        )]
        Entity,

        //[PKType(
        //    Description = "All events stored for this entity, regardless of included in snapshot or not",
        //    Type = typeof(Event), Cardinality = Cardinality.WholeCollection
        //)]
        //Events
    }
}
