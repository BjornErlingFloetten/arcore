﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Text.Json.Serialization;
using System.Collections.Generic;
using ARCCore;

/// <summary>
/// Contains the following:
/// <see cref="ARCEvent.Reg"/>
/// <see cref="ARCEvent.RegP"/>
/// <see cref="ARCEvent.RegTime"/>
/// <see cref="ARCEvent.RegId"/>
/// <see cref="ARCEvent.RegStatus"/>
/// <see cref="ARCEvent.RegVerb"/>
/// </summary>
namespace ARCEvent
{

    [Class(Description =
        "A Reg (Registration) is an item in the -" + nameof(ARConcepts.RegStream) + "-.\r\n" +
        "\r\n" +
        "If it also occurs in the -" + nameof(ARConcepts.EventStream) + "- it will be as a corresponding -" + nameof(Event) + "-.\r\n" +
        "\r\n" +
        "A Reg has a corresponding handler (see -" + nameof(IRegHandler) + "- / -" + nameof(RegP.RH) + "-).\r\n" +
        "\r\n" +
        "See also -" + nameof(RegP) + "-."
    )]
    public class Reg : PRich // TODO: Turn into PExact<>, will save some memory when storing
    { 

        public RegTime RegTime => IP.GetPV<RegTime>(RegP.RegTime);
        public EventTime EventTime => IP.GetPV<EventTime>(RegP.EventTime);

        [ClassMember(Description =
            "Alternative ('override') to -" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "-.\r\n" +
            "\r\n" +
            "Creates HTML in the \"ordinary\" manner, plus a better heading-\r\n" +
            "\r\n" +
            "NOTE: The mere existence of this method is not sufficient in order to avoid " +
            "-" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- being called.\r\n" +
            "(extension method will be chosen anyway if instance is of type -" + nameof(IP) + "-.\r\n" +
            "See " + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- for more details about this issue.\r\n"
        )]
#pragma warning disable IDE0060 // Remove unused parameter
        public string ToHTMLSimpleSingle(bool prepareForLinkInsertion = false, List<IKCoded>? linkContext = null)
#pragma warning restore IDE0060 // Remove unused parameter
        {
            try
            {
                REx.Inc(); /// Use guard here, it is too easy to mix up how <see cref="ARCDoc.Extensions.ToHTMLSimpleSingle"/> is called.
                return
                    (!IP.TryGetP<IRegHandler>(PK.FromEnum(RegP.RH), out var regHandler) ? "" :
                        (
                            "<a href=\"/Process/" + regHandler.GetType().ToStringVeryShort() + "/" + RegTime.SerialNumber + "\">Edit</a>&nbsp;&nbsp;\r\n" +
                            "<h1 style=\"font-family:monospace\">" + regHandler.Serialize().Replace("0x0020", " ") + "</h1>\r\n" +
                            "<p>All earlier edits:&nbsp;&nbsp;\r\n" +
                            "<a href=\"/RQ/dt/Reg/WHERE EventTime = '" + EventTime.ToString() + "'/ORDER BY RegId DESC\">As Registrations</a>&nbsp;&nbsp;\r\n" +
                            "<a href=\"/RQ/dt/Event/WHERE EventTime = '" + EventTime.ToString() + "'/ORDER BY RegId DESC\">As Events</a>&nbsp;&nbsp;\r\n" +
                            "</p>\r\n" +
                            "<hr>\r\n"
                        )
                    ) +
                    /// Note: Setting linkContext to null will create links starting with '/RQ/dt...', that is, they will be correct 
                    /// regardless of this page originating from <see cref="ARCAPI.RQController"/> or <see cref="ESController"/>
                    ARCDoc.Extensions.ToHTMLSimpleSingleInternal(this, prepareForLinkInsertion, linkContext: null); 
            }
            finally
            {
                REx.Dec();
            }
        }

        [ClassMember(Description =
            "Returns result of -" + nameof(IRegHandler.Serialize) + "-.\r\n" +
            "\r\n" +
            "NOTE: Do NOT confuse this with ordinary -" + nameof(ARConcepts.PropertyAccess) + "- for -" + nameof(RegP.RH) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetRegHandler(out PValue<string> retval, out string errorResponse)
        {
            if (!IP.TryGetP<IRegHandler>(PK.FromEnum(RegP.RH), out var iRegHandler, out errorResponse))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            retval = new PValue<string>(iRegHandler.Serialize());
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Returns -" + nameof(ARCEvent.EventTime.Time) + "- part of -" + nameof(EventTime) + "-.\r\n" +
            "\r\n" +
            "Makes for easier querying like \"Reg/WHERE EventTimeTime = Today\" for instance.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetEventTimeTime(out PValue<DateTime> retval, out string errorResponse)
        {
            retval = new PValue<DateTime>(EventTime.Time);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Returns a string representation of -" + nameof(ARCEvent.RegTime) + "- useful for string based sorting.\r\n" +
            "\r\n" +
            "Created because of weakness in -" + nameof(ARCQuery.QueryExpressionOrderBy) + "- which is limited in what types it can sort.\r\n" +
            "\r\n" +
            "Note that a corresponding method for -" + nameof(RegTime) + "- is not necessary because it can be sorted by RegId which " +
            "is again recognized by -" + nameof(ARCQuery.QueryExpressionOrderBy) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetEventTimeSortable(out PValue<string> retval, out string errorResponse)
        {
            retval = new PValue<string>(
                EventTime.Time.ToStringDateAndTime() + " " +
                EventTime.SerialNumber.ToString(UtilEvent.SerialNumberFormat) + " " +
                RegTime.SerialNumber.ToString(UtilEvent.SerialNumberFormat) // Added RegTime.SerialNumber 19 Jan 2022.
            );
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Returns -" + nameof(ARCEvent.RegTime.Time) + "- part of -" + nameof(RegTime) + "-.\r\n" +
            "\r\n" +
            "Makes for easier querying like \"Reg/WHERE RegTimeTime = Today\" for instance.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetRegTimeTime(out PValue<DateTime> retval, out string errorResponse)
        {
            retval = new PValue<DateTime>(RegTime.Time);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }
    }

    public class RegCollection : PCollection
    {

    }

    [Enum(
        Description = "Describes class -" + nameof(Reg) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum RegP
    {
        __invalid,

        [PKType(Description =
            "Placeholder for storing of which user performed a Registration / was behind an Event.\r\n" +
            "User management in itself is probably out-of-scope for " + nameof(ARComponents.ARCEvent) + " / AgoRapide.\r\n" +
            "\r\n" +
            "TODO: Consider also whether UserId should appear here at all. "
        )]
        UserId,

        [PKType(
            Description =
                "The timestamp along the -" + nameof(ARConcepts.EventStream) + "- \"axis\" for when this Registration shall have effect.\r\n" +
                "\r\n",
            Type = typeof(EventTime)
        )]
        EventTime,

        [PKType(
                Description =
                    "The timestamp along the -" + nameof(ARConcepts.RegStream) + "- \"axis\" when this Registration was created.\r\n" +
                    "Corresponds to 'now' when created, or -" + nameof(StreamProcessorP.Timestamp) + "- in the -" + nameof(ARConcepts.RegStream) + "-.\r\n" +
                    "\r\n",
                Type = typeof(RegTime)
            )]
        RegTime,

        [PKType(Type = typeof(RegVerb))]
        RegVerb,

        // Removed 31 Jan 2022
        //[PKType(
        //    Description =
        //        "The type of the -" + nameof(IRegHandler) + "- describing the model and handling the Registration.\r\n" +
        //        "\r\n" +
        //        "TODO: Probably superfluous if we always are going to store the registration handler itself as -" + nameof(RegP.RH) + "-.\r\n" +
        //        "TODO: And querying is done quite easily with the help of -" + nameof(ARCQuery.FunctionKeyType) + "-.\r\n" +
        //        "\r\n",
        //    Type = typeof(Type))]
        //RegHandlerType,

        [PKType(
            Description =
                "RH = Registration Handler.\r\n" +
                "\r\n" +
                "The instance of -" + nameof(IRegHandler) + "- which describes the model and handles the Registration.\r\n" +
                "\r\n" +
                "NOTE: IMPORTANT! For Registration Handlers having -" + nameof(SubProcess) + "-es, the object stored here " +
                "NOTE: IMPORTANT! is not necessarily up-to-date because such Registration Handlers must be updated as -" + nameof(Snapshot) + "-.\r\n" +
                "\r\n" +
                "TODO: Dec 2021: Reconsider naming of this, see if need stated below still holds:\r\n" +
                "The name has been shortened in order to make queries with -" + nameof(ARCQuery) + "- more succinct " +
                "(when peeking inside this structure).\r\n" +
                "\r\n" +
                "See also -" + nameof(Reg.TryGetRegHandler) + "- which returns -" + nameof(IRegHandler.Serialize) + "-.\r\n",
            Type = typeof(IP))] // TODO: Why not type of IRegHandler here?
        RH,

        [PKType(
            Description =
                "Note: -" + nameof(RegStatus) + "- and -" + nameof(ErrorResponse) + "- only relates to how the action was received " +
                "by -" + nameof(EventProcessor.TryStoreAndDistributeEvent) + "- " +
                "(whether a corresponding -" + nameof(Event) + "- was successfully generated and stored).\r\n" +
                "\r\n" +
                "These two properties will not reflect any information about how the -" + nameof(Event) + "- was later processed.\r\n",
            Type = typeof(RegStatus)
        )]
        RegStatus,

        [PKType(
            Description =
                "Note: -" + nameof(RegStatus) + "- and -" + nameof(ErrorResponse) + "- only relates to how the action was received " +
                "by -" + nameof(EventProcessor.TryStoreAndDistributeEvent) + "- " +
                "(whether a corresponding -" + nameof(Event) + "- was successfully generated and stored).\r\n" +
                "\r\n" +
                "These two properties will not reflect any information about how the -" + nameof(Event) + "- was later processed.\r\n"
        )]
        ErrorResponse,

        [PKType(
            Description =
                "Pointer to corresponding -" + nameof(Event) + "- created as a result of this -" + nameof(Reg) + "-.\r\n" +
                "Used for report / debugging purposes, not necessary in itself for -" + nameof(ARConcepts.EventSourcing) + "-.",
            Type = typeof(EventId)
        )]
        EventId,

        [PKType(
            Description = "Cost in Time (by -" + nameof(EventProcessor.TryStoreAndDistributeEvent) + "-.)\r\n",
            Type = typeof(TimeSpan))]
        CostTime,

        [PKType(
            Description = "See -" + nameof(PCollectionES.TryStoreEventAccessCount) + "-.",
            Type = typeof(long)
        )]
        CostTryStoreEvent,

        [PKType(
            Description = "See -" + nameof(PCollectionES.GlobalRequestSnapshotCount) + "-.",
            Type = typeof(long)
        )]
        CostRequestSnapshot,

        [PKType(
            Description = "See -" + nameof(PCollectionES.GlobalCreateNewSnapshotCount) + "-.",
            Type = typeof(long)
        )]
        CostCreateSnapshot
    }

    [Class(Description =
        "Points to a time on the -" + nameof(ARConcepts.RegStream) + "- \"axis\".\r\n" +
        "\r\n" +
        "See -" + nameof(EventTime) + "- for a detailed explanation about timestamps and serial numbers.\r\n" +
        "\r\n" +
        "Note how -" + nameof(EventTime) + "- and -" + nameof(RegTime) + "- offer the exact " +
        "same functionality (a timestamp and a serial number), but mixing these two up in the system would make for really weird bugs" +
        "so they therefore exist as two different classes instead of one.\r\n" +
        "(a Superclass TimeAndSerialNumber was attempted but turned out impractical " +
        "(all methods had to be implemented in the sub-classes anyway)).\r\n" +
        "\r\n" +
        "NOTE:\r\n" +
        "Since an instance of this class is stored for EVERY -" + nameof(Reg) + "-\r\n" +
        "it is tempting to reduce memory usage here (8 bytes per object) by storing the serialnumber within Ticks " +
        "of the Time-object, and restrict Time to only whole seconds.\r\n" +
        "\r\n" +
        "There are 10 000 000 ticks in a second so we could have stored \"SerialNumber % 10 000 000\", \r\n" +
        "however, we must be careful with wrap-around bugs for this module counter.\r\n" +
        "So it is considered too dangerous for the time being.\r\n"
    )]
    public class RegTime : IEquatable<RegTime>, IComparable, ITypeDescriber
    {

        public DateTime Time { get; private set; }

        [ClassMember(Description =
            "The unique id necessary for identifying a -" + nameof(Reg) + "- (Registration).\r\n"
        )]
        public long SerialNumber { get; private set; }

        public RegTime(DateTime time, long serialNumber)
        {
            Time = time;
            SerialNumber = serialNumber;
        }

        public static bool operator >(RegTime a, RegTime b)
            => (a.Time > b.Time) || (a.Time == b.Time && a.SerialNumber > b.SerialNumber);

        public static bool operator <(RegTime a, RegTime b)
            => (a.Time < b.Time) || (a.Time == b.Time && a.SerialNumber < b.SerialNumber);

        public static bool operator ==(RegTime a, RegTime b)
            => (a.Time == b.Time) && (a.SerialNumber == b.SerialNumber);

        public static bool operator !=(RegTime a, RegTime b)
            => (a.Time != b.Time) || (a.SerialNumber != b.SerialNumber);

        public static bool operator >=(RegTime a, RegTime b)
            => a > b || a == b;

        public static bool operator <=(RegTime a, RegTime b)
            => a < b || a == b;

        public bool Equals(RegTime other) => Time == other.Time && SerialNumber == other.SerialNumber;
        public override bool Equals(object other) => other is RegTime otherAsRegTime && this == otherAsRegTime;
        public override int GetHashCode() => (int)(SerialNumber % int.MaxValue);

        [ClassMember(Description =
            "'Smart' way of showing time. Removes unnecessary information.\r\n " +
            "\r\n" +
            "If this turns out to be too smart (remember, it must also fit together with -" + nameof(TryParse) + "- for instance), " +
            "then we can always make it dumber. The consequence will be more digits presented to the human user of the system however.\r\n" +
            "\r\n" +
            "TODO: Include 'bot' and 'eot'."
        )]
        public override string ToString() =>
            (Time.Date.Equals(UtilCore.DateTimeNow.Date) ? "" : (Time.ToString("yyyy-MM-dd") + " ")) +
            new Func<string>(() =>
            {
                if (Time.Millisecond != 0) return Time.ToString("HH:mm:ss.fff");
                if (Time.Second != 0) return Time.ToString("HH:mm:ss");
                return Time.ToString("HH:mm");
            })() + " " +
            SerialNumber.ToString();

        [ClassMember(Description =
            "TODO: Check if this is really needed."
        )]
        [JsonIgnore] // Ignore because information is same as SerialNumber
        public RegId ToRegId => new RegId(SerialNumber);

        public int CompareTo(Object other)
        {
            if (!(other is RegTime otherAsRegTime)) throw new InvalidObjectTypeException(other, typeof(RegTime));
            var retval = Time.CompareTo(otherAsRegTime.Time);
            if (retval != 0) return retval;
            return SerialNumber.CompareTo(otherAsRegTime.SerialNumber);
        }

        public static bool TryParse(string value, out RegTime retval, out string errorResponse)
        {
            switch (value)
            {
                case "bot":
                    retval = new RegTime(DateTime.MinValue, serialNumber: 0);
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
                case "eot":
                    retval = new RegTime(DateTime.MaxValue, serialNumber: long.MaxValue);
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
                /// Note: We are unable to decide here what now means (round backwards or forwards)
                /// It will depend on whether this comes from <see cref="RSController"/> or <see cref="ESController"/>
                //case "now":
                //    retval = new EventTime(UtilEvent.RoundDateTimeBackwards, serialNumber: 0);
                //    retval = new EventTime(UtilEvent.RoundDateTimeForwards, serialNumber: 0);
                //    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                //    return false;
                default:
                    var t = value.Split(" ");
                    if (t.Length == 2)
                    {
                        /// Allow only time part or only date part to be specified 
                        /// (see correspondingly <see cref="ToString"/> and use of <see cref="FunctionKeyShort.Short"/>)
                        if (t[0].Length >= 3 && t[0][2] == ':')
                        {
                            // Looks like only time part was specified, add date part
                            t = new string[] { UtilCore.DateTimeNow.ToString("yyyy-MM-dd"), t[0], t[1] };
                            // TOOD: This is not a very efficient approach, constructing a string and then parsing it again
                        }
                        else if (t[0].Length >= 5 && t[0][4] == '-')
                        {
                            // Looks like only date part was specified, add time part
                            t = new string[] { t[0], UtilCore.DateTimeNow.ToString("HH:mm:ss.fff"), t[1] };
                            // TOOD: This is not a very efficient approach, constructing a string and then parsing it again
                        }
                    }
                    if (t.Length != 3)
                    {
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> ; 
                        errorResponse = "Not three parts (date, time, serial number), separated by space but " + t.Length;
                        return false;
                    }
                    if (!UtilCore.DateTimeTryParse(t[0] + " " + t[1], out var dateTime))
                    {
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> ; 
                        errorResponse = "Invalid DateTime part (" + t[0] + " " + t[1] + ")";
                        return false;

                    }
                    if (!long.TryParse(t[2], out var serialNumber))
                    {
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> ; 
                        errorResponse = "Invalid SerialNumber part (" + t[2] + ")";
                        return false;
                    }
                    retval = new RegTime(dateTime, serialNumber);
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
            }
        }

        [ClassMember(Description = "'Implementing' -" + nameof(ITypeDescriber) + "-.")]
        public static void EnrichKey(PK k) =>
            k.SetValidatorAndParser(new Func<string, ParseResult>(value =>
                TryParse(value, out var retval, out var errorResponse) ?
                    ParseResult.CreateOK(retval) :
                    ParseResult.CreateError(errorResponse)
            ));
    }

    [Class(Description =
        "Strong typing of primary keys reduces risk of mix-ups in the code.\r\n" +
        "\r\n" +
        "Consists of a global serial number for the application."
    )]
    public class RegId : IKLong
    {

        public RegId(long value) : base(value) { }

        // Parse / TryParse according to AgoRapide standard. Looks like overkill of course in our case
        public static new RegId Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidRegIdException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse + "\r\n");
        public static bool TryParse(string value, out RegId retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out RegId retval, out string errorResponse)
        {
            if (string.IsNullOrEmpty(value))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = "Null or Empty value (" + value + ")";
                return false;
            }
            if (!long.TryParse(value, out var valueAsLong))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = "Invalid as long.";
                return false;
            }
            retval = new RegId(valueAsLong);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
            return true;
        }


        [ClassMember(Description = "'Implementing' -" + nameof(ITypeDescriber) + "-.")]
        public static new void EnrichKey(PK k) =>
            k.SetValidatorAndParser(new Func<string, ParseResult>(value =>
                TryParse(value, out var retval, out var errorResponse) ?
                    ParseResult.CreateOK(retval) :
                    ParseResult.CreateError(errorResponse)
            ));

        public class InvalidRegIdException : ApplicationException
        {
            public InvalidRegIdException(string message) : base(message) { }
            public InvalidRegIdException(string message, Exception inner) : base(message, inner) { }
        }
    }

    [Enum(AREnumType = AREnumType.OrdinaryEnum)]
    public enum RegStatus
    {
        __invalid,
        NEW,
        [EnumMember(Description =
            "OK: The corresponding -" + nameof(Event) + "- object has been created but not necessarily distributed or processed."
        )]
        OK,
        [EnumMember(Description =
            "ERROR: The corresponding -" + nameof(Event) + "- object has not been created " +
            "and the action will never be re-processed during the application lifetime."
        )]
        ERROR
    }

    [Enum(
        Description =
            "Describes how an -" + nameof(Reg) + "- (or rather -" + nameof(Event) + "-) " +
            "should be treated when creating -" + nameof(Snapshot) + "-s.\r\n" +
            "\r\n" +
            "Will influence -" + nameof(EventStatus) + "-.r\n" +
            "\r\n" +
            "Note that multiple -" + nameof(RegVerb) + "-s may be specified for a single -" + nameof(Event) + "- " +
            "(a single point in -" + nameof(ARConcepts.EventStream) + "-), " +
            "they will be evaluated by the -" + nameof(PCollectionES) + "-'s -" + nameof(Snapshot) + " creator " +
            "by the order in which they appear in the -" + nameof(ARConcepts.RegStream) + "-." +
            "\r\n" +
            "The identifications used for multiple registrations will be like this:\r\n" +
            "\r\n" +
            "First registration (performed on 2 Dec 2021):\r\n" +
            "2021-12-01/DO/SSPR/Customer 42/FirstName/John\r\n" +
            "RegId 2021-12-02 1  EventId 2021-12-01 1  (Serial number of eventId is same as serialNumber of RegId)\r\n" +
            "\r\n" +
            "Second operation (performed on 3 Dec 2021):\r\n" +
            "1/DO/SSPR/Customer 42/FirstName/FirstName/Johnny (correction of first Registration, Registration is identified by RegId's serialNumber '1')\r\n" +
            "RegId 2021-12-03 2  EventId 2021-12-01 1  (Registration gets new RegId, but old EventId is kept)\r\n" +
            "\r\n" +
            "Third operation (performed on 4 Dec 2021):\r\n" +
            "1/UNDO (correction of correction, use 'John' instead)\r\n" +
            "RegId 2021-12-04 3  EventId 2021-12-01 1  (Registration gets new RegId, but old EventId is kept)\r\n" +
            "\r\n" +
            "The last -" + nameof(RegVerb) + "- encountered is the deciding one, unless for -" + nameof(UNDO) + "- in which case the \r\n" +
            "Registration two steps before the -" + nameof(UNDO) + "- is the deciding one.\r\n" +
            "\r\n" +
            "NOTE: As of Nov 2021 these are not taken into consideration when creating snapshots.",
        AREnumType = AREnumType.OrdinaryEnum
    )]
    public enum RegVerb
    {
        __invalid,

        [EnumMember(Description =
            "Plan a - " + nameof(Reg) + " - (Registration).\r\n" +
            "\r\n" +
            "To be used when some action is planned to be performed, but not done yet.\r\n" +
            "\r\n" +
            "Event will appear in the -" + nameof(ARConcepts.EventStream) + "-\r\n" +
            "but will only be sent to -" + nameof(IRegHandlerTAERTE.TryApplyEventResultToEntity) + "-\r\n" +
            "if the -" + nameof(EventTime) + "- is into the future.\r\n" +
            "\r\n" +
            "Note how this allows for planning and at the same time see what consequences the plan will have.\r\n"
        )]
        PLAN,

        [EnumMember(Description =
            "The ordinary reg verb assumed to be relevant for most of the -" + nameof(Reg) + "-s.\r\n" +
            "\r\n" +
            "The -" + nameof(Event) + "- will appear ordinarily in the -" + nameof(ARConcepts.EventStream) + "-." +
            "\r\n"
        )]
        DO,

        [EnumMember(Description =
            "Undo the -" + nameof(Reg) + "- (Registration).\r\n" +
            "\r\n"
        )]
        UNDO,

        [EnumMember(Description =
            "Block the -" + nameof(Reg) + "- (Registration). It will appear in the -" + nameof(ARConcepts.EventStream) + "- but not be processed.\r\n" +
            "\r\n" +
            "Either set as default for new Registration that require -" + nameof(ALLOW) + "- (by a supervisor user for instance), or " +
            "set later in order to block the Registration.\r\n" +
            "\r\n" +
            "See also -" + nameof(DELETE) + "-."
        )]
        BLOCK,

        [EnumMember(Description =
            "Allow the -" + nameof(Reg) + "- (Registration). It will appear as an ordinary -" + nameof(DO) + "-.\r\n" +
            "\r\n" +
            "Relevant for instance when -" + nameof(EventStatus) + "- indicates BLOCK like " +
            "for instance if an earlier -" + nameof(BLOCK) + "- has been performed (-" + nameof(EventStatus.BLOCKED_USER) + "-) " +
            "or if the default for a Registration was -" + nameof(BLOCK) + "- (-" + nameof(EventStatus.BLOCKED_DEFAULT) + "-).\r\n"
        )]
        ALLOW,

        [EnumMember(Description =
            "Delete the -" + nameof(Reg) + "- (Registration).\r\n" +
            "\r\n" +
            "Note the concept of -" + nameof(ARConcepts.NoRealDeletion) + "-\r\n" +
            "\r\n" +
            "The Registration will be invisible in the -" + nameof(ARConcepts.EventStream) + "- / in -" + nameof(Snapshot) + "-s.\r\n" +
            "DELETE is a 'stronger' concept than -" + nameof(BLOCK) + "-."
        )]
        DELETE,
    }
}
