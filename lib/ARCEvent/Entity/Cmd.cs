﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

/// <summary>
/// Contains the following:
/// <see cref="ARCEvent.Cmd"/>
/// <see cref="ARCEvent.CmdCollection"/>
/// <see cref="ARCEvent.CmdP"/>
/// <see cref="ARCEvent.Cmd"/>
/// <see cref="ARCEvent.Cmd"/>
/// <see cref="ARCEvent.Cmd"/>
/// </summary>
namespace ARCEvent
{
    [Class(Description =
        "A Cmd (Command) is usually an instruction to transform some dataset into another dataset.\r\n" +
        "\r\n" +
        "See also -" + nameof(CmdP) + "-.\r\n"
    )]
    public class Cmd : PRich // TODO: Turn into PExact<>, will save some memory when storing
    { 
        [ClassMember(Description =
            "Returns result of -" + nameof(ICmdHandler.Serialize) + "-.\r\n" +
            "\r\n" +
            "NOTE: Do NOT confuse this with ordinary -" + nameof(ARConcepts.PropertyAccess) + "- for -" + nameof(CmdP.CH) + "-.\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetCmdHandler(out PValue<string> retval, out string errorResponse)
        {
            if (!IP.TryGetP<ICmdHandler>(PK.FromEnum(CmdP.CH), out var iCmdHandler, out errorResponse))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            retval = new PValue<string>(iCmdHandler.Serialize());
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

    }

    public class CmdCollection : PCollection
    {
    }

    [Enum(
        Description = "Describes class -" + nameof(Cmd) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum CmdP
    {
        __invalid,

        [PKType(Type=typeof(DateTime))]
        CmdTime,

        [PKType(Type=typeof(CmdStatus))]
        CmdStatus,

        ErrorResponse,

        [PKType(Type = typeof(TimeSpan))]
        CostTime,

        [PKType(
            Description =
                "CH = Command Handler.\r\n" +
                "\r\n" +
                "The instance of -" + nameof(IRegHandler) + "- which describes the model and handles the Registration.\r\n" +
                "\r\n" +
                "See also -" + nameof(Cmd.TryGetCmdHandler) + "- which returns -" + nameof(ICmdHandler.Serialize) + "-.\r\n",
            Type = typeof(ICmdHandler)
        )]
        CH
    }

    [Enum(AREnumType = AREnumType.OrdinaryEnum)]
    public enum CmdStatus
    {
        __invalid,

        NEW,

        OK,

        ERROR
    }
}
