﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

/// <summary>
/// Contains the following:
/// <see cref="ARCEvent.Schema"/>
/// <see cref="ARCEvent.SchemaCollection/>
/// <see cref="ARCEvent.SchemaP"/>
/// <see cref="ARCEvent.CreateSCHEMA"/>
/// <see cref="ARCEvent.CreateSCHEMACollection"/>
/// <see cref="ARCEvent.CreateSCHEMAP"/>
/// </summary>
namespace ARCEvent
{

    [Class(
        Description =
            "The Schema describes how a hierarhical collection of -" + nameof(IRegHandler) + "- defines a -" + nameof(Process) + "-.\r\n" +
            "The process itself is quite loosely defined around this collection.\r\n" +
            "\r\n" +
            "A Schema may also be used as a static collection of \"metadata\", that is, any collection of information stored " +
            "under a given entity.\r\n" +
            "(in general, -" + nameof(ARComponents.ARCEvent) + "- does not distinguish metadata and processes.)\r\n" +
            "\r\n" +
            "The Schema is meant to tie together -" + nameof(SubProcess) + "-es, which again may have sub processes but these again " +
            "are probably more tightly defined in the C# code (as individual -" + nameof(IRegHandler) + "-).\r\n" +
            "\r\n" +
            "See also -" + nameof(SchemaP) + "-.\r\n"
    )]
    public class Schema : PRich
    {

        [ClassMember(Description =
            "Creates a new process through -" + nameof(Process.Create) + "- based on this Schema.\r\n" +
            "\r\n"
        )]
        public Process ToProcess(SchemaId schemaId, bool createSubProcess = false, IK? entityKey = null) 
        {
            var retval = Process.Create(
                schemaId,
                createSubProcess ?
                    typeof(Process) :
                    IP.GetPV<Type>(SchemaP.EntityType)
            );
            if (entityKey != null)
            {
                retval.IP.TrySetPV(ProcessP.EntityKey, entityKey.ToString(), out _);
            }
            return retval;
        }

        [ClassMember(Description =
            "Creates list of sub processes, which are either based on other -" + nameof(Schema) + "-s " +
            "or are individual -" + nameof(IRegHandler) + "-.\r\n" +
            "\r\n" +
            "Throws an exception for any value in -" + nameof(SchemaP.SubProcessType) + "- which does not resolve.\r\n" +
            "(becaue this should have been checked by -" + nameof(CreateSCHEMA.TryGetTransientEvents) + "-.)\r\n" +
            "\r\n"
        )]
        public List<(SchemaId? schemaId, Schema? schema, Type subProcessType)> GetSubProcesses(IP dt) =>
            IP.GetPV<List<string>>(SchemaP.SubProcessType).Select(subProcessType => GetIRegHandlerBasedProcessOrSchemaBasedProcess(dt, subProcessType)).ToList();

        public static (SchemaId? schemaId, Schema? schema, Type subProcessType) GetIRegHandlerBasedProcessOrSchemaBasedProcess(IP dt, string processType) =>
           TryGetIRegHandlerBasedProcessOrSchemaBasedProcess(dt, processType, out var temp, out var errorResponse) ? temp :
               throw new SchemaException(errorResponse);

        [ClassMember(Description =
            "Looks in -" + nameof(IRegHandler.AllIRegHandlerDerivedTypesShorthandNamesDict) + "- and (if not found) " +
            "also in -" + nameof(SchemaCollection) + "-.\r\n" +
            "\r\n" +
            "TODO: Make possible using abbreviations (shorthand names) for accessing Schemas also (in addition to already\r\n" +
            "TODO: existing shorthand access feature -" + nameof(IRegHandler) + "-.\r\n" +
            "TODO: Such a feature must be tolerant of collisions since Schemas are user defined, and a FALSE result\r\n" +
            "TODO: would be difficult to cache (as well as a TRUE result if Schema should ever change Name(?)).\r\n"
        )]
        public static bool TryGetIRegHandlerBasedProcessOrSchemaBasedProcess(
            IP dt, string processType, out (SchemaId? schemaId, Schema? schema, Type subProcessType) regHandlerType, out string errorResponse)
        {
            if (IRegHandler.AllIRegHandlerDerivedTypesShorthandNamesDict.TryGetValue(processType, out var type))
            {
                regHandlerType = (schemaId: null, schema: null, type);
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }

            if (dt.GetCollection<SchemaCollection>(() => new SchemaCollection()).IP.
                TryGetP<Schema>(IKString.FromString(processType), out var schema, out errorResponse))
            {
                regHandlerType = (
                    schemaId: SchemaId.Parse(processType),  // This should parse well since we just found a Schema with that as id
                    schema: schema,
                    typeof(Process)
                 );
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }

            regHandlerType = default; /// <see cref="ARConcepts.TryPatternAndNull"/>
            errorResponse =
                "ERROR: Schema for '" + processType + "' not found, " +
                "neither as -" + nameof(IRegHandler) + "-, nor as -" + nameof(Schema) + "-." +
                "\r\n" +
                "Details (related to Schema): " + errorResponse + ".\r\n";
            return false;
        }

        [ClassMember(Description =
            "Alternative ('override') to -" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "-.\r\n" +
            "\r\n" +
            "Adds link -" + nameof(ProcessController) + "-.\r\n" +
            "\r\n" +
            "NOTE: The mere existence of this method is not sufficient in order to avoid " +
            "-" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- being called.\r\n" +
            "(extension method will be chosen anyway if instance is of type -" + nameof(IP) + "-.\r\n" +
            "See " + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- for more details about this issue.\r\n"
        )]
        public string ToHTMLSimpleSingle(bool prepareForLinkInsertion = false, List<IKCoded>? linkContext = null)
        {
            try
            {
                REx.Inc(); /// Use guard here, it is too easy to mix up how <see cref="ARCDoc.Extensions.ToHTMLSimpleSingle"/> is called.
                return
                    new Func<string>(() =>
                    {
                        var t = GetType().ToStringVeryShort();
                        // Note trick with finding our own id, looking in the request string (in linkContext)
                        // The checks below holds for queries against RQ and ES controller, but not necessarily other queries.
                        if (linkContext == null) return "";
                        if (linkContext.Count > 2 && linkContext[1].ToString().Equals(t)) return
                            "<a href=\"/Process/" + linkContext[2].Encoded + "\">Init process with this schema</a>";
                        if (linkContext.Count > 4 && linkContext[3].ToString().Equals(t)) return
                            "<a href=\"/Process/" + linkContext[4].Encoded + "\">Init process with this schema</a>";
                        return "";
                    })() +
                    ARCDoc.Extensions.ToHTMLSimpleSingleInternal(this, prepareForLinkInsertion, linkContext);
            }
            finally
            {
                REx.Dec();
            }
        }

        public class SchemaException : ApplicationException
        {
            public SchemaException(string message) : base(message) { }
            public SchemaException(string message, Exception inner) : base(message, inner) { }
        }

    }

    public class SchemaCollection : PCollectionES
    {
        public SchemaCollection() { }
    }

    [Enum(Description =
        "Describes class -" + nameof(Schema) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum SchemaP
    {
        __invalid,

        Name,

        [PKType(
            Description =
                "The type of entity onto which the process shall be attached.\r\n" +
                "\r\n" +
                "See also -" + nameof(IRegHandler.TryGetRecipientId) + "-.\r\n" +
                "\r\n" +
                "Note: -" + nameof(SchemaP.EntityType) + "- and -" + nameof(ProcessP.EntityType) + "- must correspond with each other.\r\n.",
            Type = typeof(Type)
        )]
        EntityType,

        [PKType(
            Description =
                "The types of the child processes that this -" + nameof(Schema) + "- describes.\r\n" +
                "\r\n" +
                "This can be a combination of:\r\n" +
                "\r\n" +
                "1) Individual -" + nameof(IRegHandler) + "-s.\r\n" +
                "These again can specify sub processes in the form of other -" + nameof(IRegHandler) + "-s " +
                "(see -" + nameof(SubProcess) + "-.).\r\n" +
                "\r\n" +
                "and\r\n" +
                "\r\n" +
                "2) Sub schemas (that is, pointing to other -" + nameof(Schema) + "-s).\r\n" +
                "\r\n" +
                "For 1) the value given here the string representation of the type of the -" + nameof(IRegHandler) + "-.\r\n" +
                "For 2) the value given here is a -" + nameof(SchemaId) + "-.\r\n" +
                "\r\n" +
                "See also -" + nameof(ARCEvent.SubProcess) + "-.",
            Type = typeof(string), Cardinality = Cardinality.WholeCollection
        )]
        SubProcessType
    }

    [Class(Description = "Strong typing of primary keys reduces risk of mix-ups in the code.")]
    public class SchemaId : IKString
    {

        [ClassMember(Description =
            "Note that we do not need to have a private constructor here, " +
            "it is only in order to make it more similar to other -" + nameof(IK) + "- classes"
        )]
        private SchemaId(string s) : base(s) { }

        public static new SchemaId FromString(string s) => new SchemaId(s);

        // Parse / TryParse according to AgoRapide standard. Looks like overkill of course in our case
        public static new SchemaId Parse(string value) => TryParse(value, out var retval, out var er) ? retval : throw new InvalidSchemaIdException(nameof(value) + ": " + value + ", " + nameof(er) + ": " + er + "\r\n");
        public static bool TryParse(string value, out SchemaId retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out SchemaId retval, out string er)
        {
            if (string.IsNullOrEmpty(value))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                er = "Null or Empty value (" + value + ")";
                return false;
            }
            retval = SchemaId.FromString(value);
            er = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
            return true;
        }


        [ClassMember(Description = "'Implementing' -" + nameof(ITypeDescriber) + "-.")]
        public static new void EnrichKey(PK k) =>
            k.SetValidatorAndParser(new Func<string, ParseResult>(value =>
                TryParse(value, out var retval, out var er) ?
                    ParseResult.CreateOK(retval) :
                    ParseResult.CreateError(er)
            ));

        public class InvalidSchemaIdException : ApplicationException
        {
            public InvalidSchemaIdException(string message) : base(message) { }
            public InvalidSchemaIdException(string message, Exception inner) : base(message, inner) { }
        }
    }

    [Class(
        Description =
            "Creates a new -" + nameof(Schema) + "-. " +
            "\r\n" +
            "NOTE: Strange capitalization in name is in order to not resolve to short name 'CS' which would have\r\n" +
            "NOTE: a very high chance of colliding with some application specific short name.\r\n" +
            "NOTE: (see -" + nameof(IRegHandler.AllIRegHandlerDerivedTypesShorthandNamesDict) + "-.).\r\n",
        LongDescription =
            "See also -" + nameof(CreateSCHEMAP) + "-."
    )]
    public class CreateSCHEMA : PRich, IRegHandler, IRegHandlerTGRI, IRegHandlerTGTE
    {

        [ClassMember(Description =
            "Note that -" + nameof(Schema) + "-s are in principle configurable and does not have to be created through " +
            "C# code (rendering this method superfluous), BUT\r\n" +
            "a use for this method could be having an -" + nameof(IRegHandler) + "- setting up a collection of -" + nameof(Schema) + "-s.\r\n" +
            "\r\n" +
            "This would simplify a customer setup process for instance."
        )]
        public static CreateSCHEMA Create(SchemaId SchemaId, string name, Type entityType, List<string> subProcessType)
        {
            var retval = new CreateSCHEMA();
            if (
                !retval.IP.TrySetP(CreateSCHEMAP.SchemaId, new PValue<SchemaId>(SchemaId), out var er) ||
                !retval.IP.TrySetP(CreateSCHEMAP.Name, new PValue<string>(name), out er) ||
                !retval.IP.TrySetP(CreateSCHEMAP.EntityType, new PValue<Type>(entityType), out er) ||
                !retval.IP.TrySetP(CreateSCHEMAP.SubProcessType, new PValue<List<string>>(subProcessType), out er)
            )
            {
                throw new ArgumentException(er);
            }
            return retval;
        }

        public bool TryGetRecipientId(out EntityTypeAndKey recipientId, out string errorResponse) =>
           ((IRegHandlerTGRI)this).TryGetRecipientIdInternal<SchemaId, CreateSCHEMAP>(
               typeof(Schema), CreateSCHEMAP.SchemaId, out recipientId, out errorResponse);

        public bool TryGetTransientEvents(IP dataStorage, Event _event, out List<IRegHandler> transientEvents, out string errorResponse)
        {
            if (
                // Note: We do not have to "unpack" and "pack" the values like we do here
                // (we could have asked for TryGetP and given that to SSPRT below),
                // but this functions as a type check on the IP values, so it is not entirely without purpose
                !IP.TryGetPV<SchemaId>(CreateSCHEMAP.SchemaId, out var schemaId, out errorResponse) ||
                !IP.TryGetPV<string>(CreateSCHEMAP.Name, out var name, out errorResponse) ||
                !IP.TryGetPV<Type>(CreateSCHEMAP.EntityType, out var entityType, out errorResponse) ||
                !IP.TryGetPV<List<string>>(CreateSCHEMAP.SubProcessType, out var subProcessTypes, out errorResponse)
            )
            {
                transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }

            /// This would have been nice but as of Dec 2021 the Schema will exist as a consequence of this event anyway
            /// See (commented out) attempt in <see cref="PCollectionES"/> at solving this problem.
            /// (as a consequence of <see cref="TryGetRecipientId"/>)
            //if (dataStorage.TryGetEntityInCollection<Schema>(_event.EventTime, regTime: null, SchemaId, out var Schema, out _))
            //{                
            //    transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            //    er = GetType().ToStringVeryShort() + ": Schema '" + SchemaId + "' already exists.";
            //    return false;
            //}
            /// This one is much safer (we "know" better about this than <see cref="PCollectionES"/>)
            if (
                dataStorage.TryGetEntityInCollection<Schema>(_event.EventTime, regTime: null, schemaId, out var schema, out _) &&
                // !schema.IP.EnumeratorReturnsEmptyCollection
                schema.IP.Any()
            )
            {
                transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = GetType().ToStringVeryShort() + ": Schema '" + schemaId + "' already exists (and has " + schema.Count() + " properties added to it)";
                return false;
            }


            //if (!dataStorage.TryGetP(PSPrefix.dt.ToString(), out var dt, out errorResponse))
            //{
            //    transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            //    return false;
            //}

            // Check that every item in process refers to either a Schema (Process) or an individual IRegHandler coded in C#
            foreach (var subProcessType in subProcessTypes)
            {
                if (!Schema.TryGetIRegHandlerBasedProcessOrSchemaBasedProcess(dataStorage, subProcessType, out _, out errorResponse))
                {
                    transientEvents = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse = "-" + nameof(Schema.TryGetIRegHandlerBasedProcessOrSchemaBasedProcess) + "-: " + errorResponse;
                    return false;
                }
            }

            var recipientId = new EntityTypeAndKey(typeof(Schema), schemaId);
            transientEvents = new List<IRegHandler> {
                CNER.Create(recipientId), /// This is unnecessary because of two reasons, <see cref="TryGetRecipientId"/> and <see cref="SSPRT{TPropertyKeyEnum}"/> below.
                // TOOD:   Create alternative to SSPRT (SMPRT) taking multiple arguments, in order to not have to
                // TODO:   look up Collection and Entity for each property, like the two properties below for Schema)                
                SSPRT<SchemaP>.Create(recipientId, SchemaP.Name, name),
                SSPRT<SchemaP>.Create(recipientId, SchemaP.EntityType, entityType),
                SSPRT<SchemaP>.Create(recipientId, SchemaP.SubProcessType, subProcessTypes),
            };
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }
    }

    public class CreateSCHEMACollection : PCollection
    {

    }

    [Enum(
        Description = "Describes class -" + nameof(CreateSCHEMA) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum CreateSCHEMAP
    {
        __invalid,

        [PKType(
            Type = typeof(SchemaId),
            IsObligatory = true
        )]
        [PKUI(
            DoNotOfferIds = true
        )]
        SchemaId,

        Name,

        [PKType(
            Type = typeof(Type),
            IsObligatory = true
        )]
        EntityType,

        [PKType(
            Cardinality = Cardinality.WholeCollection,
            IsObligatory = true
        )]
        SubProcessType
    }

}