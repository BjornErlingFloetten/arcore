﻿// Copyright (c) 2021 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using ARCCore;
using System.Linq;

/// <summary>
/// Contains the following:
/// <see cref="ARCEvent.PCollectionES"/>
/// <see cref="ARCEvent.PCollectionESExtension"/>
/// <see cref="ARCEvent.PCollectionESStat"/>
/// </summary>
namespace ARCEvent
{

    [Class(Description =
        "PCollectionES = Property Collecton Event Sourcing.\r\n" +
        "\r\n" +
        "Implementation of -" + nameof(IP) + "- supporting -" + nameof(ARConcepts.EventSourcing) + "-.\r\n" +
        "\r\n" +
        "This class corresponds (somewhat) to -" + nameof(PCollection) + "-.\r\n" +
        "\r\n" +
        "It contains a collection of a specific type of entities (in the form of -" + nameof(Snapshot) + "-s) \r\n" +
        "(just like -" + nameof(PCollection) + "-), but also holds all the -" + nameof(Event) + "-s received for each entity.\r\n" +
        "\r\n" +
        "This makes it capable of reconstructing entities from scratch to any point in the -" + nameof(ARConcepts.EventStream) + "- timeline, " +
        "even at time points into the future, and also in the -" + nameof(ARConcepts.RegStream) + "- timeline dimension " +
        "(see -" + nameof(ARConcepts.PropertyStream2D) + "-).\r\n" +
        "\r\n" +
        "Note that only -" + nameof(IP) + "- methods that entails READING are supported.\r\n" +
        "Writing, that is creating new entities, has to be wrapped in an -" + nameof(Reg) + "- like -" + nameof(SSPR) + "- " +
        "because any attempts at writing would end up in an ephemeral snapshot and be of little use.\r\n" +
        "\r\n"
    )]
    public class PCollectionES : IP
    {

        /// <summary>
        /// Gives easy access to default interface methods inside of class
        ///  For some discussion about this, see:
        ///  https://stackoverflow.com/questions/57761799/calling-c-sharp-interface-default-method-from-implementing-class
        /// Note: Do not use <see cref="ClassMemberAttribute"/> here, it will only mess up the HTML documentation for AgooRapide with unnecessary links to IP-members.
        /// </summary>
        public IP IP => this;

        public static bool _eventTimeMonotoneIncreasingGuarantee = false;
        [ClassMember(Description =
            "Use this method at application startup if events are guaranteed to always come in increasing order.\r\n" +
            "\r\n" +
            "This will simplify processing in both -" + nameof(TryStoreEvent) + "- and -" + nameof(TryGenerateAndStoreSnapshotAsRelevant) + "-.\r\n" +
            "Events will just be appended at end of event list and snapshot will be reused."
        )]
        public static void SetEventTimeMonotoneIncreasingGuarantee(bool value)
        {
            if (value)
            {
                UtilCore.AssertCurrentlyStartingUp();
            }
            _eventTimeMonotoneIncreasingGuarantee = value;
        }

        [ClassMember(Description =
            "All -" + nameof(IP) + "- derived classes should have a parameterless constructor " +
            "in case -" + nameof(PropertyStreamLine.TryStore) + "- encounters the name in the -" + nameof(ARConcepts.PropertyStream) + "-."
        )]
        public PCollectionES() { }

        [ClassMember(Description =
            "A single item in the list here is a snapshot / on-demand generated state of the entity.\r\n" +
            "\r\n" +
            "The collection has to be a ConcurrentDictionary becaues READING of property values can entail writing " +
            "to the collection (generation of a new snapshot), and through the concept of -" + nameof(ARConcepts.SingleThreadedCode) + "- " +
            "we only have the promise of a read-lock encompassing any current operation.\r\n" +
            "\r\n" +
            "TODO: Add status / logging information. For instance, a global count of snapshot-created counter in order to indicate " +
            "TODO: complexity of operations." +
            "TODO: Store this back into the Registration object."
        )]
        private readonly ConcurrentDictionary<IK, List<Snapshot>> _snapshotsDict = new ConcurrentDictionary<IK, List<Snapshot>>();

        public long SnapshotsDictCount() => _snapshotsDict.Count;
        public long SnapshotsTotalCount() => _snapshotsDict.Sum(e => (long)e.Value.Count);

        [ClassMember(Description =
            "Each individual value is supposed to always be sorted.\r\n" +
            "\r\n" +
            "TODO: Consider using SortedList, SortedDictionary or similar but consider memory implications first.\r\n" +
            "TODO: The actual sorting done by this class is quite simple, although O(n) there are few\r\n" +
            "TODO: values in each list and most of the new values go to the end of the list.\r\n" +
            "\r\n" +
            "TODO: Any performance improvements must also take into consideration the future aspect, that is " +
            "TODO: we want to also store events for the future."
        )]
        protected Dictionary<IK, List<Event>> _eventsDict = new Dictionary<IK, List<Event>>();

        public long EventsDictCount() => _eventsDict.Count;
        public long EventsTotalCount() => _eventsDict.Sum(e => (long)e.Value.Count);
        public long EventsInvalidCount() => _eventsDict.Sum(e => (long)e.Value.Count(e => !(e.ReplacedRegTime is null)));


        public IP DeepCopy() => throw new NotImplementedException();

        [ClassMember(Description =
            "Note how the enumerator is able to distinguish between collections also in periods of time,\r\n" +
            "not only single entities.\r\n" +
            "\r\n" +
            ""
        )]
        public IEnumerator<IKIP> GetEnumerator()
        {
            /// Note it may look strange that we look in the <see cref="_eventsDict"/> and not <see cref="_snapshotsDict"/>
            /// when what we want to return is snapshots, BUT, remember that snapshots are created on-demand,
            /// so it is possible that the key simple does not exist in <see cref="_snapshotsDict"/> yet
            /// (but it will exist AFTER call to this method)
            return _eventsDict.
                Select(e =>
                {
                    return TryGetP<IP>(e.Key, out var ip, out _) ? new IKIP(e.Key, ip) : null;
                }).
                Where(t => t != null).Select(t => t!).GetEnumerator();
        }

        [ClassMember(Description =
            "TODO: This is currently an O(n) operation."
        )]
        public IEnumerable<IKIP> GetKeysEqualToValue(IK key, string value) =>
            this.Where(ikip => ikip.P.TryGetPV<string>(key, out var v) && v.Equals(value)).Select(ikip => ikip);

        public bool OnTrySetP(IKIP ikip) => false; /// Do not bother, <see cref="ARConcepts.EventSourcing"/> is far more powerful than what OnTrySetP can offer.

        public bool TryGetP<T>(IK key, out T p, out string errorResponse) where T : IP
        {
            if (!TryGetP(
                EventTime.NowAsQuery(),
                regTime: null,
                key,
                out var snapshot,
                out errorResponse
            ))
            {
                p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            /// Note how we only return the actual Entity here, not the encompassing snapshot.
            /// The rationale is that we are now implementing the standard <see cref="IP"/> interface,
            /// for instance through RQController in an API, and the client will probably have
            /// no concept of <see cref="ARConcepts.EventSourcing"/>.
            if (!(snapshot.Entity is T temp))
            {
                p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Key " + key + ": Found type " + snapshot.Entity.GetType().ToStringShort() + ", not " + typeof(T).ToStringShort();
                return false;
            }
            p = temp;
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public bool TryGetV<T>(out T retval, out string errorResponse) where T : notnull
        {
            retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            errorResponse = "Irrelevant for " + GetType().ToStringShort() + " because this class does not have a value of itself, it is a collection of values";
            return false;
        }

        private const string _writeErrorMessage =
            "Write operations against " + nameof(PCollectionES) + " are not supported directly.\r\n" +
            "You must instead use the -" + nameof(Reg) + "- concept in order to first persist the operation " +
            "to the -" + nameof(ARConcepts.RegStream) + "-, for instance through -" + nameof(SSPR) + "- or similar.\r\n";

        public bool TryRemoveP(IK key, out string errorResponse)
        {
            errorResponse = _writeErrorMessage;
            // TOOD: Add Registration (models) which this instance subscribes to in the error message
            // TOOD: TODO: What was the meaning of that TODO:?
            return false;
        }

        public bool TrySetP(IKIP ikip, out string errorResponse)
        {
            errorResponse = _writeErrorMessage;
            // TOOD: Add Registration (models) which this instance subscribes to in the error message
            // TOOD: TODO: What was the meaning of that TODO:?
            return false;
        }

        public bool TrySetPP(IK entityKey, IP? entity, IKIP ikip, out string errorResponse)
        {
            errorResponse = _writeErrorMessage;
            // TOOD: Add Registration (models) which this instance subscribes to in the error message
            // TOOD: TODO: What was the meaning of that TODO:?
            return false;
        }

        [ClassMember(Description =
            "HACK: Hack in order to be able to generate super processes based on sub prosesses through the -" + nameof(Snapshot) + "- principle.\r\n" +
            "\r\n" +
            "TODO: Make -" + nameof(TryGenerateAndStoreSnapshotAsRelevant) + "- aware of the fact the first event stored IS actually the snapshot.\r\n" +
            "TODO: (in order to not make the generated snapshot so vulnerable to deletion).\r\n" +
            "\r\n" +
            "This is necessary only for -" + nameof(IRegHandler) + "-s with -" + nameof(SubProcess) + "-es.\r\n" +
            "\r\n" +
            "Other registration handlers can be stored within an ordinary -" + nameof(PCollection) + "- or similar.\r\n" +
            "(they should definitely NOT be stored within -" + nameof(PCollectionES) + "- because it is more \"expensive\" " +
            "(uses two dictionaries, one for Events one for Snapshots, and a Snapshot must be generated in order to read it back).\r\n" +
            "\r\n" +
            "See also -" + nameof(SubProcess) + "-, -" + nameof(EventStatus.WAIT_FOR_EXEC) + "-, " +
            "-" + nameof(EXEcuteR) + "- and -" + nameof(PKUIAttributeP.SubProcessType) + "-."
        )]
        public bool TryStoreSnapshotDirect(RegId regId, Event _event, IRegHandler regHandler, out string errorResponse)
        {
            if (!_eventsDict.TryAdd(regId, new List<Event> { _event }))
            {
                errorResponse = "Unable to add -" + regId + "- (of type " + regHandler.GetType() + ") to -" + nameof(_eventsDict) + "-.";
                return false;
            }
            var snapshot = new Snapshot();
            snapshot.IncreaseAccessCount();
            var ip = (IP)snapshot;
            ip.SetP(SnapshotP.Entity, regHandler);
            ip.SetPV(SnapshotP.CreatedTime, UtilCore.DateTimeNow);
            ip.SetPV(SnapshotP.EventTime, _event.EventTime);
            ip.SetPV(SnapshotP.Events, new List<Event>
            {
                _event
            });
            ip.SetPV(SnapshotP.EventsDiscarded, new List<Event>());
            if (!_snapshotsDict.TryAdd(regId, new List<Snapshot> { 
                /// TODO: If this snapshot now ever gets deleted, there is no way to get it back
                /// TOOD: Create instead a mechanism in <see cref="TryGenerateAndStoreSnapshotAsRelevant"/> which
                /// TODO: can understand that the first event IS actually the snapshot, and DO NOT store any snapshot here at all.
                snapshot
            }))
            {
                errorResponse =
                    "Unable to add -" + regId + "- (of type " + regHandler.GetType() + ") to -" + nameof(_snapshotsDict) + "-.\r\n" +
                    (!_snapshotsDict.TryGetValue(regId, out var snapshotsList) ? "" : (
                        "\r\nThere is already " + snapshotsList.Count + " snapshots stored for this id.\r\n" +
                        "Details:" + string.Join(", ", snapshotsList.Select(s => "CreatedTime: " + s.CreatedTime.ToStringDateAndTime()))
                    ));
                return false;
            }
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description = "Count of calls to -" + nameof(TryStoreEvent) + "- within application lifetime.")]
        public static long TryStoreEventAccessCount = 0;

        [ClassMember(Description =
            "Stores event.\r\n" +
            "\r\n" +
            "Gets transient events, and distributes them again, with the effect of cascading out the -" + nameof(ARConcepts.TransientEventTree) + "-.\r\n" +
            "\r\n" +
            "Operation is technically O(n) but with generally new events arriving in an orderly fashion it becomes more close to O(1).\r\n" +
            "\r\n" +
            "Assumes that a write-lock exists around the data storage now.\r\n" +
            "\r\n" +
            "Has no fail-scenario as of Nov 2021 (will \"always\" return TRUE), so the Try-pattern and the errorResponse parameter is superfluous."
        //"As of Nov 2021 this will never fail.\r\n" +
        //"For instance if key was not found, then a new List is created."
        )]
        public bool TryStoreEvent(IK key, Event _event, out string errorResponse)
        {
            System.Threading.Interlocked.Increment(ref TryStoreEventAccessCount);

            if (!_eventsDict.TryGetValue(key, out var eventList))
            {
                // This is equivalent to this event creating a new item (a new entity)
                eventList = _eventsDict[key] = new List<Event>();
            }

            if (_eventTimeMonotoneIncreasingGuarantee)
            {
                // Uncomment this code if you want to test the _eventTimeMonotoneIncreasingGuarantee
                // Note that this is not a "if (assert) ..." because of performance reasons
                // (it would probably be neglible anyway) but this code is performance critical and every CPU cycle helps.
                if (eventList.Count > 0 && eventList[^1].EventTime > _event.EventTime)
                {
                    throw new PCollectionESException(
                        GetType().ToStringShort() + ", key " + key + ": " +
                        "eventList.Count > 0 && eventList[^1].EventTime > _event.EventTime.\r\n" +
                        nameof(_eventTimeMonotoneIncreasingGuarantee) + " did not hold"
                    );
                }
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                eventList.Add(_event);
            }
            else
            {
                var i = eventList.Count - 1;
                while (i >= 0 && eventList[i].EventTime > _event.EventTime)
                {
                    i--;
                }
                i += 1;

                // Uncomment this code (together with ignoring _eventTimeMonotoneInceasingGuarantee above)
                // if you want to test the _eventTimeMonotoneInceasingGuarantee
                // Note that this is not a "if (assert) ..." because of performance reasons
                // (it would probably be neglible anyway) but this code is performance critical and every cpu cycle helps.
                //if (_eventTimeMonotoneIncreasingGuarantee && i != eventList.Count)
                //{
                //throw new PCollectionESException(
                //    GetType().ToStringShort() + ", key " + key + ": " +
                //    "i >= 0 && eventList[i].EventTime > _event.EventTime.\r\n" +
                //    nameof(_eventTimeMonotoneIncreasingGuarantee) + " did not hold"
                //);
                //}

                /// Note: The Insert operation is inherently O(n) but hopefully we insert at the end most of the time
                /// NOTE: This does not apply if we allow for PLANNING after <see cref="EventProcessorP.OptimizationTime"/>
                eventList.Insert(i, _event);

                // Remove all snapshots "at" or "after" this event-time
                // "at" is relevant for transient events.
                // For instance the root event may ask for a snapshot, then make changes to it through a transient event
                // The original snapshot must then be deleted because it does not fully reflect the situation 
                // at the event time (a root event together with its transient events) must be considered an atomic change.
                if (
                    _snapshotsDict.TryGetValue(key, out var snapshotList) &&
                    snapshotList.Count > 0 &&
                    !(snapshotList[^1].EventTime < _event.EventTime)
                )
                {
                    //// Uncomment this code if you want to test the _eventTimeMonotoneInceasingGuarantee
                    //// Note that this is not a "if (assert) ..." because of performance reasons
                    //// (it would probably be neglible anyway) but this code is performance critical and every cpu cycle helps.
                    //if (_eventTimeMonotoneInceasingGuarantee)
                    //{
                    //throw new PCollectionESException(
                    //    GetType().ToStringShort() + ", key " + key + ": " +
                    //    "Some snapshot had to be deleted after all, " + nameof(_eventTimeMonotoneInceasingGuarantee) + " did not hold."
                    //    nameof(_eventTimeMonotoneIncreasingGuarantee) + " did not hold"
                    //);
                    //    );
                    //}
                    _snapshotsDict[key] =
                        snapshotList.Where(s => s.EventTime < _event.EventTime). // List was already sorted, no need to sort once more.
                        ToList();
                }
            }
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public bool TryGetPV<T>(EventTime eventTime, RegTime? regTime, IK key, out T retval, out string errorResponse) where T : notnull
        {
            if (!TryGetP(eventTime, regTime, key, out var p, out var errorResponse2))
            {
                retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = GetType().ToStringVeryShort() + ": TryGetP failed in " + nameof(errorResponse) + ": " + errorResponse2;
                return false;
            }
            if (!p.TryGetV<T>(out retval, out var errorResponse3))
            {
                retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = GetType().ToStringVeryShort() + ": TryGetV on property '" + key + "' failed with " + nameof(errorResponse) + ": " + errorResponse3;
                if (this is T)
                {
                    // TODO: Consider just returning 'this' instead? 
                    errorResponse += "\r\nNOTE: 'this' object (" + GetType().ToStringShort() + ") is of type requested (" + typeof(T).ToStringShort() + ").\r\nDid you mistakenly call TryGetPV instead of TryGetP? ";
                }
                return false;
            }
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        /// <summary>
        ///"NOTE: The parameter regTime is a trap, do not use it!\r\n" +
        ///"TODO: Consider removing it altogether.\r\n" +
        ///"TODO: The trap is that one confuses the RegTime for when some final query should be valid, and " +
        ///"TODO: the RegTime related to some Event leading to transient events.\r\n" +
        ///"TODO: If the calling method has the first type of RegTime, but not the second one.\r\n" +
        ///"TOOD: Similar problem with <see cref="PCollectionESExtension.TryGetEntityInCollection{T}"/>
        ///"TODO: " +
        ///"TODO: Background: When recalculating transient events, if this parameter is set, then the entity returned " +
        ///"TODO: will only be updated up to the original RegTime of the Event being recalculated, meaning that " +
        ///"TODO: EARLIER events (in EventTime) but later RegTime will not be included.\r\n" +
        ///"TODO: Besides, generating snapshots with RegTime is also much more expensive.\r\n" +
        ///"TODO: TODO: However, if we want to limit snapshot generation by RegId (an RegId as input to PCollectionES)\r\n" +
        ///"TODO: TODO: then, TryGetTransientEvents should know about that..., which it does not now."
        /// </summary>
        /// <param name="regTime">Admiral Ackbar: Its a trap!</param>        /// <param name="key"></param>
        /// <returns></returns>
        //[ClassMember(Description =
        //    "Alternative to standard method in -" + nameof(IP) + "-.\r\n" +
        //    "NOTE: Including RegTime as parameter makes the result much more 'expensive' to generate.\r\n" +
        //    "NOTE: See -" + nameof(TryGenerateAndStoreSnapshotAsRelevant) + "- for details.\r\n"
        //)]
        // NOTE / TODO: Due to bug (as of May 2020) in ClassMemberAttribute as used by documentation, do not add a ClassMemberAttribute here. Document with ClassAttribute instead
        //
        public bool TryGetP(EventTime eventTime, RegTime? regTime, IK key, out Snapshot snapshot, out string errorResponse)
        {
            return TryGenerateAndStoreSnapshotAsRelevant(
                eventTime,
                regTime,
                key,
                out snapshot,
                out errorResponse);
        }

        [ClassMember(Description =
            "Count of calls to method -" + nameof(TryGenerateAndStoreSnapshotAsRelevant) + "- for all instances, within application lifetime.\r\n" +
            "\r\n" +
            "TODO: Expose this value through some -" + nameof(ARCQuery.EntityMethodKey) + "- somewhere.\r\n"
        )]
        public static long GlobalRequestSnapshotCount = 0;

        [ClassMember(Description =
            "Count of calls to method -" + nameof(TryGenerateAndStoreSnapshotAsRelevant) + "- for this instance.\r\n" +
            "\r\n" +
            "Exposed through -" + nameof(PCollectionESStat) + "- as -" + nameof(PCollectionESStatP.RequestSnapshotCount) + "-.\r\n"
        )]
        public long RequestSnapshotCount = 0;

        [ClassMember(Description =
            "Count of actual creation of new snapshots because of calls to method -" + nameof(TryGenerateAndStoreSnapshotAsRelevant) + "- " +
            "(for all instances within application lifetime).\r\n" +
            "\r\n" +
            "TODO: Expose this value through some -" + nameof(ARCQuery.EntityMethodKey) + "- somewhere.\r\n"
        )]
        public static long GlobalCreateNewSnapshotCount = 0;

        [ClassMember(Description =
            "Count of actual creation of new snapshots because of calls to method -" + nameof(TryGenerateAndStoreSnapshotAsRelevant) + "- " +
            "(for this instance)." +
            "\r\n" +
            "Exposed through -" + nameof(PCollectionESStat) + "- as -" + nameof(PCollectionESStatP.CreateNewSnapshotCount) + "-.\r\n"
        )]
        public long CreateNewSnapshotCount = 0;

        [ClassMember(Description =
            "Generates a new snapshot at the given -" + nameof(EventTime) + "- (unless the old one is already up-to-date).\r\n" +
            "\r\n" +
            "If parameter -" + nameof(RegTime) + "- is also given then the snapshot will only take into consideration -" + nameof(Reg) + "-s" +
            "up to and including the given time. The snapshot will also include more detailed information, like points in time " +
            "(along both the -" + nameof(ARConcepts.EventStream) + "- 'axis' and the -" + nameof(ARConcepts.RegStream) + "- axis) " +
            "for when changes where made.\r\n" +
            "NOTE: Including RegTime as parameter makes the snapshot much more 'expensive' to generate.\r\n" +
            "\r\n" +
            "Will also store the snapshot.\r\n" +
            "(unless a snapshot with -" + nameof(RegTime) + "--filter was generated, " +
            "these are for debugging and probably not wise to cache since they will typically be of single use).\r\n" +
            "\r\n" +
            "Code only assumes a general read-lock around data structures.\r\n" +
            "\r\n" +
            "TODO: Add some logic for cleaning up old snapshots.\r\n" +
            "\r\n" +
            "Note how two threads may destroy each others work here, but the only consequence is that\r\n" +
            "some snapshot has to be created anew the next time a query is made."
        )]
        private bool TryGenerateAndStoreSnapshotAsRelevant(EventTime eventTime, RegTime? regTime, IK key, out Snapshot snapshot, out string errorResponse)
        {

            // We have only a guarantee of a read lock, not write lock, therefore use of Interlocked.Increment
            System.Threading.Interlocked.Increment(ref GlobalRequestSnapshotCount);
            System.Threading.Interlocked.Increment(ref RequestSnapshotCount);

            if (!_eventsDict.TryGetValue(key, out var eventsList))
            {

                if (_snapshotsDict.TryGetValue(key, out var s))
                {
                    throw new PCollectionESException(
                        "Querying in -" + GetType().ToStringVeryShort() + "-: " +
                        "No events found for " + key + " at " + nameof(EventTime) + " " + eventTime.ToString() + " " +
                        "but found " + s.Count + " snapshots.");
                }

                snapshot = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Querying in -" + GetType().ToStringVeryShort() + "-. " +
                    "Entity with key '" + key + "' does not exist at all.\r\n" +
                    "Details: No events found";
                return false;
            }

            Snapshot? nearestSnapshot = null;

            SortedDictionary<RegTime, bool>? regTimeChanges = null;
            SortedDictionary<EventTime, bool>? eventTimeChanges = null; // Strictly speaking we only need a List for this, because events are already sorted by EventTime

            bool regTimeIsNull = true; // TODO: HACK: Not pretty
            if (regTime is RegTime regTimeNotNull)
            {
                if (_eventTimeMonotoneIncreasingGuarantee)
                {
                    // This is most probably just irrelevant.
                    throw new PCollectionESException(
                        "Code not validated for -" + nameof(_eventTimeMonotoneIncreasingGuarantee) + "- = TRUE.\r\n" +
                        "Possible resolution: Check that C# code actually is valid for this situation.\r\n" +
                        "Look out for how -" + nameof(TryStoreEvent) + "- does not delete snapshots that break up event atomicity."
                    );
                }
                regTimeIsNull = false;

                // Find all possible regTimeChanges
                regTimeChanges = new SortedDictionary<RegTime, bool>();
                eventsList.ForEach(e => regTimeChanges.TryAdd(e.RegTime, true));

                // Note that this is expensive but probably only relevant when debugging so it should not matter.
                eventsList = eventsList.Where(e => e.RegTime <= regTimeNotNull).ToList();

                // Find all possible eventTimeChanges. Note how this depends on the regTime chosen.
                eventTimeChanges = new SortedDictionary<EventTime, bool>();
                eventsList.ForEach(e => eventTimeChanges.TryAdd(e.EventTime, true));

                if (eventsList.Count == 0)
                {
                    snapshot = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse =
                        "Querying in -" + GetType().ToStringVeryShort() + "- for " + key + " with " + eventTime.ToString() + " / " + regTime.ToString() + " resulted in no events at all.\r\n" +
                        "Unable to generate snapshot";
                    return false;
                }
                if ((eventsList[0].RegHandler.GetType().ToStringVeryShort() + "Collection").Equals(this.GetType().ToStringVeryShort()))
                {
                    // Make exception, use first snapshot as starting point
                    // (because we do not have the initial events necessary, they are included IN the snapshot)

                    /// HACK: Hack in order to be able to generate super processes based on sub prosesses
                    /// HACK: through the <see cref="Snapshot"/> principle.

                    // The first event IS the actual snapshot, use first snapshot in list
                    if (!_snapshotsDict.TryGetValue(key, out var snapshotsList) || snapshotsList.Count == 0)
                    {
                        errorResponse =
                            "Querying in -" + GetType().ToStringVeryShort() + "- for " + key + " " +
                            "(querying with RegTime)\r\n" +
                            "First event looks like 'the snapshot' (stored through -" + nameof(TryStoreSnapshotDirect) + "-, because its type " +
                            "'" + eventsList[0].GetType().ToStringVeryShort() + "' matches 'this' type '" + this.GetType().ToStringVeryShort() + "'.\r\n" +
                            "Expected to find a snapshot to use as " + nameof(nearestSnapshot) + " but did not find any.";
                        snapshot = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        return false;
                    }
                    nearestSnapshot = snapshotsList[0];
                }
                else
                {
                    // Always generate a snapshot, do not look for other snapshots now                    
                }
                // Note how snapshot generated will not be stored (see code at end of this method)
            }
            else
            {
                // TODO: Delete commented out code
                //// NOTE: DO NOT! allow this. It will result in empty entries in _snapshotsDict
                //// NOTE: for keys which do not exist anyway in the Event collection.
                //// var snapshotsList = _snapshotsDict.GetOrAdd(key, k => new List<(EventTime eventTime, IP IP)>());
                //
                //// Correct approach, do not create new key in dictionary yet. 
                //var snapshotsList = _snapshotsDict.TryGetValue(key, out var tempList) ? tempList : new List<Snapshot>();
                //
                //// NOTE: Due to threading issues (we can only assume a read-lock context here)
                //// NOTE: we can not make any changes to this List now, but must create a new List before
                //// NOTE: putting everything back into the dictionary.
                //// NOTE: Is is therefore also a practical thing that the list is put into this code block (that is, invisible to the rest of the method)
                //
                //i = snapshotsList.Count - 1;
                //
                //// Skip snapshots that are into the future related to what is queried now.
                //while (i >= 0 && snapshotsList[i].EventTime > eventTime)
                //{
                //    i--;
                //}
                //
                //if (i >= 0)
                //{
                //    // We found a suitable snapshot to use as a starting point
                //    nearestSnapshot = snapshotsList[i];
                //}

                if (!_snapshotsDict.TryGetValue(key, out var list))
                {
                    // Give up finding a nearest snapshot
                }
                else if (_eventTimeMonotoneIncreasingGuarantee)
                {
                    if (list.Count != 1)
                    {
                        throw new PCollectionESException(
                            GetType().ToStringShort() + ", key " + key + ": " +
                            "Exact one snapshot expected in list when -" + nameof(_eventTimeMonotoneIncreasingGuarantee) + "- but found " + list.Count + "."
                        );
                    }
                    nearestSnapshot = list[0];
                }
                else
                {
                    // Try to find snapshot as possible starting point

                    var i = list.Count - 1;

                    // Skip snapshots that are into the future related to what is queried now.
                    while (i >= 0 && list[i].EventTime > eventTime)
                    {
                        i--;
                    }

                    if (
                        i >= 0 &&

                        /// This condition is necessary, because a set value here would indicate that snapshot was created under
                        /// <see cref="SetEventTimeMonotoneIncreasingGuarantee"/> context.
                        /// Note that we are now discarding ALL snapshots created under initialization
                        /// which is quite drastic.
                        /// TODO: Try to use snapshot even if IndexLastEventIncluded is set. We only need to check for
                        /// TODO: a few possible missing events within same event time.
                        list[i].IndexLastEventIncluded == -1
                    )
                    {
                        // We found a suitable snapshot to use as a starting point
                        nearestSnapshot = list[i];
                    }
                }
            }

            var iLastEventToInclude = eventsList.Count - 1;
            IP? entity;
            List<Event> eventsConstitutingThisSnapshot;
            List<Event> eventsDiscardedThisSnapshot;
            List<string>? errorResponses;

            if (_eventTimeMonotoneIncreasingGuarantee)
            {
                // Simplify check and initialization 

                if (eventsList[iLastEventToInclude].EventTime > eventTime)
                {
                    throw new PCollectionESException(
                        GetType().ToStringShort() + ", key " + key + ": " +
                        "eventsList[iLastEventToInclude].EventTime > eventTime.\r\n" +
                        "Details:\r\n" +
                        "iLastEventToInclude: " + iLastEventToInclude + "\r\n" +
                        "eventsList[iLastEventToInclude].EventTime: " + eventsList[iLastEventToInclude].EventTime + "\r\n" +
                        "eventTime: " + eventTime + "\r\n" +
                        nameof(_eventTimeMonotoneIncreasingGuarantee) + " did not hold."
                    );
                }

                if (!(nearestSnapshot is null))
                {
                    if (nearestSnapshot.IndexLastEventIncluded == -1 || nearestSnapshot.IndexLastEventIncluded > iLastEventToInclude)
                    {
                        throw new PCollectionESException(
                            GetType().ToStringShort() + ", key " + key + ": " +
                            "nearestSnapshot.IndexLastEventIncluded == -1 || nearestSnapshot.IndexLastEventIncluded > iLastEventToInclude.\r\n" +
                            nameof(_eventTimeMonotoneIncreasingGuarantee) + " did not hold"
                        );
                    }

                    // if (eventsList[iLastEventToInclude].EventTime == nearestSnapshot.EventTime)
                    if (iLastEventToInclude == nearestSnapshot.IndexLastEventIncluded)
                    {
                        // No change since last created, reuse this snapshot
                        nearestSnapshot.IncreaseAccessCount();
                        snapshot = nearestSnapshot;
                        errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        return true;
                    }

                    snapshot = nearestSnapshot;
                    snapshot.IncreaseAccessCount();

                    // Do not bother with DeepCopy / Copy now, objects are not going to be used on other contexts anyway
                    entity = nearestSnapshot.Entity;
                    eventsConstitutingThisSnapshot = nearestSnapshot.GetPVDirect<List<Event>>(SnapshotP.Events);
                    eventsDiscardedThisSnapshot = nearestSnapshot.GetPVDirect<List<Event>>(SnapshotP.EventsDiscarded);
                    errorResponses = nearestSnapshot.IP.TryGetPV<List<string>>(SnapshotP.ErrorResponses, out var temp) ? temp : null;
                }
                else
                {
                    // We have only a guarantee of a read lock, not write lock, therefore use of Interlocked.Increment
                    System.Threading.Interlocked.Increment(ref GlobalCreateNewSnapshotCount);
                    System.Threading.Interlocked.Increment(ref CreateNewSnapshotCount);
                    snapshot = new Snapshot();
                    snapshot.IncreaseAccessCount();

                    entity = null;

                    eventsConstitutingThisSnapshot = new List<Event>();
                    eventsDiscardedThisSnapshot = new List<Event>();
                    errorResponses = null;
                }
                snapshot.SetPVDirect(SnapshotP.Events, eventsConstitutingThisSnapshot);
                snapshot.SetPVDirect(SnapshotP.EventsDiscarded, eventsDiscardedThisSnapshot);
                // errorResponses is only set if relevant
            }
            else
            {
                // Do more careful checks and do not reuse objects, but copy them first.

                // Skip events that are into the future related to what is queried now.
                //
                // TODO: Implement binary search here.
                // TODO: This will become more important if we support future events, because
                // TODO: if most queries are made against "now" then the future events will disturb.
                // TODO: One possible solution is of course to store future events (supposedly PLANs)
                // TODO: in a separate manner.

                while (iLastEventToInclude >= 0 && eventsList[iLastEventToInclude].EventTime > eventTime)
                {
                    iLastEventToInclude--;
                }

                if (iLastEventToInclude < 0)
                {
                    if (nearestSnapshot != null)
                    {
                        throw new PCollectionESException(
                           "All Events found are after EventTime " + eventTime + " but found Snapshot at or before this time " +
                           "(" + nearestSnapshot.EventTime + ")."
                        );
                    }

                    snapshot = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse =
                        "Querying in -" + GetType().ToStringVeryShort() + "- for " + key + " with " +
                        eventTime.ToString() + " / " + (regTime?.ToString() ?? "[RegTime == NULL]") + " resulted in no events at all.\r\n" +
                        "Unable to generate snapshot";
                    return false;
                }

                if (nearestSnapshot != null)
                {
                    // See if we actually have to create a new snapshot

                    if (eventsList[iLastEventToInclude].EventTime == nearestSnapshot.EventTime)
                    {
                        // No change since last created, reuse this snapshot
                        nearestSnapshot.IncreaseAccessCount();
                        snapshot = nearestSnapshot;
                        errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        return true;
                    }
                }

                // We have only a guarantee of a read lock, not write lock, therefore use of Interlocked.Increment
                System.Threading.Interlocked.Increment(ref GlobalCreateNewSnapshotCount);
                System.Threading.Interlocked.Increment(ref CreateNewSnapshotCount);
                snapshot = new Snapshot();
                snapshot.IncreaseAccessCount();

                if (!(nearestSnapshot is null))
                {
                    // NOTE: The DeepCopy has a relatively high performance impact, but we do not know
                    // NOTE: where the original Snapshot came from. 
                    // NOTE: It may still be needed, even though we decide to delete it here afterwards, for instance another thread
                    // NOTE: might just have asked for it and is in the process of delivering it to an API client for instance.
                    // 
                    /// NOTE: Note <see cref="PExact{TEnumType}"/>'s high-performance implementation <see cref="PExact{TEnumType}.DeepCopy"/>
                    entity = nearestSnapshot.Entity.DeepCopy();

                    eventsConstitutingThisSnapshot = nearestSnapshot.GetPVDirect<List<Event>>(SnapshotP.Events).
                        ToList(); // Important! Create copy. Older snapshot may still be of use, do not tamper with its objects

                    eventsDiscardedThisSnapshot = nearestSnapshot.GetPVDirect<List<Event>>(SnapshotP.EventsDiscarded).
                        ToList(); // Important! Create copy. Older snapshot may still be of use, do not tamper with its objects

                    errorResponses = nearestSnapshot.IP.TryGetPV<List<string>>(SnapshotP.ErrorResponses, out var temp) ?
                        temp.ToList() : // Important! Create copy. Older snapshot may still be of use, do not tamper with its objects
                        null;
                }
                else
                {
                    entity = null;

                    eventsConstitutingThisSnapshot = new List<Event>();
                    eventsDiscardedThisSnapshot = new List<Event>();
                    errorResponses = null;
                }
                snapshot.SetPVDirect(SnapshotP.Events, eventsConstitutingThisSnapshot);
                snapshot.SetPVDirect(SnapshotP.EventsDiscarded, eventsDiscardedThisSnapshot);
                // errorResponses is only set if relevant
            }

            if (entity is null) entity = (IP)System.Activator.CreateInstance(IP.AllIPDerivedTypesDict.GetValue(
                // TODO: Create string in better manner, remove Collection at end instead of replacing.
                GetType().ToStringShort().Replace("Collection", ""), () =>
                "Querying for " + key + ": " +
                "Unable to find the assumed IP derived type '" + GetType().ToStringShort().Replace("Collection", "") + "' " +
                "which this collection (" + GetType().ToStringShort() + ") contains. " +
                "(unable to create an instance of that type)"
            ));

            // TODO: Make suggestion for new values for EventTime and Snapshot time in the snapshot returned
            // TODO: So we can browse back and forth between snapshots easily

            // Go further back in the event-stream until we reach the first event matching the snapshot
            int iFirstEventToInclude;
            if (nearestSnapshot is null)
            {
                iFirstEventToInclude = 0;
            }
            else if (_eventTimeMonotoneIncreasingGuarantee)
            {
                iFirstEventToInclude = nearestSnapshot.IndexLastEventIncluded + 1;
                if (iFirstEventToInclude == 0 || iFirstEventToInclude > iLastEventToInclude || iFirstEventToInclude >= eventsList.Count)
                {
                    throw new PCollectionESException(
                        GetType().ToStringShort() + ", key " + key + ": " +
                        "iFirstEventToInclude == 0 || iFirstEventToInclude > iLastEventToInclude || iFirstEventToInclude >= eventsList.Count.\r\n" +
                        "Details:\r\n" +
                        "iFirstEventToInclude: " + iFirstEventToInclude + "\r\n" +
                        "iLastEventToInclude: " + iLastEventToInclude + "\r\n" +
                        "eventsList.Count: " + eventsList.Count + "\r\n" +
                        nameof(_eventTimeMonotoneIncreasingGuarantee) + " did not hold"
                    );
                }
            }
            else
            {
                iFirstEventToInclude = iLastEventToInclude;
                var et = nearestSnapshot.EventTime;
                while (iFirstEventToInclude > 0 && eventsList[iFirstEventToInclude - 1].EventTime > et)
                {
                    iFirstEventToInclude--;
                }
                if (iFirstEventToInclude == 0)
                {
                    throw new DatastructureException(
                        GetType().ToStringShort() + ", key " + key + ": " +
                        "Found snapshot with event time " + et + " but all events in list is after that time"
                    );
                }
            }

            var lastEventTime = new EventTime(DateTime.MinValue, 0); // Only used for assertion

            /// Used to decide what to do with <see cref="RegVerb.PLAN"/>
            var now = EventTime.NowAsQuery();

            for (var i = iFirstEventToInclude; i <= iLastEventToInclude; i++)
            {
                // Apply the events in order

                var e = eventsList[i];
                var nextE = i >= iLastEventToInclude ? null : eventsList[i + 1];

                if (e.EventTime < lastEventTime)
                {
                    throw new DatastructureException(
                        "Event " + e.EventTime + " was found stored AFTER " + lastEventTime + ", " +
                        "for key " + key + " in " + GetType().ToStringShort() + "\r\n"
                    );
                }
                lastEventTime = e.EventTime;

                // This was buggy, eventsList[i].EventTime instead of eventsList[i+1].EventTime
                // Removed 17 Jan 2022
                //if (i < eventsList.Count - 1 && eventsList[i].EventTime == e.EventTime)
                //{
                //    // Next Event is the same Registration.
                //    // TODO: Ad checks here.
                //}

                if (!(e.ReplacedRegTime is null) && (regTimeIsNull || (regTime!) >= e.ReplacedRegTime))
                {
                    /// Ignore this event. 
                    /// It is a transient event that became replaced at a given RegTime
                    /// by <see cref="EventProcessor.MarkTransientEventsAsReplacedRecursively"/>
                    /// but not removed from List because we want to scroll backwards in RegTime.

                    // TODO: Consider adding key with reason here.
                    eventsDiscardedThisSnapshot.Add(e);
                }
                else if (e.IsRootEvent && nextE != null && nextE.IsRootEvent && e.EventTime == nextE.EventTime)
                {
                    // TODO: Design this properly. For instance, always match, DO with UNDO, BLOCK with ALLOW, DELETE with RESTORE and so on.
                    // TODO: And do not store incoming Registrations at all if not properly matched.

                    // Ignore event, replaced by new event.
                    // DO replaced with UNDO or new DO for instance (or UNDO replaced with DO)
                    // Just use the latest event.

                    // TODO: Consider adding key with reason here.
                    eventsDiscardedThisSnapshot.Add(e);
                }
                else
                {
                    /// Call 
                    /// <see cref="IRegHandlerTATPP.TryAttachToParentProcess"/> and
                    /// <see cref="IRegHandlerTAERTE.TryApplyEventResultToEntity"/>
                    /// as relevant

                    if (!(e.RegHandler is IRegHandlerTATPP tatpp))
                    {
                        // registration handler does not attach to parent process at all
                    }
                    else
                    {
                        /// Note how <see cref="IRegHandlerTATPP"/> is called regardless of EventStatus (OK or ERROR)
                        /// or ActiomType (DO / PLAN)
                        /// This is necessary in order for sub processes to show up in a stable manner as sub processes of super processes
                        /// (if not they would "magically" appear and disappear as retroactive changes changes their status).
                        if (!tatpp.TryAttachToParentProcess(e.RegTime.SerialNumber, entity, out var errorResponseTemp))
                        {
                            if (errorResponses == null) errorResponses = new List<string>();
                            errorResponses.Add(
                                "Event " + e.EventTime + ", registration " + e.RegHandler.GetType().ToStringShort() + ":\r\n" +
                                "-" + nameof(IRegHandlerTATPP.TryAttachToParentProcess) + "-: " + errorResponseTemp
                            );
                        }
                        // TODO: Currently, if both tatpp and taerte was relevant, event will be added twice
                        eventsConstitutingThisSnapshot.Add(e);
                    }

                    if (!(e.RegHandler is IRegHandlerTAERTE taerte))
                    {
                        // registration handler does not apply event results to entity at all
                    }
                    else
                    {
                        switch (e.EventStatus)
                        {
                            case EventStatus.ERROR:
                                if (errorResponses == null) errorResponses = new List<string>();
                                errorResponses.Add(
                                    e.EventTime + ": " + e.RegHandler.Serialize() + ".\r\n" +
                                    "At -" + nameof(Event.LastCallToTryGenerateAndDistributeTransientEventsRecursively) + "- " + e.LastCallToTryGenerateAndDistributeTransientEventsRecursively + ": " + (
                                        e.ErrorResponse ?? "[No ErrorResponse information]"
                                    )
                                );
                                break;
                            default:
                                switch (e.RegVerb)
                                {
                                    case RegVerb.DO:
                                    case RegVerb.PLAN:
                                        if (e.RegVerb == RegVerb.PLAN && e.EventTime < now)
                                        {
                                            // The plan is in the past, and should therefore no longer be considered.
                                            // (it "should" have been replaced with a 'DO' by now)
                                            break;
                                        }
                                        if (!taerte.TryApplyEventResultToEntity(entity, out var errorResponseTemp))
                                        {
                                            if (errorResponses == null) errorResponses = new List<string>();
                                            errorResponses.Add(
                                                "Event " + e.EventTime + ", registration " + e.RegHandler.GetType().ToStringShort() + ":\r\n" +
                                                "-" + nameof(IRegHandlerTAERTE.TryApplyEventResultToEntity) + "-: " + errorResponseTemp
                                            );
                                        }
                                        // TODO: Currently, if both tatpp and taerte was relevant, event will be added twice
                                        eventsConstitutingThisSnapshot.Add(e);
                                        break;
                                    default:
                                        // TODO: Add support for DO, UNDO, BLOCK, ALLOW and so on.
                                        break;
                                }
                                break;
                        }
                    }
                }
            }

            var snapshot2 = snapshot; // snapshot is "out"-parameter, can not be used inside lambdas below

            snapshot.SetPVDirect(SnapshotP.EventTime, eventsList[iLastEventToInclude].EventTime);

            if (_eventTimeMonotoneIncreasingGuarantee)
            {
                snapshot.IndexLastEventIncluded = iLastEventToInclude;

                /// Probably unnecessary, see instead <see cref="Snapshot.IndexLastEventIncluded"/>
                // snapshot.SetPVDirect(SnapshotP.LastEntityEventIndex, (long)iLastEventToInclude)

                if (!(nearestSnapshot is null))
                {
                    // Other values have already been set
                }
                else
                {
                    snapshot.SetPVDirect(SnapshotP.Entity, entity);
                    snapshot.SetPVDirect(SnapshotP.CreatedTime, UtilCore.DateTimeNow);
                }
                // Do not set other values, like
                //snapshot.SetPVDirect(SnapshotP.NewEventsProcessed, (long)(iLastEventToInclude - iFirstEventToInclude + 1));
            }
            else
            {
                snapshot.SetPVDirect(SnapshotP.Entity, entity);
                snapshot.SetPVDirect(SnapshotP.CreatedTime, UtilCore.DateTimeNow);
                if (nearestSnapshot != null)
                {
                    snapshot.SetPVDirect(SnapshotP.BaseSnapshotEventTime, nearestSnapshot.EventTime);
                }
                snapshot.SetPVDirect(SnapshotP.NewEventsProcessed, (long)(iLastEventToInclude - iFirstEventToInclude + 1));

                /// Probably unnecessary, see instead <see cref="Snapshot.IndexLastEventIncluded"/>
                // snapshot.SetPVDirect(SnapshotP.LastEntityEventIndex, (long)iLastEventToInclude);
            }

            if (errorResponses != null)
            {
                snapshot.SetPVDirect(SnapshotP.ErrorResponses, errorResponses);
            }

            // NOTE: Note how an in-memory structure makes it very cheap to store
            // NOTE: the created snapshot now.
            if (regTime is RegTime tempRegTime)
            {
                /// Registration time was specified. Take this as a hint that the user is probably a developer or that
                /// some debugging is going on. Give suggestions for browsing in both
                /// <see cref="RegTime"/> and <see cref="EventTime"/>
                (eventTimeChanges?.Keys.FirstOrDefault(e => e > eventTime) ?? null).Use(e =>
                {
                    if (!(e is null)) snapshot2.SetPVDirect(SnapshotP.NextEventTime, e);
                });
                (eventTimeChanges?.Keys.Reverse().FirstOrDefault(e => e < eventTime) ?? null).Use(e =>
                {
                    if (!(e is null)) snapshot2.SetPVDirect(SnapshotP.PreviousEventTime, e);
                });
                (regTimeChanges?.Keys.FirstOrDefault(e => e > regTime) ?? null).Use(e =>
                {
                    if (!(e is null)) snapshot2.SetPVDirect(SnapshotP.NextRegTime, e);
                });
                (regTimeChanges?.Keys.Reverse().FirstOrDefault(e => e < regTime) ?? null).Use(e =>
                {
                    if (!(e is null)) snapshot2.SetPVDirect(SnapshotP.PreviousRegTime, e);
                });

                snapshot.SetPVDirect(SnapshotP.RegTime, tempRegTime);

                // Do not store snapshot at all
            }
            else if (_eventTimeMonotoneIncreasingGuarantee)
            {
                if (!(nearestSnapshot is null))
                {
                    // No need for storing at all
                }
                else
                {
                    /// TODO: Remember use of <see cref="TryStoreSnapshotDirect"/>
                    _snapshotsDict[key] = new List<Snapshot> { snapshot };
                }
            }
            else
            {

                // Get list again, copy it, make corrections, and insert it back into the dictionary

                // NOTE: Due to threading issues (we can only assume a read-lock context here)
                // NOTE: we can not make any changes to this List now, but must create a new List before
                // NOTE: putting everything back into the dictionary.

                // This should not matter from a practical perspective as the List operations are 
                // probably much less expensive than building the snapshot was

                // NOTE: If two threads creates lists simultaneously, which one gets stored 
                // NOTE: is not very significant.

                // TODO: Implement some deletion of snapshots.
                // TODO: This is especially relevant after playback of Registrations at application startup
                // TODO: (most snapshots being generated will never have to be used again)
                // TODO: 
                // TODO: Design suggestions:
                // TODO: Check count of snapshots, delete if more than a specific count
                // TODO: Maybe add some hints at entity level 

                List<Snapshot> newList;
                if (!_snapshotsDict.TryGetValue(key, out var existingList))
                {
                    newList = new List<Snapshot> { snapshot };
                }
                else
                {
                    // TODO: This code is very inefficient.
                    // TODO: Since we for instance know that list is already sorted, we can make use of that.
                    newList = existingList.

                        // TODO: Either avoid this check at all, or avoid doing it every time because it is probably
                        // TODO: only relevant just after startup of application.
                        Where(s => s.IndexLastEventIncluded == -1).

                        Append(snapshot).
                        OrderBy(s => s.EventTime).
                        ToList();
                }

                _snapshotsDict[key] = newList;

                // TODO: Add cleanup of snapshot list, now it only grows and grows...
            }

            // NOTE: This is tempting but dangerous
            /// TODO: DO NOT USE THIS. Will entail writing to Dictionary under a multithreaded scenario with no locking
            /// TODO: Consider using <see cref="PConcurrent"/> as base for Snapshot, but take into account memory usage 
            //((IP)nearestSnapshot).Inc(SnapshotP.AccessCount);
            //((IP)nearestSnapshot).SetPV(SnapshotP.LastAccessedAt, UtilCore.DateTimeNow);

            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public class PCollectionESException : ApplicationException
        {
            public PCollectionESException(string message) : base(message) { }
            public PCollectionESException(string message, Exception inner) : base(message, inner) { }
        }
    }

    public static class PCollectionESExtension
    {


        public static PCollectionES GetCollection(this IP dataStorage, Type entityType, Func<PCollectionES>? defaultValueFactory = null) =>
            TryGetCollection(dataStorage, entityType, out var temp, out var errorResponse) ? temp : defaultValueFactory?.Invoke() ??
                throw new PCollectionESExtensionException(errorResponse);

        [ClassMember(Description =
            "Finds the corresponding -" + nameof(PCollectionES) + " for the given entity type.\r\n" +
            "\r\n" +
            "TODO: Create alternative, TryGetInCollection which gives out a strongly typed entity object.\r\n" +
            "TODO: as that is what is most often needed."
        )]
        public static bool TryGetCollection(this IP dataStorage, Type entityType, out PCollectionES collection, out string errorResponse)
        {
            InvalidTypeException.AssertAssignable(entityType, typeof(IP), () =>
                 "It is meaningless to ask for a collection of objects which do not inherit -" + nameof(IP) + "-"
            );
            if (!dataStorage.TryGetP<IP>(
                /// This looks intuitive but is wrong. It is related to how <see cref="ARConcepts.PropertyStream"/> are parsed, 
                /// like "Customer/42/FirstName = John" in which case "Customer" must be the Key for the Collection
                /// IKString.FromCache(entityType.ToStringVeryShort() + "Collection"),

                // This is correct
                IKType.FromType(entityType),
                out var temp,
                out errorResponse)
            )
            {
                collection = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                var t = entityType.ToStringShort();
                errorResponse += "\r\n" +
                    "Details: Collection for " + t + " ('" + t + "Collection') not found. " +
                    "Possible resolution: Initialize collection at application startup.\r\n";
                return false;

                // Abandoned approach
                //// It is probably better to throw an Exception now, because a failure here will 
                //// make it impossible to store events.
                //throw new DatastructureException(
                //    "Collection for " + entityType.ToStringShort() + " not found.\r\n" +
                //    "Possible resolution: Initialize collection at application startup.\r\n" +
                //    (
                //        !dataStorage.ContainsKey(PSPrefix.dt.ToString()) ? "" :
                //        "Possible cause: (because given dataStorage contains 'dt'). Query was made against root-object in data storage " +
                //        "instead of for instance 'dt' which usually contains entity collections.\r\n"
                //    )
                //);
            }
            /// TODO: Consider whether to assert this or not
            /// TODO: Although we have abandonded the generic typing, we can still ask <see cref="IP.AllIPDerivedTypesDict"/> for 
            /// TODO: the correct collection type to assert against, but it will decrease performance.
            //if (!(temp is T temp2)) {
            //    // This is NOT normal. An event has been set up with a class not supported by a corresponding PCollectionES
            //    throw new InvalidObjectTypeException(temp, typeof(PCollectionES),
            //        "The caller expected the type " + type.ToStringShort() + " to be supported " +
            //        "by a corresponding collection of type " + typeof(PCollectionES).ToStringShort() + " " +
            //        "but the collection found was of type " + temp.GetType().ToStringShort() + ".\r\n" +
            //        "\r\n" +
            //        "This error may be caused by an incoherent pair of 'type' and '<T>' parameters given to this method.\r\n"
            //    );
            //}

            if (!(temp is PCollectionES temp2))
            {
                // This is NOT normal. An event has been set up with a class not supported by a corresponding PCollectionES
                throw new InvalidObjectTypeException(temp, typeof(PCollectionES),
                    "The caller expected the type " + entityType.ToStringShort() + " to be supported " +
                    "by a corresponding collection of base type " + typeof(PCollectionES).ToStringShort() + " " +
                    "but the collection found was of type " + temp.GetType().ToStringShort() + ".\r\n" +
                    (!typeof(PCollection).IsAssignableFrom(temp.GetType()) ? "" :
                        "(The type found implements " + nameof(PCollection) + " however, but that is not good enough here).\r\n"
                    ) +
                    "\r\n" +
                    "This error may be caused by an incoherent pair of 'type' and '<T>' parameters given to this method.\r\n"
                );
            }

            collection = temp2;
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public static TCollectionType GetCollection<TCollectionType>(this IP dataStorage, Func<TCollectionType>? defaultValueFactory = null) where TCollectionType : PCollectionES =>
            TryGetCollection<TCollectionType>(dataStorage, out var temp, out var errorResponse) ? temp : defaultValueFactory?.Invoke() ??
                throw new PCollectionESExtensionException(errorResponse);

        [ClassMember(Description =
            "Finds the corresponding collection (a sub class of -" + nameof(PCollectionES) + ") for the given entity type.\r\n" +
            "\r\n" +
            "Assumes that the key for the collection is (somewhat non-intuitively) the name of the requested type MINUS 'Collection'.\r\n" +
            "Although 'Customer' becomes 'CustomerCollection', the key for the collection is still 'Customer'.\r\n" +
            "It is related to how Property-streams are parsed,\r\n" +
            "like \"Customer/42/FirstName = John\" in which case \"Customer\" must be the Key for the Collection"
        )]
        public static bool TryGetCollection<TCollectionType>(this IP dataStorage, out TCollectionType collection, out string errorResponse) 
            where TCollectionType : PCollectionES
        {
            if (!dataStorage.TryGetP<IP>(

                // This looks intuitive but is wrong. It is related to how Property-streams are parsed, 
                // like "Customer/42/FirstName = John" in which case "Customer" must be the Key for the Collection
                // IKType.FromType(typeof(TCollectionType))

                // This is correct
                IKString.FromCache(typeof(TCollectionType).ToStringVeryShort().Replace("Collection", "")),

                out var temp,
                out errorResponse)
            )
            {
                collection = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse += "\r\nDetails: " +
                    "Collection for " + typeof(TCollectionType).ToStringShort() + " not found. " +
                    "Looked for key '" + typeof(TCollectionType).ToStringVeryShort().Replace("Collection", "") + "'.\r\n" +
                    (!dataStorage.ContainsKey(PSPrefix.dt.ToString()) ? "" : "Parameter dataStorage contains 'dt', did you use the wrong hierarchical level?\r\n") +
                    "Possible resolution: Initialize collection at application startup.";
                return false;

                // Abandoned approach
                //// It is probably better to throw an Exception now, because a failure here will 
                //// make it impossible to store events.
                //throw new DatastructureException(
                //    "Collection for " + typeof(TCollectionType).ToStringShort() + " not found.\r\n" +
                //    "Possible resolution: Initialize collection at application startup.");
            }

            if (!(temp is TCollectionType temp2))
            {
                throw new InvalidObjectTypeException(temp, typeof(TCollectionType));
            }

            collection = temp2;
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;

        }

        /// <param name="regTime">Admiral Ackbar: Its a trap!</param>
        [ClassMember(Description =
            "Returns a strongly type entity object within the corresponding -" + nameof(PCollectionES) + "-.\r\n" +
            "\r\n" +
            "NOTE: The parameter regTime is a trap, do not use it!\r\n" +
            "TODO: Consider removing it altogether.\r\n" +
            "TODO: The trap is that one confuses the RegTime for when some final query should be valid, and " +
            "TODO: the RegTime related to some Event leading to transient events.\r\n" +
            "TODO: The calling method probably has the first type of RegTime, but not the second one.\r\n" +
            "TOOD: Similar problem with -" + nameof(PCollectionES.TryGetP) + "-.\r\n" +
            "TODO: " +
            "TODO: Background: When recalculating transient events, if this parameter is set, then the entity returned " +
            "TODO: will only be updated up to the original RegTime of the Event being recalculated, meaning that " +
            "TODO: EARLIER events (in EventTime) but later RegTime will not be included.\r\n" +
            "TODO: Besides, generating snapshots with RegTime is also much more expensive.\r\n" +
            "TODO: TODO: However, if we want to limit snapshot generation by RegId (an RegId as input to PCollectionES)\r\n" +
            "TODO: TODO: then, TryGetTransientEvents should know about that..., which it does not now."
        )]
        public static bool TryGetEntityInCollection<T>(this IP dataStorage, EventTime eventTime, RegTime? regTime, IK entityKey, out T entity, out string errorResponse) where T : IP
        {
            if (
                !dataStorage.TryGetCollection(typeof(T), out var collection, out errorResponse) ||
                !collection.TryGetP(eventTime, regTime, entityKey, out var snapshot, out errorResponse) ||
                !snapshot.TryGetEntity<T>(out entity, out errorResponse)
            )
            {
                entity = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        /// <param name="regTime">Admiral Ackbar: Its a trap!</param>
        [ClassMember(Description =
            "Overload when the entity type differs from the return type desired.\r\n" +
            "\r\n" +
            "Useful for instance when caller needs a super class type / interface, but we need to specify entity type in order " +
            "to know which collection to look into.\r\n" +
            "\r\n" +
            "TODO: As of Dec 2021 only used from -" + nameof(EXEcuteR) + "-, consider placing it there instead."
        )]
        public static bool TryGetEntityInCollection<TReturnType>(this IP dataStorage, EventTime eventTime, RegTime? regTime, Type entityType, IK entityKey, out TReturnType entity, out string errorResponse) where TReturnType : IP
        {
            if (
                !dataStorage.TryGetCollection(entityType, out var collection, out errorResponse) ||
                !collection.TryGetP(eventTime, regTime, entityKey, out var snapshot, out errorResponse) ||
                !snapshot.TryGetEntity<TReturnType>(out entity, out errorResponse)
            )
            {
                entity = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public class PCollectionESExtensionException : ApplicationException
        {
            public PCollectionESExtensionException(string message) : base(message) { }
            public PCollectionESExtensionException(string message, Exception inner) : base(message, inner) { }
        }

    }

    [Class(Description =
        "Returns detailed information about a specific instance of -" + nameof(PCollectionES) + "-.\r\n" +
        "\r\n" +
        "Especially number of entities, events and number of snapshots stored.\r\n" +
        "\r\n" +
        "Available through -" + nameof(ARComponents.ARCQuery) + "- as 'RQ/SELFCREATE PCollectionESStat'\r\n" +
        "\r\n" +
        "See also -" + nameof(PCollectionESStatP) + "-.\r\n"
    )]
    public class PCollectionESStat : PRich, ARCQuery.QueryExpressionSelfCreate.ISelfCreateCollection
    {
        public static bool TrySelfCreateCollection(IP dataStorage, out IP pCollectionESStatCollection, out string errorResponse)
        {
            if (!dataStorage.TryGetP<IP>(PSPrefix.dt.ToString(), out var dt, out errorResponse))
            {
                pCollectionESStatCollection = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }

            pCollectionESStatCollection = new PRich();

            foreach (var ikip in dt)
            {
                if (!(ikip.P is PCollectionES c)) continue;

                var s = (IP)new PCollectionESStat();
                pCollectionESStatCollection.AddP(ikip.Key, s);
                s.AddPV(PCollectionESStatP.CollectionType, c.GetType());
                if (!IP.AllIPDerivedTypesDict.TryGetValue(c.GetType().ToStringShort().Replace("Collection", ""), out var entityType))
                {
                    throw new DatastructureException(
                        "Corresponding entity type '" + c.GetType().ToStringShort().Replace("Collection", "") + "' " +
                        "not found for collection " + c.GetType().ToStringShort());
                }
                s.AddPV(PCollectionESStatP.EntityType, entityType);

                s.AddPV(PCollectionESStatP.EventsDictCount, c.EventsDictCount());
                s.AddPV(PCollectionESStatP.SnapshotsDictCount, c.SnapshotsDictCount());

                var eventsTotalCount = c.EventsTotalCount();
                var snapshotsTotalCount = c.SnapshotsTotalCount();
                s.AddPV(PCollectionESStatP.EventsTotalCount, eventsTotalCount);
                var eventsInvalidCount = c.EventsInvalidCount();
                s.AddPV(PCollectionESStatP.EventsInvalidCount, eventsInvalidCount);
                s.AddPV(PCollectionESStatP.SnapshotsTotalCount, snapshotsTotalCount);
                if (snapshotsTotalCount != 0) s.AddPV(PCollectionESStatP.EventsPerSnapshot, (double)eventsTotalCount / snapshotsTotalCount);
                var eventsValidCount = eventsTotalCount - eventsInvalidCount;
                s.AddPV(PCollectionESStatP.EventsValidCount, eventsValidCount);
                if (snapshotsTotalCount != 0) s.AddPV(PCollectionESStatP.ValidEventsPerSnapshot, (double)eventsValidCount / snapshotsTotalCount);

                s.AddPV(PCollectionESStatP.RequestSnapshotCount, c.RequestSnapshotCount);
                s.AddPV(PCollectionESStatP.CreateNewSnapshotCount, c.CreateNewSnapshotCount);
            }

            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }
    }

    [Enum(
        Description = "Describes class -" + nameof(PCollectionESStat) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum PCollectionESStatP
    {
        __invalid,

        [PKType(Type = typeof(Type))]
        CollectionType,

        [PKType(Type = typeof(Type))]
        EntityType,

        [PKType(Type = typeof(long))]
        EventsDictCount,

        [PKType(Type = typeof(long))]
        SnapshotsDictCount,

        [PKType(Type = typeof(long))]
        EventsTotalCount,

        [PKType(Type = typeof(long))]
        EventsInvalidCount,

        [PKType(Type = typeof(long))]
        SnapshotsTotalCount,

        [PKType(Type = typeof(double))]
        EventsPerSnapshot,

        [PKType(Type = typeof(long))]
        EventsValidCount,

        [PKType(Type = typeof(double))]
        ValidEventsPerSnapshot,

        [PKType(
            Description = "For documentation see -" + nameof(PCollectionES.RequestSnapshotCount) + "-.",
            Type = typeof(long)
        )]
        RequestSnapshotCount,

        [PKType(
            Description = "For documentation see -" + nameof(PCollectionES.CreateNewSnapshotCount) + "-.",
            Type = typeof(long)
        )]
        CreateNewSnapshotCount
    }
}