﻿// Copyright (c) 2016-2021 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery {
    [Class(Description =
        "Calculates the minimum value of the given properties.\r\n" +
        "\r\n" +
        "Any property not convertible to type of double is regarded as 0.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyAggregateMin : FunctionKeyAggregate {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'Min()'\r\n" +
            "Examples:\r\n" +
            "For 'Customer', 'SELECT Order.Orderline.Sum.Min()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyAggregateMin));

        public override bool TryGetPFromList(IEnumerable<IP> listIP, out IP retval, out string errorResponse) {
            var l = listIP.Select(ip => ip.GetV(0d)).ToList(); // Note how we ignore invalid values (through default parameter 0d)
            retval = new PValue<double>(l.Count == 0 ? 0 : l.Min());
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public new static FunctionKeyAggregateMin Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyAggregateMinException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyAggregateMin retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyAggregateMin retval, out string errorResponse) =>
            TryParseSingleWord(value, "min", () => new FunctionKeyAggregateMin(), () => SyntaxHelp, out retval, out errorResponse);

        public override string ToString() => "Min()";
        public class InvalidFunctionKeyAggregateMinException : ApplicationException {
            public InvalidFunctionKeyAggregateMinException(string message) : base(message) { }
            public InvalidFunctionKeyAggregateMinException(string message, Exception inner) : base(message, inner) { }
        }
    }
}