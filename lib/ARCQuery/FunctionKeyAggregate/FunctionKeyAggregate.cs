﻿// Copyright (c) 2016-2021 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery {

    [Class(Description =
        "Operates over a collection of properties.\r\n" +
        "\r\n" +
        "Specially understood by -" + nameof(ForeignKey) + "-.\r\n" +
        "\r\n" +
        "-" + nameof(FunctionKey.TryGetP) + "- is implicit understood to receive an instance of -" + nameof(IP) + "- containing an IEnumerable<IP>-collection as value.\r\n" +
        "\r\n" +
        "Implementing classes in -" + nameof(ARConcepts.StandardAgoRapideCode) + "-, more specific -" + nameof(ARComponents.ARCQuery) + "-, are:\r\n" +
        "-" + nameof(FunctionKeyAggregateAvg) + "-\r\n" +
        "-" + nameof(FunctionKeyAggregateCount) + "-\r\n" +
        "-" + nameof(FunctionKeyAggregateDistinct) + "-\r\n" +
        "-" + nameof(FunctionKeyAggregateJoin) + "-\r\n" +
        "-" + nameof(FunctionKeyAggregateMax) + "-\r\n" +
        "-" + nameof(FunctionKeyAggregateMin) + "-\r\n" +
        "-" + nameof(FunctionKeyAggregateSingle) + "-\r\n" +
        "-" + nameof(FunctionKeyAggregateSum) + "-\r\n" +
        "\r\n" +
        "Implementing classes that returns PValue<List<IP>> are:\r\n" +
        "-" + nameof(FunctionKeyAggregateDistinct) + "-\r\n" +
        "These usually give meaning only if followed by an 'ordinary' implementation that returns a PValue containing a single value.\r\n" +
        "This is specially understood by -" + nameof(ForeignKey.TryGetPInternal) + "-.\r\n" +
        "Stored in -" + nameof(ForeignKey) + "- as -" + nameof(ForeignKey.AggregateKeys) + "-.\r\n" +
        "\r\n" +
        "See also:\r\n" +
        "-" + nameof(QueryExpressionAggregate) + "- which aggregates over a key\r\n" +
        "and\r\n" +
        "-" + nameof(QueryExpressionPivot) + "- which creates a table by querying over two keys.\r\n"
    )]
    public abstract class FunctionKeyAggregate : FunctionKey {

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) {
            if (!p.TryGetV<IEnumerable<IP>>(out var listP)) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse =
                    "Parameter p does not contain a value of type List<IP>.\r\n" +
                    "The value contained is of type: " + (p.TryGetV<object>(out var o) ? o.GetType().ToString() : "[NOT FOUND]") + ".\r\n";
                return false;
            }
            return TryGetPFromList(listP, out retval, out errorResponse);
        }

        public abstract bool TryGetPFromList(IEnumerable<IP> listIP, out IP retval, out string errorResponse);
    }
}
