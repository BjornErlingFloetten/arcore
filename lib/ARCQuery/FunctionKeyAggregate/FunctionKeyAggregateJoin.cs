﻿// Copyright (c) 2016-2021 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery {
    [Class(Description =
        "Joins the given list of properties into a single property.\r\n" +
        "\r\n" +
        "Returns a PValue<List<string>>, that is, does not actually join the values because that is not necessary.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyAggregateJoin : FunctionKeyAggregate {

        /// <Joinmary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </Joinmary>
        [ClassMember(Description =
            "'Join()'\r\n" +
            "Examples:\r\n" +
            "For 'Customer', 'SELECT Order.OrderLine.Product.Name.Distinct().Join()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyAggregateJoin));

        public override bool TryGetPFromList(IEnumerable<IP> listIP, out IP retval, out string errorResponse) {
            // TODO: Make more strongly typed, if for instance listIP consist of PValue<DateTime> objects, return PValue<List<DateTime>> instead.
            retval = new PValue<List<string>>(listIP.Select(ip => ip.TryGetV<string>(out var s) ? s : null).Where(s => s != null).Select(s => s!).ToList());
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public new static FunctionKeyAggregateJoin Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyAggregateJoinException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyAggregateJoin retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyAggregateJoin retval, out string errorResponse) =>
            TryParseSingleWord(value, "join", () => new FunctionKeyAggregateJoin(), () => SyntaxHelp, out retval, out errorResponse);

        public override string ToString() => "Join()";
        public class InvalidFunctionKeyAggregateJoinException : ApplicationException {
            public InvalidFunctionKeyAggregateJoinException(string message) : base(message) { }
            public InvalidFunctionKeyAggregateJoinException(string message, Exception inner) : base(message, inner) { }
        }
    }
}