﻿// Copyright (c) 2016-2021 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery {
    [Class(Description =
        "Counts the number of properties found.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyAggregateCount : FunctionKeyAggregate {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'Count()'\r\n" +
            "Examples:\r\n" +
            "'Order/Orderline.OrderlineId.Count()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyAggregateCount));

        public override bool TryGetPFromList(IEnumerable<IP> listIP, out IP retval, out string errorResponse) {
            retval = new PValue<int>(listIP.Count());
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }
            
        public new static FunctionKeyAggregateCount Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyAggregateCountException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyAggregateCount retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyAggregateCount retval, out string errorResponse) =>
            TryParseSingleWord(value, "count", () => new FunctionKeyAggregateCount(), () => SyntaxHelp, out retval, out errorResponse);

        public override string ToString() => "Count()";
        public class InvalidFunctionKeyAggregateCountException : ApplicationException {
            public InvalidFunctionKeyAggregateCountException(string message) : base(message) { }
            public InvalidFunctionKeyAggregateCountException(string message, Exception inner) : base(message, inner) { }
        }
    }
}