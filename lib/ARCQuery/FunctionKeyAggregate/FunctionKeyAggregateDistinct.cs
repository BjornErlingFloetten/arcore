﻿// Copyright (c) 2016-2021 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery {
    [Class(Description =
        "Returns a list of the distinct values of the given properties.\r\n" +
        "\r\n" +
        "Any property not of type -" + nameof(PValue<TValue>) + "- will be ignored.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyAggregateDistinct : FunctionKeyAggregate {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'Distinct()'\r\n" +
            "Examples:\r\n" +
            "For 'Customer', 'SELECT Order.Orderline.Product.Name.Distinct().Join()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyAggregateDistinct));

        public override bool TryGetPFromList(IEnumerable<IP> listIP, out IP retval, out string errorResponse) {
            var l = listIP.Where(ip => {
                var t = ip.GetType();
                return t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(PValue<>));
            }).Distinct().ToList(); // Note how we ignore non PValue<TValue> types.
            retval = new PValue<List<IP>>(l);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public new static FunctionKeyAggregateDistinct Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyAggregateDistinctException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyAggregateDistinct retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyAggregateDistinct retval, out string errorResponse) =>
            TryParseSingleWord(value, "distinct", () => new FunctionKeyAggregateDistinct(), () => SyntaxHelp, out retval, out errorResponse);

        public override string ToString() => "Distinct()";
        public class InvalidFunctionKeyAggregateDistinctException : ApplicationException {
            public InvalidFunctionKeyAggregateDistinctException(string message) : base(message) { }
            public InvalidFunctionKeyAggregateDistinctException(string message, Exception inner) : base(message, inner) { }
        }
    }
}