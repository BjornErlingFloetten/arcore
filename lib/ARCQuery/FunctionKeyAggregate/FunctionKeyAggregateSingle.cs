﻿// Copyright (c) 2016-2021 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery {
    [Class(Description =
        "Ensures that the given list of properties contains only a single item and returns that item.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyAggregateSingle : FunctionKeyAggregate {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'Single()'\r\n" +
            "Examples:\r\n" +
            "For 'Order', 'SELECT OrderDetails.Address.Single()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyAggregateSingle));

        public override bool TryGetPFromList(IEnumerable<IP> listIP, out IP retval, out string errorResponse) {
            retval = listIP.SingleOrDefault();
            if (retval == null) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "No items found, or multiple items found";
                return false;
            }
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public new static FunctionKeyAggregateSingle Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyAggregateSingleException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyAggregateSingle retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyAggregateSingle retval, out string errorResponse) =>
            TryParseSingleWord(value, "single", () => new FunctionKeyAggregateSingle(), () => SyntaxHelp, out retval, out errorResponse);

        public override string ToString() => "Single()";
        public class InvalidFunctionKeyAggregateSingleException : ApplicationException {
            public InvalidFunctionKeyAggregateSingleException(string message) : base(message) { }
            public InvalidFunctionKeyAggregateSingleException(string message, Exception inner) : base(message, inner) { }
        }
    }
}