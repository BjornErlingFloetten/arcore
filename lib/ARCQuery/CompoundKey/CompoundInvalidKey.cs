﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {
    [Class(Description =
        "Used as a practical solution in order to know that a given key has been attemped parsed, but was not valid.\r\n" +
        "(avoids reparsing again and again)."
    )]
    public class CompoundInvalidKey : CompoundKey {

        public string ErrorResponse { get; private set; }

        public override bool TryGetPInternal(IKIP ikip, IP dataStorage, out IP retval, out string errorResponse) {
            retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            errorResponse = "Irrelevant for " + nameof(CompoundInvalidKey);
            return false;
        }

        public override string ToString() => "CompoundInvalidKey"; /// Call to <see cref="CompoundKey.BaseToString"/> is not necessary.

        public CompoundInvalidKey(string errorResponse) => ErrorResponse = errorResponse;

        public class CompoundInvalidKeyException : ApplicationException {
            public CompoundInvalidKeyException(string message) : base(message) { }
            public CompoundInvalidKeyException(string message, Exception inner) : base(message, inner) { }
        }
    }
}
