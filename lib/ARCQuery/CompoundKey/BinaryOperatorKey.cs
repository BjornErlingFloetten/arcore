﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery {

    [Class(Description =
        "Offers expressions with operators like +, -, * and /.\r\n" +
        "\r\n" +
        "An implementation of -" + nameof(CompoundKey) + "- that operates on two operands which themselves can be -" + nameof(CompoundKey) + "-s.\r\n" +
        "\r\n" +
        "Example 1): For 'Customer', \"SELECT FirstName+' '+LastName\" (Operator plus, +).\r\n" +
        "\r\n" +
        "Example 2): For 'Customer', 'SELECT DateOfBirth().Year-2021 AS Age. (Operator minus, -).\r\n" +
        "\r\n" +
        "Example 3): For 'Orderline', 'SELECT Product.Name, Quantity, Product.UnitPrice, Product.UnitPrice*Quantity AS Sum' (Operator multiply / '*').\r\n"
    )]
    public class BinaryOperatorKey : CompoundKey {

        public IK LeftOperandKey { get; private set; }
        public string Operator { get; private set; }
        public IK RightOperandKey { get; private set; }

        private CompoundKey? _cachedLeftOperandKey = null;
        private CompoundKey? _cachedRightOperandKey = null;

        public BinaryOperatorKey(IK leftOperandKey, string _operator, IK rightOperandKey) {
            LeftOperandKey = leftOperandKey;
            Operator = _operator;
            RightOperandKey = rightOperandKey;
        }

        public override bool TryGetPInternal(IKIP ikip, IP dataStorage, out IP retval, out string errorResponse) {
            if (!ikip.TryGetP(dataStorage, LeftOperandKey, out var leftOperand, ref _cachedLeftOperandKey, out errorResponse)) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"
                errorResponse = "Left operand not found: Details: " + errorResponse;
                return false;
            }
            if (!ikip.TryGetP(dataStorage, RightOperandKey, out var rightOperand, ref _cachedRightOperandKey, out errorResponse)) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"
                errorResponse = "Right operand not found: Details: " + errorResponse;
                return false;
            }
            var leftLong = leftOperand switch {
                PValue<long> l => (long?)l.Value,
                PValue<int> i => (long?)i.Value,
                _ => null
            };
            var rightLong = rightOperand switch {
                PValue<long> l => (long?)l.Value,
                PValue<int> i => (long?)i.Value,
                _ => null
            };
            if (leftLong != null && rightLong != null) {
                retval = Operator switch {
                    "+" => (IP)new PValue<long>(leftLong.Value + rightLong.Value),
                    "-" => (IP)new PValue<long>(leftLong.Value - rightLong.Value),
                    "*" => (IP)new PValue<long>(leftLong.Value * rightLong.Value),
                    "/" => rightLong == 0 ? (IP)new PValue<string>("#DIV/0!") : (IP)new PValue<long>(leftLong.Value / rightLong.Value),
                    _ => null! // We check against null straight below
                };
            } else {
                var leftDouble = leftOperand switch {
                    PValue<double> l => (double?)l.Value,
                    PValue<long> l => (double?)l.Value,
                    PValue<int> i => (double?)i.Value,
                    _ => null
                };
                var rightDouble = rightOperand switch {
                    PValue<double> l => (double?)l.Value,
                    PValue<long> l => (double?)l.Value,
                    PValue<int> i => (double?)i.Value,
                    _ => null
                };
                if (leftDouble != null && rightDouble != null) {
                    retval = Operator switch {
                        "+" => (IP)new PValue<double>(leftDouble.Value + rightDouble.Value),
                        "-" => (IP)new PValue<double>(leftDouble.Value - rightDouble.Value),
                        "*" => (IP)new PValue<double>(leftDouble.Value * rightDouble.Value),
                        "/" => rightDouble == 0 ? (IP)new PValue<string>("#DIV/0!") : (IP)new PValue<double>(leftDouble.Value / rightDouble.Value),
                        _ => null! // We check against null straight below
                    };
                } else {
                    // Only possibility now is string concatenation
                    retval = Operator switch {
                        "+" => new PValue<string>(leftOperand.GetV<string>("") + rightOperand.GetV<string>("")),
                        _ => null! // We check against null straight below
                    };
                }
            }
            if (retval == null) {
                errorResponse = "Invalid operand types for operand (" + leftOperand.GetType().ToStringShort() + Operator + rightOperand.GetType().ToStringShort() + ")";
                return false;
            }
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"
            return true;
        }

        public class InvalidBinaryOperatorKeyException : ApplicationException {
            public InvalidBinaryOperatorKeyException(string message) : base(message) { }
            public InvalidBinaryOperatorKeyException(string message, Exception inner) : base(message, inner) { }
        }

        public string? _toString;
        [ClassMember(Description =
            "The ToString representation is the one relevant when used as key, like in -" + nameof(IK) + "- or when selecting like 'SELECT {fieldName}'."
        )]
        public override string ToString() => _toString ??= (
            LeftOperandKey.ToString() +
            Operator +
            RightOperandKey.ToString() +
            BaseToString()
        );

        //[Enum(
        //    Description =
        //        "TODO: PROBABLY NOT NEEDED, as can instead store operator directly as string.\r\n" +
        //        "\r\n" +
        //        "Textual description of artithmetic and logical operators supported by -" + nameof(BinaryOperatorKey) + "-.\r\n" +
        //        "\r\n" +
        //        "This representation of an operator is able to be passed without restriction in HTTP GET URL's.\r\n" +
        //        "\r\n" +
        //        "Example: Instead of writing 'UnitPrice*Quantity' you can write 'Product.UnitPrice|MULT|Quantity' in a typical query.",
        //    AREnumType = AREnumType.OrdinaryEnum)]
        //public enum BinaryOperator {
        //    __invalid,
        //    [EnumMember(Description = "Minus, -")]
        //    MINUS,
        //    [EnumMember(Description = "Plus, +")]
        //    PLUS,
        //    [EnumMember(Description = "Multiply, *")]
        //    MULT,
        //    [EnumMember(Description = "Divide, /")]
        //    DIV
        //}

    }
}
