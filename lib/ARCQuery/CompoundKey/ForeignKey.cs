﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using ARCCore;
using System.Linq;

/// <summary>
/// Contains the following:
/// <see cref="ARCQuery.ForeignKey"/>
/// <see cref="ARCQuery.ForeignKeyDirection"/>
/// </summary>
namespace ARCQuery
{
    [Class(Description =
        "Can find related values, through compound field names.\r\n" +
        "\r\n" +
        "Like for 'Order', it can find 'Customer.FirstName' (assuming 'Order' contains 'CustomerId').\r\n" +
        "Can relate several steps like for 'OrderLine', 'Order.Customer.FirstName'.\r\n" +
        "See -" + nameof(ForeignKeyDirection.ToOne) + "-.\r\n" +
        "\r\n" +
        "Can also traverse 'the other' way  (-" + nameof(ForeignKeyDirection.ToMany) + "-), " +
        "like for 'Customer', 'Order.OrderLine.Sum.Sum()' (assuming 'Order' contains 'CustomerId' and 'OrderLine' contains 'OrderId').\r\n" +
        "\r\n" +
        "Note that in a series of mixed traversal (that is containing both -" + nameof(ForeignKeyDirection.ToMany) + "- and -" + nameof(ForeignKeyDirection.ToOne) + "-) " +
        "each -" + nameof(ForeignKeyDirection.ToOne) + "- traversal will be limited to distinct primary keys.\r\n" +
        "For instance (from the ARNorthwind example database), " +
        "for query 'Territory/SELECT EmployeeTerritory.Employee.Order.OrderLine.Sum()' only unique 'EmployeeId' will be used before looking up 'Order'.\r\n" +
        "(this does not necessarily make the query above meaningful though).\r\n" +
        "\r\n" +
        "Foreign key can be used in combination with -" + nameof(EntityMethodKey) + "-, -" + nameof(NewKey) + "- and -" + nameof(FunctionKey) + "-.\r\n" +
        "For instance like 'Order/SELECT ShippedDate, Sum, Customer.Name' were 'Customer.Name' is an -" + nameof(EntityMethodKey) + "- " +
        "combining FirstName, LastName and CompanyName).\r\n" +
        "\r\n" +
        "This is an not an expandable concept (class is sealed).\r\n" +
        "In other words, this class contains all the relevant functionality and you are not expected to " +
        "implement anything specific in your -" + nameof(ARConcepts.ApplicationSpecificCode) + "-.\r\n" +
        "\r\n" +
        "Note: In queries like\r\n" +
        "  \"Order/SELECT CustomerId, Created, Amount/WHERE Customer.FirstName = 'John'\"\r\n" +
        "the foreign-key 'CustomerId' should be included as shown in here (see -" + nameof(QueryExpressionSelect) + "-).\r\n" +
        "If you only do\r\n" +
        "  \"Order/SELECT Created, Amount/WHERE Customer.FirstName = 'John'\"\r\n" +
        "then the link to Customer is not available.\r\n" +
        "(For the example given above it would of course have been more natural to just write:\r\n" +
        "  \"Order/WHERE Customer.FirstName = 'John'/SELECT Created, Amount\"\r\n" +
        ")" +
        "\r\n" +
        "See also -" + nameof(MemberEntityKey) + "-.\r\n" +
        "\r\n"
    )]
    public sealed class ForeignKey : CompoundKey
    {

        [ClassMember(Description =
            "The actual -" + nameof(ForeignKeySingleStep) + "- leading to the -" + nameof(ForeignField) + "-.\r\n" +
            "For 'Order' 'Customer.Name' this would be one step (finding 'CustomerId' in 'Order' and looking up the 'Customer').\r\n" +
            "For 'OrderLine' 'Order.Customer.Name' this would be two steps, finding 'OrderId' in 'Order', and then as above.\r\n" +
            "\r\n" +
            "Can contain a mix of -" + nameof(ForeignKeyDirection) + "-s, like for 'Region',\r\n" +
            "Territory.EmployeeTerritory.Employee.Order.OrderDetail.Sum.Sum()\r\n" +
            "(assuming Territory contains RegionId, EmployeeTerritory contains TerritoryId and EmployeeId, Order contains EmployeeId, OrderDetails contains OrderId)."
        )]
        public List<ForeignKeySingleStep> Steps { get; private set; }

        [ClassMember(Description =
            "If TRUE then -" + nameof(TryGetPInternal) + ") will use much simpler code without collections and parallell executions."
        )]
        public bool StepsAreOnlyDirectionToOne { get; private set; }

        [ClassMember(Description = "" +
            "The actual field in the final foreign entity to return value for.\r\n" +
            "May actually also be a -" + nameof(CompoundKey) + "-, that is using -" + nameof(EntityMethodKey) + "-, -" + nameof(NewKey) + "- and -" + nameof(FunctionKey) + "-."
        )]
        public IK ForeignField { get; private set; }

        [ClassMember(Description =
            "Aggregations to be used on the multiple foreign entity values that are found when one of the -" + nameof(Steps) + "- are -" + nameof(ForeignKeyDirection.ToMany) + "-.\r\n" +
            "Not relevant if no -" + nameof(Steps) + "- with -" + nameof(ForeignKeyDirection.ToMany) + "- are given.\r\n" +
            "\r\n" +
            "Example: 'Order.OrderLine.Sum.Sum()' (see -" + nameof(FunctionKeyAggregateSum) + "-).\r\n" +
            "\r\n" +
            "If multiple keys are used then the all keys minus the last one must return a PValue<List<IP>> value.\r\n" +
            "For instance like for 'Customer' 'Order.OrderLine.Product.Name.Distinct().Join()' Distinct() will return a PValue<List<IP>> value for consumption by Join().\r\n" +
            "See -" + nameof(FunctionKeyAggregateDistinct) + "- and -" + nameof(FunctionKeyAggregateJoin) + "-.\r\n"
        )]
        public List<FunctionKeyAggregate> AggregateKeys { get; private set; }

        [ClassMember(Description =
            "Cached version of -" + nameof(ForeignField) + "-.\r\n" +
            "Note how it is cached between calls to -" + nameof(TryGetPInternal) + "-.\r\n"
        )]
        private CompoundKey? _cachedForeignFieldCompoundKey = null;

        /// <summary>
        /// As of Feb 2021 <paramref name="errorResponse"/> is not used when not <see cref="StepsAreOnlyDirectionToOne"/>
        /// </summary>
        /// <param name="ikip"></param>
        /// <param name="dataStorage"></param>
        /// <param name="retval"></param>
        /// <param name="errorResponse"></param>
        /// <returns></returns>
        public override bool TryGetPInternal(IKIP ikip, IP dataStorage, out IP retval, out string errorResponse)
        {
            if (StepsAreOnlyDirectionToOne)
            {
                // Use vastly simplified code, avoid collections and parallell execution, and also, return meaningful error response.

                var currentEntity = ikip.P;
                for (var i = 0; i < Steps.Count; i++)
                {
                    var step = Steps[i];
                    if (!currentEntity.TryGetPV<string>(step.Relation.Key, out var foreignKeyValue))
                    {
                        errorResponse =
                            currentEntity.GetType().ToStringVeryShort() + "/" + ikip.Key + "/" + step.Relation.Key + " not found.\r\n" +
                            "(for finding " + step.Relation.EntityType + ".)\r\n" +
                            "Details: Step " + i + " (" + step.ToString() + ")";
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        return false;
                    }

                    if (!Steps[i].ForeignEntitiesCollection.TryGetP<IP>(foreignKeyValue, out var foreignEntity))
                    {
                        // This is really a referential integrity problem and qualifies for an exception being thrown
                        /// TODO: Consider passing a <see cref="Query"/>-parameter here, and terminate query now.
                        errorResponse =
                            currentEntity.GetType().ToStringVeryShort() + "/" + ikip.Key + "/" + step.Relation.Key + " = " + foreignKeyValue + " " +
                            "is invalid (for finding " + step.Relation.EntityType + ").\r\n" +
                            "Details: Step " + i + " (" + step.ToString() + ")";
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        return false;
                    }
                    ikip = new IKIP(foreignKeyValue, foreignEntity);
                    currentEntity = foreignEntity;
                }
                /// Note that although ikip.TryGetP is only called once now, 
                /// caching of foreign field (<see cref="_cachedForeignFieldCompoundKey"/>) is still relevant, because 
                /// this method (TryGetPInternal) will usually be called multiple times for this instance.
                return ikip.TryGetP(dataStorage, ForeignField, out retval, ref _cachedForeignFieldCompoundKey, out errorResponse);
            }

            /// We must plan for <see cref="ForeignKeyDirection.ToMany"/>, that is, multiple entities.
            var currentEntities = (IEnumerable<IKIP>)new List<IKIP> { ikip };
            /// NOTE: An example of mixed directions (mixed <see cref="ForeignKeyDirection"/>) would be:
            /// NOTE: Region/SELECT Description, Territory.EmployeeTerritory.Employee.Order.OrderDetail.Sum.Sum()

            for (var i = 0; i < Steps.Count; i++)
            {
                var step = Steps[i]; // Avoid one level of redirection, especially in parallell code
                currentEntities = (step.Direction) switch
                {

                    ForeignKeyDirection.ToOne => currentEntities.AsParallel().Select(ikip =>
                    {
                        // Each entity in list will lead to maximum one new entity

                        var currentEntity = ikip.P;
                        if (!currentEntity.TryGetPV<string>(step.Relation.Key, out var foreignKeyValue))
                        {
                            return null;
                        }

                        if (!step.ForeignEntitiesCollection.TryGetP<IP>(foreignKeyValue, out var foreignEntity))
                        {
                            // This is really a referential integrity problem and qualifies for an exception being thrown
                            /// TODO: Consider passing a <see cref="Query"/>-parameter here, and terminate query now.
                            // errorResponse = "ERROR: Invalid foreign key value (" + foreignKeyValue + "), does not exist (for storage of " + ForeignTypes[i] + ")";
                            return null;
                        }
                        return new IKIP(foreignKeyValue, foreignEntity);
                    }).
                        Where(ikip => ikip != null).Select(ikip => ikip!). // Remove nulls
                        ToList(), // Avoid deferred execution

                    ForeignKeyDirection.ToMany => currentEntities.AsParallel().SelectMany(ikip =>
                            // Each entity in list may lead to multiple new entities

                            /// NOTE: It is critical for performance that indexing is done now. 
                            /// NOTE: <see cref="IGetKeysEqualToValue"/> / <see cref="ITrySetPP"/> / <see cref="ARConcepts.Indexing"/>
                            step.ForeignEntitiesCollection.GetKeysEqualToValue(step.Relation.Key, ikip.Key.ToString())
                        ).
                        ToList(), // Avoid deferred execution
                    _ => throw new InvalidEnumException(step.Direction)
                };

                if (step.Direction == ForeignKeyDirection.ToOne && i > 0 && Steps[i - 1].Direction == ForeignKeyDirection.ToMany)
                {
                    // We just crossed a many-to-many relationship.
                    // Limit to Distinct values
                    // Like for query 'Territory/SELECT EmployeeTerritory.Employee.Order.OrderLine.Sum()' 
                    // ensure only unique 'EmployeeId' will be used before looking up 'Order'.\r\n" +
                    currentEntities = currentEntities.Distinct(new IKIPEqualityComparer()).ToList();
                }
            }

            /// The ikip values in currentEntities should now point to the final entities in which we are to look for the foreign field.

            // Extract all values of the foreign field.

            var list = currentEntities.
                AsParallel().
                Select(ikip =>
                    /// Note that we use <see cref="ARCQuery.Extensions.TryGetP"/> instead of <see cref="CompoundKey.TryGetP"/>
                    /// in order to be able to use <see cref="CompoundKey"/> also for the foreign field.
                    ikip.TryGetP(dataStorage, ForeignField, out var r, ref _cachedForeignFieldCompoundKey, out _) ? r : null
                ).
                Where(ip => ip != null).Select(ip => ip!).
                ToList(); // Avoid deferred execution

            if (AggregateKeys.Count == 0) throw new InvalidForeignKeyException("No aggregate keys, but involves " + nameof(ForeignKeyDirection.ToMany) + ". Should have been caught in constructor");

            retval = null!; // Looks necessary in order to avoid compilator warning below

            for (var i = 0; i < AggregateKeys.Count; i++)
            {
                if (!AggregateKeys[i].TryGetPFromList(list, out retval, out errorResponse))
                {
                    errorResponse = errorResponse +
                        "\r\nDetails: " + AggregateKeys[i].GetType().ToStringShort() + ", index " + i + " " +
                        "in list of " + string.Join(", ", AggregateKeys.Select(k => k.ToString())) + ".\r\n";
                    return false;
                }
                if (i < AggregateKeys.Count - 1)
                {
                    // Ensure that retval 'PValue<List<IP>>' was returned (or actually that we can extract a property value of type 'List<IP>' from it).
                    if (!retval.TryGetV<List<IP>>(out list))
                    {
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        errorResponse =
                            AggregateKeys[i].GetType().ToStringShort() + " is not last in list of " + string.Join(", ", AggregateKeys.Select(k => k.ToString())) + ".\r\n" +
                            "It should therefore return PValue<List<IP>> but it does not (it returns " + retval.GetType().ToStringShort() + ").\r\n";
                        return false;
                    }
                }
            }

            /// Note that in case the final aggregate was a key like <see cref="FunctionKeyAggregateDistinct"/> which returns
            /// PValue{List{IP}} instead of a "single" value, the result now may be a little unexpected, but not necessarily wrong.

            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>

            if (retval == null) throw new InvalidForeignKeyException("retval == null for " + ToString());
            return true;
        }

        [Class(Description = "Used by -" + nameof(ForeignKey.TryGetPInternal) + "- when it needs distinct list of foreign entities, based on their keys")]
        private class IKIPEqualityComparer : IEqualityComparer<IKIP>
        {
            public bool Equals(IKIP x, IKIP y)
            {
                var retval = x.Key.Equals(y.Key);
                if (object.ReferenceEquals(x.P, y.P) != retval) throw new InvalidForeignKeyException(
                    "Invalid use of " + GetType().ToStringShort() + ".\r\n" +
                    "\r\n" +
                    "This comparer assumes that when Key-parts are equal then P-parts also refer to the same object (object.ReferenceEquals = TRUE), " +
                    "and vice versa, that when Key-parts are not equal, that P-parts do not refer to the same object (object (object.ReferenceEquals = FALSE).\r\n" +
                    "\r\n" +
                    "In this case Key-parts are " + x.Key + " and " + y.Key + " (that is " + (x.Key.Equals(y.Key) ? "EQUAL" : "NOT EQUAL") + ") " +
                    "and object.ReferenceEquals returned " + object.ReferenceEquals(x.P, y.P) + ".\r\n" +
                    "\r\n" +
                    "Possible resolution: Recheck assumption about use of this comparer in the C# code."
                );
                return retval;
            }
            public int GetHashCode(IKIP ikip) => ikip.Key.GetHashCode();
        }

        public ForeignKey(IP dataStorage, List<ForeignKeySingleStep> steps, List<FunctionKeyAggregate> aggregateKeys, IK foreignField)
        {
            if (steps.Count == 0) throw new InvalidForeignKeyException(nameof(steps) + " is empty (Count == 0)");
            Steps = steps;
            // var currentFromType = initialType;
            //foreach (var t in toForeignTypes)
            //{
            //    var direction = (IPRelationsKeysPointingFrom.TryGetPV<List<string>>(t, out var l) && l.Contains(currentFromType)) ?
            //        ForeignKeyDirection.ToMany :
            //        ForeignKeyDirection.ToOne; // Note that also possible with weakly typed schema.

            //    Steps.Add(new ForeignKeySingleStep(
            //        direction,
            //        foreignType: t,
            //        actualForeignKey: direction switch
            //        {
            //            ForeignKeyDirection.ToOne => IKString.FromCache(t + "Id"),
            //            ForeignKeyDirection.ToMany => IKString.FromCache(currentFromType + "Id"),
            //            _ => throw new InvalidEnumException(direction)
            //        },
            //        dataStorage.TryGetP(t, out var storage) ?
            //            storage :
            //            throw new InvalidForeignKeyException(
            //                "No key found in data storage for foreign type '" + t + "'.\r\n" +
            //                "Some of the keys found where: " + string.Join(", ", dataStorage.Keys.Take(20).Select(ik => ik.ToString())) + "\r\n")
            //    ));
            //    currentFromType = t;
            //}
            StepsAreOnlyDirectionToOne = Steps.All(s => s.Direction == ForeignKeyDirection.ToOne);
            AggregateKeys = aggregateKeys;
            ForeignField = foreignField;

            if (StepsAreOnlyDirectionToOne && AggregateKeys.Count > 0) throw new InvalidForeignKeyException(
                "Aggregate keys given (" + string.Join(".", AggregateKeys) + "), " +
                "but does not involve -" + nameof(ForeignKeyDirection) + "-.-" + nameof(ForeignKeyDirection.ToMany) + "-.\r\n" +
                "Resolution: Remove aggregate keys from query."
            );

            if (!StepsAreOnlyDirectionToOne && AggregateKeys.Count == 0) throw new InvalidForeignKeyException(
                "No aggregate keys given, but involves -" + nameof(ForeignKeyDirection) + "-.-" + nameof(ForeignKeyDirection.ToMany) + "-.\r\n" +
                "Resolution: Add aggregate key like Sum() or Join()."
            );
        }

        public static bool TryParse(IP dataStorage, IEnumerable<string> tOriginal, Type initialType, List<FunctionKey> followingFunctionKeys, QuantileKey? quantileKey, out ForeignKey retval, out string errorResponse)
        {

            var t = tOriginal.ToList();

            // Pick aggregate keys from end of list
            var aggregateKeys = new List<FunctionKeyAggregate>();
            while (t.Count > 1 && FunctionKey.TryParse(t[^1], out var fk) && fk is FunctionKeyAggregate ak)
            {
                aggregateKeys.Add(ak);
                t.RemoveAt(t.Count - 1);
            }
            aggregateKeys.Reverse();

            if (t.Count < 2)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Foreign type and foreign field not found in " + string.Join(".", t) + ". Found aggregate keys " + string.Join(".", aggregateKeys.ToString());
                return false;
            }

            // Pick foreign types from start of list
            var steps = new List<ForeignKeySingleStep>();

            //while (t.Count > 1 && dataStorage.ContainsKey(t[0]))
            //{
            //    foreignTypes.Add(t[0]);
            //    t.RemoveAt(0);
            //}

            //if (foreignTypes.Count == 0)
            //{
            //    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            //    errorResponse =
            //        "No foreign types found. Must exist at root-level in data storage. Possible candidates as foreign types where: " +
            //        string.Join(", ", t.Select(s => "'" + s + "'"));
            //    return false;
            //}

            // var strInitialType = initialType.ToStringVeryShort();
            var currentFromType = initialType;
            var toManyFound = false;
            while (t.Count > 1)
            {
                if (IPRelationsKeysPointingTo.TryGetPV<List<EntityTypeAndKey>>(IKType.FromType(currentFromType), out var relations))
                {
                    var r = relations.FirstOrDefault(r => r.OppositeTerm.Equals(t[0])); // Note: Always index 0 because we remove items from lista as we progress
                    if (!(r is null))
                    {

                        toManyFound = true;
                        if (aggregateKeys.Count == 0)
                        {
                            // Note: This is same check as in constructor, except that here we can still return a friendly
                            // errorResponse instead of throwing an exception like the constructor does.
                            // There is duplicate work in the code though.
                            retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            errorResponse =
                                "No " + nameof(FunctionKeyAggregate) + " found.\r\n" +
                                "The traversal from " + currentFromType + " to " + r.OppositeTerm + " (" + r.EntityType.ToStringVeryShort() + ") " +
                                "is a -" + nameof(ForeignKeyDirection) + "-.-" + nameof(ForeignKeyDirection.ToMany) + "-, " +
                                "that is, based on '" + r.EntityType.ToStringVeryShort() + "' containing a foreign key '" + r.Key + "'.\r\n" +
                                "Since this may result in multiple results, there must be a -" + nameof(FunctionKeyAggregate) + "- at the end, " +
                                "such as -Sum-() or -Join-(), " +
                                "in order to reduce this down to one scalar result, but no such key was found.\r\n" +
                                "You may use -" + nameof(FunctionKeyAggregateSingle) + "- (-Single-() " +
                                "if you are sure that only a single result will occur.";
                            return false;
                        }

                        if (!dataStorage.TryGetP<IP>(IKType.FromType(r.EntityType), out var foreignEntityCollection))
                        {
                            retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            errorResponse =
                                "No key found in data storage for foreign type '" + r.EntityType + "'.\r\n" +
                                "Some of the keys found where: " +
                                    string.Join(", ", dataStorage.Keys.Take(20).Select(ik => ik.ToString())) + "\r\n";
                            return false;
                        }
                        steps.Add(new ForeignKeySingleStep(
                            ForeignKeyDirection.ToMany,
                            relation: r,
                            foreignEntityCollection
                        ));
                        currentFromType = r.EntityType;
                        t.RemoveAt(0);
                        continue;
                    }
                }

                if (IPRelationsKeysPointingFrom.TryGetPV<List<EntityTypeAndKey>>(IKType.FromType(currentFromType), out relations))
                {
                    var r = relations.FirstOrDefault(r => r.KeyWithoutId.Equals(t[0])); // Note: Always index 0 because we remove items from lista as we progress
                    if (!(r is null))
                    {
                        if (!dataStorage.TryGetP<IP>(IKType.FromType(r.EntityType), out var foreignEntityCollection))
                        {
                            retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            errorResponse =
                                "No key found in data storage for foreign type '" + r.EntityType + "'.\r\n" +
                                "Some of the keys found where: " +
                                    string.Join(", ", dataStorage.Keys.Take(20).Select(ik => ik.ToString())) + "\r\n";
                            return false;
                        }
                        steps.Add(new ForeignKeySingleStep(
                            ForeignKeyDirection.ToOne,
                            relation: r,
                            foreignEntityCollection
                        ));
                        currentFromType = r.EntityType;
                        t.RemoveAt(0);
                        continue;
                    }
                }

                /// TODO: NOTE: Experimental (added 25 Feb 2022): For situations where we create a field first (with SELECT),
                /// TODO: in order to link a foreign entity here with the next SELECT, we have a problem with that the 'initialType'
                /// TODO: input parameter to this method is usually PRich, instead of what we could assume to be 
                /// TODO: <see cref="QueryProgressP.StrCurrentType"/>
                /// TODO: Example would be: SELECT Something AS OrderId/SELECT Order.Amount ...
                /// TODO: In this case Order will not be understood above because initial type will probably be PRich
                /// TODO: But we can assume "Order, OrderId" here as <see cref="EntityTypeAndKey"/>
                /// TODO: "NOTE: Key in EntityTypeAndKey was relaxed from PK to IK 25 Feb 2022 in order for experiment in -" + nameof(ForeignKey) +"- to work.\r\n" +
                /// TODO: "NOTE: It may be changed back to PK if we give up on that experiement.\r\n"
                if (
                    steps.Count == 0 && 
                    IP.AllIPDerivedTypesDict.TryGetValue(t[0], out var foreignType)
                )
                {
                    var r = new EntityTypeAndKey(foreignType, IKString.FromString(t[0] + "Id"));
                    if (!dataStorage.TryGetP<IP>(IKType.FromType(r.EntityType), out var foreignEntityCollection))
                    {
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        errorResponse =
                            "No key found in data storage for foreign type '" + r.EntityType + "'.\r\n" +
                            "Some of the keys found where: " +
                                string.Join(", ", dataStorage.Keys.Take(20).Select(ik => ik.ToString())) + "\r\n";
                        return false;
                    }
                    steps.Add(new ForeignKeySingleStep(
                        ForeignKeyDirection.ToOne,
                        relation: r,
                        foreignEntityCollection
                    ));
                    currentFromType = r.EntityType;
                    t.RemoveAt(0);
                    continue;
                }

                /// What is left is the actual foreign field.
                break;
            }

            if (steps.Count == 0)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = 
                    "Potential foreign key traversals '" + string.Join(".", t) + "' was not understood, " +
                    "starting from " + nameof(initialType) + " '" + initialType.ToStringVeryShort() + "'.";
                return false;
            }

            if (!toManyFound && aggregateKeys.Count > 0)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse =
                    "Irrelevant " + nameof(FunctionKeyAggregate) + "s found.\r\n" +
                    "The traversals " + initialType.ToStringVeryShort() + "." + string.Join(".", tOriginal) + " " +
                    "does not involve a -" + nameof(ForeignKeyDirection) + "-.-" + nameof(ForeignKeyDirection.ToMany) + "-, " +
                    "Since this will never result in multiple results, there can not be a -" + nameof(FunctionKeyAggregate) + "- at the end, " +
                    "but the following was found: " +
                    string.Join(".", aggregateKeys) + ".\r\n";
                return false;
            }

            if (t.Count == 0)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Foreign field not found, found traversals " + string.Join("\r\n", steps.Select(s => s.ToString()));
                return false;
            }

            /// What is left in 't' now is the foreign field.
            /// It may be a <see cref="CompoundKey"/> also.
            /// We will have to wait with parsing it until we find the actual foreign entity.

            retval = new ForeignKey(
                dataStorage,
                steps,
                aggregateKeys,
                foreignField:
                    /// Try to use <see cref="PK"/> as key of possible.
                    /// This is only possible if not a compound key.
                    /// TODO: Consider whether this check is useful or not
                    (
                      t.Count == 1 && // One element in t means foreign field has only one component.
                      IP.AllIPDerivedTypesDict.ContainsKey(steps[^1].Relation.EntityType.ToStringVeryShort()) &&
                      PK.TryGetFromTypeAndFieldName(strEntityType: steps[^1].Relation.EntityType.ToStringVeryShort(), field: t[0], out var pk)
                    ) ?
                        (IK)pk :
                        IKString.FromString(string.Join(".", t)) // Use original found value (either with multiple items, or just not known PK)
            )
            {
                FunctionKeys = followingFunctionKeys,
                QuantileKey = quantileKey
            };
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public string? _toString;
        public override string ToString() => _toString ??=
            string.Join(".", Steps.Select(s => s.Direction == ForeignKeyDirection.ToOne ? s.Relation.KeyWithoutId : s.Relation.OppositeTerm)) + "." +
            ForeignField +
            (AggregateKeys.Count() == 0 ? "" :
                ("." + string.Join(".", AggregateKeys.Select(k => k.ToString())))
            ) +
            BaseToString();

        [ClassMember(
            Description =
                "Contains, for each (entity type) key, a list of entity types with foreign keys pointing to that entity type.\r\n" +
                "\r\n" +
                "Shows how all the -" + nameof(IP) + "- classes relate to each other.\r\n" +
                "\r\n" +
                // TOOD: Delete commented out code
                //"TODO: Expand to take into account use of -" + nameof(PKRelAttribute) + "- (-" + nameof(PKRelAttributeP.ForeignEntity) + "-).\r\n" +
                //"TODO: Instead of List<string>, use List<(string foreignType, string foreignKey)> or similar.\r\n" +
                //"\r\n" +
                "Used to automatically create links to related objects in a dynamic context.\r\n" +
                "(see for instance -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "- and -" + nameof(QueryExpressionRel) + "-.)\r\n" +
                "\r\n" +
                "Depends on -" + nameof(IP.AllIPDerivedTypes) + "- (the static defined schema), " +
                "that is does not attempt to dynamically discover any structures.\r\n" +
                "\r\n" +
                "Key is the very short string representations of the type " +
                "(as generated from -" + nameof(ARCCore.Extensions.ToStringVeryShort) + "-)\r\n" +
                "Value is List<-" + nameof(EntityTypeAndKey) + "-> with corresponding types that refer to that type.\r\n" +
                "\r\n" +
                "See also -" + nameof(EntityTypeAndKey) + "-, -" + nameof(IPRelationsKeysPointingFrom) + "- and -" + nameof(ForeignKeyDirection) + "-.",
            /// Note: Keep Description short as it shows up as a heading in API queries for <see cref="ARConcepts.ExposingApplicationState"/>
            LongDescription =
                "Example:\r\n" +
                "\r\n" +
                "If we have classes Customer, Order and SupportTicket and property keys OrderP.CustomerId and SupportTicketP.CustomerId then the " +
                "entry for the key 'Customer' would be like " +
                "  new List<EntityTypeAndKey> { \"Order CustomerId\", 'SupportTicket CustomerId' }.\r\n" +
                "\r\n" +
                "This may be used to dynamically and automatically create links to related objects. For instance for context " +
                "  'Customer/42' " +
                "it will (with the help of this information) be possible to create corresponding links " +
                "  'Order/WHERE CustomerId = 42' and " +
                "  'SupportTicket/WHERE CustomerId = 42'.\r\n" +
                "\r\n" +
                "Relations defined with -" + nameof(PKRelAttribute) + "- (-" + nameof(PKRelAttributeP.ForeignEntity) + "-) " +
                "is also taken into account.\r\n" +
                "For instance for Employee there might be a key EmployeeP.SupervisorId pointing to another Employee.\r\n" +
                "TODO: Document use of -" + nameof(EntityTypeAndKey.OppositeTerm) + "- in such cases.\r\n" +
                "\r\n" +
                "Note that as of Jun 2020 this concept relies on predefined property keys (-" + nameof(AREnumType.PropertyKeyEnum) + "-).\r\n" +
                "There is however in principle nothing wrong with dynamically finding the same information, " +
                "as long as 'standardized' names are used for foreign keys, like 'Order/1968/CustomerId = Customer/42' for instance.\r\n" +
                "TODO: Consider implementation of such a dynamic discovery."
        )]
        public static IP IPRelationsKeysPointingTo = new Func<PRich>(() =>
        {
            // NOTE: This code is not very performant. It is O(n^3) and there is no particular attempts at optimization
            // NOTE: On the other hand it should not matter as it is run only once
            // NOTE: at application startup (at type loading), and the number of types should be limited.
            var retval = new PRich();
            retval.IP.AddPV(
                ARCDoc.DocumentatorP._Description,
                /// Note trick here of getting text above into _Description
                ClassMemberAttribute.GetAttribute(
                    typeof(ForeignKey),
                    nameof(IPRelationsKeysPointingTo)
                ).IP.GetPV<string>(BaseAttributeP.Description)
            );
            IP.AllIPDerivedTypes.ForEach(classType =>
            {
                var thisClassRetval = new List<EntityTypeAndKey>();

                var classTypeWithIdAppended = classType.ToStringVeryShort() + "Id";

                /// Go through all other IP derived types, and add
                /// 
                /// 1) All keys with <see cref="PKRelAttributeP.ForeignEntity"/> set to this entity (manual mapping)
                /// 
                /// 2) All keys without <see cref="PKRelAttributeP"/> where name of key equals classTypeWithIdAppended (automatic mapping)

                IP.AllIPDerivedTypes.ForEach(candidateType =>
                {
                    var candidateTypeWithPAppended = candidateType.ToStringVeryShort() + "P";
                    PK.GetAllPKForEntityType(candidateType).ForEach(pk =>
                    {
                        if (pk.TryGetA<PKRelAttribute>(out var pkrel))
                        {
                            if (pkrel.IP.TryGetPV<Type>(PKRelAttributeP.ForeignEntity, out var t))
                            {
                                if (t.Equals(classType))
                                {
                                    // Manual mapping, foreign entity is set explicit for this key
                                    thisClassRetval.Add(new EntityTypeAndKey(
                                        entityType: candidateType,
                                        key: pk,
                                        oppositeTerm: pkrel.IP.TryGetPV<string>(PKRelAttributeP.OppositeTerm, out var temp) ? temp : null
                                    ));
                                }
                            }
                            // Note that check below (automatic matching) only applies when PKRelAttribute 
                            // is NOT defined for this class.
                        }
                        else
                        {
                            if (pk.ToString().Equals(classTypeWithIdAppended))
                            {
                                // Automatic mapping, because key name matches
                                thisClassRetval.Add(new EntityTypeAndKey(
                                    entityType: candidateType,
                                    key: pk,
                                    oppositeTerm: null // Not known, will be set to entityType
                                ));
                            }
                        }
                    });
                    //var correspondingEnumType = PK.AllPKEnums.FirstOrDefault(t => (t.ToStringVeryShort()).Equals(candidateTypeWithPAppended));
                    //if (correspondingEnumType != null)
                    //{

                    //    UtilCore.EnumGetMembers(correspondingEnumType).ForEach(.Any(e => classTypeWithIdAppended.Equals(e.ToString())))
                    //    {
                    //        thisClassRetval.Add(candidateType.ToStringVeryShort());
                    //    }
                    //}
                });

                if (thisClassRetval.Count > 0)
                {
                    retval.IP[classType.ToStringVeryShort()] = new PValue<List<EntityTypeAndKey>>(thisClassRetval.
                        OrderBy(s => s.ToString()).
                        ToList()
                    );
                }
            });
            return retval;
        })();

        [ClassMember(
            Description =
                "Contains, for each (entity type) key, a list of entity types which that entity type points to.\r\n" +
                "\r\n" +
                "Shows how all the -" + nameof(IP) + "- classes relate to each other.\r\n" +
                "\r\n" +
                "Key is the very short string representations of the type " +
                "(as generated from -" + nameof(ARCCore.Extensions.ToStringVeryShort) + "-)\r\n" +
                "Value is List<-" + nameof(EntityTypeAndKey) + "-> with corresponding types that refer to that type.\r\n" +
                "\r\n" +
                "Depends on -" + nameof(IP.AllIPDerivedTypes) + "- (the static defined schema), " +
                "that is, it does not attempt to dynamically discover any structures.\r\n" +
                "\r\n" +
                "See also -" + nameof(IPRelationsKeysPointingTo) + "- and -" + nameof(ForeignKeyDirection) + "-.",
            /// Note: Keep Description short as it shows up as a heading in API queries for <see cref="ARConcepts.ExposingApplicationState"/>
            LongDescription =
                "Example:\r\n" +
                "\r\n" +
                "If we have a class called OrderLine with property keys OrderLineP.OrderId and OrderLineP.ProductId then the " +
                "entry for the key 'OrderLine' would be like " +
                "  new List<EntityTypeAndKey> { \"Order OrderId\", 'Product ProductId' }.\r\n" +
                "\r\n" +
                "Relations defined with -" + nameof(PKRelAttribute) + "- (-" + nameof(PKRelAttributeP.ForeignEntity) + "-) " +
                "is also taken into account.\r\n" +
                "For instance for Employee there might be a key EmployeeP.SupervisorId pointing to another Employee.\r\n" +
                "TODO: Document use of -" + nameof(EntityTypeAndKey.OppositeTerm) + "- in such cases.\r\n"
        )]
        public static IP IPRelationsKeysPointingFrom = new Func<PRich>(() =>
        {
            var retval = new PRich();
            retval.IP.AddPV(
                ARCDoc.DocumentatorP._Description,
                /// Note trick here of getting text above into _Description
                ClassMemberAttribute.GetAttribute(typeof(ForeignKey), nameof(IPRelationsKeysPointingFrom)).
                IP.GetPV<string>(BaseAttributeP.Description)
            );
            var dict = new Dictionary<Type, List<EntityTypeAndKey>>();
            IPRelationsKeysPointingTo.ForEach(ikip =>
            {
                if (nameof(ARCDoc.DocumentatorP._Description).Equals(ikip.Key.ToString()))
                {
                    return;
                }
                var list = ikip.P.GetV<List<EntityTypeAndKey>>();
                list.ForEach(t =>
                {
                    if (!dict.TryGetValue(t.EntityType, out var pointingFrom))
                    {
                        pointingFrom = dict[t.EntityType] = new List<EntityTypeAndKey>();
                    }
                    pointingFrom.Add(new EntityTypeAndKey(
                        entityType: IP.AllIPDerivedTypesDict.GetValue(ikip.Key.ToString()),
                        t.Key,
                        t.OppositeTerm
                    ));
                });
            });
            dict.ForEach(e =>
            {
                retval.IP.AddPV(e.Key.ToStringVeryShort(), e.Value);
            });
            return retval;
        })();


        private static readonly ConcurrentDictionary<string, List<IKString>> _allForeignKeysForEntityTypeCache =
            new ConcurrentDictionary<string, List<IKString>>();
        /// <summary>
        /// </summary>
        /// <param name="entityType">
        /// Should be the very short string representations of the type (as generated from <see cref="ARCCore.Extensions.ToStringVeryShort"/>
        /// </param>
        /// <returns></returns>
        [ClassMember(Description =
            "Returns all possible foreign keys relevant for the given entity type, " +
            "like for Order: 'Customer.FirstName' and 'Customer.LastName'.\r\n" +
            "\r\n" +
            "Does also understand -" + nameof(EntityMethodKey) + "- (like Customer.Name based on FirstName + LastName).\r\n" +
            "\r\n" +
            "Useful in error message or when giving suggestions to users about possible keys to use.\r\n" +
            "\r\n" +
            "Will only traverse one step in the relational structure, that is, for OrderLine, " +
            "will only traverse to Order, like 'Order.OrderDate', it will not traverse multiple steps, like to Customer through Order " +
            "like 'Order.Customer.Firstname'.\r\n" +
            "\r\n" +
            "Depends on -" + nameof(IP.AllIPDerivedTypes) + "- and - " + nameof(PK.GetAllPKForEntityTypeString) + "- (the static defined schema), " +
            "that is, it does not attempt to dynamically discover any structures.\r\n" +
            "\r\n" +
            "The string representation of the entity type (the entity type parameter) " +
            "should be the very short string representations of the type " +
            "(as generated from -" + nameof(ARCCore.Extensions.ToStringVeryShort) + "-)." +
            "\r\n" +
            "Returns an empty collection if no foreign keys found.\r\n" +
            "\r\n" +
            "Note how the return value is of type -" + nameof(IKString) + "- and not -" + nameof(ForeignKey) + "-, " +
            "because the latter requires more information than given here in order to initialize."
        )]
        public static List<IKString> AllForeignKeysForEntityType(string entityType) =>
            _allForeignKeysForEntityTypeCache.GetOrAdd(entityType, strEntityType =>
        {
            var retval = new List<IKString>();

            if (!IPRelationsKeysPointingFrom.TryGetPV<List<EntityTypeAndKey>>(strEntityType, out var relations))
            {
                return retval;
            }
            relations.ForEach(r =>
            {
                /// NOTE: Similar code in <see cref="ForeignKey.AllForeignKeysForEntityType" and 
                /// NOTE: <see cref="QueryExpression.TryCheckForExistenceOfKey"/>

                retval.AddRange(PK.GetAllPKForEntityType(r.EntityType).
                    Select(pk => IKString.FromCache(r.KeyWithoutId + "." + pk))
                );
                retval.AddRange(EntityMethodKey.AllEntityMethodKeysForEntityTypeList(r.EntityType).
                    Select(k => IKString.FromCache(r.KeyWithoutId + "." + k))
                );
            });
            return retval;
        });

        [Class(Description =
            "Describes an entity type and a key.\r\n" +
            "\r\n" +
            "Used by -" + nameof(IPRelationsKeysPointingTo) + "- and -" + nameof(IPRelationsKeysPointingFrom) + "-.\r\n" +
            "\r\n"
        // TODO: Delete commented out code (IT IS NOT MUCH SIMILAR)
        //"Note: If it ever becomes desired to let this class implement ITypeDescriber then the code in the almost identical class EntityTypeAndKey " +
        //"in -" + nameof(ARComponents.ARCEvent) + "- can be copied almost verbatim.\r\n"
        )]
        public class EntityTypeAndKey
        {
            [ClassMember(Description =
                "For -" + nameof(IPRelationsKeysPointingTo) + "-, corresponds to foreign entity type.\r\n" +
                "\r\n" +
                "For -" + nameof(IPRelationsKeysPointingFrom) + "-, " +
                "corresponds to -" + nameof(PKRelAttributeP) + "-.- " + nameof(PKRelAttributeP.ForeignEntity) + "-.\r\n" +
                "\r\n" +
                "TODO: Consider creating in the constructor a EntityTypeAsString or similar, " +
                "TODO: since there are some ToStringVeryShort() operations\r\n" +
                "TODO: being done on returned value of this property"
            )]
            public Type EntityType { get; private set; }

            [ClassMember(Description =
                "NOTE: Key in EntityTypeAndKey was relaxed from PK to IK 25 Feb 2022 in order for experiment in -" + nameof(ForeignKey) +"- to work.\r\n" +
                "NOTE: It may be changed back to PK if we give up on that experiement.\r\n"
            )]
            public IK Key { get; private set; }

            public string KeyWithoutId { get; private set; }

            [ClassMember(Description =
                "Corresponds to -" + nameof(PKRelAttributeP) + "-.- " + nameof(PKRelAttributeP.OppositeTerm) + "-.\r\n" +
                "\r\n" +
                "If not given then will be set to string representation of -" + nameof(EntityType) + "-.\r\n"
            )]
            public string OppositeTerm;

            public EntityTypeAndKey(Type entityType, IK key, string? oppositeTerm = null)
            {
                EntityType = entityType;
                Key = key;
                if (!Key.ToString().EndsWith("Id"))
                {
                    KeyWithoutId = Key.ToString();
                }
                else
                {
                    KeyWithoutId = Key.ToString()[..^2];
                }
                OppositeTerm = oppositeTerm ?? entityType.ToStringVeryShort();
            }

            public override string ToString() => EntityType.ToStringVeryShort() + " " + Key.ToString() + " " + OppositeTerm;
        }

        // Maybe useful, see 
        //public static class EntityTypeAndKeyExtensions
        //{

        //    public bool TryGetRelation(this List<ForeignKey.EntityTypeAndKey> relations, 
        //        string strType, out EntityTypeAndKey relation, Func<EntityMethodKey, bool> comparer, out int position, out string errorResponse
        //    )
        //    {

        //    }
        //}

        [Class(Description =
            "Describes how to traverse between related entities.\r\n" +
            "\r\n" +
            "For traversing from 'Order' to 'Customer.Name' this would look like " +
            "Direction = ToOne, " +
            "ForeignType = Customer, " +
            "ActualForeignKey = CustomerId and " +
            "ForeignEntities = Customer collection.\r\n" +
            "\r\n" +
            "TODO: Create example the other way around too.\r\n"
        )]
        public class ForeignKeySingleStep
        {

            public ForeignKeyDirection Direction { get; private set; }

            public EntityTypeAndKey Relation { get; private set; }

            //[ClassMember(Description =
            //    "The foreign type.\r\n"
            //)]
            //public string ForeignType { get; private set; }

            //[ClassMember(Description =
            //     "The actual foreign key like OrderId or CustomerId to use for this step\r\n"
            // )]
            //public IKString ActualForeignKey { get; private set; }

            [ClassMember(Description =
                "The collection containing all the (foreign) entities for the type that this step leads to\r\n"
            )]
            public IP ForeignEntitiesCollection { get; private set; }

            public ForeignKeySingleStep(ForeignKeyDirection direction, EntityTypeAndKey relation, IP foreignEntitiesCollection)
            {
                Direction = direction;
                Relation = relation;
                ForeignEntitiesCollection = foreignEntitiesCollection;
            }

            public override string ToString() => Direction.ToString() + ": " + (Direction == ForeignKeyDirection.ToOne ?
                ("Use " + Relation.Key + " to find one " + Relation.EntityType.ToStringVeryShort()) :
                ("Use " + Relation.Key + " in " + Relation.EntityType.ToStringVeryShort() + " to find many " + Relation.EntityType.ToStringVeryShort()));
        }

        public class InvalidForeignKeyException : ApplicationException
        {
            public InvalidForeignKeyException(string message) : base(message) { }
            public InvalidForeignKeyException(string message, Exception inner) : base(message, inner) { }
        }

        public class ForeignKeyException : ApplicationException
        {
            public ForeignKeyException(string message) : base(message) { }
            public ForeignKeyException(string message, Exception inner) : base(message, inner) { }
        }
    }

    [Enum(
         Description =
            "Describes the type of traversing to a foreign entity possible.\r\n" +
            "\r\n" +
            "See also -" + nameof(ForeignKey.IPRelationsKeysPointingFrom) + "- and -" + nameof(ForeignKey.IPRelationsKeysPointingTo) + "-.\r\n",
         AREnumType = AREnumType.OrdinaryEnum)]
    public enum ForeignKeyDirection
    {
        __invalid,

        [EnumMember(Description =
            "Traverse to one entity, like " +
            "traversing from OrderLine to Order (based on OrderLine containing foreign key OrderId) or " +
            "Order to Customer (based on Order containing foreign key CustomerId)."
         )]
        ToOne,

        [EnumMember(Description =
            "Traverse  to many entities, like " +
            "traversing from Customer to Order (based on Order containing foreign key CustomerId) or " +
            "Order to OrderLine (based on OrderLine containing foreign key OrderId)."
        )]
        ToMany
    }
}