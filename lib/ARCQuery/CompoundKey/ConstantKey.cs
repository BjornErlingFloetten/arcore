﻿using ARCCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ARCQuery {

    [Class(Description = 
        "Represents a constant expression like \"' '\", \"', '\", \"42\"."
    )]
    public class ConstantKey<T> : CompoundKey where T : notnull {

        public T Value { get; private set; }
        private PValue<T> _retval;

        public ConstantKey(T value) {
            Value = value;
            _retval = new PValue<T>(value);
        }

        public override bool TryGetPInternal(IKIP ikip, IP dataStorage, out IP retval, out string errorResponse) {
            retval = _retval;
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public string? _toString;
        public override string ToString() => _toString ??= (Value is string s ? ("'" + s + "'") : PValue<T>.ConvertObjectToString(Value)) + BaseToString();
    }
}
