﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
// using System.Collections.Concurrent;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery
{
    [Class(
        Description =
            "A class able to expand the concept of entity keys.\r\n" +
            "\r\n" +
            "Example: In addition to ordinary keys for 'Customer' like 'FirstName', 'LastName' and 'DateOfBirth', it is able to offer:\r\n" +
            "1) Expressions like \"FirstName'+' '+'LastName\" (see -" + nameof(BinaryOperatorKey) + "- and -" + nameof(ConstantKey<TValue>) + "-).\r\n" +
            "2) Keys implemented directly in actual class like 'Name' (see -" + nameof(EntityMethodKey) + "-).\r\n" +
            "3) Functions acting on keys, like 'DateOfBirth.Year()' (see -" + nameof(FunctionKey) + "- / -" + nameof(FunctionKeyYear) + "-).\r\n" +
            "4) Global new keys like 'CountP()' (see -" + nameof(NewKey) + "- / -" + nameof(NewKeyCountP) + "-).\r\n" +
            "5) Foreign keys with for instance aggregations like 'Order.Amount.Sum()' (see -" + nameof(ForeignKey) + "- / -" + nameof(FunctionKeyAggregate) + "- / -" + nameof(FunctionKeyAggregateSum) + ").\r\n" +
            "\r\n" +
            "These keys / expressions can then be used whenever ordinary keys are used, " +
            "like in -" + nameof(QueryExpression) + "-s like SELECT, WHERE, AGGREGATE, PIVOT and so on.\r\n" +
            "\r\n" +
            "Most relevant sub classes of -" + nameof(CompoundKey) + "- are:\r\n" +
            "\r\n" +
            "1) -" + nameof(BinaryOperatorKey) + "-: Offers expressions with operators like +, -, * and /.\r\n" +
            "\r\n" +
            "2) -" + nameof(ConstantKey<TValue>) + "-: Represents a constant expression like \"' '\", \"', '\", \"42\".\r\n" +
            "\r\n" +
            "3) -" + nameof(EntityMethodKey) + "-: Key able to call read-only methods defined in the entity class with names like 'TryGet{FieldName}'.\r\n" +
            "Example: For 'Customer' it can find 'Customer.Name' which is calculated in a read-only method Customer.TryGetName " +
            "as combination of 'FirstName', 'LastName' and 'CompanyName'.\r\n" +
            "\r\n" +
            "4) -" + nameof(NewKey) + "-: Creates new keys available for all entities.\r\n" +
            "Example: For 'Customer' it can count number of properties through 'CountP' (see -" + nameof(NewKeyCountP) + "-.)\r\n" +
            "\r\n" +
            "5) -" + nameof(ForeignKey) + "-: Can find related values.\r\n" +
            "Example: For 'Order', it can find 'Customer.Name'.\r\n" +
            "\r\n" +
            "6) -" + nameof(MemberEntityKey) + "-: Can find value for entity which is member of entity being queried.\r\n" +
            "Example: For 'Order', it can find 'Customer.Name'.\r\n" +
            "\r\n" +
            "Closely related classes " +
            "(Stored inside -" + nameof(CompoundKey) + "- as members " +
            "-" + nameof(CompoundKey.FunctionKeys) + "-, and " +
            "-" + nameof(CompoundKey.QuantileKey) + "-" +
            ") " +
            "are:\r\n" +
            "1) -" + nameof(FunctionKey) + "- / -" + nameof(FunctionKeyAggregate) + "-: " +
            "Extracts data from an already existing value like 'Created.Year()' (see -" + nameof(FunctionKeyYear) + "-)\r\n" +
            "and\r\n" +
            "2) -" + nameof(ARCQuery.QuantileKey) + "-: Can separate entities into quantiles, like Quartiles, Quintiles or Sextiles\r\n" +
            "\r\n" +
            "(See also -" + nameof(ValueComparer) + "- which can evaluation expressions like for 'Customer': 'WHERE Created = ThisYear')\r\n" +
            "\r\n" +
            "Other sub-classes of -" + nameof(CompoundKey) + "- are -" + nameof(OrdinaryKeyPlusFunctionOrQuantile) + "- and -" + nameof(CompoundInvalidKey) + "-.\r\n" +
            "\r\n" +
            "See common parser for all compound keys (-" + nameof(Parse) + "-) which will always 'succeed' " +
            "(a 'TryParse'-method is neither needed nor offered, -" + nameof(CompoundInvalidKey) + "- is returned if not key found).\r\n" +
            "\r\n" +
            "Note how compound keys (and related keys) may be chained several levels deep, like " +
            "for 'Order': 'Customer.Created.Year()' or " +
            "for 'Customer': 'Order.Amount.Sum()' or " +
            "even 'Order.Amount.Sum().QUARTILE'.\r\n" +
            "This is all parsed in -" + nameof(Parse) + "- and stored in -" + nameof(FunctionKeys) + "- / -" + nameof(CompoundKey.QuantileKey) + "-.\r\n" +
            "\r\n" +
            "See -" + nameof(QueryExpressionSelect) + "- for hint about using 'AS'-clause in order to simplify parsing and evaluation in subsequent " +
            "-" + nameof(QueryExpression) + "-s when using -" + nameof(CompoundKey) + "-.\r\n" +
            "\r\n" +
            "All keys can be followed by one or several -" + nameof(FunctionKey) + "-s\r\n" +
            "\r\n" +
            "A -" + nameof(QuantileKey) + "- can only be the last key in a chain, like 'Customer.Order.Amount.Sum().QUARTILE'.\r\n" +
            "It is not evaluated by -" + nameof(CompoundKey) + "-.-" + nameof(TryGetP) + "-, " +
            "it is instead evaluated by -" + nameof(ARComponents.ARCQuery) + "-. -" + nameof(Extensions) + "-.-" + nameof(Extensions.TryGetP) + "-)\r\n" +
            "\r\n"
    )]
    public abstract class CompoundKey
    {

        [ClassMember(Description =
            "All -" + nameof(FunctionKey) + "-s chained at end of compound key (but before an eventual -" + nameof(QuantileKey) + "-).\r\n" +
            "\r\n" +
            "Example: For 'Customer', 'Created.Year()' (see -" + nameof(FunctionKeyYear) + "-)\r\n" +
            "Multiple function keys may be chained, like for instance\r\n" +
            "for 'Customer', 'Created.Year().Format(\"00\")' (note, this example is contrived, because as of Jul 2020 there is no Format-function available).\r\n" +
            "\r\n" +
            "TODO: Introduce into constructor in order to make this class immutable."
        )]
        public IEnumerable<FunctionKey> FunctionKeys { get; set; } = new List<FunctionKey>();

        [ClassMember(Description =
            "Eventual final -" + nameof(QuantileKey) + "- at end of compound key.\r\n" +
            "\r\n" +
            "Example: For 'Customer', DateOfBirth.Year().QUARTILE.\r\n" +
            "Example: For 'Order', Sum.TERTILE.\r\n" +
            "\r\n" +
            "Note that this is not evaluated through -" + nameof(CompoundKey) + "-.-" + nameof(CompoundKey.TryGetP) + "- " +
            "(in contrast to -" + nameof(FunctionKeys) + "-), " +
            "instead -" + nameof(QuantileKey) + "-.-" + nameof(QuantileKey.TryGetP) + "- is called with a compound key parameter." +
            "(This is done in -" + nameof(ARComponents.ARCQuery) + "-.-" + nameof(Extensions) + "-.-" + nameof(Extensions.TryGetP) + "-).\r\n" +
            "\r\n" +
            "TODO: Introduce into constructor in order to make this class immutable."
        )]
        public QuantileKey? QuantileKey { get; set; } = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ikip">
        /// Note that the only implementing class which actually needs the 'Key'-part of this parameter is
        /// <see cref="EntityMethodKey"/>. All the other classes only need the 'P'-part.
        /// </param>
        /// <param name="dataStorage"></param>
        /// <param name="retval"></param>
        /// <param name="errorResponse"></param>
        /// <returns></returns>
        [ClassMember(Description =
            "The outwards facing executor, understanding use of -" + nameof(FunctionKeys) + "-.\r\n"
        )]
        public bool TryGetP(IKIP ikip, IP dataStorage, out IP retval, out string errorResponse)
        {
            if (!TryGetPInternal(ikip, dataStorage, out retval, out errorResponse)) return false;

            foreach (var fk in FunctionKeys)
            {
                if (!fk.TryGetP(retval, out retval, out errorResponse))
                {
                    errorResponse =
                        "For function " + fk.ToString() + ":\r\n" +
                        errorResponse;
                    return false;
                }
            }

            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "The internal executor, containing the specific functionality for how to evaluate the key."
        )]
        public abstract bool TryGetPInternal(IKIP ikip, IP dataStorage, out IP retval, out string errorResponse);

        [ClassMember(Description =
            "Note that no TryParse is offered here, because Parse will always succeed " +
            "(it just returns -" + nameof(CompoundInvalidKey) + "- if 'fails').\r\n" +
            "\r\n" +
            "Called from -" + nameof(ARCQuery.Extensions.TryGetP) + "- when that fails to find the given key for the entity. " +
            "In other words, when the key does not exist direct on the entity, it tries to parse it as a -" + nameof(CompoundKey) + "- instead.\r\n"
        )]
        public static CompoundKey Parse(IP dataStorage, string strKey, Type entityType)
        {
            if (!typeof(IP).IsAssignableFrom(entityType))
            {
                return new CompoundInvalidKey("Invalid " + nameof(entityType) + " (-" + entityType.ToStringVeryShort() + "-), not assignable to -" + nameof(IP) + "-.");
            }

            /// Extract final quantile evaulator at end of key (like .QUARTILE)
            /// Note how this takes precedence over parsing of an eventual <see cref="BinaryOperatorKey"/>
            /// For instance for 'Order', 'SELECT AmountExVAT+VAT.QUARTILE' will evaluate as 'SELECT (AmountExVAT+VAT).QUARTILE'.
            var pos = strKey.LastIndexOf(".");
            QuantileKey? quantileKey = null;
            if (pos > 0)
            {
                var candidate = strKey[(pos + 1)..];
                if (QuantileKey.TryParse(candidate, out quantileKey))
                {
                    if (long.TryParse(candidate, out _))
                    {
                        // Discard, too easily confused with double (decimal numbers)
                        // Example: Avoid parsing of 'Amount+5.05' as '(Amount+5).QUINTILE'
                        quantileKey = null;
                    }
                    else
                    {
                        // Accept quantile key found
                        strKey = strKey[0..pos];
                    }
                }
            }

            // TODO: Add paranthesis in order to control order of evaluation
            // TODO: Now everything is left to right (?)

            /// Check for <see cref="BinaryOperatorKey"/>
            pos = UtilQuery.IndexOfAnyOutsideOf(strKey, anyOf: new char[] { '+', '-', '*', '/' }, outsideOf: '\'');
            if (pos > 0 && pos < strKey.Length - 1)
            {
                return new BinaryOperatorKey(
                    leftOperandKey: IKString.FromString(strKey[0..pos]), strKey[pos].ToString(), rightOperandKey: IKString.FromString(strKey[(pos + 1)..])
                )
                {
                    /// Note: <see cref="CompoundKey.FunctionKeys"/> will be contained within left / right operand.
                    QuantileKey = quantileKey
                };
            }

            var t = UtilQuery.SplitOutsideOf(strKey, separator: ".", outsideOf: '\'');
            // Allow full stop as desimal point, by joining adjacent 'integers'           
            // This allows keys like 'Amount+5.25 AS AmountIncPostage' (contrived example of course)
            bool found;
            do
            {
                found = false;
                for (var i = 0; i < t.Count - 1; i++)
                {
                    if (long.TryParse(t[i], out _) && long.TryParse(t[i + 1], out _))
                    {
                        found = true;
                        t[i] = t[i] + "." + t[i + 1];
                        t.RemoveAt(i + 1);
                        break;
                    }
                }
            } while (found);

            // Extract function keys, like .Year() (multiple such keys may be found now)
            // Do not include aggregate keys as they are always related to foreign keys.
            var followingFunctionKeys = new List<FunctionKey>();
            while (t.Count > 1 && FunctionKey.TryParse(t[^1], out var fk) && !(fk is FunctionKeyAggregate))
            {
                followingFunctionKeys.Add(fk);
                t.RemoveAt(t.Count - 1);
            }
            followingFunctionKeys.Reverse();

            if (t.Count > 1)
            {
                /// Only possibility now (for parsing) is <see cref="ForeignKey"/> or <see cref="MemberEntityKey"/>

                if (t[^1].EndsWith("()"))
                {
                    /// This must be a <see cref="FunctionKeyAggregate"/>, if not something is wrong with query syntax
                    if (!FunctionKey.TryParse(t[^1], out var k) || !(k is FunctionKeyAggregate))
                    {
                        return new CompoundInvalidKey("'" + t[^1] + "' is not recognized as a -" + nameof(FunctionKey) + "- or -" + nameof(FunctionKeyAggregate) + "-.");
                    }
                    /// TODOe: We can have multiple <see cref="FunctionKeyAggregate"/>, like Distinct().Join() for instance, but look for only one here.
                }
                if (ForeignKey.TryParse(dataStorage, t, entityType, followingFunctionKeys, quantileKey, out var fk, out var fkErrorResponse))
                {
                    // Alternative 1) Try ordinary foreign key (lookup in data storage)
                    // Try this alternative first because it does not depend on the entity itself
                    // (avoid weakness where caching of compound keys gives different results depending on first entity tried)
                    return fk;
                }
                else if (MemberEntityKey.TryParse(dataStorage, strKey, t, entityType, out var mek, out var mekErrorResponse))
                {
                    // Alternative 2), MemberEntityKey. Only possible when 
                    // 1) Strongly typed schema for member key, or
                    // 2) Member key is an EntityMethodKey
                    return mek;
                }
                else
                {
                    return new CompoundInvalidKey(
                        "Invalid " + nameof(ForeignKey) + " because of: " + fkErrorResponse + "\r\n" +
                        "Invalid " + nameof(MemberEntityKey) + " because of: " + mekErrorResponse + "\r\n");
                }
            }

            /// There is now only one "part" (t[0])

            /// Priority 1, <see cref="ConstantKey{T}"/>
            /// ==============================================
            /// Example: "Customer/SELECT LastName+', '+FirstName" (where "', '" is the Constant key we are looking for now)
            if (followingFunctionKeys.Count > 0 || quantileKey != null)
            {
                /// HACK: Field names that can be confused with constant expression (especially integer constant expressions)
                /// HACK: is an issue.
                /// HACK: For instance if we have field names '2019', '2020', '2021', '2022' then
                /// HACK: 'SELECT 2021.TMB(), 2022.TMB()' 
                /// HACK: should not be understood as <see cref="ConstantKey{T}"/> of type long plus <see cref="FunctionKeyTMB"/>
                /// HACK: Instead, the should be understood as <see cref="OrdinaryKeyPlusFunctionOrQuantile"/>.
                /// HACK: In general, we therefore just disallow function keys and quantile keys in conjuntion with constant keys.  
                /// NOTE:
                /// NOTE: In general (related to above), 'SELECT 2021, 2022' in example above works because 
                /// NOTE: then this parser will not be called at all by <see cref="Extensions.TryGetP"/>
                /// NOTE: because field will be found by ordinary call to <see cref="IP.TryGetP"/>
                /// NOTE: BUT, note potential ugly situation mentioned in comments in <see cref="Extensions.TryGetP"/>
                /// NOTE: that can arise if field exists for some values but not all.
            }
            else
            {
                if (t[0].Length > 1 && '\''.Equals(t[0][0]) && '\''.Equals(t[0][^1]))
                {
                    // Looks like a constant string expression 
                    return new ConstantKey<string>(t[0][1..^1]);
                    // NOTE: This is also the place to look for global key, like Now() for instance.
                    // TOOD: Implement global key
                }
                else if (long.TryParse(t[0], out var l))
                { /// Note: Similar code in <see cref="PValue{T}.TryGetV"/> and <see cref="PKTypeAttribute.StandardValidatorAndParser"/>
                    return new ConstantKey<long>(l);
                }
                else if (UtilCore.DoubleTryParse(t[0], out var d))
                { /// Note: Similar code in <see cref="PValue{T}.TryGetV"/> and <see cref="PKTypeAttribute.StandardValidatorAndParser"/>
                    // Note trick with decimal point when splitting at full stops '.' above (because full stop is also a decimal point)
                    return new ConstantKey<double>(d);
                }
            }

            /// Priority 2, <see cref="EntityMethodKey"/>
            /// ==============================================
            /// Example: 'Customer/SELECT Name, ...'
            // Check for direct implementation on entity
            if (EntityMethodKey.TryParse(entityType, "TryGet" + t[0], out var emk, out _))
            { // Note how we do not consume errorResponse here. (TODO:?)
                emk.FunctionKeys = followingFunctionKeys;
                emk.QuantileKey = quantileKey;
                return emk;
            }

            /// Priority 3, <see cref="NewKey"/>
            /// ==============================================
            /// Example: 'Customer/SELECT CountP, ...' (<see cref="NewKeyCountP"/>
            // Check next for new key ("extension method").
            if (NewKey.TryParse(t[0], out var nk, out var _))
            { // Note how we ignore the error response here.
                nk.FunctionKeys = followingFunctionKeys;
                nk.QuantileKey = quantileKey;
                return nk;
            }

            /// Priority 4, <see cref="OrdinaryKeyPlusFunctionOrQuantile"/>
            /// ==============================================
            /// Example 1: 'Customer/SELECT Age.QUARTILE, ...' (<see cref="Quantile.QUARTILE"/>
            /// Example 2: 'Customer/SELECT DateOfBirth.Year(), ...' (<see cref="FunctionKeyYear"/>
            /// Example 3: 'Customer/SELECT DateOfBirth.Year().QUARTILE, ...' (combination)
            /// 
            /// TODO: This messes up order of priority.
            /// TODO: Now <see cref="EntityMethodKey"/> and <see cref="NewKey"/> suddenly got precedence over an ordinary PK (see above), 
            /// TODO: just because we added a function or quantilizer to the ordinary PK here.
            if (followingFunctionKeys.Count > 0 || quantileKey != null)
            {
                return new OrdinaryKeyPlusFunctionOrQuantile(IKString.FromString(t[0]))
                {
                    FunctionKeys = followingFunctionKeys,
                    QuantileKey = quantileKey
                };
            }

            return new CompoundInvalidKey("Invalid key.\r\n");
        }

        [ClassMember(Description =
            "The ToString representation is the one relevant when used as key, like in -" + nameof(IK) + "- or when selecting like 'SELECT {fieldName}'.\r\n" +
            "\r\n" +
            "NOTE: Implementing classes should append return value with result from -" + nameof(BaseToString) + "-")]
        public abstract override string ToString();

        [ClassMember(Description = "Returns string-representation of -" + nameof(FunctionKeys) + "- and -" + nameof(QuantileKey) + "-.")]
        protected string BaseToString() =>
            (FunctionKeys.Count() == 0 ? "" :
                ("." + string.Join(".", FunctionKeys.Select(k => k.ToString())))
            ) +
            (QuantileKey == null ? "" : ("." + QuantileKey!.ToString()));
    }
}
