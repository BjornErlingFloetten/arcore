﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery
{

    [Class(Description =
        "Can find value for entity which is member of entity being queried.\r\n" +
        "\r\n" +
        "Works when either:\r\n" +
        "1) Member key is strongly defined in a schema for the member key (there must exist a -" + nameof(PK) + "- for the member key).\r\n" +
        "2) Member key is a -" + nameof(EntityMethodKey) + "-.\r\n" +
        "\r\n" +
        "Note: 2) was added on 3 Jan 2022.\r\n" +
        "It is useful only in more obscure scenarios, because having an EntityMethodKey return an 'Entity' (like 'Customer' or 'Order') " +
        "is normally superfluous, because that is exactly the purpose of -" + nameof(ForeignKey) + "-.\r\n" +
        "If however ForeignKey is not usable in a specific scenario (that is, if it is not able to automatically deduce which " +
        "foreign entity is desired), this might come in handy.\r\n" +
        "\r\n" +
        "Example (from AgoRapide documentation): -" + nameof(ARCDoc.DocFrag) + "- contains -" + nameof(ARCDoc.DocFragP.Attribute) + "- " +
        "which itself is an entity.\r\n" +
        "This key (-" + nameof(MemberEntityKey) + "-) then enables queries like 'DocFrag/SELECT Attribute.MethodSignature'.\r\n" +
        "\r\n" +
        "The key may be chained, that is, it can find value for member of member and so on.\r\n" +
        "\r\n" +
        "See also -" + nameof(ForeignKey) + "-.\r\n" +
        "\r\n" +
        "NOTE: As of Mar 2021 quite new and untested. See TODO about parsing below."
    )]
    public class MemberEntityKey : CompoundKey
    {

        public PK? MemberKey { get; private set; }
        public EntityMethodKey? EntityMethodKey { get; private set; }

        public CompoundKey? FollowingKeys;
        public IK? FollowingKey;

        public MemberEntityKey(PK? memberKeyAsPK, EntityMethodKey? memberKeyAsEntityMethodKey, CompoundKey? followingKeys, IK? followingKey)
        {
            MemberKey = memberKeyAsPK;
            EntityMethodKey = memberKeyAsEntityMethodKey;
            if (memberKeyAsPK == null && memberKeyAsEntityMethodKey == null) throw new NullReferenceException("Both " + nameof(memberKeyAsPK) + " and " + nameof(memberKeyAsEntityMethodKey) + " was null. One must be null only.");
            if (memberKeyAsPK != null && memberKeyAsEntityMethodKey != null) throw new NullReferenceException("None of " + nameof(memberKeyAsPK) + " and " + nameof(memberKeyAsEntityMethodKey) + " was null. One must be null only.");

            FollowingKeys = followingKeys;
            FollowingKey = followingKey;
            if (followingKeys == null && followingKey == null) throw new NullReferenceException("Both " + nameof(followingKeys) + " and " + nameof(followingKey) + " was null. One must be null only.");
            if (followingKeys != null && followingKey != null) throw new NullReferenceException("None of " + nameof(followingKeys) + " and " + nameof(followingKey) + " was null. One must be null only.");
        }

        public override bool TryGetPInternal(IKIP ikip, IP dataStorage, out IP retval, out string errorResponse)
        {
            IKIP? memberIKIP = null;
            if (MemberKey != null)
            {
                if (!ikip.P.TryGetP<IP>(MemberKey, out var memberEntity, out errorResponse))
                {
                    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse = "Member entity (" + MemberKey.ToString() + ") (as -" + nameof(PK) + "-) not found.\r\nDetails: " + errorResponse;
                    return false;
                }
                memberIKIP = new IKIP(MemberKey, memberEntity);
            }
            else if (EntityMethodKey != null)
            {
                if (!EntityMethodKey.TryGetP(ikip, dataStorage, out var memberEntity, out errorResponse))
                {
                    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse = "Member entity (" + EntityMethodKey.ToString() + ") (as -" + nameof(EntityMethodKey) + "-) not found.\r\nDetails: " + errorResponse;
                    return false;
                }
                InvalidObjectTypeException.AssertAssignable(memberEntity, EntityMethodKey.ReturnType, () =>
                    "Type of entity returned by -" + nameof(EntityMethodKey) + "- (" + EntityMethodKey.ToString() + ") " +
                    "does not correspond to -" + nameof(EntityMethodKey.ReturnType) + "-."
                );
                memberIKIP = new IKIP(EntityMethodKey.MethodNameAsIK, memberEntity);
            }
            else
            {
                throw new NullReferenceException("Both " + nameof(MemberKey) + " and " + nameof(EntityMethodKey) + " was null. One must be null only.");
            }

            if (FollowingKeys != null)
            {
                return FollowingKeys.TryGetP(memberIKIP, dataStorage, out retval, out errorResponse);
            }
            else if (FollowingKey != null)
            {
                CompoundKey? cachedCompoundKey = null;
                return memberIKIP.TryGetP(dataStorage, FollowingKey, out retval, ref cachedCompoundKey, out errorResponse);
            }
            else
            {
                throw new NullReferenceException("Both " + nameof(FollowingKeys) + " and " + nameof(FollowingKey) + " was null. One must be null only.");
            }
        }

        public string? _toString;
        public override string ToString() => _toString ??= (
            MemberKey?.ToString() ?? EntityMethodKey?.ToString() ?? throw new NullReferenceException("Both " + nameof(MemberKey) + " and " + nameof(EntityMethodKey) + " was null")
         ) + "." + (
            FollowingKey?.ToString() ?? FollowingKeys?.ToString() ?? throw new NullReferenceException("Both " + nameof(FollowingKeys) + " and " + nameof(FollowingKey) + " was null")
         );

        public static bool TryParse(IP dataStorage, string strKey, List<string> t, Type entityType, out MemberEntityKey retval, out string errorResponse)
        {
            if (t.Count < 2)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Less than two items (" + t.Count + ")";
                return false;
            }

            Type? newEntityType = null;
            EntityMethodKey? entityMethodKey = null; // TODO: Why do we have to declare this here, instead of using 'var' below?

            if (PK.TryGetFromTypeAndFieldName(entityType, t[0], out var pk))
            {
                // 1) Member key is strongly defined in a schema for the member key 
                if (!typeof(IP).IsAssignableFrom(pk.Type))
                {
                    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    var s = entityType.ToStringVeryShort();
                    errorResponse =
                        "Invalid type -" + pk.Type.ToStringShort() + "- for '-" + s + "P-.-" + t[0] + "-'.\r\n" +
                        "Must inherit -" + nameof(IP) + "- in order to work as a -" + nameof(MemberEntityKey) + " -.";
                    return false;
                }
                newEntityType = pk.Type;
            }
            else if (EntityMethodKey.TryParse(entityType, "TryGet" + t[0], out entityMethodKey, out _)) // Note how we do not consume errorResponse here. (TODO:?)
            {
                /// 2) Member key is a <see cref="EntityMethodKey"/>
                /// New code from 3 Jan 2022
                if (!IP.AllIPDerivedTypes.Contains(entityMethodKey.ReturnType))
                {
                    var s = entityType.ToStringVeryShort();
                    errorResponse =
                        "Invalid type -" + entityMethodKey.ReturnType.ToStringShort() + "- returned by '" + s + ".-TryGet." + t[0] + "-'.\r\n" +
                        "Must inherit -" + nameof(IP) + "- in order to work as a -" + nameof(MemberEntityKey) + " -.";

                }
                newEntityType = entityMethodKey.ReturnType;
            }
            else
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                var s = entityType.ToStringVeryShort();
                errorResponse =
                    "Property '" + t[0] + "' not defined in schema for '" + s + "' (enum '" + s + "P." + t[0] + "' does not exist,\r\n" +
                    "and also\r\n" +
                    "EntityMethodKey 'TryGet" + t[0] + "' not defined for " + s;
                return false;
            }

            var len = (t[0] + ".").Length;
            if (strKey.Length < len)
            {
                throw new ArgumentException(nameof(strKey) + " (" + strKey + ") is invalid compared to " + nameof(t) + ", too short, must be longer than '" + t[0] + ".'.");
            }
            var followingKeys = CompoundKey.Parse(dataStorage, strKey[len..], newEntityType);
            IK? followingKey = null;
            if (followingKeys is CompoundInvalidKey cik)
            {
                /// This is quite possible if we have only one more "item" in t (that is, an "ordinary" key), 
                /// The important thing is that (by calling <see cref="CompoundKey.Parse"/>) we have now checked for possible <see cref="EntityMethodKey"/> and <see cref="NewKey"/>
                /// BUT, we must also check for <see cref="BinaryOperatorKey"/>
                /// TODO: Find better way of doing this, code checking for "'+', '-', '*', '/'" is now duplicated outside of <see cref="CompoundKey.Parse"/>
                if (t.Count == 2 && UtilQuery.IndexOfAnyOutsideOf(strKey, anyOf: new char[] { '+', '-', '*', '/' }, outsideOf: '\'') == -1)
                {
                    // Following key is just an ordinary key
                    followingKey = IKString.FromString(t[1]);
                }
                else
                {
                    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse = "Valid as -" + nameof(MemberEntityKey) + "- but rest of key invalid due to:\r\n" + cik.ErrorResponse;
                    return false;
                }
            }
            retval = new MemberEntityKey(pk, entityMethodKey, followingKey == null ? followingKeys : null, followingKey);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }
    }
}
