﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery
{
    [Class(Description =
        "Key able to call read-only methods defined in the entity class with names like 'TryGet{FieldName}'.\r\n" +
        "\r\n" +
        "Some common usages of this would be to implement:\r\n" +
        "- In a class called 'Customer' with 'FirstName' and 'LastName', a new property 'Name'.\r\n" +
        "- In a class called 'OrderLine' with 'Quantity' and 'UnitPrice', a new property 'Sum'.\r\n" +
        "\r\n" +
        "In essence this is making an entity class able to describe itself, something which is quite natural " +
        "in many cases.\r\n" +
        "\r\n" +
        "The principle can also be used to simplify querying while still having tables normalized.\r\n" +
        "If for instance address data for 'Customer' objects is normalized into a separate table 'Geography' " +
        "then one could implement properties on 'Customer' like 'City', 'State', 'Country'.\r\n" +
        "This would enable querying for 'Customer' like\r\n" +
        "'SELECT Name, Address, City, State, Country'\r\n" +
        "instead of the more laborous\r\n" +
        "'SELECT Geography.Name, Geography.Address, Geography.City, Geography.State, Geography.Country'\r\n" +
        "(see -" + nameof(TryGetForeignField) + "- which is a helper method for this purpose).\r\n" +
        "\r\n" +
        "Implementations of a few such properties can often significantly reduce the complexity of queries / " +
        "reduce the need for denormalizing tables in the database.\r\n" +
        "\r\n" +
        "The risk of bugs are also reduced since the properties will only be defined in one place, as close as possible to its source properties" +
        "instead of complex query components being repeated multiple times in a reporting system for instance.\r\n" +
        "\r\n" +
        "In addition, memory and disk space is conserved because the returned values are usually ephemeral.\r\n" +
        "Performance MAY also be increased as the values are only calculated when needed.\r\n" +
        "\r\n" +
        "This class (-" + nameof(EntityMethodKey) + "-) looks for such entity property defining methods through reflection. " +
        "\r\n" +
        "It is able to call methods with signatures like either:\r\n" +
        "  bool TryGet{KeyName}(out IP retval, out string errorResponse) (see -" + nameof(MethodType.Simple) + "-)\r\n" +
        "or\r\n" +
        "  bool TryGet{KeyName}(IP dataStorage, IK thisKey, out IP retval, out string errorResponse) " +
        "(see -" + nameof(MethodType.Complex) + "-)\r\n" +
        "\r\n" +
        "The second overload makes the entity class able to query other parts of the system in order to calculate the return value.\r\n" +
        "\r\n" +
        "These should be read-only methods / read-only properties, that is, " +
        "they should not change the state of the entity or the data storage.\r\n" +
        "\r\n" +
        "Note: 'out IP retval' can be any type assignable to -" + nameof(IP) + "-, so it is recommended to use more strongly typing " +
        "in the method signature, like 'out PValue<DateTime> retval' for instance.\r\n" +
        "\r\n" +
        "See also -" + nameof(NewKey) + "- (creating new keys (new fields) for all entity object types)."
    )]
    public class EntityMethodKey : CompoundKey
    {

        private readonly MethodType _methodType;

        [ClassMember(Description = "The actuall TryGet method implemented in the relevant entity class.")]
        public System.Reflection.MethodInfo Method { get; private set; }

        [ClassMember(Description = "Name of TryGet method but without 'TryGet' in name")]
        public IK MethodNameAsIK { get; private set; }

        [ClassMember(Description =
            "The type of the 'retval' out-parameter of the method.\r\n" +
            "\r\n" +
            "This must be of type IP, often it will be a -" + nameof(PValue<TValue>) + "- like PValue<string>, PValue<long> or similar.\r\n"
        )]
        public Type ReturnType { get; private set; }

        public ClassMemberAttribute Attribute;

        public override bool TryGetPInternal(IKIP ikip, IP dataStorage, out IP retval, out string errorResponse)
        {
            // InvalidObjectTypeException.AssertEquals(ikip.P, Method.DeclaringType, () =>
            if (!Method.DeclaringType.IsAssignableFrom(ikip.P.GetType()))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse =
                    "This EntityMethodKey (" + Method.Name + ") is valid for " + Method.DeclaringType + " " +
                    "but the parameter IKIP.P is of type " + ikip.P.GetType().ToStringShort();
                return false;
            }

            var (parameters, retvalIndex, errorResponseIndex) = new Func<(object?[], int, int)>(() =>
               (_methodType) switch
               {
                   MethodType.Simple => (new object?[] { null, null }, 0, 1),
                   MethodType.Complex => (new object?[] { dataStorage, ikip.Key, null, null }, 2, 3),
                   _ => throw new InvalidEnumException(_methodType)
               }
            )();
            bool result;
            try
            {
                result = (bool)Method.Invoke(ikip.P, parameters);
            }
            catch (Exception ex)
            {
                throw new System.Reflection.TargetInvocationException(
                    "Unable to call method " + Method.Name + "\r\n" +
                    "(" + Method.ToString() + ")\r\n" +
                    "for " + ikip.P.GetType() + "\r\n" +
                    "Assumed to be of type " + _methodType + "\r\n",
                    ex
                );
            }
            if (!result)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = parameters[errorResponseIndex] as string ?? throw new NullReferenceException(
                    parameters[errorResponseIndex] != null ?
                    (
                        "Out-parameter errorResponse (index " + errorResponseIndex + ") of wrong type " +
                        "(" + parameters[errorResponseIndex]!.GetType().ToString() + "), string expected. " +
                        "Set by " + Method.DeclaringType + "." + Method.ToString()
                    ) :
                    (
                        "Out-parameter errorResponse (index " + errorResponseIndex + ") not set by " + Method.DeclaringType + "." + Method.ToString()
                    )
                );
                return false;
            }
            retval = parameters[retvalIndex] as IP ?? throw new NullReferenceException(
                parameters[retvalIndex] != null ?
                (
                    "Out-parameter retval (index " + retvalIndex + ") of wrong type (" + parameters[retvalIndex]!.GetType().ToString() + "), " +
                    "IP expected. Set by " + Method.DeclaringType + "." + Method.ToString()
                ) :
                (
                    "Out-parameter retval (index " + retvalIndex + ") not set by " + Method.DeclaringType + " method " + Method.ToString()
                )
            );
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public EntityMethodKey(MethodType methodType, System.Reflection.MethodInfo method, Type returnType)
        {
            _methodType = methodType;
            Method = method;
            MethodNameAsIK = IKString.FromCache(Method.Name["TryGet".Length..]);
            ReturnType = returnType;
            Attribute = ClassMemberAttribute.GetAttribute(method);
        }

        public string? _toString;
        public override string ToString() => _toString ??= MethodNameAsIK.ToString() + BaseToString();

        /// <summary>
        /// Looks for existence of method <paramref name="methodName"/> in <paramref name="entityType"/>
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="methodName">May start with 'TryGet', or not ('TryGet' will be prepended as necessary when looking for method)</param>
        /// <param name="retval"></param>
        /// <returns></returns>
        public static bool TryParse(Type entityType, string methodName, out EntityMethodKey retval, out string errorResponse)
        {
            var method = entityType.GetMethod(
                (methodName.StartsWith("TryGet") ? "" : "TryGet") + methodName,
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public
            );
            if (method == null)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Method not found at all";
                return false;
            }
            if (method.ReturnType == null || !method.ReturnType.Equals(typeof(bool)))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Method found but with wrong return type  ('" + (method.ReturnType?.ToStringShort() ?? "void") + "') instead of 'bool'";
                return false;
            }
            var parameters = method.GetParameters();
            switch (parameters.Length)
            {
                case 2: /// <see cref="MethodType.Simple"/>
                    var retvalReturnType = parameters[0].ParameterType.GetElementType();
                    if (
                        // Note that we can not compare against parameterType for out-parameters,
                        // but must compare against GetElementType (which again may be null, so check the correct way around)
                        // (Their ToString-representation for instance looks like 'ARCore.IP&' and 'System.String&' respectively).
                        typeof(IP).IsAssignableFrom(retvalReturnType) && parameters[0].IsOut &&
                        typeof(string).Equals(parameters[1].ParameterType.GetElementType()) && parameters[1].IsOut)
                    {
                        retval = new EntityMethodKey(MethodType.Simple, method, retvalReturnType);
                        errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        return true;
                    }
                    else
                    {
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        errorResponse =
                            "Method found with 2 parameters but wrong signature, " +
                            "not 'bool TryGet{KeyName}(out IP retval, out string errorResponse)'";
                        return false;
                    }
                case 4: /// <see cref="MethodType.Complex"/>
                    retvalReturnType = parameters[2].ParameterType.GetElementType();
                    if (
                        typeof(IP).Equals(parameters[0].ParameterType) && // IsIn will be false... Why? parameters[0].IsIn &&
                        typeof(IK).Equals(parameters[1].ParameterType) && // IsIn will be false... Why? parameters[1].IsIn &&

                        // Note that we can not compare against parameterType for out-parameters,
                        // but must compare against GetElementType (which again may be null, so check the correct way around)
                        // (Their ToString-representation for instance looks like 'ARCore.IP&' and 'System.String&' respectively).
                        typeof(IP).IsAssignableFrom(retvalReturnType) && parameters[2].IsOut &&
                        typeof(string).Equals(parameters[3].ParameterType.GetElementType()) && parameters[3].IsOut)
                    {
                        retval = new EntityMethodKey(MethodType.Complex, method, retvalReturnType);
                        errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        return true;
                    }
                    else
                    {
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        errorResponse =
                            "Method found with 4 parameters but wrong signature, " +
                            "not 'bool TryGet{KeyName}(IP dataStorage, IK thisKey, out IP retval, out string errorResponse)'";
                        return false;
                    }
                default:
                    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse = "Method found but with wrong parameters count (" + parameters.Length + ") instead of 2 or 4";
                    return false;
            }
        }

        private readonly static ConcurrentDictionary<Type, Dictionary<string, EntityMethodKey>> _allEntityMethodKeysForEntityTypeDictCache =
            new ConcurrentDictionary<Type, Dictionary<string, EntityMethodKey>>();
        [ClassMember(Description =
            "Returns all -" + nameof(EntityMethodKey) + "- in the class definition of the given entity type.\r\n" +
            "\r\n" +
            "Key in returned dictionary is Method Name INCLUDING \"TryGet\".\r\n" +
            "\r\n" +
            "Useful in order to give query suggestions to the user for instance.\r\n" +
            "\r\n" +
            "Returns an empty collection if no methods found.\r\n" +
            "\r\n" +
            "See also -" + nameof(AllEntityMethodKeysForEntityTypeList) + "-.\r\n"
        )]
        public static Dictionary<string, EntityMethodKey> AllEntityMethodKeysForEntityTypeDict(Type entityType) =>
            _allEntityMethodKeysForEntityTypeDictCache.GetOrAdd(entityType, t =>
        {
            var retval = new Dictionary<string, EntityMethodKey>();
            foreach (var entityMethodKey in entityType.
                GetMethods(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public).
              // TODO: Use signature check instead because now we can not have inheriting entity classes.
                Where(m => m.DeclaringType.Equals(t)). /// Filter out methods like <see cref="IP.TryGetP"/>.
                Where(m => m.Name.StartsWith("TryGet")).
                Select(m => TryParse(t, m.Name, out var retval, out _) ? retval : null).Where(k => k != null).Select(k => k!)
            )
            {
                if (!retval.TryAdd(entityMethodKey.Method.Name, entityMethodKey))
                {
                    throw new EntityMethodKeyException(
                        "Duplicate " + nameof(EntityMethodKey) + " '" + entityMethodKey.Method.Name + "' " +
                        "found for type " + entityType.ToString() + ".\r\n" +
                        "Explanation: There can only be one method with name '" + entityMethodKey.Method.Name + "' " +
                        "understood by " + nameof(EntityMethodKey) + "." + nameof(EntityMethodKey.TryParse) + "."
                    );
                }
            }
            return retval;
        });

        private readonly static ConcurrentDictionary<Type, List<EntityMethodKey>> _allEntityMethodKeysForEntityTypeListCache = 
            new ConcurrentDictionary<Type, List<EntityMethodKey>>();
        [ClassMember(Description =
            "Returns List sorted by Method Name of Value of Dictionary returned by -" + nameof(AllEntityMethodKeysForEntityTypeDict) + "-"
        )]
        public static List<EntityMethodKey> AllEntityMethodKeysForEntityTypeList(Type entityType) => 
            _allEntityMethodKeysForEntityTypeListCache.GetOrAdd(entityType, t =>
                AllEntityMethodKeysForEntityTypeDict(t).Values.OrderBy(k => k.Method.Name).ToList()
            );


        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataStorage"></param>
        /// <param name="_this"></param>
        /// <param name="foreignKey"></param>
        /// <param name="foreignField">This may actually also be parseable as a <see cref="CompoundKey"/></param>
        /// <param name="retval"></param>
        /// <param name="errorResponse"></param>
        /// <returns></returns>
        [ClassMember(Description =
            "Returns foreign field through foreign key given.\r\n" +
            "\r\n" +
            "Helper method not used directly by -" + nameof(EntityMethodKey) + "- but useful for actually implementation methods in entity classes.\r\n" +
            "\r\n" +
            "For instance, given 'CustomerP.GeographyId' it can look up values like " +
            "'GeographyId.City', 'GeographyId.Province', 'GeographyId.State', 'GeographyId.Country'.\r\n" +
            "\r\n" +
            "This enables easier querying without having to denormalize the tables themselves, like when querying for 'Customer', to do\r\n" +
            "'SELECT Name, Address, City, State, Country'\r\n" +
            "instead of the more laborous\r\n" +
            "'SELECT Geography.Name, Geography.Address, Geography.City, Geography.State, Geography.Country'\r\n" +
            "(the latter approach would have utilized -" + nameof(ForeignKey) + "-)."
        )]
        public static bool TryGetForeignField(IP dataStorage, IP _this, IK foreignKey, IK foreignField, out IP retval, out string errorResponse)
        {
            if (!_this.TryGetPV<string>(foreignKey, out var foreignKeyValue, out errorResponse))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            var s = foreignKey.ToString();
            if (!(s.Length > 2) || !s.EndsWith("Id"))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Invalid " + nameof(foreignField) + " (" + s + "), not 3 characters or longer or does not end with 'Id'";
                return false;
            }
            if (!dataStorage.TryGetP<IP>(s[0..^2], out var collection, out errorResponse))
            {
                // This is actually a referential integrity error and could quality for throwing an exception
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            var IKForeignKeyValue = IKString.FromString(foreignKeyValue);
            if (!collection.TryGetP<IP>(IKForeignKeyValue, out var foreignEntity, out errorResponse))
            {
                // This is actually a referential integrity error and could quality for throwing an exception
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            CompoundKey? cachedCompoundKey = null;
            /// Now how we use <see cref="ARCQuery.Extensions.TryGetP"/> instead of <see cref="IP.TryGetP"/> now.
            return new IKIP(IKForeignKeyValue, foreignEntity).TryGetP(dataStorage, foreignField, out retval, ref cachedCompoundKey, out errorResponse);
        }

        [Enum(
            Description = "Explains type of method signature found (trough reflection) for corresponding entity class.",
            AREnumType = AREnumType.OrdinaryEnum
        )]
        public enum MethodType
        {
            __invalid,
            [EnumMember(Description =
                "Signature like 'bool TryGet{KeyName}(out IP retval, out string errorResponse)'"
            )]
            Simple,

            [EnumMember(Description =
                "Signature like 'bool TryGet{KeyName}(IP dataStorage, IK thisKey, out IP retval, out string errorResponse)'\r\n" +
                "\r\n" +
                "Relevant when the method needs the complete dataStorage, typically for referencing other entities through relations\r\n" +
                "(like for instance when you need to generate Sum for an Order based on OrderLines)." +
                "\r\n" +
                "The 'thisKey' parameter is the objects own key, for instance for Order/42 'thisKey' would be '42'.\r\n" +
                "The 'thisKey' parameter is necessary because the entity will not know its own key" +
                "(so for an Order / OrderLine scenario, it needs to know which key to look for in the OrderLines collection).\r\n"
            )]
            Complex,
        }

        public class EntityMethodKeyException : ApplicationException
        {
            public EntityMethodKeyException(string message) : base(message) { }
            public EntityMethodKeyException(string message, Exception inner) : base(message, inner) { }

        }
    }
}