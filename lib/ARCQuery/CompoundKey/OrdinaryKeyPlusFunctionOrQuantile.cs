﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {
    [Class(Description =
        "A -" + nameof(CompoundKey) + "- containing some function keys or quantile key operating on an existing property of the entity in question.\r\n" +
        "\r\n" +
        "Example 1: 'Customer/SELECT Age.QUARTILE, ...' (see -" + nameof(Quantile.QUARTILE) + "-).\r\n" +
        "Example 2: 'Customer/SELECT DateOfBirth.Year(), ...' (see -" + nameof(FunctionKeyYear) + "-).\r\n" +
        "Example 3: 'Customer/SELECT DateOfBirth.Year().QUARTILE, ...' (combination).\r\n" +
        "\r\n" +
        "Compared to other implementations of -" + nameof(CompoundKey) + "-, " +
        "-" + nameof(TryGetPInternal) + "- has no specific functionality, it just calls -" + nameof(IP.TryGetP) + "-."
    )]
    public class OrdinaryKeyPlusFunctionOrQuantile : CompoundKey {

        public IK Key { get; private set; }
        public override bool TryGetPInternal(IKIP ikip, IP dataStorage, out IP retval, out string errorResponse) =>
            ikip.P.TryGetP(Key, out retval, out errorResponse);

        [ClassMember(Description =
            "Note that calling only this constructor " +
            "without afterwards setting -" + nameof(CompoundKey.FunctionKeys) + "- and / or -" + nameof(CompoundKey.QuantileKey) + "- " +
            "would be meaningless.\r\n"
        )]
        public OrdinaryKeyPlusFunctionOrQuantile(IK key) => Key = key;

        public override string ToString() => Key.ToString() + BaseToString();
    }
}
