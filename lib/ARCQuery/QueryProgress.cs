﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;
using ARCDoc;
using System.Net;

/// <summary>
/// Contains the following classes:
/// <see cref="ARCQuery.Query"/>
/// <see cref="ARCQuery.QueryProgress"/>
/// <see cref="ARCQuery.QueryProgressP"/>
/// </summary>
namespace ARCQuery
{

    [Class(Description =
        "Keeps track of execution of query (that is, a query consisting of multiple -" + nameof(QueryExpression) + "-), and also stores final result.\r\n" +
        "\r\n" +
        "Contains a special version of -" + nameof(ToHTMLSimpleSingle) + "- which is necessary " +
        "in order to present the final result because a query result:\r\n" +
        "1) Is a uniform collection (of identical type), and\r\n" +
        "2) Has a 'location' URL-wise that is very difficult to generate relative links for.\r\n" +
        "Therefore, the standard -" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- method is not very well suited.\r\n" +
        "\r\n" +
        "Recognized for instance by -RQController- in -" + nameof(ARComponents.ARAAPI) + "-.\r\n" +
        "\r\n"
    )]
    public class QueryProgress : PConcurrent
    {

        private IP? _dataStorage;
        [ClassMember(Description =
            "Note how this property will point to the hierarhical level in the data storage from which " +
            "the first -" + nameof(QueryExpression) + "- was found in the query string.\r\n" +
            "For instance, for 'RQ/dt/Order/SELECT *' (in which 'SELECT *' is the first QueryExpressionFound), " +
            "this property will point to 'RQ/dt'.\r\n" +
            "\r\n" +
            "An example of how this property is used is how -" + nameof(ToHTMLSimpleSingle) + "- decides which " +
            "keys are foreign keys for which HTML links should be generated.\r\n" +
            "Example: For 'RQ/dt/Order/SELECT CustomerId, Date, Amount', if 'Customer' is found at the same level as 'Order' " +
            "(found within this property) then CustomerId will show as an HTML link.\r\n" +
            "\r\n" +
            "Can be changed with -" + nameof(QueryExpressionDataStorage) + "-.\r\n"
        )]
        public IP DataStorage
        {
            get => _dataStorage ?? throw new NullReferenceException(nameof(DataStorage) + ". Should have been set at initialization");
            set => _dataStorage = value;
        }

        [ClassMember(Description =
            "This gets updated continously as the different query expressions get evaluated.\r\n" +
            "\r\n" +
            "NOTE: Note how this property is not stored within -" + nameof(Properties) + "- / not included in -" + nameof(IP.DeepCopy) + "- and so on.\r\n" +
            "NOTE: The information stored within -" + nameof(Properties) + "- is only the metadata about how the query progresses, not this actual result."
        )]
        public IEnumerable<IKIP> Result { get; set; } = new List<IKIP>();

        [ClassMember(Description =
            "Override of default interface method -" + nameof(IP.ToJSONSerializeable) + "- is necessary because resulting data is 'hidden' within -" + nameof(Result) + "-."
        )]
        public Dictionary<string, object?> ToJSONSerializeable(int depthRemaining)
        {
            try
            {
                REx.Inc();
                var retvalResult = new Dictionary<string, object?>();
                Result.ForEach(ikip =>
                {
                    // Note that we assume that depthRemaining is 1 or greater now.
                    // (or actually that we are the "top" object / first object called recursively)
                    retvalResult.Add(ikip.Key.ToString(), ikip.P.ToJSONSerializeable(depthRemaining - 1)); /// In order to get consistent results with <see cref="IP.ToJSONSerializeable"/> we must decrement depthRemaining here.
                });

                var retvalQuery = new Dictionary<string, object?>();
                this.ForEach(ikip => retvalQuery.Add(ikip.Key.ToString(), IP.GetValueForJSONSerializer(ikip.P)));

                return new Dictionary<string, object?> {
                    { "Result", retvalResult },
                    { "Query", retvalQuery }
                };
            }
            finally
            {
                REx.Dec();
            }
        }

        public string StrCurrentType
        {
            get => IP.GetPV<string>(QueryProgressP.StrCurrentType);
            set => IP.SetPV(QueryProgressP.StrCurrentType, value);
        }

        public int Limit
        {
            get => IP.GetPV<int>(QueryProgressP.Limit);
            set => IP.SetPV(QueryProgressP.Limit, value);
        }

        public List<QueryExpression> Queries
        {
            get => IP.GetPV<List<QueryExpression>>(QueryProgressP.Queries);
            set => IP.SetPV(QueryProgressP.Queries, value);
        }

        [ClassMember(Description = "The result from calling -" + nameof(QueryExpression.Execute) + "- for each query in -" + nameof(Queries) + "-.")]
        public List<QueryExpressionWithSuggestions> ExecutedQueries { get; set; } = new List<QueryExpressionWithSuggestions>();

        [ClassMember(Description = "TODO: This is somewhat loosely associated.")]
        public QueryExpression? PreviousQueryJustExecuted()
        {
            var q = Queries;
            if (ExecutedQueries.Count == 0) return null;
            return q[ExecutedQueries.Count - 1];
        }

        [ClassMember(Description =
            "TODO: This is somewhat loosely associated.\r\n" +
            "TODO: Note that the CURRENT query (the query being executed 'now') " +
            "TODO: is assumed NOT to have been added to the collection -" + nameof(ExecutedQueries) + "- yet."
        )]
        public QueryExpression? NextQueryToExecute()
        {
            if (ExecutedQueries.Count + 1 >= Queries.Count) return null; // This is the last query.
            return Queries[ExecutedQueries.Count + 1];
        }

        [ClassMember(Description =
            "Alternative ('override') to -" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "-.\r\n" +
            "\r\n" +
            "Present result as a uniform table with all keys present.\r\n" +
            "Creates only links to 'ordinary' entity objects (links from keys).\r\n" +
            "\r\n" +
            "Returns detailed log and exception information in addition to result if considered relevant.\r\n" +
            "\r\n" +
            "NOTE: The mere existence of this method is not sufficient in order to avoid " +
            "-" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- being called.\r\n" +
            "(extension method will be chosen anyway if instance is of type -" + nameof(IP) + "-.\r\n" +
            "See " + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- for more details about this issue.\r\n" +
            "\r\n" +
            "TODO: Method is quite long (Jul 2020). Consider breaking up into sub-methods."
        )]
        public string ToHTMLSimpleSingle(bool prepareForLinkInsertion = false, List<IKCoded>? linkContext = null)
        {
            if (_dataStorage == null)
            {
                /// HACK: Hack relevant when querying like
                /// HACK: https://localhost:44343/api/RQ/Controller/api/RQController/~std/QueryProgressDetails
                /// HACK: That is, when an instance of this class has been created 
                /// HACK: (typically by <see cref="PropertyStreamLine.TryStore"/>)
                /// HACK: without an actual query context
                /// 
                /// HACK: Resolution: Just use the ordinary presenter
                /// NOTE: CAREFUL, do not call <see cref="ARCDoc.Extensions.ToHTMLSimpleSingle"/>, it will just call us back.
                return ARCDoc.Extensions.ToHTMLSimpleSingleInternal(this, prepareForLinkInsertion, linkContext);
            }

            var retval = new StringBuilder();

            retval.Append(GetHTMLLinksToIndividualQueryParts(linkContext, Queries, countOfQueriesThatFailedToParse: 0, ExecutedQueries, IP.GetPV<bool>(QueryProgressP.CreateHints)));

            var resultAsList = Result.ToList(); // Avoids repeated evaluation in order to Count() below
            var queries = Queries;

            // Find all unique keys used (necessary in order to build up table heading)
            /// TODO: This result may be known, for instance if coming from <see cref="QueryExpressionPivot"/>
            /// TODO: Therefore, add keys as parameter instead?
            var keys = resultAsList.Take(Limit).SelectMany(e => e.P.Keys).Distinct().ToList(); // NOTE: This is somewhat time consuming
            if (keys.Count > 1000)
            {
                /// TODO: Create configurable parameter for this, like a QueryExpressionMaxKeys or similar
                /// TODO: See <see cref="QueryExpressionHint"/> or <see cref="QueryExpressionStrict"/> for examples of similar functionality.
                IP.SetPV(QueryProgressP.TerminateReason,
                    "Query executed with a result of " + resultAsList.Count + " items but " +
                    "too many keys (too many columns) resulted (" + keys.Count + " keys was found).\r\n" +
                    "The limit (chosen somewhat arbitrarily) is 1000.\r\n" +
                    "The system is therefore unable to generate a HTML table.\r\n" +
                    "Some of the keys are:\r\n" +
                    string.Join("\r\n", keys.Take(50).Select(k => WebUtility.HtmlEncode(k.ToString())))
                );
            }

            retval.Append(GetGeneralHTMLInformationAboutQuery(queries, resultAsList));

            if (IP.ContainsKey(nameof(QueryProgressP.TerminateReason)))
            {
                // No point in attempting to show a result
                return retval.ToString();
            }

            // Generate the actual result
            keys = OrderAndReconstructKeys(queries, StrCurrentType, keys);

            retval.Append("<table>\r\n<tr><th>" + StrCurrentType + "Id</th>");
            keys.ForEach(k => retval.Append("<th>" + WebUtility.HtmlEncode(k.ToString()) + "</th>"));
            retval.Append("</tr>\r\n");

            // Precalculate information for resolving foreign keys
            var foreignKeysCollections = keys. // Will contain whole collection of foreign items for this key
                Append(IKString.FromCache(StrCurrentType + "Id")). // Hack in order to generate link also for left-most column (the id column). Information is stored as last item in collection.
                Select(ik =>
                {
                    var s = ik.ToString();
                    if (!s.EndsWith("Id")) return null;

                    var type =
                        (
                            !(ik is PK pk) ||
                            !pk.TryGetA<PKRelAttribute>(out var pkrel) ||
                            !pkrel.IP.TryGetPV<Type>(PKRelAttributeP.ForeignEntity, out var temp)
                        ) ?
                        s[0..^2] :
                        temp.ToStringVeryShort();

                    if (!DataStorage.TryGetP<IP>(IKString.FromString(type), out var collection)) return null;
                    return collection;
                }).ToList();

            var foreignTypes = keys. // Will contain items like 'Customer' or 'Order'.
                Append(IKString.FromCache(StrCurrentType + "Id")). // Hack in order to generate link also for left-most column (the id column). Information is stored as last item in collection.
                Select(ik =>
                {
                    var s = ik.ToString();
                    if (!s.EndsWith("Id")) return null;

                    return
                        (
                            !(ik is PK pk) ||
                            !pk.TryGetA<PKRelAttribute>(out var pkrel) ||
                            !pkrel.IP.TryGetPV<Type>(PKRelAttributeP.ForeignEntity, out var temp)
                        ) ?
                        s[0..^2] :
                        temp.ToStringVeryShort();
                }).ToList();

            var pkHtmlAttributes = keys. /// Will contain collection of <see cref="PKHTMLAttribute"/>
                Append(IKString.FromCache(StrCurrentType + "Id")). // Will be ignored. Make code familiar to code above
                Select(k =>
                {
                    if (!(k is PK pk)) return null;
                    if (!pk.TryGetA<PKHTMLAttribute>(out var pkHTMLAttribute)) return null;
                    return pkHTMLAttribute;
                }).ToList();

            var keysCardinalityMany = keys.Select(key => key is PK pk && pk.Cardinality.IsMultiple()).
            Append(false). // Hack (see Appends above)
            ToList();

            // Find all columns to sum. Initialize sum to 0.
            // Note that we do not check whether the column actual contain a valid value now
            var sums = keys.
                Append(IKString.FromCache(StrCurrentType + "Id")). // Will be ignored. Make code familiar to code above
                Select(key =>
                !IP.TryGetPV<List<QueryExpressionSum.Field>>(QueryProgressP.Sum, out var sum) ||
                !sum.Any(s => s.IsWildcardField || s.Name.Equals(key)) ?
                    (double?)null :
                    0d
            ).ToList();

            var rootLevel = string.Join("", Enumerable.Repeat("../",
                Queries.Count + // Necessary adjustment because each query expression results in one hierarchical level deeper down as seen from the browser
                IP.GetPV<int>(QueryProgressP.LinkAdjustment, 0)) /// Adjustment epxlicit given by <see cref="QueryExpressionDataStorage"/>
            );

            // Ensures that foreign keys are actually linking
            var linkOrJustValueGenerator = new Func<int, string, IKIP?, string>((i, retval, ikip) =>
            {
                if (sums[i] != null)
                {
                    if (
                        ikip == null ||
                        !ikip.P.TryGetPV<double>(keys[i], out var d)
                    )
                    {
                        sums[i] = double.NaN; // Give up, and mark as invalid
                    }
                    else
                    {
                        sums[i] = sums[i] + d;
                    }
                }
                if (foreignKeysCollections[i] != null)
                {
                    // For this key we should generate links, but check for each and every id whether the entity
                    // "on the other side" actually exists.
                    if (keysCardinalityMany[i] && ikip != null)
                    {
                        /// NOTE: This is really support for <see cref="ARConcepts.ManyToManyRelations"/> without a 'third table'.
                        /// NOTE: One could ask if we go a little too far
                        /// NOTE: here, it might be more reasonable to just add a third entity type like in the traditional
                        /// NOTE: database manner in order to support many-to-many relations.

                        // Note: Original 'retval' was now probably unnecessary to have asked for...
                        if (ikip.P.TryGetPV<List<string>>(keys[i], out var list))
                        {
                            return string.Join(", ", list.Select(retval =>
                            {
                                if (foreignKeysCollections[i]!.TryGetP<IP>(IKString.FromString(retval), out _))
                                {
                                    return "<a href=\"" +
                                        rootLevel + WebUtility.UrlEncode(foreignTypes[i]!) + "/" +
                                            // This was preferred method (until 10 Mar 2021) but does not work for complicated id's
                                            // WebUtility.UrlEncode(retval) +
                                            // New encoding, after 10 Mar 2021. This is safer but will not work for very long keys (over 250 characters)
                                            IKCoded.Encode(retval) +
                                        "\">" +
                                        WebUtility.HtmlEncode(retval) + "</a>";
                                }
                                return WebUtility.HtmlEncode(retval).Replace("\r\n", "<br>\r\n");
                            }));
                        }
                    }
                    else
                    {
                        if (foreignKeysCollections[i]!.TryGetP<IP>(IKString.FromString(retval), out _))
                        {
                            return "<a href=\"" +
                                rootLevel + WebUtility.UrlEncode(foreignTypes[i]!) + "/" +
                                    // This was preferred method (until 10 Mar 2021) but does not work for complicated id's
                                    // WebUtility.UrlEncode(retval) +
                                    // New encoding, after 10 Mar 2021. This is safer but will not be correct for very long keys (over 250 characters)
                                    IKCoded.Encode(retval) +
                                "\">" +
                                WebUtility.HtmlEncode(retval) + "</a>";
                        }
                    }
                }
                else if (pkHtmlAttributes[i] != null)
                {
                    return pkHtmlAttributes[i]!.Encode(retval);
                }
                return WebUtility.HtmlEncode(retval).Replace("\r\n", "<br>\r\n");
            });

            // Find which columns to right align (when all elements parse as double or timespan)
            var rightAligns = keys.Select(key => resultAsList.Take(Limit).AsParallel().All(ikip =>
                !ikip.P.TryGetP<IP>(key, out var p) ||
                p is PValue<double> ||
                UtilCore.DoubleTryParse(p.GetV(defaultValue: "0"), out _) ||
                p is PValue<TimeSpan> ||
                UtilCore.TimeSpanTryParse(p.GetV(defaultValue: "00:00"), out _)
            )).ToList();

            resultAsList.Take(Limit).ForEach(ikip =>
            {
                // First show the primary key:
                // Note how objects do not contain their own key, we therefore have to list ikip.Key separately
                retval.Append("<tr><td>" + linkOrJustValueGenerator(
                    foreignKeysCollections.Count - 1,  // Hack in order to generate link also for left-most column (the id column). Information is stored as last item in collection.
                    ikip.Key.ToString(),
                    null) + "</th>");
                // Then show the rest of the fields:
                for (var i = 0; i < keys.Count; i++)
                {
                    retval.Append("<td" + (rightAligns[i] ? " align=right" : "") + ">" + ((!ikip.P.TryGetPV<string>(keys[i], out var s) || "".Equals(s)) ?
                        "&nbsp;" :
                        linkOrJustValueGenerator(i, s, ikip)
                    ) +
                    "</td>");
                }
                retval.Append("</tr>\r\n");
            });
            if (sums.Any(s => s != null))
            {
                retval.Append("<tr><td><b>Sum</b></td>");
                for (var i = 0; i < keys.Count; i++)
                {
                    retval.Append("<td align=right>" + (sums[i] == null ? "&nbsp;" :
                        // TODO: Use common method for transforming double to string
                        ((double)sums[i]!).ToString("0.00").Replace(",", ".")
                    ) + "</td>");

                }
                retval.Append("</tr>\r\n");
            }
            retval.Append("</table>\r\n");
            return retval.ToString();
        }


        [ClassMember(Description =
            "Generates general HTML information about the query like:\r\n" +
            "1) Warning if no result.\r\n" +
            "2) Count of items in result\r\n" +
            "3) Description of query (-" + nameof(DocumentatorP._Description) + "-).\r\n" +
            "4) -" + nameof(QueryProgressP.TerminateReason) + " if any.\r\n" +
            "5) -" + nameof(PP.ExceptionText) + " if any.\r\n" +
            "6) -" + nameof(QueryProgressP.AllLogEntriesForThisQuery) + " (whenever no result).\r\n" +
            "7) Warning if passed -" + nameof(QueryProgressP.Limit) + "- (typically when more than 1000 items in result).\r\n"
        )]
        private string GetGeneralHTMLInformationAboutQuery(List<QueryExpression> queries, List<IKIP> result)
        {
            var retval = new StringBuilder();

            if (IP.TryGetPV<string>(QueryProgressP.Title, out var title))
            {
                retval.Append("<h1>" + WebUtility.HtmlEncode(title) + "</h1>\r\n");
            }

            if (result.Count == 0)
            {
                retval.AppendLine("<p style=\"color:red\">No result. See details below for possible causes</p>");
            }
            else
            {
                retval.Append("<p>" + result.Count + " items</p>");
            }

            if (result.Count == 0 && IP.TryGetPV<string>(DocumentatorP._Description, out var s))
            {
                retval.AppendLine("<h1>" + nameof(DocumentatorP._Description) + "</h1>");
                retval.AppendLine("<p>" + WebUtility.HtmlEncode(s).Replace("\r\n", "<br>\r\n") + "</p>");
            }

            var showLog = false;
            // In case of non ordinary execution, some or all of these keys may be set:
            if (IP.TryGetPV<string>(QueryProgressP.TerminateReason, out s))
            { // NOTE: Using explicit generic type parameter <string> here in order to get rid of presumed erroneus compiler warning about nullability  (Visual Studio 16.6.3)
                retval.AppendLine("<p>Query was prematurely terminated</p>");
                retval.AppendLine("<h1>" + nameof(QueryProgressP.TerminateReason) + "</h1>");
                retval.AppendLine("<p>" + WebUtility.HtmlEncode(s).Replace("\r\n", "<br>\r\n") + "</p>");
                showLog = true;
            }

            if (IP.TryGetPV<string>(PP.ExceptionText, out s))
            { // NOTE: Using explicit generic type parameter <string> here in order to get rid of presumed erroneus compiler warning about nullability (Visual Studio 16.6.3)
                retval.AppendLine("<h1>" + nameof(PP.ExceptionText) + "</h1>");
                retval.AppendLine("<p>" + WebUtility.HtmlEncode(s).Replace("\r\n", "<br>\r\n") + "</p>");
                showLog = true;
            }

            if ((showLog || result.Count == 0) && IP.TryGetPV<string>(QueryProgressP.AllLogEntriesForThisQuery, out s))
            { // NOTE: Using explicit generic type parameter <string> here in order to get rid of presumed erroneus compiler warning about nullability (Visual Studio 16.6.3)
                retval.AppendLine("<h1>" + nameof(QueryProgressP.AllLogEntriesForThisQuery) + "</h1>");
                retval.AppendLine("<p>" + WebUtility.HtmlEncode(s).Replace("\r\n", "<br>\r\n") + "</p>");
            }

            if (result.Count > Limit)
            {
                retval.AppendLine(
                    "<p " +
                        "style=\"color:red\"" +  // It is very important to emphasize this
                    ">" +
                    "NOTE: Only " + Limit + " elements of total " + result.Count + " shown. " +
                    "Use " +
                    "<a href=\"./" + WebUtility.HtmlEncode(queries[^1].ToString()) + "/SKIP 0/TAKE 1000\">SKIP and TAKE</a>" +
                    ", or change LIMIT. " +
                    (queries.Any(q => q is QueryExpressionShuffle) ? "" : (
                    "Try also " +
                    "<a href=\"./" + WebUtility.HtmlEncode(queries[^1].ToString()) + "/SHUFFLE\">SHUFFLE</a>" +
                    "."
                    )) +
                    "\r\n</p>");
            }
            return retval.ToString();
        }

        [ClassMember(Description =
            "Part 1:\r\n" +
            "Order keys either:\r\n" +
            "a) According to last -" + nameof(QueryExpressionSelect) + "-, or (if no SELECT found)\r\n" +
            "b) Alphabetically.\r\n" +
            "\r\n" +
            "Part 2:\r\n" +
            "Will also attempt to reconstruct keys to the more strongly typed -" + nameof(PK) + "-- based on keys found for input parameter strCurrentType.\r\n" +
            "Because queries may be in multiple steps, the original strongly typed key may not be carried through the different -" + nameof(QueryExpression) + "-s.\r\n" +
            "\r\n" +
            "Example: For 'Customer/All', 'Customer/SELECT *', 'Customer/WHERE ...' we would have the original key already, because the " +
            "query result will consist of the original (whole) Customer-objects.\r\n" +
            "But for a query like 'Customer/SELECT Name, Address' new temporary objects are created, usually with -" + nameof(IKString) + "- as type.\r\n" +
            "\r\n" +
            "The original -" + nameof(PK) + "- is necessary in order for instance for -" + nameof(PKHTMLAttribute) + "- to do its work.\r\n"
        )]
        public static List<IK> OrderAndReconstructKeys(List<QueryExpression> queries, string strCurrentType, List<IK> keys)
        {
            // Order keys
            if (queries.LastOrDefault(q => q is QueryExpressionSelect) is QueryExpressionSelect select)
            {
                // Use fields given in SELECT. Note that this may or may not be relevant
                // (SELECT may have been followed by something else, PIVOT for instance, in which case it no longer applies)
                var h = keys.ToHashSet();
                var newOrder = new List<IK>();
                select.Fields.ForEach(f =>
                {
                    if (h.Contains(f.NameAs))
                    {
                        newOrder.Add(f.NameAs);
                        h.Remove(f.NameAs);
                    }
                });
                newOrder.AddRange(h.OrderBy(s => s.ToString()).ToList());
                keys = newOrder;
            }
            else
            {
                // We have no hints. Order alphabetically in order to at least make it consistens
                keys = keys.OrderBy(k => k.ToString()).ToList();
            }

            var pkKeys = PK.GetAllPKForEntityTypeString(strCurrentType).Select(pk => (IK)pk).ToHashSet();
            keys = keys.Select(k =>
                /// Try to reconstruct to the original <see cref="PK"/> keys.
                pkKeys.TryGetValue(k, out var pk) ? pk : k
            ).ToList();

            return keys;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="linkContext">
        /// The complete link context, including query expressions.<br>
        /// </param>
        /// <param name="queries">
        /// The actual queries 
        /// </param>
        /// <param name="countOfQueriesThatFailedToParse"/>
        /// Necessary in order to generate correct number of ..-items in linking relatively upwards in the hierarchy<br>
        /// Only relevant when <paramref name="executedQueries"/> is not given.
        /// </param>
        /// <param name="createHints">
        /// Only relevant if <paramref name="executedQueries"/> parameter is also given. 
        /// TRUE means that <see cref="QueryExpressionWithSuggestions.Previous"/> and <see cref="QueryExpressionWithSuggestions.Next"/> shall be shown.
        /// </param>
        /// <param name="executedQueries">
        /// Only relevant if query has actually been executed. If not yet executed, links are shown without any result count or suggestions
        /// </param>
        /// <returns></returns>
        [ClassMember(Description =
            "Is able to generate the following:\r\n" +
            "\r\n" +
            "1) Links to all items of query, including any non--" + nameof(QueryExpression) + "- parts at start of query.\r\n" +
            "\r\n" +
            "2) Hints for modifying query, from -" + nameof(QueryExpressionWithSuggestions) + "- (linked to -" + nameof(QueryProgressP.CreateHints) + "-).\r\n" +
            "\r\n" +
            "3) An HTML <Textarea>-field for editing the query (as an alternative to editing the URL field in the browser).\r\n"
        )]
        public static string GetHTMLLinksToIndividualQueryParts(
            List<IKCoded>? linkContext,
            IEnumerable<QueryExpression> queries,
            int countOfQueriesThatFailedToParse,
            IEnumerable<QueryExpressionWithSuggestions>? executedQueries = null,
            bool createHints = false
        )
        {
            var retval = new StringBuilder();

            // Create HTML <Textarea> field for editing query
            if (linkContext != null)
            {
                retval.Append(
                    //"<form " +
                    //    "method=\"get\", " +
                    //    "enctype = \"application/x-www-form-urlencoded\", " +
                    //    "accept-charset=\"utf-8\", " +
                    //"\">\r\n" +
                    "<textarea id=\"query\" rows=\"" + (linkContext.Count + 1) + "\" " +
                    "cols = \"300\"" + // "Extremely" wide box helps to reduce misunderstandings about line breaks contra word wrap
                    ">" +
                    string.Join("\r\n", linkContext.Select(l => WebUtility.HtmlEncode(l.Unencoded.ToString()))) +
                    "</textarea>\r\n" +
                    "<input type=\"submit\" text=\"Execute\" onclick=\"execute()\">" +
                    "<script>\r\n" +
                    "function execute() {\r\n" +
                    "  var query = document.getElementById(\"query\").value.split(\"\\n\").filter(function(el) { return el != \"\"; });\r\n" +
                    "  var url = \"\";\r\n" +
                    "  for (var i = 0; i < query.length; i++) {\r\n" +
                    "     url = url + encodeURI(\r\n" +
                    "      query[i].\r\n" +
                    "      trim()" +
                    "      " + AdditionalJavascriptUrlEncoder + "\r\n" +
                    "     );\r\n" +
                    "     if (i < (query.length - 1)) {\r\n" +
                    "        url = url + \"/\";\r\n" +
                    "     }\r\n" +
                    "  }\r\n" +
                    // "  alert(\"Hello: \" + url);\r\n" +
                    "  window.location.assign(window.location.href + \"/\" + \"" + string.Join("/", Enumerable.Repeat("..", linkContext.Count)) + "\" + \"/\" + url);\r\n" +
                    "}\r\n" +
                    "</script>\r\n"
                // "</form>\r\n"
                ); ;
            }

            // STEP 1, list the current query, with suggestions for potential new queries
            var l = queries.ToList();
            retval.Append("<p>\r\n");

            /// STEP 1a, Link to non-<see cref="QueryExpression"/> part of link context
            /// This is everything found "outside of" of <see cref="Query"/>, that is, before <see cref="Query.ExecuteAll"/> is called.
            // This could typically be the key Order in the query "api/RQ/Order/WHERE Value > 1000 NOK/REL Customer/ORDER BY LastName, FirstName/TAKE 50"
            // (The point of " - l.Count" is to stop before the query expression part)
            if (linkContext != null && linkContext.Count - l.Count > 0)
            {
                retval.Append("<a href=\"" +
                    string.Join("/", Enumerable.Repeat("..", linkContext.Count - 1 + countOfQueriesThatFailedToParse)) +
                    ((linkContext.Count - 1) == 0 ? "" : "/") +
                    "\">[ROOT]</a>&nbsp;&nbsp;"
                );
                for (var i = 0; i < linkContext.Count - l.Count; i++)
                {
                    retval.Append("<a href=\"" +
                        string.Join("/", Enumerable.Repeat("..", linkContext.Count - i - 1 + countOfQueriesThatFailedToParse)) +
                        "/" +
                        linkContext[i].Encoded + "\">" + WebUtility.HtmlEncode(linkContext[i].Unencoded.ToString()) + "</a>&nbsp;&nbsp;"
                    );
                }
            }

            var e = executedQueries?.ToList();
            /// STEP 1b, link to all the <see cref="QueryExpression"/>s found (everything executed by <see cref="Query.ExecuteAll"/>)
            // This would typically be everything following the key Order in the query "api/RQ/Order/WHERE Value > 1000 NOK/REL Customer/ORDER BY LastName, FirstName/TAKE 50"
            for (var i = 0; i < l.Count; i++)
            {
                var executedQuery = e == null ? null : (e.Count <= i ? null : e[i]);

                if (createHints && executedQuery?.Previous != null)
                { // Show '<' for previous
                    retval.Append("<a href=\"" +
                        ((i == (l.Count - 1) && countOfQueriesThatFailedToParse == 0) ?
                        "./" :
                        string.Join("", Enumerable.Repeat("../", l.Count - i - 1 + countOfQueriesThatFailedToParse))) +
                        WebUtility.UrlEncode(executedQuery.Previous.ToString()).Replace("+", "%20") +
                        (i == (l.Count - 1) ? "" : "/") +
                        string.Join("/", l.Skip(i + 1).Select(r => WebUtility.UrlEncode(r.ToString()).Replace("+", "%20"))) +
                        "\">&lt;</a>" + "&nbsp;\r\n"
                    );
                }

                retval.Append( // Show link to this query
                    (executedQuery != null ? ("<span title=\"" + executedQuery.ResultCount + " rows\">") : "") +
                    "<a href=\"" +
                        ((i == (l.Count - 1) && countOfQueriesThatFailedToParse == 0) ?
                        "./" :
                        string.Join("", Enumerable.Repeat("../", l.Count - i - 1 + countOfQueriesThatFailedToParse))) +
                    WebUtility.UrlEncode(l[i].ToString()).Replace("+", "%20") +
                    "\">" + WebUtility.HtmlEncode(l[i].ToString()) +
                    "</a>" +
                    (executedQuery != null ? "</span>" : "") +
                    "&nbsp;\r\n");

                if (createHints && executedQuery?.Next != null)
                { // Show '>' for next
                    retval.Append("<a href=\"" +
                        ((i == (l.Count - 1) && countOfQueriesThatFailedToParse == 0) ?
                        "./" :
                        string.Join("", Enumerable.Repeat("../", l.Count - i - 1 + countOfQueriesThatFailedToParse))) +
                        WebUtility.UrlEncode(executedQuery.Next.ToString()).Replace("+", "%20") +
                        (i == (l.Count - 1) ? "" : "/") +
                        string.Join("/", l.Skip(i + 1).Select(r => WebUtility.UrlEncode(r.ToString()).Replace("+", "%20"))) +
                        "\">&gt;</a>" + "&nbsp;\r\n"
                    );
                }
                retval.Append("&nbsp;");
            }
            retval.Append("</p>\r\n");

            return retval.ToString();
        }

        /// Unnecessary, we use <see cref="PropertyStreamLine.Decode"/> instead.
        //[ClassMember(Description =
        //    "Additional URL decoder for characters that are difficult to pass in URLs.\r\n" +
        //    "\r\n" +
        //    "These characters have typically been encoded by -" + nameof(AdditionalJavascriptUrlEncoder) + "-.\r\n" +
        //    "\r\n" +
        //    "Note: Additional URL decoding and encoding is a pragmatic hack in order to be able to use HTTP GET also for complex queries."
        //)]
        //public static string AdditionalUrlDecode(string s) => s.Replace("|PLUS|", "+");
        [ClassMember(Description =
            "Returns the Javascript code for converting characters that are difficult to pass in URLs " +
            "to a format understood by -" + nameof(PropertyStreamLine.Decode) + "-.\r\n" +
            "\r\n" +
            "Note: Additional URL decoding and encoding is a pragmatic hack in order to be able to use HTTP GET also for complex queries, " +
            "like those involving -" + nameof(BinaryOperatorKey) + "-.\r\n" +
            "\r\n"
        // "See also -" + nameof(AdditionalJavascriptUrlEncoder) + "-, -" + nameof(AdditionalUrlEncode) + "-, -" + nameof(AdditionalUrlDecode) + "-.\r\n"
        )]
        public static string AdditionalJavascriptUrlEncoder =
            @".replace(/\+/g," + "\"0x002B\")" +
            @".replace(/\#/g," + "\"0x0023\")" +
            // @".replace(/\*/g," + "\"0x002A\")" + Asterisk is assumed to not cause trouble in URLs
            @".replace(/\//g," + "\"0x002F\")"
         ;
    }

    [Enum(
        Description = "Describes class -" + nameof(QueryProgress) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum QueryProgressP
    {
        __invalid,

        [PKType(Description =
            "The title to be used in the final presentation of the query result.\r\n" +
            "\r\n" +
            "Specified through -" + nameof(QueryExpressionTitle) + "- and stored in -" + nameof(QueryProgressP.Title) + "-." +
            "\r\n"
        )]
        Title,

        [PKType(
            Description =
                "Specify for which fields to provide a sum.\r\n" +
                "\r\n" +
                "Specified through -" + nameof(QueryExpressionSum) + "- and stored in -" + nameof(QueryProgressP.Sum) + "-." +
                "\r\n",
            Type = typeof(QueryExpressionSum.Field), Cardinality = Cardinality.WholeCollection
        )]
        Sum,

        [PKType(Type = typeof(QueryExpression), Cardinality = Cardinality.WholeCollection)]
        Queries,

        [PKType(
            Description =
                "Normally the -" + nameof(ARCCore.Extensions.ToStringVeryShort) + "- representation of the type of objects " +
                "in the -" + nameof(QueryProgress.Result) + "- collection.\r\n" +
                "\r\n" +
                "If the collection is a result of -" + nameof(QueryExpressionPivot) + "- or similar however, " +
                "then the value will be something like 'Row' instead, that is, a name describing the data rather then " +
                "the formal type (which would typically be typeof(PRich) or similar.\r\n" +
                "TODO: Elaborate on this.",
            IsObligatory = true
        )]
        StrCurrentType,

        [PKType(
            Description =
                "Describes -" + nameof(Strictness) + "- ON / OFF when executing query. " +
                "\r\n" +
                "Specified through -" + nameof(QueryExpressionStrict) + "- and stored in -" + nameof(QueryProgressP.CurrentStrictness) + "-.",
            Type = typeof(Strictness),
            DefaultValue = Strictness.ON
        )]
        CurrentStrictness,

        [PKType(
            Description =
                "Describes if hints are to be generated (as query progresses) for new queries. " +
                "\r\n" +
                "Specified through -" + nameof(QueryExpressionHint) + "- and stored in -" + nameof(QueryProgressP.CreateHints) + "-.",
            Type = typeof(bool),
            DefaultValue = true
        )]
        CreateHints,

        [PKType(
            Description =
                "Describes if cache is to be used or not.\r\n" +
                "\r\n" +
                "Specified through -" + nameof(QueryExpressionCache) + "- and stored in -" + nameof(QueryProgressP.UseCache) + "-.\r\n" +
                "\r\n" +
                "NOTE: Caching in itself is not implemented in AgoRapide as of Jul 2020",
            Type = typeof(bool),
            DefaultValue = false
        )]
        UseCache,

        [PKType(
            Description =
                "Experimental: Adjustment to be done by -" + nameof(QueryProgress) + "-.- " + nameof(QueryProgress.ToHTMLSimpleSingle) + "- " +
                "(when it ascertains how to go back to root level).\r\n" +
                "\r\n" +
                "Set by -" + nameof(QueryExpressionDataStorage) + "-.",
            Type = typeof(int)
        )]
        LinkAdjustment,

        [PKType(
            Description =
                "Limits the final HTML presentation of results (limits the number of elements being presented). " +
                "Does not affect the query in itself. Used in order to avoid building huge HTML pages which will not be consumed anyway.\r\n" +
                "Specified through -" + nameof(QueryExpressionLimit) + "- and stored in -" + nameof(QueryProgressP.Limit) + "-." +
                "\r\n" +
                "See also -" + nameof(QueryExpressionTake) + "- which takes a specific number of elements from the current query result.\r\n",
            Type = typeof(int),
            DefaultValue = 1000
        )]
        Limit,

        [PKType(Description =
            "The reason for a query being terminated. This property should contain a user friendly message about the problem and how to fix it."
        )]
        TerminateReason,

        AllLogEntriesForThisQuery
    }
}