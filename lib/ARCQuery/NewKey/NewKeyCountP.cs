﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery {

    [Class(Description = 
        "Returns the number of properties for a given entity object.\r\n" +
        "\r\n" +
        "See also -" + nameof(NewKeyCountPRec) + "-, recursive variant of -" + nameof(NewKeyCountP) + "-.\r\n"
    )]
    public class NewKeyCountP : NewKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'CountP()'\r\n" +
            "Examples:\r\n" +
            "'Order/SELECT *, CountP(), CountPRec()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(NewKeyCountP));

        public override bool TryGetPInternal(IKIP ikip, IP dataStorage, out IP retval, out string errorResponse) {
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            retval = new PValue<int>(ikip.P.Count());
            return true;
        }

        public new static NewKeyCountP Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidNewKeyCountPException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out NewKeyCountP retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out NewKeyCountP retval, out string errorResponse) =>
            TryParseSingleWord(value, "countp()", () => new NewKeyCountP(), () => SyntaxHelp, out retval, out errorResponse);
        public NewKeyCountP() : base() { }
        public override string ToString() => "CountP()";
        public class InvalidNewKeyCountPException : ApplicationException {
            public InvalidNewKeyCountPException(string message) : base(message) { }
            public InvalidNewKeyCountPException(string message, Exception inner) : base(message, inner) { }
        }
    }
}
