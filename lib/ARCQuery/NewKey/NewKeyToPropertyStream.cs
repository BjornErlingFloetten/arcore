﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery {

    [Class(Description =
        "Returns the -" + nameof(ARConcepts.PropertyStream) + "- representation of the given object.\r\n" +
        "\r\n" +
        "(In programming terms the property stream representation is the serialization representation of the given object).\r\n" +
        "\r\n" +
        "Works by calling -" + nameof(IP.ToPropertyStream) + "-.\r\n" +
        "\r\n" +
        "WARNING: Will generate huge amount of data if called for the top-level of a typical hierarchical data storage.\r\n"
    )]
    public class NewKeyToPropertyStream : NewKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'ToPropertyStream()'\r\n" +
            "Examples:\r\n" +
            "'Order/SELECT ToPropertyStream()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(NewKeyToPropertyStream));

        public override bool TryGetPInternal(IKIP ikip, IP dataStorage, out IP retval, out string errorResponse) {
            //try {
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            retval = new PValue<List<string>>(ikip.P.ToPropertyStream().ToList());
            return true;
        }

        public new static NewKeyToPropertyStream Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidNewKeyToPropertyStreamException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out NewKeyToPropertyStream retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out NewKeyToPropertyStream retval, out string errorResponse) =>
            TryParseSingleWord(value, "topropertystream()", () => new NewKeyToPropertyStream(), () => SyntaxHelp, out retval, out errorResponse);
        public NewKeyToPropertyStream() : base() { }
        public override string ToString() => "ToPropertyStream()";
        public class InvalidNewKeyToPropertyStreamException : ApplicationException {
            public InvalidNewKeyToPropertyStreamException(string message) : base(message) { }
            public InvalidNewKeyToPropertyStreamException(string message, Exception inner) : base(message, inner) { }
        }
    }
}
