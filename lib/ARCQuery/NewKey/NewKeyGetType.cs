﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery {

    [Class(Description =
        "Returns the internal C# object type of the given object.\r\n" +
        "\r\n" +
        "Probably most useful for debugging purposes only.\r\n"
    )]
    public class NewKeyGetType : NewKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'GetType()'\r\n" +
            "Examples:\r\n" +
            "'Order/SELECT GetType()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(NewKeyGetType));

        public override bool TryGetPInternal(IKIP ikip, IP dataStorage, out IP retval, out string errorResponse) {
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            retval = new PValue<Type>(ikip.P.GetType());
            return true;
        }

        public new static NewKeyGetType Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidNewKeyGetTypeException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out NewKeyGetType retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out NewKeyGetType retval, out string errorResponse) =>
            TryParseSingleWord(value, "gettype()", () => new NewKeyGetType(), () => SyntaxHelp, out retval, out errorResponse);
        public NewKeyGetType() : base() { }
        public override string ToString() => "GetType()";
        public class InvalidNewKeyGetTypeException : ApplicationException {
            public InvalidNewKeyGetTypeException(string message) : base(message) { }
            public InvalidNewKeyGetTypeException(string message, Exception inner) : base(message, inner) { }
        }
    }
}
