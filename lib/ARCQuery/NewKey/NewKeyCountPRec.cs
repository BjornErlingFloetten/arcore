﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery {

    [Class(Description =
        "Returns the number of properties for a given entity object.\r\n" +
        "Operates recursively, that is, sums up recursively properties also for sub-properties.\r\n" +
        "\r\n" +
        "See also -" + nameof(NewKeyCountP) + "-, non-recursive variant of -" + nameof(NewKeyCountPRec) + "-.\r\n"
    )]
    public class NewKeyCountPRec : NewKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'CountPRec()'\r\n" +
            "Examples:\r\n" +
            "'Order/SELECT *, CountPRec(), CountP()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(NewKeyCountPRec));

        public override bool TryGetPInternal(IKIP ikip, IP dataStorage, out IP retval, out string errorResponse) {
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            retval = new PValue<long>(CountPRec(ikip.P));
            return true;
        }

        [ClassMember(Description =
            "Counts recursively the number of properties for a given entity object."
        )]
        public static long CountPRec(IP ip) {
            try {
                REx.Inc(); // Guard against properties being backwards linked (structure not DAG)
                var retval = 0L;
                ip.ForEach(p => {
                    retval++;
                    retval += CountPRec(p.P);
                });
                return retval;
            } finally {
                REx.Dec();
            }
        }

        public new static NewKeyCountPRec Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidNewKeyCountPRecException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out NewKeyCountPRec retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out NewKeyCountPRec retval, out string errorResponse) =>
            TryParseSingleWord(value, "countprec()", () => new NewKeyCountPRec(), () => SyntaxHelp, out retval, out errorResponse);
        public NewKeyCountPRec() : base() { }
        public override string ToString() => "CountPRec()";
        public class InvalidNewKeyCountPRecException : ApplicationException {
            public InvalidNewKeyCountPRecException(string message) : base(message) { }
            public InvalidNewKeyCountPRecException(string message, Exception inner) : base(message, inner) { }
        }
    }
}
