﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {
    [Class(Description =
        "Describes if hints are to be generated (as query progresses) for potential new queries.\r\n" +
        "\r\n" +
        "Specified through -" + nameof(QueryExpressionHint) + "- and stored in -" + nameof(QueryProgressP.CreateHints) + "-." +
        "\r\n" +
        "Does not affect query result in itself, only its presentation\r\n" +
        "\r\n" +
        "Set to OFF if hints are not needed (for instance if consumer of query is not a human). This will slightly improve performance.\r\n" +
        "\r\n" +
        "See -" + nameof(QueryExpressionWithSuggestions) + "-.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class QueryExpressionHint : QueryExpression {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "HINT {ON|OFF}\r\n" +
            "Examples:\r\n" +
            "HINT OFF\r\n" +
            "HINT ON"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionHint));

        public bool CreateHints { get; private set; }

        public QueryExpressionHint(bool createHints) => CreateHints = createHints;

        public override QueryExpressionWithSuggestions Execute(Query query) {
            query.Progress.IP.SetPV(QueryProgressP.CreateHints, CreateHints);
            return new QueryExpressionWithSuggestions(
                this, 
                previous: !CreateHints ? null : new QueryExpressionHint(createHints: false),
                next: CreateHints ? null : new QueryExpressionHint(createHints: true) // This is somewhat meaningless because will not show up in UI (since hints now are OFF)
           );
        }
         
        public new static QueryExpressionHint Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryExpressionHintException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionHint retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionHint retval, out string errorResponse) {
            value = value.Trim().Replace("  ", " ");
            var t = value.Split(" ");
            if (!"hint".Equals(t[0].ToLower())) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'HINT' as first word." + SyntaxHelp;
                return false;
            }
            if (t.Length != 2) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not two words like 'HINT {ON|OFF}' but " + t.Length + " words." + SyntaxHelp;
                return false;
            }
            switch (t[1].ToUpper()) {
                case "ON":
                    retval = new QueryExpressionHint(createHints: true);
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
                case "OFF":
                    retval = new QueryExpressionHint(createHints: false);
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
                default:
                    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                    errorResponse = "Illegal value (" + t[1] + "), use one of 'ON' or 'OFF'";
                    return false;
            }
        }

        public override string ToString() => "HINT " + (CreateHints ? "ON" : "OFF");

        public class InvalidQueryExpressionHintException : ApplicationException {
            public InvalidQueryExpressionHintException(string message) : base(message) { }
            public InvalidQueryExpressionHintException(string message, Exception inner) : base(message, inner) { }
        }
    }

    /// <summary>
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionHintExtension {
        public static Query Hint(this Query query, bool createHints) =>
            query.ExecuteOne(new QueryExpressionHint(createHints));
    }
}