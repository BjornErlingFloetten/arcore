﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ARCCore;

namespace ARCQuery
{

    [Class(Description =
        "Selects which fields to include.\r\n" +
        "\r\n" +
        "Note: -" + nameof(QueryExpressionAll) + "- differs from -" + nameof(QueryExpressionSelect) + "- 'SELECT *' regarding use of -" + nameof(EntityMethodKey) + "-s.\r\n" +
        "The query 'Customer/All' will not include -" + nameof(EntityMethodKey) + "-s, while 'Customer/SELECT *' would.\r\n" +
        "\r\n" +
        "'SELECT *' can be used in connection with -" + nameof(CompoundKey) + "-, like for instance\r\n" +
        "'Customer/SELECT *, CountP()' which will select all fields in addition to a new field calculated by -" + nameof(NewKeyCountP) + "-.\r\n" +
        "See -" + nameof(CompoundKey) + "- for the quite rich functionality offered (like traversion of entity relations for instance).\r\n" +
        "\r\n" +
        "HINT: It is recommended to use 'AS' when using -" + nameof(CompoundKey) + "- if subsequent -" + nameof(QueryExpression) + "-s follows.\r\n" +
        "HINT: This will reduce parsing complexities because a complex field name may be unnecessary attempted evaluated as a -" + nameof(CompoundKey) + "-.\r\n" +
        "HINT: Example: For 'Customer/SELECT Name, Order.OrderLine.Sum.Sum()/ORDER BY Order.OrderLine.Sum.Sum()', " +
        "HINT: if the key 'Order.OrderLine.Sum.Sum()' does not exist for some Customer, it will be attempted reevaluated by the following -" + nameof(QueryExpressionOrderBy) + "-\r\n" +
        "HINT: and may be misunderstood.\r\n" +
        "HINT: Doing instead 'Customer/SELECT Name, Order.OrderLine.Sum.Sum() AS OrderSum/ORDER BY OrderSum' will remove this possibility\r\n" +
        "HINT: (and be more readable also).\r\n" +
        "\r\n" +
        "Note: The original key type is not necessarily preserved.\r\n" +
        "See hack in -" + nameof(QueryProgress.OrderAndReconstructKeys) + "- (called from -" + nameof(QueryProgress.ToHTMLSimpleSingle) + "- for details.\r\n" +
        "\r\n" +
        "TODO: Add some functionality recognizing -" + nameof(ARCDoc.PriorityOrder) + "-, like\r\n" +
        "TODO: 'SELECT' which could select only from -" + nameof(ARCDoc.PriorityOrder.Important) + "- or lesser value.\r\n" +
        "TODO: Or 'SELECT WHERE PriorityOrder LEQ Important' (Using negative values for PriorityOrder is not very intuitive here).\r\n" +
        "TODO: or any other query against the property keys for the relevant entity class.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class QueryExpressionSelect : QueryExpression
    {

        public static readonly string UrlSegmentMaxLengthWarning =
            "A SELECT query expression will often become very long.\r\n" +
            "On Windows, http.sys (server side) will probably not allow length greater than 260 characters " +
            "(when querying through HTTP that is).\r\n" +
            "\r\n" +
            "Possible correction with Registry Editor (RegEdit.msc):\r\n" +
            @"Computer\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\HTTP\Parameters" + "\r\n" +
            "Set UrlSegmentMaxLength to a longer value than 260 and restart PC.\r\n" +
            "https://stackoverflow.com/questions/1185739/asp-net-mvc-url-routing-maximum-path-url-length \r\n";
        // NOTE: Changing in web.config was futile, for instance setting <httpRuntime relaxedUrlToFileSystemMapping="true" />


        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "SELECT {field-1}, {field-2} ... {field-n}\r\n" +
            "or\r\n" +
            "SELECT {field-1} AS {field-1-as}, {field-2} AS {field-2-as} ... {field-n} AS {field-n-as}\r\n" +
            "Examples:\r\n" +
            "'SELECT *'\r\n" +
            "'SELECT *.TMB()' -- Apply a -" + nameof(FunctionKey) + "- to every field.\r\n" +
            "'SELECT FirstName, LastName'\r\n" +
            "'SELECT FirstName.Left(10), LastName.Left(20)' -- Apply a -" + nameof(FunctionKey) + "- to single fields\r\n" +
            "'SELECT FirstName AS fn, LastName AS ln'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionSelect));

        public List<Field> Fields { get; private set; }

        public QueryExpressionSelect(List<Field> fields) => Fields = fields;

        //public QueryExpressionSelect(List<string> fields) : this(fields.Select(f => {
        //    var ik = (IK)IKString.FromString(f);
        //    return (ik, ik);
        //}).ToList()) { }
        //public QueryExpressionSelect(List<IK> fields) : this(fields.Select(f => (f, f)).ToList()) { }
        //public QueryExpressionSelect(List<(IK Field, IK FieldAs)> fields) => Fields = fields;

        public override QueryExpressionWithSuggestions Execute(Query query)
        {

            var retval = new QueryExpressionWithSuggestions(this);

            foreach (var field in Fields)
            {
                // NOTE: StartsWith("*") here, and not Equals("*") because we may apply functions on every field, like "SELECT *.TMB()"
                if (field.IsWildcardField) continue;
                if (!TryCheckForExistenceOfKey(query, field.Name, out _))
                {
                    return retval;
                }
            }

            // NOTE: This will only be used if we have any wildcard fields.            
            var entityMethodKeys = (IP.AllIPDerivedTypesDict.TryGetValue(query.Progress.StrCurrentType, out var currentType) ?
                EntityMethodKey.AllEntityMethodKeysForEntityTypeList(currentType) : new List<EntityMethodKey>()).Select(k =>
                (IK: IKString.FromCache(k.ToString()), Key: k));

            // NOTE: cachedCompoundKeys are calculated below based on first element, at once (before parallell processing)
            // NOTE: See below for details.
            var cachedCompoundKeys = new CompoundKey?[Fields.Count];

            var select = new Func<IKIP, IKIP>(ikip =>
            {
                /// NOTE: Note that we can not do this now:
                /// NOTE:   var row = (IP)System.Activator.CreateInstance(ikip.P.GetType());
                /// NOTE: First, it would not for instance pass a <see cref="IP.AssertIntegrity"/> test (something which we do not do here though anyway).
                /// NOTE: But more important, because of <see cref="CompoundKey"/> we may add fields 
                /// NOTE: that are not allowed according to the schema, like in 'Order/SELECT Amount, Customer.Firstname'.
                /// NOTE: The actual type (ikip.P.GetType()) may not allowe random new field names like this (for instance if it is <see cref="PExact{EnumType}"/>

                // So this is the correct approach
                var row = new PRich();

                for (var i = 0; i < Fields.Count; i++)
                {
                    var field = Fields[i];
                    if (field.IsWildcardField)
                    {
                        if (field.FunctionKeys != null)
                        {

                            // We are going to apply a function (or multiple functions) to every field.
                            // Combine original field name with function(s) to be applied.

                            // 7 APR 2021: Discarded approach 
                            // ikip.P.ForEach(ikipField => {
                            /// Does not work with field names that can be mistaken as part of compound keys
                            /// For instance if we have field names containing hyphen (minus), it will be misunderstood as <see cref="BinaryOperatorKey"/>
                            /// (such field names could typical result from using <see cref="QueryExpressionPivot"/> for instance).
                            //// TODO: Inefficient, could have been stored somehow 
                            //var newKeyField = IKString.FromString(ikipField.Key.ToString() + Fields[i].Name.ToString()[1..]);

                            //// TODO: Document this. SELECT *.TMB() AS * for instance.
                            //var newKeyFieldAs = IKString.FromString(ikipField.Key.ToString() + Fields[i].NameAs.ToString()[1..]);

                            //// TODO: We can not use the array of cached compound keys now, because key will change between each field. 
                            //// TODO: This is inefficient!
                            //CompoundKey? dummy = null;
                            //// TODO: It is inefficient to fetch the original property once more (in TryGetP below)
                            //if (ikip.TryGetP(query.Progress.DataStorage, newKeyField, out var p, ref dummy, out _)) {
                            //    row.TrySetP(new IKIP(newKeyFieldAs, p), out _); // TrySetP is the most "direct" overload
                            //}
                            ///// TOOD: Update 09 Mar 2021: Is text below correct?
                            ///// TODO: We are missing use of entityMethodKeys here, but on the other hand it is most probably
                            ///// TODO: not relevant (applying a function for every field is more relevant for 
                            ///// TODO: results from <see cref="QueryExpressionPivot"/> and similar, with homogenous fields.
                            ///// TODO: existing as data, not as methods.
                            ///
                            // });

                            /// Better (less bug-prone) approach
                            /// (we only recognize <see cref="FunctionKey"/>s now, not a whole <see cref="CompoundKey"/>).

                            // Step 1, all data values (existing directly)
                            ikip.P.ForEach(ikipField =>
                            {
                                var ip = ikipField.P;
                                foreach (var fk in field.FunctionKeys)
                                {
                                    if (!fk.TryGetP(ip, out ip, out _))
                                    {
                                        // Note how we simply fail silently here.
                                        return;
                                    }
                                }
                                row.TrySetP(new IKIP(
                                    ikipField.Key.ToString() + Fields[i].NameAs.ToString()[1..], // Skip *-part in NameAs.
                                    ip
                                ), out _); // TrySetP is the most "direct" overload)
                            });
                            /// Step 2, all dynamically generated values (all <see cref="EntityMethodKey"/>)
                            entityMethodKeys.ForEach(k =>
                            {
                                if (k.Key.TryGetPInternal(ikip, query.Progress.DataStorage, out var ip, out _))
                                {
                                    foreach (var fk in field.FunctionKeys)
                                    {
                                        if (!fk.TryGetP(ip, out ip, out _))
                                        {
                                            // Note how we simply fail silently here.
                                            return;
                                        }
                                    }
                                    row.TrySetP(new IKIP(
                                        k.IK.ToString() + Fields[i].NameAs.ToString()[1..], // Skip *-part in NameAs.
                                        ip
                                    ), out _);
                                }
                            });
                        }
                        else
                        {
                            // Return each field value 'as is'
                            // Step 1, all data values (existing directly)
                            ikip.P.ForEach(ikip => row.TrySetP(ikip, out _)); // TrySetP is the most "direct" overload)
                            /// Step 2, all dynamically generated values (all <see cref="EntityMethodKey"/>)
                            entityMethodKeys.ForEach(k =>
                            {
                                if (k.Key.TryGetPInternal(ikip, query.Progress.DataStorage, out var ip, out _))
                                {
                                    row.TrySetP(new IKIP(k.IK, ip), out _);
                                }
                            });
                        }
                    }
                    else
                    {
                        if (ikip.TryGetP(query.Progress.DataStorage, field.Name, out var p, ref cachedCompoundKeys[i], out _))
                        {
                            row.TrySetP(new IKIP(Fields[i].NameAs, p), out _); // TrySetP is the most "direct" overload
                        }
                    }
                }
                return new IKIP(ikip.Key, row);
            });
            query.Progress.Result =
                /// Execute first item not in parallell, in order to populate all values for
                /// cached compound keys. This is important in order to avoid too many cached compound keys being
                /// created (and especially important for keys like <see cref="QuantileKey"/> because for them
                /// every single key will generate values for ALL entities.
                /// NOTE: This is not a 100% solution because hypotetically the first entity may contain
                /// NOTE: the property directly, meaning no cached compound key is created, and other entities
                /// NOTE: may not contain the property directly, in which case cached compound keys would be created
                /// NOTE: in parallell, resulting in possible duplicates being created.
                query.Progress.Result.Take(1).Select(ikip => select(ikip)).ToList().Concat( // ToList is VERY IMPORTANT in order to avoid deferred execution
                query.Progress.Result.Skip(1).AsParallel().Select(ikip => select(ikip)));

            return retval;
        }

        public new static QueryExpressionSelect Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQuerysSelectException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionSelect retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionSelect retval, out string errorResponse)
        {
            value = value.Trim();
            var pos = value.IndexOf(" ");
            if (pos == -1 || !"select".Equals(value[..pos].ToLower()))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'SELECT' as first word." + SyntaxHelp;
                return false;
            }
            var ikFields = UtilQuery.SplitOutsideOf(value[(pos + 1)..], separator: ",", outsideOf: '\'').Select(f => f.Trim()).Where(f => !"".Equals(f)).Select(f =>
            {
                pos = UtilQuery.IndexOfOutsideOf(f.ToUpper(), value: " AS ", outsideOf: '\'');
                if (pos == -1)
                {
                    var ik = (IK)IKString.FromString(f);
                    return (Name: ik, NameAs: ik);
                }
                else
                {
                    return (Name: IKString.FromString(f[..pos]), NameAs: IKString.FromString(f[(pos + " AS ".Length)..]));
                }
            }).ToList();
            if (ikFields.Count == 0)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "No fields given" + SyntaxHelp;
                return false;
            }
            // Check for wildcard fields and function keys
            var fields = new List<Field>();
#pragma warning disable IDE0042 // Deconstruct variable declaration
            foreach (var ikField in ikFields)
            {
#pragma warning restore IDE0042 // Deconstruct variable declaration
                var strName = ikField.Name.ToString();
                if (!strName.StartsWith("*"))
                {
                    fields.Add(new Field(ikField.Name, ikField.NameAs));
                    continue;
                }
                if (strName.Equals("*"))
                {
                    fields.Add(new Field(ikField.Name, ikField.NameAs, functionKeys: null));
                    continue;
                }
                if (!strName.StartsWith("*.") || strName.Length == 2)
                {
                    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                    errorResponse = "Illegal use of wildcard (" + strName + "), use either only '*' or '*.' followed by a list of -" + nameof(FunctionKey) + "- separated by full stops." + SyntaxHelp;
                    return false;
                }
                var t = UtilQuery.SplitOutsideOf(strName[2..], separator: ".", outsideOf: '\'');
                var functionKeys = new List<FunctionKey>();
                foreach (var strFunctionKey in t)
                {
                    if (!FunctionKey.TryParse(strFunctionKey, out var fk, out errorResponse))
                    {
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                        errorResponse = "Invalid -" + nameof(FunctionKey) + "- (" + strFunctionKey + "). Details: " + errorResponse;
                        return false;
                    }
                    if (fk is FunctionKeyAggregate)
                    {
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                        errorResponse = "Invalid -" + nameof(FunctionKey) + "- (" + strFunctionKey + "). -" + nameof(FunctionKeyAggregate) + "- not allowed";
                        return false;
                    }
                    functionKeys.Add(fk);
                }
                fields.Add(new Field(ikField.Name, ikField.NameAs, functionKeys));
            }
            retval = new QueryExpressionSelect(fields);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString() => "SELECT " + string.Join(", ", Fields.Select(f => f.ToString()));


        public class Field
        {
            public IK Name { get; private set; }
            public IK NameAs { get; private set; }

            [ClassMember(Description =
                "Wildcard like 'SELECT *'. May be combined with -" + nameof(FunctionKeys) + "-, like 'SELECT *.TMB()' " +
                "(see -" + nameof(FunctionKeyTMB) + "-).\r\n"
            )]
            public bool IsWildcardField { get; private set; }

            [ClassMember(Description = "Only relevant when -" + nameof(IsWildcardField) + "- is TRUE.\r\n")]
            public List<FunctionKey>? FunctionKeys { get; private set; }

            public Field(IK name)
            {
                if (name.ToString().StartsWith("*")) throw new InvalidQuerysSelectException("Invalid field name (" + name + "), for wildcard fields use constructor with FunctionKey parameter instead");
                Name = name;
                NameAs = name;
                IsWildcardField = false;
                FunctionKeys = null;
            }
            public Field(IK name, IK nameAs)
            {
                if (name.ToString().StartsWith("*")) throw new InvalidQuerysSelectException("Invalid field name (" + name + "), for wildcard fields use constructor with FunctionKey parameter instead");
                Name = name;
                NameAs = nameAs;
                IsWildcardField = false;
                FunctionKeys = null;
            }

            public Field(IK name, IK nameAs, List<FunctionKey>? functionKeys)
            {
                if (!name.ToString().StartsWith("*")) throw new InvalidQuerysSelectException("Invalid field name (" + name + "), for non-wildcard fields use constructor without FunctionKey parameter instead");
                Name = name;
                NameAs = nameAs;
                IsWildcardField = true;
                FunctionKeys = functionKeys;
            }

            public override string ToString() => Name.Equals(NameAs) ? Name.ToString() : Name.ToString() + " AS " + NameAs.ToString();
        }

        public class InvalidQuerysSelectException : ApplicationException
        {
            public InvalidQuerysSelectException(string message) : base(message) { }
            public InvalidQuerysSelectException(string message, Exception inner) : base(message, inner) { }
        }
    }

    /// <summary>
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionSelectExtension
    {
        public static Query Select(this Query query, params string[] fields) => Select(query, fields.Select(s => new QueryExpressionSelect.Field(IKString.FromString(s))).ToList());
        public static Query Select(this Query query, List<QueryExpressionSelect.Field> fields) => query.ExecuteOne(new QueryExpressionSelect(fields));
    }
}