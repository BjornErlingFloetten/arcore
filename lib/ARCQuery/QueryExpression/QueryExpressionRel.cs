﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery
{

    /// <summary>
    /// TODO: Throw exceptions for invalid data / references in Execute
    /// </summary>
    [Class(Description =
        "Enables 'jumping' or 'travelling' from a collection of one entity type to another (related) entity type.\r\n" +
        "\r\n" +
        "Enables queries like\r\n" +
        "'Order/WHERE Amount > 10000 EUR/REL Customer/REL SupportTicket'\r\n" +
        "which will show support tickets " +
        "for all customer who at some time have ordered something for at least 10 000 EUR.\r\n" +
        "\r\n" +
        "Corresponds (very) coarsely to the JOIN concept in SQL\r\n" +
        "\r\n" +
        "There are two ways for this to happen:\r\n" +
        "\r\n" +
        "1) -" + nameof(ForeignKeyDirection.ToMany) + "-:\r\n" +
        "If -" + nameof(ForeignKey.IPRelationsKeysPointingTo) + "- " +
        "has a link for the current entity type, like 'Customer' having a link to 'Order' then " +
        "a jump from a collection of Customers can be made to for instance an Order collection by iterating through all Orders with " +
        "CustomerId present in the collection of Customers.\r\n" +
        "\r\n" +
        "2) -" + nameof(ForeignKeyDirection.ToOne) + "-:\r\n" +
        "A jump from a collection of 'OrderLine' to 'Order', or 'Order' to 'Customer' can be made by iterating through " +
        "the whole collection looking for respectively 'OrderId = xxx' and 'CustomerId = xxx' properties.\r\n" +
        "\r\n" +
        "Variant 1) (-" + nameof(ForeignKeyDirection.ToMany) + "-) will be the default attempted. " +
        "If relevant relations for 1) is not found, then variant 2) (-" + nameof(ForeignKeyDirection.ToOne) + "-) will be attempted.\r\n" +
        "\r\n" +
        "Does also work with -" + nameof(PKRelAttribute) + "- (-" + nameof(PKRelAttributeP.ForeignEntity) + "-).\r\n" +
        "\r\n" +
        "NOTE: The relevant keys for 'jumping' must exist. If you for instance do like this\r\n" +
        "NOTE: (admittedly meaningless query, but it explains the point though):\r\n" +
        "NOTE:   'Order/WHERE Amount > 10000 EUR/SELECT Amount, Created/REL Customer'\r\n" +
        "NOTE: then the key CustomerId is not available, and the jumping breaks down.\r\n" +
        "NOTE:   'Order/WHERE Amount > 10000 EUR/SELECT CustomerId, Amount, Created/REL Customer'\r\n" +
        "NOTE: on the other hand would work (but use of SELECT is meaningless anyway of course).\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class QueryExpressionRel : QueryExpression
    {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'REL {type}'\r\n" +
            "Examples:\r\n" +
            "'REL Customer',\r\n" +
            "'REL Order'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionRel));

        [ClassMember(Description =
            "In the simplest terms, the -" + nameof(ARCCore.Extensions.ToStringVeryShort) + "- " +
            "representation of the type that we are 'jumping' to.\r\n" +
            "\r\n" +
            "A more exact explanation is that this is either " +
            "-" + nameof(ForeignKey.EntityTypeAndKey) + "-.-" + nameof(ForeignKey.EntityTypeAndKey.OppositeTerm) + "- " +
            "(Variant 1, -" + nameof(ForeignKeyDirection.ToMany) + "-) or " +
            "-" + nameof(ForeignKey.EntityTypeAndKey.KeyWithoutId) + "-.\r\n" +
            "(Variant 2, -" + nameof(ForeignKeyDirection.ToOne) + "-)." +
            "\r\n" +
            "TODO: Clarify this / explain in more formal terms."
        )]
        public string StrType { get; private set; }

        public override QueryExpressionWithSuggestions Execute(Query query)
        {

            var retval = new QueryExpressionWithSuggestions( // DEFAULT VALUE, will be updated below
                this
            );

            {
                if (ForeignKey.IPRelationsKeysPointingTo.TryGetPV<List<ForeignKey.EntityTypeAndKey>>(query.Progress.StrCurrentType, out var relations))
                {
                    /// Variant 1, <see cref="ForeignKeyDirection.ToMany"/>
                    // There are foreign keys pointing to the current type

                    for (var i = 0; i < relations.Count; i++)
                    {
                        var r = relations[i];
                        if (r.OppositeTerm.Equals(StrType))
                        {
                            // Create other suggestions for relational 'jumping'
                            /// NOTE: About suggestions for Previous and Next based on <see cref="QueryExpression.IPRelationsKeysPointingTo"/>           
                            /// NOTE: It can lead to meaningless queries because
                            /// NOTE: there might be other query expression following depending on the type chosen now
                            /// NOTE: For instance
                            /// NOTE:   Order/WHERE Created.Year = ThisYear/REL Customer/SELECT Name, Address
                            /// NOTE: would be meaningless to turn into
                            /// NOTE:   Order/WHERE Created.Year = ThisYear/REL Product/SELECT Name, Address

                            var thisPosition = i;
                            retval = new QueryExpressionWithSuggestions(this,
                                previous: thisPosition <= 0 ? null : new QueryExpressionRel(relations[thisPosition - 1].OppositeTerm),
                                next: thisPosition == -1 || thisPosition == relations.Count - 1 ? null : new QueryExpressionRel(relations[thisPosition + 1].OppositeTerm)
                            );

                            // The 'jump to' type has foreign keys pointing to instances of the current type

                            // Use variant 1)
                            if (!query.Progress.DataStorage.TryGetP<IP>(IKType.FromType(r.EntityType), out var jumpToCollection, out var errorResponse))
                            {
                                // Note that this is somewhat brittle. We do not throw an exception if no value found.
                                if (query.ShouldTerminate(
                                    "JumpToCollection (of " + r.EntityType.ToStringVeryShort() + ") was not found in data storage for '" + ToString() + "'.\r\n" +
                                    "Details: " + errorResponse + ".\r\n" +
                                    "Some of the keys found where: " + string.Join(", ", query.Progress.DataStorage.Keys.Take(20).Select(ik => ik.ToString())) + "\r\n")
                                )
                                {
                                    return retval;
                                }
                            }

                            //// Find all id's of current collection (of current type)
                            var currentCollectionKeys = query.Progress.Result.AsParallel().Select(ikip => ikip.Key.ToString()).ToHashSet(); // Note use of AsParallell here

                            //var foreignKey = PK.TryGetFromTypeAndFieldName(StrType, query.Progress.StrCurrentType + "Id", out var pk) ?
                            //    (IK)pk :
                            //    IKString.FromString(query.Progress.StrCurrentType + "Id");

                            query.Progress.Result =
                                (r.Key is PK pk && pk.Cardinality.IsMultiple() ?
                                /// NOTE: This is really support for <see cref="ARConcepts.ManyToManyRelations"/> without a 'third table'.
                                /// NOTE: One could ask if we go a little too far
                                /// NOTE: here, it might be more reasonable to just add a third entity type like in the traditional
                                /// NOTE: database manner in order to support many-to-many relations.
                                jumpToCollection.AsParallel().Where(ikip =>
                                    // NOTE: Although we ask for IEnumerable<string> here, a List<string> is probably generated
                                    // NOTE: meaning that we loose out on the Any-expression not having to actually consume all items.
                                    ikip.P.TryGetPV<IEnumerable<string>>(r.Key, out var list) && list.Any(v => currentCollectionKeys.Contains(v))
                                ) :
                                // Ordinary case, not multiple
                                jumpToCollection.AsParallel().Where(ikip =>
                                    /// TODO: Slow code in <see cref="ForeignKey.TryGetPInternal"/> and <see cref="QueryExpressionRel.Execute"/>
                                    /// TODO: because we (as of Feb 2021) miss indexing on foreign keys.
                                    /// TODO: NOTE: One "cheap" way out here is avoiding the inherent O(n^2) scenario by building the index 
                                    /// TODO: BEFORE executing the overarching query.
                                    ikip.P.TryGetPV<string>(r.Key, out var v) && currentCollectionKeys.Contains(v)
                                ));
                            query.Progress.StrCurrentType = r.EntityType.ToStringVeryShort();
                            return retval;
                        }
                    }
                }
            }

            {
                if (ForeignKey.IPRelationsKeysPointingFrom.TryGetPV<List<ForeignKey.EntityTypeAndKey>>(query.Progress.StrCurrentType, out var relations))
                /// Use variant 2), <see cref="ForeignKeyDirection.ToOne"/>
                {
                    for (var i = 0; i < relations.Count; i++)
                    {
                        var r = relations[i];
                        if (r.KeyWithoutId.Equals(StrType))
                        {

                            var thisPosition = i;
                            retval = new QueryExpressionWithSuggestions(this,
                                previous: thisPosition <= 0 ? null : new QueryExpressionRel(relations[thisPosition - 1].KeyWithoutId),
                                next: thisPosition == -1 || thisPosition == relations.Count - 1 ? null : new QueryExpressionRel(relations[thisPosition + 1].KeyWithoutId)
                            );

                            if (!query.Progress.DataStorage.TryGetP<IP>(IKType.FromType(r.EntityType), out var jumpToCollection, out var errorResponse))
                            {
                                if (query.ShouldTerminate(
                                    "JumpToCollection (of " + r.EntityType.ToStringVeryShort() + ") was not found in data storage.\r\n" +
                                    "Details: " + errorResponse + ".\r\n" +
                                    "Some of the keys found where: " + string.Join(", ", query.Progress.DataStorage.Keys.Take(20).Select(ik => ik.ToString()))
                                ))
                                {
                                    return retval;
                                }
                            }

                            // NOTE: We refer to queryProgress.Details.StrCurrentType inside a deferred query.
                            // NOTE: Therefore this value is stored in a separate instance (before it gets changed below)
                            var originalCurrentType = query.Progress.StrCurrentType;

                            var emptyList = new List<string>();
                            query.Progress.Result =
                                (r.Key is PK pk && pk.Cardinality.IsMultiple() ?
                                    /// NOTE: This is really support for <see cref="ARConcepts.ManyToManyRelations"/> without a 'third table'.
                                    /// NOTE: One could ask if we go a little too far
                                    /// NOTE: here, it might be more reasonable to just add a third entity type like in the traditional
                                    /// NOTE: database manner in order to support many-to-many relations.
                                    query.Progress.Result.AsParallel(). // Note use of AsParallell here.
                                    SelectMany(ikip =>
                                        // Find all items containing expected foreign key (like all Orders containing 'CustomerId = xxx'
                                        ikip.P.TryGetPV<List<string>>(r.Key, out var list) ? list : emptyList
                                    ) :
                                    query.Progress.Result.AsParallel(). // Note use of AsParallell here.
                                    Select(ikip =>
                                        // Find all items containing expected foreign key (like all Orders containing 'CustomerId = xxx'
                                        ikip.P.TryGetPV<string>(r.Key, out var foreignKeyValue) ? foreignKeyValue : null! // ! in order to get rid of compilator warning. See null-check immediately below
                                    )
                                ).
                                Where(strKey => strKey != null).
                                Select(strKey => strKey!).
                                Distinct(). // Important, make collection of keys distinct before attempting to retrieve any of the related values.
                                            // AsParallel(). // Unnecessary to repeat AsParallell here.
                                Select(strKey =>
                                {
                                    var key = IKString.FromString(strKey);
                                    if (!jumpToCollection.TryGetP<IP>(key, out var jumpToItem))
                                    {
                                        // This is not good. We have an invalid reference.

                                        // NOTE: queryProgress.ShouldTerminate will be called multiple times as code is now
                                        if (query.ShouldTerminate(
                                            "Found invalid foreign key '" + r.Key + " = " + key + "' in collection of " +
                                            // NOTE: We refer to queryProgress.Details.StrCurrentType inside a deferred query.
                                            // NOTE: Therefore this value is stored in a separate instance (before it gets changed below)
                                            originalCurrentType +
                                            ".\r\n" +
                                            "This key does not exist in collection of " + r.EntityType.ToStringVeryShort() + ".\r\n" +
                                            "(" + r.EntityType.ToStringVeryShort() + "/" + key + " does not exist)"
                                        ))
                                        {
                                            // ?
                                        }
                                        return null;
                                    }
                                    return new IKIP(key, jumpToItem);
                                }).
                                Where(ikip => ikip != null).
                                Select(ikip => ikip!);

                            query.Progress.StrCurrentType = r.EntityType.ToStringVeryShort();
                            return retval;
                        }
                    }
                }
            }

            {
                query.ShouldTerminate("No relations found for " + query.Progress.StrCurrentType + " corresponding to " + StrType);
                return retval;
            }
        }

        public QueryExpressionRel(Type type) =>
            StrType = type.ToStringVeryShort();

        public QueryExpressionRel(string strType) =>
            StrType = strType;

        public new static QueryExpressionRel Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryExpressionRelException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionRel retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionRel retval, out string errorResponse)
        {
            value = value.Trim().Replace("  ", " ");
            var t = value.Split(" ");
            if (!"rel".Equals(t[0].ToLower()))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'REL' as first word." + SyntaxHelp;
                return false;
            }
            if (t.Length != 2)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not two words like 'REL {type}' but " + t.Length + " words." + SyntaxHelp;
                return false;
            }
            retval = new QueryExpressionRel(strType: t[1]);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString() => "REL " + StrType;

        public class InvalidQueryExpressionRelException : ApplicationException
        {
            public InvalidQueryExpressionRelException(string message) : base(message) { }
            public InvalidQueryExpressionRelException(string message, Exception inner) : base(message, inner) { }
        }
    }

    /// <summary>
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionRelExtension
    {
        public static Query Rel(this Query query, Type type) =>
            Rel(query, type.ToStringVeryShort());
        public static Query Rel(this Query query, string strType) =>
            query.ExecuteOne(new QueryExpressionRel(strType));
    }
}