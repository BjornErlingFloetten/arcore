﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ARCCore;
using ARCDoc;

namespace ARCQuery
{

    [Class(
        Description =
            "Represents part of a query (or actually a transformation) against a given collection and the data storage from which it was taken.\r\n" +
            "\r\n" +
            "A series of queries are usually chained together in a fluent fashion\r\n" +
            "for instance as an API call like:\r\n" +
            "  'api/RQ/Order/WHERE Value > 1000 NOK/REL Customer/ORDER BY LastName, FirstName/TAKE 50/'\r\n" +
            "or as a C# expression like:\r\n" +
            "  'new QueryProgress(DataStorage, Orders).Where(\"Value > 100\").Rel(typeof(Customer)).OrderBy(\"LastName,FirstName\").Take(50)'\r\n" +
            "(but in the latter case the standard LINQ extension in .NET will of course usually provide the same or better functionality " +
            "(expect for AgoRapide unique queries like -" + nameof(QueryExpressionRel) + "-)).\r\n" +
            "\r\n" +
            "See also -" + nameof(CompoundKey) + "- which expands the concept of keys.\r\n" +
            "\r\n" +
            "The main purpose of the -" + nameof(QueryExpression) + "- concept is to empower API users to create their own simple ad-hoc reports, " +
            "without needing assistance from the API developers " +
            "(that is, without any C# code having to be written or new API endpoints to be created).\r\n" +
            "\r\n" +
            "The -" + nameof(QueryExpression) + "- concept is not meant to be exhaustive with relation to relational modelling, " +
            "nor is it proven formally.\r\n" +
            "This concept just takes away some (or actually rather quite a lot) of the need for 'hand-crafted' code in order to cover " +
            "what are quite often very common scenarios.\r\n" +
            "\r\n" +
            "More complex needs can be covered by -" + nameof(ARConcepts.ApplicationSpecificCode) + "- implementations of this class (see below).\r\n" +
            "\r\n" +
            "Note that queries can also be embedded in a -" + nameof(Subscription) + "- " +
            "(but this only gives meaning when querying against a hierarchical object storage, see -" + nameof(ARConcepts.AdHocQuery) + "-).\r\n" +
            "\r\n" +
            "The most important implementing classes in -" + nameof(ARConcepts.StandardAgoRapideCode) + "- are:\r\n" +
            "-" + nameof(QueryExpressionAll) + "-\r\n" +
            "-" + nameof(QueryExpressionSelect) + "-\r\n" +
            "-" + nameof(QueryExpressionWhere) + "-\r\n" +
            "-" + nameof(QueryExpressionOrderBy) + "-\r\n" +
            "-" + nameof(QueryExpressionSkip) + "-\r\n" +
            "-" + nameof(QueryExpressionTake) + "-\r\n" +
            "Some classes useful for aggregation queries are:\r\n" +
            "-" + nameof(QueryExpressionPivot) + "-\r\n" +
            "-" + nameof(QueryExpressionAggregate) + "-\r\n" +
            "-" + nameof(QueryExpressionRel) + "-\r\n" +
            "Self creation of collection:\r\n" +
            "-" + nameof(QueryExpressionSelfCreate) + "-\r\n" +
            "Other implementing classes:\r\n" +
            "-" + nameof(QueryExpressionDataStorage) + "-\r\n" +
            "-" + nameof(QueryExpressionTitle) + "-\r\n" +
            "-" + nameof(QueryExpressionSum) + "-\r\n" +
            "-" + nameof(QueryExpressionShuffle) + "-\r\n" +
            "-" + nameof(QueryExpressionLimit) + "-\r\n" +
            "-" + nameof(QueryExpressionHint) + "-\r\n" +
            "-" + nameof(QueryExpressionStrict) + "-\r\n" +
            "-" + nameof(QueryExpressionCache) + "-\r\n" +
            "-" + nameof(QueryExpressionComment) + "-\r\n" +
            "\r\n" +
            "TODO: Potential sub-classes to be added:\r\n" +
            "-QueryExpressionUnion- (for instance like /UNION {subQuery})\r\n" +
            "\r\n" +
            "Note that the concept is inherently expandable, you can create your own sub-classes " +
            "and integrate them with -" + nameof(ARConcepts.StandardAgoRapideCode) + "-.\r\n" +
            "Note how any -" + nameof(ARConcepts.ApplicationSpecificCode) + "- implementations must be added to " +
            "-" + nameof(Parsers) + "- at application startup in order for them to be recognized by -" + nameof(QueryExpression.TryParse) + "-.\r\n" +
            "\r\n" +
            "One practical application for -" + nameof(ARConcepts.ApplicationSpecificCode) + "- implementations of -" + nameof(QueryExpression) + "- is " +
            "hand-crafted reports, for instance a 'QueryExpressionReport042', available as for instance 'api/RQ/Report042'.\r\n" +
            "Another application could be hand-crafted HTML-representations of result sets.\r\n" +
            "\r\n" +
            "See also -" + nameof(CompoundKey) + "- (like -" + nameof(ForeignKey) + "-, -" + nameof(EntityMethodKey) + "- and -" + nameof(NewKey) + "-),\r\n" +
            "-" + nameof(FunctionKey) + "-,\r\n" +
            "-" + nameof(QuantileKey) + "- and\r\n" +
            "-" + nameof(ValueComparer) + "-.\r\n" +
            "\r\n" +
            "The implementations of -" + nameof(QueryExpression) + "- (together with the classes mentioned above) constitute building blocks " +
            "which for instance an API end-user can fit together to construct reports as needed.\r\n" +
            "This compares well to monolithic report creation where elements can not be reused easily.\r\n" +
            "(it is similar to the UNIX philosophy of small utilities each performing a well defined easy-to-understand function).\r\n" +
            "\r\n" +
            "Some online applications demonstrating -" + nameof(QueryExpression) + "- are:\r\n" +
            "http://ARNorthwind.AgoRapide.com and\r\n" +
            "http://ARAdventureWorksOLAP.AgoRapide.com \r\n"
    )]
    public abstract class QueryExpression : ITypeDescriber, IEquatable<QueryExpression>
    {

        [ClassMember(Description =
            "Transforms the given input collection into a resulting collection.\r\n" +
            "\r\n" +
            "The parameter data storage is used for sub classes implementing relations (transforming to collections of other types).\r\n" +
            "\r\n" +
            "TODO: Most probably we also need a parameter for what entity type the input collection represents " +
            "TODO: (one might of course sample it, but the calling method should know anyway). " +
            "\r\n" +
            "TODO: It is possible also that we need a parameter for current LinkContext, or OR " +
            "TODO: rather, let parameter DataStorage just be the same where we find the key of the current entity type " +
            "TODO: and assume that other entity collections are stored at the same level.\r\n" +
            "TODO:\r\n" +
            "TODO: SOLUTION: QueryCollection-class with all necessary parameters. This also enables\r\n" +
            "TODO: method chaining (fluent interface) AND logging AND timing AND exception handling AND so on AND so on.\r\n" +
            "TODO:\r\n" +
            "TODO: Each Query-implementation adds an Extension method against QueryCollection.\r\n" +
            "TODO: QueryCollection itself is IP, with keys for log-data and a Result-key."
        )]
        public abstract QueryExpressionWithSuggestions Execute(Query query);

        [ClassMember(Description =
            "Parsers for all the relevant sub classes of -" + nameof(QueryExpression) + "-.\r\n" +
            "Used by -" + nameof(QueryExpression.TryParse) + "- in order to parse to correct sub class.\r\n" +
            "\r\n" +
            "Key is first word of query, like 'STRICT', 'WHERE', 'ORDER' [BY], 'SKIP', 'TAKE', 'LIMIT', 'PIVOT', 'SELECT' and so on.\r\n" +
            "Note that multiple sub classes can share first word (because value in this dictionary is List<...>).\r\n" +
            "If multiple parsers thereby exists then the first parser that succeeds with parsing 'wins'.\r\n" +
            "\r\n" +
            "Add to this collection if you have -" + nameof(QueryExpression) + "- sub classes " +
            "implemented in your -" + nameof(ARConcepts.ApplicationSpecificCode) + "-.\r\n" +
            "The function added should call the corresponding TryParse in your sub class implementation.\r\n" +
            "\r\n" +
            "TODO: Consider automatic injecting to this collection at application startup, " +
            "TODO: also for -" + nameof(ARConcepts.StandardAgoRapideCode) + "-, that is, eliminating code below.\r\n" +
            "\r\n" +
            "NOTE: Be careful to actually implement TryParse in your implementation.\r\n" +
            "NOTE: If not implemented then TryParse in this class (-" + nameof(TryParse) + "-)\r\n" +
            "NOTE: will only call itself leading to a stack overflow exception.\r\n" +
            "\r\n" +
            "See also -" + nameof(AddParser) + "-.\r\n"
        )]
        public static Dictionary<string, List<Func<string, (QueryExpression? queryId, string? errorResponse)>>> Parsers = new Func<Dictionary<string, List<Func<string, (QueryExpression? queryId, string? errorResponse)>>>>(() =>
        {
            var retval = new Dictionary<string, List<Func<string, (QueryExpression? queryId, string? errorResponse)>>>();
            var adder = new Action<string, Func<string, (QueryExpression? queryId, string? errorResponse)>>((firstWord, parser) =>
                  /// Note how all sub classes in <see cref="ARConcepts.StandardAgoRapideCode"/> 
                  retval[firstWord] = new List<Func<string, (QueryExpression? queryId, string? errorResponse)>> {
                    parser
                  }
            );
            // TOOD: Decide some better ordering here.
            adder("DATASTORAGE", value => QueryExpressionDataStorage.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("TITLE", value => QueryExpressionTitle.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("SUM", value => QueryExpressionSum.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("COMMENT", value => QueryExpressionComment.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("--", value => QueryExpressionComment.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("LOG", value => QueryExpressionLog.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("CACHE", value => QueryExpressionCache.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("STRICT", value => QueryExpressionStrict.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("LIMIT", value => QueryExpressionLimit.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("HINT", value => QueryExpressionHint.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("ALL", value => QueryExpressionAll.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("WHERE", value => QueryExpressionWhere.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("REL", value => QueryExpressionRel.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("SELFCREATE", value => QueryExpressionSelfCreate.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("SKIP", value => QueryExpressionSkip.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("TAKE", value => QueryExpressionTake.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("ORDER", value => QueryExpressionOrderBy.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("THEN", value => QueryExpressionThenBy.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("AGGREGATE", value => QueryExpressionAggregate.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("PIVOT", value => QueryExpressionPivot.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("SHUFFLE", value => QueryExpressionShuffle.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            adder("SELECT", value => QueryExpressionSelect.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((QueryExpression?)null, errorResponse));
            return retval;
        })();

        [ClassMember(Description =
            "Adds a -" + nameof(ARConcepts.ApplicationSpecificCode) + "- parser.\r\n" +
            "\r\n" +
            "Only to be done when -" + nameof(UtilCore.CurrentlyStartingUp) + "-."
        )]
        public static void AddParser(string firstWord, Func<string, (QueryExpression? queryId, string? errorResponse)> parser)
        {
            UtilCore.AssertCurrentlyStartingUp();
            if (!Parsers.ContainsKey(firstWord)) Parsers[firstWord] = new List<Func<string, (QueryExpression? queryId, string? errorResponse)>>();
            Parsers[firstWord].Add(parser);
        }

        public static QueryExpression Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryExpressionException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpression retval) => TryParse(value, out retval, out _);
        [ClassMember(Description =
            "Find parsers from -" + nameof(Parsers) + "- based on first word in query.\r\n" +
            "\r\n" +
            "NOTE: Be careful to actually implement TryParse in your implementation.\r\n" +
            "NOTE: If not implemented then TryParse in this class (-" + nameof(TryParse) + "-)\r\n" +
            "NOTE: will only call itself leading to a stack overflow exception.\r\n"
        )]
        public static bool TryParse(string value, out QueryExpression retval, out string errorResponse)
        {
            var pos = value.IndexOf(" ");
            var firstWord = (pos == -1 ? value : value[..pos]).ToUpper();
            if (!Parsers.TryGetValue(firstWord, out var parsers))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse =
                    "Start of query expression '" + firstWord + "' not recognized. " +
                    "Must start with one of\r\n\r\n" +
                    string.Join(", ", Parsers.Keys.OrderBy(k => k).
                        /// Note attempt to make ready for <see cref="ARConcepts.LinkInsertionInDocumentation"/> here.
                        Select(k => (k) switch
                        {
                            /// Note hack for <see cref="QueryExpressionOrderBy"/> for which only -ORDER- would not link properly.
                            "ORDER" => "ORDER (as in (-OrderBy-)",
                            /// Note hack for <see cref="QueryExpressionThenBy"/> for which only -THEN- would not link properly.
                            "THEN" => "THEN (as in (-ThenBy-)",
                            // All other query expression can be linked to directly
                            _ => ("-" + k + "-")
                        })
                    );
                return false;
            }
            if (parsers.Count == 0)
            {
                throw new InvalidQueryExpressionException("First word keyword '" + firstWord + "' resulted in 0 parsers. Key wrongly added to -" + nameof(Parsers) + "-.");
            }
            var errorResponses = new List<string>();

            // Try all parsers. The first one to parse correctly 'wins'.
            foreach (var parser in parsers)
            {
                var result = parser(value);
                if (result.queryId != null)
                { // First parser that suceeds wins
                    retval = result.queryId;
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
                }
                errorResponses.Add(result.errorResponse ?? throw new NullReferenceException(nameof(result.errorResponse) + " for parser '" + firstWord + "'. Value: '" + value + "'"));
            }

            retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            errorResponse =
                (parsers.Count == 1 ? "" : (
                    "Found " + parsers.Count + " possible parsers for expressions beginning with '" + firstWord + "'.\r\n" +
                    "None of them succeeded.\r\n"
                )) +
                nameof(errorResponses) + ":\r\n" +
                string.Join("\r\n", errorResponses);
            return false;
        }

        public override int GetHashCode() => ToString().GetHashCode();
        public bool Equals(QueryExpression other) => ToString().Equals(other.ToString());
        public override bool Equals(object other) => other is QueryExpression q && Equals(q);

        /// <summary>
        /// 'Implementing' <see cref="ITypeDescriber"/>
        /// </summary>
        /// <param name="key"></param>
        public static void EnrichKey(PK key) =>
            key.SetValidatorAndParser(new Func<string, ParseResult>(value =>
                TryParse(value, out var retval, out var errorResponse) ?
                    ParseResult.CreateOK(retval) :
                    ParseResult.CreateError(errorResponse)
            ));

        [ClassMember(Description =
            "Used by query expressions in order to catch wrong field names used or misspelling of field names.\r\n" +
            "\r\n" +
            "Returns TRUE if either:\r\n" +
            "1) -" + nameof(PK.TryGetFromTypeAndFieldName) + " succeeds (in which case out parameter keyAsPK will be set),\r\n" +
            "2) (new from Jan 2022) -" + nameof(EntityMethodKey.AllEntityMethodKeysForEntityTypeDict) + " contains the key,\r\n" +
            "3) -" + nameof(QueryProgressP.CurrentStrictness) + "- = " + nameof(Strictness.OFF) + ",\r\n" +
            "4) -" + nameof(QueryProgress.Result) + "- is empty, or,\r\n" +
            "5) At least one element in -" + nameof(QueryProgress.Result) + "- contains the given key\r\n" +
            "\r\n" +
            "Note that this check must be deferred to the actual execution of the query, not at -" + nameof(QueryExpression.TryParse) + "-, " +
            "because the query expression mechanism has no concept of for which entity types it operates on.\r\n" +
            "\r\n" +
            "Will call -" + nameof(Query.ShouldTerminate) + "- if fails to get the key, this again will set " +
            "-" + nameof(QueryProgressP.TerminateReason) + "- with detailed information about available keys " +
            "(that is, with a helpful error message)."
        )]
        public bool TryCheckForExistenceOfKey(Query query, IK key, out PK keyAsPK)
        {
            // 9 Mar 2022: Removing FunctionKeys plus QuantileKeys before comparing
            // (in order to avoid error message about missing key just because a function key was added for instance)
            var checkKey =
                (
                IP.AllIPDerivedTypesDict.TryGetValue(query.Progress.StrCurrentType, out var temp) &&
                CompoundKey.Parse(query.Progress.DataStorage, key.ToString(), temp) is OrdinaryKeyPlusFunctionOrQuantile o
                ) ?
                o.Key :
                key;
                                   
            if (PK.TryGetFromTypeAndFieldName(query.Progress.StrCurrentType, checkKey.ToString(), out keyAsPK))
            {
                // OK, defined in schema
                return true;
            }

            // New functionality added on 11 Jan 2022 in order for queries which contain "valid" keys (as EntityMethodKeys), but 
            // for which no values for the keys were found, not to return with an error.
            // (this is the same principle as check for PK above, if was just simply not done for EntityMethodKeys)
            if (
                IP.AllIPDerivedTypesDict.TryGetValue(query.Progress.StrCurrentType, out var t) &&
                EntityMethodKey.AllEntityMethodKeysForEntityTypeDict(t).ContainsKey("TryGet" +
                    // Note: Using checkKey instead of key here does probably not have any effect (because if key was an EntityMethodKey it would not be reflected in checkKey anyway)
                    checkKey.ToString() 
                )
            )
            {
                return true;
            }

            if ((query.Progress.StrCurrentType + "Id").Equals(key.ToString()))
            {
                // OK, key is the identifier / "primary key"
                return true;
            }

            if (query.Progress.IP.GetPVM<Strictness>() == Strictness.OFF)
            {
                // Do not bother
                return true;
            }

            var first = query.Progress.Result.FirstOrDefault();
            if (first == default)
            {
                // Result is empty, issue does not matter
                return true;
            }

            CompoundKey? cachedCompoundKey = null;
            string? errorResponse = null;
            // NOTE: This is a potential performance problem, even if query eventually succeeds.
            // NOTE: In a query where only a very small percentage percent of calls to TryGetP succeed, the probability of
            // NOTE: having to evaluate a significant part of the result twice
            // NOTE: (once here, once at the final execution of the query expression) increases.

            // TODO: This check fails for 
            // TODO:   Customer/SELECT */ORDER BY Order.OrderId.Sum()
            // TODO: because evaluating Order.OrderId.Sum() returns TRUE now.
            if (query.Progress.Result.Any(ikip => ikip.TryGetP(query.Progress.DataStorage, key, out _, ref cachedCompoundKey, out errorResponse)))
            {
                // OK, found in actual collection
                return true;
            }

            return query.ShouldTerminate(
                "For query expression\r\n" +
                "\r\n" +
                ToString() + "\r\n" +
                "\r\n" +
                "the key\r\n" +
                "\r\n" +
                key + "\r\n" +
                "\r\n" +
                "was not found.\r\n" +
                "\r\n" +
                (errorResponse != null ? ("Last " + nameof(errorResponse) + " is:\r\n\r\n" + errorResponse + "\r\n\r\n") : "") +
                (cachedCompoundKey != null && !(cachedCompoundKey is CompoundInvalidKey) ? ("key was understood to be of type -" + cachedCompoundKey.GetType().ToStringShort() + "-\r\n") : "") +
                "The current type is " + query.Progress.StrCurrentType + ".\r\n" +
                "Probably the wrong key was used as criteria, or the key name was misspelled.\r\n" +
                "\r\n" +
                /// TODO: The string of possible keys that we build now is actually a data collection and should have been exposed as so in the 
                /// TODO: standard <see cref="ARConcepts.PropertyAccess"/> manner instead of as a simple string.
                "Some possible correct keys are:\r\n" +

                // The primary key is always "outside" of the key storage for every entity, it must therefore be mentioned explicitly
                query.Progress.StrCurrentType + "Id, " +
                string.Join(", ",
                    /// NOTE: Similar code in <see cref="ForeignKey.AllForeignKeysForEntityType" and <see cref="QueryExpression.TryCheckForExistenceOfKey"/>

                    /// Step 1, schema
                    /// Note that this list is not necessarily complete because it may be possible to add keys to the entities outside of the "schema" described here
                    /// (for classes inheriting <see cref="PExact{EnumType}"/> however, this list should be complete)
                    /// Note use of prepending and appending "-"s, facilitating <see cref="ARConcepts.LinkInsertionInDocumentation"/>
                    PK.GetAllPKForEntityTypeString(query.Progress.StrCurrentType).Select(pk => "-" + pk.ToString() + "-").
                    Concat(IP.AllIPDerivedTypesDict.TryGetValue(query.Progress.StrCurrentType, out var currentType) ?
                        /// Step 2, get all TryGet-methods (dynamically generated properties)
                        EntityMethodKey.AllEntityMethodKeysForEntityTypeList(currentType).Select(c => "-" + c.ToString() + "-") :
                        new List<string>()).
                    Concat(
                        // Regardless of schema or no schema, look also at first item for keys
                        // Note that this list is not necessarily complete (because we only look at the first entity in the list).
                        first.P.Select(ikip => "-" + ikip.Key.ToString() + "-")).
                    Distinct().
                    OrderBy(
                        // Order everything together alphabetically (expect the primary key)
                        k => k
                    )
                ) +
                "\r\n" +
                "\r\n" +
                (ForeignKey.AllForeignKeysForEntityType(query.Progress.StrCurrentType).Count == 0 ? "" : (
                    "Some foreign keys are:\r\n" +
                    string.Join(", ", ForeignKey.AllForeignKeysForEntityType(query.Progress.StrCurrentType).Select(k =>
                        /// "-" +   Irrelevant, to try <see cref="ARConcepts.LinkInsertionInDocumentation"/> because the foreign key will not show up in documentation anyway
                        k.ToString()
                    /// "-" +   Irrelevant, to try <see cref="ARConcepts.LinkInsertionInDocumentation"/> because the foreign key will not show up in documentation anyway
                    )) + "\r\n" +
                    "\r\n"
                )
                ) +
                "Some new key functions (available for all entity classes) are:\r\n" +
                // TODO: This will now be offered in CAPITAL LETTERS. 
                // TODO: Offer in original PascalCase instead.
                string.Join(", ", NewKey.Parsers.Keys.OrderBy(k => k).Select(k =>
                    "-" +
                    // TODO: This will now be offered in CAPITAL LETTERS. 
                    // TODO: Offer in original PascalCase instead.                    
                    (k.EndsWith("()") ? k[0..^2] : k) + // TODO: Remove use of paranthesis in keys for NewKey so we do not need this code
                    "-" + "()"
                )) + "\r\n" +
                "\r\n" +
                "Some function keys (used on existing keys) are:\r\n" +
                // TODO: This will now be offered in CAPITAL LETTERS. 
                // TODO: Offer in original PascalCase instead.
                string.Join(", ", FunctionKey.Parsers.Keys.OrderBy(k => k).Select(k =>
                    "-" + k + "-" + "()"
                 )) + "\r\n" +
                "\r\n" +
                "Some quantile keys (used on existing keys) are:\r\n" +
                // TODO: This will now be offered in CAPITAL LETTERS. 
                // TODO: Offer in original PascalCase instead.
                string.Join(", ", UtilCore.EnumGetMembers<Quantile>().Select(k =>
                    "-" + k.ToString() + "-"
                 )) + "\r\n" +
                "\r\n"
            );
        }

        [ClassMember(Description = "Simple utility method always returning a value (as string).")]
        protected static string GetPVAsString(IKIP ikip, IP dataStorage, IK key, ref CompoundKey? newKey, string defaultValue = "") =>
            ikip.TryGetP(dataStorage, key, out var retval, ref newKey, out _) ?
                retval.GetV(defaultValue) :
                defaultValue;

        [ClassMember(Description = "Returns FALSE if either key is not found, or if no string-value found for property (for instance if value is a -" + nameof(PValueEmpty) + "-).")]
        protected static bool TryGetPVAsString(IKIP ikip, IP dataStorage, IK key, ref CompoundKey? newKey, out string value)
        {
            if (!ikip.TryGetP(dataStorage, key, out var retval, ref newKey, out _))
            {
                value = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            if (!retval.TryGetV<string>(out value))
            {
                /// May happen for instance if value is a <see cref="PValueEmpty"/>
                value = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            return true;
        }

        protected static string? GetPVAsStringOrNull(IKIP ikip, IP dataStorage, IK key, ref CompoundKey? newKey) =>
            ikip.TryGetP(dataStorage, key, out var p, ref newKey, out _) ?
                (p.TryGetV<string>(out var value) ? value : (string?)null) :
                (string?)null; // Note that TryGetV<string> should really 'never' fail.

        public class QueryExpressionException : ApplicationException
        {
            public QueryExpressionException(string message) : base(message) { }
            public QueryExpressionException(string message, Exception inner) : base(message, inner) { }
        }

        public class InvalidQueryExpressionException : ApplicationException
        {
            public InvalidQueryExpressionException(string message) : base(message) { }
            public InvalidQueryExpressionException(string message, Exception inner) : base(message, inner) { }
        }
    }

    [Class(Description =
        "Keeps track as each query item gets executed.\r\n" +
        "\r\n" +
        "TODO: Find better name for this class.\r\n" +
        "\r\n" +
        "Contains the original query (" + nameof(QueryExpression) + ") and suggestions for potential new queries.\r\n" +
        "-" + nameof(Next) + "- and -" + nameof(Previous) + "- are the suggestions for potential new queries.\r\n" +
        "\r\n" +
        "Instances of this object are generated by methods implementing -" + nameof(QueryExpression.Execute) + "-.\r\n" +
        "\r\n" +
        "The generated suggestions are typically consumed according to -" + nameof(QueryProgressP.CreateHints) + "-.\r\n" +
        "\r\n" +
        "An example is -" + nameof(QueryExpressionSkip) + "-, if it sees -" + nameof(QueryExpressionTake) + "- as next query to be executed " +
        "then it knows how many more or less to skip (see -" + nameof(QueryExpressionSkip.Execute) + "-).\r\n" +
        "\r\n" +
        "This class is not immutable (because of -" + nameof(ResultCount) + "-)."
    )]
    public class QueryExpressionWithSuggestions
    {
        public QueryExpression QueryExpression { get; private set; }
        [ClassMember(Description = "Suggestion for a 'browse backwards' button.")]
        public QueryExpression? Previous { get; private set; }
        [ClassMember(Description = "Suggestion for a 'browse forwards' button.\r\n")]
        public QueryExpression? Next { get; private set; }

        public int ResultCount { get; set; }

        public QueryExpressionWithSuggestions(QueryExpression queryExpression, QueryExpression? previous = null, QueryExpression? next = null)
        {
            QueryExpression = queryExpression;
            Previous = previous;
            Next = next;
        }
    }
}