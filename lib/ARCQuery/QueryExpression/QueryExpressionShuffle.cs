﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery {

    [Class(Description = 
        "Shuffles collection in random order.\r\n" +
        "\r\n" +
        "Useful when you want to see a limited but still 'representative' sample of result set.\r\n" +
        "\r\n" +
        "Useful in collection with -" + nameof(QueryExpressionTake) + "-.\r\n" +
        "\r\n" +
        "Note extension method -" + nameof(QueryExpressionShuffleExtension.Shuffle) + "- which can be used in C# directly.\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class QueryExpressionShuffle : QueryExpression {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "SHUFFLE"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionShuffle));

        public override QueryExpressionWithSuggestions Execute(Query query) {
            query.Progress.Result = query.Progress.Result.Shuffle(new Random());
            return new QueryExpressionWithSuggestions(this);
        }


        public new static QueryExpressionShuffle Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryExpressionShuffleException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionShuffle retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionShuffle retval, out string errorResponse) {
            if (!"shuffle".Equals(value.Trim().ToLower())) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'SHUFFLE' as only word." + SyntaxHelp;
                return false;
            }
            retval = new QueryExpressionShuffle();
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString() => "SHUFFLE";

        public class InvalidQueryExpressionShuffleException : ApplicationException {
            public InvalidQueryExpressionShuffleException(string message) : base(message) { }
            public InvalidQueryExpressionShuffleException(string message, Exception inner) : base(message, inner) { }
        }
    }

    /// <summary>
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionShuffleExtension {
        public static Query Shuffle(this Query query, int count) =>
            query.ExecuteOne(new QueryExpressionSkip(count));

        /// <summary>
        ///    "Code from Jon Skeet at stackoverflow:\r\n" +
        ///    "https://stackoverflow.com/questions/1287567/is-using-random-and-orderby-a-good-shuffle-algorithm"
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="rng"></param>
        /// <returns></returns>
        // Do not use ClassMemberAttribute because it only clutters up the documentation 
        //[ClassMember(Description =
        //    "Code from Jon Skeet at stackoverflow:\r\n" +
        //    "https://stackoverflow.com/questions/1287567/is-using-random-and-orderby-a-good-shuffle-algorithm"
        //)]
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source, Random rng) {
            var elements = source.ToArray();
            for (var i = elements.Length - 1; i >= 0; i--) {
                // Swap element "i" with a random earlier element it (or itself)
                // ... except we don't really need to swap it fully, as we can
                // return it immediately, and afterwards it's irrelevant.
                var swapIndex = rng.Next(i + 1);
                yield return elements[swapIndex];
                elements[swapIndex] = elements[i];
            }
        }
    }
}