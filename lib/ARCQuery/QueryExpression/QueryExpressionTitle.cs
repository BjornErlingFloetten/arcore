﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery
{
    [Class(Description =
        "The title to be used in the final presentation of the query result.\r\n" +
        "\r\n" +
        "Specified through -" + nameof(QueryExpressionTitle) + "- and stored in -" + nameof(QueryProgressP.Title) + "-." +
        "\r\n" +
        "Does not affect query result in itself, only its presentation\r\n" +
        "\r\n" +
        "Note that query is terminated before 'TITLE ...' is encountered, then the Title will not show in the presentation.\r\n" +
        "Place 'TITLE ...' as early as possible in the query string in order to avoid this.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class QueryExpressionTitle : QueryExpression
    {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "TITLE {title}\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionTitle));

        public string Title { get; private set; }

        public QueryExpressionTitle(string title) => Title = title;

        public override QueryExpressionWithSuggestions Execute(Query query)
        {
            query.Progress.IP.SetPV(QueryProgressP.Title, Title);
            return new QueryExpressionWithSuggestions(this);
        }

        public new static QueryExpressionTitle Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryExpressionTitleException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionTitle retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionTitle retval, out string errorResponse)
        {
            if (!value.ToLower().StartsWith("title "))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'TITLE' as first word." + SyntaxHelp;
                return false;
            }
            retval = new QueryExpressionTitle(value[6..]);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString() => "TITLE " + Title;

        public class InvalidQueryExpressionTitleException : ApplicationException
        {
            public InvalidQueryExpressionTitleException(string message) : base(message) { }
            public InvalidQueryExpressionTitleException(string message, Exception inner) : base(message, inner) { }
        }
    }

    /// <summary>
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionTitleExtension
    {
        public static Query Title(this Query query, string title) =>
            query.ExecuteOne(new QueryExpressionTitle(title));
    }
}