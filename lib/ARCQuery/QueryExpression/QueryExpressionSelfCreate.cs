﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery
{

    [Class(Description =
        "Returns self created collection from given class type.\r\n" +
        "\r\n" +
        "(class must support -" + nameof(ISelfCreateCollection) + "-.\r\n" +
        "\r\n"
    )]
    public class QueryExpressionSelfCreate : QueryExpression
    {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "SELFCREATE {Type supporting " + nameof(ISelfCreateCollection) + "}'"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionSelfCreate));

        public Type Type { get; private set; }

        public QueryExpressionSelfCreate(Type type) => Type = type;

        public override QueryExpressionWithSuggestions Execute(Query query)
        {
            var methodName = ISelfCreateCollection.TrySelfCreateCollectionMethodName;
            try
            {
                var method = Type.GetMethod(methodName, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static) ?? throw new InvalidTypeException(Type,
                    "The class " + Type.ToStringShort() + " does not implement the method\r\n" +
                    "   public static void " + ISelfCreateCollection.TrySelfCreateCollectionMethodName + "\r\n" +
                    "but it should have according to the explanation (documentation) for interface " + typeof(ISelfCreateCollection).ToStringShort() + ".\r\n" +
                    "\r\n" +
                    "Resolution: Either add the method, or do not mark " + Type.ToStringShort() + " as implementing " + typeof(ITypeDescriber).ToStringShort() + ".\r\n" +
                    "Note: The compiler was unable to catch this problem because the actual method is supposed to be static and therefore really not part " +
                    "of the interface mechanism.\r\n"
                );
                // TODO: Implement signature check on method

                var parameters = new object[3] { query.Progress.DataStorage, null!, null! };
                var result = (bool)method.Invoke(null, parameters);

                if (!result)
                {
                    query.ShouldTerminate(parameters[2] as string ?? throw new NullReferenceException(
                        parameters[2] != null ?
                        ("Out-parameter errorResponse (index 2 of wrong type (" + parameters[2].GetType().ToString() + "), string expected. Set by " + method.DeclaringType + "." + method.ToString()) :
                        ("Out-parameter errorResponse (index 2) not set by " + method.DeclaringType + "." + method.ToString()))
                    );
                    return new QueryExpressionWithSuggestions(this);
                }

                query.Progress.Result = parameters[1] as IP ?? throw new NullReferenceException(
                    parameters[1] != null ?
                    ("Out-parameter collection (index 1) of wrong type (" + parameters[1].GetType().ToString() + "), IP expected. Set by " + method.DeclaringType + "." + method.ToString()) :
                    ("Out-parameter collection (index 1) not set by " + method.DeclaringType + " method " + method.ToString()));
                query.Progress.StrCurrentType = Type.ToStringVeryShort();
                return new QueryExpressionWithSuggestions(this);
            }
            catch (Exception ex)
            {
                throw new InvalidQueryExpressionSelfCreateException(
                    "Unable to invoke for type " + Type.ToStringShort() + " the method\r\n" +
                    "   public static bool " + methodName + "\r\n" +
                    "Resolution: Check that it exists and that it takes exactly these paramters:\r\n" +
                    "'IP dataStorage, out IP collection, out string errorResponse'.\r\n" +
                    "In other words it should look like\r\n\r\n" +
                    "   public static bool " + methodName + "(IP dataStorage, out IP collection, out string errorResponse)\r\n",
                    inner: ex
                );
            }
        }

        public new static QueryExpressionSelfCreate Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryExpressionSelfCreateException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionSelfCreate retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionSelfCreate retval, out string errorResponse)
        {
            value = value.Trim().Replace("  ", " ");
            var t = value.Split(" ");
            if (!"selfcreate".Equals(t[0].ToLower()))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'SELFCREATE' as first word." + SyntaxHelp;
                return false;
            }

            if (t.Length != 2)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not two words like 'SELFCREATE {type}' but " + t.Length + " words." + SyntaxHelp;
                return false;
            }

            if (!IP.AllIPDerivedTypesDict.TryGetValue(t[1], out var type))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Type '" + t[1] + "' not recognized as implementing -" + nameof(IP) + "-." + SyntaxHelp;
                return false;
            }

            if (!typeof(ISelfCreateCollection).IsAssignableFrom(type))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Type '" + type.ToStringShort() + "' does not implement -" + nameof(ISelfCreateCollection) + "-." + SyntaxHelp;
                return false;
            }

            retval = new QueryExpressionSelfCreate(type);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString() => "SELFCREATE " + Type.ToStringVeryShort();

        public class InvalidQueryExpressionSelfCreateException : ApplicationException
        {
            public InvalidQueryExpressionSelfCreateException(string message) : base(message) { }
            public InvalidQueryExpressionSelfCreateException(string message, Exception inner) : base(message, inner) { }
        }

        [Class(
            Description =
                "Provides a standardized mechanism for describing classes that are able to autogenerate collection of themeselves.\r\n" +
                "\r\n" +
                "This is done by a static method call 'TrySelfCreateCollection' which is supposed to be available in a class 'implementing' this interface.\r\n" +
                "In this manner, types implementing this 'interface' is by this principle able to describe themselves to AgoRapide).\r\n" +
                "\r\n" +
                "For a typical example, see [TODO: No examples available in ARCore as of Dec 2021]\r\n" +
                "\r\n" +
                "Note how this interface in itself is empty \r\n" +
                "\r\n" +
                "TrySelfCreateCollection should have the following signature:\r\n" +
                "  public static bool TrySelfCreateCollection(IP dataStorage, out IP collection, out string errorResponse)\r\n" +
                "\r\n"
        )]
        public interface ISelfCreateCollection : IP
        {

            public const string TrySelfCreateCollectionMethodName = "TrySelfCreateCollection";
        }
    }
}
