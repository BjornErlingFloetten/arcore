﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ARCCore;

namespace ARCQuery
{

    [Class(Description =
        "Orders collection.\r\n" +
        "\r\n" +
        "Handles a single criteria (single column) for sorting.\r\n" +
        "If you want to sort by multiple columns,\r\n" +
        "use 'ORDER BY' first, followed by 'THEN BY' (see -" + nameof(QueryExpressionThenBy) + "-.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-.\r\n"
    )]
    public class QueryExpressionOrderBy : QueryExpression
    {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "ORDER BY {key} [DESC] / [ASC]"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionOrderBy));

        public override QueryExpressionWithSuggestions Execute(Query query)
        {
            /// NOTE: Almost identical code in <see cref="QueryExpressionOrderBy"/> and <see cref="QueryExpressionThenBy"/>

            // TODO: Consider adding suggestions for sorting other fields
            // TODO: (must then sample collection, for instance first item)
            var retval = new QueryExpressionWithSuggestions(
                this,
                Descending ? null : new QueryExpressionOrderBy(Key, descending: true),
                !Descending ? null : new QueryExpressionOrderBy(Key, descending: false)
            );

            // NOTE:
            // Do not use type of objects.
            // Would often be RRich for instance, meaning no meaningful key (PRichId would be meaningless)
            // And could vary in a single collection due to inheritance.
            // var type = queryProgress.Details.FirstOrDefault()?.P.GetType();
            // if (type != null && (type.ToStringVeryShort() + "Id").Equals(Key.ToString())) {

            if ((query.Progress.StrCurrentType + "Id").Equals(Key.ToString()))
            {
                // If Key is something like 'CustomerId' in 'Customer/ORDER BY CustomerId/' 
                // or "MonthId" in 'PIVOT Month BY Product' 
                // then it is NOT FOUND within ikip.P but rather as ikip.Key
                if (query.Progress.Result.AsParallel().All(ikip =>
                { // Note use of AsParallell here.
                    /// HACK: Hack in order for _SUM from <see cref="QueryExpressionPivot"/> to not stop the rest from being sorted.
                    /// TODO: Find better solution for this
                    var s = ikip.Key.ToString();
                    if ("_SUM".Equals(s) || "[NULL]".Equals(s)) return true;
                    return UtilCore.DoubleTryParse(s, out _);
                }))
                {
                    // Use double comparision (because all values parse as doubles)
                    // Note performance issue here, if value was original integer or double it should be unnecessary to parse it again
                    query.Progress.Result = Descending ?
                        query.Progress.Result.OrderByDescending(ikip =>
                        {
                            /// HACK: Hack in order for _SUM from <see cref="QueryExpressionPivot"/> to not stop the rest from being sorted.
                            /// TODO: Find better solution for this
                            var s = ikip.Key.ToString();
                            if ("_SUM".Equals(s)) return Descending ? double.MinValue : double.MaxValue;
                            if ("[NULL]".Equals(s)) return Descending ? double.MaxValue : double.MinValue;
                            return UtilCore.DoubleParse(s);
                        }) :
                        query.Progress.Result.OrderBy(ikip =>
                        {
                            /// HACK: Hack in order for _SUM from <see cref="QueryExpressionPivot"/> to not stop the rest from being sorted.
                            /// TODO: Find better solution for this
                            var s = ikip.Key.ToString();
                            if ("_SUM".Equals(s)) return Descending ? double.MinValue : double.MaxValue;
                            if ("[NULL]".Equals(s)) return Descending ? double.MaxValue : double.MinValue;
                            return UtilCore.DoubleParse(s);
                        });
                }
                else
                {
                    query.Progress.Result = Descending ?
                        query.Progress.Result.OrderByDescending(ikip => ikip.Key.ToString()) :
                        query.Progress.Result.OrderBy(ikip => ikip.Key.ToString());
                }
            }
            else
            {
                if (!TryCheckForExistenceOfKey(query, Key, out var keyAsPK))
                {
                    return retval;
                }

                var key = keyAsPK ?? Key; // Use a more refined key if possible. This will for instance make TryGetP against PExact more efficient.

                /// NOTE: Supporting <see cref="CompoundKey"/> here has a somewhat marginal value.
                /// NOTE: It means that we will use as sorting key a key not existing in the collection 
                /// NOTE: (because if it did exist, like in 
                /// NOTE:   Order/SELECT CustomerId, Amount, Customer.FirstName/ORDER BY Customer.FirstName, 
                /// NOTE: then <see cref="CompoundKey"/> would not be necessary anyway).
                /// NOTE:
                /// NOTE: On the other hand, if field is absent for some entity, then a <see cref="CompoundInvalidKey"/>
                /// NOTE: needs to be created by <see cref="CompoundKey.Parse"/>, something which is OK to have cached.
                /// NOTE: 
                /// NOTE: And note need for actually including CustomerId above, for instance for
                /// NOTE:   Order/SELECT Amount, Customer.FirstName/ORDER BY Customer.FirstName, 
                /// NOTE: use of compound foreign key would break down, because the link to Customer-storage is not there.
                CompoundKey? cachedCompoundKey = null;

                // NOTE: Following code was experimental. It gave little if no performance increase
                // NOTE: (but with potentially increased temporary memory usage)
                // NOTE: It was therefore not taken into permanent use.
                //var useAlternativeOrdering = true;
                //if (useAlternativeOrdering) {
                //    var bag = new System.Collections.Concurrent.ConcurrentBag<(IKIP, double)>();
                //    bool terminate = false;
                //    queryProgress.Details.Result.AsParallel().ForAll(ikip => {
                //        if (terminate) {
                //            return;
                //        }
                //        if (!Util.DoubleTryParse(ikip.P.GetPVAsString(queryProgress.Details.DataStorage, key, ref cachedCompoundKey), out var dbl)) {
                //            terminate = true; // TODO: Is this thread-safe?
                //            return;
                //        }
                //        bag.Add((ikip, dbl));
                //    });
                //    if (!terminate) {
                //        // NOTE: We could also assert bag.Count against Result.Count
                //        // Use double comparision (because all values parse as doubles)
                //        queryProgress.Details.Result = bag.OrderBy(e => e.Item2).Select(e => e.Item1);
                //    } else {
                //        queryProgress.Details.Result = queryProgress.Details.Result.OrderBy(ikip =>
                //            ikip.P.GetPVAsString(queryProgress.Details.DataStorage, key, ref cachedCompoundKey));
                //    }
                //} else {
                if (query.Progress.Result.AsParallel().All(ikip => // Note use of AsParallell here.
                                                                   // TODO: Consider checking for type of PK (int, long etc), instead of going through whole collection like this.
                    !TryGetPVAsString(ikip, query.Progress.DataStorage, key, ref cachedCompoundKey, out var value) || UtilCore.DoubleTryParse(value, out _))
                )
                {
                    // Use double comparision (because all values parse as doubles or are null)
                    // Note performance issue here, if value was original integer or double it should be unnecessary to parse it again
                    query.Progress.Result = Descending ?
                        query.Progress.Result.OrderByDescending(ikip =>
                            TryGetPVAsString(ikip, query.Progress.DataStorage, key, ref cachedCompoundKey, out var value) ?
                            UtilCore.DoubleParse(GetPVAsString(ikip, query.Progress.DataStorage, key, ref cachedCompoundKey)) :
                            (Descending ? double.MaxValue : double.MinValue)
                        ) :
                        query.Progress.Result.OrderBy(ikip =>
                            TryGetPVAsString(ikip, query.Progress.DataStorage, key, ref cachedCompoundKey, out var value) ?
                            UtilCore.DoubleParse(GetPVAsString(ikip, query.Progress.DataStorage, key, ref cachedCompoundKey)) :
                            (Descending ? double.MaxValue : double.MinValue)
                        );
                }
                else
                {
                    // Use string comparision

                    // TODO: Make use of standardized .Net mechanisms for comparing / comparision.
                    // TODO: in order to understand more types than just double and string. 
                    query.Progress.Result = Descending ?
                        query.Progress.Result.OrderByDescending(ikip => GetPVAsString(ikip, query.Progress.DataStorage, key, ref cachedCompoundKey)) :
                        query.Progress.Result.OrderBy(ikip => GetPVAsString(ikip, query.Progress.DataStorage, key, ref cachedCompoundKey));
                }
                // }
            }
            /// Change 23 Jan 2022:
            /// Not possible after introduction of <see cref="QueryExpressionThenBy"/>
            /// because the result would then be IEnumerable{IKIP} instead of IOrderedEnumerable{IKIP}
            /// Instead Descending is taken into account above (this should also incidentally improve performance for a single ORDER BY ... DESC)
            //if (Descending)
            //{
            //    query.Progress.Result = query.Progress.Result.Reverse();
            //}
            return retval;
        }

        public IK Key { get; private set; }

        public bool Descending { get; private set; }

        public QueryExpressionOrderBy(IK key, bool descending)
        {
            Key = key;
            Descending = descending;
        }

        public new static QueryExpressionOrderBy Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryOrderByException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionOrderBy retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionOrderBy retval, out string errorResponse)
        {
            value = value.Trim().Replace("  ", " ");
            var t = UtilQuery.SplitOutsideOf(value, separator: " ", outsideOf: '\'');
            if (!"order".Equals(t[0].ToLower()))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'ORDER' as first word." + SyntaxHelp;
                return false;
            }
            if (t.Count != 3 && t.Count != 4)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not three or four words but " + t.Count + " words." + SyntaxHelp;
                return false;
            }
            if (!"by".Equals(t[1].ToLower()))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'BY' as second word." + SyntaxHelp;
                return false;
            }
            var t3 = t.Count == 4 ? t[3].ToLower() : "asc";
            if (!"asc".Equals(t3) && !"desc".Equals(t3))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'ASC' or 'DESC' as third word." + SyntaxHelp;
                return false;
            }

            retval = new QueryExpressionOrderBy(IKString.FromString(t[2]), "desc".Equals(t3));
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString() => "ORDER BY " + Key + (!Descending ? "" : " DESC");

        public class InvalidQueryOrderByException : ApplicationException
        {
            public InvalidQueryOrderByException(string message) : base(message) { }
            public InvalidQueryOrderByException(string message, Exception inner) : base(message, inner) { }
        }
    }

    /// <summary>
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionOrderByExtension
    {
        public static Query OrderBy(this Query query, string key, bool descending = false) =>
            OrderBy(query, IKString.FromString(key), descending);

        public static Query OrderBy(this Query query, IK key, bool descending = false) =>
            query.ExecuteOne(new QueryExpressionOrderBy(key, descending));
    }
}