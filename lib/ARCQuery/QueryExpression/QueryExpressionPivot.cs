﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ARCCore;

namespace ARCQuery {

    [Class(Description =
        "Creates a table by querying over two keys like\r\n" +
        "  For 'Order', 'PIVOT Product BY Year'.\r\n" +
        "or\r\n" +
        "  'PIVOT Product BY Year SUM Amount'.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-.\r\n" +
        "\r\n" +
        "See also:\r\n" +
        "-" + nameof(QueryExpressionAggregate) + "- which aggregates over only one key\r\n" +
        "and\r\n" +
        "-" + nameof(FunctionKeyAggregate) + "- which operates over a collection of properties.\r\n" +
        "\r\n" +
        "TODO: Nov 2020: There is lot of similar code in -" + nameof(QueryExpressionPivot) + "- and -" + nameof(QueryExpressionAggregate) + ".\r\n"
    )]
    public class QueryExpressionPivot : QueryExpression {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "1) 'PIVOT {rowKey} BY {columnKey}' like \"PIVOT Colour BY Production.Year\"\r\n" +
            "or\r\n" +
            "2) 'PIVOT {rowKey} BY {columnKey} {aggregationType} {aggregationKey}' like \"PIVOT Colour BY Production.Year SUM Price\""
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionPivot));

        public IK RowKey { get; private set; }
        public IK ColumnKey { get; private set; }

        [ClassMember(Description =
            "Most common value is -" + nameof(AggregationType.Count) + "-, " +
            "in which case -" + nameof(AggregationKey) + "- may be correspondingly set to null."
        )]
        public AggregationType AggregationType { get; private set; }

        [ClassMember(Description =
            "This is often null (with -" + nameof(AggregationType) + "- correspondingly set to " + nameof(AggregationType.Count) + "-).\r\n" +
            "Iteration then looks like\r\n" +
            "  'PIVOT Product BY Year'.\r\n" +
            "If not null then iteration looks like:\r\n" +
            "  'PIVOT Product BY Year SUM Amount'"
        )]
        public IK? AggregationKey { get; private set; }

        public override QueryExpressionWithSuggestions Execute(Query query) {
            // TODO: Note rooms for big improvements in processing speeds 
            // TODO: This is only a naïve first implementation of pivoting

            // TODO: Add support for aggregation type? Or row? Or column? Many possibilities here.
            var retval = new QueryExpressionWithSuggestions(this);

            // TOOD: Add parallell processing for TryCheckForExistenceOfField (3 threads)
            if (!TryCheckForExistenceOfKey(query, RowKey, out _)) {
                return retval;
            }

            if (!TryCheckForExistenceOfKey(query, ColumnKey, out _)) {
                return retval;
            }

            // NOTE: If AggregationKey was simply left out of a preceding "SELECT" then this check will not work. 
            // NOTE:
            // NOTE: Example, if the correct query is
            // NOTE:   Order/SELECT Customer.Country AS Country, ShippingOption AS Shipping, Amount/PIVOT Country BY Shipping SUM Amount
            // NOTE: but instead one wrote
            // NOTE:   Order/SELECT Customer.Country AS Country, ShippingOption AS Shipping/PIVOT Country BY Shipping SUM Amount
            // NOTE: then this will happily return "0" for all sums, since "Amount" was not included in preceding "SELECT".
            // NOTE: The reason for this is that TryCheckForExistenceOfKey sees that Amount is found in Schema for Order.
            // NOTE:    
            // NOTE: A similar weakness can also be found in the check above for RowKey and ColumnKey.
            if (AggregationKey != null && !TryCheckForExistenceOfKey(query, AggregationKey, out _)) {
                return retval;
            }

            // TOOD: Add parallell processing for finding values (two threads)
            // 1) Find all possible row values, including NULL
            var cachedCompoundKeyRow = (CompoundKey?)null;
            var rowValues = query.Progress.Result.AsParallel().Select(ikip => { // Note use of AsParallell here.
                return GetPVAsStringOrNull(ikip, query.Progress.DataStorage, RowKey, ref cachedCompoundKeyRow);
            }).Where(s => s != null).Distinct().Select(s => s!).Append("[NULL]").ToList();

            // 2) Find all possible column values, including NULL
            var cachedCompoundKeyColumn = (CompoundKey?)null;
            var columnValues = query.Progress.Result.AsParallel().Select(ikip => { // Note use of AsParallell here.
                return GetPVAsStringOrNull(ikip, query.Progress.DataStorage, ColumnKey, ref cachedCompoundKeyColumn);
            }).Where(s => s != null).Distinct().Select(s => s!).Append("[NULL]").ToList();

            // TODO: Create PK dynamically for column-values, of type long, double etc. as relevant.
            // TODO: (Instead of using IKString)
            // TODO: This will help with later sorting among other advantages.

            // 3) Ensure that not too complicated
            if ((rowValues.Count * columnValues.Count) > 1000000) { // This is more than for instance 1000 rows by 1000 columns.
                // NOTE: This limitation may be too conservative. Although we probably would never want to browse
                // NOTE: such a huge table, we will be interested in some ordered version with maybe top 100 rows or similar.
                // NOTE: If limitation causes problem, increase it by a factor of 10 each time until clear 
                // NOTE: issues regarding processing time and / or memory usage materializes themselves.
                throw new InvalidQueryExpressionPivotException(
                    "Too complicated result (too many possible values for rows and columns).\r\n" +
                    nameof(rowValues) + ".Count: " + rowValues.Count + "\r\n" +
                    nameof(columnValues) + ".Count: " + columnValues.Count + "\r\n"
                );
            }

            // TODO: Note rooms for big improvements in processing speeds 
            // TODO: This is only a naïve first implementation of iterating
            switch (AggregationType) {
                case AggregationType.Count: {
                        Aggregate<long>(query, rowValues, columnValues, aggregator: (d1, d2) => d1 + d2, zeroValue: 0L, GetCrossSummationChecker<long>(), countOnly: true);
                        break;

                        // TODO: Delete commented out code
                        // Old code, before introduction og generics 9 Mar 2021
                        //// First key in this dictionary is row value, second key is column value.
                        //var result = new Dictionary<string, Dictionary<string, long>>();
                        //rowValues.ForEach(rowValue => {
                        //    var columns = new Dictionary<string, long>();
                        //    result[rowValue] = columns;
                        //    columnValues.ForEach(columnValue => {
                        //        columns[columnValue] = 0;
                        //    });
                        //});

                        //var rowNullFound = false; // Decides if corresponding NULL index is needed 
                        //var columnNullFound = false;
                        //var totalElements = 0;
                        //query.Progress.Result.ForEach(ikip => { // Do the actual calculation NOTE: DO NOT USE AsParallell here! Not thread-safe code.
                        //    totalElements++;
                        //    var rowValue = GetPVAsString(ikip, query.Progress.DataStorage, RowKey, ref cachedCompoundKeyRow, "[NULL]");
                        //    if ("[NULL]".Equals(rowValue)) rowNullFound = true;
                        //    var columns = result.GetValue(rowValue);
                        //    var columnValue = GetPVAsString(ikip, query.Progress.DataStorage, ColumnKey, ref cachedCompoundKeyColumn, "[NULL]");
                        //    if ("[NULL]".Equals(columnValue)) columnNullFound = true;

                        //    // Support Sum by checking for value.
                        //    // Use duck-typing (long or double), without bothering with actual PK
                        //    columns[columnValue] = columns.GetValue(columnValue) + 1;

                        //    // TODO: Final HTML result should contain links for showing
                        //    // TODO: objects actually 'behind' the calculated value
                        //});
                        //var totalSum1 = 0L;
                        //var sumColumns = columnValues.ToDictionary(s => s, s => 0L);
                        //if (!rowNullFound) rowValues.RemoveAt(rowValues.Count - 1);           // Remove [NULL], not needed. TODO: Ensure that did not confuse row with column here!
                        //if (!columnNullFound) columnValues.RemoveAt(columnValues.Count - 1);  // Remove [NULL], not needed. TODO: Ensure that did not confuse row with column here!
                        //query.Progress.Result = rowValues.Select(rowValue => { // NOTE: DO NOT USE AsParallell here! Not thread-safe code.
                        //    var row = new PRich();
                        //    var columnsResult = result.GetValue(rowValue);
                        //    var sumRow = 0L;
                        //    columnValues.ForEach(columnValue => {
                        //        var thisPoint = columnsResult.GetValue(columnValue);
                        //        row.IP.AddPV(columnValue, thisPoint);
                        //        sumRow += thisPoint;

                        //        sumColumns[columnValue] = sumColumns.GetValue(columnValue) + thisPoint;
                        //    });
                        //    row.IP.AddPV("_SUM", sumRow); // NO: THIS DID NOT WORK: Tilde ¨ has ASCII value 126, increases chance of SUM being rightmost / downmost
                        //    totalSum1 += sumRow;
                        //    return new IKIP(IKString.FromString(rowValue), row);
                        //}).ToList(); // ToList is necessary because of 'side-effects' in Select (force evaluation now)
                        //var sumColumnsRow = new PRich();
                        //var totalSum2 = 0L;
                        //columnValues.ForEach(columnValue => {
                        //    var sumThisColumn = sumColumns.GetValue(columnValue);
                        //    sumColumnsRow.IP.AddPV(columnValue, sumThisColumn);
                        //    totalSum2 += sumThisColumn;
                        //});
                        //if (totalSum1 != totalSum2) {
                        //    throw new InvalidQueryExpressionPivotException("Cross summation failure, variant 1 (" + totalSum1 + " vs " + totalSum2 + ")");
                        //}
                        //if (AggregationType == AggregationType.Count) {
                        //    if (totalSum1 != totalElements) {
                        //        throw new InvalidQueryExpressionPivotException("Cross summation failure, variant 2 (" + totalSum1 + " vs " + totalElements + ")");
                        //    }
                        //}
                        //sumColumnsRow.IP.AddPV("_SUM", totalSum2);
                        //query.Progress.Result = query.Progress.Result.Append(
                        //    new IKIP(IKString.FromString("_SUM"), sumColumnsRow)
                        //);
                        //break;
                    }
                case AggregationType.Sum:
                    if (AggregationKey == null) throw new NullReferenceException(nameof(AggregationKey) + ": Supposed set for " + nameof(AggregationType) + " = " + AggregationType);

                    bool Try<T>(Func<IP, bool>? valueChecker, Func<T, T, T> aggregator, T zeroValue, Action<T, T>? crossSummationChecker = null) where T : notnull {
                        if (crossSummationChecker == null) crossSummationChecker = GetCrossSummationChecker<T>();
                        if (!QueryExpressionAggregate.IsSuitableAggregator<T>(query.Progress, AggregationKey ?? throw new NullReferenceException(nameof(AggregationKey)), valueChecker)) {
                            return false;
                        } else {
                            Aggregate<T>(query, rowValues!, columnValues!, aggregator, zeroValue, crossSummationChecker);
                            return true;
                        }
                    }

                    if (Try(valueChecker: null, aggregator: (t1, t2) => t1 + t2, zeroValue: TimeSpan.Zero)) {
                        // Finished
                    } else {
                        /// Decide between double, long and int.
                        if (Try(valueChecker: p => p is PValue<double>, aggregator: (i1, i2) => i1 + i2, zeroValue: 0D)) {
                            // Finished
                        } else if (Try(valueChecker: p => p is PValue<long>, aggregator: (i1, i2) => i1 + i2, zeroValue: 0L)) {
                            // Finished
                        } else if (Try(valueChecker: p => p is PValue<int>, aggregator: (i1, i2) => i1 + i2, zeroValue: 0)) {
                            // Finished
                        } else if (Try(valueChecker: p => p.TryGetV<long>(out _), aggregator: (l1, l2) => l1 + l2, zeroValue: 0L)) {
                            // Finished
                        } else {
                            // Aggregate doubles
                            Aggregate<double>(query, rowValues, columnValues, aggregator: (d1, d2) => d1 + d2, zeroValue: 0D, GetCrossSummationChecker<double>());
                        }
                    }
                    break;

                // TODO: Delete commented out code
                // Old code, before introduction og generics 9 Mar 2021
                //if (Math.Abs(totalSum1.Subtract(totalSum2).TotalMinutes) > 1) { // Note huge error margin here (since we are more interesting in catching blatant coding errors, not dwelving into the finer points of floating point maths
                //    throw new InvalidQueryExpressionPivotException("Cross summation failure, variant 1 (" + totalSum1 + " vs " + totalSum2 + ")");
                //}

                //var oneValueFound = false;
                //if (
                //    (AggregationKey is PK pk && pk.Type.Equals(typeof(TimeSpan))) ||
                //    (
                //        query.Progress.Result.AsParallel().All(ikip => { // Note use of AsParallell here.
                //            if (!ikip.TryGetP(query.Progress.DataStorage, AggregationKey, out var d, ref cachedCompoundKeyAggregation, out _)) {
                //                return true;
                //            }
                //            oneValueFound = true; // Note use of dubious side effect
                //            // TODO: Add explanation for why we try to parse as timespan here, even when key has been found to be of type Timespan
                //            return d is PValue<TimeSpan> || Util.TimeSpanTryParse(d.GetV(defaultValue: "00:00"), out _);
                //        }) &&
                //        oneValueFound // Note use of dubious side effect (avoid summing as timespan just because we have no values)
                //    )
                //) {
                //    // Sum as timespan
                //    // First key in this dictionary is row value, second key is column value.
                //    var result = new Dictionary<string, Dictionary<string, TimeSpan>>();
                //    rowValues.ForEach(rowValue => {
                //        var columns = new Dictionary<string, TimeSpan>();
                //        result[rowValue] = columns;
                //        columnValues.ForEach(columnValue => {
                //            columns[columnValue] = TimeSpan.Zero;
                //        });
                //    });

                //    var rowNullFound = false; // Decides if corresponding NULL index is needed 
                //    var columnNullFound = false;
                //    var totalElements = 0;
                //    query.Progress.Result.ForEach(ikip => { // Do the actual calculation NOTE: DO NOT USE AsParallell here! Not thread-safe code.
                //        totalElements++;
                //        var rowValue = GetPVAsString(ikip, query.Progress.DataStorage, RowKey, ref cachedCompoundKeyRow, "[NULL]");
                //        if ("[NULL]".Equals(rowValue)) rowNullFound = true;
                //        var columns = result.GetValue(rowValue);
                //        var columnValue = GetPVAsString(ikip, query.Progress.DataStorage, ColumnKey, ref cachedCompoundKeyColumn, "[NULL]");
                //        if ("[NULL]".Equals(columnValue)) columnNullFound = true;

                //        columns[columnValue] = columns.GetValue(columnValue) + (
                //            ikip.TryGetP(query.Progress.DataStorage, AggregationKey, out var d, ref cachedCompoundKeyAggregation, out _) ?
                //            d.GetV(TimeSpan.Zero) : TimeSpan.Zero
                //        );

                //        // TODO: Final HTML result should contain links for showing
                //        // TODO: objects actually 'behind' the calculated value
                //    });
                //    var totalSum1 = TimeSpan.Zero;
                //    var sumColumns = columnValues.ToDictionary(s => s, s => TimeSpan.Zero);
                //    if (!rowNullFound) rowValues.RemoveAt(rowValues.Count - 1);           // Remove [NULL], not needed. TODO: Ensure that did not confuse row with column here!
                //    if (!columnNullFound) columnValues.RemoveAt(columnValues.Count - 1);  // Remove [NULL], not needed. TODO: Ensure that did not confuse row with column here!
                //    query.Progress.Result = rowValues.Select(rowValue => { // NOTE: DO NOT USE AsParallell here! Not thread-safe code.
                //        var row = new PRich();
                //        var columnsResult = result.GetValue(rowValue);
                //        var sumRow = TimeSpan.Zero;
                //        columnValues.ForEach(columnValue => {
                //            var thisPoint = columnsResult.GetValue(columnValue);
                //            row.IP.AddPV(columnValue, thisPoint);
                //            sumRow += thisPoint;

                //            sumColumns[columnValue] = sumColumns.GetValue(columnValue) + thisPoint;
                //        });
                //        row.IP.AddPV("_SUM", sumRow); // NO: THIS DID NOT WORK: Tilde ¨ has ASCII value 126, increases chance of SUM being rightmost / downmost
                //        totalSum1 += sumRow;
                //        return new IKIP(IKString.FromString(rowValue), row);
                //    }).ToList(); // ToList is necessary because of 'side-effects' in Select (force evaluation now)
                //    var sumColumnsRow = new PRich();
                //    var totalSum2 = TimeSpan.Zero;
                //    columnValues.ForEach(columnValue => {
                //        var sumThisColumn = sumColumns.GetValue(columnValue);
                //        sumColumnsRow.IP.AddPV(columnValue, sumThisColumn);
                //        totalSum2 += sumThisColumn;
                //    });
                //    if (Math.Abs(totalSum1.Subtract(totalSum2).TotalMinutes) > 1) { // Note huge error margin here (since we are more interesting in catching blatant coding errors, not dwelving into the finer points of floating point maths
                //        throw new InvalidQueryExpressionPivotException("Cross summation failure, variant 1 (" + totalSum1 + " vs " + totalSum2 + ")");
                //    }
                //    sumColumnsRow.IP.AddPV("_SUM", totalSum2);
                //    query.Progress.Result = query.Progress.Result.Append(
                //        new IKIP(IKString.FromString("_SUM"), sumColumnsRow)
                //    );
                //} else {
                //    // Sum as double
                //    // First key in this dictionary is row value, second key is column value.
                //    var result = new Dictionary<string, Dictionary<string, double>>();
                //    rowValues.ForEach(rowValue => {
                //        var columns = new Dictionary<string, double>();
                //        result[rowValue] = columns;
                //        columnValues.ForEach(columnValue => {
                //            columns[columnValue] = 0;
                //        });
                //    });

                //    var rowNullFound = false; // Decides if corresponding NULL index is needed 
                //    var columnNullFound = false;
                //    var totalElements = 0;
                //    query.Progress.Result.ForEach(ikip => { // Do the actual calculation NOTE: DO NOT USE AsParallell here! Not thread-safe code.
                //        totalElements++;
                //        var rowValue = GetPVAsString(ikip, query.Progress.DataStorage, RowKey, ref cachedCompoundKeyRow, "[NULL]");
                //        if ("[NULL]".Equals(rowValue)) rowNullFound = true;
                //        var columns = result.GetValue(rowValue);
                //        var columnValue = GetPVAsString(ikip, query.Progress.DataStorage, ColumnKey, ref cachedCompoundKeyColumn, "[NULL]");
                //        if ("[NULL]".Equals(columnValue)) columnNullFound = true;

                //        columns[columnValue] = columns.GetValue(columnValue) + (
                //            ikip.TryGetP(query.Progress.DataStorage, AggregationKey, out var d, ref cachedCompoundKeyAggregation, out _) ?
                //            d.GetV(0d) : 0d
                //        );

                //        // TODO: Final HTML result should contain links for showing
                //        // TODO: objects actually 'behind' the calculated value
                //    });
                //    var totalSum1 = 0d;
                //    var sumColumns = columnValues.ToDictionary(s => s, s => 0d);
                //    if (!rowNullFound) rowValues.RemoveAt(rowValues.Count - 1);           // Remove [NULL], not needed. TODO: Ensure that did not confuse row with column here!
                //    if (!columnNullFound) columnValues.RemoveAt(columnValues.Count - 1);  // Remove [NULL], not needed. TODO: Ensure that did not confuse row with column here!
                //    query.Progress.Result = rowValues.Select(rowValue => { // NOTE: DO NOT USE AsParallell here! Not thread-safe code.
                //        var row = new PRich();
                //        var columnsResult = result.GetValue(rowValue);
                //        var sumRow = 0d;
                //        columnValues.ForEach(columnValue => {
                //            var thisPoint = columnsResult.GetValue(columnValue);
                //            row.IP.AddPV(columnValue, thisPoint);
                //            sumRow += thisPoint;

                //            sumColumns[columnValue] = sumColumns.GetValue(columnValue) + thisPoint;
                //        });
                //        row.IP.AddPV("_SUM", sumRow); // NO: THIS DID NOT WORK: Tilde ¨ has ASCII value 126, increases chance of SUM being rightmost / downmost
                //        totalSum1 += sumRow;
                //        return new IKIP(IKString.FromString(rowValue), row);
                //    }).ToList(); // ToList is necessary because of 'side-effects' in Select (force evaluation now)
                //    var sumColumnsRow = new PRich();
                //    var totalSum2 = 0d;
                //    columnValues.ForEach(columnValue => {
                //        var sumThisColumn = sumColumns.GetValue(columnValue);
                //        sumColumnsRow.IP.AddPV(columnValue, sumThisColumn);
                //        totalSum2 += sumThisColumn;
                //    });
                //    if (Math.Abs(totalSum1 - totalSum2) > 1) { // Note huge error margin here (since we are more interesting in catching blatant coding errors, not dwelving into the finer points of floating point maths
                //        throw new InvalidQueryExpressionPivotException("Cross summation failure, variant 1 (" + totalSum1 + " vs " + totalSum2 + ")");
                //    }
                //    sumColumnsRow.IP.AddPV("_SUM", totalSum2);
                //    query.Progress.Result = query.Progress.Result.Append(
                //        new IKIP(IKString.FromString("_SUM"), sumColumnsRow)
                //    );
                //}
                //break;
                default:
                    throw new InvalidEnumException(AggregationType, "Not implemented");
            }
            // This is somewhat dubious, but is enables further processing (sorting by row value for instance)
            query.Progress.StrCurrentType = RowKey.ToString();
            return retval;
        }

        [ClassMember(Description =
            "Returns a cross sum checker for int, long, double, Timespan.\r\n" +
            "\r\n" +
            "Throws an exception for other types.\r\n" +
            "\r\n" +
            "The cross checker throws an exeption if the two sums given are not equal, or (in the case of floating point values) differ too much.\r\n" +
            "(The floating point checks are performed with a huge margin since we are more interesting in catching blatant coding errors, " +
            "not dwelving into the finer points of floating point maths.)"
        )]
        public static Action<T, T> GetCrossSummationChecker<T>() {
            if (typeof(T).Equals(typeof(int))) {
                return (Action<T, T>)(object)new Action<int, int>((sum1, sum2) => {
                    if (sum1 != sum2) throw new InvalidCrossSummationException<int>(sum1, sum2);
                });
            } else if (typeof(T).Equals(typeof(long))) {
                return (Action<T, T>)(object)new Action<long, long>((sum1, sum2) => {
                    if (sum1 != sum2) throw new InvalidCrossSummationException<long>(sum1, sum2);
                });
            } else if (typeof(T).Equals(typeof(double))) {
                return (Action<T, T>)(object)new Action<double, double>((sum1, sum2) => {
                    if (Math.Abs(sum1 - sum2) > 1) throw new InvalidCrossSummationException<double>(sum1, sum2);
                });
            } else if (typeof(T).Equals(typeof(TimeSpan))) {
                return (Action<T, T>)(object)new Action<TimeSpan, TimeSpan>((sum1, sum2) => {
                    if (Math.Abs((sum1 - sum2).TotalMinutes) > 1) throw new InvalidCrossSummationException<TimeSpan>(sum1, sum2);
                });
            } else {
                throw new InvalidTypeException(typeof(T), "Expected, int, long, double or TimeSpan");
            }
        }

        public class InvalidCrossSummationException<T> : ApplicationException {
            public InvalidCrossSummationException(T sum1, T sum2) : base("Cross summation failure (type = " + typeof(T).ToStringShort() + "), the two sums " + sum1 + " and " + sum2 + " are not equal / differ too much") { }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="rowValues"></param>
        /// <param name="columnValues"></param>
        /// <param name="aggregator"></param>
        /// <param name="zeroValue"></param>
        /// <param name="crossSummationChecker">
        /// See <see cref="GetCrossSummationChecker{T}"/> for some standard checkers available.
        /// </param>
        /// <param name="countOnly"></param>
        public void Aggregate<T>(Query query, List<string> rowValues, List<string> columnValues, Func<T, T, T> aggregator, T zeroValue, Action<T, T> crossSummationChecker, bool countOnly = false) where T : notnull {
            if (countOnly) InvalidTypeException.AssertEquals(typeof(T), typeof(long), () => nameof(countOnly));
            if (!countOnly && AggregationKey == null) throw new ArgumentNullException(nameof(AggregationKey) + ", can only be null when " + nameof(countOnly) + " = TRUE");
            var one = countOnly ? (T)(object)1L : zeroValue; /// Note type assertion in constructor

            // TODO: Note rooms for big improvements in processing speeds 
            // TODO: This is only a naïve first implementation of iterating

            var cachedCompoundKeyAggregation = (CompoundKey?)null;
            var cachedCompoundKeyRow = (CompoundKey?)null;
            var cachedCompoundKeyColumn = (CompoundKey?)null;

            // First key in this dictionary is row value, second key is column value.
            var result = new Dictionary<string, Dictionary<string, T>>();
            rowValues.ForEach(rowValue => {
                var columns = new Dictionary<string, T>();
                result[rowValue] = columns;
                columnValues.ForEach(columnValue => {
                    columns[columnValue] = zeroValue;
                });
            });

            var rowNullFound = false; // Decides if corresponding NULL index is needed 
            var columnNullFound = false;
            var totalElements = 0;
            query.Progress.Result.ForEach(ikip => { // Do the actual calculation NOTE: DO NOT USE AsParallell here! Not thread-safe code.
                totalElements++;
                var rowValue = GetPVAsString(ikip, query.Progress.DataStorage, RowKey, ref cachedCompoundKeyRow, "[NULL]");
                if ("[NULL]".Equals(rowValue)) rowNullFound = true;
                var columns = result.GetValue(rowValue);
                var columnValue = GetPVAsString(ikip, query.Progress.DataStorage, ColumnKey, ref cachedCompoundKeyColumn, "[NULL]");
                if ("[NULL]".Equals(columnValue)) columnNullFound = true;

                columns[columnValue] = aggregator(columns.GetValue(columnValue),
                    countOnly ? one :
                   (
                        ikip.TryGetP(
                            query.Progress.DataStorage,
                            AggregationKey!, // ! is necessary because as of Mar 2021 compiler is not smart enough to catch null check above
                            out var p,
                            ref cachedCompoundKeyAggregation,
                            out _) ?
                        p.GetV(zeroValue) : zeroValue
                    )
                );

                // TODO: Final HTML result should contain links for showing
                // TODO: objects actually 'behind' the calculated value
            });
            var totalSum1 = zeroValue;
            var sumColumns = columnValues.ToDictionary(s => s, s => zeroValue);
            if (!rowNullFound) rowValues.RemoveAt(rowValues.Count - 1);           // Remove [NULL], not needed. TODO: Ensure that did not confuse row with column here!
            if (!columnNullFound) columnValues.RemoveAt(columnValues.Count - 1);  // Remove [NULL], not needed. TODO: Ensure that did not confuse row with column here!
            query.Progress.Result = rowValues.Select(rowValue => { // NOTE: DO NOT USE AsParallell here! Not thread-safe code.
                var row = new PRich();
                var columnsResult = result.GetValue(rowValue);
                var sumRow = zeroValue;
                columnValues.ForEach(columnValue => {
                    var thisPoint = columnsResult.GetValue(columnValue);
                    row.IP.AddPV(columnValue, thisPoint);
                    sumRow = aggregator(sumRow, thisPoint);

                    sumColumns[columnValue] = aggregator(sumColumns.GetValue(columnValue), thisPoint);
                });
                row.IP.AddPV("_SUM", sumRow); // NO: THIS DID NOT WORK: Tilde ¨ has ASCII value 126, increases chance of SUM being rightmost / downmost
                totalSum1 = aggregator(totalSum1, sumRow);
                return new IKIP(IKString.FromString(rowValue), row);
            }).ToList(); // ToList is necessary because of 'side-effects' in Select (force evaluation now)
            var sumColumnsRow = new PRich();
            var totalSum2 = zeroValue;
            columnValues.ForEach(columnValue => {
                var sumThisColumn = sumColumns.GetValue(columnValue);
                sumColumnsRow.IP.AddPV(columnValue, sumThisColumn);
                totalSum2 = aggregator(totalSum2, sumThisColumn);
            });
            crossSummationChecker(totalSum1, totalSum2);
            sumColumnsRow.IP.AddPV("_SUM", totalSum2);
            query.Progress.Result = query.Progress.Result.Append(
                new IKIP(IKString.FromString("_SUM"), sumColumnsRow)
            );
        }

        public new static QueryExpressionPivot Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryExpressionPivotException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionPivot retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionPivot retval, out string errorResponse) {
            value = value.Trim();
            if (!value.ToLower().StartsWith("pivot ")) { /// TODO: Move code here into <see cref="QueryIdFieldIterator"/>
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Does not start with 'PIVOT '";
                return false;
            }

            var t = UtilQuery.SplitOutsideOf(value, separator: " ", outsideOf: '\'').Select(s => s.Trim()).Where(s => !string.IsNullOrEmpty(s)).ToList();
            if (t.Count != 4 && t.Count != 6) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Not four items or six items but " + t.Count + " items." + SyntaxHelp;
                return false;
            }

            var rowKey = IKString.FromString(t[1]); // NOTE: We accept quite weakly typing here, we do not attempt to find PK for instance

            if (!"by".Equals(t[2].ToLower())) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Literal 'BY' not found as third element, found '" + t[2] + "'." + SyntaxHelp;
                return false;
            }

            var columnKey = IKString.FromString(t[3]); // NOTE: We accept quite weakly typing here, we do not attempt to find PK for instance

            if (t.Count == 4) { // We are finished
                retval = new QueryExpressionPivot(rowKey, columnKey, AggregationType.Count, aggregationKey: null);
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            } else if (t.Count == 6) { // Continue with parsing last two parameters. 
                                       // Continue parsing
                if (!UtilCore.EnumTryParse<AggregationType>(t[4], out var aggregationType)) {
                    /// Note second attempt, which will accept all-upper case like SUM, MEDIAN (assuming only the first letter is upper case in the actual enum-string). 
                    /// (this gives a more natural syntax)
                    if (!UtilCore.EnumTryParse(t[4][..1] + t[4][1..].ToLower(), out aggregationType)) {
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        errorResponse = "Invalid {aggregationType} (" + t[4] + "). Use one of " + string.Join(", ", UtilCore.EnumGetMembers<AggregationType>()) + "." + SyntaxHelp;
                        return false;
                    }
                }
                var aggregationKey = IKString.FromString(t[5]);

                retval = new QueryExpressionPivot(rowKey, columnKey, aggregationType, aggregationKey);
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            } else {
                throw new InvalidCountException("Found  " + t.Count + ", expected 4 or 6");
            }
        }

        public QueryExpressionPivot(IK rowKey, IK columnKey, AggregationType aggregationType = AggregationType.Count, IK? aggregationKey = null) {
            RowKey = rowKey ?? throw new NullReferenceException(nameof(rowKey));
            // From AgoRapide 2017, not in use here (overly complicated)
            // ColumnType = columnType ?? throw new NullReferenceException(nameof(columnType));
            ColumnKey = columnKey ?? throw new NullReferenceException(nameof(columnKey));
            AggregationType = aggregationType;
            if (aggregationKey == null && aggregationType != AggregationType.Count) throw new ArgumentNullException(nameof(aggregationKey) + ". (null is only allowed for -" + nameof(aggregationType) + "- " + nameof(AggregationType.Count) + ", not " + aggregationType);
            AggregationKey = aggregationKey;
        }

        public override string ToString() =>
            "PIVOT " + RowKey.ToString() + " BY " +
            // ColumnType.ToStringVeryShort() + "." + 
            ColumnKey.ToString() +
            (AggregationKey == null ?
                "" :
                (" " + AggregationType.ToString().ToUpper() + " " + AggregationKey.ToString()) /// Note upper-case for field <see cref="AggregationType"/>
            );

        public static new void EnrichKey(PK key) => QueryExpression.EnrichKey(key);

        public class InvalidQueryExpressionPivotException : ApplicationException {
            public InvalidQueryExpressionPivotException(string message) : base(message) { }
            public InvalidQueryExpressionPivotException(string message, Exception inner) : base(message, inner) { }
        }
    }

    /// <summary>
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionPivotExtension {
        public static Query Pivot(this Query query, IK rowKey, IK columnKey, AggregationType aggregationType = AggregationType.Count, IK? aggregationKey = null) =>
            query.ExecuteOne(new QueryExpressionPivot(rowKey, columnKey, aggregationType, aggregationKey));
    }
}