﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ARCCore;

namespace ARCQuery {

    [Class(Description =
        "Takes a specific number of elements from the current query result.\r\n" +
        // NO, see Shuffle instead
        // "If the modifier RND is specified then will sample randomly from the collection, up to the number of specified elements.\r\n" +
        "\r\n" +
        "See also -" + nameof(QueryExpressionLimit) + "- which limits the final HTML presentation of results.\r\n" +
        "\r\n" +
        "Useful in combination with either -" + nameof(QueryExpressionSkip) + "- or -" + nameof(QueryExpressionShuffle) + "-.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class QueryExpressionTake : QueryExpression {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "TAKE {Count}\r\n" +
            // NO, see Shuffle instead
            // "TAKE {Count} [RND]\r\n" +
            "Examples:\r\n" +
            "'TAKE 42'\r\n"
        // NO, see Shuffle instead
        // "'TAKE 42 RND'"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionTake));

        public int Count { get; private set; }
        public bool IsRnd { get; private set; }

        public QueryExpressionTake(int count) => // , bool isRnd = false) {
            Count = count;
        // NO, see Shuffle instead
        //    IsRnd = isRnd;
        //}

        public override QueryExpressionWithSuggestions Execute(Query query) {
            // TODO: Add code from AgoRapide 2017, sampling over the collection.
            // if (IsRnd) throw new NotImplementedException(nameof(IsRnd));
            query.Progress.Result = query.Progress.Result.Take(Count);
            return new QueryExpressionWithSuggestions(
                this,
                previous: Count <= 1 ? null : new QueryExpressionTake(Count / 2),
                next: Count == query.Progress.Limit ? null : new QueryExpressionTake(Count * 2 > query.Progress.Limit ? 
                    query.Progress.Limit :
                    Count * 2)
                );
        }

        public new static QueryExpressionTake Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryExpressionTakeException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionTake retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionTake retval, out string errorResponse) {
            value = value.Trim().Replace("  ", " ");
            var t = value.Split(" ");
            if (!"take".Equals(t[0].ToLower())) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'TAKE' as first word." + SyntaxHelp;
                return false;
            }
            if (t.Length != 2) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not two words like 'TAKE {count}' but " + t.Length + " words." + SyntaxHelp;
                return false;
            }
            if (!int.TryParse(t[1], out var count) || count <= 0) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Illegal {count}, not a valid positive integer." + SyntaxHelp;
                return false;
            }
            retval = new QueryExpressionTake(count);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString() => "TAKE " + Count;

        public class InvalidQueryExpressionTakeException : ApplicationException {
            public InvalidQueryExpressionTakeException(string message) : base(message) { }
            public InvalidQueryExpressionTakeException(string message, Exception inner) : base(message, inner) { }
        }
    }

    /// <summary>
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionTakeExtension {
        public static Query Take(this Query query, int count) =>
            query.ExecuteOne(new QueryExpressionTake(count));
    }
}