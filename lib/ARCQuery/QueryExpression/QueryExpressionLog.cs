﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ARCCore;

namespace ARCQuery {

    [Class(Description =
        "A debugging tool used to extract detailed log information about the execution of the query.\r\n" +
        "\r\n" +
        "Gives as result the content of -" + nameof(Query.AllLogEntriesForThisQuery) + "-.\r\n" +
        "\r\n" +
        "Useful in circumstances where the query is considered successful from the view of -" + nameof(ARComponents.ARCQuery) +"- " +
        "but the user need to debug how the result was achieved.\r\n" +
        "(Note that if a query fails then -" + nameof(Query.Progress.ToHTMLSimpleSingle) + "- will show the log result anyway).\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class QueryExpressionLog : QueryExpression {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'Log'."
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionLog));

        public override QueryExpressionWithSuggestions Execute(Query query) {
            var retval = new QueryExpressionWithSuggestions(this);
            var logId = 1;
            var key = IKString.FromCache("LogText");
            query.Progress.StrCurrentType = "Log";
            query.Progress.Result = query.AllLogEntriesForThisQuery.Select(s => new IKIP(
                IKString.FromCache(logId++.ToString()), 
                new Func<IP>(() => {
                    var retval = new PRich();
                    retval.IP.AddP(key, new PValue<string>(s));
                    return retval;
                })()
            )).ToList(); // ToList in order to avoid deferred execution
            return retval;
        }

        public QueryExpressionLog() { }

        public new static QueryExpressionLog Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryExpressionLogException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionLog retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionLog retval, out string errorResponse) {
            value = value.Trim();
            if (!"log".Equals(value.ToLower())) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'Log'." + SyntaxHelp;
                return false;
            }
            retval = new QueryExpressionLog();
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString() => "Log";

        public class InvalidQueryExpressionLogException : ApplicationException {
            public InvalidQueryExpressionLogException(string message) : base(message) { }
            public InvalidQueryExpressionLogException(string message, Exception inner) : base(message, inner) { }
        }
    }

    /// <summary>
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionLogExtension {
        public static Query Log(this Query query) => query.ExecuteOne(new QueryExpressionLog());
    }
}