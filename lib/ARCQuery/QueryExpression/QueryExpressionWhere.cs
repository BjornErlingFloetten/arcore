﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ARCCore;

namespace ARCQuery
{
    [Class(
        Description =
            "Executes a 'WHERE {key} {operator} {value}' against the given collection.\r\n" +
            "\r\n" +
            "This class offers extensive functionality.\r\n" +
            "TODO: Document functionality better.\r\n" +
            "\r\n" +
            "See -" + nameof(FunctionKey) + "- for possible functions that can be applied to properties.\r\n" +
            "\r\n" +
            "See -" + nameof(ARCQuery.RelationalOperator) + "- for list of possible operators.\r\n" +
            "\r\n" +
            "Understands use of -" + nameof(ValueComparer) + "- like -" + nameof(ValueComparerDateTime) + "-\r\n" +
            "\r\n" +
            "TODO: Expand syntax, in order to support 'AND' and 'OR'. " +
            "TODO: Currently 'AND' can be simulated by chaining multiple -" + nameof(QueryExpressionWhere) + "-, but not 'OR'.\r\n" +
            "\r\n" +
            "TODO: Allow 'IS NULL' and 'IS NOT NULL' in addition to 'EQ NULL' and 'NEQ NULL'\r\n" +
            "\r\n" +
            "TODO: Include support for regular expressions (but keep easy syntax for 'ordinary' users).\r\n" +
            "\r\n" +
            "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class QueryExpressionWhere : QueryExpression
    {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "WHERE {key} {operator} {value}\r\n" +
            "Examples:\r\n" +
            "\"WHERE FirstName LIKE 'John*'\", \r\n" +
            "\"WHERE Colour = Red\",\r\n" +
            "\"WHERE Created = LastWeek\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionWhere));

        public override QueryExpressionWithSuggestions Execute(Query query)
        {

            // Note: Is is important to call GetPrevious / GetNext BEFORE we execute "ourselves".
            List<object>? distinctValues = null;
            var retval = new QueryExpressionWithSuggestions(this, GetPrevious(query, ref distinctValues), GetNext(query, ref distinctValues));

            if ((query.Progress.StrCurrentType + "Id").Equals(Key.ToString()))
            {
                // If Key is something like 'CustomerId' in 'Customer/ORDER BY CustomerId/' 
                // or "MonthId" in 'PIVOT Month BY Product' 
                // then it is NOT FOUND within ikip.P but rather as ikip.Key
                query.Progress.Result = query.Progress.Result.AsParallel().Where(ikip =>
                {
                    if (!TryGetValue(query.Progress.DataStorage, ikip, out var value))
                    {
                        return false;
                    }
                    return IsMatch(value, new PValue<string>(ikip.Key.ToString()));
                });
            }
            else
            {
                if (!TryCheckForExistenceOfKey(query, Key, out var keyAsPK))
                {
                    return retval;
                }

                switch (Value)
                {
                    /// TODO: Consider looking for ValueComparer here, and implement ToDebugProperties as abstract method on that class instead.
                    case ValueComparerDateTime v:
                        // This might be difficult to understand for users. Log in order to explain clearly what is going on
                        query.Progress.IP.LogText("Comparing against " + v.ToString());
                        query.Progress.IP.Log(v.ToDebugProperties);
                        break;
                }

                CompoundKey? cachedCompoundKey = null;
                query.Progress.Result = query.Progress.Result.AsParallel().Where(ikip => // Note use of AsParallell here.

                    // ====================
                    // TODO: Clean up text here.
                    // ====================
                    /// NOTE: Supporting <see cref="CompoundKey"/> here has a somewhat marginal value.
                    /// NOTE: It means that we will use as selection key a key not existing in the collection 
                    /// NOTE: (because if it did exist, like in Order/SELECT CustomerId, Amount, Customer.FirstName/WHERE Customer.FirstName = 'John'
                    /// NOTE: then <see cref="CompoundKey"/> would not be necessary anyway).
                    /// NOTE:
                    /// NOTE: On the other hand, if field is absent for some entity, then a <see cref="CompoundInvalidKey"/>
                    /// NOTE: needs to be created by <see cref="CompoundKey.Parse"/>, something which is OK to have cached.
                    /// NOTE:
                    /// NOTE: And note need for actually including CustomerId above, for instance for
                    /// NOTE:   Order/SELECT Amount/WHERE Customer.FirstName = 'John'
                    /// NOTE: use of compound foreign key would break down, because the link to Customer-storage is not there.
                    IsMatch(query.Progress.DataStorage, ikip, ref cachedCompoundKey)
                );
            }
            return retval;
        }

        [ClassMember(Description =
            "Returns all possible distinct values that -" + nameof(Key) + "- can describe in the given collection.\r\n" +
            "\r\n" +
            "Used by -" + nameof(GetPrevious) + "- and -" + nameof(GetNext) + "-.\r\n" +
            "\r\n" +
            "Will return either string values or values of type -" + nameof(ValueEnum) + "-.\r\n"
        )]
        private List<object> GetDistinctValuesForKey(IP dataStorage, IEnumerable<IKIP> collection)
        {
            CompoundKey? cachedCompoundKey = null;
            var nullFound = false;
            // var notNullFound = false;
            var retval = collection.Select(ikip =>
            {
                var b = ikip.TryGetP(dataStorage, Key, out var p, ref cachedCompoundKey, out _);
                if (!b)
                {
                    nullFound = true;
                    return null;
                }
                b = p.TryGetV<string>(out var str);
                if (!b)
                {
                    nullFound = true;
                    return null;
                }
                // notNullFound = true;
                return (object)str;
            }).Distinct().Where(s => s != null).Select(s => s!).OrderBy(s => s).ToList();
            switch (Operator)
            {
                /// TODO: Support IS (like IS NULL) and IS NOT (like IS NOT NULL)
                case RelationalOperator.EQ:
                case RelationalOperator.NEQ:
                    if (nullFound)
                    {
                        retval.Add(ValueEnum.NULL);
                        retval.Add(ValueEnum.NOTNULL);
                    }
                    // if (notNullFound) retval.Add(ValueEnum.NOTNULL);
                    retval.Add(ValueEnum.ALL);
                    break;
            }
            return retval;
        }

        public QueryExpressionWhere? GetPrevious(Query query, ref List<object>? distinctValues)
        {
            switch (Value)
            {
                case ValueComparer vc:
                    if (vc.TryGetPrevious(out var n))
                    {
                        return new QueryExpressionWhere(Key, Operator, n);
                    }
                    break;
            }
            if (Key is PK pk)
            {
                /// NOTE: Does not work as of Jun 2020 because <see cref="TryParse"/> only operates with <see cref="IKString"/>
                /// NOTE: Possible solutions:
                /// NOTE: 1) Implement PK in TryParse (will require it to know type of entities being queried)
                /// NOTE: 2) Implement range-checking of values (if HasLimitedRange or similar), and use that. Pass Collection here for this to work.
                /// NOTE: 3) Look for enum type equal to Key (more brittle, because fieldname does not have to coincide with enum type name)
                if (Value != null && pk.Type.IsEnum && UtilCore.EnumTryParse(pk.Type, Value.ToString(), out var e) && ((int)e) > 0)
                {
                    // Find previous enum value
                    return new QueryExpressionWhere(Key, Operator, UtilCore.EnumParse(pk.Type, ((int)e - 1).ToString()));
                }
            }
            if (!query.Progress.IP.GetPV<bool>(QueryProgressP.CreateHints))
            {
                // Do not bother because somewhat expensive to continue now
                return null;
            }
            switch (Operator)
            {
                case RelationalOperator.EQ:
                case RelationalOperator.NEQ:
                    if (distinctValues == null) distinctValues = GetDistinctValuesForKey(query.Progress.DataStorage, query.Progress.Result);
                    if (distinctValues.Count == 0) return null;
#pragma warning disable CS8604 // Possible null reference argument.
                    var thisIndex = distinctValues.IndexOf(Value); // TODO: WHY DO WE GET A "Possible null reference" warning here?
#pragma warning restore CS8604 // Possible null reference argument.
                    return thisIndex <= 0 ?
                        null :
                        new QueryExpressionWhere(Key, Operator, distinctValues[thisIndex - 1]);
            }
            return null;
        }

        public QueryExpressionWhere? GetNext(Query query, ref List<object>? distinctValues)
        {
            switch (Value)
            {
                case ValueComparer vc:
                    if (vc.TryGetNext(out var n))
                    {
                        return new QueryExpressionWhere(Key, Operator, n);
                    }
                    break;
            }
            if (Key is PK pk)
            {
                /// NOTE: Does not work as of Jun 2020 because <see cref="TryParse"/> only operates with <see cref="IKString"/>
                /// NOTE: See <see cref="TryGetPrevious"/> for possible solutions.
                if (Value != null && pk.Type.IsEnum && UtilCore.EnumTryParse(pk.Type, Value.ToString(), out var e) && ((int)e) < (UtilCore.EnumGetMembers(pk.Type).Count - 1))
                {
                    // Find next enum value
                    return new QueryExpressionWhere(Key, Operator, UtilCore.EnumParse(pk.Type, ((int)e + 1).ToString()));
                }
            }
            if (!query.Progress.IP.GetPV<bool>(QueryProgressP.CreateHints))
            {
                // Do not bother because somewhat expensive to continue now
                return null;
            }
            switch (Operator)
            {
                case RelationalOperator.EQ:
                case RelationalOperator.NEQ:
                    if (distinctValues == null) distinctValues = GetDistinctValuesForKey(query.Progress.DataStorage, query.Progress.Result);
                    if (distinctValues.Count == 0) return null;
#pragma warning disable CS8604 // Possible null reference argument.
                    var thisIndex = distinctValues.IndexOf(Value); // TODO: WHY DO WE GET A "Possible null reference" warning here?
#pragma warning restore CS8604 // Possible null reference argument.
                    return thisIndex == -1 ?
                        new QueryExpressionWhere(Key, Operator, distinctValues[0]) :
                        (thisIndex < distinctValues.Count - 1 ? new QueryExpressionWhere(Key, Operator, distinctValues[thisIndex + 1]) : null);
            }
            return null;
        }

        public IK Key { get; private set; }
        public RelationalOperator Operator { get; private set; }

        [ClassMember(
            Description =
                "The Value should ideally be as strongly typed as possible. " +
                "Note that it may be of type -" + nameof(ValueEnum) + "- relevant for queries like WHERE Name = NULL.\r\n" +
                "\r\n" +
                "TODO: From 16 Mar 2021 may also be a key, meaning IsMatch should extract the corresponding value."
        )]
        public object Value { get; private set; }

        [ClassMember(Description =
            "Value to use for operators -" + nameof(RelationalOperator.LIKE) + "- and -" + nameof(RelationalOperator.ILIKE) + "-." +
            "The original Value like 'ABC*', '*ABC' or '*ABC*' is represented here as 'ABC' (or 'abc' for ILIKE).\r\n"
        )]
        private readonly string? _valueLike;

        private readonly WildcardPositionType _wildcardPosition;

        [Enum(Description = 
            "Position of wildcard in value, " +
            "like '*ABC' (-" + nameof(AtStart) + "-), " +
            "'ABC*' (-" + nameof(AtEnd) + "-) and\r\n" +
            "'*ABC*' (-" + nameof(AtStartAndEnd) + "-).\r\n"
        )]
        private enum WildcardPositionType
        {
            __invalid,
            [EnumMember(Description = "Example: '*ABC'.\r\n")]
            AtStart,
            [EnumMember(Description = "Example: 'ABC*'.\r\n")]
            AtEnd,
            [EnumMember(Description = "Example: '*ABC*'.\r\n")]
            AtStartAndEnd
        }

        private CompoundKey? _valueAsCachedCompoundKey = null;

        public new static QueryExpressionWhere Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryExpressionWhereException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionWhere retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionWhere retval, out string errorResponse)
        {
            value = value.Trim();
            if (!value.ToLower().StartsWith("where "))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Does not starte with 'WHERE '";
                return false;
            }

            var pos = 0;
            value += " "; // Simplifies parsing
            var nextWord = new Func<string?>(() =>
            {
                var nextPos = value.IndexOf(' ', pos);
                if (nextPos == -1) return null;
                var word = value[pos..nextPos];
                pos = nextPos + 1;
                return word;
            });

            nextWord(); var strKey = nextWord();
            if (strKey == null)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "No {key} given." + SyntaxHelp;
                return false;
            }
            var key = IKString.FromString(strKey); // TODO: Attempt to find PK instead?

            var strOperator = nextWord();
            if (strOperator == null)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "No {operator} given. Use one of: " + string.Join(", ", UtilCore.EnumGetMembers<RelationalOperator>()) + "." + SyntaxHelp;
                return false;
            }
            if (UtilCore.EnumTryParse<RelationalOperator>(strOperator, out var _operator))
            {
                // OK
            }
            else
            {
                switch (strOperator)
                {
                    case "<": _operator = RelationalOperator.LT; break;
                    case "<=": _operator = RelationalOperator.LEQ; break;
                    case "=": _operator = RelationalOperator.EQ; break;
                    case "==": _operator = RelationalOperator.EQ; break;
                    case "!=": _operator = RelationalOperator.NEQ; break;
                    case "<>": _operator = RelationalOperator.NEQ; break;
                    case ">=": _operator = RelationalOperator.GEQ; break;
                    case ">": _operator = RelationalOperator.GT; break;
                    default:
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        errorResponse = "Invalid {operator} (" + strOperator + "). Use one of: " + string.Join(", ", UtilCore.EnumGetMembers<RelationalOperator>()) + "." + SyntaxHelp;
                        return false;
                }
            }

            var strValue = nextWord();
            if (strValue == null)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "No {value} given." + SyntaxHelp;
                return false;
            }
            var strLeftover = nextWord();

            // TODO: Hack from AgoRapide 2017. Try to remove this!
            /// HACK: Ugly hack in order for <see cref="Money"/> to parse
            /// HACK: Or something like WHERE VismaOrderLineProductGeneralName EQ 'GSM Unit Bosch'
            /// TODO: Implement better parsing. Look for starting ' and ending '.
            if (strLeftover != null)
            {
                if (strValue.StartsWith("'"))
                {
                    while (strLeftover != null)
                    {
                        strValue = strValue + " " + strLeftover;
                        strLeftover = nextWord();
                    }
                }
            }
            if (strLeftover != null)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = nameof(strLeftover) + ": " + strLeftover;
                return false;
            }

            if ("'*'".Equals(value))
            { // Hack before we check for apostrophes
                value = ValueEnum.ALL.ToString();
            }

            if (strValue.StartsWith("'") && strValue.EndsWith("'"))
            {
                switch (_operator)
                {
                    case RelationalOperator.LIKE:
                    case RelationalOperator.ILIKE:
                    case RelationalOperator.NOTLIKE:
                    case RelationalOperator.NOTILIKE:
                        if (strValue.Length < 3)
                        { /// Must be like the format 'ABC*', at least something like '*', that is, at least three characters (including apostrophes)
                            retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            errorResponse =
                                "For operator " + _operator + ", search value must contain at least one character, " +
                                "with the last, the first, or both being the wildcard character '*' (asterix).\r\n" +
                                "Example: 'ABC*', '*ABC' or '*ABC*'.\r\n" +
                                "Value given was " + strValue;
                            return false;
                        }
                        if (
                            !strValue.EndsWith("*'") &&
                            !strValue.StartsWith("'*")

                        /// Support for % removed at 25 Feb 2022 (because added support for wildcard <see cref="WildcardPositionType.AtStart"/>)
                        // && !strValue.EndsWith("%'") // Note how % is also accepted (but it will probably not pass through URL filter of ISS)
                        )
                        {
                            retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            errorResponse =
                                "For operator " + _operator + ", search value must contain a wildcard character '*' (asterix), " +
                                "either at start position, at end position or in both positions.\r\n" +
                                "Example: 'ABC*', '*ABC' or '*ABC*' " +
                                "Value given was " + strValue;
                            return false;
                        }

                        break;
                }
                retval = new QueryExpressionWhere(key, _operator, strValue[1..^1]); // Since enclosed in hypens, treat like string
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }

            var toUpper = strValue.ToUpper();
            if ("*".Equals(toUpper)) toUpper = ValueEnum.ALL.ToString();

            if (
                !int.TryParse(toUpper, out _) && /// Avoid 1 being parsed as <see cref="ValueEnum.NULL"/> and so on.
                UtilCore.EnumTryParse<ValueEnum>(toUpper, out var valueEnum)
            )
            {
                if (_operator != RelationalOperator.EQ && _operator != RelationalOperator.NEQ)
                { /// TODO: Add more operators here when <see cref="QueryExpressionWhere.IsMatch"/> has been updated correspondingly
                    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse =
                        typeof(ValueEnum) + " in this case " + valueEnum.GetType().ToStringShort() + "." + valueEnum + ", " +
                        "can only be used against " + nameof(Operator) + "." + RelationalOperator.EQ + " / " + RelationalOperator.NEQ + ", " +
                        "not " + _operator;
                    return false;
                }
                retval = new QueryExpressionWhere(key, _operator, valueEnum);
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }

            if (ValueComparer.TryParse(strValue, out var valueComparer, out var debugErrorResponse))
            {
                if (!valueComparer.SupportedOperators.Contains(_operator))
                {
                    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse =
                        nameof(ValueComparer) + " " + valueComparer.GetType().ToStringShort() + " does not support operator " + _operator + ".\r\n" +
                        "Supported operators are " + string.Join(", ", valueComparer.SupportedOperators);
                    return false;
                }

                retval = new QueryExpressionWhere(key, _operator, valueComparer);
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }

            if ("TRUE".Equals(toUpper))
            { // Make for more friendly parsing of boolean
                strValue = true.ToString();
            }
            else if ("FALSE".Equals(toUpper))
            {
                strValue = false.ToString();
            }
            if (bool.TryParse(strValue, out var bln))
            {
                retval = new QueryExpressionWhere(key, _operator, bln);
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }

            // TODO: This check is not sufficient, and we should have more checks above.
            switch (_operator)
            {
                case RelationalOperator.NEQ:
                case RelationalOperator.EQ:
                case RelationalOperator.GT:
                case RelationalOperator.LT:
                case RelationalOperator.GEQ:
                case RelationalOperator.LEQ:
                case RelationalOperator.CONTAINS:
                case RelationalOperator.NOTCONTAINS:
                    // OK
                    break;
                default:
                    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse = "Operator " + _operator + " only allowed for string comparisions. Enclose value in apostrophes like '" + strValue + "' instead.";
                    return false;
            }

            retval = new QueryExpressionWhere(key, _operator, new Func<object>(() =>
            {
                if (long.TryParse(strValue, out var lng)) return lng;
                if (UtilCore.DoubleTryParse(strValue, out var dbl)) return dbl;
                if (UtilCore.DateTimeTryParse(strValue, out var dtm)) return dtm;
                if (UtilCore.TimeSpanTryParse(value, out var tsp)) return tsp;

                // TODO: We should have had a hint here about type of IP-class which we are going to Query against.
                // TODO: Then key could be PK instead of IK, giving us an actual type of value, which we could parse against now.

                // TODO: Eventually, give hint to EXECUTE about class, and transform type.

                // TODO: If Key is PK then we can check for this
                // TODO: BUT QUITE POSSIBLE NOT 'POSSIBLE' TO IMPLEMENT
                // TODO: (because we do not have context of entity type available when parsing, and therefore it is difficult to
                // TODO: find a relevant PK)
                //if (!key.Key.TryValidateAndParse(strValue, out var valueResult)) {
                //    id = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                //    errorResponse = "Invalid {value} (" + strValue + ") given for {key} " + key.Key.PToString + ".\r\nDetails: " + valueResult.ErrorResponse;
                //    return false;
                //}

                // Instead we can only do like this (assume that value is a key / compound key)
                return IKString.FromString(strValue);
            })());
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        // NOTE: Different overloads of constructor here hints about types being supported.
        // TODO: Consider hiding constructor with object as parameter.
        // TODO: (would also entail changing extension methods in QueryExpressionWhereExtension)
        public QueryExpressionWhere(IK key, RelationalOperator _operator, ValueEnum valueEnum) : this(key, _operator, (object)valueEnum) { }
        public QueryExpressionWhere(IK key, RelationalOperator _operator, string str) : this(key, _operator, (object)str) { }
        public QueryExpressionWhere(IK key, RelationalOperator _operator, bool bln) : this(key, _operator, (object)bln) { }
        public QueryExpressionWhere(IK key, RelationalOperator _operator, int _int) : this(key, _operator, (object)_int) { }
        public QueryExpressionWhere(IK key, RelationalOperator _operator, double dbl) : this(key, _operator, (object)dbl) { }
        public QueryExpressionWhere(IK key, RelationalOperator _operator, DateTime dateTime) : this(key, _operator, (object)dateTime) { }
        public QueryExpressionWhere(IK key, RelationalOperator _operator, TimeSpan timeSpan) : this(key, _operator, (object)timeSpan) { }
        public QueryExpressionWhere(IK key, RelationalOperator _operator, ValueComparer valueComparer) : this(key, _operator, (object)valueComparer) { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="_operator"></param>
        /// <param name="value">
        /// Use preferable a strong type like int, long, double, DateTime, TimeSpan
        /// Note that <see cref="ValueEnum"/> is also possible to use
        /// Use string in other cases.
        /// </param>
        public QueryExpressionWhere(IK key, RelationalOperator _operator, object value)
        {

            switch (_operator)
            {
                case RelationalOperator.LIKE:
                case RelationalOperator.ILIKE:
                case RelationalOperator.NOTLIKE:
                case RelationalOperator.NOTILIKE:
                    var s = value as string ?? throw new InvalidObjectTypeException(value, typeof(string), "Parameter " + nameof(value));
                    if (s.StartsWith("*") && s.EndsWith("*"))
                    {
                        _valueLike = _operator.ToString().Contains("ILIKE") ? s[1..^1].ToLower() : s[1..^1];
                        _wildcardPosition = WildcardPositionType.AtStartAndEnd;
                    }
                    else if (s.StartsWith("*"))
                    {
                        _valueLike = _operator.ToString().Contains("ILIKE") ? s[1..].ToLower() : s[1..];
                        _wildcardPosition = WildcardPositionType.AtStart;
                    }
                    else if (s.EndsWith("*"))
                    {
                        _valueLike = _operator.ToString().Contains("ILIKE") ? s[0..^1].ToLower() : s[0..^1]; ;
                        _wildcardPosition = WildcardPositionType.AtEnd;
                    }
                    else
                    {
                        throw new ArgumentException(
                            "For operator " + _operator + ", " +
                            "wildcard '*' must be included either at start position, at end position or both, like '*ABC', 'ABC*' or '*ABC*'.");
                    }
                    break;
                default:
                    // TODO: Add some more assertions here.
                    break;
            }

            if (value is ValueEnum v && v != ValueEnum.ALL)
            {
                /// NOTE: This check is not sufficient.
                /// NOTE: See <see cref="TryParse"/> for a more exhaustive syntax check.
                switch (_operator)
                {
                    case RelationalOperator.EQ:   // TODO: Why do we not use IS and IS NOT here?
                    case RelationalOperator.NEQ:  // TODO: Why do we not use IS and IS NOT here?
                        break;
                    default:
                        throw new ArgumentException(
                            "Use of " + v.GetType() + "." + v + " only allowed for " + nameof(_operator) + " " + RelationalOperator.EQ + " or " + RelationalOperator.NEQ + "\r\n" +
                            nameof(_operator) + " attempted set to " + _operator,
                            nameof(value));
                }
            }

            Key = key;
            Operator = _operator != RelationalOperator.__invalid ? _operator : throw new InvalidEnumException(_operator);
            Value = value;

        }

        [ClassMember(Description =
            "Returns TRUE if property matches criteria.\r\n" +
            "\r\n" +
            "May include lookup in data storage if -" + nameof(Value) + "- is -" + nameof(IK) + "- (see -" + nameof(TryGetValue) + "-).\r\n" +
            "\r\n" +
            "TODO: See Execute for how to add a PK-parameter here, instead of the IK-key that we have now"
        )]
        public bool IsMatch(IP dataStorage, IKIP ikip, ref CompoundKey? cachedCompoundKey)
        {

            // TODO: The logic for deciding HOW to match is now executed for each and every
            // TODO: ikip that we are asked to match against.
            // TODO: It would be much better to, at time of initializion, to create a Function which does the matching
            // TOOD: so here we only do "return isMatch(...) without any more logic."

            var valueEnum = Value as ValueEnum?;

            if (valueEnum != null && valueEnum == ValueEnum.ALL)
            {
                return true;
            }

            if (!ikip.TryGetP(dataStorage, Key, out var p, ref cachedCompoundKey, out _))
            {
                /// NOTE: Supporting <see cref="CompoundKey"/> here has a somewhat marginal value.
                /// NOTE: It means that we will use as sorting key a key not existing in the collection 
                /// NOTE: (because if it did exist, like in SELECT Customer.FirstName/ORDER BY Customer.FirstName, 
                /// NOTE: then <see cref="CompoundKey"/> would not be necessary anyway).

                if (valueEnum != null)
                {
                    return Operator switch
                    {
                        RelationalOperator.NEQ => valueEnum == ValueEnum.NOTNULL,
                        RelationalOperator.EQ => valueEnum == ValueEnum.NULL,
                        _ => throw new InvalidEnumException(Operator, nameof(Operator) + ": " + Operator + " for " + valueEnum.GetType() + "." + valueEnum + " (only NEQ / EQ allowed)")
                    };
                }

                // In all other cases we treat absence of a value as a FALSE match.
                return false;
            }

            if (valueEnum != null)
            {
                return Operator switch
                {
                    RelationalOperator.NEQ => valueEnum == ValueEnum.NULL,
                    RelationalOperator.EQ => valueEnum == ValueEnum.NOTNULL,
                    _ => throw new InvalidEnumException(Operator, nameof(Operator) + ": " + Operator + " for " + valueEnum.GetType() + "." + valueEnum + " (only NEQ / EQ allowed)")
                };
            }

            if (!TryGetValue(dataStorage, ikip, out var value))
            {
                /// value was a key (<see cref="IK"/>) and its value was not found.
                /// TODO: This may be dubious. It might be situations where we would actually want to match against a null value
                return false;
            }

            return IsMatch(value, p);
        }

        [ClassMember(Description =
            "Returns -" + nameof(Value) + "-, unless it is -" + nameof(IK) + "-, in which case a lookup of corresponding value will be done.\r\n"
        )]
        private bool TryGetValue(IP dataStorage, IKIP ikip, out object retval)
        {
            if (Value is IK ik)
            {
                if (!ikip.TryGetP(dataStorage, ik, out var ip, ref _valueAsCachedCompoundKey, out _))
                {
                    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return false;
                }
                return ip.TryGetV<object>(out retval);
            }
            retval = Value;
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value">
        /// Normally from <see cref="Value"/> except if it was an <see cref="IK"/>
        /// Will be ignored for LIKE, ILIKE, NOTLIKE, NOTILIKE
        /// (<see cref="TryGetValue(IP, IKIP, out object)"/>)
        /// </param>
        /// <param name="property"></param>
        /// <returns></returns>
        public bool IsMatch(object value, IP property)
        {

            // TODO: The logic for deciding HOW to match is now executed for each and every
            // TODO: ikip that we are asked to match against.
            // TODO: It would be much better to, at time of initializion, to create a Function which does the matching
            // TOOD: so here we only do "return isMatch(...) without any more logic."

            if (value is ValueComparer vcp) return vcp.IsMatch(this, property);

            // NOTE: It is quite possible that this will fail below:
            // NOTE:   property.TryGetV<{someType}>(out var pv)) {
            // NOTE: even for {someType} being {object}
            // NOTE: It could possible be caused by the property being a collection, for instance if we have a key 
            // NOTE: 'Customer' with a collection if customer-objects, and we try
            // NOTE:  WHERE Customer = 'John Smith'
            // NOTE: this would happen (because the collection of Customer objects has no intrinsic values, only sub-properties.

            /// NOTE: Code below takes into account <see cref="PValue{T}.TryGetV"/>'s ability to convert to / upgrade to requested type
            /// NOTE: It also assumes that <see cref="PValue{T}.ConvertObjectToString"/> does not cover types not explicitly mentioned here.

            return Operator switch
            {
                RelationalOperator.EQ =>
                    // Note support for List (if one of the elements matches, return TRUE)
                    // List comparision is O(n), that is, not very efficient.
                    (value) switch
                    {
                        double dbl1 => property.TryGetV<double>(out var dbl2) ? // Ensure that long and int gets upgraded to double
                            dbl1.Equals(dbl2) :
                            property.TryGetV<IEnumerable<double>>(out var lst) && lst.Contains(dbl1),
                        long lng1 => property.TryGetV<long>(out var lng2) ? // Ensure that int gets upgraded to long
                            lng1.Equals(lng2) :
                            property.TryGetV<IEnumerable<long>>(out var lst) && lst.Contains(lng1),
                        // int int1 => property.TryGetV<int>(out var int2) && int1.Equals(int2), // Unnecessary
                        DateTime dtm1 => property.TryGetV<DateTime>(out var dtm2) ?
                            dtm1.Equals(dtm2) :
                            property.TryGetV<IEnumerable<DateTime>>(out var lst) && lst.Contains(dtm1),
                        TimeSpan tsp1 => property.TryGetV<TimeSpan>(out var tsp2) ?
                            tsp1.Equals(tsp2) :
                            property.TryGetV<IEnumerable<TimeSpan>>(out var lst) && lst.Contains(tsp1),
                        Type typ1 => property.TryGetV<Type>(out var typ2) ?
                            typ1.Equals(typ2) :
                            property.TryGetV<IEnumerable<Type>>(out var lst) && lst.Contains(typ1),
                        string str1 => property.TryGetV<string>(out var str2) ?
                            str1.Equals(str2) :
                            property.TryGetV<IEnumerable<string>>(out var lst) && lst.Contains(str1),
                        _ => property.TryGetV<string>(out var str2) ?  // Assume that string comparision is sufficient
                            value.ToString().Equals(str2) :
                            property.TryGetV<IEnumerable<string>>(out var lst) && lst.Contains(value.ToString())
                    },
                RelationalOperator.NEQ =>
                    // Note support for List (if none of the elements matches, return TRUE)
                    // List comparision is O(n), that is, not very efficient.
                    (value) switch
                    {
                        double dbl1 => property.TryGetV<double>(out var dbl2) ? // Ensure that long and int gets upgraded to double
                            !dbl1.Equals(dbl2) :
                            property.TryGetV<IEnumerable<double>>(out var lst) && !lst.Contains(dbl1),
                        long lng1 => property.TryGetV<long>(out var lng2) ? // Ensure that int gets upgraded to long
                            !lng1.Equals(lng2) :
                            property.TryGetV<IEnumerable<long>>(out var lst) && !lst.Contains(lng1),
                        // int int1 => property.TryGetV<int>(out var int2) && int1.Equals(int2), // Unnecessary
                        DateTime dtm1 => property.TryGetV<DateTime>(out var dtm2) ?
                            !dtm1.Equals(dtm2) :
                            property.TryGetV<IEnumerable<DateTime>>(out var lst) && !lst.Contains(dtm1),
                        TimeSpan tsp1 => property.TryGetV<TimeSpan>(out var tsp2) ?
                            !tsp1.Equals(tsp2) :
                            property.TryGetV<IEnumerable<TimeSpan>>(out var lst) && !lst.Contains(tsp1),
                        Type typ1 => property.TryGetV<Type>(out var typ2) ?
                            !typ1.Equals(typ2) :
                            property.TryGetV<IEnumerable<Type>>(out var lst) && !lst.Contains(typ1),
                        string str1 => property.TryGetV<string>(out var str2) ?
                            !str1.Equals(str2) :
                            property.TryGetV<IEnumerable<string>>(out var lst) && !lst.Contains(str1),
                        _ => property.TryGetV<string>(out var str2) ?  // Assume that string comparision is sufficient
                            !value.ToString().Equals(str2) :
                            property.TryGetV<IEnumerable<string>>(out var lst) && !lst.Contains(value.ToString())
                    },
                RelationalOperator.CONTAINS => // We now assume that we must ask for (can only ask for) a list of properties to compare against.
                      (value) switch
                      {
                          double dbl1 => property.TryGetV<IEnumerable<double>>(out var dbl2) && dbl2.Contains(dbl1), // Ensure that long and int gets upgraded to double
                          long lng1 => property.TryGetV<IEnumerable<long>>(out var lng2) && lng2.Contains(lng1), // Ensure that int gets upgraded to long                                                                                                        
                          DateTime dtm1 => property.TryGetV<IEnumerable<DateTime>>(out var dtm2) && dtm2.Contains(dtm1),
                          TimeSpan tsp1 => property.TryGetV<IEnumerable<TimeSpan>>(out var tsp2) && tsp2.Contains(tsp1),
                          Type typ1 => property.TryGetV<IEnumerable<Type>>(out var typ2) && typ2.Contains(typ1),
                          string str1 => property.TryGetV<IEnumerable<string>>(out var str2) && str2.Contains(str1),
                          _ => property.TryGetV<IEnumerable<string>>(out var str2) && str2.Contains(value.ToString()) // Assume that string comparision is sufficient
                      },
                RelationalOperator.NOTCONTAINS => // We now assume that we must ask for (can only ask for) a list of properties to compare against.
                    (value) switch
                    {
                        double dbl1 => property.TryGetV<IEnumerable<double>>(out var dbl2) && !dbl2.Contains(dbl1), // Ensure that long and int gets upgraded to double
                        long lng1 => property.TryGetV<IEnumerable<long>>(out var lng2) && !lng2.Contains(lng1), // Ensure that int gets upgraded to long                                                                                                        
                        DateTime dtm1 => property.TryGetV<IEnumerable<DateTime>>(out var dtm2) && !dtm2.Contains(dtm1),
                        TimeSpan tsp1 => property.TryGetV<IEnumerable<TimeSpan>>(out var tsp2) && !tsp2.Contains(tsp1),
                        Type typ1 => property.TryGetV<IEnumerable<Type>>(out var typ2) && !typ2.Contains(typ1),
                        string str1 => property.TryGetV<IEnumerable<string>>(out var str2) && !str2.Contains(str1),
                        _ => property.TryGetV<IEnumerable<string>>(out var str2) && !str2.Contains(value.ToString()) // Assume that string comparision is sufficient
                    },
                RelationalOperator.GT =>
                    (value) switch
                    {
                        double dbl1 => property.TryGetV<double>(out var dbl2) && dbl1 < dbl2, // Ensure that long and int gets upgraded to double
                        long lng1 => property.TryGetV<long>(out var lng2) && lng1 < lng2, // Ensure that int gets upgraded to long
                        DateTime dtm1 => property.TryGetV<DateTime>(out var dtm2) && dtm1 < dtm2,
                        TimeSpan tsp1 => property.TryGetV<TimeSpan>(out var tsp2) && tsp1 < tsp2,
                        string str1 => property.TryGetV<string>(out var str2) && str1.CompareTo(str2) < 0,
                        _ => throw new InvalidQueryExpressionWhereException("For operator " + Operator + " value type " + value.GetType() + " should not have been allowed")
                    },
                RelationalOperator.LT =>
                    (value) switch
                    {
                        double dbl1 => property.TryGetV<double>(out var dbl2) && dbl1 > dbl2, // Ensure that long and int gets upgraded to double
                        long lng1 => property.TryGetV<long>(out var lng2) && lng1 > lng2, // Ensure that int gets upgraded to long
                        DateTime dtm1 => property.TryGetV<DateTime>(out var dtm2) && dtm1 > dtm2,
                        TimeSpan tsp1 => property.TryGetV<TimeSpan>(out var tsp2) && tsp1 > tsp2,
                        string str1 => property.TryGetV<string>(out var str2) && str1.CompareTo(str2) > 0,
                        _ => throw new InvalidQueryExpressionWhereException("For operator " + Operator + " value type " + value.GetType() + " should not have been allowed")
                    },
                RelationalOperator.GEQ =>
                    (value) switch
                    {
                        double dbl1 => property.TryGetV<double>(out var dbl2) && dbl1 <= dbl2, // Ensure that long and int gets upgraded to double
                        long lng1 => property.TryGetV<long>(out var lng2) && lng1 <= lng2, // Ensure that int gets upgraded to long
                        DateTime dtm1 => property.TryGetV<DateTime>(out var dtm2) && dtm1 <= dtm2,
                        TimeSpan tsp1 => property.TryGetV<TimeSpan>(out var tsp2) && tsp1 <= tsp2,
                        _ => throw new InvalidQueryExpressionWhereException("For operator " + Operator + " value type " + value.GetType() + " should not have been allowed")
                    },
                RelationalOperator.LEQ =>
                    (value) switch
                    {
                        double dbl1 => property.TryGetV<double>(out var dbl2) && dbl1 >= dbl2, // Ensure that long and int gets upgraded to double
                        long lng1 => property.TryGetV<long>(out var lng2) && lng1 >= lng2, // Ensure that int gets upgraded to long
                        DateTime dtm1 => property.TryGetV<DateTime>(out var dtm2) && dtm1 >= dtm2,
                        TimeSpan tsp1 => property.TryGetV<TimeSpan>(out var tsp2) && tsp1 >= tsp2,
                        _ => throw new InvalidQueryExpressionWhereException("For operator " + Operator + " value type " + value.GetType() + " should not have been allowed")
                    },

                // Before 25 Feb 2022
                //RelationalOperator.LIKE => // We assume Value to be on the form 'ABC*' where the asterix (*) is the wild-card character. The last character is ignored anyway.
                //    (value) switch
                //    { // TODO: Considering comparing against IEnumerable<string> also (as done for EQ, NEQ)
                //        string str1 => property.TryGetV<string>(out var str2) && str2.StartsWith(str1[0..^1]), // TODO: Inefficient substring extraction at each comparision. Save result of str1[0..^1] instead.
                //        _ => throw new InvalidQueryExpressionWhereException("For operator " + Operator + " value type " + value.GetType() + " should not have been allowed")
                //    },
                //RelationalOperator.ILIKE => // We assume Value to be on the form 'ABC*' where the asterix (*) is the wild-card character. The last character is ignored anyway.
                //    (value) switch
                //    { // TODO: Considering comparing against IEnumerable<string> also (as done for EQ, NEQ)
                //        string str1 => property.TryGetV<string>(out var str2) && str2.ToLower().StartsWith(str1.ToLower()[0..^1]), // TODO: Inefficient substring extraction at each comparision. Save result of str1.ToLower()[0..^1] instead.
                //        _ => throw new InvalidQueryExpressionWhereException("For operator " + Operator + " value type " + value.GetType() + " should not have been allowed"),
                //    },
                //RelationalOperator.NOTLIKE => // We assume Value to be on the form 'ABC*' where the asterix (*) is the wild-card character. The last character is ignored anyway.
                //    (value) switch
                //    { // TODO: Considering comparing against IEnumerable<string> also (as done for EQ, NEQ)
                //        string str1 => property.TryGetV<string>(out var str2) && !str2.StartsWith(str1[0..^1]), // TODO: Inefficient substring extraction at each comparision. Save result of str1[0..^1] instead.
                //        _ => throw new InvalidQueryExpressionWhereException("For operator " + Operator + " value type " + value.GetType() + " should not have been allowed")
                //    },
                //RelationalOperator.NOTILIKE => // We assume Value to be on the form 'ABC*' where the asterix (*) is the wild-card character. The last character is ignored anyway.
                //    (value) switch
                //    { // TODO: Considering comparing against IEnumerable<string> also (as done for EQ, NEQ)
                //        string str1 => property.TryGetV<string>(out var str2) && !str2.ToLower().StartsWith(str1.ToLower()[0..^1]), // TODO: Inefficient substring extraction at each comparision. Save result of str1.ToLower()[0..^1] instead.
                //        _ => throw new InvalidQueryExpressionWhereException("For operator " + Operator + " value type " + value.GetType() + " should not have been allowed"),
                //    },

                // After 25 Feb 2022
                RelationalOperator.LIKE => // Note how we ignore value-parameter now.
                    property.TryGetV<string>(out var str2) && (_wildcardPosition) switch
                    { // TODO: Considering comparing against IEnumerable<string> also (as done for EQ, NEQ)
                        WildcardPositionType.AtStartAndEnd => str2.Contains(_valueLike ?? throw new NullReferenceException(nameof(_valueLike))),
                        WildcardPositionType.AtStart => str2.EndsWith(_valueLike ?? throw new NullReferenceException(nameof(_valueLike))),
                        WildcardPositionType.AtEnd => str2.StartsWith(_valueLike ?? throw new NullReferenceException(nameof(_valueLike))),
                        _ => throw new InvalidEnumException(_wildcardPosition)
                    },
                RelationalOperator.ILIKE => // Note how we ignore value-parameter now.
                    property.TryGetV<string>(out var str2) && (_wildcardPosition) switch
                    { // TODO: Considering comparing against IEnumerable<string> also (as done for EQ, NEQ)
                        WildcardPositionType.AtStartAndEnd => str2.ToLower().Contains(_valueLike ?? throw new NullReferenceException(nameof(_valueLike))),
                        WildcardPositionType.AtStart => str2.ToLower().EndsWith(_valueLike ?? throw new NullReferenceException(nameof(_valueLike))),
                        WildcardPositionType.AtEnd => str2.ToLower().StartsWith(_valueLike ?? throw new NullReferenceException(nameof(_valueLike))),
                        _ => throw new InvalidEnumException(_wildcardPosition)
                    },
                RelationalOperator.NOTLIKE => // Note how we ignore value-parameter now.
                    property.TryGetV<string>(out var str2) && (_wildcardPosition) switch
                    { // TODO: Considering comparing against IEnumerable<string> also (as done for EQ, NEQ)
                        WildcardPositionType.AtStartAndEnd => !str2.Contains(_valueLike ?? throw new NullReferenceException(nameof(_valueLike))),
                        WildcardPositionType.AtStart => !str2.EndsWith(_valueLike ?? throw new NullReferenceException(nameof(_valueLike))),
                        WildcardPositionType.AtEnd => !str2.StartsWith(_valueLike ?? throw new NullReferenceException(nameof(_valueLike))),
                        _ => throw new InvalidEnumException(_wildcardPosition)
                    },
                RelationalOperator.NOTILIKE => // Note how we ignore value-parameter now.
                    property.TryGetV<string>(out var str2) && (_wildcardPosition) switch
                    { // TODO: Considering comparing against IEnumerable<string> also (as done for EQ, NEQ)
                        WildcardPositionType.AtStartAndEnd => !str2.ToLower().Contains(_valueLike ?? throw new NullReferenceException(nameof(_valueLike))),
                        WildcardPositionType.AtStart => !str2.ToLower().EndsWith(_valueLike ?? throw new NullReferenceException(nameof(_valueLike))),
                        WildcardPositionType.AtEnd => !str2.ToLower().StartsWith(_valueLike ?? throw new NullReferenceException(nameof(_valueLike))),
                        _ => throw new InvalidEnumException(_wildcardPosition)
                    },
                _ => throw new NotImplementedException(nameof(Operator) + ": " + Operator + " for " + value.GetType())
            };
        }


        /// <summary>
        /// Improve on use of <see cref="QueryExpression.ToString"/> (value is meant to be compatible with parser)
        /// 
        /// Note how Value as enum is given without apostrophes. 
        /// </summary>
        /// <returns></returns>
        public override string ToString() =>
            "WHERE " + Key.ToString() + " " +
            // Operator.ToMathSymbol()  // NOTE: This would be preferred method. More human readable.
            Operator +                  // NOTE: This is chosen method, makes the resulting URL IIS-safe in order to avoid System.Web.HttpException "A potentially dangerous Request.Path value was detected from the client (<)."
            " " +
            (Value) switch
            {
                ValueEnum valueEnum => valueEnum.ToString(),
                string str => "'" + str + "'",
                _ => PValue<TValue>.ConvertObjectToString(Value)
            };

        public class InvalidQueryExpressionWhereException : ApplicationException
        {
            public InvalidQueryExpressionWhereException(string message) : base(message) { }
            public InvalidQueryExpressionWhereException(string message, Exception inner) : base(message, inner) { }
        }

        [Enum(
            Description =
                "Helper enum used internally by -" + nameof(QueryExpressionWhere) + "- in order to represent " +
                "queries like WHERE Name = NULL, WHERE Name = NOTNULL and WHERE Name = * (ALL).\r\n" +
                "\r\n" +
                "TODO: Find better name for this",
            AREnumType = AREnumType.OrdinaryEnum
        )]
        public enum ValueEnum
        {
            __invalid,

            NULL,

            [EnumMember(Description =
                "Practical value enabling suggestions from " +
                "-" + nameof(QueryExpressionWhere.GetPrevious) + "- and -" + nameof(QueryExpressionWhere.GetNext) + "-."
            )]
            NOTNULL,

            [EnumMember(Description =
                "Equivalent to query like WHERE Name = *\r\n" +
                "\r\n" +
                "Practical value enabling suggestions from " +
                "-" + nameof(QueryExpressionWhere.GetPrevious) + "- and -" + nameof(QueryExpressionWhere.GetNext) + "-."
            )]
            ALL
        }
    }

    /// <summary>
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionWhereExtension
    {

        /// <summary>
        /// Note how this can be confused with LINQ overload
        /// </summary>
        /// <param name="query"></param>
        /// <param name="key"></param>
        /// <param name="_operator"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        // Do not use ClassMemberAttribute because it only clutters up the documentation 
        // [ClassMember(Description = "Note how this can be confused with LINQ overload")]
        public static Query Where(this Query query, string key, RelationalOperator _operator, object value) =>
            Where(query, IKString.FromString(key), _operator, value);
        /// <summary>
        /// Note how this can be confused with LINQ overload
        /// </summary>
        /// <param name="query"></param>
        /// <param name="key"></param>
        /// <param name="_operator"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        // Do not use ClassMemberAttribute because it only clutters up the documentation 
        // [ClassMember(Description = "Note how this can be confused with LINQ overload")]
        public static Query Where(this Query query, IK key, RelationalOperator _operator, object value) =>
            query.ExecuteOne(new QueryExpressionWhere(key, _operator, value));
    }
}