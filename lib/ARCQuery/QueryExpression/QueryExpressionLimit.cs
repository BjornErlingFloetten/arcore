﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery
{

    [Class(Description =
        "Limits the final HTML presentation of results (limits the number of elements being presented). " +
        "\r\n" +
        "Does not affect the query in itself. Used in order to avoid building huge HTML pages which will not be consumed anyway.\r\n" +
        "Specified through -" + nameof(QueryExpressionLimit) + "- and stored in -" + nameof(QueryProgressP.Limit) + "-." +
        "\r\n" +
        "See also -" + nameof(QueryExpressionTake) + "- which takes a specific number of elements from the current query result.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class QueryExpressionLimit : QueryExpression
    {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "Syntax: LIMIT {limit} like 'LIMIT 42'"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionLimit));

        public override QueryExpressionWithSuggestions Execute(Query query)
        {
            query.Progress.Limit = Limit;
            return new QueryExpressionWithSuggestions(
                this,
                previous: Limit <= 1 ? null : new QueryExpressionLimit(Limit / 2),
                next: new QueryExpressionLimit(Limit * 2)
            );
        }

        public int Limit { get; private set; }

        public QueryExpressionLimit(int limit) => Limit = limit;

        public new static QueryExpressionLimit Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryExpressionLimitException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionLimit retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionLimit retval, out string errorResponse)
        {
            value = value.Trim().Replace("  ", " ");
            var t = value.Split(" ");
            if (!"limit".Equals(t[0].ToLower()))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'LIMIT' as first word." + SyntaxHelp;
                return false;
            }
            if (t.Length != 2)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not two words like 'LIMIT {limit}' but " + t.Length + " words." + SyntaxHelp;
                return false;
            }
            if (!int.TryParse(t[1], out var limit) || limit <= 0)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Illegal {limit}, not a valid positive integer." + SyntaxHelp;
                return false;
            }
            retval = new QueryExpressionLimit(limit);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString() => "LIMIT " + Limit;

        public class InvalidQueryExpressionLimitException : ApplicationException
        {
            public InvalidQueryExpressionLimitException(string message) : base(message) { }
            public InvalidQueryExpressionLimitException(string message, Exception inner) : base(message, inner) { }
        }
    }

    /// <summary>
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionLimitExtension
    {
        public static Query Limit(this Query query, int limit) =>
            query.ExecuteOne(new QueryExpressionLimit(limit));
    }
}