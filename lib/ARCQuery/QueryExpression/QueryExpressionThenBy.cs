﻿// Copyright (c) 2016-2022 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ARCCore;

namespace ARCQuery
{

    [Class(Description =
        "Allows sorting by multiple columns.\r\n" +
        "\r\n" +
        "Only to be used after 'ORDER BY' (see -" + nameof(QueryExpressionOrderBy) + "-).\r\n" +
        "\r\n" +
        "Sorts query further that has already been ordered by -" + nameof(QueryExpressionOrderBy) + "-.\r\n" +
        "(adds secondary, tertiary ... criterias for sorting).\r\n" +
        "\r\n" +
        "Note: For improved performance, instead of using 'ORDER BY', 'THEN BY',\r\n" + 
        "you can use -" + nameof(EntityMethodKey) + "- to create a single property\r\n" +
        "that can be sorted by a single -" + nameof(QueryExpressionOrderBy) + "- clause.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-.\r\n"
    )]
    public class QueryExpressionThenBy : QueryExpression
    {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "THEN BY {key} [DESC] / [ASC]"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionThenBy));

        public override QueryExpressionWithSuggestions Execute(Query query)
        {
            var result = query.Progress.Result as IOrderedEnumerable<IKIP> ?? throw new InvalidObjectTypeException(
                query.Progress.Result, typeof(IOrderedEnumerable<IKIP>), "Resolution: Use 'ORDER BY' first, before using 'THEN BY'."
            );

            /// NOTE: Almost identical code in <see cref="QueryExpressionOrderBy"/> and <see cref="QueryExpressionThenBy"/>

            /// NOTE: See <see cref="QueryExpressionOrderBy.Execute"/> for general
            /// NOTE: comments about this code 
            /// NOTE: (code here does not contain any comments, only code in <see cref="QueryExpressionOrderBy.Execute"/>)

            var retval = new QueryExpressionWithSuggestions(
                this,
                Descending ? null : new QueryExpressionThenBy(Key, descending: true),
                !Descending ? null : new QueryExpressionThenBy(Key, descending: false)
            );

            if ((query.Progress.StrCurrentType + "Id").Equals(Key.ToString()))
            {
                if (query.Progress.Result.AsParallel().All(ikip =>
                { // Note use of AsParallell here.
                    var s = ikip.Key.ToString();
                    if ("_SUM".Equals(s) || "[NULL]".Equals(s)) return true;
                    return UtilCore.DoubleTryParse(s, out _);
                }))
                {
                    query.Progress.Result = Descending ?
                        result.ThenByDescending(ikip =>
                        {
                            var s = ikip.Key.ToString();
                            if ("_SUM".Equals(s)) return Descending ? double.MinValue : double.MaxValue;
                            if ("[NULL]".Equals(s)) return Descending ? double.MaxValue : double.MinValue;
                            return UtilCore.DoubleParse(s);
                        }) :
                        result.ThenBy(ikip =>
                        {
                            var s = ikip.Key.ToString();
                            if ("_SUM".Equals(s)) return Descending ? double.MinValue : double.MaxValue;
                            if ("[NULL]".Equals(s)) return Descending ? double.MaxValue : double.MinValue;
                            return UtilCore.DoubleParse(s);
                        });
                }
                else
                {
                    query.Progress.Result = Descending ?
                        result.ThenByDescending(ikip => ikip.Key.ToString()) :
                        result.ThenBy(ikip => ikip.Key.ToString());
                }
            }
            else
            {
                if (!TryCheckForExistenceOfKey(query, Key, out var keyAsPK))
                {
                    return retval;
                }

                var key = keyAsPK ?? Key;
                CompoundKey? cachedCompoundKey = null;

                if (query.Progress.Result.AsParallel().All(ikip => // Note use of AsParallell here.
                    !TryGetPVAsString(ikip, query.Progress.DataStorage, key, ref cachedCompoundKey, out var value) || UtilCore.DoubleTryParse(value, out _))
                )
                {
                    query.Progress.Result = Descending ?
                        result.ThenByDescending(ikip =>
                            TryGetPVAsString(ikip, query.Progress.DataStorage, key, ref cachedCompoundKey, out var value) ?
                            UtilCore.DoubleParse(GetPVAsString(ikip, query.Progress.DataStorage, key, ref cachedCompoundKey)) :
                            (Descending ? double.MaxValue : double.MinValue)
                        ) :
                        result.ThenBy(ikip =>
                            TryGetPVAsString(ikip, query.Progress.DataStorage, key, ref cachedCompoundKey, out var value) ?
                            UtilCore.DoubleParse(GetPVAsString(ikip, query.Progress.DataStorage, key, ref cachedCompoundKey)) :
                            (Descending ? double.MaxValue : double.MinValue)
                        );
                }
                else
                {
                    query.Progress.Result = Descending ?
                        result.ThenByDescending(ikip => GetPVAsString(ikip, query.Progress.DataStorage, key, ref cachedCompoundKey)) :
                        result.ThenBy(ikip => GetPVAsString(ikip, query.Progress.DataStorage, key, ref cachedCompoundKey));
                }
                // }
            }
            return retval;
        }

        public IK Key { get; private set; }

        public bool Descending { get; private set; }

        public QueryExpressionThenBy(IK key, bool descending)
        {
            Key = key;
            Descending = descending;
        }

        public new static QueryExpressionThenBy Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryThenByException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionThenBy retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionThenBy retval, out string errorResponse)
        {
            value = value.Trim().Replace("  ", " ");
            var t = UtilQuery.SplitOutsideOf(value, separator: " ", outsideOf: '\'');
            if (!"then".Equals(t[0].ToLower()))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'THEN' as first word." + SyntaxHelp;
                return false;
            }
            if (t.Count != 3 && t.Count != 4)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not three or four words but " + t.Count + " words." + SyntaxHelp;
                return false;
            }
            if (!"by".Equals(t[1].ToLower()))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'BY' as second word." + SyntaxHelp;
                return false;
            }
            var t3 = t.Count == 4 ? t[3].ToLower() : "asc";
            if (!"asc".Equals(t3) && !"desc".Equals(t3))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'ASC' or 'DESC' as third word." + SyntaxHelp;
                return false;
            }

            retval = new QueryExpressionThenBy(IKString.FromString(t[2]), "desc".Equals(t3));
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString() => "THEN BY " + Key + (!Descending ? "" : " DESC");

        public class InvalidQueryThenByException : ApplicationException
        {
            public InvalidQueryThenByException(string message) : base(message) { }
            public InvalidQueryThenByException(string message, Exception inner) : base(message, inner) { }
        }
    }

    /// <summary>
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionThenByExtension
    {
        public static Query ThenBy(this Query query, string key, bool descending = false) =>
            ThenBy(query, IKString.FromString(key), descending);

        public static Query ThenBy(this Query query, IK key, bool descending = false) =>
            query.ExecuteOne(new QueryExpressionThenBy(key, descending));
    }
}