﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ARCCore;

namespace ARCQuery {

    [Class(Description =
        "Skips the specified number of elements from the current query result.\r\n" +
        "\r\n" +
        "Useful in combination with -" + nameof(QueryExpressionTake) + "-.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class QueryExpressionSkip : QueryExpression {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "SKIP {Count} like 'SKIP 42'"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionSkip));

        public int Count { get; private set; }

        public QueryExpressionSkip(int count) => Count = count;

        public override QueryExpressionWithSuggestions Execute(Query query) {
            query.Progress.Result = query.Progress.Result.Skip(Count);
            var next = query.Progress.NextQueryToExecute() as QueryExpressionTake;
            return new QueryExpressionWithSuggestions(this,
                previous: next == null || Count <= 0 ? null : new QueryExpressionSkip(Math.Max(0, Count - next.Count)),
                next: next == null ? null : new QueryExpressionSkip(Count + next.Count)
            );
        }


        public new static QueryExpressionSkip Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryExpressionSkipException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionSkip retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionSkip retval, out string errorResponse) {
            value = value.Trim().Replace("  ", " ");
            var t = value.Split(" ");
            if (!"skip".Equals(t[0].ToLower())) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'SKIP' as first word." + SyntaxHelp;
                return false;
            }
            if (t.Length != 2) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not two words like 'SKIP {count}' but " + t.Length + " words." + SyntaxHelp;
                return false;
            }
            if (!int.TryParse(t[1], out var count) || count < 0) { // Note that 0 is acceptable (useful in auto-generated queries)
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Illegal {count}, not a valid positive integer." + SyntaxHelp;
                return false;
            }
            retval = new QueryExpressionSkip(count);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString() => "SKIP " + Count;

        public class InvalidQueryExpressionSkipException : ApplicationException {
            public InvalidQueryExpressionSkipException(string message) : base(message) { }
            public InvalidQueryExpressionSkipException(string message, Exception inner) : base(message, inner) { }
        }
    }

    /// <summary>
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionSkipExtension {
        public static Query Skip(this Query query, int count) =>
            query.ExecuteOne(new QueryExpressionSkip(count));
    }
}
