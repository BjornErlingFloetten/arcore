﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery
{
    [Class(Description =
        "Allows insertion of SQL style comments (beginning with double hyphens / minus signs, '--') into queries.\r\n" +
        "\r\n" +
        "Does not affect query result.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class QueryExpressionComment : QueryExpression
    {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "COMMENT {comment text}\r\n" +
            "-- {comment text}\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionComment));

        public QueryExpressionComment() { }

        public override QueryExpressionWithSuggestions Execute(Query query)
        {
            return new QueryExpressionWithSuggestions(this);
        }

        public new static QueryExpressionComment Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryExpressionCommentException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionComment retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionComment retval, out string errorResponse)
        {
            if (!value.StartsWith("--") && !value.ToLower().StartsWith("comment "))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Does not start with '--' or 'COMMENT '." + SyntaxHelp;
                return false;
            }
            retval = new QueryExpressionComment();
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString() => "COMMENT";

        public class InvalidQueryExpressionCommentException : ApplicationException
        {
            public InvalidQueryExpressionCommentException(string message) : base(message) { }
            public InvalidQueryExpressionCommentException(string message, Exception inner) : base(message, inner) { }
        }
    }

    /// <summary>
    /// TODO: (VERY) PROBABLY QUITE UNNECESSARY.
    /// 
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionCommentExtension
    {
        public static Query Comment(this Query query) =>
            query.ExecuteOne(new QueryExpressionComment());
    }
}