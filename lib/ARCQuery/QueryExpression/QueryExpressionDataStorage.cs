﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery
{

    [Class(Description =
        "Experimental as of Mar 2022: Resets value for -" + nameof(QueryProgress.DataStorage) + "-\r\n" +
        "\r\n" +
        "Useful when you want to link to entities placed at a different level in the hierarchical structure " +
        "than the current level.\r\n" +
        "\r\n"
    )]
    public class QueryExpressionDataStorage : QueryExpression
    {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "DATASTORAGE {new hierarchical level} [link adjustment]\r\n" +
            "Examples:\r\n" +
            "DATASTORAGE dt 2\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionDataStorage));

        public List<IKCoded> Levels { get; private set; }

        [ClassMember(Description = "See -" + nameof(QueryProgressP.LinkAdjustment) + "- for documentation.\r\n")]
        public int LinkAdjustment { get; private set; }

        public QueryExpressionDataStorage(List<IKCoded> levels, int linkAdjustment)
        {
            Levels = levels;
            LinkAdjustment = linkAdjustment;
        }

        public override QueryExpressionWithSuggestions Execute(Query query)
        {
            if (!TryNavigateToLevelInDataStorage(Levels, out var dataStorage, out var errorResponse))
            {
                query.ShouldTerminate(errorResponse); // We ignore result here
            }
            else
            {
                query.Progress.DataStorage = dataStorage;
            }
            query.Progress.IP.SetPV(QueryProgressP.LinkAdjustment, LinkAdjustment);
            return new QueryExpressionWithSuggestions(this);
        }

        public new static QueryExpressionDataStorage Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryExpressionDataStorageException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionDataStorage retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionDataStorage retval, out string errorResponse)
        {
            var t = value.Split(" ");
            if (!"datastorage".Equals(t[0].ToLower()))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'DATASTORAGE' as first word." + SyntaxHelp;
                return false;
            }

            var levels = t.Length switch
            {
                1 => new List<IKCoded>(),
                // TODO: Improve on this. "DATASTORAGE /" is now accepted, but so is also DATASTORAGE dt//Customer which is not optimal
                2 => t[1].Split("/").Where(l => !string.IsNullOrEmpty(l)).Select(l => IKCoded.FromUnencoded(IKString.FromString(l))).ToList(),
                3 => t[1].Split("/").Where(l => !string.IsNullOrEmpty(l)).Select(l => IKCoded.FromUnencoded(IKString.FromString(l))).ToList(),
                _ => null
            };

            if (levels is null)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not two words like 'DATASTORAGE {hierchical level}' but " + t.Length + " words." + SyntaxHelp;
                return false;

            }

            int linkAdjustment = 0;
            if (t.Length == 3)
            {
                if (!int.TryParse(t[2], out linkAdjustment))
                {
                    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                    errorResponse = "Invalid value for -" + nameof(QueryProgressP.LinkAdjustment) + "-. Invalid as long." + SyntaxHelp;
                    return false;
                }
            }

            // Check that given level actually exists
            if (!TryNavigateToLevelInDataStorage(levels, out _, out errorResponse))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = nameof(TryNavigateToLevelInDataStorage) + " failed. Details: " + errorResponse;
                return false;
            }

            retval = new QueryExpressionDataStorage(levels, linkAdjustment);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public static bool TryNavigateToLevelInDataStorage(List<IKCoded> levels, out IP retval, out string errorResponse)
        {
            if (GlobalDataStorage is null)
            {
                throw new InvalidQueryExpressionDataStorageException(
                    "GlobalDataStorage is null. Should have been initialized at application startup.\r\n" +
                    "For instance, for an application using -" + nameof(ARComponents.ARCAPI) + "-, " +
                    "it should be set to 'DataStorage.Storage with something like\r\n" +
                    "\"QueryExpressionDataStorage.GlobalDataStorage = DataStorage.Storage;\""
                );
            }

            var currentLevel = GlobalDataStorage;
            for (var i = 0; i < levels.Count; i++)
            {
                if (!currentLevel.TryGetP<IP>(levels[i].Unencoded, out var temp, out errorResponse))
                {
                    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                    errorResponse = "Item '" + levels[i].Unencoded + "' (" + (i + 1) + " of " + levels.Count + ") not found. Details: " + errorResponse;
                    return false;
                }
                currentLevel = temp;
            }
            retval = currentLevel;
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString() => "DATASTORAGE " + string.Join("/", Levels);

        [ClassMember(Description =
            "TODO: Find some better initialization here."
        )]
        public static IP? GlobalDataStorage = null;

        public class InvalidQueryExpressionDataStorageException : ApplicationException
        {
            public InvalidQueryExpressionDataStorageException(string message) : base(message) { }
            public InvalidQueryExpressionDataStorageException(string message, Exception inner) : base(message, inner) { }
        }
    }
}
