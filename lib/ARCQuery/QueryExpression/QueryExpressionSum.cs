﻿// Copyright (c) 2016-2022 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ARCCore;

namespace ARCQuery
{

    [Class(Description =
        "Specify for which fields to provide a sum.\r\n" +
        "\r\n" +
        "The actual summation is done by -" + nameof(QueryProgress) + "-.-" + nameof(QueryProgress.ToHTMLSimpleSingle) + "-.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class QueryExpressionSum : QueryExpression
    {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "SUM {field-1}, {field-2} ... {field-n}\r\n" +
            "Examples:\r\n" +
            "'SUM Quantity, Amount'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionSum));

        public List<Field> Fields { get; private set; }

        public QueryExpressionSum(List<Field> fields) => Fields = fields;

        public override QueryExpressionWithSuggestions Execute(Query query)
        {
            /// The actual summation is done by <see cref="QueryProgress.ToHTMLSimpleSingle"/>
            query.Progress.IP.SetPV(QueryProgressP.Sum, Fields);
            return new QueryExpressionWithSuggestions(this);
        }

        public new static QueryExpressionSum Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQuerysSumException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionSum retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionSum retval, out string errorResponse)
        {
            value = value.Trim();
            var pos = value.IndexOf(" ");
            if (pos == -1 || !"sum".Equals(value[..pos].ToLower()))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'SUM' as first word." + SyntaxHelp;
                return false;
            }
            var fields = UtilQuery.SplitOutsideOf(value[(pos + 1)..], separator: ",", outsideOf: '\'').Select(f => f.Trim()).Where(f => !"".Equals(f)).Select(f =>
                new Field(IKString.FromString(f))
            ).ToList();
            if (fields.Count == 0)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "No fields given" + SyntaxHelp;
                return false;
            }
            retval = new QueryExpressionSum(fields);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString() => "SUM " + string.Join(", ", Fields.Select(f => f.ToString()));


        [Class(Description = "TODO: Consider if this class is necessary at all.\r\n")]
        public class Field
        {
            public IK Name { get; private set; }
            public bool IsWildcardField { get; private set; }

            public Field(IK name)
            {
                Name = name;
                IsWildcardField = name.ToString().StartsWith("*");
            }

            public override string ToString() => Name.ToString();
        }

        public class InvalidQuerysSumException : ApplicationException
        {
            public InvalidQuerysSumException(string message) : base(message) { }
            public InvalidQuerysSumException(string message, Exception inner) : base(message, inner) { }
        }
    }

    /// <summary>
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionSumExtension
    {
        public static Query Sum(this Query query, params string[] fields) => Sum(query, fields.Select(s => new QueryExpressionSum.Field(IKString.FromString(s))).ToList());
        public static Query Sum(this Query query, List<QueryExpressionSum.Field> fields) => query.ExecuteOne(new QueryExpressionSum(fields));
    }
}