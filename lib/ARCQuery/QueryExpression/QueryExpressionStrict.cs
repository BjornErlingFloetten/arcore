﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery {

    [Class(Description =
        "Describes -" + nameof(ARCQuery.Strictness) + "- ON / OFF when executing query. " +
        "\r\n" +
        "Specified through -" + nameof(QueryExpressionStrict) + "- and stored in -" + nameof(QueryProgressP.CurrentStrictness) + "-." +
        "\r\n" +
        "Does not affect query result, only whether query will terminate or not due to perceived inconsistencies in the data.\r\n" +
        "\r\n" +
        "A typical example where you may want to set 'STRICT OFF' is when you have queries results where sometimes a specific field is not set, " +
        "and that field is used in a subsequent query expression, for instance 'WHERE' or 'ORDER BY'.\r\n" +
        "This is because -" + nameof(QueryExpression) + " does not have any concept of schema / 'supposed to exist' fields, it only sees the actual data.\r\n" +
        "Example: If you have this:\r\n" +
        "  Customer/42/FirstName = John\r\n" +
        "  Customer/42/LastName = Smith\r\n" +
        "  Customer/43/FirstName = Ann\r\n" +
        "Then the query " +
        "  \"Customer/WHERE FirstName = 'Ann'/ORDER BY LastName'\"\r\n" +
        "will fail because -" + nameof(QueryExpressionOrderBy) + "- will not see any field 'LastName'.\r\n" +
        "This query\r\n" +
        "  \"Customer/WHERE FirstName = 'Ann'/STRICT OFF/ORDER BY LastName'\"\r\n" +
        "on the other hand will succeed.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class QueryExpressionStrict : QueryExpression {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "STRICT {strictnessLevel}\r\n" +
            "Examples:\r\n" +
            "STRICT OFF\r\n" +
            "STRICT ON"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionStrict));

        public Strictness Strictness { get; private set; }

        public QueryExpressionStrict(Strictness strictness) => Strictness = strictness;

        public override QueryExpressionWithSuggestions Execute(Query query) {
            query.Progress.IP.SetPVM(Strictness);
            return new QueryExpressionWithSuggestions(
                this,
                previous: Strictness == Strictness.OFF ? null : new QueryExpressionStrict(Strictness.OFF),
                next: Strictness == Strictness.ON ? null : new QueryExpressionStrict(Strictness.ON)
           );
        }

        public new static QueryExpressionStrict Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryExpressionStrictException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionStrict retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionStrict retval, out string errorResponse) {
            value = value.Trim().Replace("  ", " ");
            var t = value.Split(" ");
            if (!"strict".Equals(t[0].ToLower())) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'STRICT' as first word." + SyntaxHelp;
                return false;
            }
            if (t.Length != 2) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not two words like 'STRICT {strictness}' but " + t.Length + " words." + SyntaxHelp;
                return false;
            }
            if (!UtilCore.EnumTryParse<Strictness>(t[1].ToUpper(), out var strictness)) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Illegal {strictness} (" + t[1] + "), use one of " + string.Join(", ", UtilCore.EnumGetMembers<Strictness>().Select(e => e.ToString()));
                return false;
            }
            retval = new QueryExpressionStrict(strictness);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString() => "STRICT " + Strictness;

        public class InvalidQueryExpressionStrictException : ApplicationException {
            public InvalidQueryExpressionStrictException(string message) : base(message) { }
            public InvalidQueryExpressionStrictException(string message, Exception inner) : base(message, inner) { }
        }
    }

    /// <summary>
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionStrictExtension {
        public static Query Strict(this Query query, Strictness strictness) =>
            query.ExecuteOne(new QueryExpressionStrict(strictness));
    }
}