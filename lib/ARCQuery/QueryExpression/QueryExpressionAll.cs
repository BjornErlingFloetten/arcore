﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ARCCore;

namespace ARCQuery {

    [Class(Description = 
        "Represents all entities for a given collection.\r\n" +
        "\r\n" +
        "(-" + nameof(Execute) + "- just returns the input collection, without making any changes).\r\n" +
        "\r\n" +
        "Useful in order to get result set presented by -" + nameof(QueryProgress.ToHTMLSimpleSingle) + "- (from -" + nameof(QueryProgress) + "-) " +
        "instead of -" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- (from -" + nameof(ARComponents.ARCDoc) + "-.-" + nameof(ARCDoc.Extensions) + "-).\r\n" +
        "\r\n" +
        "Example:\r\n" +
        "For a query like 'Customer' presentation will be done by -" + nameof(ARComponents.ARCDoc) + "-.-" + nameof(ARCDoc.Extensions) + "-.-" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- as default.\r\n" +
        "For a query like 'Customer/All' presentation will be done by -" + nameof(QueryProgress) + "-.-" + nameof(QueryProgress.ToHTMLSimpleSingle) + "- as default.\r\n" +
        "\r\n" +
        "Note: -" + nameof(QueryExpressionAll) + "- differs from -" + nameof(QueryExpressionSelect) + "- 'SELECT *' regarding use of -" + nameof(EntityMethodKey) +"-s.\r\n" +
        "The query 'Customer/All' will not include -" + nameof(EntityMethodKey) + "-s, while 'Customer/SELECT *' would.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class QueryExpressionAll : QueryExpression {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'ALL' (literally)."        
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionAll));

        public override QueryExpressionWithSuggestions Execute(Query query) => new QueryExpressionWithSuggestions(this); // No change

        public QueryExpressionAll() { }

        public new static QueryExpressionAll Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryExpressionAllException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionAll retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionAll retval, out string errorResponse) {
            value = value.Trim();
            if (!"all".Equals(value.ToLower())) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not 'ALL'." + SyntaxHelp;
                return false;
            }
            retval = new QueryExpressionAll();
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override string ToString() => "ALL";

        public class InvalidQueryExpressionAllException : ApplicationException {
            public InvalidQueryExpressionAllException(string message) : base(message) { }
            public InvalidQueryExpressionAllException(string message, Exception inner) : base(message, inner) { }
        }
    }

    /// <summary>
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionAllExtension {
        public static Query All(this Query query) => query.ExecuteOne(new QueryExpressionAll());
    }
}