﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ARCCore;

namespace ARCQuery {

    [Class(Description =
        "Aggregates over the given key.\r\n" +
        "\r\n" +
        "Examples:\r\n" +
        "  'AGGREGATE Product'.\r\n" +
        "or\r\n" +
        "  'AGGREGATE Product BY Year SUM Amount'.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-.\r\n" +
        "\r\n" +
        "See also:\r\n" +
        "-" + nameof(QueryExpressionPivot) + "- which aggregates over two keys (row key and column key).\r\n" +
        "and\r\n" +
        "-" + nameof(FunctionKeyAggregate) + "- which operates over a collection of properties.\r\n" +
        "\r\n" +
        "TODO: As of Nov 2020 there is lot of similar code in -" + nameof(QueryExpressionPivot) + "- and -" + nameof(QueryExpressionAggregate) + ".\r\n"
    )]
    public class QueryExpressionAggregate : QueryExpression {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "1) 'AGGREGATE {rowKey}' like \"AGGREGATE Product\"\r\n" +
            "or\r\n" +
            "2) 'AGGREGATE {rowKey} {aggregationType} {aggregationKey}' like \"AGGREGATE Colour SUM Price\""
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(QueryExpressionAggregate));

        public IK RowKey { get; private set; }

        [ClassMember(Description =
            "Most common value is -" + nameof(AggregationType.Count) + "-, " +
            "in which case -" + nameof(AggregationKey) + "- may be correspondingly set to null."
        )]
        public AggregationType AggregationType { get; private set; }

        [ClassMember(Description =
            "This is often null (with -" + nameof(AggregationType) + "- correspondingly set to " + nameof(AggregationType.Count) + "-).\r\n" +
            "Iteration then looks like\r\n" +
            "  'AGGREGATE Product'.\r\n" +
            "If not null then iteration looks like:\r\n" +
            "  'AGGREGATE Product SUM Amount'"
        )]
        public IK? AggregationKey { get; private set; }

        public override QueryExpressionWithSuggestions Execute(Query query) {
            /// NOTE: Code here taken from <see cref="QueryExpressionPivot"/> and modified accordingly
            /// 
            // TODO: Note rooms for big improvements in processing speeds 
            // TODO: This is only a naïve first implementation of Aggregateing

            // TODO: Add support for aggregation type? Or row? Or column? Many possibilities here.
            var retval = new QueryExpressionWithSuggestions(this);

            if (!TryCheckForExistenceOfKey(query, RowKey, out _)) {
                return retval;
            }

            if (AggregationKey != null && !TryCheckForExistenceOfKey(query, AggregationKey, out _)) {
                return retval;
            }

            // 1) Find all possible row values, including NULL
            var cachedCompoundKeyRow = (CompoundKey?)null;
            var rowValues = query.Progress.Result.AsParallel().Select(ikip => { // Note use of AsParallell here.
                return GetPVAsStringOrNull(ikip, query.Progress.DataStorage, RowKey, ref cachedCompoundKeyRow);
            }).Where(s => s != null).Distinct().Select(s => s!).Append("[NULL]").ToList();

            switch (AggregationType) {
                case AggregationType.Count: {
                        Aggregate(query, rowValues, aggregator: (l1, l2) => l1 + l2, zeroValue: 0L, countOnly: true);
                        break;
                    }
                case AggregationType.Sum:
                    if (AggregationKey == null) throw new NullReferenceException(nameof(AggregationKey) + ": Supposed set for " + nameof(AggregationType) + " = " + AggregationType);

                    bool Try<T>(Func<IP, bool>? valueChecker, Func<T, T, T> aggregator, T zeroValue) where T : notnull {
                        if (!IsSuitableAggregator<T>(query.Progress, AggregationKey ?? throw new NullReferenceException(nameof(AggregationKey)), valueChecker)) {
                            return false;
                        } else {
                            Aggregate<T>(query, rowValues!, aggregator, zeroValue);
                            return true;
                        }
                    }

                    /// There are three possible alternatives for deciding which type T to aggregate:
                    /// 1) <see cref="AggregationKey"/> is PK with the desired type T. This is the preferred check.
                    /// 2) All values are of type PValue{T}
                    /// 3) All values can be converted to type T (through <see cref="PValue{T}.TryGetV"/>). This is time consuming as potential parsing has to be done for each element in order to decide for a given type T (unfortunately this parsed result is discarded again immediately, instead of being kept for later aggregation)
                    /// Alternative 1) is inbuilt in <see cref="Aggregator{T}.TryCreate"/>,
                    /// alternative 2) and 3) is supplied as parameter 'valueChecker', but we must be careful with the order 
                    /// because <see cref="PValue{T}.TryGetV"/> will happily return long even if it is PValue{double}
                    /// Therefore we wait with checking with "valueChecker: p => p.TryGetV{long}(out _)" until we have checked for double with "valueChecker: p => p is PValue{double}"
                    if (Try(valueChecker: null, aggregator: (t1, t2) => t1 + t2, zeroValue: TimeSpan.Zero)) {
                        // Finished
                    } else {
                        /// Decide between double, long and int.
                        if (Try(valueChecker: p => p is PValue<double>, aggregator: (i1, i2) => i1 + i2, zeroValue: 0D)) {
                            // Finished
                        } else if (Try(valueChecker: p => p is PValue<long>, aggregator: (i1, i2) => i1 + i2, zeroValue: 0L)) {
                            // Finished
                        } else if (Try(valueChecker: p => p is PValue<int>, aggregator: (i1, i2) => i1 + i2, zeroValue: 0)) {
                            // Finished
                        } else if (Try(valueChecker: p => p.TryGetV<long>(out _), aggregator: (l1, l2) => l1 + l2, zeroValue: 0L)) {
                            // Finished
                        } else {
                            // Aggregate doubles
                            Aggregate<double>(query, rowValues, aggregator: (d1, d2) => d1 + d2, zeroValue: 0D);
                            // NOTE: double may now be chosen only because there are no values, in which case it might have been better to not aggregate at all
                            // NOTE: This is only a problem with weakly typed schemas. If the type was chosen based on PK.Type, this problem does not manifest itself.

                            // TODO: Add support for any class that supports an aggregate function
                            // TODO: A typically example could be Money for instance.
                            // TODO: It is easy to define an interface with an aggregator function, but 
                            // TODO: specifying the zero / initial value is more complicated. 
                            // TODO: 
                            // TODO: This would be easy in C# "ten": 
                            // TODO: See article here:
                            // TODO: https://github.com/dotnet/csharplang/issues/1711
                            // TODO: (scroll down to "An example: Numeric abstraction")
                            // TODO: TLDR; version is here:
                            // TODO: https://stackoverflow.com/questions/9415257/how-can-i-implement-static-methods-on-an-interface
                            // TODO:
                            // TODO: Alternative: Let the property key itself contain the zero / initial value.
                        }
                    }
                    break;
                default:
                    throw new InvalidEnumException(AggregationType, "Not implemented");
            }
            // This is somewhat dubious, but is enables further processing (sorting by row value for instance)
            query.Progress.StrCurrentType = RowKey.ToString();
            return retval;
        }

        /// <summary>
        /// Made static in order to be reused from <see cref="QueryExpressionPivot"/>
        /// </summary>
        /// <param name="queryProgress"></param>
        /// <param name="AggregationKey"></param>
        /// <param name="retval"></param>
        /// <param name="valueChecker">
        /// If null then "p => p is PValue<T> || p.TryGetV<T>(out _)" will be used.
        /// </param>
        /// <param name="aggregator"></param>
        /// <returns></returns>
        [ClassMember(Description =
            "Returns true if\r\n" +
            "1) the aggregation key matches the given generic type OR (\r\n" +
            "2) At least one value is found AND\r\n" +
            "3) All values found matches generic type.)\r\n" +
            "\r\n" +
            "NOTE: In case 3), a potentially costly parsing may be performed by -" + nameof(PValue<TValue>.TryGetV) + "-.\r\n" +
            "NOTE: This result is then discarded here, before being needed again by -" + nameof(Aggregate) + "-.\r\n" +
            "NOTE: This is only a problem with weakly typed schemas though, and therefore not considered critical."
        )]
        public static bool IsSuitableAggregator<T>(QueryProgress queryProgress, IK aggregationKey, Func<IP, bool>? valueChecker) where T : notnull {
            var cachedCompoundKeyAggregation = (CompoundKey?)null;
            if (valueChecker == null) valueChecker = p => p is PValue<T> || p.TryGetV<T>(out _);

            // TODO: The point with ensuring at least one value is found is to avoid 
            // TODO: a non-suitable aggregator being chosen...
            // TODO: (See how calls to TryCreate are made in an order of priority)
            // TODO:
            // TODO: All this needs better explanation (maybe code below can be dramatically shorted, only comparing against PK.Type
            var oneValueFound = false;

            return
                (aggregationKey is PK pk && pk.Type.Equals(typeof(T))) ||
                (
                    queryProgress.Result.AsParallel().All(ikip => { // Note use of AsParallell here.
                        if (!ikip.TryGetP(queryProgress.DataStorage, aggregationKey, out var d, ref cachedCompoundKeyAggregation, out _)) {
                            return true;
                        }
                        oneValueFound = true; // Note use of dubious side effect below.
                        return valueChecker(d);
                    }) &&
                    oneValueFound // Note use of dubious side effect (avoid summing as timespan just because we have no values)
                );
        }

        public void Aggregate<T>(Query query, List<string> rowValues, Func<T, T, T> aggregator, T zeroValue, bool countOnly = false) where T : notnull {
            if (countOnly) InvalidTypeException.AssertEquals(typeof(T), typeof(long), () => nameof(countOnly));
            if (!countOnly && AggregationKey == null) throw new ArgumentNullException(nameof(AggregationKey) + ", can only be null when " + nameof(countOnly) + " = TRUE");
            var one = countOnly ? (T)(object)1L : zeroValue; /// Note type assertion in constructor

            // TODO: Note rooms for big improvements in processing speeds 
            // TODO: This is only a naïve first implementation of iterating

            var cachedCompoundKeyAggregation = (CompoundKey?)null;
            var cachedCompoundKeyRow = (CompoundKey?)null;

            var result = new Dictionary<string, T>();
            rowValues.ForEach(rowValue => {
                result[rowValue] = zeroValue;
            });

            var rowNullFound = false; // Decides if corresponding NULL index is needed 
            var emptyResult = true;
            query.Progress.Result.ForEach(ikip => { // Do the actual calculation NOTE: DO NOT USE AsParallell here! Not thread-safe code.
                emptyResult = false;
                var rowValue = GetPVAsString(ikip, query.Progress.DataStorage, RowKey, ref cachedCompoundKeyRow, "[NULL]");
                if ("[NULL]".Equals(rowValue)) rowNullFound = true;

                result[rowValue] = aggregator(result.GetValue(rowValue),
                    countOnly ? one :
                   (
                        ikip.TryGetP(
                            query.Progress.DataStorage,
                            AggregationKey!, // ! is necessary because as of Mar 2021 compiler is not smart enough to catch null check above
                            out var p,
                            ref cachedCompoundKeyAggregation,
                            out _) ?
                        p.GetV(zeroValue) : zeroValue
                    )
                );
                // TODO: Final HTML result should contain links for showing
                // TODO: objects actually 'behind' the calculated value
            });
            var totalSum1 = zeroValue;
            if (!rowNullFound) rowValues.RemoveAt(rowValues.Count - 1);           // Remove [NULL], not needed. TODO: Ensure that did not confuse row with column here!
            query.Progress.Result = rowValues.Select(rowValue => { // NOTE: DO NOT USE AsParallell here! Not thread-safe code.
                var row = new PRich();
                var sumRow = result.GetValue(rowValue);
                row.IP.AddPV("_SUM", sumRow); // NO: THIS DID NOT WORK: Tilde ¨ has ASCII value 126, increases chance of SUM being rightmost / downmost
                totalSum1 = aggregator(totalSum1, sumRow);
                return new IKIP(IKString.FromString(rowValue), row);
            }).ToList(); // ToList is necessary because of 'side-effects' in Select (force evaluation now)
            if (emptyResult) {
                // Do not add _SUM row. Especially because "wrong" type of aggregation may have been chosen, 
                // for instance, user expects Timespan but aggregation shows _SUM 0.00 (as double)
            } else {
                var sumColumnsRow = new PRich();
                sumColumnsRow.IP.AddPV("_SUM", totalSum1);
                query.Progress.Result = query.Progress.Result.Append(
                    new IKIP(IKString.FromString("_SUM"), sumColumnsRow)
                );
            }
        }

        public new static QueryExpressionAggregate Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQueryExpressionAggregateException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QueryExpressionAggregate retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out QueryExpressionAggregate retval, out string errorResponse) {
            value = value.Trim();
            if (!value.ToLower().StartsWith("aggregate ")) { /// TODO: Move code here into <see cref="QueryIdFieldIterator"/>
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Does not start with 'AGGREGATE '";
                return false;
            }

            var t = UtilQuery.SplitOutsideOf(value, separator: " ", outsideOf: '\'').Select(s => s.Trim()).Where(s => !string.IsNullOrEmpty(s)).ToList();
            if (t.Count != 2 && t.Count != 4) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Not two items or four items but " + t.Count + " items." + SyntaxHelp;
                return false;
            }

            var rowKey = IKString.FromString(t[1]); // NOTE: We accept quite weakly typing here, we do not attempt to find PK for instance

            if (t.Count == 2) { // We are finished
                retval = new QueryExpressionAggregate(rowKey, AggregationType.Count, aggregationKey: null);
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            } else if (t.Count == 4) { // Continue with parsing last two parameters. 
                                       // Continue parsing
                if (!UtilCore.EnumTryParse<AggregationType>(t[2], out var aggregationType)) {
                    /// Note second attempt, which will accept all-upper case like SUM, MEDIAN (assuming only the first letter is upper case in the actual enum-string). 
                    /// (this gives a more natural syntax)
                    if (!UtilCore.EnumTryParse(t[2][..1] + t[2][1..].ToLower(), out aggregationType)) {
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        errorResponse = "Invalid {aggregationType} (" + t[2] + "). Use one of " + string.Join(", ", UtilCore.EnumGetMembers<AggregationType>()) + "." + SyntaxHelp;
                        return false;
                    }
                }
                var aggregationKey = IKString.FromString(t[3]);

                retval = new QueryExpressionAggregate(rowKey, aggregationType, aggregationKey);
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            } else {
                throw new InvalidCountException("Found  " + t.Count + ", expected 2 or 4");
            }
        }

        public QueryExpressionAggregate(IK rowKey, AggregationType aggregationType = AggregationType.Count, IK? aggregationKey = null) {
            RowKey = rowKey ?? throw new NullReferenceException(nameof(rowKey));
            AggregationType = aggregationType;
            if (aggregationKey == null && aggregationType != AggregationType.Count) throw new ArgumentNullException(nameof(aggregationKey) + ". (null is only allowed for -" + nameof(aggregationType) + "- " + nameof(AggregationType.Count) + ", not " + aggregationType);
            AggregationKey = aggregationKey;
        }

        public override string ToString() =>
            "AGGREGATE " + RowKey.ToString() +
            (AggregationKey == null ?
                "" :
                (" " + AggregationType.ToString().ToUpper() + " " + AggregationKey.ToString()) /// Note upper-case for field <see cref="AggregationType"/>
            );

        public static new void EnrichKey(PK key) => QueryExpression.EnrichKey(key);

        public class InvalidQueryExpressionAggregateException : ApplicationException {
            public InvalidQueryExpressionAggregateException(string message) : base(message) { }
            public InvalidQueryExpressionAggregateException(string message, Exception inner) : base(message, inner) { }
        }
    }

    /// <summary>
    /// Enables fluent interface through method chaining
    /// </summary>
    // Do not use ClassAttribute because it only clutters up the documentation 
    // [Class(Description = "Enables fluent interface through method chaining")]
    public static class QueryExpressionAggregateExtension {
        public static Query Aggregate(this Query query, IK rowKey, AggregationType aggregationType = AggregationType.Count, IK? aggregationKey = null) =>
            query.ExecuteOne(new QueryExpressionAggregate(rowKey, aggregationType, aggregationKey));
    }
}