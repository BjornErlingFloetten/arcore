﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using ARCDoc;
using System.Linq;

namespace ARCQuery
{
    [Class(Description =
        "Keeps track of progress of query, and the interim result.\r\n" +
        "\r\n" +
        "Should be considered a one-time-use only object.\r\n" +
        "This is because methods like -" + nameof(Query.ExecuteOne) + "- and -" + nameof(Query.ExecuteAll) + "- " +
        "change internal state. In other words, if you want to execute the same query twice, create a new -" + nameof(Query) + "- instance the second time.\r\n"
    )]
    /// NOTE: There is a reason why this class does not inherit PConcurrent
    /// NOTE: Inheriting would mess up use of extension methods like <see cref="QueryExpressionWhereExtension.Where"/>
    /// NOTE: because <see cref="IP"/> supports <see cref="IEnumerable{T}"/>
    /// NOTE: (it would look confusing in Intellisense for instance).
    /// NOTE: Therefore we use a member class <see cref="QueryProgress"/> instead. 
    public class Query
    {

        private readonly List<string> _allLogEntriesForThisQuery = new List<string>();
        [ClassMember(Description =
            "Temporary storage of log information.\r\n" +
            "\r\n" +
            "Will be copied to -" + nameof(QueryProgressP.AllLogEntriesForThisQuery) + "- by -" + nameof(ExecuteAll) + "-."
        )]
        public IEnumerable<string> AllLogEntriesForThisQuery => _allLogEntriesForThisQuery;

        [ClassMember(Description =
            "Use of member class in order for -" + nameof(Query) + "- not to inherit -" + nameof(IP) + "-.\r\n"
        )]
        public QueryProgress Progress { get; private set; }

        [ClassMember(Description =
            "Attempts to explain problem, in -" + nameof(QueryProgressP.TerminateReason) + "- and by logging.\r\n" +
            "Returns TRUE if -" + nameof(QueryProgressP.CurrentStrictness) + " = " + nameof(Strictness.ON)
        )]
        public bool ShouldTerminate(string problem)
        {
            Progress.IP.LogText(problem);
            if (Progress.IP.GetPVM<Strictness>() == Strictness.OFF) return false;
            Progress.IP.SetPV(QueryProgressP.TerminateReason,
                problem + "\r\n" +
                "HINT: Use 'STRICT OFF' in order to force execution of query (ignoring this problem)."
            );
            return true;
        }

        [ClassMember(Description =
            "NOTE: Note how avoids deferred execution in order to always correctly time execution of each query element separately.\r\n" +
            "NOTE: This unfortunately kind of defeats the purpose of -" + nameof(QueryExpression) + "- implementations\r\n" +
            "NOTE: returning IEnumerable instead of List.\r\n" +
            "\r\n" +
            "TODO: Implement setting that decides whether timing is desired or not, and if not, use deferred execution instead."
        )]
        public Query ExecuteOne(QueryExpression query)
        {
            // NOTE: Logging 'CountBefore' looks superfluous because it should be equal to 'CountAfter' for previous query,
            // NOTE: but not if this is the first query in a sequence of queries.

            Progress.IP.LogExecuteTime(
                action: () =>
                {
                    {
                        switch (Progress.Result)
                        {
                            case IOrderedEnumerable<IKIP> o:
                                /// Hack for IOrderedEnumerable, in order for
                                /// <see cref="QueryExpressionThenBy"/> to function after <see cref="QueryExpressionOrderBy"/>
                                /// Do not convert to list
                                Progress.IP.LogKeyValue("CountBefore", Progress.Result.Count().ToString());
                                break;
                            default:
                                // Avoid deferred execution by converting to List
                                // This is assumed to remove some complexities but not absolutely necessary (and note hack above anyway)
                                if (!(Progress.Result is List<IKIP> list))
                                {
                                    list = (List<IKIP>)(Progress.Result = Progress.Result.ToList());
                                }
                                Progress.IP.LogKeyValue("CountBefore", list.Count.ToString());
                                break;
                        }
                    }
                    Progress.ExecutedQueries.Add(query.Execute(this));
                    {
                        switch (Progress.Result)
                        {
                            case IOrderedEnumerable<IKIP> o:
                                /// Hack for IOrderedEnumerable, in order for
                                /// <see cref="QueryExpressionThenBy"/> to function after <see cref="QueryExpressionOrderBy"/>
                                /// Do not convert to list
                                var count = Progress.Result.Count(); ;
                                Progress.IP.LogKeyValue("CountAfter", count.ToString());
                                Progress.ExecutedQueries[^1].ResultCount = count;
                                break;
                            default:
                                // Avoid deferred execution by converting to List
                                // This is assumed to remove some complexities but not absolutely necessary (and note hack above anyway)
                                if (!(Progress.Result is List<IKIP> list))
                                {
                                    // Is probably not a List as Query-implementations should use deferred execution
                                    list = (List<IKIP>)(Progress.Result = Progress.Result.ToList());
                                }
                                Progress.IP.LogKeyValue("CountAfter", list.Count.ToString());
                                Progress.ExecutedQueries[^1].ResultCount = list.Count;
                                break;
                        }
                    }
                },
                properties: new List<(string, string)> {
                    ("Query", query.ToString())
                }
            );
            Progress.IP.LogText("------------------------------------------------------");
            return this;
        }

        [ClassMember(Description =
            "Executes all queries.\r\n" +
            "\r\n" +
            "Result is found in " + nameof(Query.Progress.Result) + ".\r\n" +
            "\r\n" +
            "Any errors (including Exceptions that occcurred) are explained in -" + nameof(QueryProgressP.TerminateReason) + "-.\r\n" +
            "\r\n" +
            "See also static method -" + nameof(TryExecute) + "- which is much more simple / easy to use.\r\n"
        )]
        public void ExecuteAll()
        {
            try
            {
                Progress.ExecutedQueries = new List<QueryExpressionWithSuggestions>();
                var queries = Progress.Queries;
                for (var i = 0; i < queries.Count; i++)
                {
                    var q = queries[i];
                    ExecuteOne(q);
                    if (Progress.IP.TryGetPV<string>(QueryProgressP.TerminateReason, out _))
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Progress.IP.SetPV(QueryProgressP.TerminateReason, "Exception occured (" + ex.GetType().ToString() + ")");
                Progress.IP.HandleException(ex);
            }

            if (_allLogEntriesForThisQuery.Count > 0)
            {
                // TODO: Store as List<string> instead?
                Progress.IP.SetPV(QueryProgressP.AllLogEntriesForThisQuery, string.Join("\r\n", _allLogEntriesForThisQuery));
            }
        }

        /// <param name="currentType">Typical type of first element in initial collection</param>
        [ClassMember(Description =
            "Simple (easy to use) alternative to -" + nameof(ExecuteAll) + "- operating in a more 'standardized' AgoRapide way.\r\n" +
            "\r\n"
        )]
        public static bool TryExecute(
            IP dataStorage, List<QueryExpression> queryExpressions, IEnumerable<IKIP> initialCollection, Type currentType,
            out IEnumerable<IKIP> retval, out string errorResponse
        )
        {
            if (queryExpressions.Count == 0)
            {
                // NOTE: One could of course consider other actions here,
                // NOTE: like ignoring the fact that no query expressions are given,
                // NOTE. or returning FALSE now.
                throw new ArgumentException("queryExpressions.Count == 0");
            }

            var log = new System.Text.StringBuilder();
            var query = new Query(dataStorage, queryExpressions, initialCollection, currentType, s => log.AppendLine(s));
            query.ExecuteAll();
            if (query.Progress.IP.TryGetPV<string>(QueryProgressP.TerminateReason, out var terminateReason))
            {
                errorResponse =
                    nameof(terminateReason) + ": " + terminateReason + "\r\n" +
                    "Log: " + log.ToString() + "\r\n" + // This is probably a duplicate of information below?
                    (
                        !query.Progress.IP.TryGetPV<string>(QueryProgressP.AllLogEntriesForThisQuery, out var logEntries) ? "" :
                        (nameof(QueryProgressP.AllLogEntriesForThisQuery) + ": " + logEntries)
                    );
            }
            retval = query.Progress.Result;
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        /// <param name="dataStorage">
        /// Necessary in order for functionality like <see cref="ForeignKey"/> and <see cref="QueryExpressionRel"/> to work.
        /// 
        /// For queries beginning with 'dt' like 'dt/Customer/WHERE CustomerId = 42' this parameter should
        /// point to the 'dt' node in the hierarchical structure.
        /// </param>
        /// <param name="initialCollection">
        /// For queries like 'dt/Customer/WHERE CustomerId = 42' this parameter should point to the 'Customer' node 
        /// in the hierarchical structure, that is a Collection of Customer objedcts.
        /// </param>
        public Query(IP dataStorage, List<QueryExpression> queries, IEnumerable<IKIP> initialCollection, Type currentType, Action<string>? logger)
        {
            Progress = new QueryProgress
            {
                DataStorage = dataStorage,
                Result = initialCollection,
                Queries = queries
            };
            Progress.StrCurrentType = currentType.ToStringVeryShort();
            Progress.IP.AssertIntegrity();

            // Unnecessary, these have default values set
            //Details.IP.AddPVM(Strictness.ON);
            //Details.IP.AddPV(QueryProgressDetailsP.Limit, 1000);

            // Note: Setting property before setting log context means that this text does not get logged (something which is desired)
            Progress.IP.AddPV(DocumentatorP._Description,
                "Contains, in addition to result, also log information about execution of API query\r\n" +
                string.Join("/", queries.Select(q => q.ToString())) + "\r\n" +
                "\r\n" +
                "Individual query expressions are:\r\n" +
                string.Join("\r\n", queries.Select(q => q.ToString())) + "\r\n" +
                "\r\n" +
                "Note that the ordinary result (from an ordinary exceution of query) is stored in -" + nameof(QueryProgress.Result) + "- " +
                "which is 'invisible' from ordinary -" + nameof(ARConcepts.PropertyAccess) + "-.\r\n"
            );

            if (logger == null)
            { // TODO: Improve in this hastily put together code.
                Progress.IP.Logger = s =>
                {
                    // Log internally only
                    _allLogEntriesForThisQuery.Add(s);
                };
            }
            else
            {
                Progress.IP.Logger = s =>
                {
                    // Log internally plus to requested logger
                    _allLogEntriesForThisQuery.Add(s);
                    logger(s);
                };
            }
        }
    }
}
