﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using ARCCore;

/// <summary>
/// Contains the following:
/// <see cref="ARCQuery.Translations"/>
/// <see cref="ARCQuery.TranslationsCollection"/>
/// <see cref="ARCQuery.TranslationsP"/>
/// <see cref="ARCQuery.TranslationSingle"/>
/// <see cref="ARCQuery.TranslationSingleP"/>
/// <see cref="ARCQuery.TranslationException"/>
/// </summary>
namespace ARCQuery
{
    [Class(Description =
        "Offers a very simple system for keeping track of translation between different (\"western\") languages.\r\n" +
        "\r\n" +
        "(this class has, as of Jan 2022, not been tested for other languages than those using an alphabet similar to the Latin alphabet " +
        "with left to right direction.)\r\n" +
        "\r\n" +
        "Various approaches to storing translations are:\r\n" +
        "\r\n" +
        "Approach 1: Storing translations in the classic -" + nameof(ARConcepts.PropertyStream) + "-:\r\n" +
        "Recommended prefix is -" + nameof(PSPrefix.t) + "-, translations can then be added like this:\r\n" +
        "  t/de_DE/_Car = Wagen\r\n" +
        "  t/fr_FR/_Car = Auto\r\n" +
        "  t/nb_NO/_Car = Bil\r\n" +
        "See -" + nameof(TranslationSingle.TryGetPS) + "- (but note that it offers a slightly different format).\r\n" +
        "\r\n" +
        "Approach 2: Storing translation strings directly within C# source code.\r\n" +
        "For instance a class called \"Translations_de_DE.cs\" could have an adder method \"a\" working like this:\r\n" +
        "  a(\"Car\",\"Wagen\");\r\n" +
        "  a(\"Bicycle\",\"Fahrrad\");\r\n" +
        "(the adder method is then supposed to populate this class).\r\n" +
        "See -" + nameof(TranslationSingle.TryGetA) + "-\r\n" +
        "\r\n" +
        "Approach 3: Storing translations as -MacroType-.-Translation-.\r\n" +
        "(requires use of -" + nameof(ARComponents.ARCEvent) + "-)\r\n" +
        "This approach makes it possible to choose translation on-the-fly at time of development.\r\n" +
        "See -" + nameof(TranslationSingle.TryGetMacro) + "-\r\n" +
        "\r\n" +
        "For all three approaches, this class can be put into the global data storage," +
        "with prefix -" + nameof(PSPrefix.t) + "-, making translations available to API clients through ordinary queries.\r\n" +
        "\r\n" +
        "In all cases, this class will add untranslated keys (assumed that they start with an underscore ('_'), " +
        "with -" + nameof(TranslationSingleP) + "-.-" + nameof(TranslationSingleP.Text) + "- " +
        "set to key (minus the underscore) and -" + nameof(TranslationSingleP.IsMissing) + "- set to TRUE.\r\n" +
        "\r\n" +
        "This is accomplished through override of method -" + nameof(TryGetP) + "-.\r\n" +
        "\r\n" +
        "These \"IsMissing\" translations can then be queried, and added either to the property stream or C# source code as suggested above.\r\n" +
        "\r\n" +
        "That is, translation keys should always start with an underscore ('_').\r\n" +
        "This makes it possible to use -" + nameof(ARComponents.ARCQuery) + "- against this class, because keys not starting with " +
        "an underscore can then be attempted parsed as a query like 'All', 'SELECT *' or similar.\r\n" +
        "\r\n" +
        "Note that if you are using -" + nameof(ARComponents.ARCEvent) + "- with exhaustive unit testing, that is, with all " +
        "possible translation keys being excercised, then this class can be queried afterwards for any missing translations.\r\n" +
        "\r\n" +
        "Keeping everything in-memory / in C# code has the advantage of not polluting any database with random translation keys that " +
        "come and go if code is frequently changed. This is because new keys are ephemeral (the exist only within application lifetime).\r\n" +
        "You then implementing new translation keys only when you know that you have a stabile software configuration, " +
        "that is, only when you know that the keys that you have are actually permanent.\r\n" +
        "\r\n" +
        "Also, you can at any time (assumed exhaustive unit testing) query this class for currently " +
        "in use translation keys, with their translation, and paste that direct into the C# code (replacing the existing code), " +
        "thereby removing unused keys from the C# code.\r\n" +
        "\r\n" +
        "Note how inherits -" + nameof(PConcurrent) + "- in order to work in a multi-threaded context.\r\n" +
        "\r\n" +
        "Note regarding choice of which AgoRapide library to put this class into:\r\n" +
        "At time of development, no clear natural home was found for this class.\r\n" +
        "It was first put into -" + nameof(ARComponents.ARCDoc) + "-, then -" + nameof(ARComponents.ARCQuery) + "- " +
        "because it used -" + nameof(QueryExpression) + "-.-" +nameof(QueryExpression.TryParse) + "-.\r\n" +
        "It no longer uses QueryExpression.TryParse, but since it does use the concept of -" + nameof(EntityMethodKey) +"-, " +
        "moving back again to ARCDoc looks unnatural (but is quite possible).\r\n" +
        "Note that this class also refers to ARCEvent in the comments.\r\n" +
        "\r\n" +
        "See also -" + nameof(TranslationsP) + "-.\r\n"
    )]
    public class Translations : PConcurrent // Inherit PConcurrent because of TryGetP below
    {
        public override bool TryGetP<T>(IK key, out T p, out string errorResponse)
        {
            if (!base.TryGetP<TranslationSingle>(key, out var pts, out errorResponse))
            {
                if (!key.ToString().StartsWith("_"))
                {
                    p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse += ".\r\nDetails: " +
                        "Key does not start with underscore ('_'). Will therefore not add as IsMissing.\r\n" +
                        "The reason for this is to make it possible to use -" + nameof(ARComponents.ARCQuery) + "- against this class " +
                        "(if every key was added here, then attempts to parse as -QueryExpression- " +
                        "(see -" + nameof(ARComponents.ARCQuery) + "-) would not be possible).\r\n";
                    return false;
                }

                // Abandoned approach (Giving up on adding translations from "outside", and also using short URLs)
                /// Due to the <see cref="PropertyStreamLine.TryStore"/> works
                /// (see line "if (!currentPointer.TryGetP(keys[i].key, out var newCurrentPointer)) {")
                /// when attempting to add Text, we will have returned here (below) a new TranslationSingle
                /// object, for which <see cref="TranslationSingleP.Text"/> will then be changed, 
                /// but not <see cref="TranslationSingle.Text"/>, meaning the object will have two different Texts.
                //if (!(p is null) && p.TryGetPV<string>("Text", out var existingValue))
                //{
                //    /// A translation Text has been added in a manner which did not result in a 
                //    /// TranslationSingle object. 
                //    /// (typically through an API-call like /Add/t/_{key}/Text = {text})
                //    /// 
                //    /// Change into a TranslationSingle object.
                //    /// This makes it possible to use <see cref="TranslationSingle.ReferenceCount"/> for instance.
                //    /// 
                //    /// NOTE: This is due to a deliberate choice being made in <see cref="ARComponents.ARCEvent"/> to avoid long API URLs 
                //    /// NOTE: (that is, to not use syntax more like /Add/Translation/_{key}/Text = {text})
                //    /// NOTE: See related code in RunMacroR
                //    pts = new TranslationSingle(
                //       text: existingValue,
                //       isMissing: false
                //   );
                //}
                //else
                //{
                pts = new TranslationSingle(
                   text: key.ToString()[1..],
                   isMissing: true
               );
                // }

                // Note that for a "simultaneous" miss from two threads, one of the values initialized
                // may now be discarded.
                try
                {
                    REx.Inc(); // There are som recursive issues here, therefore this guard.
                    IP.TrySetP(key, pts, out _); // Important to use TrySet and not TryAdd, because TryAdd will call us again.
                }
                finally
                {
                    REx.Dec();
                }
            }
            System.Threading.Interlocked.Increment(ref pts.ReferenceCount);
            if (!(pts is T temp)) // NOTE: TranslationSingle is only the meaning value for T here.
            {
                p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = 
                    "Found type " + pts.GetType().ToStringShort() + ", not " + typeof(T).ToStringShort() + ".\r\n" + 
                    "NOTE: It is probably meaningless to ask for anything else than " + nameof(TranslationSingle) + " here.";
                return false;
            }
            p = temp;
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }
    }

    [Class(Description =
        "This would consist of one key per language, with keys like \"de_DE\", \"fr_FR\", \"nb_NO\"."
    )]
    public class TranslationsCollection : PCollection
    {

    }

    [Enum(Description =
        "This enum has no values because the keys are the actual string to be translated.\r\n" +
        "\r\n" +
        "Describes class -" + nameof(Translations) + "-."
    )]
    public enum TranslationsP
    {
        __invalid,
    }

    [Class(Description =
        "Describes a single translation.\r\n" +
        "\r\n" +
        "See -" + nameof(Translations) + "- for information.\r\n" +
        "\r\n" +
        "Note hybrid approach, implementing -" + nameof(IP) + "- for flexible querying, and offering " +
        "traditional C# property for high performance.\r\n" +
        "\r\n" +
        "Note how -" + nameof(ReferenceCount) + "- may be changed from outside.\r\n"
    )]
    public class TranslationSingle : PRich
    {
        public string Text { get; private set; }

        //public bool IsMissing { get; private set; }

        [ClassMember(Description =
            "May be set from outside, for instance with System.Threading.Interlocked.Increment.\r\n" +
            "\r\n" +
            "See also -" + nameof(TryGetReferenceCount) + "-.\r\n"
        )]
        public long ReferenceCount; // { get; set; } // Do not use "get; set;" because that does not work with System.Threading.Interlocked.Increment

        [ClassMember(Description = "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-.")]
        public bool TryGetReferenceCount(out PValue<long> retval, out string er)
        {
            retval = new PValue<long>(ReferenceCount);
            er = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true; // Note: True in the sense metod succeede, actually value desires is inside the PValue object
        }

        [ClassMember(Description =
            "Returns a -" + nameof(PropertyStreamLine) + "- representation of this translation.\r\n" +
            "\r\n" +
            "Supports Approach 1: Storing translations in the classic -" + nameof(ARConcepts.PropertyStream) + "-:\r\n" +
            "\r\n" +
            "TODO: Language is not supported here.\r\n" +
            "Current output looks like this:\r\n" +
            "  't/TranslationSingle/_Car = Wagen'\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetPS(IP dataStorage, IK thisKey, out PValue<string> retval, out string er)
        {
            retval = new PValue<string>(
                (
                    PSPrefix.t + "/" +
                    nameof(TranslationSingle) + "/" +
                    PropertyStreamLine.EncodeKeyPart(thisKey.ToString()) + " = " +
                    PropertyStreamLine.EncodeValuePart(
                        !IP.GetPV<bool>(TranslationSingleP.IsMissing, defaultValue: false) ?
                            Text :
                            "<Insert translation here>"
                    )
                ).Replace(
                    "0x005B", "[").Replace( // Relax encoding
                    "0x005D", "]").Replace(
                    "0x003C", "<").Replace(
                    "0x003E", ">").Replace(
                    "0x007B", "{").Replace(
                    "0x007D", "}"
                )
            );
            er = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true; // Note: True in the sense metod succeede, actually value desires is inside the PValue object
        }

        [ClassMember(Description =
            "Returns a -" + nameof(PropertyStreamLine) + "- representation of this translation.\r\n" +
            "\r\n" +
            "Supports approach 2: Storing translation strings directly within C# source code.\r\n" +
            "\r\n" +
            "Output looks like:\r\n" +
            "  'a(\"car\", \"wagen\");'\r\n" +  
            "\r\n" +
            "Generated C# code can be parsed into a relevant .cs file, like Translations_de_DE.cs\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetA(IP dataStorage, IK thisKey, out PValue<string> retval, out string er)
        {
            retval = new PValue<string>(
                "a(\r\n" +
                    "   \"" +
                        thisKey.ToString()[1..] + "\",\r\n" +
                    "   \"" +
                    (IP.GetPV<bool>(TranslationSingleP.IsMissing, defaultValue: false) ?
                        Text :
                        "<Insert translation here>"
                    ) +
                    "\"\r\n" +
                ");\r\n"
            );
            er = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true; // Note: True in the sense metod succeede, actually value desires is inside the PValue object
        }

        [ClassMember(Description =
            "Returns a -" + nameof(ARConcepts.PropertyStream) + "- representation of this translation.\r\n" +
            "\r\n" +
            "Suppports approach 3: Storing translations as -MacroAttribute-.-Translation-.\r\n" +
            "Syntax used is understood specifically by -RunMacroR-.\r\n" +
            "\r\n" +
            "TODO: Language is not supported here, the current apporach will only support one language\r\n" +
            "TOOD: at a time per API process.\r\n" +
            "\r\n" +
            "Output looks like:\r\n" +
            "  'Car = Wagen'\r\n" +
            "\r\n" +
            "Method signature understood by -" + nameof(ARCQuery.Extensions.TryGetP) + "- / -" + nameof(ARCQuery.EntityMethodKey) + "-."
        )]
        public bool TryGetMacro(IP dataStorage, IK thisKey, out PValue<string> retval, out string er)
        {
            retval = new PValue<string>(
                (
                    /// PSPrefix.t + "/" + // "t/" is unnecessary, assumed by RunMacroR

                    PropertyStreamLine.EncodeKeyPart(thisKey.ToString()[1..]) + " = " +
                    PropertyStreamLine.EncodeValuePart(
                        !IP.GetPV<bool>(TranslationSingleP.IsMissing, defaultValue: false) ?
                            Text :
                            "<Insert translation here>"
                    )
                ).Replace(
                    "0x005B", "[").Replace( // Relax encoding
                    "0x005D", "]").Replace(
                    "0x003C", "<").Replace(
                    "0x003E", ">").Replace(
                    "0x007B", "{").Replace(
                    "0x007D", "}"
                )
            );
            er = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true; // Note: True in the sense metod succeede, actually value desires is inside the PValue object
        }

        public TranslationSingle()
        {
            Text = "[INITIALIZED FROM PARAMETER-LESS CONSTRUCTOR]";
        }

        public TranslationSingle(string text, bool isMissing)
        {
            Text = text;
            IP.AddPV(TranslationSingleP.Text, text);
            if (isMissing) IP.AddPV(TranslationSingleP.IsMissing, isMissing);
        }

        // public override string ToString() => Text + (!IsMissing ? "" : " (IsMissing)");
    }

    [Enum(
        Description = "Describes class -" + nameof(TranslationSingle) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum TranslationSingleP
    {
        __invalid,

        Text,

        [PKType(
            Description =
                "Value TRUE indicates that the Text given is simply the original key, in other words, the translation " +
                "for the key is actually missing.",
            Type = typeof(bool)
        )]
        IsMissing
    }

    public class TranslationException : ApplicationException
    {
        public TranslationException(string message) : base(message) { }
#pragma warning disable IDE0060 // Remove unused parameter
        public TranslationException(string message, Exception inner) : base(message) { }
#pragma warning restore IDE0060 // Remove unused parameter
    }
}