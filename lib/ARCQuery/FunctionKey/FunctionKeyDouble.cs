﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {

    [Class(Description =
        "Converts an integer to a double (decimal value).\r\n" +
        "\r\n" +
        "Useful in order to force a division to be of type double (with decimals) (see -" + nameof(BinaryOperatorKey) +"-).\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyDouble : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'Double()'\r\n" +
            "Examples:\r\n" +
            "'Order/SELECT Sum.Double()/Quantity AS UnitPrice'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyDouble));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<long>(p, tryParser: null, transformer: lng => new PValue<double>((double)lng), out retval, out errorResponse);

        public new static FunctionKeyDouble Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyDoubleException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyDouble retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyDouble retval, out string errorResponse) {
            if (TryParseSingleWord(value, "double", () => new FunctionKeyDouble(), () => SyntaxHelp, out retval, out errorResponse)) return true;
            if (TryParseSingleWord(value, "decimal", () => new FunctionKeyDouble(), () => SyntaxHelp, out retval, out errorResponse)) return true;
            if (TryParseSingleWord(value, "dec", () => new FunctionKeyDouble(), () => SyntaxHelp, out retval, out errorResponse)) return true;
            // Get back original error response (requiring 'Double').
            return TryParseSingleWord(value, "double", () => new FunctionKeyDouble(), () => SyntaxHelp, out retval, out errorResponse);
        }
        public override string ToString() => "Double()";
        public class InvalidFunctionKeyDoubleException : ApplicationException {
            public InvalidFunctionKeyDoubleException(string message) : base(message) { }
            public InvalidFunctionKeyDoubleException(string message, Exception inner) : base(message, inner) { }
        }
    }
}