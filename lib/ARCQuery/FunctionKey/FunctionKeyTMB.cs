﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {

    [Class(Description =
        "TMB = Thousands, Millions, Billions.\r\n" +
        "\r\n" +
        "Formats big numbers in an easier to understand format as number of Thousands, number of Millions, number of Billions.\r\n" +
        "(Note that K (Kilo) is used for Thousands, like 10K = 10 Thousand).\r\n" +
        "\r\n" +
        "Useful in economic reports where the magnitude of the numbers are more important than the exact numbers\r\n" +
        "(Note: Sorting on the generated values is difficult as of Jul 2020, because it is done alphabetically).\r\n" +
        "\r\n" +
        "TODO: Introduce also KMGT for Kilo, Mega, Giga, Tera etc (powers of 1000), and KiMiGiTi for (powers of 1024).\r\n" +
        "\r\n" +
        "TODO: This function does not work well before 'ORDER BY' (-" + nameof(QueryExpressionOrderBy) + "-), " +
        "TODO: and we can not use it AFTER 'ORDER BY', because " +
        "TODO: ordering is not kept between each -" + nameof(QueryExpression) + "- (because keys are not required to be sorted in -" + nameof(IP) + "-)." +
        "TODO: Possible fix: Do not return PValue<string> but PValue<TypeX> where TypeX is understood på 'ORDER BY' through " +
        "TOOD: some standard .Net mechanism for comparing.\r\n" +
        "TODO: The TypeX instance could then store both the double-value (for comparing) and its TMB-string representation (for displaying).\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyTMB : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'TMB()'\r\n" +
            "Examples:\r\n" +
            "'Customer/SELECT Name, OrderSum.TMB()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyTMB));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<double>(p, tryParser: null, transformer: Amount => new PValue<string>(new Func<string>(() => {
                if (Amount >= 50000000000) return (Amount / 1000000000).ToString("0").Replace(",", ".") + "B";
                if (Amount >= 1000000000) return (Amount / 1000000000).ToString("0.0").Replace(",", ".") + "B";
                if (Amount >= 50000000) return (Amount / 1000000).ToString("0").Replace(",", ".") + "M";
                if (Amount >= 1000000) return (Amount / 1000000).ToString("0.0").Replace(",", ".") + "M";
                if (Amount >= 50000) return (Amount / 1000).ToString("0").Replace(",", ".") + "K";
                if (Amount >= 1000) return (Amount / 1000).ToString("0.0").Replace(",", ".") + "K";
                if (Amount > -1000) return (Amount).ToString("0");
                if (Amount > -50000) return (Amount / 1000).ToString("0.0").Replace(",", ".") + "K";
                if (Amount > -1000000) return (Amount / 1000).ToString("0").Replace(",", ".") + "K";
                if (Amount > -50000000) return (Amount / 1000000).ToString("0.0").Replace(",", ".") + "M";
                if (Amount > -1000000000) return (Amount / 1000000).ToString("0").Replace(",", ".") + "M";
                if (Amount > -50000000000) return (Amount / 1000000000).ToString("0.0").Replace(",", ".") + "B";
                return (Amount / 1000000000).ToString("0").Replace(",", ".") + "B";
            })()), out retval, out errorResponse);

        public new static FunctionKeyTMB Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyTMBException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyTMB retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyTMB retval, out string errorResponse) =>
            TryParseSingleWord(value, "tmb", () => new FunctionKeyTMB(), () => SyntaxHelp, out retval, out errorResponse);
        public override string ToString() => "TMB()";
        public class InvalidFunctionKeyTMBException : ApplicationException {
            public InvalidFunctionKeyTMBException(string message) : base(message) { }
            public InvalidFunctionKeyTMBException(string message, Exception inner) : base(message, inner) { }
        }
    }
}