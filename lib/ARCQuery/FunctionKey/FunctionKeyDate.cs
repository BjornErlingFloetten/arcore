﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {

    [Class(Description =
        "Formats a date without time component.\r\n" +
        "\r\n" +
        "Uses format 'yyyy-MM-dd'.\r\n" +
        "\r\n" +
        "Example: '2021-03-02 14:35:00' becomes '2021-03-02'.\r\n" +
        "\r\n" +
        "Note: For dates that have 'no' time-component, like '2021-03-02 00:00:00.000', " +
        "-" + nameof(PValue<TValue>.ConvertObjectToString) + "- will use only the date information, like '2021-03-02', " +
        "leaving -" + nameof(FunctionKeyDate) + "- somewhat superfluous.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyDate : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'Date()'\r\n" +
            "Examples:\r\n" +
            "'Order/PIVOT Created.Date() BY Product'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyDate));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<DateTime>(p, tryParser: null, transformer: dateTime => new PValue<string>(dateTime.ToString("yyyy-MM-dd")), out retval, out errorResponse);

        public new static FunctionKeyDate Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyDateException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyDate retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyDate retval, out string errorResponse) =>
            TryParseSingleWord(value, "date", () => new FunctionKeyDate(), () => SyntaxHelp, out retval, out errorResponse);
        public override string ToString() => "Date()";
        public class InvalidFunctionKeyDateException : ApplicationException {
            public InvalidFunctionKeyDateException(string message) : base(message) { }
            public InvalidFunctionKeyDateException(string message, Exception inner) : base(message, inner) { }
        }
    }
}