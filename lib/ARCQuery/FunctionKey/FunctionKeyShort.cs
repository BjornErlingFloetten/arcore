﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery
{
    [Class(Description =
        "'Smart' way of showing time. Removes unnecessary information, for instance if date is the same as the current date " +
        "then the date will not be shown at all. Trailing zeroes are also removed.\r\n " +
        "Example: '2020-12-21 15:40:00.000' at date '2021-12-21' becomes '15:40'.\r\n" +
        "\r\n" +
        "Note: Do not confuse -" + nameof(FunctionKeyShort) + "- (date / time formatting) with " +
        "-" + nameof(FunctionKeyVeryShort) + "- (type formatting).\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyShort : FunctionKey
    {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'Short()'\r\n" +
            "Examples:\r\n" +
            "'Order/SELECT Amount, Created.Short()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyShort));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<DateTime>(p, tryParser: null, transformer: dateTime => new PValue<string>(Short(dateTime)), out retval, out errorResponse);

        public static string Short(DateTime dateTime) =>
            (dateTime.Date.Equals(UtilCore.DateTimeNow.Date) ? // No need to show date.
            (
                new Func<string>(() =>
                {
                    if (dateTime.Millisecond != 0) return dateTime.ToString("HH:mm:ss.fff");
                    if (dateTime.Second != 0) return dateTime.ToString("HH:mm:ss");
                    return dateTime.ToString("HH:mm");
                })()
            ) :
            (
                (dateTime.ToString("yyyy-MM-dd"))) +
                new Func<string>(() =>
                {
                    if (dateTime.Millisecond != 0) return " " + dateTime.ToString("HH:mm:ss.fff");
                    if (dateTime.Second != 0) return " " + dateTime.ToString("HH:mm:ss");
                    if (dateTime.Minute != 0 || dateTime.Hour != 0) return " " + dateTime.ToString("HH:mm");
                    return ""; // Date was sufficient (time is at midnight)
                })()
            );

        public new static FunctionKeyShort Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyShortException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyShort retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyShort retval, out string errorResponse) =>
            TryParseSingleWord(value, "short", () => new FunctionKeyShort(), () => SyntaxHelp, out retval, out errorResponse);
        public override string ToString() => "Short()";
        public class InvalidFunctionKeyShortException : ApplicationException
        {
            public InvalidFunctionKeyShortException(string message) : base(message) { }
            public InvalidFunctionKeyShortException(string message, Exception inner) : base(message, inner) { }
        }
    }
}