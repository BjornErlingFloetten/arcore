﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {

    [Class(Description =
        "Converts a timespan to total minutes (as an integer).\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyTotalMinutes : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'TotalMinutes()'\r\n"  // TODO: Create example here
                                    //"Examples:\r\n" +
                                    //"'Order/PIVOT Created.TotalMinutes() BY Product'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyTotalMinutes));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<TimeSpan>(p, tryParser: null, transformer: timeSpan => new PValue<long>((long)(timeSpan.TotalMinutes + .5)), out retval, out errorResponse);

        public new static FunctionKeyTotalMinutes Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyTotalMinutesException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyTotalMinutes retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyTotalMinutes retval, out string errorResponse) =>
            TryParseSingleWord(value, "totalminutes", () => new FunctionKeyTotalMinutes(), () => SyntaxHelp, out retval, out errorResponse);
        public override string ToString() => "TotalMinutes()";
        public class InvalidFunctionKeyTotalMinutesException : ApplicationException {
            public InvalidFunctionKeyTotalMinutesException(string message) : base(message) { }
            public InvalidFunctionKeyTotalMinutesException(string message, Exception inner) : base(message, inner) { }
        }
    }
}