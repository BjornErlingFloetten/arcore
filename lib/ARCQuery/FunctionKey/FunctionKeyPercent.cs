﻿// Copyright (c) 2016-2022 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery
{
    [Class(Description =
        "Multiplies the field value by 100 and adds a percentage sign.\r\n" +
        "Example: '0.035' becomes '3.50 %.'\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyPercent : FunctionKey
    {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'Percent() or Pct()'\r\n" +
            "Example:\r\n" +
            "'Order/SELECT Rebate.Percent()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyPercent));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<double>(p, tryParser: null, transformer: dbl => new PValue<string>((dbl * 100).ToString("0.00").Replace(",", ".") + " %"), out retval, out errorResponse);

        public new static FunctionKeyPercent Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyPercentException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyPercent retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyPercent retval, out string errorResponse) =>
            TryParseSingleWord(value, "pct", () => new FunctionKeyPercent(), () => SyntaxHelp, out retval, out errorResponse) || 
            TryParseSingleWord(value, "percent", () => new FunctionKeyPercent(), () => SyntaxHelp, out retval, out errorResponse);

        public override string ToString() => "Percent()";
        public class InvalidFunctionKeyPercentException : ApplicationException
        {
            public InvalidFunctionKeyPercentException(string message) : base(message) { }
            public InvalidFunctionKeyPercentException(string message, Exception inner) : base(message, inner) { }
        }
    }
}