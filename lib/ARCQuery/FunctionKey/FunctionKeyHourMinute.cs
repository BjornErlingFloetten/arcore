﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {

    [Class(Description =
        "Formats a timespan with only the hour and minute part. Uses format hh:mm.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyHourMinute : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'HourMinute()'\r\n" // TODO: Create example here
            //"Examples:\r\n" +
            //"'Order/PIVOT Created.TotalMinutes() BY Product'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyHourMinute));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<TimeSpan>(p, tryParser: null, transformer: timeSpan => new PValue<string>(timeSpan.ToString(@"hh\:mm")), out retval, out errorResponse);

        public new static FunctionKeyHourMinute Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyHourMinuteException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyHourMinute retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyHourMinute retval, out string errorResponse) =>
            TryParseSingleWord(value, "hourminute", () => new FunctionKeyHourMinute(), () => SyntaxHelp, out retval, out errorResponse);
        public override string ToString() => "HourMinute()";
        public class InvalidFunctionKeyHourMinuteException : ApplicationException {
            public InvalidFunctionKeyHourMinuteException(string message) : base(message) { }
            public InvalidFunctionKeyHourMinuteException(string message, Exception inner) : base(message, inner) { }
        }
    }
}