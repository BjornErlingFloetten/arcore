﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery
{
    [Class(Description =
        "Returns the absolute value of field, like 4 for '-4'.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyAbs : FunctionKey
    {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'Abs()'\r\n" +
            "Example (somewhat contrived):\r\n" +
            "'Invoice/WHERE Amount.Abs() > 10000'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyAbs));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<double>(p, tryParser: null, transformer: dbl => new PValue<double>(Math.Abs(dbl)), out retval, out errorResponse);

        public new static FunctionKeyAbs Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyAbsException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyAbs retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyAbs retval, out string errorResponse) =>
            TryParseSingleWord(value, "abs", () => new FunctionKeyAbs(), () => SyntaxHelp, out retval, out errorResponse);
        public override string ToString() => "Abs()";
        public class InvalidFunctionKeyAbsException : ApplicationException
        {
            public InvalidFunctionKeyAbsException(string message) : base(message) { }
            public InvalidFunctionKeyAbsException(string message, Exception inner) : base(message, inner) { }
        }
    }
}