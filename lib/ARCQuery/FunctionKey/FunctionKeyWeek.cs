﻿// Copyright (c) 2016-2021 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
namespace ARCQuery {

    [Class(Description =
        "Extract week-number from field according to ISO 8601.\r\n" +
        "\r\n" +
        "Uses the System.Globalization.ISOWeek.GetWeekOfYear() method in .Net.\r\n" +
        "\r\n" +
        "See also -" + nameof(FunctionKeyYearWeek) + "-.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyWeek : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'Week()'\r\n" +
            "Examples:\r\n" +
            "'Order/WHERE Order.Created.Week() < 52',\r\n" +
            "'Customer/SELECT Name, Order.Created.Week()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyWeek));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<DateTime>(p, tryParser: null, transformer: dateTime => new PValue<string>(System.Globalization.ISOWeek.GetWeekOfYear(dateTime).ToString("00")), out retval, out errorResponse);

        public new static FunctionKeyWeek Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyWeekException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyWeek retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyWeek retval, out string errorResponse) =>
            TryParseSingleWord(value, "week", () => new FunctionKeyWeek(), () => SyntaxHelp, out retval, out errorResponse);
        public override string ToString() => "Week()";
        public class InvalidFunctionKeyWeekException : ApplicationException {
            public InvalidFunctionKeyWeekException(string message) : base(message) { }
            public InvalidFunctionKeyWeekException(string message, Exception inner) : base(message, inner) { }
        }
    }
}