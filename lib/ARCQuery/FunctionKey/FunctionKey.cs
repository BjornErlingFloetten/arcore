﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery
{
    [Class(Description =
        "Extracts data from an already existing value like extracting Year from a DateTime in 'Created.Year()'.\r\n" +
        "\r\n" +
        "This is an expandable concept, that is, you can create your own implementations of this class " +
        "in your -" + nameof(ARConcepts.ApplicationSpecificCode) + "-.\r\n" +
        "\r\n" +
        "Often the extraction is actually a simplification of the existing value, like " +
        "Created.Year() which would for '2020-06-11 13:36' return '2020'.\r\n" +
        "\r\n" +
        "Implementing classes in -" + nameof(ARConcepts.StandardAgoRapideCode) + "-, more specific -" + nameof(ARComponents.ARCQuery) + "-, are:\r\n" +
        "-" + nameof(FunctionKeyAbs) + "-\r\n" +
        "-" + nameof(FunctionKeyDate) + "-\r\n" +
        "-" + nameof(FunctionKeyDayOfWeek) + "-\r\n" +
        "-" + nameof(FunctionKeyDouble) + "-\r\n" +
        "-" + nameof(FunctionKeyFirstLine) + "-\r\n" +
        "-" + nameof(FunctionKeyHourMinute) + "-\r\n" +
        "-" + nameof(FunctionKeyInt) + "-\r\n" +
        "-" + nameof(FunctionKeyLength) + "-\r\n" +
        "-" + nameof(FunctionKeyMonth) + "-\r\n" +
        "-" + nameof(FunctionKeyQuarter) + "-\r\n" +
        "-" + nameof(FunctionKeyShort) + "-\r\n" +
        "-" + nameof(FunctionKeyTMB) + "-\r\n" +
        "-" + nameof(FunctionKeyTotalHours) + "-\r\n" +
        "-" + nameof(FunctionKeyTotalMinutes) + "-\r\n" +
        "-" + nameof(FunctionKeyType) + "-\r\n" +
        "-" + nameof(FunctionKeyVeryShort) + "-\r\n" +
        "-" + nameof(FunctionKeyWeek) + "-\r\n" +
        "-" + nameof(FunctionKeyYear) + "-\r\n" +
        "-" + nameof(FunctionKeyYearMonth) + "-\r\n" +
        "-" + nameof(FunctionKeyYearQuarter) + "-\r\n" +
        "-" + nameof(FunctionKeyYearWeek) + "-\r\n" +
        "(these are stored in -" + nameof(CompoundKey) + "- as -" + nameof(CompoundKey.FunctionKeys) + "-.)\r\n\r\n" +
        "and also (see -" + nameof(FunctionKeyAggregate) + "-):\r\n" +
        "-" + nameof(FunctionKeyAggregateAvg) + "-\r\n" +
        "-" + nameof(FunctionKeyAggregateCount) + "-\r\n" +
        "-" + nameof(FunctionKeyAggregateDistinct) + "-\r\n" +
        "-" + nameof(FunctionKeyAggregateJoin) + "-\r\n" +
        "-" + nameof(FunctionKeyAggregateMax) + "-\r\n" +
        "-" + nameof(FunctionKeyAggregateMin) + "-\r\n" +
        "-" + nameof(FunctionKeyAggregateSingle) + "-\r\n" +
        "-" + nameof(FunctionKeyAggregateSum) + "-\r\n" +
        "(these are stored in -" + nameof(ForeignKey) + "- as -" + nameof(ForeignKey.AggregateKeys) + "-.)\r\n" +
        "\r\n" +
        "TODO: Add a -" + nameof(PK) + "- field, describing the value that is returned.\r\n" +
        "\r\n" +
        "It can also be said to work as a function against a value field, but with the syntax 'value.functionName()' instead of " +
        "functionName(value).\r\n" +
        "\r\n" +
        "(See also -" + nameof(ValueComparer) + "- which can evaluate expressions like 'WHERE Created = ThisYear')\r\n" +
        "\r\n" +
        "Explanation of the conceptual difference between -" + nameof(ValueComparer) + "- and -" + nameof(FunctionKey) + "-:\r\n" +
        "These two concepts are working from two different 'origins'.\r\n" +
        "-" + nameof(FunctionKey) + "- is necessary if you want to pivot around the value (for instance with -" + nameof(QueryExpressionPivot) + "-).\r\n" +
        "-" + nameof(ValueComparer) + "- is more practical if the value can have different interpretations at the same time.\r\n" +
        "An example is -" + nameof(ValueComparerDateTime) + "- which understands that 'Created' can be all of " +
        "'ThisYear', 'ThisMonth', 'ThisWeek', 'InTheMorning', 'ThisMorning' and so on.\r\n" +
        "(It would be time consuming to write all the necessary -" + nameof(FunctionKey) + "- implementations in order to support this. " +
        "and the query syntax would be more verbose ('Created.YearNumber = ThisYear' or something similar, instead of just 'Created = ThisYear').)\r\n" +
        "\r\n" +
        "Note that the concept is inherently expandable, you can create your own sub-classes " +
        "and integrate them with -" + nameof(ARConcepts.StandardAgoRapideCode) + "-.\r\n" +
        "\r\n" +
        "Note how any -" + nameof(ARConcepts.ApplicationSpecificCode) + "- implementations must be added to " +
        "-" + nameof(Parsers) + "- at application startup in order for them to be recognized by -" + nameof(QueryExpression.TryParse) + "-."
    )]
    public abstract class FunctionKey : ITypeDescriber, IEquatable<FunctionKey>
    {

        [ClassMember(Description =
            "NOTE: The in-parameter is a property, not an entity.\r\n" +
            "\r\n" +
            "TODO: Consider renaming into TryTransform, TryExecute or similar."
        )]
        public abstract bool TryGetP(IP p, out IP retval, out string errorResponse);

        [ClassMember(Description =
            "Instances of all the relevant sub classes of -" + nameof(FunctionKey) + "-.\r\n" +
            "Used by -" + nameof(TryParse) + "- in order to parse to correct sub class.\r\n" +
            "\r\n" +
            "Key usually corresponds to actual class name like 'Year' for -" + nameof(FunctionKeyYear) + "-, " +
            "'YearQuarter' for -" + nameof(FunctionKeyYearQuarter) + " and so on.\r\n" +
            "\r\n" +
            "Note that multiple sub classes can share the same function name (because value in this dictionary is List<...>).\r\n" +
            "If multiple parsers thereby exists then the first parser that succeeds with parsing 'wins'.\r\n" +
            "\r\n" +
            "(This primitive form of function overloading could for instance be resolved by the parsers " +
            "differentiating by entity type and key, whether they should fail or not. " +
            "For instance, if you have several sub classes all with the function name 'Category', for categorizing certain values, " +
            "like for Order Value.Category() and for Product Price.Category() the parser could differentiate by entity type (Order or Product), " +
            "and / or by key, (Value or Price).)\r\n" +
            "TODO: NOT POSSIBLE AS OF JUN 2020 BECAUSE ENTITY TYPE / KEY NOT KNOWN WHEN PARSING.\r\n" +
            "\r\n" +
            "Add to this collection if you have -" + nameof(FunctionKey) + "- sub classes " +
            "implemented in your -" + nameof(ARConcepts.ApplicationSpecificCode) + "-.\r\n" +
            "\r\n" +
            "TODO: Consider automatic injecting to this collection at application startup, " +
            "TODO: also for -" + nameof(ARConcepts.StandardAgoRapideCode) + "-, that is, eliminating code below.\r\n" +
            "\r\n" +
            "NOTE: Be careful to actually implement TryParse in your implementation.\r\n" +
            "NOTE: If not implemented then TryParse in this class (-" + nameof(TryParse) + "-)\r\n" +
            "NOTE: will only call itself leading to a stack overflow exception.\r\n" +
            "\r\n" +
            "See also -" + nameof(AddParser) + "-.\r\n"
        )]
        public static Dictionary<string, List<Func<string, (FunctionKey? queryId, string? errorResponse)>>> Parsers = new Func<Dictionary<string, List<Func<string, (FunctionKey? queryId, string? errorResponse)>>>>(() =>
        {
            var retval = new Dictionary<string, List<Func<string, (FunctionKey? queryId, string? errorResponse)>>>();
            var adder = new Action<string, Func<string, (FunctionKey? queryId, string? errorResponse)>>((firstWord, parser) =>
                  /// Note how all sub classes in <see cref="ARConcepts.StandardAgoRapideCode"/> 
                  retval[firstWord] = new List<Func<string, (FunctionKey? queryId, string? errorResponse)>> {
                    parser
                  }
            );
            adder("ABS", value => FunctionKeyAbs.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("LENGTH", value => FunctionKeyLength.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));

            adder("DATE", value => FunctionKeyDate.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));

            // Note: Parser does also understand Long and Integer, we could add those here
            adder("INT", value => FunctionKeyInt.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));

            // Note: Parser does also understand Dec and Decimal, we could add those here
            adder("DOUBLE", value => FunctionKeyDouble.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));

            adder("PERCENT", value => FunctionKeyPercent.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("PCT", value => FunctionKeyPercent.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));

            adder("TYPE", value => FunctionKeyType.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("VERYSHORT", value => FunctionKeyVeryShort.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));

            adder("TMB", value => FunctionKeyTMB.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("FIRSTLINE", value => FunctionKeyFirstLine.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));

            adder("HOURMINUTE", value => FunctionKeyHourMinute.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("TOTALHOURS", value => FunctionKeyTotalHours.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("TOTALMINUTES", value => FunctionKeyTotalMinutes.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));

            adder("YEAR", value => FunctionKeyYear.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("QUARTER", value => FunctionKeyMonth.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("MONTH", value => FunctionKeyMonth.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("YEARQUARTER", value => FunctionKeyYearQuarter.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("YEARMONTH", value => FunctionKeyYearMonth.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("DAYOFWEEK", value => FunctionKeyDayOfWeek.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("YEARWEEK", value => FunctionKeyYearWeek.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("WEEK", value => FunctionKeyWeek.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));

            adder("SHORT", value => FunctionKeyShort.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));

            adder("AVG", value => FunctionKeyAggregateAvg.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("COUNT", value => FunctionKeyAggregateCount.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("DISTINCT", value => FunctionKeyAggregateDistinct.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("JOIN", value => FunctionKeyAggregateJoin.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("MAX", value => FunctionKeyAggregateMax.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("MIN", value => FunctionKeyAggregateMin.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("SINGLE", value => FunctionKeyAggregateSingle.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));
            adder("SUM", value => FunctionKeyAggregateSum.TryParse(value, out var retval, out var errorResponse) ? (retval, (string?)null) : ((FunctionKey?)null, errorResponse));

            return retval;
        })();

        [ClassMember(Description =
            "Adds a -" + nameof(ARConcepts.ApplicationSpecificCode) + "- parser.\r\n" +
            "\r\n" +
            "Not used by -" + nameof(ARConcepts.StandardAgoRapideCode) + "-.\r\n" +
            "\r\n" +
            "Only to be done when -" + nameof(UtilCore.CurrentlyStartingUp) + "-."
        )]
        public static void AddParser(string firstWord, Func<string, (FunctionKey? queryId, string? errorResponse)> parser)
        {
            UtilCore.AssertCurrentlyStartingUp();
            if (!Parsers.ContainsKey(firstWord)) Parsers[firstWord] = new List<Func<string, (FunctionKey? queryId, string? errorResponse)>>();
            Parsers[firstWord].Add(parser);
        }

        public static FunctionKey Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKey retval) => TryParse(value, out retval, out _);
        [ClassMember(Description =
            "Find parsers from -" + nameof(Parsers) + "- based on first word in query.\r\n" +
            "\r\n" +
            "NOTE: Be careful to actually implement TryParse in your implementation.\r\n" +
            "NOTE: If not implemented then TryParse in this class (-" + nameof(TryParse) + "-)\r\n" +
            "NOTE: will only call itself leading to a stack overflow exception.\r\n"
        )]
        public static bool TryParse(string value, out FunctionKey retval, out string errorResponse)
        {

            //// TODO: Go through the whole parsing methodology here.
            //value = value.Trim();
            //var t = value.Split(".");
            //if (t.Length != 2) { // TODO: Can function keys also have next key? For Customer Creater.Year().Something ?
            //    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
            //    errorResponse = "Not two items separated by full stop (.) but " + t.Length + ".";
            //    return false;
            //}

            var pos = value.IndexOf("(");
            var firstWord = (pos == -1 ? value : value[..pos]).ToUpper();
            if (!Parsers.TryGetValue(firstWord, out var parsers))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                // TODO: Create pre-calculated value for most of this errorResponse
                errorResponse =
                    "Function expression (" + firstWord + ") not recognized.\r\n" +
                    "Must be one of\r\n" +
                    string.Join(", ", Parsers.Keys.Select(k => "'" + k + "'"));
                return false;
            }
            if (parsers.Count == 0)
            {
                throw new InvalidFunctionKeyException("Keyword '" + firstWord + "' resulted in 0 parsers. Key wrongly added to -" + nameof(Parsers) + "-.");
            }
            var errorResponses = new List<string>();

            // Try all parsers. The first one to parse correctly 'wins'.
            foreach (var parser in parsers)
            {
                var result = parser(value);
                if (result.queryId != null)
                { // First parser that suceeds wins
                    retval = result.queryId;
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
                }
                errorResponses.Add(result.errorResponse ?? throw new NullReferenceException(nameof(result.errorResponse) + " for parser '" + firstWord + "'. Value: '" + value + "'"));
            }

            retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            errorResponse =
                (parsers.Count == 1 ? "" : (
                    "Found " + parsers.Count + " possible parsers for expressions beginning with '" + firstWord + "'.\r\n" +
                    "None of them succeeded.\r\n"
                )) +
                nameof(errorResponses) + ":\r\n" +
                string.Join("\r\n", errorResponses);
            return false;
        }

        /// <param name="creator">
        /// First parameter is <see cref="CompoundKey.NextKey"/>
        /// Second parameter is <see cref="Key"/>
        /// </param>
        [ClassMember(Description =
            "Recommended parser to use for functions without parameters. " +
            "\r\n" +
            "See for instance use in -" + nameof(FunctionKeyYear) + "-.-" + nameof(FunctionKeyYear.TryParse) + "-.\r\n" +
            "\r\n" +
            "TODO: Does it have meaning letting this class implement ITypeDescriber?\r\n" +
            "TODO: (considering that it must also contain the Key, like the key Created in for Customer SELECT Created.Year()\r\n" +
            "TODO: Does it have any use?\r\n"
        )]
        public static bool TryParseSingleWord<T>(string value, string singleWord, Func<T> creator, Func<string> syntaxHelp, out T retval, out string errorResponse) where T : FunctionKey
        {
            value = value.Trim();
            if (!(singleWord + "()").Equals(value.ToLower()))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
                errorResponse = "Not '" + singleWord + "()'." + syntaxHelp();
                return false;
            }
            retval = creator();
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Overload which will try to call -" + nameof(IP.TryGetV) + "- with generic type parameter, and if that does not " +
            "succeed will try to call the optional 'tryParser'.\r\n" +
            "NOTE: The 'tryParser' is probably not necessary for types like int, long, double, DateTime, TimeSpan,\r\n" +
            "NOTE: as those are understood by -" + nameof(PValue<TValue>.TryGetV) + "- anyway.\r\n" +
            "\r\n" +
            "Afterwards will then transform value as instructed.\r\n" +
            "\r\n" +
            "See for instance use in -" + nameof(FunctionKeyYear) + "-.-" + nameof(FunctionKeyYear.TryGetP) + "-.\r\n" +
            "\r\n" +
            "TODO: Became a little too complicated. Could possibly be split into one function for getting value and parsing it,\r\n" +
            "TODO: and one for transforming it.\r\n" +
            "\r\n"
        )]
        public static bool TryGetP<T>(
            IP p,
            Func<string, (bool success, T retval, string errorResponse)>? tryParser,
            Func<T, IP> transformer,
            out IP retval,
            out string errorResponse
        ) where T : notnull
        {
            if (p.TryGetV<T>(out var temp, out var errorResponse3))
            {
                retval = transformer(temp);
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
            if (!p.TryGetV<string>(out var strRetval, out var errorResponse4))
            {
                retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = p.GetType().ToStringVeryShort() + ": TryGetV on property failed with " + nameof(errorResponse) + ": " + errorResponse3 + " and TryGetV<string> failed with response " + errorResponse4;
                if (p is T)
                {
                    // TODO: Consider just returning 'this' instead? 
                    errorResponse += "\r\nNOTE: 'this' object (" + p.GetType().ToStringShort() + ") is of type requested (" + typeof(T).ToStringShort() + ").\r\nDid you mistakenly call TryGetPV instead of TryGetP? ";
                }
                return false;
            }
            if (tryParser == null)
            {
                retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Only string value resulted and no parser available";
                return false;
            }
            var tempRes = tryParser(strRetval);
            if (!tempRes.success)
            {
                retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = p.GetType().ToStringVeryShort() + ": TryGetV on property failed with " + nameof(errorResponse) + ": " + errorResponse3 + " and parser failed with response " + tempRes.errorResponse;
                if (p is T)
                {
                    // TODO: Consider just returning 'this' instead? 
                    errorResponse += "\r\nNOTE: 'this' object (" + p.GetType().ToStringShort() + ") is of type requested (" + typeof(T).ToStringShort() + ").\r\nDid you mistakenly call TryGetPV instead of TryGetP? ";
                }
                return false;
            }
            retval = transformer(tempRes.retval);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
            return true;
        }

        public override int GetHashCode() => ToString().GetHashCode();
        public bool Equals(FunctionKey other) => ToString().Equals(other.ToString());
        public override bool Equals(object other) => other is FunctionKey f && Equals(f);

        /// <summary>
        /// 'Implementing' <see cref="ITypeDescriber"/>
        /// </summary>
        /// <param name="key"></param>
        public static void EnrichKey(PK key) =>
            key.SetValidatorAndParser(new Func<string, ParseResult>(value =>
                TryParse(value, out var retval, out var errorResponse) ?
                    ParseResult.CreateOK(retval) :
                    ParseResult.CreateError(errorResponse)
            ));

        public class InvalidFunctionKeyException : ApplicationException
        {
            public InvalidFunctionKeyException(string message) : base(message) { }
            public InvalidFunctionKeyException(string message, Exception inner) : base(message, inner) { }
        }
    }
}