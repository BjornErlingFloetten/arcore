﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {

    [Class(Description =
        "Extracts Year + Month component from field like 2020-06.\r\n" +
        "\r\n" +
        "See also -" + nameof(FunctionKeyYearWeek) + "-.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyYearMonth : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'YearMonth()'\r\n" +
            "Examples:\r\n" +
            "'Order/PIVOT Created.YearMonth() BY Product'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyMonth));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<DateTime>(p, tryParser: null, transformer: dateTime => new PValue<string>(dateTime.ToString("yyyy-MM")), out retval, out errorResponse);

        public new static FunctionKeyYearMonth Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyYearMonthException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyYearMonth retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyYearMonth retval, out string errorResponse) =>
            TryParseSingleWord(value, "yearmonth", () => new FunctionKeyYearMonth(), () => SyntaxHelp, out retval, out errorResponse);
        public override string ToString() => "YearMonth()";
        public class InvalidFunctionKeyYearMonthException : ApplicationException {
            public InvalidFunctionKeyYearMonthException(string message) : base(message) { }
            public InvalidFunctionKeyYearMonthException(string message, Exception inner) : base(message, inner) { }
        }
    }
}