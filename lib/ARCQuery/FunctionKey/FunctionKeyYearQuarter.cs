﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {

    [Class(Description =
        "Extracts Year + Quarter component from field like 2020Q1.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyYearQuarter : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'YearQuarter()'\r\n" +
            "Examples:\r\n" +
            "'Order/PIVOT Created.YearQuarter() BY Product'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyYearQuarter));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<DateTime>(p, tryParser: null, transformer: dateTime => new PValue<string>(dateTime.Year + "Q" + (((dateTime.Month - 1) / 3) + 1)), out retval, out errorResponse);

        public new static FunctionKeyYearQuarter Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyYearQuarterException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyYearQuarter retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyYearQuarter retval, out string errorResponse) =>
            TryParseSingleWord(value, "yearquarter", () => new FunctionKeyYearQuarter(), () => SyntaxHelp, out retval, out errorResponse);
        public override string ToString() => "YearQuarter()";
        public class InvalidFunctionKeyYearQuarterException : ApplicationException {
            public InvalidFunctionKeyYearQuarterException(string message) : base(message) { }
            public InvalidFunctionKeyYearQuarterException(string message, Exception inner) : base(message, inner) { }
        }
    }
}