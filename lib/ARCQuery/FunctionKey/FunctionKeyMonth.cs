﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {
    [Class(Description =
        "Extracts month component from field like '2020-12-09' becoming '12'.\r\n" +
        "\r\n" +
        "Note: In many cases you probably want to use -" + nameof(FunctionKeyYearMonth) + "- instead of -" + nameof(FunctionKeyMonth) + "-.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyMonth : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'Month()'\r\n" +
            "Examples:\r\n" +
            "'Order/PIVOT Created.Year() BY Created.Month()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyMonth));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<DateTime>(p, tryParser: null, transformer: dateTime => new PValue<string>(dateTime.ToString("MM")), out retval, out errorResponse);

        public new static FunctionKeyMonth Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyMonthException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyMonth retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyMonth retval, out string errorResponse) =>
            TryParseSingleWord(value, "month", () => new FunctionKeyMonth(), () => SyntaxHelp, out retval, out errorResponse);
        public override string ToString() => "Month()";
        public class InvalidFunctionKeyMonthException : ApplicationException {
            public InvalidFunctionKeyMonthException(string message) : base(message) { }
            public InvalidFunctionKeyMonthException(string message, Exception inner) : base(message, inner) { }
        }
    }
}