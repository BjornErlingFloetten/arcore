﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {

    [Class(Description =
        "Extracts first line from string of text.\r\n" +
        "\r\n" +
        "Useful for instance in tables with lots of text, when you want a compact presentation.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyFirstLine : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'FirstLine()'\r\n" +
            "Examples:\r\n" +
            "'DocFrag/SELECT Description.FirstLine()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyFirstLine));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<string>(p, tryParser: null, transformer: s => new PValue<string>(new Func<string>(() => {
                var pos = s.IndexOf("\r\n");
                return pos == -1 ? s : s.Substring(0, pos);
            })()), out retval, out errorResponse);

        public new static FunctionKeyFirstLine Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyFirstLineException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyFirstLine retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyFirstLine retval, out string errorResponse) =>
            TryParseSingleWord(value, "firstline", () => new FunctionKeyFirstLine(), () => SyntaxHelp, out retval, out errorResponse);
        public override string ToString() => "FirstLine()";
        public class InvalidFunctionKeyFirstLineException : ApplicationException {
            public InvalidFunctionKeyFirstLineException(string message) : base(message) { }
            public InvalidFunctionKeyFirstLineException(string message, Exception inner) : base(message, inner) { }
        }
    }
}