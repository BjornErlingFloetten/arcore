﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {

    [Class(Description =
        "Rounds a double value to the nearest integer (uses .NET data type Int64 (long)).\r\n" +
        "(formulae used is: '(long)(dbl + .5)').\r\n" +
        "\r\n" +
        "TODO: Throws exception if double value is too big. Fix this (do not return any value in such a case maybe?)\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyInt : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'Int()'\r\n" +
            "Examples:\r\n" +
            "'Order/SELECT Amount.Int()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyInt));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<double>(p, tryParser: null, transformer: dbl => new PValue<long>((long)(dbl + .5)), out retval, out errorResponse);

        public new static FunctionKeyInt Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyIntException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyInt retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyInt retval, out string errorResponse) {
            if (TryParseSingleWord(value, "int", () => new FunctionKeyInt(), () => SyntaxHelp, out retval, out errorResponse)) return true;
            if (TryParseSingleWord(value, "integer", () => new FunctionKeyInt(), () => SyntaxHelp, out retval, out errorResponse)) return true;
            if (TryParseSingleWord(value, "long", () => new FunctionKeyInt(), () => SyntaxHelp, out retval, out errorResponse)) return true;
            // Get back original error response (requiring 'int').
            return TryParseSingleWord(value, "int", () => new FunctionKeyInt(), () => SyntaxHelp, out retval, out errorResponse);
        }
        public override string ToString() => "Int()";
        public class InvalidFunctionKeyIntException : ApplicationException {
            public InvalidFunctionKeyIntException(string message) : base(message) { }
            public InvalidFunctionKeyIntException(string message, Exception inner) : base(message, inner) { }
        }
    }
}