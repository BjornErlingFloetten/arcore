﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {
    [Class(Description =
        "Extracts quarter component from datetime field like '2020-12-09' becoming 'Q4'.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyQuarter : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'Quarter()'\r\n" +
            "Examples:\r\n" +
            "'Order/PIVOT Created.Year() BY Created.Quarter()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyQuarter));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<DateTime>(p, tryParser: null, transformer: dateTime => new PValue<string>("Q" + (((dateTime.Month - 1) / 3) + 1)), out retval, out errorResponse);

        public new static FunctionKeyQuarter Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyQuarterException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyQuarter retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyQuarter retval, out string errorResponse) =>
            TryParseSingleWord(value, "quarter", () => new FunctionKeyQuarter(), () => SyntaxHelp, out retval, out errorResponse);
        public override string ToString() => "Quarter()";
        public class InvalidFunctionKeyQuarterException : ApplicationException {
            public InvalidFunctionKeyQuarterException(string message) : base(message) { }
            public InvalidFunctionKeyQuarterException(string message, Exception inner) : base(message, inner) { }
        }
    }
}