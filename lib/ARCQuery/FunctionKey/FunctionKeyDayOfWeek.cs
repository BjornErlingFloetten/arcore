﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
namespace ARCQuery {

    [Class(Description =
        "Extracts DayOfWeek from field like 'Monday', 'Tuesday' and so on.\r\n" +
        "\r\n" +
        "TODO: Consider sorting issues here.\r\n" +
        "TODO: Create alternative, DayOfWeekInt or similar for integer-representation.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
        )]
    public class FunctionKeyDayOfWeek : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'DayOfWeek()'\r\n" +
            "Examples:\r\n" +
            "'Order/PIVOT Created.DayOfWeek() BY Product'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyDayOfWeek));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<DateTime>(p, tryParser: null, transformer: dateTime => new PValue<string>(dateTime.DayOfWeek.ToString()), out retval, out errorResponse);

        public new static FunctionKeyDayOfWeek Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyDayOfWeekException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyDayOfWeek retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyDayOfWeek retval, out string errorResponse) =>
            TryParseSingleWord(value, "dayofweek", () => new FunctionKeyDayOfWeek(), () => SyntaxHelp, out retval, out errorResponse);
        public override string ToString() => "DayOfWeek()";
        public class InvalidFunctionKeyDayOfWeekException : ApplicationException {
            public InvalidFunctionKeyDayOfWeekException(string message) : base(message) { }
            public InvalidFunctionKeyDayOfWeekException(string message, Exception inner) : base(message, inner) { }
        }
    }
}