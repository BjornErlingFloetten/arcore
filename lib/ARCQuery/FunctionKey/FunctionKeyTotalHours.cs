﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {

    [Class(Description =
        "Converts a timespan to total hours (as an integer).\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyTotalHours : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'TotalHours()'\r\n"  // TODO: Create example here
                                    //"Examples:\r\n" +
                                    //"'Order/PIVOT Created.TotalHours() BY Product'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyTotalHours));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<TimeSpan>(p, tryParser: null, transformer: timeSpan => new PValue<long>((long)(timeSpan.TotalHours + .5)), out retval, out errorResponse);

        public new static FunctionKeyTotalHours Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyTotalHoursException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyTotalHours retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyTotalHours retval, out string errorResponse) =>
            TryParseSingleWord(value, "totalhours", () => new FunctionKeyTotalHours(), () => SyntaxHelp, out retval, out errorResponse);
        public override string ToString() => "TotalHours()";
        public class InvalidFunctionKeyTotalHoursException : ApplicationException {
            public InvalidFunctionKeyTotalHoursException(string message) : base(message) { }
            public InvalidFunctionKeyTotalHoursException(string message, Exception inner) : base(message, inner) { }
        }
    }
}