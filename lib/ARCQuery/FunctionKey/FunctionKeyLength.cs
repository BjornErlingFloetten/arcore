﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {
    [Class(Description =
        "Returns length of (string) value of field, like 4 for 'John'.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyLength : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'Length()'\r\n" +
            "Examples:\r\n" +
            "'Customer/WHERE FirstName.Length() < 2'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyLength));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<string>(p, tryParser: null, transformer: str => new PValue<long>(str.Length), out retval, out errorResponse);

        public new static FunctionKeyLength Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyLengthException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyLength retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyLength retval, out string errorResponse) =>
            TryParseSingleWord(value, "length", () => new FunctionKeyLength(), () => SyntaxHelp, out retval, out errorResponse);
        public override string ToString() => "Length()";
        public class InvalidFunctionKeyLengthException : ApplicationException {
            public InvalidFunctionKeyLengthException(string message) : base(message) { }
            public InvalidFunctionKeyLengthException(string message, Exception inner) : base(message, inner) { }
        }
    }
}