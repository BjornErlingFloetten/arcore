﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
namespace ARCQuery {

    [Class(Description =
        "Extracts Year-component from field.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyYear : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'Year()'\r\n" +
            "Examples:\r\n" +
            "'Order/WHERE Created.Year() < 2020',\r\n" +
            "'Customer/SELECT Name, Created.Year()',\r\n" +
            "'Order/PIVOT Created.Year() BY Created.Month()'."
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyYear));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<DateTime>(p, tryParser: null, transformer: dateTime => new PValue<int>(dateTime.Year), out retval, out errorResponse);

        public new static FunctionKeyYear Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyYearException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyYear retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyYear retval, out string errorResponse) =>
            TryParseSingleWord(value, "year", () => new FunctionKeyYear(), () => SyntaxHelp, out retval, out errorResponse);
        public override string ToString() => "Year()";
        public class InvalidFunctionKeyYearException : ApplicationException {
            public InvalidFunctionKeyYearException(string message) : base(message) { }
            public InvalidFunctionKeyYearException(string message, Exception inner) : base(message, inner) { }
        }
    }
}