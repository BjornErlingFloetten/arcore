﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery
{

    [Class(Description =
        "Returns the -" + nameof(ARCCore.Extensions.ToStringVeryShort) + "- representation of the given type.\r\n" +
        "\r\n" +
        "Usually used in conjuction with -" + nameof(FunctionKeyType) + "-.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-.\r\n" +
        "\r\n" +
        "Note: Do not confuse -" + nameof(FunctionKeyShort) + "- (date / time formatting) with " +
        "-" + nameof(FunctionKeyVeryShort) +"- (type formatting).\r\n" +
        "\r\n" +
        "See also -" + nameof(FunctionKeyVeryShort) + "-.\r\n"
    )]
    public class FunctionKeyVeryShort : FunctionKey
    {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'VeryShort()'\r\n" +
            "Example (somewhat contrived):\r\n" +
            "'Order/SELECT Customer.Type().VeryShort()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyType));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse)
        {
            if (!(p is PValue<Type> t))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Not of type Type";
                return false;
            }

            retval = new PValue<string>(t.Value.ToStringVeryShort());
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public new static FunctionKeyVeryShort Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyVeryShortException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyVeryShort retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyVeryShort retval, out string errorResponse) =>
            TryParseSingleWord(value, "veryshort", () => new FunctionKeyVeryShort(), () => SyntaxHelp, out retval, out errorResponse);

        public override string ToString() => "VeryShort()";
        public class InvalidFunctionKeyVeryShortException : ApplicationException
        {
            public InvalidFunctionKeyVeryShortException(string message) : base(message) { }
            public InvalidFunctionKeyVeryShortException(string message, Exception inner) : base(message, inner) { }
        }
    }
}