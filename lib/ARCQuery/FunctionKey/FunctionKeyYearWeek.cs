﻿// Copyright (c) 2016-2021 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
namespace ARCQuery {

    [Class(Description =
        "Extract year and week number from field according to ISO 8601 like '2021-51'\r\n" +
        "\r\n" +
        "Uses the System.Globalization.ISOWeek.GetWeekOfYear() and GetYear() methods in .Net.\r\n" +
        "(Note: The date '2021-01-01' will for instance show as '2020-53'.)\r\n" +
        "\r\n" +
        "See also -" + nameof(FunctionKeyYearMonth) + "-.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyYearWeek : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'YearWeek()'\r\n" +
            "Examples:\r\n" +
            "'Order/WHERE ShippedDate.YearWeek = '2020-51'',\r\n" +
            "'Order/PIVOT Employee.Name BY OrderDate.YearWeek()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyYearWeek));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) =>
            TryGetP<DateTime>(p, tryParser: null, transformer: dateTime => new PValue<string>(System.Globalization.ISOWeek.GetYear(dateTime) + "-" + System.Globalization.ISOWeek.GetWeekOfYear(dateTime).ToString("00")), out retval, out errorResponse);

        public new static FunctionKeyYearWeek Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyYearWeekException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyYearWeek retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyYearWeek retval, out string errorResponse) =>
            TryParseSingleWord(value, "yearweek", () => new FunctionKeyYearWeek(), () => SyntaxHelp, out retval, out errorResponse);
        public override string ToString() => "YearWeek()";
        public class InvalidFunctionKeyYearWeekException : ApplicationException {
            public InvalidFunctionKeyYearWeekException(string message) : base(message) { }
            public InvalidFunctionKeyYearWeekException(string message, Exception inner) : base(message, inner) { }
        }
    }
}