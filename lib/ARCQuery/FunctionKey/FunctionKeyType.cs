﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {

    [Class(Description =
        "Returns the type of a property.\r\n" +
        "\r\n" +
        "Most useful for debugging AgoRapide itself, for instance to see what kind of object types are used to store values.\r\n" +
        "\r\n" +
        "See -" + nameof(SyntaxHelp) + "-."
    )]
    public class FunctionKeyType : FunctionKey {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'Type()'\r\n" +
            "Examples:\r\n" +
            "'Order/SELECT *.Type()'\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(FunctionKeyType));

        public override bool TryGetP(IP p, out IP retval, out string errorResponse) {
            retval = new PValue<Type>(p.GetType());
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public new static FunctionKeyType Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidFunctionKeyTypeException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out FunctionKeyType retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out FunctionKeyType retval, out string errorResponse) =>
            TryParseSingleWord(value, "type", () => new FunctionKeyType(), () => SyntaxHelp, out retval, out errorResponse);

        public override string ToString() => "Type()";
        public class InvalidFunctionKeyTypeException : ApplicationException {
            public InvalidFunctionKeyTypeException(string message) : base(message) { }
            public InvalidFunctionKeyTypeException(string message, Exception inner) : base(message, inner) { }
        }
    }
}