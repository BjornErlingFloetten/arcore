﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery {
    [Class(
        Description =
            "-" + nameof(ValueComparerDateTime) + "- is an attempt at offering a human language interface for date and time queries.\r\n" +
            "\r\n" +
            "This class understands such terms as Today, BeforeYesterday, LastTwoDays, Last6Months, ThisYear, LastYear and so on.\r\n" +
            "\r\n" +
            "Enables queries like 'Order/WHERE Created = ThisMonth', that is, a query where you do not have to change the " +
            "condition of the query expression as time passes.\r\n" +
            "\r\n" +
            "Understood by -" + nameof(QueryExpressionWhere) + "-.\r\n" +
            "\r\n" +
            "Terms like Ago and In means that specific period. For instance on Thursday " +
            "TwoDaysAgo means Tuesday (only) while InTwoDays means Saturday (only).\r\n" +
            "\r\n" +
            "Terms like Last and Next means an interval (until today or from today). For instance on Thursday " +
            "LastTwoDays means Tuesday and Wednesday while NextTwoDays means Friday and Saturday.\r\n" +
            "(see -" + nameof(IsInterval) + "-.)\r\n" +
            "\r\n" +
            "See also -" + nameof(IsAfter) + "-, -" + nameof(IsBefore) + "-, -" + nameof(IsGliding) + "-.\r\n" +
            "\r\n" +
            "NOTE: As soon as an instance of -" + nameof(ValueComparerDateTime) + "- is created in C#,\r\n" +
            "NOTE: it has a limited usefulness as time passes because the\r\n" +
            "NOTE: time of initialization decides the time range (-" + nameof(StartPeriod) + "- / -" + nameof(EndPeriod) + "-) that it compares against.\r\n" +
            "NOTE: For instance, at time 09:59 NextHour would mean 10:00 to 11:00, but the same object used later\r\n" +
            "NOTE: at 10:01 for instance would still compare against 10:00 to to 11:00.\r\n" +
            "NOTE: In other words, these object instances are time-limited and for one-time-use only.\r\n"
    )]
    public class ValueComparerDateTime : ValueComparer {

        [ClassMember(Description = "See -" + nameof(ResolutionEnum) + "-.")]
        public ResolutionEnum Resolution { get; private set; }

        [ClassMember(Description =
            "Value 0 would mean 'This', like ThisHour, Today, ThisMonth and so on.\r\n" +
            "Value -1 would mean 'Last', like LastHour, YesterDay, LastMonth and so on.\r\n" +
            "Value -2 would mean 'LastX' like LastTwoHours (Last2Hours) and so on..." +
            "Value 1 would mean 'Next', like NextHour, 'Tomorrow', NextMonth and so on.\r\n"
        )]
        public int Steps { get; private set; }

        [ClassMember(Description =
            "The modifier 'Gliding' (or just 'G') can be appended in order to mean continous gliding periode, so for instance\r\n" +
            "at time 09:15 " +
            "NextTwoHoursGliding (or NextTwoHoursG) would mean between 09:15 and 11:15, instead of between 10:00 and 12:00.\r\n" +
            "Likewise at date 2020-12-09 time 09:15 " +
            "LastTwoYearsGliding (or LastTwoYearsG) would mean betweem 2018-12-09 09:15 and 2020-12-09 09:15 instead of the years of 2018 and 2019.\r\n" +
            "\r\n" +
            "The modifier 'Gliding' can also be used with 'Ago' and 'In',\r\n" +
            "like at Thursday 09:15 " +
            "TwoDaysAgoGliding (or TwoDaysAgoG) would mean between Monday 09:15 and Tuesday 09:15 instead of Tuesday.\r\n" +
            "Likewise, InTwoDaysGliding (or InTwoDaysG) would mean betweeen Saturday 09:15 and Sunday 09:15 instead of Saturday.\r\n" +
            "NOTE: The use of the modifier 'Gliding' with 'Ago' and 'In' may be non-intuitive. "
        )]
        public bool IsGliding { get; private set; }

        [ClassMember(Description =
            "The modifier 'Before' can be prepended in order to mean from any time before the specified time.\r\n" +
            "Note: 'Until' is synonymous with 'Before' but not recommended, as it can be ambigious.\r\n" +
            "Example: BeforeLastHour would at time 09:15 mean anything before 08:00\r\n" +
            "BeforeLastHourGliding would at time 09:15 mean anything before 08:15.\r\n" +
            "\r\n" +
            "Note that setting -" + nameof(IsBefore) + "- is equivalent to setting -" + nameof(StartPeriod) + "- to -" + nameof(DateTime.MinValue) + "-."
        )]
        public bool IsBefore { get; private set; }

        [ClassMember(Description =
            "The modifier 'After' can be prepended in order to mean any time after the specified time.\r\n" +
            "Note: 'From' and 'Since' is synonynous with 'After' but not recommended, as it can be ambigious.\r\n" +
            "Example: AfterLastHour would at time 09:15 mean anything after 08:00\r\n" +
            "AfterLastHourGliding would at time 09:15 mean anything after 08:15.\r\n" +
            "\r\n" +
            "Note that setting -" + nameof(IsAfter) + "- is equivalent to setting -" + nameof(EndPeriod) + "- to -" + nameof(DateTime.MaxValue) + "-."
        )]
        public bool IsAfter { get; private set; }

        [ClassMember(Description =
            "Normally the interval which is compared against is the timespan corresponding to -" + nameof(Resolution) + "-.\r\n" +
            "(unless of course -" + nameof(IsBefore) + "- or -" + nameof(IsAfter) + "- is used.)\r\n" +
            "\r\n" +
            "If terms like 'Last3Days' or 'Next2Months' are used then the interval to compare against " +
            "would be centered around the -" + nameof(InitValue) + "- (the 'now' value).\r\n" +
            "\r\n" +
            "Example: On Thursday 3DaysAgo would mean only Monday, while Last3Days would mean 'Monday to Wednesday'.\r\n" +
            "Likewise in April 3MonthsAgo would mean only January, while Last3Months would mean 'January to March'.\r\n"
        )]
        public bool IsInterval { get; private set; }

        [ClassMember(Description = "For debugging purposes. The initial 'now' value used before calculating -" + nameof(StartPeriod) + "- and -" + nameof(EndPeriod) + "-.")]
        public DateTime InitValue { get; private set; }

        [ClassMember(Description =
            "The start of the interval which this instance compares against.\r\n" +
            "The comparision should be 'greater than or equal to' -" + nameof(StartPeriod) + " and 'less than' -" + nameof(EndPeriod) + ".\r\n" +
            "\r\n" +
            "Note that setting -" + nameof(IsBefore) + "- is equivalent to setting -" + nameof(StartPeriod) + "- to -" + nameof(DateTime.MinValue) + "-."
        )]
        public DateTime StartPeriod { get; private set; }

        [ClassMember(Description =
            "The end of the interval which this instance compares against.\r\n" +
            "The comparision should be 'greater than or equal to' -" + nameof(StartPeriod) + " and 'less than' -" + nameof(EndPeriod) + ".\r\n" +
            "\r\n" +
            "Note that setting -" + nameof(IsAfter) + "- is equivalent to setting -" + nameof(EndPeriod) + "- to -" + nameof(DateTime.MaxValue) + "-."
        )]
        public DateTime EndPeriod { get; private set; }

        public override bool TryGetPrevious(out ValueComparer retval) {
            retval = new ValueComparerDateTime(Resolution,
                Steps == 1 && IsInterval ?
                    -1 :  // Go straight from NextDay to LastDay, in order to keep IsInterval
                    Steps - 1,
                IsGliding, IsBefore, IsAfter, IsInterval);
            return true;
        }

        public override bool TryGetNext(out ValueComparer retval) {
            retval = new ValueComparerDateTime(Resolution,
                Steps == -1 && IsInterval ?
                    1 :  // Go straight from LastDay to NextDay, in order to keep IsInterval
                    Steps + 1,
                IsGliding, IsBefore, IsAfter, IsInterval);
            return true;
        }

        public ValueComparerDateTime(ResolutionEnum resolution, int steps, bool isGliding, bool isBefore, bool isAfter, bool isInterval) {
            Resolution = resolution;
            Steps = steps;
            IsGliding = isGliding;
            IsBefore = isBefore;
            IsAfter = isAfter;
            IsInterval = isInterval;

            InitValue = new Func<DateTime>(() => {
                /// TODO: How do we work with UTC or not UTC here? This is not natural for the end user.
                /// TODO: Also, we should also take into account <see cref="StreamProcessorP.Timestamp"/> in
                /// TOOD order to execute queries from any point in the <see cref="ARCCore.ARConcepts.PropertyStream"/>
                var now = UtilCore.DateTimeNow;

                if (isGliding) return now;
                return (Resolution) switch
                {
                    ResolutionEnum.Second => new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second),
                    ResolutionEnum.Minute => new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0),
                    ResolutionEnum.Hour => new DateTime(now.Year, now.Month, now.Day, now.Hour, 0, 0),
                    ResolutionEnum.Day => new DateTime(now.Year, now.Month, now.Day, 0, 0, 0),
                    ResolutionEnum.Week => new DateTime(now.Year, now.Month, now.Day, 0, 0, 0).Subtract(TimeSpan.FromDays((int)now.DayOfWeek)), // Note: Weeks start on Sunday
                    ResolutionEnum.Month => new DateTime(now.Year, now.Month, 1, 0, 0, 0),
                    ResolutionEnum.Quarter => new DateTime(now.Year, (((now.Month - 1) / 3) + 1) * 3, 1, 0, 0, 0),
                    ResolutionEnum.Year => new DateTime(now.Year, 1, 1, 0, 0, 0),
                    _ => throw new InvalidEnumException(Resolution)
                };
            })();

            var Adder = new Func<int, DateTime>(s => (Resolution) switch { // TODO: Remove parameter s here, not needed (use steps instead)
                ResolutionEnum.Second => InitValue.AddSeconds(s),
                ResolutionEnum.Minute => InitValue.AddMinutes(s),
                ResolutionEnum.Hour => InitValue.AddHours(s),
                ResolutionEnum.Day => InitValue.AddDays(s),
                ResolutionEnum.Week => InitValue.AddDays(s * 7),
                ResolutionEnum.Month => InitValue.AddMonths(s),
                ResolutionEnum.Quarter => InitValue.AddMonths(s * 3),
                ResolutionEnum.Year => InitValue.AddYears(s),
                _ => throw new InvalidEnumException(Resolution)
            });

            // var isGlidingNumber = isGliding ? 0 : 1;

            // Med intervall får vi slik som
            // (Steps > 0 ? Adder(0 + (IsGliding ? 0 : 1)) :
            // for start / stopp periode.

            if (IsInterval) {
                // IsBefore / IsAfter is not relevant now
                StartPeriod = Steps < 0 ? Adder(Steps) : Adder(0 + (IsGliding ? 0 : 1));
                EndPeriod = Steps > 0 ? Adder(Steps + (IsGliding ? 0 : 1)) : InitValue;
            } else {
                StartPeriod =
                    IsBefore ? DateTime.MinValue :
                    Adder(Steps + (IsGliding ? (Steps < 0 ? -1 : 0) : (IsAfter ? 1 : 0))); /// NOTE: Bug-fix done 3 Feb 2021, changed to "IsGliding ? (Steps < 0 ? -1 : 0)" from "IsGliding ? 0" and improved documentation for <see cref="IsGliding"/>

                EndPeriod = isAfter ? DateTime.MaxValue :
                    Adder(Steps + (IsGliding ? (Steps > 0 ? 1 : 0) : (IsBefore ? 0 : 1))); /// NOTE: Bug-fix done 3 Feb 2021, changed to "IsGliding ? (Steps > 0 ? 1 : 0)" from "IsGliding ? 0" and improved documentation for <see cref="IsGliding"/>
            }
        }


        private static HashSet<RelationalOperator> _supportedOperators = new HashSet<RelationalOperator> { RelationalOperator.EQ, RelationalOperator.NEQ, RelationalOperator.GT, RelationalOperator.LT, RelationalOperator.GEQ, RelationalOperator.LEQ };
        public override ICollection<RelationalOperator> SupportedOperators => _supportedOperators;

        public override bool IsMatch(QueryExpressionWhere queryExpressionWhere, IP property) => queryExpressionWhere.Operator switch
        {
            RelationalOperator.EQ => property.TryGetV<DateTime>(out var dtm) ?
                (dtm >= StartPeriod && dtm < EndPeriod) :
                property.TryGetV<IEnumerable<DateTime>>(out var lst) && lst.Any(dtm => dtm >= StartPeriod && dtm < EndPeriod),
            RelationalOperator.NEQ => property.TryGetV<DateTime>(out var dtm) ?
                !(dtm >= StartPeriod && dtm < EndPeriod) :
                property.TryGetV<IEnumerable<DateTime>>(out var lst) && !lst.Any(dtm => dtm >= StartPeriod && dtm < EndPeriod),
            RelationalOperator.GT => property.TryGetV<DateTime>(out var dtm) ?
                (dtm >= EndPeriod) : // Note >= instead of > because EndPeriod itself is not "included"
                property.TryGetV<IEnumerable<DateTime>>(out var lst) && lst.Any(dtm => dtm >= EndPeriod),
            RelationalOperator.LT => property.TryGetV<DateTime>(out var dtm) ?
                (dtm < StartPeriod) :
                property.TryGetV<IEnumerable<DateTime>>(out var lst) && lst.Any(dtm => dtm < StartPeriod),
            RelationalOperator.GEQ => property.TryGetV<DateTime>(out var dtm) ?
                (dtm >= StartPeriod) : // Note compare against StartPeriod, not EndPeriod
                property.TryGetV<IEnumerable<DateTime>>(out var lst) && lst.Any(dtm => dtm >= StartPeriod),
            RelationalOperator.LEQ => property.TryGetV<DateTime>(out var dtm) ?
                (dtm < EndPeriod) : // // Note compare against EndPeriod, not StartPeriod. Note < instead of <= because EndPeriod itself is not "included"
                property.TryGetV<IEnumerable<DateTime>>(out var lst) && lst.Any(dtm => dtm < EndPeriod),
            _ => throw new InvalidEnumException(queryExpressionWhere.Operator, nameof(queryExpressionWhere.Operator) + ": " + queryExpressionWhere.Operator + " for " + this.GetType())
        };



        public new static ValueComparerDateTime Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidValueComparerDateTimeException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out ValueComparerDateTime retval) => TryParse(value, out retval, out _);
        [ClassMember(Description =
            "Note: This parser is probably exhaustive but not necessarily logical\r\n" +
            "Note: (it will accept as valid input insensical expression)\r\n" +
            "Note: Parser is tailored to the English language"
        )]
        public static bool TryParse(string value, out ValueComparerDateTime retval, out string errorResponse) {

            var originalValue = value;

            value = value.ToLower();

            var temp = new Func<ValueComparerDateTime?>(() => {
                return (value) switch
                {
                    // TODO: A better terminology would be Order.ShippingDate > Now()
                    // TOOD: instead of Order.ShippingDate = AfterNow
                    "beforenow" => new ValueComparerDateTime(ResolutionEnum.Day, steps: 0, isGliding: true, isBefore: true, isAfter: false, isInterval: false),
                    "untilnow" => new ValueComparerDateTime(ResolutionEnum.Day, steps: 0, isGliding: true, isBefore: true, isAfter: false, isInterval: false),
                    "afternow" => new ValueComparerDateTime(ResolutionEnum.Day, steps: 0, isGliding: true, isBefore: false, isAfter: true, isInterval: false),
                    "fromnow" => new ValueComparerDateTime(ResolutionEnum.Day, steps: 0, isGliding: true, isBefore: false, isAfter: true, isInterval: false),
                    _ => null
                };
            })();
            if (temp != null) {
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                retval = temp;
                return true;
            }

            var isAfter = false;
            if (value.StartsWith("after")) {
                isAfter = true;
                value = value.Substring("after".Length);
            } else if (value.StartsWith("from")) { /// Not recommended to use, see <see cref="IsAfter"/>
                isAfter = true;
                value = value.Substring("from".Length);
            } else if (value.StartsWith("since")) { /// Not recommended to use, see <see cref="IsAfter"/>
                isAfter = true;
                value = value.Substring("since".Length);
            }

            var isBefore = false;
            if (value.StartsWith("before")) {
                isBefore = true;
                value = value.Substring("before".Length);
            } else if (value.StartsWith("until")) {
                isBefore = true;
                value = value.Substring("until".Length);
            }

            var isGliding = false;
            if (value.EndsWith("g")) {
                isGliding = true;
                value = value.Substring(0, value.Length - "g".Length);
            }
            if (value.EndsWith("gliding")) {
                isGliding = true;
                value = value.Substring(0, value.Length - "gliding".Length);
            }

            int direction = 0;
            var isInterval = false;
            if (value.StartsWith("last")) { // Last / Next would mean intervals if more than 1 step.
                direction = -1;
                if (!(isBefore || isAfter)) isInterval = true;
                value = value.Substring("last".Length);
            } else if (value.EndsWith("ago")) {
                direction = -1;
                value = value.Substring(0, value.Length - "ago".Length);
            }
            if (value.StartsWith("next")) {
                direction = 1;
                if (!(isBefore || isAfter)) isInterval = true;
                value = value.Substring("next".Length);
            } else if (value.StartsWith("in")) {
                direction = 1;
                value = value.Substring("in".Length);
            } else if (value.EndsWith("hence")) {
                direction = 1;
                value = value.Substring(0, value.Length - "hence".Length);
            }

            if (value.EndsWith("s")) {
                // Remove like s in Hours, Days, Years, Months and so on.
                value = value.Substring(0, value.Length - "s".Length);
            }

            var numbers = new List<string> { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten" };
            for (var i = 0; i < numbers.Count; i++) { // Replace words with numbers (TwoDay becomes 2Day)
                if (value.StartsWith(numbers[i])) {
                    value = i + value.Substring(numbers[i].Length);
                }
            }

            temp = new Func<ValueComparerDateTime?>(() => {
                return (value) switch
                {
                    "yesterday" => new ValueComparerDateTime(ResolutionEnum.Day, steps: -1, isGliding, isBefore, isAfter, isInterval: false),
                    "today" => new ValueComparerDateTime(ResolutionEnum.Day, steps: 0, isGliding, isBefore, isAfter, isInterval: false),
                    "tomorrow" => new ValueComparerDateTime(ResolutionEnum.Day, steps: 1, isGliding, isBefore, isAfter, isInterval: false),
                    _ => null
                };
            })();
            if (temp != null) {
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                retval = temp;
                return true;
            }

            var resolution = ResolutionEnum.__invalid;
            foreach (var c in UtilCore.EnumGetMembers<ResolutionEnum>()) {
                if (value.EndsWith(c.ToString().ToLower())) {
                    value = value.Substring(0, value.Length - c.ToString().Length);
                    resolution = c;
                    break;
                }
            }
            if (resolution == ResolutionEnum.__invalid) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse =
                    "Resolution not recognized in '" + value + "'.\r\n" +
                    "Try one of " + string.Join(", ", UtilCore.EnumGetMembers<ResolutionEnum>().Select(e => e.ToString())) + "\r\n" +
                    "(" + nameof(direction) + ": " + direction + ", " + nameof(isGliding) + ": " + isGliding + ", " + nameof(IsBefore) + ": " + isBefore + ", " + nameof(isAfter) + ": " + isAfter + ")";
                return false;
            }
            var steps = 0;
            if ("this".Equals(value)) {
                steps = 0;
            } else if ("".Equals(value)) {
                steps = 1;
            } else if (!int.TryParse(value, out steps)) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse =
                    "Steps not recognized as '" + value + "'.\r\n" +
                    "Not a valid integer.\r\n" +
                    "(" + nameof(resolution) + ": " + resolution + ", " + nameof(direction) + ": " + direction + ", " + nameof(isGliding) + ": " + isGliding + ", " + nameof(IsBefore) + ": " + isBefore + ", " + nameof(isAfter) + ": " + isAfter + ")";
                return false;
            }
            if (steps > 0 && direction == 0) {
                // HACK: Necessary for phrases like 'Before3Days' to not end up as 'BeforeToday'
                direction = 1;
            }

            retval = new ValueComparerDateTime(resolution, steps * direction, isGliding, isBefore, isAfter, isInterval);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;

        }

        private string? _toString = null;
        public override string ToString() => _toString ?? (_toString = new Func<string>(() =>
        (
            (IsAfter ? "After" : "") +
            (IsBefore ? "Before" : "") +
            (Steps < 0 && IsInterval ? "Last" : "") +
            (Steps > 0 && IsInterval ? "Next" : "") +
            (Steps > 0 && !IsInterval ? "In" : "") +
            (Steps == 0 ? "This" : (Math.Abs(Steps) == 1 && IsInterval ? "" : (Math.Abs(Steps)) switch
            {
                1 => "One",
                2 => "Two",
                3 => "Three",
                4 => "Four",
                5 => "Five",
                _ => Math.Abs(Steps).ToString()
            })) +
            Resolution +
            (Math.Abs(Steps) > 1 ? "s" : "") +
            (Steps < 0 && !IsInterval ? "Ago" : "") +
            // (Steps > 0 && !IsInterval ? "Hence" : "") +
            (IsGliding ? "G" : "")
        ).Replace(
            /// Note commented out replaces. They are not executed, 
            /// in order not to change IsInterval and thereby mess up <see cref="TryGetNext"/> / <see cref="TryGetPrevious"/>
            "ThisDay", "Today").Replace(
            /// "LastDay", "Yesterday").Replace( 
            /// "NextDay", "Tomorrow").Replace( 
            "DayAgo", "Yesterday").Replace(
            //"WeekAgo", "LastWeek").Replace(
            //"MonthAgo", "LastMonth").Replace(
            //"QuarterAgo", "LastQuarter").Replace(
            //"YearAgo", "LastYear").Replace(
            "DayHence", "Tomorrow").Replace(
            //"WeekHence", "NextWeek").Replace(
            //"MonthHence", "NextMonth").Replace(
            //"QuarterHence", "NextQuarter").Replace(
            //"YearHence", "NextYear").Replace(
            "BeforeTodayG", "BeforeNow").Replace(
            "AfterIn", "After").Replace(
            "BeforeIn", "BeforeNext").Replace(
            "AfterTodayG", "AfterNow").Replace(
            "BeforeNextOneDay", "BeforeTomorrow").Replace(
            "BeforeNextOneDay", "BeforeTomorrow").Replace(
            "AfterOneDay", "AfterTomorrow").Replace(
            "OneYesterday", "Yesterday")
        )());

        private IEnumerable<(string Key, string Value)>? _toDebugProperties;
        [ClassMember(Description = 
            "Exposes all members of this class.\r\n" +
            "\r\n" +
            "TODO: Is it relevant to let this class implement -" + nameof(IP) + "- instead?"
        )]
        public IEnumerable<(string Key, string Value)> ToDebugProperties => _toDebugProperties ?? (_toDebugProperties = new List<(string Key, string Value)> {
            (nameof(Resolution), Resolution.ToString()) ,
            (nameof(Steps), Steps.ToString()) ,
            (nameof(IsGliding), IsGliding.ToString()) ,
            (nameof(IsBefore), IsBefore.ToString()) ,
            (nameof(IsAfter), IsAfter.ToString()) ,
            (nameof(IsInterval), IsInterval.ToString()) ,
            (nameof(InitValue), InitValue.ToStringDateAndTime()) ,
            (nameof(StartPeriod), StartPeriod.ToStringDateAndTime()) ,
            (nameof(EndPeriod), EndPeriod.ToStringDateAndTime()) ,
        });

        public class InvalidValueComparerDateTimeException : ApplicationException {
            public InvalidValueComparerDateTimeException(string message) : base(message) { }
            public InvalidValueComparerDateTimeException(string message, Exception inner) : base(message, inner) { }
        }

        [Enum(AREnumType = AREnumType.OrdinaryEnum)]
        public enum ResolutionEnum {
            __invalid,
            Second,
            Minute,
            Hour,
            Day,
            Week,
            Month,
            Quarter,
            Year
        }
    }
}
