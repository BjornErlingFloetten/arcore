﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {
    [Class(
        Description =
            "Can evaluation expressions like 'WHERE Created = ThisYear.\r\n" +
            "\r\n" +
            "Simplifies query expression regarding to the actual value comparision.\r\n" +
            "An example is 'Order/WHERE Created = ThisYear' as offered by -" + nameof(ValueComparerDateTime) + "-.\r\n" + 
            "\r\n" +
            "Implementing classes:\r\n" +
            "-" + nameof(ValueComparerDateTime) + "-.\r\n" +
            "\r\n" +
            "TODO: Make extensible somehow.\r\n" +
            "TODO: Now all implementing classes have to be explicitly recognized by -" + nameof(TryParse) + "-\r\n" +
            "TODO: meaning that it is not possible to implement classes outside of this library (outside of -" + nameof(ARComponents.ARCQuery) + "-).\r\n" +
            "TODO:\r\n" +
            "TODO: See for instance how -" + nameof(QueryExpression) + "- and -" + nameof(FunctionKey) + "- has been made extensible.\r\n" +
            "TODO: (note that an exact same approach can not be used for -" + nameof(ValueComparer) + "-\r\n" +
            "TODO: because there is no single word in the start identifying which parser to use.\r\n"
    )]
    public abstract class ValueComparer : ITypeDescriber, IEquatable<ValueComparer> {

        public abstract ICollection<RelationalOperator> SupportedOperators { get; }

        public abstract bool IsMatch(QueryExpressionWhere queryExpressionWhere, IP property);

        public ValueComparer GetPrevious => TryGetPrevious(out var retval) ? retval : throw new ValueComparerException("No suggestion for previous query expression found");
        [ClassMember(Description = "Suggestion for previous query (browsing 'backwards')")]
        public virtual bool TryGetPrevious(out ValueComparer retval) {
            retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return false;
        }

        public ValueComparer GetNext => TryGetNext(out var retval) ? retval : throw new ValueComparerException("No suggestion for next query expression found");
        [ClassMember(Description = "Suggestion for next query (browsing 'forwards')")]
        public virtual bool TryGetNext(out ValueComparer retval) {
            retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return false;
        }

        public static ValueComparer Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidValueComparerException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out ValueComparer retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out ValueComparer retval, out string errorResponse) {

            if (ValueComparerDateTime.TryParse(value, out var retvalTemp, out errorResponse)) {
                retval = retvalTemp;
                return true;
            }
            // TODO: See TODO at top of this file about making extensible somehow

            retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return false;
        }

        public override int GetHashCode() => ToString().GetHashCode();
        public bool Equals(ValueComparer other) => ToString().Equals(other.ToString());
        public override bool Equals(object other) => other is ValueComparer v && Equals(v);

        // public override string ToString() => "[NOT IMPLEMENTED]";

        /// <summary>
        /// 'Implementing' <see cref="ITypeDescriber"/>
        /// </summary>
        /// <param name="key"></param>
        public static void EnrichKey(PK key) =>
            key.SetValidatorAndParser(new Func<string, ParseResult>(value =>
                TryParse(value, out var retval, out var errorResponse) ?
                    ParseResult.CreateOK(retval) :
                    ParseResult.CreateError(errorResponse)
            ));

        public class ValueComparerException : ApplicationException {
            public ValueComparerException(string message) : base(message) { }
            public ValueComparerException(string message, Exception inner) : base(message, inner) { }
        }

        public class InvalidValueComparerException : ApplicationException {
            public InvalidValueComparerException(string message) : base(message) { }
            public InvalidValueComparerException(string message, Exception inner) : base(message, inner) { }
        }
    }
}
