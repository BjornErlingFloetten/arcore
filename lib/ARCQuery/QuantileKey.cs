﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using ARCCore;
using System.Linq;

namespace ARCQuery {

    [Class(Description =
        "Can separate entities into quantiles, like Quartiles, Quintiles or Sextiles.\r\n" +
        "\r\n" +
        "This is an not an expandable concept, that is, this class contains all the relevant functionality and you are not expected to " +
        "implement anything specific in your -" + nameof(ARConcepts.ApplicationSpecificCode) + "-.\r\n" +
        "\r\n" +
        "This is a one-time-use only object. It should not be reused between different API request for instance, because " +
        "the first call to -" + nameof(TryGetP) + "- caches the result (within itself) for ALL entities of given type within the data storage.\r\n" +
        "\r\n" +
        "This also means that it is ESSENTIAL to use the concept of cached compound key in the C# code using this class. " +
        "(the ref-parameter cachedCompoundKey to -" + nameof(ARCQuery.Extensions.TryGetP) + "-.)\r\n" +
        "(in order to avoid all entities being evaluated every time the quantile for one entity is requested.)\r\n" +
        "\r\n" +
        "Note that quantiles are calculated for alle entities, regardless of the actual selection."
    )]
    public class QuantileKey {

        public Quantile Quantile { get; private set; }

        [ClassMember(Description =
            "Cache result for all entities of same type in data storage.\r\n" +
            "The cache is based on actual value of the property (instead of stored a quantile for each entity).\r\n" +
            "(originally this became the design because we do not have the entity-keys available. " +
            "(they are available when cache is constructed, but not when -" + nameof(TryGetP) + "- is called))\r\n" +
            "\r\n" +
            "NOTE: We are limited to providing quantiles for only the whole collection of entities.\r\n" +
            "Note that in order to save memory we only store the Quantile itself, not a PValue<Quantile> object.\r\n" +
            "(most probably the value will only be consumed once, meaning no additional processing will be required anyway)."

        )]
        private readonly ConcurrentDictionary<string, int> _cache = new ConcurrentDictionary<string, int>();

        [ClassMember(Description =
            "Returns an integer with the number of the quantile group for the value returned by the given -" + nameof(CompoundKey) + "-).\r\n" +
            "For instance if quantile -" + nameof(Quantile.QUARTILE) + "- is requested, will return a value between 1 and 4.\r\n" +
            "\r\n" +
            "TODO: This code is mathematically unsound regarding homogenous or small datasets."
        //"Note that as of Jul 2020 duplicate values can be assigned to different quantile groups.\r\n" +
        //"For instance, for the set { 5, 5, 5, 5 } and -" + nameof(Quantile.QUARTILE) + "- " +
        //"the first element will be assigned to 1, the next to 2 and so on.\r\n" +
        //"\r\n"
        //"Classification is based on actual value of the given quantile key.\r\n" +
        //"This means (for duplicate values) that " +
        //"the resulting groups are not necessarily of the same size.\r\n" +
        //"For instance, for the set { 5, 5, 5, 5, 5, 10, 10, 10, 10, 10 } the first five elements would be put into " +
        //"the first quantile group, and the next (last) five elements put into the second quantile group, regardless of quantile requested.\r\n" +
        //"(only value 1 or 2 would be returned).\r\n" +
        // "\r\n"
        )]
        public bool TryGetP(IKIP ikip, IP dataStorage, CompoundKey compoundKey, out IP retval, out string errorResponse) {
            if (!compoundKey.TryGetP(ikip, dataStorage, out var propertyToBeQuantilized, out errorResponse)) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }

            if (!propertyToBeQuantilized.TryGetV<string>(out var s, out errorResponse)) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }

            if (_cache.TryGetValue(s, out var quantileGroup)) {
                retval = new PValue<int>(quantileGroup);
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }

            // We did not find value inside cache. This is because cache has not been initialized.
            lock (_cache) {
                if (_cache.TryGetValue(s, out quantileGroup)) {
                    // Another thead initialized the cache while we waited for the lock
                    retval = new PValue<int>(quantileGroup);
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
                }

                // Calculate quantile for all values
                if (!dataStorage.TryGetP<IP>(IKType.FromType(ikip.P.GetType()), out var allEntitiesOfGivenType, out errorResponse)) {
                    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse =
                        "No collection for entities of type " + ikip.P.GetType() + " found.\r\n" +
                        "\r\n" +
                        "NOTE: As of Feb 2021 " + nameof(QuantileKey) + " is only able to work directly in an entity collection, not on a sub-query.\r\n" +
                        "NOTE: For instance, you can do 'Hub/SELECT HubId, Device.DeviceId.Count().QUARTILE'\r\n" +
                        "NOTE: but not 'Hub/SELECT HubId, Device.DeviceId.Count() AS Count/SELECT HubId, Count, Count.QUARTILE'.\r\n" +
                        "\r\n" +
                        "Details:\r\n" + errorResponse;
                    return false;
                }
                var allValues = allEntitiesOfGivenType.AsParallel().Select(e => {
                    if (!compoundKey.TryGetP(e, dataStorage, out var p, out _)) {
                        return null;
                    }
                    if (!p.TryGetV<string>(out var s)) {
                        return null;
                    }
                    return s;
                }).Where(s => s != null).Select(s => s!).ToList();
                if (allValues.AsParallel().All(s => UtilCore.DoubleTryParse(s, out _))) {
                    // TODO: INEFFICIENT CODE. Store result of parsing instead.
                    // TODO: We can also consider taking DISTINCT result first, before parsing.

                    // Use double comparision (because all values parse as doubles)
                    // Note performance issue here, if value was original integer or double it should be unnecessary to parse it again
                    allValues.Sort(new Comparison<string>((s1, s2) => UtilCore.DoubleParse(s1).CompareTo(UtilCore.DoubleParse(s2))));
                } else {
                    allValues.Sort();
                }

                // TODO: This code is mathematically unsound regarding homogenous small datasets.
                var groupSize = allValues.Count / (int)Quantile;
                for (var i = 0; i < allValues.Count; i++) {
                    var quantile = Math.Min((i / groupSize) + 1, (int)Quantile); // Group size is rounded down, avoid too high quantile
                    _cache[allValues[i]] = quantile;
                }

                if (_cache.TryGetValue(propertyToBeQuantilized.GetV<string>(), out quantileGroup)) {
                    retval = new PValue<int>(quantileGroup);
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
                }
                throw new QuantilKeyException(
                    "Unable to find quantile group for '" + propertyToBeQuantilized.GetV<string>() + "' " +
                    "after having quantilized all entities (" + allEntitiesOfGivenType.Count() + " entities, " + allValues.Count + " different values)\r\n" +
                    "_cache size is now " + _cache.Count);
            }
        }

        public QuantileKey(Quantile quantile) => Quantile = quantile;

        public static QuantileKey Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidQuantileKeyException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse);
        public static bool TryParse(string value, out QuantileKey retval) => TryParse(value, out retval, out _);

        [ClassMember(Description =
            "Note that any value (positive integer greated than 2) is accepted, " +
            "not necessarily only string-representations of enums within -" + nameof(ARCQuery.Quantile) + "-.\r\n" +
            "\r\n" +
            "On the other hand, -" + nameof(CompoundKey.Parse) + "- will discard integers, " +
            "because they are too easily confused with doubles (decimal numbers).\r\n"
        )]
        public static bool TryParse(string value, out QuantileKey retval, out string errorResponse) {
            value = value.Trim();
            if (int.TryParse(value, out var _int) && _int > 1) {
                retval = new QuantileKey((Quantile)_int);
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
            if (UtilCore.EnumTryParse<Quantile>(value.ToUpper(), out var quantile)) {
                retval = new QuantileKey(quantile);
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
            retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>;
            errorResponse = "Nor int nor a Quantile";
            return false;
        }
        public override string ToString() => Quantile.ToString();

        public class InvalidQuantileKeyException : ApplicationException {
            public InvalidQuantileKeyException(string message) : base(message) { }
            public InvalidQuantileKeyException(string message, Exception inner) : base(message, inner) { }
        }

        public class QuantilKeyException : ApplicationException {
            public QuantilKeyException(string message) : base(message) { }
        }
    }

    [Enum(
        Description = "Describes quantiles like -" + nameof(TERTILE) + "- and -" + nameof(QUARTILE) + "-.",
        AREnumType = AREnumType.OrdinaryEnum
    )]
    public enum Quantile {
        __invalid,
        [EnumMember(Description = "Divide into two parts")]
        MEDIAN = 2,
        [EnumMember(Description = "Divide into three parts")]
        TERTILE = 3,
        [EnumMember(Description = "Divide into four parts")]
        QUARTILE = 4,
        [EnumMember(Description = "Divide into five parts")]
        QUINTILE = 5,
        [EnumMember(Description = "Divide into 6 parts")]
        SEXTILE = 6,
        [EnumMember(Description = "Divide into 7 parts")]
        SEPTILE = 7,
        [EnumMember(Description = "Divide into 8 parts")]
        OCTILE = 8,
        [EnumMember(Description = "Divide into 10 parts")]
        DECILE = 10,
        [EnumMember(Description = "Divide into 12 parts")]
        DUODECILE = 12,
        [EnumMember(Description = "Divide into 16 parts")]
        HEXADECILE = 16,
        [EnumMember(Description = "Divide into 20 parts")]
        VENTILE = 20,
        [EnumMember(Description = "Divide into 100 parts")]
        PERCENTILE = 100,
        [EnumMember(Description = "Divide into 1000 parts")]
        PERMILLE = 1000
    }
}