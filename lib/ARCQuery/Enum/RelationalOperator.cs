﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCQuery {
    [Enum(
        Description =
            "Relational / comparision operators.\r\n" +
            "\r\n" +
            "Used by -" + nameof(QueryExpressionWhere) + "- and implementations of -" + nameof(ValueComparer) + "- like -" + nameof(ValueComparerDateTime) + "-.\r\n" +
            "\r\n" +
            "TODO: Support IS (like IS NULL) and IS NOT (like IS NOT NULL)\r\n" +
            "\r\n" +
            "TODO: Support IN and NOT IN (for sets).",
        AREnumType = AREnumType.OrdinaryEnum
    )]
    public enum RelationalOperator {
        __invalid,
        EQ,
        CONTAINS,
        NEQ,
        NOTCONTAINS,
        GT,
        LT,
        GEQ,
        LEQ,
        [EnumMember(Description = "Case sensitive string wildcard comparision.")]
        LIKE,
        [EnumMember(Description = "Case insensitive string wildcard comparision.")]
        ILIKE,
        [EnumMember(Description = "Case sensitive string wildcard comparision.")]
        NOTLIKE,
        [EnumMember(Description = "Case insensitive string wildcard comparision.")]
        NOTILIKE,
    }

    // TODO: Possibly delete commented out code
    // TODO: Suggested extension-class, if ever needed.
    //public static class OperatorExtension {
    //    public static string ToMathSymbol(this RelationalOperator _operator) => _operator switch {
    //        RelationalOperator.EQ => "=",
    //        RelationalOperator.NEQ => "!=",
    //        RelationalOperator.LT => "<",
    //        RelationalOperator.GT => ">",
    //        RelationalOperator.LEQ => "<=",
    //        RelationalOperator.GEQ => ">=",
    //        _ => _operator.ToString() // TODO: Decide if this approach is good enough
    //    };
    //}

}
