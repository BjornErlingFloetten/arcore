﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {

    [Enum(
        Description =
            "Describes level of -" + nameof(Strictness) + "- when executing query. " +
            "\r\n" +
            "Specified through -" + nameof(QueryExpressionStrict) + "- and stored in -" + nameof(QueryProgressP.CurrentStrictness) + "-.",
        AREnumType = AREnumType.OrdinaryEnum
    )]
    public enum Strictness {
        __invalid,
        ON,
        OFF
    }
}
