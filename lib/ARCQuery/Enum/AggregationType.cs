﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {
    [Enum(
        Description =
            "Used by -" + nameof(QueryExpressionPivot) + "- and -" + nameof(QueryExpressionAggregate) + "-.\r\n" +
            "\r\n" +
            "See also -" + nameof(FunctionKeyAggregate) + "- which operates over a collection of properties.\r\n",
            AREnumType = AREnumType.OrdinaryEnum
    )]
    public enum AggregationType {
        __invalid,
        Count,
        Sum,
        //Min,
        //Max,
        //Average,
        //Median
    }
}