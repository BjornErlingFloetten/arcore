﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {

    [Class(Description = 
        "Utility methods for -" + nameof(ARComponents.ARCQuery) + "-." +
        "\r\n" +
        "See -" + nameof(UtilCore) + "- for documentation about what belongs in such a Utility class like this." +
        "\r\n" +
        "See also -" + nameof(Extensions) + "-."
    )]
    public static class UtilQuery {

        [ClassMember(Description =
            "Contains prefixes for which links (see -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "-) are also desired without the prefix.\r\n" +
            "\r\n" +
            "For instance it is useful to write -" + nameof(FunctionKey) + "- suggestions, like -" + nameof(FunctionKeyYear) + "-', " +
            "but only writing -Year- instead of -FunctionKeyYear-.\r\n" +
            "\r\n" +
            "Used as parameter to -" + nameof(ARCDoc.DocLinkCollection.Create) + "-.\r\n" +
            "\r\n" +
            "See also -" + nameof(ARCDoc.Extensions.DescribeValue) + ", inner function prepareForLinkInsertionIfNoWhitespace.\r\n"
        )]
        public static List<string> SpecialLinkPrefixes = new List<string> {
            "TryGet", /// Supports <see cref="EntityMethodKey"/>
            nameof(NewKey),                /// Linking to -CountP- instead  <see cref="NewKeyCountP"/>
            nameof(FunctionKey),           /// Linking to -Year- instead of <see cref="FunctionKeyYear"/>
            nameof(FunctionKeyAggregate),  /// Linking to -Count- instead of <see cref="FunctionKeyAggregateCount"/>
            nameof(QueryExpression) };     /// Linking to -PIVOT- instead of <see cref="QueryExpressionPivot"/>

        [ClassMember(Description =
            "Equivalent to string." + nameof(string.IndexOf) + " " +
            "except that any character encountered inside pairs of the 'outsideOf'-char are not counted.\r\n" +
            "\r\n" +
            "Example: For parameters (\"'+'+'AB'\", \"+\", " + @"'\''" + ") 3 is returned, not 1"
         )]
        public static int IndexOfOutsideOf(string _string, string value, char outsideOf, int startIndex = 0) {
            var outsideOfPosEnd = startIndex - 1;
            while (true) {
                var anyOfPos = _string.IndexOf(value, outsideOfPosEnd + 1);
                if (anyOfPos == -1) return -1;
                var outsideOfPosStart = _string.IndexOf(outsideOf, outsideOfPosEnd + 1);
                if (outsideOfPosStart == -1) return anyOfPos;
                if (anyOfPos < outsideOfPosStart) return anyOfPos;
                outsideOfPosEnd = _string.IndexOf(outsideOf, outsideOfPosStart + 1);
                if (outsideOfPosEnd == -1) return -1; // Unmatched pair
            }
        }

        [ClassMember(Description =
            "Equivalent to string." + nameof(string.IndexOfAny) + " " +
            "except that any character encountered inside pairs of the 'outsideOf'-char are not counted.\r\n" +
            "\r\n" +
            "Example: For parameters (\"'+'+'AB'\", new char[] { '+' }, " + @"'\''" + ") 3 is returned, not 1"
         )]
        public static int IndexOfAnyOutsideOf(string _string, char[] anyOf, char outsideOf, int startIndex = 0) {
            var outsideOfPosEnd = startIndex - 1;
            while (true) {
                var anyOfPos = _string.IndexOfAny(anyOf, outsideOfPosEnd + 1);
                if (anyOfPos == -1) return -1;
                var outsideOfPosStart = _string.IndexOf(outsideOf, outsideOfPosEnd + 1);
                if (outsideOfPosStart == -1) return anyOfPos;
                if (anyOfPos < outsideOfPosStart) return anyOfPos;
                outsideOfPosEnd = _string.IndexOf(outsideOf, outsideOfPosStart + 1);
                if (outsideOfPosEnd == -1) return -1; // Unmatched pair
            }
        }

        [ClassMember(Description =
            "Equivalent to string." + nameof(string.Split) + " " +
            "except that any character encountered inside pairs of the 'outsideOf'-char are not counted as separators.\r\n" +
            "\r\n" +
            "Example: For parameters (\"'.'.A\", \".\") two elements are returned, not three.\r\n" +
            "\r\n" +
            "TODO: As of Mar 2021 the code is very inefficient for long strings."
         )]
        public static List<string> SplitOutsideOf(string _string, string separator, char outsideOf) {
            var retval = new List<string>();
            while (true) {
                var pos = IndexOfOutsideOf(_string, separator, outsideOf);
                if (pos == -1) {
                    retval.Add(_string);
                    return retval;
                } else {
                    retval.Add(_string[0..pos]);
                    _string = _string[(pos + 1)..]; // TOOD: This is very inefficient for long strings
                }
            };
        }
    }
}
