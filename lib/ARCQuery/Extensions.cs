﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCQuery {
    public static class Extensions {

        [ClassMember(Description =
            "Same functionality as -" + nameof(IP.TryGetP) + "- but understands the concept of -" + nameof(CompoundKey) + "-.\r\n" +
            "\r\n" +
            "Note how this is an extension of -" + nameof(IKIP) + "-, not -" + nameof(IP) + "-.\r\n" +
            "The reason is that a -" + nameof(CompoundKey) + "--implementations might need the entity's primary key in order to do lookup against the data storage " +
            "(looking up other entities pointing to that entity, that is, searching for foreign keys equal the the primary key).\r\n" +
            "This is the case for -" + nameof(EntityMethodKey) + "- for instance.\r\n" +
            "\r\n" +
            "A generated -" + nameof(CompoundKey) + "- is passed as ref, enabling it to be cached by outside context.\r\n" +
            "This is useful because creating the -" + nameof(CompoundKey) + "- is somewhat complicated " +
            "(multiple lookups for -" + nameof(ForeignKey) + "- for instance).\r\n" +
            "(and for -" + nameof(QuantileKey) + "- it is absolutely essential, in order to avoid huge duplicate processing).\r\n" +
            "\r\n"
        )]
        public static bool TryGetP(this IKIP ikip, IP dataStorage, IK key, out IP retval, ref CompoundKey? cachedCompoundKey, out string errorResponse) {

            /// TODO: Delete commented out code. Strategy changed 16 Feb 2021 to NOT _ALWAYS_ try the ordinary TryGetP FIRST
            ///// NOTE: VERY IMPORTANT POINT: _ALWAYS_ try the ordinary TryGetP FIRST. 
            ///// NOTE: One might believe that the existence of a <param name="cachedCompoundKey"/> means that 
            ///// NOTE: the ordinary TryGetP here is not relevant, and we can go direct for the compound key.
            ///// NOTE: BUT, we may have situations with NULL fields, that is, the first TryGetP fails due to field being 
            ///// NOTE: NULL for a specific entity, and a compound key then gets generated. But that does not mean that
            ///// NOTE: it is correct to use the compound key uncritically the next time (for another entity).
            ///// NOTE: So, in conclusion: _ALWAYS_ try the ordinary TryGetP FIRST. 

            if (cachedCompoundKey == null || cachedCompoundKey is CompoundInvalidKey) {
                /// Either this is 
                /// 1) First call, and cached Compound key is not set yet, or
                /// 2) We have already established that compound key is invalid, that is, not relevant, 
                ///    and our only 'chance' is looking direct at entity
                ///
                /// NOTE: Warning about non-consistent returning of results:
                /// NOTE: If both 
                /// NOTE  a) some, but not all, entities contains the property, and 
                /// NOTE: b) the key is also a valid compound key
                /// NOTE: then
                /// NOTE: we will now get different results depending on which entity was first checked.
                /// NOTE: 
                /// NOTE: Example: If there exists an EntityMethodKey for Customer.Name (or a NewKey called Name), 
                /// NOTE: and some Customer objects actually also have the Name property defined in the PropertyStream 
                /// NOTE: then the following will happen:
                /// NOTE:
                /// NOTE: 1) When first object does contain Name: 
                /// NOTE:    Its value will be returned (by call here to <see cref="IP.TryGetP"/>, 
                /// NOTE:    instead of value of EntityMethodKey-value.
                /// NOTE:    Cached compound key will not be set, so the same goes for subsequent objects
                /// NOTE:    until an object without the property is found, 
                /// NOTE:    in which case cachedCompondKey will be set to the EntityMethodKey and
                /// NOTE:    for all subsequent objects, regardless of they containing Name or not.
                /// NOTE: 2) When first object does not contain Name: 
                /// NOTE:    Cached compond key will be set immediately, and only result of EntityMethodKey will be returned.
                /// NOTE: 
                /// NOTE. Use of AsParallell / multi-threading complicates this further, as multiple calls 
                /// NOTE: may be made with cachedCompoundKey set to null
                /// NOTE:
                /// NOTE: In general, as a rule, there should never be a mix of direct defined properties for entities
                /// NOTE: and entity method keys with same name.
                /// NOTE: 
                /// NOTE: In order to avoid problem described above, it might be reasonable instead
                /// NOTE: use a strategy of _ALWAYS_ trying the ordinary TryGetP FIRST
                /// NOTE: but that will have significant performance consequences
                /// NOTE: because it is often "expensive" when <see cref="IP.TryGetP"/> fails, in order to return an error response.
                /// NOTE: See for instance <see cref="PExact{EnumType}"/>, metod TryConvertIKToPK.
                /// NOTE:
                /// TODO: Update 9 Mar 2021:
                /// TODO: An even uglier situation arises related to <see cref="ConstantKey{T}"/>
                /// TODO: If field names are years, like for instance 2019, 2020, 2021, 2022, 
                /// TODO: and you want to 'SELECT 2021, 2022' then, you may get either literally '2021' / '2022' as <see cref="ConstantKey{T}"/>
                /// TODO: or the content of the fields 2021 / 2022 due to the same reasoning as above.

                if (ikip.P.TryGetP(key, out retval, out errorResponse)) {
                    return true;
                }
            } else {
                errorResponse = null!; // Throw away result of errorResponse so far (we will check for null later)
            }

            /// TODO: Erroneusly recognized compound keys may cause problems:
            /// TODO: 
            /// TODO: Note that for queries with multiple components, we have a problem if
            /// TODO: a key is recognized as a compound key, but it is really not.
            /// TODO: 
            /// TODO: For instance, a key may actually have been a Compound key in the preceding query component, like
            /// TODO: for 'Order SELECT Amount, Customer.FirstName/ORDER BY Customer.FirstName'
            /// TODO: then Customer.FirstName will exist directly as a key in the collection returned by SELECT 
            /// TODO: so although it looks like a Compound key, it is not, BUT, when executing the second part,
            /// TODO: if Customer.FirstName does not exist, it will be attemped found as a CompoundKey
            /// TODO: 
            /// TODO: This can create problems if the expression is actually a valid CompoundKey like:
            /// TODO: 1) Expensive resultless queries may be attempted each time property is not found
            /// TODO: 2) (or worse, rather hypothetically, but still), the queries may actually return a value.
            /// TODO: The problem can be avoided to some degree by using the "AS"-modified in <see cref="QueryExpressionSelect"/>
            /// TODO: but it is not a satisfactory explanation.
            /// TODO: 
            /// TODO: Possible solutions:
            /// TODO: None as of 16 Feb 2021.
            /// 

            var strKey = key.ToString();

            if (cachedCompoundKey != null) {
                /// No need to check whether primary-key was asked for, because we know that these checks below failed at earlier call.
            } else {
                if (strKey.EndsWith("Id")) { // Note check for "hint", 'EndsWith("Id")' first, because 'ikip.P.GetType().ToStringVeryShort()' below is very expensive
                    /// Check if primary-key was asked for.
                    /// Note how we actually have the primary key available here, in contrast to the 'ordinary' <see cref="IP.TryGetP"/>)
                    /// NOTE: If inheritance is used, the check below may fail...  ikip.P may be of a sub-type...
                    if ((ikip.P.GetType().ToStringVeryShort() + "Id").Equals(strKey)) {

                        // Old method before 3 June 2021, but not allowed by PValue if Key implements IP
                        // retval = new PValue<IK>(ikip.Key);

                        // New method from 3 June 2021. 
                        if (ikip.Key is IP ip) {
                            /// Typical example would be <see cref="PK"/>
                            /// (Note how constructor of PValue does not allow IP so code below ("new PValue...") wil not work)
                            retval = ip;
                        } else {
                            retval = new PValue<IK>(ikip.Key);
                        }

                        errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        return true;
                    }
                }
            }

            /// TODO: ---------
            /// TODO: Implement use of <see cref="QueryProgressP.UseCache"/> as specified by <see cref="QueryExpressionCache"/>
            /// TODO: See <see cref="QueryExpressionCache"/> for practical issues involved.
            /// TODO: ---------

            if (cachedCompoundKey == null) {
                cachedCompoundKey = CompoundKey.Parse(dataStorage, strKey, ikip.P.GetType());
            }

            switch (cachedCompoundKey) {
                case CompoundInvalidKey invalidKey:
                    /// Do not bother calling TryGetP because <see cref="CompoundInvalidKey.TryGetPInternal"/> will always return false. Instead return better error message.
                    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>

                    /// Use possible value of errorResponse from "ordinary" call to <see cref="IP.TryGetP"/> above
                    /// and add information about compoundKey
                    errorResponse = (!String.IsNullOrEmpty(errorResponse) ?
                        (errorResponse + "\r\n") : "") +
                        nameof(CompoundKey) + "." + nameof(errorResponse) + ": " + invalidKey.ErrorResponse;
                    return false;
                default:
                    if (cachedCompoundKey.QuantileKey != null) {
                        return cachedCompoundKey.QuantileKey.TryGetP(ikip, dataStorage, cachedCompoundKey, out retval, out errorResponse);
                    }
                    return cachedCompoundKey.TryGetP(ikip, dataStorage, out retval, out errorResponse);
            }
        }
    }
}