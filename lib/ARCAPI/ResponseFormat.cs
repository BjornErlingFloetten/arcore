﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCAPI {

    [Enum(
        Description = 
            "The response format, HTML or JSON, for an API request.\r\n" +
            "\r\n" +
            "Note: Looking up documentation for individual members of this enum may be inconsistent because value may be interpreted as response format, not a key",
        AREnumType = AREnumType.OrdinaryEnum)]
    public enum ResponseFormat {
        __invalid,
        JSON,
        HTML
        // TODO: Add CSV, PDF and so on.
    }
}
