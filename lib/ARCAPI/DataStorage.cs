﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading;
using ARCCore;
using ARCDoc;
using ARCQuery;

namespace ARCAPI
{

    /// </summary>
    [Class(Description =
        "The all-in-memory global application data storage.\r\n" +
        "\r\n" +
        "Does also contain the necessary global locking object for thread-safe access (-" + nameof(Lock) + "-).\r\n" +
        "\r\n" +
        "Is usually accessed within -" + nameof(ARComponents.ARAAPI) + "- as a static instance Program.DataStorage.\r\n" +
        "\r\n" +
        "Can be kept up to date by -" + nameof(Subscription) + "- to -" + nameof(ARConcepts.PropertyStream) + "- " +
        "(see -" + nameof(StreamProcessor.OutsideLocalReceiver) + "-).\r\n" +
        "\r\n" +
        "Will usually contain (in addition to -" + nameof(PSPrefix.dt) + "-), " +
        "also other 'branches' like -" + nameof(PSPrefix.app) + "- and -" + nameof(PSPrefix.doc) + "-.\r\n" +
        "\r\n" +
        "See constructor for information about -" + nameof(ARConcepts.ExposingApplicationState) + "- through this class.\r\n" +
        "\r\n" +
        "This class is immutable."
    )]
    public class DataStorage
    {

        [ClassMember(Description =
            "The actual storage.\r\n" +
            "\r\n" +
            "The root-pointer to all objects stored."
        )]
        public IP Storage { get; private set; }

        [ClassMember(Description =
            "Usually (but not always) a branch of -" + nameof(Storage) + "- (like Storage[\"log\"][\"{nodeId}\"][\"Controller\"]).\r\n" +
            "\r\n" +
            "Enables -" + nameof(BaseController) + "-.- " + nameof(BaseController.CatchAll) + "- to dispatch an API request to the correct controller.\r\n" +
            "\r\n" +
            "See -" + nameof(BaseController.BuildControllerStorage) + "- for documentation.\r\n" +
            "\r\n"
        )]
        public IP ControllerStorage { get; private set; }

        /// <summary>
        /// TODO: Remove this as parameter to constructor, and ask Documentator for it instead (where it is located within Storage, since documentator put it there)
        /// </summary>
        [ClassMember(Description =
            "Note that may be null.\r\n" +
            "\r\n" +
            "A collection of relevant keys from -" + nameof(Storage) + "-, usually limited to Storage[\"doc\"].\r\n" +
            "\r\n" +
            "Enables -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "-.\r\n" +
            "Originates (normally) from -" + nameof(DocLinkCollection) + "-.-" + nameof(DocLinkCollection.Create) + "-.\r\n" +
            "\r\n" +
            "Most relevant when querying (static) documentation because when storage is static " +
            "then this parameter can be pre-calculated at application startup.\r\n" +
            "If on the other hand documentation storage is dynamically updated throughout the application lifetime then this parameter " +
            "must also be continously updated, something which could have an unacceptable performance impact.\r\n" +
            "\r\n" +
            "Often set at application startup with links to (static) documentation.\r\n" +
            "This will improve error message for instance, by enabling linking directy to the documentation in the error message.\r\n" +
            "\r\n" +
            "NOTE: From Dec 2021 it it ASSUMED in AgoRapide in general that content here is static and will not change,\r\n" +
            "NOTE: In other words, that content may be accessed without any locking (see -" + nameof(Lock) + "-).\r\n" +
            "NOTE: This significantly reduces the performance impact of " +
            "NOTE: -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "-."
        )]
        public DocLinkCollection? DocLinks { get; private set; }

        [ClassMember(Description =
            "Locking object for thread-safe access from simultaneous API requests.\r\n" +
            "\r\n" +
            "-" + nameof(BaseController) + "- components like -" + nameof(RQController) + "- and -" + nameof(EditController) + "- " +
            "(offering HTML forms POSTing to -" + nameof(AddController) + "-) " +
            "will typically need to acquire a Read lock " +
            "(" + nameof(ReaderWriterLockSlim.EnterReadLock) + "),\r\n" +
            "while components like -" + nameof(AddController) + "- will typically need to acquire a Write lock " +
            "(" + nameof(ReaderWriterLockSlim.EnterWriteLock) + ").\r\n" +
            "\r\n" +
            "Locking is also need by when keeping storage up-to-date " +
            "by -" + nameof(Subscription) + "- to -" + nameof(ARConcepts.PropertyStream) + "- " +
            "through -" + nameof(StreamProcessor.OutsideLocalReceiver) + "-.\r\n" +
            "\r\n" +
            "In -" + nameof(ARComponents.ARCEvent) + "- typical components requiring read locks are " +
            "-ProcessController-, -ESController- and -UIController- while -ASController- requires first a read lock (for parsing) " +
            "and then a write lock.\r\n" +
            "\r\n" +
            "NOTE: It is assumed that -" + nameof(DocLinks) + "- can be accessed without locking\r\n" +
            "NOTE: (see under -" + nameof(DocLinks) + "- for details)."
        )]
        public ReaderWriterLockSlim Lock { get; } = new System.Threading.ReaderWriterLockSlim();

        [ClassMember(Description =
            "Typically used by -" + nameof(AddController) + "-. " +
            "\r\n" +
            "Will normally be a method calling -" + nameof(StreamProcessor.SendFromLocalOrigin) + "-.\r\n" +
            "\r\n" +
            "Note: It is meaningless to have -" + nameof(DoNotStoreInternally) + " = FALSE and -" + nameof(ExternalReceiver) + "- = NULL at the same time."
        )]
        public Action<string>? ExternalReceiver { get; private set; }

        [ClassMember(Description =
            "Typically used by -" + nameof(AddController) + "-.\r\n" +
            "\r\n" +
            "Signals that properties should not be added to -" + nameof(DataStorage) + "- directly. " +
            "\r\n" +
            "Will typically be used in situations where one wants to ensure that data is actually added to -" + nameof(ARNodeType.CoreDB) + "- " +
            "before showing it in the API. This would again typically be implemented by using -" + nameof(ExternalReceiver) + "- " +
            "and waiting for the data to come back over a -" + nameof(Subscription) + "- to the -" + nameof(ARConcepts.PropertyStream) + "-.\r\n" +
            "\r\n" +
            "NOTE: As of Jun 2020 there is no mechanism in AgoRapide for ensuring that this actually happens other than for the client to use polling " +
            "against the API to ensure that the round-trip to the core and back has completed.\r\n" +
            "\r\n" +
            "Note: It is meaningless to have -" + nameof(DoNotStoreInternally) + " = FALSE and -" + nameof(ExternalReceiver) + "- = NULL at the same time."
        )]
        public bool DoNotStoreInternally { get; private set; }

        /// <param name="skipDocumentation">
        /// If TRUE then documentation will not be available and <see cref="ARConcepts.LinkInsertionInDocumentation"/> will not work.
        /// 
        /// Setting this value to TRUE will shorten application startup time by a few seconds (typically 3 seconds) 
        /// and also save approximately 10-20 MB of RAM.
        /// </param>
        [ClassMember(Description =
            "Creates a -" + nameof(DataStorage) + "- instance with default parameters.\r\n" +
            "\r\n" +
            "Note: You are not obliged to use this method, you can call constructor directly instead and set up this instance 'manually'.\r\n" +
            "\r\n" +
            "This method will:\r\n" +
            "Call constructor with assemblies from -" + nameof(UtilCore) + "-.-" + nameof(UtilCore.Assemblies) + "- and " +
            "-" + nameof(DoNotStoreInternally) + "- FALSE,\r\n" +
            "Create -" + nameof(PSPrefix.dt) + "- container (if not already set)r\n" +
            "Create -" + nameof(PSPrefix.doc) + "- container with -" + nameof(Documentator) + "-,\r\n" +
            "Set -" + nameof(DocLinks) + "-,\r\n" +
            "Connect -" + nameof(ExternalReceiver) + "- to -" + nameof(StreamProcessor.SendFromLocalOrigin) + "- and\r\n" +
            "Create -" + nameof(PSPrefix.app) + "- container with data about " +
            "-" + nameof(StreamProcessor) + "-, -" + nameof(ControllerStorage) + "-, -" + nameof(ForeignKey.IPRelationsKeysPointingTo) + "- and " +
            "-" + nameof(ForeignKey.IPRelationsKeysPointingFrom) + "-."
        )]
        public static DataStorage Create(IK nodeId, IP? storage = null, StreamProcessor? streamProcessor = null, bool skipDocumentation = false)
        {
            var retval = new DataStorage(nodeId, UtilCore.Assemblies, doNotStoreInternally: false, storage: storage);
            if (!retval.Storage.ContainsKey(nameof(PSPrefix.dt))) retval.Storage[nameof(PSPrefix.dt)] = new PRich();

            if (skipDocumentation)
            {
                /// Setting this value to TRUE will shorten application startup time by a few seconds (typically 3 seconds) 
                /// and also save approximately 10-20 MB of RAM.

                /// TODO: Create documentation in separate thread after startup, in order to make perceived startup time shorter.
                /// TODO: This will entail some changes in <see cref="Documentator.CreateAndInsert"/> in order to make it thread safe.

                var doc = new PRich();
                doc.IP.AddPV(BaseAttributeP.Description, 
                    "Documentation was not created " +
                    "(because parameter " + nameof(skipDocumentation) + " was set to TRUE at call to DataStorage.Create)."
                );
                retval.Storage[nameof(PSPrefix.doc)] = doc;
            }
            else
            {
                // Note that this call will insert the created Documentator into the storage.
                Documentator.CreateAndInsert(retval.Storage, UtilCore.Assemblies, specialPrefixes: UtilQuery.SpecialLinkPrefixes, ARCQueryIsAvailable: true);
                retval.DocLinks = retval.Storage[nameof(PSPrefix.doc)].GetPV<DocLinkCollection>(nameof(DocLink)); // HACK: Assume this key used by Documentator.
            }

            retval.ExternalReceiver = streamProcessor == null ? (Action<string>?)null : (s => streamProcessor!.SendFromLocalOrigin(s));

            retval.Storage[nameof(PSPrefix.app)] = new PRich();
            var thisApp = retval.Storage[nameof(PSPrefix.app)][nodeId] = new PRich();
            thisApp.AddPV(DocumentatorP._Description, "-" + nameof(ARConcepts.ExposingApplicationState) + "- for " + nodeId);

            thisApp["Controller"] = retval.ControllerStorage;
            thisApp[nameof(ForeignKey.IPRelationsKeysPointingTo)] = ForeignKey.IPRelationsKeysPointingTo;
            thisApp[nameof(ForeignKey.IPRelationsKeysPointingFrom)] = ForeignKey.IPRelationsKeysPointingFrom;

            if (streamProcessor != null)
            {
                thisApp[nameof(StreamProcessor)] = streamProcessor;
            }

            return retval;
        }

        [ClassMember(Description =
            "This method will:\r\n" +
            "Initialize -" + nameof(Storage) + "-,\r\n" +
            "Initialize -" + nameof(ControllerStorage) + "-.\r\n"
        )]
        public DataStorage(
                IK nodeId,
                IEnumerable<System.Reflection.Assembly> assemblies,
                bool doNotStoreInternally = false,
                IP? storage = null
        )
        {
            DoNotStoreInternally = doNotStoreInternally;
            Storage = storage ?? new PRich();
            ControllerStorage = BaseController.BuildControllerStorage(assemblies);
        }
    }
}