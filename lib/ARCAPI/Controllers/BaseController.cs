﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;
using ARCDoc;
using ARCQuery;
using System.Net;

namespace ARCAPI
{

    [Class(Description =
        "Contains basic functionality for serving API methods.\r\n" +
        "\r\n" +
        "Regarding -" + nameof(ARConcepts.ExposingApplicationState) + "-:\r\n" +
        "The specific controller instances serving requests are usually stored in a global -" + nameof(DataStorage) + "- " +
        "like 'app/{nodeId}/Controller/{TypeOfController}/{ControllerId}...'.\r\n" +
        "(in other words, -" + nameof(DataStorage.ControllerStorage) + "- actually points to a branch inside -" + nameof(DataStorage.Storage) + "-.)\r\n" +
        "\r\n" +
        "Examples of API requests and how they are resolved to a Controller by -" + nameof(CatchAll) + "-:\r\n" +
        "API request URL: https://yourserver.com/RQ/dt/Customer/42 \r\n" +
        "Controller instance (in this case of type -" + nameof(RQController) + "-) " +
        "to be found in controllerStorage[\"RQController\"]\r\n" +
        "Method -" + nameof(APIMethod) + "- is called on this instance.\r\n" +
        "\r\n" +
        "The following controller classes are included within -" + nameof(ARComponents.ARCAPI) + "-:\r\n" +
        "\r\n" +
        "-" + nameof(RQController) + "-:\r\n" +
        "Executes a REST like query request against the given -" + nameof(DataStorage) + "-.\r\n" +
        "Example: https://yourserver.com/RQ/dt/Customer/42 \r\n" +
        "\r\n" +
        "-" + nameof(AddController) + "-:\r\n" +
        "Parses and stores a -" + nameof(PropertyStreamLine) + "-.\r\n" +
        "Example: https://yourserver.com/Add/dt/Customer/42/FirstName = John\r\n" +
        "\r\n" +
        "-" + nameof(GQController) + "-:\r\n" +
        "Offers GraphQL queries.\r\n" +
        "Not implemented as of Feb 2022.\r\n" +
        "\r\n" +
        "-" + nameof(EditController) + "-:\r\n" +
        "Offers a simple HTML editing environment of the raw -" + nameof(ARConcepts.PropertyStream) + "- from the given key.\r\n" +
        "Example: https://yourserver.com/Edit/dt/Customer/42\r\n" +
        "\r\n" +
        "Note how these controllers are sufficient for a basic CRUD API (Create, Read, Update, Delete).\r\n" +
        "(Delete is done by using the metakey -" + nameof(PP.Invalid) + "-.)\r\n" +
        "\r\n" +
        "Often no additional -" + nameof(ARConcepts.ApplicationSpecificCode) + "- is needed.\r\n" +
        "\r\n" +
        "-" + nameof(ARComponents.ARCEvent) + "- offers additional controllers of which the most important are -RSController- and -ESController- " +
        "for operating against the -" + nameof(ARConcepts.RegStream) + "- and -" + nameof(ARConcepts.EventStream) + "- respectively.\r\n" +
        "\r\n" +
        "See -" + nameof(BuildControllerStorage) + "- for how -" + nameof(ARConcepts.ApplicationSpecificCode) + "- controllers are integrated.\r\n" +
        "\r\n" +
        "This system has the following advantages:\r\n" +
        "(Note: The concepts described below has shown little practical value as of Feb 2022).\r\n" +
        "\r\n" +
        "1) The API-routing syntax is by default exposed to the outside because the -" + nameof(DataStorage.ControllerStorage) + "- " +
        "is normally inserted into the global -" + nameof(DataStorage) + "-.\r\n" +
        "Therefore the standard query mechanism in AgoRapide can be used to get this information by querying like\r\n" +
        "'RQ/log/[nodeId]/Controller'\r\n" +
        "\r\n" +
        "2) The API itself is discoverable even without querying like above, without much special code.\r\n" +
        "This is because an invalid API request is actually analyzed by the same mechanism answering standard queries.\r\n" +
        "When the error message is returned it actually contains data about the correct syntax.\r\n" +
        "\r\n" +
        "3) The API is configurable both through the API itself and also over the -" + nameof(ARConcepts.PropertyStream) + "-.\r\n" +
        "For instance like 'Add/log/[nodeId]/Controller/RQ/~std/LogLevel = Warning' which would change the log level " +
        "for the standard instance ('std') of -" + nameof(RQController) + "- to 'Warning'.\r\n" +
        "\r\n" +
        "4) Different instances of the same controller type can exist and be configured individually.\r\n" +
        "For instance if some specific query needs additional " +
        "logging down to debug level, a dedicated controller can be created for this and configured individually, like\r\n" +
        "'Add/log/[nodeId]/Controller/RQController/~dbg/LogLevel = Debug'\r\n" +
        "(Note: Such a debug controller is created by default by -" + nameof(BuildControllerStorage) + "-).\r\n" +
        "\r\n" +
        "NOTE: Such specific controller instances can be used like 'RQ~dbg/...' instead of 'RQ/~dbg/...'. " +
        "NOTE: This make inserted links more correct, for instance when -" + nameof(RQController) + "- links to -" + nameof(EditController) + "-\r\n" +
        "NOTE: The reason for the somewhat special ~ prefix for the controller instance key is to avoid ambiguities " +
        "NOTE: which are inherent because the complete hierarchical level has not been deemed necessary to specify explicitly.\r\n" +
        "\r\n" +
        "Some limitations of this approach are:\r\n" +
        "1) Actual API method name is limited to the name of the controller type (without the 'Controller' part).\r\n" +
        "That is, a controller called CustomerController can only serve queries starting with 'Customer'.\r\n" +
        "\r\n" +
        "Note however that in your -" + nameof(ARConcepts.ApplicationSpecificCode) + "- you can implement any additional " +
        "controllers with routing structure totally independent of this standard generic AgoRapide mechanism.\r\n" +
        "In other words, you can keep the AgoRapide mechanism for its standardised approach to data manipulation " +
        "and add additional traditional controllers and methods as needed.\r\n" +
        "\r\n" +
        "Note how the -" + nameof(BaseController) + "- classes (like this class) " +
        "do not inherit any special Microsoft.AspNetCore.Mvc class. " +
        "They are totally stand-alone with regard to Microsoft.AspNetCore.Mvc.\r\n" +
        "\r\n"
    )]
    /// NOTE: Class should really have been marked "abstract" but then the strict enforcement of <see cref="EnumAttributeP.CorrespondingClass"/> will fail.
    public class BaseController : PConcurrent
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="assemblies">The assemblies into which to look for classes implementing <see cref="BaseController"/></param>
        /// <param name="globalRoutingPrefix"></param>
        /// <returns></returns>
        [ClassMember(
            Description =
                "Builds a collection of 'std' (standard) controller instances to use for serving API requests.\r\n" +
                "\r\n" +
                "Reflection is used to search the 'assemblies' parameter for classes implementing -" + nameof(BaseController) + "-.\r\n" +
                "\r\n" +
                "Note that if the returned storage is coupled to the -" + nameof(ARConcepts.PropertyStream) + "- then additional " +
                "elements may be added later, either through API methods or through -" + nameof(Subscription) + "- " +
                "(or the 'std' instances' properties may also be changed, through the same mechanisms).\r\n" +
                "\r\n" +
                "TODO: Consider moving this method to -" + nameof(DataStorage) + "-."
        )]
        public static IP BuildControllerStorage(IEnumerable<System.Reflection.Assembly> assemblies)
        { // , string globalRoutingPrefix = "api") {

            // TODO: Implement a IDictionary sub-class of IP that is only going to hold a collection of specified object types.
            // TODO: Add _Description to this object
            var retval = new PRich();
            retval.IP.AddPV(DocumentatorP._Description,
                "This collection contains instances of available controllers (classes inheriting -" + nameof(BaseController) + "-).\r\n" +
                "\r\n" +
                "Each controller may have multiple instances.\r\n" +
                "\r\n" +
                "In addition to the standard instance ('~std'), a debug instance ('~dbg') is also available which will do detailed logging.\r\n" +
                "\r\n" +
                "Additional instances may also be created in your -" + nameof(ARConcepts.ApplicationSpecificCode) + "- at application startup " +
                "or through ordinary API calls like \r\n" +
                "'Add/log/[nodeId]/Controller/RQController/~MyInstance/LogLevel = Debug'\r\n" +
                "(assuming that -" + nameof(DataStorage.ControllerStorage) + "- has been included in the -" + nameof(DataStorage.Storage) + "-)."
            );

            // Create standard instances of all controller classes found.
            assemblies.ForEach(a =>
            {
                a.GetTypes().Where(t => !t.IsAbstract && typeof(BaseController).IsAssignableFrom(t) && !typeof(BaseController).Equals(t)).ForEach(t =>
                {
                    var n = t.ToStringVeryShort();

                    // Do not do this. It would not be compatible with updates over the property stream
                    // (because 'API/1/RQ/~std/LogLevel = Debug' would not be understood, it must look like 'API/1/RQController/std/LogLevel = Debug')
                    // if (n.EndsWith("Controller")) n = n.Substring(0, n.Length - "Controller".Length);

                    if (retval.IP.ContainsKey(n))
                    {
                        throw new BaseControllerException(
                            "Multiple controller classes found that resolve to the same short name '" + n + "'.\r\n" +
                            "One of them is " + t.ToStringDB() + ".\r\n" +
                            "Possible resolution: Change name of, or remove one of the assemblies.\r\n" +
                            "Assemblies are:\r\n" + string.Join("\r\n", UtilCore.Assemblies.Select(a => a.GetName().FullName)) + "\r\n" +
                            "\r\n");
                    }

                    var controllerInstances = new PRich();
                    retval.IP.AddP(n, controllerInstances);

                    // Always available, 'std' Controller
                    var controller = (BaseController)System.Activator.CreateInstance(t);
                    controller.IP.AddPV(DocumentatorP._Description, "The standard instance of " + t.ToStringShort() + ". Used as default if no controller specified.");
                    controller.IP.AddPV(BaseControllerP.LogLevel, "STANDARD"); // TODO: Create stronger typing for this.
                    controller.IP.AddPV(PP.Created, UtilCore.DateTimeNow);
                    // TODO: Consider setting of Logger here.
                    // TODO: But be careful, Query for instance will log a lot then.
                    controllerInstances.IP.AddP("~std", controller);

                    // Always available, 'dbg' Controller
                    controller = (BaseController)System.Activator.CreateInstance(t);
                    controller.IP.AddPV(DocumentatorP._Description, "The debug (dbg) instance of " + t.ToStringShort() + ". This instance will do detailed logging.");
                    controller.IP.AddPV(BaseControllerP.LogLevel, "DEBUG"); // TODO: Create stronger typing for this.
                    controller.IP.AddPV(PP.Created, UtilCore.DateTimeNow);
                    // TODO: Consider setting of Logger here.
                    // TODO: But be careful, Query for instance will log a lot then.
                    controllerInstances.IP.AddP("~dbg", controller);
                });
            });

            if (!retval.IP.Any())
            {
                throw new BaseControllerException(
                    "No controller classes found.\r\n" +
                    "Possible cause: Necessary assemblies, like for instance " + typeof(RQController).Assembly.GetName().FullName + " " +
                    "was not included in parameter " + nameof(assemblies) + ".\r\n" +
                    "Possible resolution: Add assembly to parameter " + nameof(assemblies) + ".\r\n" +
                    "Assemblies given:\r\n" + string.Join("\r\n", assemblies.Select(a => a.GetName().FullName)) + "\r\n" +
                    "\r\n");
            }
            return retval;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="jsonDepth">
        /// The requested depth for which to recurse when generating a JSON response (only relevant when <see cref="ResponseFormat.JSON"/>
        /// </param>
        /// <param name="request">
        /// The URL-elements (as originally separated by /)
        /// </param>
        /// <param name="postData">
        /// Only relevant for HTTP POST.
        /// </param>
        /// <param name="dataStorage">
        /// </param>
        /// <returns></returns>
        [ClassMember(
             Description =
                 "The (only) API method that this controller supports.\r\n" +
                 "\r\n"
         )]
        public virtual ContentResult APIMethod(ResponseFormat format, int jsonDepth, List<IKCoded> request, string? postData, DataStorage dataStorage) => ContentResult.CreateError(format, "<p>\r\n" +
            nameof(APIMethod) + " not implemented in sub-class " + WebUtility.HtmlEncode(GetType().ToStringShort()) + ".<br>\r\n" +
            "Request: " + (request.Count == 0 ? "[EMPTY]" : ("'" + string.Join("/", request.ToString()) + "'")) + ".\r\n" +
            nameof(postData) + ": " + (string.IsNullOrEmpty(postData) ? "[EMPTY / NOT GIVEN]" : ("of length " + postData.Length)) +
            "</p>");

        [ClassMember(Description =
            "Serves the typical single generic API method in an AgoRapide API.\r\n" +
            "\r\n" +
            "Finds the correct controller to handle the query (from -" + nameof(DataStorage.ControllerStorage) + "), " +
            "and returns result from that controller.\r\n" +
            "\r\n" +
            "The API-routing corresponds to the type name of the controller (without the 'Controller' part if desired) and " +
            "optionally the ~id of the controller when other than the standard instance.\r\n" +
            "For -" + nameof(RQController) + "-, the routing would typically be like this:\r\n" +
            "  'RQ/[parameters]'  (identical to 'RQController/[parameters]').\r\n" +
            "The debug instance is available like this:\r\n" +
            "  'RQ~dbg/[parameters]'  (identical to 'RQ/~dbg/[parameters]' / 'RQController~dbg/[parameters]' / 'RQController/~dbg/[parameters]').\r\n" +
            "\r\n" +
            "If query does not 'reach' any controller, routing information is returned instead " +
            "(from -" + nameof(DataStorage.ControllerStorage) + "-).\r\n" +
            "For instance querying nothing at all will list corresponding information " +
            "from -" + nameof(DataStorage.ControllerStorage) + "- instead.\r\n" +
            "\r\n" +
            "Note how uses -" + nameof(DataStorage.Lock) + "- in order to be thread safe in connection with -" + nameof(StreamProcessor.OutsideLocalReceiver) + "-."
        )]
        public static ContentResult CatchAll(string? strRequest, string? postData, DataStorage dataStorage)
        {

            // STEP 1, Construct request list
            // =========================
            var listStrRequest = strRequest?.
                Split("/").
                Select(s =>
                    /// Note that a lot of special characters may come through the standard URLEncode mechanism to here
                    /// in which case we just encode them back.
                    /// Typical examples would be ~, space, *, other than latin alphabet characters and so on.
                    /// HACK: / TODO: This special treatment of some characters may cause problems in <see cref="AddController.APIMethod"/>
                    string.Join("", s.Select(c => IKCoded.AllowedEncodedCharacters.Contains(c) ? c.ToString() : IKCoded.Encode(c.ToString())))
                ).
                ToList() ?? new List<string>();
            var (format, jsonDepth) = GetFormatAndAdjustRequest(ref listStrRequest);

            // STEP 2, Adjust request
            // =========================
            if (listStrRequest.Count >= 1)
            {
                var pos = listStrRequest[0].IndexOf("0x007E"); // Hex 7E is tilde, '~'.
                if (pos > 0)
                {
                    // A specific controller instance can be specified without a hierarhical level, like 'api/RQ~dbg/..' instead of 'api/RQ/~dbg/..'. Turn into 'api/RQ/~dbg/..'.
                    listStrRequest.Insert(1, listStrRequest[0][pos..]);
                    listStrRequest[0] = listStrRequest[0][..pos];
                }
                else if (listStrRequest.Count == 1)
                {
                    // TODO: Delete commented out code. Caused more harm than gain.
                    //// No controller instance specified. Use ~std
                    //listStrRequest.Insert(1, "0x007E" + "std");
                }
                else
                {
                    if (listStrRequest[1].IndexOf("0x007E") == -1)
                    {
                        // No controller instance specified. Use ~std
                        listStrRequest.Insert(1, "0x007E" + "std");
                    }
                }
                if (!listStrRequest[0].EndsWith("Controller"))
                {
                    // TODO: Make configurable. As now this assumes that controller storage is like 'api/{controllerName}/...'
                    // TODO: but there is no reason for limiting the controller type to zero-based index 1 (second position).
                    listStrRequest[0] = listStrRequest[0] + "Controller";
                    /// See comment inside <see cref="BuildControllerStorage"/> for why 'Controller' could not be removed within
                    /// <see cref="DataStorage.ControllerStorage"/> instead.
                }
            }

            // STEP 3, Decode request
            // =========================
            var request = new List<IKCoded>();
            foreach (var s in listStrRequest)
            {
                if ("".Equals(s))
                {
                    // Special case for empty string, not allowed by TryParse but allowed by constructor.
                    // There is for instance a difference between doing ".../RQ" and ".../RQ/"
                    // (showing details about RQ-controller or querying root of data storage)
                    request.Add(IKCoded.FromEncoded(s));
                    continue;
                }
                if (!IKCoded.TryParse(s, out var ikCoded, out var errorResponse))
                {
                    return ContentResult.CreateError(format,
                        "<p>" +
                        "Part '" + System.Net.WebUtility.HtmlEncode(s) + "' of request " +
                        "is not a valid key. " + nameof(errorResponse) + ": " +
                        "'" + WebUtility.HtmlEncode(errorResponse) + "'" +
                        "</p>");
                }
                request.Add(ikCoded);
            }

            // STEP 4, Find controller with which to serve request
            // =========================
            /// NOTE: Code below is <see cref="ARConcepts.AvoidingRepetition"/> in practice.
            /// NOTE: It offers routing information /  API usage through the standard RESTQuery mechanism
            (List<IKCoded> linkContext, IP controller) tuple = default;
            try
            {
                dataStorage.Lock.EnterReadLock(); /// Important to lock, because <see cref="DataStorage.ControllerStorage"/> may be updated over the property stream (if included in <see cref="DataStorage.Storage"/>)

                if (!TryNavigateToLevelInDataStorage(dataStorage, request.Take(Math.Min(2, request.Count)).ToList(), controller: null, out tuple, out var errorResponseHTML,
                    initialPointer: dataStorage.ControllerStorage) // Note use of initialPointer-parameter here
                )
                {
                    //    // TODO: Improve on error messages, standardise more
                    //    // TODO: Take into account requested format.
                    //    // TODO: Make ContentResult create error messages for any format (just specify the format)
                    return ContentResult.CreateError(format, errorResponseHTML);
                }

                // TODO: Check for * at end of Query (if three items)
                var returnFlattenedHTML = false;
                if (request.Count > 0 && "*".Equals(request[^1].Unencoded.ToString()))
                {
                    returnFlattenedHTML = true;
                    // TODO: Modify request collection?
                }

                var linkToActualAPIMethod = "";
                if (request.Count == 2)
                {
                    // TODO: Make 2 configurable. As now this assumes that the first two levels points to a controller

                    // TODO: The code for chosing controller no longer works after we removed /api/ from request string Mar 2021.   

                    // Add specific link to start page
                    // Problem: Query now (AFTER insertion of ~std above) is similar to
                    //   RQ/~std       (request.Count == 2)
                    // But this is kind of ambigious, do we want information about the
                    // controller, or do we want to call the controller's API method?
                    // In the last case, an explicit way of specifying that would be 
                    //   RQ/~std/      (request.Count == 3)   // TODO: Test and clarify what was meant here
                    // Which also makes it easier to insert relative links on that page

                    // We therefore choose the former, but with link to the latter
                    linkToActualAPIMethod = "<p>" +
                        "NOTE: Showing information about " + tuple.controller.GetType().ToStringShort() + " serving API method.<br>\r\n" +
                        "Click <a href=\"" + request[1].Encoded + "/\">here</a> " +
                        "in order to <a href=\"" + request[1].Encoded + "/\">call actual API method</a> instead.\r\n" +
                        "</p>\r\n\r\n";
                }
                if (request.Count <= 2)
                { // Or tuple.linkContext!=2
                  // TODO: Make configurable. As now this assumes that the first two levels points to a controller

                    // The path does not constitute a complete API method call because it does not contain any parameters
                    // List out information about routing and controllers instead.
                    // Note that we do not bother with generating other formats than HTML here.
                    // Client should be "aware" of the fact that its query is not an ordinary query, 
                    // but an API discovery query.

                    /// TODO: Note that <see cref="DataStorage.PotentialLinks"/> is difficult to use now
                    /// TODO: (for calling <see cref="Documentator.InsertLinksSingleFile"/>)
                    /// TODO: because of the 'location' of the URL being served now.
                    /// TODO: FIX THIS!
                    return ContentResult.CreateHTMLOK(UtilDoc.GenerateHTMLPage(
                        linkToActualAPIMethod +
                        (returnFlattenedHTML ?
                            tuple.controller.ToHTMLSimpleSingleRecursive(prepareForLinkInsertion: false, currentIK: tuple.linkContext) : /// Extension method belongs here: <see cref="ARCDoc.Extensions.ToHTMLSimpleSingle"/>
                            tuple.controller.ToHTMLSimpleSingle(prepareForLinkInsertion: false, tuple.linkContext) /// Extension method belongs here: <see cref="ARCDoc.Extensions.ToHTMLSimpleSingle"/>
                        )
                    ));
                }
            }
            finally
            {
                dataStorage.Lock.ExitReadLock();
            }

            // STEP 5, Call ordinary method in controller.
            // =========================

            // NOTE: Do not remove empty string as list item in request
            // NOTE: We should not do this, because it would hide from the controller the hierarchical level that the browser sees.
            // NOTE: This again would mess up relative linking between controllers, for instance RQController attempting to link to EditController by ../Edit
            //if ("".Equals(request[^1])) {
            //    /// Would happen with RQ/Index.html, RQ/.html, RQ/
            //    /// NOTE: Do not move this code to <see cref="GetFormatAndAdjustRequest"/>
            //    request.RemoveAt(request.Count - 1);
            //}

            /// Note how the implemented <see cref="BaseController.APIMethod"/> must make the correct choice between ReadLock or WriteLock.
            InvalidObjectTypeException.AssertAssignable(tuple.controller, typeof(BaseController));
            tuple.controller.Inc(BaseControllerP.CountRequest);
            try
            {
                return ((BaseController)tuple.controller).APIMethod(
                    format,
                    jsonDepth,
                    request.Skip(tuple.linkContext.Count).ToList(), // Skip the request part that chose the controller, like 'RQ/~std' (usually two items)
                    postData,
                    dataStorage
                );
            }
            catch (Exception ex)
            {
                tuple.controller.HandleException(ex);
                tuple.controller.Inc(BaseControllerP.CountException);
                // TODO: Take into account other formats than HTML!
                // TODO: Create link to data storage with information about Controller.
                return ContentResult.CreateError(
                    format,
                    "<p>Exception " + ex.GetType() + " occurred<p>\r\n" +
                    "<p>Exception details</p>\r\n" +
                    WebUtility.HtmlEncode(tuple.controller.GetPV(PP.ExceptionText, defaultValue: "[NO EXCEPTION DETAILS AVAILABLE]")).Replace("\r\n", "<br>\r\n")
                ); ;
            }
        }

        // TODO: Make NavigateToLevelInDataStorage in addition to TryNavigateToLevelInDataStorage
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataStorage"></param>
        /// <param name="levels">
        /// Should be in format as encoded by <see cref="PropertyStreamLine.EncodeKeyPart"/>. 
        /// Each level will be decoded by <see cref="PropertyStreamLine.Decode"/>
        /// </param>
        /// <param name="controller">
        /// Will be null when called from a static context, like <see cref="BaseController.CatchAll"/>.<br>
        /// Relevant when called from an actual controller instance, like <see cref="RQController.APIMethod"/>.<br>
        /// Used for logging purposes.
        /// </param>
        /// <param name="retval"></param>
        /// <param name="errorResponseHTML"></param>
        /// <param name="initialPointer"/>
        /// May be null.
        /// Set this only when initial pointer is not <see cref="DataStorage.Storage"/> but for instance <see cref="DataStorage.ControllerStorage"/>
        /// Note that no attempts will be made at <see cref="ARConcepts.LinkInsertionInDocumentation"/> in <paramref name="errorResponseHTML"/> 
        /// if this parameter is set.
        /// </param>
        /// <returns></returns>
        [ClassMember(
            Description =
                "Navigates to the specified level (usually within -" + nameof(DataStorage.Storage) + "-).\r\n" +
                "\r\n" +
                "If unsuccesful returns a corresponding error message with link to last 'correct' level.\r\n" +
                "Example: If attempting to navigate to 'Customer/42/MiddleName' but 'MiddleName' does not exist, will offer link to Customer/42.\r\n" +
                "\r\n" +
                "If unsuccessful, then the out-parameter errorResponseHTML will also contain an eventual warning " +
                "from -" + nameof(GetTimestampIsOldWarningHTML) + "-.\r\n" +
                "This is pertinent in cases where the actual key has just not been read yet " +
                "from the -" + nameof(ARConcepts.PropertyStream) + "--storage.\r\n" +
                "\r\n" +
                "Also able to understand and parse -" + nameof(QueryExpression) + "-.\r\n" +
                "\r\n" +
                "Note that also used for navigating within -" + nameof(DataStorage.ControllerStorage) + "-, " +
                "that is, to route an API-method to the desired controller.\r\n" +
                "\r\n" +
                "TODO: This concept is quite generic. It is useful also in console applications and similar.\r\n" +
                "TODO: Move to ARCCore instead."
        )]
        public static bool TryNavigateToLevelInDataStorage(DataStorage dataStorage, List<IKCoded> levels, BaseController? controller, out (List<IKCoded> linkContext, IP ip) retval, out string errorResponseHTML, IP? initialPointer = null)
        {

            var errorResponseModifier = new Func<string, string>(r =>
            {
                r = GetTimestampIsOldWarningHTML(dataStorage.Storage) + r;
                if (initialPointer != null)
                {
                    // Do not try link insertion because most probably links will not resolve
                }
                else if (dataStorage.DocLinks != null)
                {
                    r = dataStorage.DocLinks.InsertLinks(
                        contentLocation: levels.
                            Take(levels.Count == 0 ? 0 : (levels.Count - 1)).
                            ToList(),
                        contentHTML: r
                    );
                }
                return r;
            });
            // Step 1: Navigate to needed element 
            // Return friendly error message if fails.

            var beforeLastPointer = (IP?)null; /// Hack for understanding <see cref="EntityMethodKey"/> below

            var lastPointer = (IP?)null;
            var pointer = initialPointer ?? dataStorage.Storage;
            var linkContext = new List<IKCoded>();
            for (var i = 0; i < levels.Count; i++)
            {

                linkContext.Add(levels[i]);

                if ("".Equals(levels[i].Encoded))
                {
                    // This is related to need for "api/RQ" to be identical to "api/RQ/", or rather "api/Customer/42" to be identical to "api/Customer/42/"
                    // TODO: If this code should be added BEFORE or AFTER adding to linkContext about has not been decided.
                    // TODO: Look especially out for creation of relative links (number of "../"'s needed to travel UP in hierarchy before going downwards,
                    // TODO: as the browser sees is (for the browser, a trailing / (slash) actually makes a difference in this regard.
                    continue;
                }

                if ("*".Equals(levels[i].Unencoded.ToString()))
                {
                    if (i < levels.Count - 1)
                    {
                        // TODO: Introduce wildcard at any level. At that level, 
                        // TODO: iterate for every key, and combine the result.
                        // TODO: Will that work with wildard at last level?
                        // TODO: (We would get multiple #002A top of page anchors for instance...)

                        // TODO: Create more elaborate error pages
                        retval = default;
                        errorResponseHTML = "<p>ERROR: Wildcard '*' only allowed at last level</p>";
                        return false;
                    }
                    else
                    {
                        break; // We are finished. pointer 'points' correctly.
                    }
                }

                if (!pointer.TryGetP<IP>(linkContext[^1].Unencoded, out var nextLevel))
                {
                    var lastQueryParseError = (string?)null;
                    var entityMethodKeyErrorResponse = (string?)null;

                    /// Added functionality 2 Mar 2022: Add functionality for understanding <see cref="EntityMethodKey"/>

                    /// Before attempting to parse as query expression, see if this is an <see cref="EntityMethodKey"/>
                    /// TODO: Consider adding support for other keys here also, <see cref="NewKey"/> for instance.
                    if (
                        i > 0 &&
                        !(beforeLastPointer is null) &&
                        EntityMethodKey.AllEntityMethodKeysForEntityTypeDict(pointer.GetType()).TryGetValue("TryGet" + linkContext[^1].Unencoded.ToString(), out var key)
                    )
                    {
                        if (key.TryGetP(new IKIP(levels[i - 1].Unencoded, pointer), dataStorage: beforeLastPointer, out nextLevel, out entityMethodKeyErrorResponse))
                        {
                            goto found_nextLevel;
                        }
                        else
                        {
                            entityMethodKeyErrorResponse =
                                "Unable to find a value for " + pointer.GetType().ToStringShort() + "." + linkContext[^1].Unencoded.ToString() + ". " +
                                "Details: " + entityMethodKeyErrorResponse;
                            goto query_parse_error_or_key_not_found;
                        }
                    }

                    /// Key at this level not found. This is OK if all subsequents level are valid query expressions
                    /// So, if all elements from here on are valid queries, then we can execute as <see cref="QueryExpression"/>
                    var queries = new List<QueryExpression>();
                    int j;
                    for (j = i; j < levels.Count; j++)
                    {
                        if (!QueryExpression.TryParse(levels[j].Unencoded.ToString(), out var query, out lastQueryParseError))
                        {
                            break;
                        }
                        queries.Add(query);
                    }
                    if (queries.Count == 0)
                    {
                        // We have a dilemma. Should we 
                        // 1) Show error message for key not found in hierarchical level, OR
                        // 2) Show parse error for query expression

                        // We choose 1), but utilize lastQueryParseError in order to clarify
                        goto query_parse_error_or_key_not_found;
                    }

                    linkContext.RemoveAt(linkContext.Count - 1); /// Remove last item, because that is a <see cref="QueryExpression"/>

                    if (queries.Count != (levels.Count - i))
                    {
                        // Some (but not all) query expressions failed to parse

                        for (var k = i; k < levels.Count; k++)
                        {
                            /// Add query expressions also to link context before generating error response
                            /// (necessary for <see cref="QueryProgress.GetHTMLLinksToIndividualQueryParts"/> to construct an edit-field for query)
                            linkContext.Add(levels[k]);
                        }

                        retval = default;
                        errorResponseHTML = errorResponseModifier(
                                QueryProgress.GetHTMLLinksToIndividualQueryParts(linkContext, queries, countOfQueriesThatFailedToParse: levels.Count - i - queries.Count) + "\r\n" +
                                "<p>ERROR:<br><br>" +
                                ("".Equals(levels[j]) ? (
                                    "Empty Query expression found.\r\n" +
                                    "Possible resolution: Remove superfluous forward slash ('/') in query."
                                ) : (
                                    "Query expression<br><br>" +
                                    "<b>" + WebUtility.HtmlEncode(levels[j].Unencoded.ToString()) + "</b><br><br>" +
                                    "failed to parse with the following error response:<br><br>\r\n\r\n" +
                                    "<b>" + WebUtility.HtmlEncode(lastQueryParseError).Replace("\r\n", "<br>") + "</b><br>\r\n" +
                                    "</p>"
                                )) +
                                "<p>" +
                                "Last query expression to successfully parse was '" + WebUtility.HtmlEncode(queries[^1].ToString()) + "'." +
                                "</p>" +
                                "<p>(NOTE. After the first valid -" + nameof(QueryExpression) + "-, all following items in the request must also be valid query expressions)</p>\r\n"
                        );
                        return false;
                    }

                    // TODO: MOVE THIS CODE TO SEPARATE METHOD!

                    // Create collection of current pointer (everything except _Description)
                    var descriptionKey = PK.FromEnum(DocumentatorP._Description);
                    var initialCollection = pointer.Where(ikip => !descriptionKey.Equals(ikip.Key));

                    var progress = new Query(

                        /// The normal case would be to look for related entities on-level-up from pointer, that is lastPointer,
                        /// but if none exists, just use current pointer.
                        /// TODO: This causes confusion with what we want <see cref="QueryProgress.DataStorage"/> to point to.
                        dataStorage: lastPointer ?? pointer,

                        queries: queries,
                        initialCollection: initialCollection,
                        /// Sample collection, find initial type. 
                        /// TODO: This only works for uniform collections (exact same type, even inheritance not allowed)
                        /// 
                        /// NOTE: Weakness with property stream format:
                        /// NOTE: If you put data like 'dt/Customer/13 ='
                        /// NOTE: in the property strema, then the object will be chosen as PValueEmpty by PropertyStreamLine.TryStore
                        /// NOTE: and it can possible mess up all subsequent queries if this is the first object chosen by for instance
                        /// NOTE: <see cref="ARCQuery.Extensions.TryGetP"/> when finding a <see cref="CompoundKey"/>
                        currentType: initialCollection.FirstOrDefault()?.P.GetType() ?? typeof(object),
                        logger: controller == null || controller.IP.Logger == null ?  /// Note: controller == null (meaning we are called from <see cref="CatchAll"/> would be a weird case, using Query-mechanism for discovering API routing
                            (Action<string>?)null :
                            // Note how we chain together loggers here, by just adding a context to call to the logger in the containing class.
                            s => controller!.IP.Logger!(nameof(Query) + s)
                    );

                    progress.ExecuteAll(); // Note how this has its own exception handler.

                    /// Make result (log, terminating reason, exception) available for later query (<see cref="ARConcepts.ExposingApplicationState"/>)
                    controller?.IP.SetP(
                        IKType.FromType(typeof(QueryProgress)),
                        /// Note: DeepCopy is a trick in order for <see cref="QueryProgress.Result"/> not to "linger around" in memory as that collection may be quite big.
                        /// It also ensures a better presentation by <see cref="QueryProgress.ToHTMLSimpleSingle"/>
                        progress.Progress.DeepCopy()
                    );

                    for (j = i; j < levels.Count; j++)
                    {
                        // Add query expressions also to link context before returning
                        linkContext.Add(levels[j]);
                    }

                    retval = (
                        linkContext,
                        progress.Progress // Result is either a 'normal' result, or some log / exception information
                    );
                    errorResponseHTML = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;

                query_parse_error_or_key_not_found:

                    // TODO: Create more elaborate error pages
                    retval = default;

                    /// Add query expressions (failed or not) also to link context before generating error response
                    /// (necessary for <see cref="QueryProgress.GetHTMLLinksToIndividualQueryParts"/> to construct an edit-field for query)
                    linkContext = levels.ToList(); // Make copy with ToList "just in case" (since Lists are after all muteable)

                    errorResponseHTML = errorResponseModifier(
                        /// It is very practical to use <see cref="QueryProgress.GetHTMLLinksToIndividualQueryParts"/> now in order to generate links, although no queries are present here
                        QueryProgress.GetHTMLLinksToIndividualQueryParts(linkContext, new List<QueryExpression>(), levels.Count - i) +
                        "<p>ERROR:<br><br>" +
                        "<b>At hierarhical level " + (i == 0 ? "[ROOT]" : string.Join("/", levels.Take(i))) + ", key '" + WebUtility.HtmlEncode(levels[i].Unencoded.ToString()) + "' not found</b>" +
                        "</p>\r\n" +
                        (lastQueryParseError is null ? "" :
                            (
                                "<p>In case you attempted to supply a -" + nameof(QueryExpression) + "-, " +
                                "this error message may be of help:<br><br>" +
                                "<b>" + WebUtility.HtmlEncode(lastQueryParseError).Replace("\r\n", "<br>") + "</b>" +
                                "<br></p>\r\n"
                            )
                        ) +
                        (entityMethodKeyErrorResponse is null ? "" :
                            (
                                "<p>In case you attempted to use an -" + nameof(EntityMethodKey) + "-, " +
                                "this error message may be of help:<br><br>" +
                                "<b>" + WebUtility.HtmlEncode(entityMethodKeyErrorResponse).Replace("\r\n", "<br>") + "</b>" +
                                "<br></p>\r\n"
                            )
                        ) +
                        "<br>\r\n" +
                        "<p>Use this link to see available keys at this level:</p>\r\n" +
                        "<p>" +
                        (i == 0 ?
                            (
                            "<a href=\"" +
                                string.Join("/", Enumerable.Repeat("..", levels.Count - 1)) + // Move 'up' as needed
                                (levels.Count == 1 ? "." : "/") +
                                "\">" +
                                WebUtility.HtmlEncode("[ROOT]") + "</a>"
                            ) :
                            (
                            "<a href=\"" +
                                string.Join("/", Enumerable.Repeat("..", levels.Count - i)) + // Move 'up' as needed
                                (i == (0) ? "" : "/") +
                                linkContext[i - 1].Encoded + "\">" +
                                WebUtility.HtmlEncode(linkContext[i - 1].Unencoded.ToString()) + "</a>"
                            )
                        ) +
                        "</p>\r\n"
                    );
                    return false;
                }

            found_nextLevel:

                beforeLastPointer = lastPointer; 
                lastPointer = pointer;
                pointer = nextLevel;
            }
            retval = (linkContext, pointer);
            errorResponseHTML = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(
            Description =
                "Decides -" + nameof(ResponseFormat) + "- (like HTML, JSON, CSV, PDF and so on).\r\n" +
                "\r\n" +
                "For Requests ending with /.html HTML-format is assumed, for .json JSON is assumed and so on.\r\n" +
                "\r\n" +
                "Default format is -" + nameof(ResponseFormat.HTML) + "-.\r\n" +
                "\r\n" +
                ".json may be given as .json{depth} indicating to which depth the serialization mechanism should work. 1 is default.\r\n" +
                "\r\n" +
                "If a format is specified (as the end-item in the request item list) then that corresponding item is removed from list.\r\n" +
                "\r\n" +
                "TODO: Improve this, especially the index.html-part which is not generic")]
        public static (ResponseFormat format, int jsonDepth) GetFormatAndAdjustRequest(ref List<string> request)
        {
            ResponseFormat format;
            var depth = 0;
            if (request.Count == 0)
            {
                format = ResponseFormat.HTML; // Change 2 Feb 2021 to HTML as default format instead of JSON (human requested content should have the least complicated URL)
            }
            else
            {
                var strFormat = request[^1].ToUpper();
                if (strFormat.EndsWith("." + nameof(ResponseFormat.HTML)))
                {
                    // TODO: Change to equals[".HTML"], that is, enforce /.HTML as end (less chance of 'colliding' with data)
                    // TODO: Or maybe not, because we want to allow Index.html? Or not?
                    format = ResponseFormat.HTML;
                    request[^1] = request[^1][..^".HTML".Length];
                    if (request[^1].ToUpper().Equals("INDEX"))
                    {
                        /// The concept Index.html is not understood by the routing mechanism. 
                        /// It shows up due to <see cref="ARCDoc.Extensions.ToHTMLSimpleSingle"/> inserting links
                        /// back to root-level.
                        request[^1] = ""; /// Do not remove (because of how <see cref="CatchAll"/> distinguishes at root level
                    }
                }
                else
                {
                    // JSON format is specified as either '.json' or '.json{depth}' (where depth is requested recursive depth, default = 1)

                    /// NOTE: JSON depth and returnFlattenedHTML (in <see cref="RQController.APIMethod"/> is somewhat corresponding concepts
                    /// NOTE: and could have been be treated in a more coherent manner.
                    /// NOTE: (like having .html{depth} like we have .json{depth} now).

                    var pos = strFormat.IndexOf("." + nameof(ResponseFormat.JSON));
                    if (pos < 1)
                    {
                        // Note that "/.json" is not allowed, only ".json".
                        format = ResponseFormat.HTML; // Change 2 Feb 2021 to HTML as default format instead of JSON (human requested content should have the least complicated URL)
                    }
                    else
                    {
                        var strDepth = strFormat[(pos + ("." + nameof(ResponseFormat.JSON)).Length)..];
                        if ("".Equals(strDepth)) strDepth = "1";
                        if (!int.TryParse(strDepth, out depth) || depth < 1)
                        {
                            depth = 0;
                            format = ResponseFormat.HTML; // Change 2 Feb 2021 to HTML as default format instead of JSON (human requested content should have the least complicated URL)
                        }
                        else
                        {
                            request[^1] = request[^1][..pos];
                            if (request[^1].ToUpper().Equals("INDEX"))
                            {
                                /// TODO: As of Feb 2021 not relevant for JSON because we do not have a corresponding ARCDoc.Extensions.ToJSONSimpleSingle
                                /// TODO: to <see cref="ARCDoc.Extensions.ToHTMLSimpleSingle"/> inserting links back to root-level.
                                /// TODO: Code kept "just in case" and for clarity
                                request[^1] = ""; /// Do not remove (because of how <see cref="CatchAll"/> distinguishes at root level
                            }
                            format = ResponseFormat.JSON;
                        }
                    }
                }
            }
            return (format, depth);
        }

        [ClassMember(Description =
            "If -" + nameof(StreamProcessorP.TimestampIsOld) + "- is found in the rootStorage-parameter, " +
            "returns warning message in HTML format.\r\n" +
            "Otherwise, empty string is returned."
        )]
        public static string GetTimestampIsOldWarningHTML(IP rootStorage) =>
            !rootStorage.GetPV<bool>(StreamProcessorP.TimestampIsOld, defaultValue: false) ? "" : ( // Note that absence of key is considered as FALSE
                "<p style=\"color:red\">" +
                    "WARNING: -" + nameof(StreamProcessorP.TimestampIsOld) + "-.\r\n" +
                    "Possible cause: API is currently initializing.\r\n" +
                    (!rootStorage.TryGetPV<DateTime>(StreamProcessorP.Timestamp, out var t) ? "" :
                    ("Returned data is current up to about <b>" + t.ToStringDateAndTime() + "</b>. Refresh this page for update."
                    )) +
                    "</p>"
            );


        public class BaseControllerException : ApplicationException
        {
            public BaseControllerException(string message) : base(message) { }
            public BaseControllerException(string message, Exception inner) : base(message, inner) { }
        }
    }

    [Enum(
        AREnumType = AREnumType.PropertyKeyEnum,
        Description = "Describes class -" + nameof(BaseController) + "-."
    )]
    public enum BaseControllerP
    {
        __invalid,

        // TODO: Add configuration and also 'username' / 'password' for controllers.

        [PKType(
              Description = "Number of calls to -" + nameof(BaseController.APIMethod) + "- since application initialization",
             Type = typeof(long)
         )]
        [PKLog(DoNotLogAtAll = true)] // TODO: Implement TimeInterval here.
        CountRequest,

        [PKType(
            Description = "Number of exceptions at call to -" + nameof(BaseController.APIMethod) + "- since application initialization",
            Type = typeof(long)
        )]
        [PKLog(DoNotLogAtAll = true)] // TODO: Implement TimeInterval here.
        CountException,

        /// <summary>
        /// TODO: ADD TYPE HERE.
        /// </summary>
        [PKLog(DoNotLogAtAll = true)] // TODO: Implement TimeInterval here.
        LogLevel
    }
}
