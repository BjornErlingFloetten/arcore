﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using ARCDoc;
using System.Linq;

namespace ARCAPI {

    [Class(
        Description =
            "Parses and stores a -" + nameof(PropertyStreamLine) + "-.\r\n" +
            "Example: https://yourserver.com/Add/dt/Customer/42/FirstName = John\r\n" +
            "\r\n" +
            "Sends to -" + nameof(DataStorage.ExternalReceiver) + "- if given.\r\n" +
            "Adds to the given -" + nameof(DataStorage) + "- unless -" + nameof(DataStorage.DoNotStoreInternally) + "- is specified.\r\n" +
            "\r\n" +
            "Bulk addition of data:\r\n" +
            "If HTTP POST is used and the parameter postData is given, then it will be treated as a collection of -" + nameof(PropertyStreamLine) + "-s, " +
            "to which the original request will be prepended for every line.\r\n" +
            "Example:\r\n" +
            "If request is 'Add/Customer/42' and postData is the following two lines\r\n" +
            "  FirstName = John\r\n" +
            "  LastName = Smith\r\n" +
            "then it will be treated as two separate requests like\r\n" +
            "  Add/Customer/42/FirstName = John\r\n" +
            "  Add/Customer/42/LastName = Smith\r\n" +
            "\r\n" +
            "TODO: Support use of Query for adding values to multiple entities at once.\r\n" +
            "TODO: like 'Add/Customer/WHERE FirstName = John/NameEvaluation = NiceName'\r\n" +
            "\r\n" +
            "Hint: You can test input validation by using the inbuilt classes of -" + nameof(ARCDoc) + "-.\r\n" +
            "(assuming that the Apple and Orange classes are included in -" + nameof(IP.AllIPDerivedTypes) + "-.)\r\n" +
            "Example queries:\r\n" +
            "api/Add/Apple/1/Colour = Red   // Should succeed\r\n" +
            "api/Add/Apple/1/Colour = Redd  // Should fail\r\n" +
            "api/Add/Apple/1/Color = Red    // Does not fail since Apple class accepts any property (it inherits -" + nameof(PRich) + "-).\r\n" +
            "api/Add/Orange/1/Color = Red    // Sould fail since Orange accepts only Colour and Name (it inherits -" + nameof(PExact<TPropertyKeyEnum>) + "-).\r\n"
        )]
    public class AddController : BaseController {

        [ClassMember(Description =
            "Describes what data is allowed to change through a list of -" + nameof(Subscription) + "- instances.\r\n" +
            "\r\n" +
            "Not set (Null) by default, meaning that EVERYTHING may be changed\r\n" +
            "Set to 'new List<Subscription> { Subscription.Parse(\"-*\") }' if you want to disallow editing altogether.\r\n" +
            "\r\n" +
            "A typical setting could be the following list:\r\n" +
            "  '+" + nameof(PSPrefix.dt) + "/'\r\n" +
            "That is, only allowing changes to -" + nameof(PSPrefix.dt) + "-.\r\n" +
            "\r\n"
        )]
        public static List<Subscription>? AllowFilter { get; set; } = null;

        // NOTE / TODO: Due to bug (as of May 2020) in ClassMemberAttribute as used by documentation, do not add a ClassMemberAttribute here. Document with ClassAttribute instead
        public override ContentResult APIMethod(
            ResponseFormat format,
            int jsonDepth,
            List<IKCoded> request,
            string? postData,
            DataStorage dataStorage
        ) {
            var propertyStreamLine = request.Count == 0 ? "" : string.Join("/", request.Take(request.Count - 1).Select(ik => ik.Encoded));

            if (request.Count > 0) {
                /// HACK: Property stream line parser expects " = " as separator between key part and value part.
                /// HACK: TODO: This must be compared against special treatment of space in <see cref="BaseController.CatchAll"/>
                /// HACK: TODO: Solution is suboptimal.
                propertyStreamLine +=
                    (!"".Equals(propertyStreamLine) ? "/" : "") +
                    request[^1].Encoded.Replace("0x0020=0x0020", " = ");
            }

            var actionsTaken = new List<string>();
            var returner = new Func<ContentResult>(() => {
                // TODO: Create standardized mechanism for returning API results like in AgoRapide 2017
                return format switch {
                    ResponseFormat.HTML => new ContentResult(
                        statusCode: System.Net.HttpStatusCode.OK, // This may lie actually...
                        contentType: "text/html",
                        content: UtilDoc.GenerateHTMLPage(
                            new Func<string>(() => { /// Insert link 'backwards' for querying through <see cref="RQController"/>
                                var retval = new StringBuilder();
                                var toRQController =
                                    string.Join("/", Enumerable.Repeat("..", request.Count)) +
                                    (request.Count == 0 ? "" : "/") +
                                    "RQ/"; /// NOTE: Note how we assume that routing RQ is used

                                retval.Append("<p>");
                                retval.Append("<a href=\"" +
                                    toRQController +
                                    "\">[ROOT]</a>"
                                );
                                for (var i = 0; i < request.Count; i++) {
                                    // Link to all levels in hierarchical structure
                                    retval.Append(
                                        " / " +
                                        "<a href=\"" +
                                            toRQController +
                                            string.Join("/", request.Take(i).Select(s => s.Encoded)) +
                                            (i == 0 ? "" : "/") +
                                            (i < request.Count - 1 ?
                                                request[i].Encoded :
                                                request[i].Encoded.Split("0x0020=0x0020")[0] // Last item also includes the actual value
                                            ) + "\">" +
                                            System.Net.WebUtility.HtmlEncode(i < request.Count - 1 ?
                                                request[i].Unencoded.ToString() :
                                                request[i].Unencoded.ToString().Split("0x0020=0x0020")[0] // Last item also includes the actual value
                                            ) + "</a>"
                                    );
                                }
                                retval.Append("</p>\r\n");

                                // Repeat the actual value being set
                                retval.Append("<p>" + System.Net.WebUtility.HtmlEncode(request[^1].Unencoded.ToString()) + "</p>\r\n");

                                // An last, show actions taken (insert links also in case there are helpful references here)
                                var strActionsTaken = string.Join("", actionsTaken.Select(a => "<p>" + System.Net.WebUtility.HtmlEncode(a) + "</p>\r\n"));
                                if (dataStorage.DocLinks != null) {
                                    strActionsTaken = dataStorage.DocLinks.InsertLinks(
                                        contentLocation: new List<IKCoded>(), // HACK: See below for how we instead modify every link (since link insertion assumes same Controller is used)
                                        contentHTML: strActionsTaken
                                    );
                                }
                                strActionsTaken = strActionsTaken.Replace("<a href=\"", "<a href=\"" + toRQController);
                                retval.Append(strActionsTaken);
                                return retval.ToString();
                            })()
                        )
                    ),
                    ResponseFormat.JSON => throw new NotImplementedException("JSON"),
                    _ => throw new InvalidEnumException(format)
                };
            });

            if (string.IsNullOrEmpty(propertyStreamLine)) {
                actionsTaken.Add("ERROR: Empty -" + nameof(PropertyStreamLine) + "-.");
                return returner();
            }

            if (AllowFilter != null && !Subscription.IsMatch(AllowFilter, propertyStreamLine)) { // TODO: Create IsMatch-overload which can receive IKCoded instead or similar in order to not parse multiple times.
                actionsTaken.Add("ERROR: Not allowed according to -" + nameof(AllowFilter) + "-.");
                return returner();
            }

            if (!PropertyStreamLineParsed.TryParse(propertyStreamLine, out var propertyStreamLineParsed, out var errorResponse)) {
                actionsTaken.Add("ERROR: Parsing failed. " + nameof(errorResponse) + ": " + errorResponse); /// NOTE: As of Jun 2020 <see cref="PropertyStreamLineParsed.TryParse"/> Has very few fail-scenarios (that is will almost always return TRUE)
                return returner();
            }
            actionsTaken.Add("OK: Parsed, " + propertyStreamLineParsed.Keys.Count + " keys");

            if (dataStorage.DoNotStoreInternally && dataStorage.ExternalReceiver == null) {
                actionsTaken.Add("ERROR: Meaningless to have both dataStorage.DoNotStoreInternally and dataStorage.ExternalReceiver==null");
                return returner();
            }

            if (postData == null) {
                // HTTP GET or HTTP POST with no postData
                // Example:
                //   api/Add/Customer/42/FirstName = John
                if (dataStorage.ExternalReceiver != null) {
                    // Note how this is independent of parsing just done above
                    dataStorage.ExternalReceiver(propertyStreamLine);
                    actionsTaken.Add("OK: Sent to " + nameof(dataStorage.ExternalReceiver));
                }

                if (!dataStorage.DoNotStoreInternally) {
                    try {
                        dataStorage.Lock.EnterWriteLock();
                        if (!PropertyStreamLine.TryStore(dataStorage.Storage, propertyStreamLineParsed, out errorResponse)) {
                            actionsTaken.Add("ERROR: " + nameof(PropertyStreamLine.TryStore) + " locally failed with " + nameof(errorResponse) + ": " + errorResponse);
                        } else {
                            actionsTaken.Add("OK: Stored locally");
                        }
                    } finally {
                        dataStorage.Lock.ExitWriteLock();
                    }
                }
            } else {
                // HTTP POST with postData. Parse every line and append to request
                // Example, for URL
                //    api/Add/Customer/42
                // and postData 
                //   FirstName = John
                //   LastName = Smith
                // the following will be stored:
                //   Customer/42/FirstName = John
                //   Customer/42/LastName = Smith

                var t = postData.Split("\r\n").Where(s => s.Trim() != "").ToList();
                var tParsed = new List<PropertyStreamLineParsed>();
                for (var i = 0; i < t.Count; i++) {
                    if (!PropertyStreamLineParsed.TryParse(propertyStreamLine +
                        (t[i].StartsWith(" = ") ?
                            "" : // URL was like 'Customer/42/FirstName' and postData like ' = John'
                            "/"  // More "normal" whole-post editing, URL was like 'Customer/42' and postData like 'FirstName = John', 'LastName = Smith' and so son
                        ) +
                        t[i], out propertyStreamLineParsed, out errorResponse)) { /// NOTE: As of Jun 2020 <see cref="PropertyStreamLineParsed.TryParse"/> Has very few fail-scenarios (that is will almost always return TRUE)
                        actionsTaken.Add(
                            "ERROR: Parsing failed for non-empty line no " + (i + 1) + " in postData.\r\n" +
                            nameof(errorResponse) + ": " + errorResponse);
                        return returner();
                    }
                    tParsed.Add(propertyStreamLineParsed);
                }
                if (t.Count != tParsed.Count) throw new InvalidCountException(t.Count, tParsed.Count, "t.Count!=tParsed.Count");
                if (dataStorage.ExternalReceiver != null) {
                    // Note how this is independent of parsing just done above
                    for (var i = 0; i < t.Count; i++) {
                        dataStorage.ExternalReceiver(propertyStreamLine + "/" + t[i]);
                    }
                    actionsTaken.Add("OK: Sent " + t.Count + " " + nameof(propertyStreamLine) + "s to " + nameof(dataStorage.ExternalReceiver));
                }

                if (!dataStorage.DoNotStoreInternally) {
                    try {
                        dataStorage.Lock.EnterWriteLock();
                        var okCount = 0;
                        for (var i = 0; i < tParsed.Count; i++) {
                            if (!PropertyStreamLine.TryStore(dataStorage.Storage, tParsed[i], out errorResponse)) {
                                actionsTaken.Add("ERROR: " + nameof(PropertyStreamLine.TryStore) + " locally failed for non-empty line no " + (i + 1) + " in postData with " + nameof(errorResponse) + ": " + errorResponse);
                            } else {
                                okCount++;
                            }
                        }
                        if (tParsed.Count > 0 && okCount == 0) {
                            actionsTaken.Add("ERROR: " + nameof(PropertyStreamLine.TryStore) + " locally failed for all " + okCount + " " + nameof(propertyStreamLine) + "s");
                        }
                        if (okCount != tParsed.Count) {
                            actionsTaken.Add("PARTIALLY OK: Stored locally " + okCount + " " + nameof(propertyStreamLine) + "s out of total " + tParsed.Count);
                        } else {
                            actionsTaken.Add("OK: Stored locally all " + okCount + " " + nameof(propertyStreamLine) + "s");
                        }
                    } finally {
                        dataStorage.Lock.ExitWriteLock();
                    }
                }
            }

            return returner();

        }

        public class AddControllerException : ApplicationException {
            public AddControllerException(string message) : base(message) { }
            public AddControllerException(string message, Exception inner) : base(message, inner) { }
        }
    }
}