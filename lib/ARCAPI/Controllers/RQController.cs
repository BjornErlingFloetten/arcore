﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Net;
using ARCCore;
using ARCDoc;
using ARCQuery;

namespace ARCAPI
{

    [Class(Description =
        "RQ = REST Query. Executes a REST like query request against the given -" + nameof(DataStorage) + "-.\r\n" +
        "Example: https://yourserver.com/RQ/dt/Customer/42 \r\n" +
        "\r\n" +
        "This class understands two main query-methods, " +
        "\r\n" +
        "1) Direct key-based into data storage, and\r\n" +
        "\r\n" +
        "2) -" + nameof(QueryExpression) + "- based.\r\n" +
        "\r\n" +
        "Examples of 1) Direct key-based into data storage queries:\r\n" +
        "'Customer/42' => Get all the keys for Customer with id 42, but do not get sub-keys.\r\n" +
        "'Customer/42/*' => Get all the keys for Customer with id 42, and also all underlying sub-keys (and their sub-keys and so on).\r\n" +
        "Append query with '.json' in order to get -" + nameof(ResponseFormat.JSON) + "- (default is -" + nameof(ResponseFormat.HTML) + "-) .\r\n" +
        "For -" + nameof(ResponseFormat.HTML) + "- this controller is able to do -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "-, " +
        "presumed that -" + nameof(DataStorage.DocLinks) + "- is set.\r\n" +
        "\r\n" +
        "Examples of 2) -" + nameof(QueryExpression) + "- based queries.\r\n" +
        "'Customer/WHERE FirstName = John/SELECT FirstName, LastName/SKIP 10/TAKE 10'\r\n" +
        "(see -" + nameof(QueryExpression) + "- for more examples).\r\n" +
        "\r\n"
    )]
    public class RQController : BaseController
    {

        // NOTE / TODO: Due to bug (as of May 2020) in ClassMemberAttribute as used by documentation, do not add a ClassMemberAttribute here. Document with ClassAttribute instead-
        public override ContentResult APIMethod(
            ResponseFormat format,
            int jsonDepth,
            List<IKCoded> request,
            string? postData,
            DataStorage dataStorage
        )
        {
            // NOTE: JSON depth and returnFlattenedHTML is somewhat corresponding concepts
            // NOTE: and could have been be treated in a more coherent manner.
            // NOTE: (like having .html{depth} like we have .json{depth} now).
            var returnFlattenedHTML = request.Count > 0 && "*".Equals(request[^1].Unencoded.ToString());

            if (format == ResponseFormat.JSON)
            {
                // Note how JSON is treated in a much simpler manner than HTML 
                try
                {
                    dataStorage.Lock.EnterReadLock();
                    return
                        TryNavigateToLevelInDataStorage(dataStorage, request, controller: this, out var tuple, out var errorResponseHTML) ?
                            ContentResult.CreateJSONOK(jsonDepth, tuple.ip) :
                            ContentResult.CreateError(format, errorResponseHTML);
                }
                finally
                {
                    dataStorage.Lock.ExitReadLock();
                }
            }

            if (format == ResponseFormat.HTML)
            {
                var htmlResponse = new Func<(List<IKCoded> linkContext, bool isSuccessfulQueryProgress, Type? resultType, string html)>(() =>
                {
                    try
                    {
                        dataStorage.Lock.EnterReadLock();

                        if (!TryNavigateToLevelInDataStorage(dataStorage, request, controller: this, out var tuple, out var errorResponseHTML))
                        {
                            /// TODO: Create more elaborate error pages
                            /// Note that this is somewhat confusing, error response returned now will be sent to <see cref="ContentResult.CreateHTMLOK"/>
                            return (new List<IKCoded>(), false, null, errorResponseHTML);
                        }
                        var pointer = tuple.ip;
                        var linkContext = tuple.linkContext ?? new List<IKCoded>();

                        return (
                                linkContext,

                                // If no terminate reason we assume query to be successful.
                                pointer is QueryProgress && !pointer.ContainsKey(PK.FromEnum(QueryProgressP.TerminateReason)),

                                pointer.GetType(),

                                (
                                    returnFlattenedHTML ?
                                        /// Extension method belongs here: <see cref="ARCDoc.Extensions.ToHTMLSimpleSingle"/>
                                        pointer.ToHTMLSimpleSingleRecursive(prepareForLinkInsertion: dataStorage.DocLinks != null, linkContext) :

                                        /// Extension method belongs here: <see cref="ARCDoc.Extensions.ToHTMLSimpleSingle"/>
                                        /// NOTE: Extension method uses reflection in order to call actually implemented method in class if present.
                                        pointer.ToHTMLSimpleSingle(prepareForLinkInsertion: dataStorage.DocLinks != null, linkContext)
                                ) +
                                GetTimestampIsOldWarningHTML(dataStorage.Storage)
                             );
                    }
                    finally
                    {
                        dataStorage.Lock.ExitReadLock();
                    }
                })();

                /// We are now outside of any locking context. 
                /// (the main reason for operating without locking now is that <see cref="DocLinkCollection.InsertLinks"/> is hugely expensive,
                /// but in general as much code as possible should of course be executed outside of a locking context)
                /// 
                // Any code following from here should NOT depend on data structures that may change.
                // (therefore any evaluations on changing datastructures have been prepared already in 'htmlResponse')

                var retval = htmlResponse.html;

                // Insert links if relevant
                if (dataStorage.DocLinks != null)
                {
                    if (htmlResponse.isSuccessfulQueryProgress)
                    {
                        // Contains a successful query result

                        // This is repetitive information, usually presented in a table format, possible in huge quantities.
                        // Insertion of links will therefore take some time.
                        // We do therefore assume that links are not desired now.
                        // TODO: Add a QueryExpressionLink-mechanism for communicating whether links are desired after all
                        // TODO: (default should be LINK OFF)
                    }
                    else if (false)
                    {
                        // TODO: We could also consider looking at length of retval
                        // TODO: and not insert link if returned document is huge.
                    }
                    else
                    {
                        // TODO: Consider using overload using "toDoc" parameter instead (just assume /RQ/doc)
                        retval = dataStorage.DocLinks.InsertLinks(
                            contentLocation: htmlResponse.linkContext.
                            /// TODO: It is unclear why code below work ('Count - 1'). But note that index.html or empty string was removed if first item in <param name="request"/>
                            /// TODO: so that explains 'Count == 0' part.
                            Take(htmlResponse.linkContext.Count == 0 ? 0 : (htmlResponse.linkContext.Count - 1)).
                            ToList(),
                            contentHTML: htmlResponse.html
                        );
                    }
                }

                // Insert link suggestions to the related entity types if relevant
                if (
                    htmlResponse.linkContext.Count >= 2 &&  // We assume query in the form 'api/RQ/Customer/42'

                    // TODO: This will break down when we introduce 'Query' in linkContext
                    // TODO: NOTE: If query here is a collection of objects, offer a 
                    // TODO: 'REL / JOIN' link instead.
                    htmlResponse.resultType != null &&
                    ForeignKey.IPRelationsKeysPointingTo.TryGetPV<List<ForeignKey.EntityTypeAndKey>>(htmlResponse.resultType.ToStringVeryShort(), out var relations)
                )
                {                     
                    var thisKeyURLEndcoded = "'" + htmlResponse.linkContext[^1].Encoded + "'";
                    retval =
                        (
                            /// TODO: IRegHandlers (in <see cref="ARComponents.ARCEvent"/>) will show up here
                            /// TODO: but will not be reachable by the URL used here 
                            /// TODO: if they are in the <see cref="ARConcepts.TransientEventTree"/>
                            "<p>See related " +
                                string.Join(" ", relations.Select(r =>  // Create something like '../Order/WHERE CustomerId = 42'
                                    "<a href=\"../" +
                                    WebUtility.UrlEncode(r.EntityType.ToStringVeryShort()) + "/" +
                                    "WHERE " +
                                        WebUtility.UrlEncode(r.Key.ToString()) + " = " +
                                        thisKeyURLEndcoded +
                                    "\">" + WebUtility.HtmlEncode(r.OppositeTerm) +
                                    "</a>")
                                ) +
                            "</p>\r\n"
                        ) +
                        retval;
                }

                /// Insert Edit-link if deemed appropriate (linking to <see cref="EditController"/>)
                if (
                    !returnFlattenedHTML && // NOTE: This is an arbitrary decision. The flattened HTML format use only a representation, editing is eminently possible
                    request.Count > 0 && // Do not allow edit at root-level. NOTE: This is a somewhat arbitrary decision. NOTE: If you want to allow, code below must be tweaked
                    !(typeof(QueryProgress).IsAssignableFrom(htmlResponse.resultType)) && // Irrelevant to 'Edit'
                    !(typeof(HTOC).IsAssignableFrom(htmlResponse.resultType)) // Irrelevant to 'Edit'

                // TODO: Create generic system for deciding whether 'Edit' is relevant or not

                // NOTE: Count limit removed 01 Jan 2022. We could include it in htmlResponse
                // NOTE: but it would mean unnecessary iteration over query result for instance
                // NOTE: because it would be evaluated every tie.
                // NOTE: (and of course we can not evaluate it here, because we are outside of locking context)
                //// NOTE: This is an arbitrary decision. We want to avoid editing of huge datasets,
                //// NOTE: but the recursive aspect means that the dataset may be huge anyway
                //// NOTE: even if there is less than 1000 elements at starting level.
                //// NOTE: Note that we do only remove a suggestion, the user is still free to edit the resulting dataset)
                //htmlResponse.Count < 1000
                )
                {
                    // Insert edit link
                    retval += (
                        "<p>" +
                        "<a href=\"" +
                            string.Join("/", Enumerable.Repeat("..", request.Count - 1)) +
                            (request.Count == 1 ? "" : "/") +
                            "../" +
                            "Edit/" + /// NOTE: Note how we assume that routing Edit is used
                                                            string.Join("/", request.Select(s => s.Encoded)) +
                        "\">Edit</a></p>\r\n"
                    );
                }

                return ContentResult.CreateHTMLOK(UtilDoc.GenerateHTMLPage(retval));
            }
            throw new InvalidEnumException(format);
        }

        public class RQControllerException : ApplicationException
        {
            public RQControllerException(string message) : base(message) { }
            public RQControllerException(string message, Exception inner) : base(message, inner) { }
        }
    }
}