﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using ARCDoc;
using System.Linq;

namespace ARCAPI {

    [Class(Description =
        "Offers a simple HTML editing environment of the raw -" + nameof(ARConcepts.PropertyStream) + "- from the given key.\r\n" +
        "Example: https://yourserver.com/Edit/dt/Customer/42\r\n" +
        "\r\n" +
        "The HTML form offered will POST directly to -" + nameof(AddController) + "- with the postData-parameter set.\r\n" +
        "\r\n" +
        "TODO: Note that -" + nameof(ResponseFormat.JSON) + "- is currently not supported.\r\n" +
        "TODO: It might appear to be irrelevant, but it could contain the basic elements for building an HTML editing environment,\r\n" +
        "TODO: enabling to client to construct its own HTML page."
    )]
    public class EditController : BaseController {
        public override ContentResult APIMethod(
            ResponseFormat format,
            int jsonDepth,
            List<IKCoded> request,
            string? postData,
            DataStorage dataStorage
        ) {
            try {
                dataStorage.Lock.EnterReadLock();
                return format switch
                {
                    ResponseFormat.HTML => new ContentResult(
                        statusCode: System.Net.HttpStatusCode.OK, // This may lie actually...
                        contentType: "text/html",
                        content: UtilDoc.GenerateHTMLPage(
                            new Func<string>(() => { /// Insert link 'backwards' for querying through <see cref="RQController"/>
                                var retval = new StringBuilder();
                                retval.Append("<p>");
                                for (var i = 0; i < request.Count; i++) {
                                    // Link to all levels in hierarchical structure
                                    retval.Append(
                                        " / " +
                                        "<a href=\"" +
                                            string.Join("/", Enumerable.Repeat("..", request.Count - i - 1)) +
                                             // "/" +
                                             (i == request.Count - 1 ? "" : "/") +
                                            request[i].Encoded + "\">" +
                                            System.Net.WebUtility.HtmlEncode(request[i].Unencoded.ToString()) + "</a>"
                                    );
                                }

                                retval.Append("</p>\r\n");
                                // Repeat the actual value being set
                                retval.Append("<p>" + (request.Count == 0 ? "[ROOT]" : System.Net.WebUtility.HtmlEncode(request[^1].Unencoded.ToString())) + "</p>\r\n");

                                if (!TryNavigateToLevelInDataStorage(dataStorage, request, controller: this, out var tuple, out var errorResponseHTML)) {
                                    /// TODO: Create more elaborate error pages
                                    /// Note that this is somewhat confusing, error response returned now will be sent to <see cref="ContentResult.CreateHTMLOK"/>
                                    retval.Append(errorResponseHTML);
                                    // Give up
                                    return retval.ToString();
                                }

                                new List<(string Information, int Rows, int RecursiveDepth)> {
                                    ("Edit keys at this level only", Rows: 8, RecursiveDepth: 1),
                                    ("Edit keys at all sub levels.<br>\r\nNOTE: Edits are always additive. Use '" + nameof(PP.Invalid) + "' in order to delete keys.", Rows: 14, RecursiveDepth: int.MaxValue),
                                }.ForEach(t => {
                                    var propertyStream = string.Join("\r\n", tuple.ip.ToPropertyStream(recurseDepth: t.RecursiveDepth));
                                    if (propertyStream.Length > 100000) {
                                        retval.Append(
                                            "<p><b>" + t.Information + "</b></p>\r\n" +
                                            "<p>Too many keys, unable to offer editing textbox in HTML</p>\r\n" +
                                            "<p>Total size of property stream for editing is " + propertyStream.Length + " characters while limit is 100 000 characters</p>\r\n" +
                                            "<p>Possible resolution: Try to edit at a sub level (sub key) instead</p>"
                                        );
                                    } else {
                                        retval.Append(
                                            "<p><b>" + t.Information + "</b></p>\r\n" +
                                            "<form " +
                                            "method=\"post\", " +
                                            "enctype = \"application/x-www-form-urlencoded\", " +
                                            "accept-charset=\"utf-8\", " +
                                            "action=\"" + (
                                                string.Join("/", Enumerable.Repeat("..", request.Count)) +
                                                (request.Count == 0 ? "" : "/") +
                                                /// NOTE: Note how we assume that routing Add is used
                                                "Add/" +
                                                string.Join("/", request.Select(s => s.Encoded))
                                            ) +
                                            "\">\r\n" +
                                            "<textarea name=\"postData\", cols=\"90\", rows=\"" + t.Rows + "\">" +
                                            System.Net.WebUtility.HtmlEncode(propertyStream) +
                                            "</textarea>" +
                                            "<br>\r\n" +
                                            "<br>\r\n" +
                                            "<input type=\"submit\" value=\"Submit\">\r\n" +
                                            "</form>\r\n"
                                        );
                                    }
                                });

                                // Add links for editing at keys at this level (editing one level down)
                                var t = tuple.ip.OrderBy(ikip => ikip.Key.ToString()).ToList();
                                if (t.Count > 0) {
                                    retval.Append(
                                        "<p><b>Edit single sub keys only</b>: " +
                                        (t.Count > 1000 ? ("NOTE: Only first 1000 sub keys out of a total of " + t.Count + " shown") : "") +
                                        string.Join(" / ", t.Take(1000).Select(ikip =>
                                            "<a href=\"" +
                                            ((request.Count == 0 || "".Equals(request[^1])) ? "" : (request[^1].Encoded + "/")) +
                                            System.Net.WebUtility.UrlEncode(ikip.Key.ToString()) +
                                            "\">" +
                                            System.Net.WebUtility.HtmlEncode(ikip.Key.ToString()) + "</a>"
                                        )) +
                                        "</p>"
                                    );
                                }
                                return retval.ToString();
                            })()
                        )
                    ),
                    ResponseFormat.JSON => throw new NotImplementedException("JSON"),
                    _ => throw new InvalidEnumException(format)
                };
            } finally {
                dataStorage.Lock.ExitReadLock();
            }
        }
    }
}
