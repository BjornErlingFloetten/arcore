﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCAPI {
    [Class(Description = 
        "Offers GraphQL queries.\r\n" +
        "\r\n" +
        "Not implemented as of Dec 2020.\r\n"
    )]
    public class GQController : BaseController {
        // NOTE / TODO: Due to bug (as of May 2020) in ClassMemberAttribute as used by documentation, do not add a ClassMemberAttribute here. Document with ClassAttribute instead-
        public override ContentResult APIMethod(
            ResponseFormat format,
            int jsonDepth,
            List<IKCoded> request,
            string? postData,
            DataStorage dataStorage
        ) {
            try {
                dataStorage.Lock.EnterReadLock();
                return ContentResult.CreateError(format, "<p>Not implemented</p>");
            } finally {
                dataStorage.Lock.ExitReadLock();
            }
        }
    }
}
