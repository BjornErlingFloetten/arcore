﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using ARCDoc;
using System.Linq;

namespace ARCAPI {

    [Class(Description =
        "Mimicks Microsoft.AspNetCore.Mvc.ContentResult.\r\n" +
        "\r\n" +
        "Note that although -" + nameof(ARComponents.ARCAPI) + "- is supposed to work with a Microsoft.AspNetCore.Mvc application, " +
        "it does not link to the Microsoft.AspNetCore.Mvc library itself.\r\n" +
        "Therefore the returned instance from controller methods like " +
        "-" + nameof(RQController.APIMethod) + "- or -" + nameof(GQController.APIMethod) + "-) " +
        "is not an instance of Microsoft.AspNetCore.Mvc.ContentResult (or Microsoft.AspNetCore.Mvc.ActionResult) " +
        "but rather an instance of this class, AgoRapide -" + nameof(ContentResult) + "-.\r\n" +
        "\r\n" +
        "This class reflects the components needed to construct a Microsoft.AspNetCore.Mvc.ContentResult instance, namely \r\n" +
        "'(string Content, string ContentType, System.Net.HttpStatusCode StatusCode)'.\r\n" +
        "\r\n" +
        "This class is immutable.\r\n"
    )]
    public class ContentResult {
        public System.Net.HttpStatusCode StatusCode { get; private set; }
        public string ContentType { get; private set; }
        public string Content { get; private set; }

        public ContentResult(System.Net.HttpStatusCode statusCode, string contentType, string content) {
            StatusCode = statusCode;
            ContentType = contentType;
            Content = content;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="completeHTMLPage">
        /// NOTE: This parameter must be a complete HTML page<br>
        /// HINT: Use <see cref="Documentator.GenerateHTMLPage"/> in order to create this parameter.
        /// </param>
        /// <returns></returns>
        [ClassMember(Description =
            "NOTE: Parameter must be a complete HTML page.\r\n" +
            "HINT: Use -" + nameof(UtilDoc.GenerateHTMLPage) + "- in order to create parameter."
        )]
        public static ContentResult CreateHTMLOK(string completeHTMLPage) => new ContentResult(
            System.Net.HttpStatusCode.OK,
            contentType: "text/html",
            content: completeHTMLPage
        );

        public static ContentResult CreateJSONOK(int depth, IP ip) => new ContentResult(
            System.Net.HttpStatusCode.OK,
            contentType: "text/json",
            System.Text.Json.JsonSerializer.Serialize(new Dictionary<string, object> {
                { "status_code", "ok" },
                { "data", ip is ARCQuery.QueryProgress q ? 
                    /// TODO: <see cref="ARCQuery.QueryProgress.ToJSONSerializeable"/> is not called as expected
                    /// TODO: unless this explicit cast is done.
                    /// TODO: Is this a bug in '.NET'? "The class always wins" should apply in these situations, 
                    /// TODO: (explicit implemented method should be called anyway)
                    /// TODO: Search for TypeLoadException for similar problems in AgoRapide.
                    /// TODO: (Hint: Is it related to classes being located in different assemblies?)
                    q.ToJSONSerializeable(depthRemaining: depth) :
                    ip.ToJSONSerializeable(depthRemaining: depth)
                }
            })
        );

        /// <summary>
        /// Note how error messages are created by a single method regardless of <see cref="ResponseFormat"/> 
        /// while there individual methods for ordinary "OK" responses.
        /// </summary>
        /// <param name="format"></param>
        /// <param name="errorMessageHTML">
        /// Only the actual error message, to be encapsulated into a complete HTML page by this method through <see cref="Documentator.GenerateHTMLPage"/><br>
        /// Note how the error message is supposed to be in HTML format, even if format is not <see cref="ResponseFormat.HTML"/>.<br>
        /// This does in effect prioritize HTML responses, since they will be easier to read.<br>
        /// (the error message is returned for other formats too, but will be correspondingly harder to read).<br>
        /// </param>
        /// <returns></returns>
        [ClassMember(Description = "TODO: Add a key / value format for error messages, like done for -" + nameof(IP.Log) + "-.")]
        public static ContentResult CreateError(ResponseFormat format, string errorMessageHTML) => (format) switch
        {
            ResponseFormat.JSON => new ContentResult(
                    System.Net.HttpStatusCode.OK, // OK is always used since we do not know which detailed error code to use. TODO: Improve on this.
                    contentType: "text/json",
                    System.Text.Json.JsonSerializer.Serialize(new Dictionary<string, string> {
                        { "status_code", "error" },
                        { "error_message", errorMessageHTML  },
                    })
               ),
            ResponseFormat.HTML =>
                new ContentResult(
                    System.Net.HttpStatusCode.OK, // OK is used in order for browser not to hide the detailed error message
                    contentType: "text/html",
                    content: UtilDoc.GenerateHTMLPage(errorMessageHTML)
                ),
            _ => throw new InvalidEnumException(format)
        };
    }
}
