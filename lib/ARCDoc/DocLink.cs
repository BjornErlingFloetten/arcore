﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

/// <summary>
/// Contains:
/// <see cref="ARCDoc.DocLink"/>
/// <see cref="ARCDoc.DocLinkP"/>
/// <see cref="ARCDoc.DocLinkCollection"/>
/// </summary>
namespace ARCDoc
{

    [Class(Description =
        "Describes to where a single HTML link points.\r\n" +
        "\r\n" +
        "By link we mean a link according to -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "-, " +
        "that is words in an HTML-document like -MyCustomerClass-.\r\n" +
        "\r\n" +
        "This class works with HTML, like -" + nameof(LinkWordHTML) + "- being encoded in HTML,  " +
        "so for instance a link to a generic class like -MyCustomerClass<T>- " +
        "actually looks like -MyCustomerClass&lt;T&gt;-.\r\n" +
        "\r\n" +
        "Used as input to -" + nameof(DocLinkCollection.InsertLinks) + "- " +
        "(telling it what to look for, and how to replace the text with a corresponding '<a href = ...' tag)\r\n" +
        "\r\n" +
        "Note how there is almost a one-to-one match between -" + nameof(HLocation) + "- and -" + nameof(DocLink) + "- " +
        "with the latter pointing to a corresponding instance of the former. " +
        "It is only when we have duplicate filenames that -" + nameof(DocLink) + "- will instead " +
        "point to a dedicated -" + nameof(DocLinkResolution) + "- page for that filename.\r\n" +
        "\r\n" +
        "Note that out of performance needs this class has a duplicate storage of properties.\r\n" +
        "(Ordinary -" + nameof(IP) + "- for debugging / querying needs, and traditional C# properties for higher performance).\r\n" +
        "This class should therefore not be changed after initialization (nor is it any need to), as it can easily lead to out-of-sync issues.\r\n" +
        "\r\n" +
        "NOTE: / TODO: One might argue for leaving out -" + nameof(LinkWordHTMLBeforeLt) + "-\r\n" +
        "NOTE: / TODO: and instead use TWO instances of -" + nameof(DocLink) + "- when '&lt;' is encountered when creating instances of this class.\r\n" +
        "NOTE: / TODO: There are both pros and cons for this approach.\r\n"
    )]
    public class DocLink : PRich
    {
        [ClassMember(Description = "Copy of -" + nameof(DocLinkP.Location) + "-.")]
        public List<IKCoded> Location { get; private set; }

        [ClassMember(Description = "Copy of -" + nameof(DocLinkP.LinkWordHTML) + "-.")]
        public IKCoded Filename { get; private set; }

        [ClassMember(Description = "Copy of -" + nameof(DocLinkP.LinkWordHTML) + "-.")]
        public string LinkWordHTML { get; private set; }

        [ClassMember(Description = "-" + nameof(LinkWordHTML) + "- with '-' (minus sign / hyphen) prepended and appended.")]
        public string LinkWordHTMLWithHyphens { get; private set; }

        [ClassMember(Description =
            "If -" + nameof(LinkWordHTML) + " contains '<' (really '&lt;') " +
            "(for generic types, like 'PValue<TValue>') " +
            "then this will contain the part before '<' like 'PValue'.\r\n" +
            "\r\n" +
            "This is due to use 'nameof' for generic classes like 'nameof(PValue<TValue>)' will result in " +
            "only the name of the base type, ending up with '-PValue-' instead of '-PValue&lt;TValue&gt;-'" +
            "\r\n" +
            "Therefore, we must create links also for the part before '&lt;'."
        )]
        public string? LinkWordHTMLBeforeLt { get; private set; } = null;

        [ClassMember(Description = "-" + nameof(LinkWordHTMLBeforeLt) + "- with '-' (minus sign / hyphen) prepended and appended.")]
        public string? LinkWordHTMLBeforeLtWithHyphens { get; private set; } = null;

        [ClassMember(Description = "'Filename.Encoded + \"> + LinkWordHTML + </a>'")]
        public string AHrefEndPart;

        public DocLink(List<IKCoded> location, IKCoded filename, string linkWordHTML)
        {
            Location = location;
            IP.AddPV(DocLinkP.Location, location);
            Filename = filename;
            IP.AddPV(DocLinkP.Filename, filename);
            LinkWordHTML = linkWordHTML;
            IP.AddPV(DocLinkP.LinkWordHTML, linkWordHTML);

            // Precalculate some more values in order to improve performance
            // We assume that memory performance will be insignificant, while speed performance will improve dramatically when inserting links
            LinkWordHTMLWithHyphens = "-" + LinkWordHTML + "-";

            var pos = LinkWordHTML.IndexOf("&lt;");
            if (pos > -1)
            {
                LinkWordHTMLBeforeLt = LinkWordHTML.Substring(0, pos);
                LinkWordHTMLBeforeLtWithHyphens = "-" + LinkWordHTMLBeforeLt + "-";
            }

            AHrefEndPart = Filename.Encoded + "\">" + LinkWordHTML + "</a>";
        }

        // TODO: Delete commented out code
        // Now need longer, now that we ARE an IP-instance
        //[ClassMember(Description =
        //    "Transforms class into -" + nameof(IP) + "-.\r\n" +
        //    "(NOTE / TODO: One could argue for having this class implementing IP originally would have been better).\r\n"
        //)]
        //public IP ToIP() {
        //    var retval = (IP)new PRich();
        //    retval.AddPV(nameof(Location), Location);
        //    retval.AddPV(nameof(Filename), Filename);
        //    retval.AddPV(nameof(LinkWordHTML), LinkWordHTML);
        //    return retval;
        //}
    }

    [Enum(
        Description = "Describes class -" + nameof(DocLink) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum DocLinkP
    {
        __invalid,

        [PKType(
            Description =
                "The location that this link points to.\r\n" +
                "This will either be directly to the source, or to doc/DocLinkResolution in case of ambigious links.",
            Type = typeof(IKCoded), Cardinality = Cardinality.WholeCollection, IsObligatory = true
        )]
        Location,

        [PKType(Type = typeof(IKCoded), IsObligatory = true)]
        Filename,

        [PKType(Description = "Actual link as it is expected to occur in text, like '-MyCustomerClass&lt;T&gt;-'.", IsObligatory = true)]
        LinkWordHTML,
    }

    [Class(Description =
        "Contains a collection of -" + nameof(DocLink) + "- objects.\r\n"
    )]
    public class DocLinkCollection : PRich
    {

        /// <summary>
        /// </summary>
        /// <param name="hLocations">
        /// </param>
        /// <param name="specialPrefixes">
        /// Should contain prefixes for which links are also desired for potential links without the prefix.
        /// See <see cref="ARComponents.ARCQuery"/> class QUtil for example of use.
        /// </param>
        /// <param name="docLinkResolutionLocation">
        /// If not given then <see cref="DefaultDocLinkResolutionLocation"/> will be used.
        /// </param>
        /// <returns></returns>
        [ClassMember(Description =
            "Notes all locations and their positions in the hierarchical directory structure. " +
            "\r\n" +
            "Creates one or more -" + nameof(DocLink) + "- for every -" + nameof(HLocation) + "- given.\r\n" +
            "\r\n" +
            "For instance, if sees a location called 'Assembly/ARCDoc/Class/Apple.html', " +
            "that is a file 'Apple.html' in location ''Assembly/ARCDoc/Class' " +
            "it will will create a potential link to that location for -Apple-.\r\n" +
            "\r\n" +
            "Only the first word in the filename will be used.\r\n" +
            "\r\n" +
            "The result is used by -" + nameof(InsertLinks) + "- " +
            "which whenever it sees -Apple- in the given HTML, will replace it with the corresponding link.\r\n" +
            "\r\n" +
            "Duplicate potential links are linked to a resolution page (See -" + nameof(DocLinkResolution) + "-).\r\n" +
            "So for instance if multiple files have -ARCDoc- as a potential link, then it will resolve to such a resolution page.\r\n" +
            "\r\n" +
            "The parameter specialPrefixes contains prefixes for which links are also desired for potential links without the prefix.\r\n" +
            "See -" + nameof(ARComponents.ARCQuery) + "-.-QUtil- for example of use.\r\n" +
            "\r\n" +
            "See also -" + nameof(Extensions.DescribeValue) + ", inner function prepareForLinkInsertionIfNoWhitespace.\r\n"
        )]
        public static (DocLinkCollection docLinks, DocLinkResolutionCollection docLinkResolutions) Create(
            HLocationCollection hLocations,
            List<IKCoded> docLinkResolutionLocation,
            List<string>? specialPrefixes = null
        )
        {
            // key is actual link word.
            // value is the possible filenames and their locations. Value 'filenameFirstWord' is used for target resolution
            var potentialLinksDic = new Dictionary<IKCoded, List<(IKCoded filename, IKCoded filenameFirstWord, List<IKCoded> location)>>();

            var getterFirstWord = new Func<IKCoded, IKCoded>(ikCodedFilename =>
            {
                // Wrong:
                // var filename = ikCodedFilename.Encoded.Split(' ')[0];
                // Correct:
                var filename = ikCodedFilename.Encoded.Split("0x0020")[0];
                var l = filename.ToLower();
                if (l.EndsWith(".htm")) return IKCoded.FromEncoded(filename[0..^".htm".Length]);
                if (l.EndsWith(".html")) return IKCoded.FromEncoded(filename[0..^".html".Length]);
                return IKCoded.FromEncoded(filename); // Probably OK as long as file has no extension (no last name)
            });

            var adder = new Action<IKCoded, IKCoded, List<IKCoded>>((linkWord, filename, location) =>
            {
                if (!potentialLinksDic.TryGetValue(linkWord, out var list))
                {
                    potentialLinksDic[linkWord] = list = new List<(IKCoded filename, IKCoded filenameWithoutHtml, List<IKCoded> location)>();
                }
                list.Add((
                    filename,
                    filenameFirstWord: getterFirstWord(filename),
                    location
                ));
            });
            hLocations.ForEach(ikip =>
            {
                if (!(ikip.P is HLocation hLocation)) return; /// Could have thrown exception here, but this allows for use of <see cref="DocumentatorP._Description"/> for instance within collection

                var linkWord = getterFirstWord(hLocation.Filename);

                adder(linkWord, hLocation.Filename, hLocation.Location);
                if (specialPrefixes != null)
                {
                    var strLinkword = linkWord.Unencoded.ToString();
                    specialPrefixes.ForEach(s =>
                    {
                        if (strLinkword.StartsWith(s) && strLinkword.Length > s.Length)
                        {
                            var stripped = strLinkword[s.Length..];
                            adder(IKCoded.FromUnencoded(IKString.FromString(stripped)), hLocation.Filename, hLocation.Location);
                            // In ARCQuery, suggestions are often given in capital letters.
                            // TODO: Offer in original PascalCase instead.
                            // TODO: Look for "string.Join(", ", NewKey.Parsers.Keys.OrderBy("
                            var toUpper = stripped.ToUpper();
                            if (toUpper != stripped) adder(IKCoded.FromUnencoded(IKString.FromString(toUpper)), hLocation.Filename, hLocation.Location);
                        }
                    });
                }
            });
            var duplicateKeys = potentialLinksDic.Where(e => e.Value.Count > 1).ToList(); // NOTE: If we had left out 'ToList' here then this would not evaluate until we want to modify collection, leading to bug (in other words, we must avoid deferred execution now)
            var docLinkResolutions = new DocLinkResolutionCollection();
            duplicateKeys.ForEach(e =>
            {

                // Duplicate, step 1) Replace value for this key with single pointer, to target resolution page:

                /// Note that although <see cref="ARComponents.ARCAPI"/> works quite well without .html-endings in links,
                /// the structures generated now should also be suitable for storing on disk, so we need .HTML in filenames.
                var filenameWithHTML = IKCoded.FromEncoded(e.Key.Encoded + ".html");

                potentialLinksDic[e.Key] = new List<(IKCoded filename, IKCoded filenamewithoutHTML, List<IKCoded> location)> {(
                    filename: filenameWithHTML,
                    filenamewithoutHTML: e.Key,
                    docLinkResolutionLocation
                )};

                // Duplicate, step 1) Create the DocLinkResolution that will list all links, 
                docLinkResolutions.IP.AddP(
                    e.Key.Encoded,
                    new DocLinkResolution(
                        linkWord: e.Key.Unencoded.ToString(),
                        docLinks: e.Value.Select(v => new DocLink(
                            location: v.location,
                            filename: v.filename,
                            linkWordHTML: System.Net.WebUtility.HtmlEncode(e.Key.Unencoded.ToString())
                        )).ToList()
                    )
                );
            });

            var docLinks = new DocLinkCollection();
            potentialLinksDic.ForEach(e =>
            {
                if (e.Value.Count != 1) throw new InvalidCountException(e.Value.Count, 1, "Expected duplicates to have been replaced with link to target resolution page. Key: " + e.Key);
                docLinks.IP.AddP(e.Key.Encoded, new DocLink(
                    location: e.Value[0].location,
                    filename: e.Value[0].filename,
                    linkWordHTML: System.Net.WebUtility.HtmlEncode(e.Key.Unencoded.ToString())
                ));
            });

            return (docLinks, docLinkResolutions);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentLocation">
        /// Used to compare HTML content's position with position of link target.
        /// 
        /// This parameter is necessary because links are always relative.
        /// 
        /// TODO: In principle <paramref name="contentLocation"/> and <paramref name="contentHTML"/> 
        /// TOOD: could be replaced by an instance of <see cref="HLocation"/>
        /// </param>
        /// <param name="contentHTML">
        /// The actual HTML content in which we are to insert links.
        /// 
        /// TODO: In principle <paramref name="contentLocation"/> and <paramref name="contentHTML"/> 
        /// TOOD: could be replaced by an instance of <see cref="HLocation"/>
        /// </param>
        /// <param name="potentialLinks"></param>
        /// <returns></returns>
        [ClassMember(Description =
            "Does -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "- on the given HTML content.\r\n" +
            "\r\n" +
            "Replaces all occurrences of any -[HTMLLink]- with corresponding '<a href = \"...' tag.\r\n" +
            "\r\n" +
            "NOTE: Only works when documentation is in the same root as the current location,\r\n" +
            "NOTE: typically when current location starts with RQ, like RQ/dt/Customer/42 and documentation is under RQ/doc/...\r\n" +
            "NOTE:\r\n" +
            "NOTE: See overload with 'toDoc' parameter for use when caller 'knows' how to navigate to -" + nameof(PSPrefix.doc) + "-.\r\n" +
            "\r\n" +
            "TODO: Possible easy improvement for better performance:\r\n" +
            "TODO: DocLinks could be grouped by first letters or similar\r\n" +
            "TODO: If hyphen + that letter do not exist, then we can skip that group altogether.\r\n" +
            "TDDO: This should greatly speed up insertion of links in small documents."
        )]
        public string InsertLinks(List<IKCoded> contentLocation, string contentHTML)
        {
            var retval = contentHTML;
            CreateIteratorBasedOnContent(contentHTML).ForEach(docLink =>
            {
                // if (!(ikip.P is DocLink docLink)) return; /// Could have thrown exception here, but this allows for use of <see cref="DocumentatorP._Description"/> for instance within collection

                // Precheck added 24 Mar 2021, improves performance by skipping some position / path calculation and string construction.
                // This is based on the assumption that most links will not be present in most documents.
                var check1 = retval.IndexOf(docLink.LinkWordHTMLWithHyphens) != -1;
                var check2 = docLink.LinkWordHTMLBeforeLt != null && (retval.IndexOf(docLink.LinkWordHTMLBeforeLtWithHyphens) != -1);
                if (!(check1 || check2)) return;

                var commonPosition = 0; // Find common part of path, necessary in order to decide relative links
                while (commonPosition < docLink.Location.Count && commonPosition < contentLocation.Count && docLink.Location[commonPosition] == contentLocation[commonPosition])
                {
                    commonPosition++;
                }
                var pathNavigationUp = string.Join("/", Enumerable.Repeat("..", contentLocation.Count - commonPosition)); // Move up until common position
                var pathNavigationDown = string.Join("/", docLink.Location.Skip(commonPosition).Select(l => l.Encoded)); // Move 'down' until reaches position of link word. 
                var ahrefPlusPath = "<a href=\"" +
                    pathNavigationUp +
                    ("".Equals(pathNavigationUp) || "".Equals(pathNavigationDown) ? "" : "/") +
                    pathNavigationDown +
                    ("".Equals(pathNavigationUp) && "".Equals(pathNavigationDown) ? "" : "/");

                if (check1) retval = retval.Replace(docLink.LinkWordHTMLWithHyphens, ahrefPlusPath + docLink.AHrefEndPart);
                if (check2) retval = retval.Replace(docLink.LinkWordHTMLBeforeLtWithHyphens, ahrefPlusPath + docLink.AHrefEndPart);
            });
            return retval;
        }

        /// <param name="toDoc">
        /// Typical value is "/RQ" when "doc" is residing under "RQ" like "/RQ/doc/..."
        /// </param>
        [ClassMember(Description =
            "See overload with 'contentLocation' as parameter for documentation.\r\n" +
            "\r\n" +
            "The toDoc parameter will typically look like \"/RQ\"/ when \"doc\" is residing under \"RQ\" like \"RQ/doc/...\".\r\n" +
            "\r\n"
        )]
        public string InsertLinks(string toDoc, string contentHTML)
        {
            var retval = contentHTML;
            CreateIteratorBasedOnContent(contentHTML).ForEach(docLink =>
            {
                // if (!(ikip.P is DocLink docLink)) return; /// Could have thrown exception here, but this allows for use of <see cref="DocumentatorP._Description"/> for instance within collection

                // Precheck added 24 Mar 2021, improves performance by skipping some position / path calculation and string construction.
                // This is based on the assumption that most links will not be present in most documents.
                var check1 = retval.IndexOf(docLink.LinkWordHTMLWithHyphens) != -1;
                var check2 = docLink.LinkWordHTMLBeforeLt != null && (retval.IndexOf(docLink.LinkWordHTMLBeforeLtWithHyphens) != -1);
                if (!(check1 || check2)) return;

                var pathNavigationUp = toDoc;
                var pathNavigationDown = string.Join("/", docLink.Location.Select(l => l.Encoded)); // Move 'down' until reaches position of link word. 
                var ahrefPlusPath = "<a href=\"" +
                    pathNavigationUp +
                    ("".Equals(pathNavigationUp) || "".Equals(pathNavigationDown) ? "" : "/") +
                    pathNavigationDown +
                    ("".Equals(pathNavigationUp) && "".Equals(pathNavigationDown) ? "" : "/");

                if (check1) retval = retval.Replace(docLink.LinkWordHTMLWithHyphens, ahrefPlusPath + docLink.AHrefEndPart);
                if (check2) retval = retval.Replace(docLink.LinkWordHTMLBeforeLtWithHyphens, ahrefPlusPath + docLink.AHrefEndPart);
            });
            return retval;
        }

        [ClassMember(Description =
            "Creates a 'smart' iterator that leaves out -" + nameof(DocLink) + "- fragments known not to exist in the HTML content.\r\n" +
            "\r\n" +
            "Works by first going through the HTML content, looking at first letter or letters of any letters following a hyphen (minus sign).\r\n" +
            "Combinations known not to exist, are then left out of the iterator returned.\r\n" +
            "Example: If '-a...' is not found in the document, then DocLinks starting with '-a...' are not returned from the iterator.\r\n" +
            "\r\n" +
            "Gives significantly improved performance, especially for small documents.\r\n" +
            "\r\n" +
            "Note: Any 'bug' here will have worst case consequence that some links are not inserted.\r\n"
        )]
        private IEnumerable<DocLink> CreateIteratorBasedOnContent(string contentHtml)
        {
            // NOTE: We only bother with checking for the first 256 character codes
            // 
            // NOTE: This is trivial to expand to 65536 combinations (first two letters instead of first letter)
            // NOTE: The cost of creating an array of 65536 elements would be
            // NOTE: trivial compared to iterating through unnecessary DocLink's.

            // TODO: Consider expanding to two letters, and also NOT to analyze document if it is "huge".
            // TODO: (because if "huge" then all first letter combinations are expected to be present anyway)
            var firstLetters = new bool[256];

            for (var i = 0; i < contentHtml.Length - 1; i++)
            {
                // Not how we do not disinguish between start hyphen or end hyphen here. 
                // The end hyphen will probably be followed by a space, punctuation mark, bracket or similar
                // which should have no consequence inserting into 'firstLetters' now because no link words
                // start with these characters anyway.
                // (and if this is not true, the only consequence will be a performance one, not wrong insertion of links)
                if (contentHtml[i] != '-') continue; 
                var _char = contentHtml[i+1];
                if (_char > 255) continue;
                firstLetters[_char] = true;
            }
            return this.Where(ikip =>
            {
                if (!(ikip.P is DocLink docLink)) return false; // TODO: We could possibly cache all IKIP's into a List<DocLink> instead.
                var _char = docLink.LinkWordHTML[0];
                if (_char > 255) return true; // Strange character but we must include this.
                return firstLetters[_char];
            }).Select(ikip => (DocLink)ikip.P); // TODO: We could possibly cache all IKIP's into a List<DocLink> instead.
        }
    }
}
