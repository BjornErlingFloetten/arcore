﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

/// <summary>
/// Contains:
/// <see cref="ARCDoc.DocFrag"/>
/// <see cref="ARCDoc.DocFragCollection"/>
/// <see cref="ARCDoc.DocFragP"/>
/// <see cref="ARCDoc.DocFragType"/>
/// </summary>
namespace ARCDoc {
    [Class(Description =
        "DocFrag = Document fragment.\r\n" +
        "\r\n" +
        "-" + nameof(DocFrag) + "- can be seen as a simplified de-normalized alternative view to the full documentation structure.\r\n" +
        "\r\n" +
        "A document fragment may describe one of Assembly, Namespace, Class, ClassMember, Enum, EnumMember (se -" + nameof(DocFragType) + "-).\r\n" +
        "\r\n" +
        "Used as input to -" + nameof(HTOC) + "-.-" + nameof(HTOC.Create) + "-.\r\n" +
        "\r\n" +
        "Does also offer an alternative view to documentation suitable for querying with -" + nameof(ARComponents.ARCQuery) + "-.\r\n" +
        "Using -" + nameof(DocFrag) + "- it is for instance easy to generate statistics about documentation.\r\n" +
        "\r\n" +
        "For a given instance of -" + nameof(DocFrag) + "-, property -" + nameof(DocFragP.Attribute) + "- gives the full documentation information.\r\n" +
        "\r\n"
    )]
    public class DocFrag : PRich {

        [ClassMember(Description =
            "Creates a -" + nameof(DocFrag) + "- object representing an assembly or a namespace.\r\n" +
            "\r\n" +
            "If description is not given, a description from -" + nameof(ARComponents) + "- is used if available (if name matches one of the members)."
        )]
        public static IKIP Create(DocFragType docFragType, string assemblyNameOrNamespace, string? description = null) {
            if (docFragType != DocFragType.Assembly && docFragType != DocFragType.Namespace) throw new InvalidEnumException(docFragType,
                "Only -" + nameof(DocFragType.Assembly) + "- / -" + nameof(DocFragType.Namespace) + "- allowed when -" + nameof(assemblyNameOrNamespace) + "- is given");
            var retval = (IP)new DocFrag();
            retval.AddPV(DocFragP.DocFragType, docFragType);
            retval.AddPV(DocFragP.Name, assemblyNameOrNamespace);
            if (docFragType == DocFragType.Namespace) {
                retval.AddPV(DocFragP.Namespace, assemblyNameOrNamespace);
                var t = assemblyNameOrNamespace.Split('.');
                if (t.Length > 1) retval.AddPV(DocFragP.ParentNamespace, string.Join(".", t.Take(t.Length - 1)));
            }
            // retval.AddPV(DocFragP.ASMLevel, 1);
            if (!string.IsNullOrEmpty(description)) {
                retval.AddPV(BaseAttributeP.Description, description);
                retval.AddPV(DocFragP.Lines, description.Split("\r\n").Length); // NOTE: A bit inefficient, it would be quicker to count number of line breaks instead.
            } else if (UtilCore.EnumTryParse<ARComponents>(assemblyNameOrNamespace, out var c)) {
                // Note: If DocFraType is Namespace, this is not FORMALLY correct (namespaces do not have to be related to assemblies, they are ortogonal to each other)
                // But, it is a pragmatic choice to use documentation for assembly as they should be the same.
                // TODO: Find some way of documenting namespace which is NOT an ARComponent (applicatio specific namespaces are not documented now)
                var a = EnumMemberAttribute.GetAttribute(c);
                if (a.IP.TryGetP<IP>(BaseAttributeP.Description, out var d)) retval.AddP(BaseAttributeP.Description, d);
                if (a.IP.TryGetP<IP>(BaseAttributeP.LongDescription, out d)) retval.AddP(BaseAttributeP.LongDescription, d);
                // Do not count lines, they will probably be counted as part of the ARComponents attribute instead (avoid double counting)
                // TODO: Is this correct? We could like to see fragments with a long description maybe (many lines).
            } else {
                // No description available
            }
            return new IKIP(IKString.FromCache(docFragType + " " + assemblyNameOrNamespace), retval);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="docFragType"></param>
        /// <param name="classOrEnumName"></param>
        /// <param name="memberName"></param>
        /// <param name="attribute">This is probably a <see cref="BaseAttribute"/>, except when a <see cref="PK"/></param>
        /// <param name="shortMemberName">Relevant for class members with overloads. Member name without any Overload1 / Overload2 and so on appended.</param>
        /// <returns></returns>
        [ClassMember(Description =
            "Creates a -" + nameof(DocFrag) + "- object representing a class, class member, enum or enum member.\r\n" +
            "\r\n" +
            "Returns both a -" + nameof(DocFrag) + "- instance and its recommended key"
        )]
        public static IKIP Create(
            DocFragType docFragType,
            Type type,
            IP attribute,
            string? memberName = null,
            string? shortMemberName = null,
            bool? isInherited = null,
            bool? isInheritedDoc = null
        ) {
            var retval = (IP)new DocFrag();
            retval.AddPV(DocFragP.DocFragType, docFragType);
            retval.AddPV(DocFragP.Assembly, type.Assembly.GetName().Name);
            if (type.Namespace != null) {
                retval.AddPV(DocFragP.Namespace, type.Namespace);
                var t = type.Namespace.Split('.');
                if (t.Length > 1) retval.AddPV(DocFragP.ParentNamespace, string.Join(".", t.Take(t.Length - 1)));
            }
            retval.AddPV(DocFragP.Type, type);
            retval.AddPV(DocFragP.Name, shortMemberName ?? memberName ?? type.ToStringShort()); // type.ToStringShort means class or enum name
            if (isInherited != null) retval.AddPV(DocFragP.IsInherited, isInherited.Value);
            if (isInheritedDoc != null) retval.AddPV(DocFragP.IsInheritedDoc, isInheritedDoc.Value);
            retval.AddP(DocFragP.Attribute, attribute);
            var lines = 0L;
            if (attribute.TryGetP<IP>(BaseAttributeP.Description, out var d)) {
                retval.AddP(BaseAttributeP.Description, d);
                if (isInheritedDoc != null && !isInheritedDoc.Value) lines += d.GetV<string>(defaultValue: "").Split("\r\n").Length; // NOTE: A bit inefficient, it would be quicker to count number of line breaks instead.
            }
            if (attribute.TryGetP<IP>(BaseAttributeP.LongDescription, out d)) {
                retval.AddP(BaseAttributeP.LongDescription, d);
                if (isInheritedDoc != null && !isInheritedDoc.Value) lines += d.GetV<string>(defaultValue: "").Split("\r\n").Length; // NOTE: A bit inefficient, it would be quicker to count number of line breaks instead.
            }
            if (lines > 0) retval.AddPV(DocFragP.Lines, lines);
            return new IKIP(IKString.FromCache(type.Namespace + "." + type.ToStringShort() + (memberName == null ? "" : ("." + memberName))), retval);
        }

        [ClassMember(Description =
            "Alternative ('override') to -" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "-.\r\n" +
            "\r\n" +
            "Does also present all properties of -" + nameof(DocFragP.Attribute) + "-, that is, reducing need for navigating one more level down in the hierarchy."
        )]
        public string ToHTMLSimpleSingle(bool prepareForLinkInsertion = false, List<IKCoded>? linkContext = null) {
            var retval = new System.Text.StringBuilder();
            retval.Append(UtilDoc.GetCurrentLevelInHierarchyAsHTML(linkContext, out var internalLinks, out var strLinkContext));
            retval.Append("<table>\r\n<tr><th>Key</th><th>Value</th></tr>\r\n");
            retval.Append(string.Join("", this.OrderBy(ikip => ikip.Key.ToString()).Select(ikip =>
                (
                    /// Skip <see cref="DocFragP.Attribute"/> because it will be show under anyway in detail.
                    ikip.Key.Equals(PK.FromEnum(DocFragP.Attribute)) ||
                    /// Skip <see cref="BaseAttributeP.Description"/> and <see cref="BaseAttributeP.LongDescription"/> 
                    /// Show them separately below instead.
                    /// (Often they are not of interest, for instance if we are part of an <see cref="HTOC"/> instance, which
                    /// show the description at a higher level anyway, with the HTML description of the <see cref="DocFrag"/>
                    /// itself being only of use if some detailed information is desired).
                    ikip.Key.Equals(PK.FromEnum(BaseAttributeP.Description)) ||
                    ikip.Key.Equals(PK.FromEnum(BaseAttributeP.LongDescription))
                ) ? "" :
                ikip.ToHTMLSimpleAsTableRow(strLinkContext, prepareForLinkInsertion)
            )));
            retval.Append("</table>\r\n");
            if (IP.TryGetP<IP>(DocFragP.Attribute, out var a)) {

                // Adjust linkcontext, since what we are showing now is actually one step further down the hierarchy.
                strLinkContext = strLinkContext + nameof(DocFragP.Attribute) + "/";

                retval.Append("<p>" + System.Net.WebUtility.HtmlEncode(a.GetType().ToStringShort()) + "</p>");
                retval.Append("<table>\r\n<tr><th>Key</th><th>Value</th></tr>\r\n");
                retval.Append(string.Join("", a.OrderBy(ikip => ikip.Key.ToString()).Select(ikip =>
                    (
                        /// Skip <see cref="BaseAttributeP.Description"/> and <see cref="BaseAttributeP.LongDescription"/> 
                        /// because those where copied into "this" object.
                        ikip.Key.Equals(PK.FromEnum(BaseAttributeP.Description)) ||
                        ikip.Key.Equals(PK.FromEnum(BaseAttributeP.LongDescription))
                    ) ? "" :
                    ikip.ToHTMLSimpleAsTableRow(strLinkContext, prepareForLinkInsertion)
                )));
                retval.Append("</table>\r\n");
            }

            if (IP.TryGetPV<string>(BaseAttributeP.Description, out var d)) {
                retval.Append("<p>" + System.Net.WebUtility.HtmlEncode(d).Replace("\r\n", "<br>\r\n") + "</p>");
            }

            if (IP.TryGetPV<string>(BaseAttributeP.LongDescription, out d)) {
                retval.Append("<p>" + System.Net.WebUtility.HtmlEncode(d).Replace("\r\n", "<br>\r\n") + "</p>");
            }

            return retval.ToString();
        }

        public static Func<HTOC.ParentChain?, IKIP, IK?> HTOCFilterAllAssemblies = new Func<HTOC.ParentChain?, IKIP, IK?>((_null, content) => (
            content.P.TryGetPV(DocFragP.DocFragType, out DocFragType t) && t == DocFragType.Assembly
        ) ? IKString.FromCache(content.P.GetPV<string>(DocFragP.Name)) : null);

        public static Func<HTOC.ParentChain?, IKIP, IK?> HTOCFilterAllClassesAndEnumsWithAssemblyEqualToParent = new Func<HTOC.ParentChain?, IKIP, IK?>((parent, content) => (
            content.P.TryGetPV(DocFragP.Assembly, out string s) &&
            s == parent!.IKIP.P.GetPV<string>(DocFragP.Name) &&
            (content.P.GetPV<DocFragType>(DocFragP.DocFragType) switch {
                DocFragType.Class => true,
                DocFragType.Enum => true,
                _ => false
            })
        ) ? IKString.FromCache(content.P.GetPV<string>(DocFragP.Name)) : null);

        public static Func<HTOC.ParentChain?, IKIP, IK?> HTOCFilterAllClassesWithAssemblyEqualToParentsParent = new Func<HTOC.ParentChain?, IKIP, IK?>((parent, content) => (
            content.P.TryGetPV(DocFragP.Assembly, out string s) &&
            s == parent!.Parent!.IKIP.P.GetPV<string>(DocFragP.Name) &&
            (content.P.GetPV<DocFragType>(DocFragP.DocFragType) switch {
                DocFragType.Class => true,
                _ => false
            })
        ) ? IKString.FromCache(content.P.GetPV<string>(DocFragP.Name)) : null);

        public static Func<HTOC.ParentChain?, IKIP, IK?> HTOCFilterAllEnumsWithAssemblyEqualToParentsParent = new Func<HTOC.ParentChain?, IKIP, IK?>((parent, content) => (
            content.P.TryGetPV(DocFragP.Assembly, out string s) &&
            s == parent!.Parent!.IKIP.P.GetPV<string>(DocFragP.Name) &&
            (content.P.GetPV<DocFragType>(DocFragP.DocFragType) switch {
                DocFragType.Enum => true,
                _ => false
            })
        ) ? IKString.FromCache(content.P.GetPV<string>(DocFragP.Name)) : null);

        public static Func<HTOC.ParentChain?, IKIP, IK?> HTOCFilterAllClassMembersAndEnumMembersWithTypeEqualToParent = new Func<HTOC.ParentChain?, IKIP, IK?>((parent, content) => (
            content.P.TryGetPV(DocFragP.Type, out Type t) &&
            t == parent!.IKIP.P.GetPV(DocFragP.Type, typeof(object)) &&
            (content.P.GetPV<DocFragType>(DocFragP.DocFragType) switch {
                DocFragType.ClassMember => true,
                DocFragType.EnumMember => true,
                _ => false
            })
        ) ? IKString.FromCache(content.P.GetPV<string>(DocFragP.Name)) : null);

        public static Func<HTOC.ParentChain?, IKIP, IK?> HTOCFilterAllClassMembersWithTypeEqualToParent = new Func<HTOC.ParentChain?, IKIP, IK?>((parent, content) => (
            content.P.TryGetPV(DocFragP.Type, out Type t) &&
            t == parent!.IKIP.P.GetPV(DocFragP.Type, typeof(object)) &&
            (content.P.GetPV<DocFragType>(DocFragP.DocFragType) switch {
                DocFragType.ClassMember => true,
                _ => false
            })
        ) ? IKString.FromCache(content.P.GetPV<string>(DocFragP.Name)) : null);

        public static Func<HTOC.ParentChain?, IKIP, IK?> HTOCFilterAllEnumMembersWithTypeEqualToParent = new Func<HTOC.ParentChain?, IKIP, IK?>((parent, content) => (
            content.P.TryGetPV(DocFragP.Type, out Type t) &&
            t == parent!.IKIP.P.GetPV(DocFragP.Type, typeof(object)) &&
            (content.P.GetPV<DocFragType>(DocFragP.DocFragType) switch {
                DocFragType.EnumMember => true,
                _ => false
            })
        ) ? IKString.FromCache(content.P.GetPV<string>(DocFragP.Name)) : null);

        public static Func<HTOC.ParentChain?, IKIP, IK?> HTOCFilterAllRootNamespaces = new Func<HTOC.ParentChain?, IKIP, IK?>((_null, content) => (
            content.P.TryGetPV(DocFragP.DocFragType, out DocFragType t) && t == DocFragType.Namespace &&
            // !content.P.TryGetP(DocFragP.ParentNamespace, out _)
            !content.P.ContainsKey(DocFragP.ParentNamespace)
        ) ? IKString.FromCache(content.P.GetPV<string>(DocFragP.Name)) : null);

        public static Func<HTOC.ParentChain?, IKIP, IK?> HTOCFilterNamespacesClassesAndEnumsWithNamespaceEqualToParent = new Func<HTOC.ParentChain?, IKIP, IK?>((parent, content) => (
            (
                content.P.TryGetPV(DocFragP.ParentNamespace, out string s) &&
                s == parent!.IKIP.P.GetPV<string>(DocFragP.Name) &&
                (content.P.GetPV<DocFragType>(DocFragP.DocFragType) switch {
                    DocFragType.Namespace => true,
                    _ => false
                })
            ) || (
                content.P.TryGetPV(DocFragP.Namespace, out s) &&
                s == parent!.IKIP.P.GetPV<string>(DocFragP.Name) &&
                (content.P.GetPV<DocFragType>(DocFragP.DocFragType) switch {
                    DocFragType.Class => true,
                    DocFragType.Enum => true,
                    _ => false
                })
            )
        ) ? IKString.FromCache(content.P.GetPV<string>(DocFragP.Name)) : null);

        public static Func<HTOC.ParentChain?, IKIP, IK?> HTOCFilterAllSuperclasses = new Func<HTOC.ParentChain?, IKIP, IK?>((_null, content) => (
            content.P.TryGetPV(DocFragP.DocFragType, out DocFragType t) && t == DocFragType.Class &&
            content.P.TryGetP<IP>(DocFragP.Attribute, out var a) &&
            (
                !a.TryGetPV<List<Type>>(ClassAttributeP.BaseTypes, out var baseTypes) ||
                baseTypes.Count == 0 // Should be a superfluous check, but better safe than sorry
            ) &&
            a.TryGetPV<List<Type>>(ClassAttributeP.SubTypes, out _)
        ) ? IKString.FromCache(content.P.GetPV<string>(DocFragP.Name)) : null);

        public static Func<HTOC.ParentChain?, IKIP, IK?> HTOCFilterAllClassesWithBaseTypeOrInterfaceEqualToParent = new Func<HTOC.ParentChain?, IKIP, IK?>((parent, content) => (
            content.P.TryGetPV(DocFragP.DocFragType, out DocFragType t) && t == DocFragType.Class &&
            parent!.IKIP.P.TryGetPV<Type>(DocFragP.Type, out var parentType) &&
            content.P.TryGetP<IP>(DocFragP.Attribute, out var a) &&
            (
                (
                    a.TryGetPV<List<Type>>(ClassAttributeP.BaseTypes, out var baseTypes) &&
                    baseTypes.Count > 0 && // Should be a superfluous check, but better safe than sorry
                    baseTypes[^1].Equals(parentType)
                ) ||
                (
                    a.TryGetPV<List<Type>>(ClassAttributeP.InterfacesDirectlyImplemented, out var interfaces) &&
                    interfaces.Contains(parentType)
                )
            )
        ) ? IKString.FromCache(content.P.GetPV<string>(DocFragP.Name)) : null);

    }

    [Class(Description = "Contains a collection of -" + nameof(DocFrag) + "- objects.")]
    public class DocFragCollection : PRich {

        /// <summary>
        /// This was originally code placed in <see cref="Documentator.BuildDocumentationHierarchy"/>
        /// </summary>
        /// <param name="assemblies">
        /// Assemblies into which to look for enums and classes. Normally taken from <see cref="UtilCore.Assemblies"/>
        /// </param>
        /// <param name="initialDescription">
        /// The 'getting started' text on the 'front-page' / root level of the documentation generated.
        /// If not given then description for <see cref="ARConcepts.GettingStarted"/> will be used.
        /// </param>
        /// <returns></returns>
        [ClassMember(Description =
            "Creates documentation fragments (-" + nameof(DocFrag) + "-) for all possible -" + nameof(BaseAttribute) + "- elements found in the given assemblies."
        )]
        public static DocFragCollection Create(IEnumerable<System.Reflection.Assembly> assemblies) {
            var retval = new DocFragCollection();

            retval.IP.AddPV(DocumentatorP._Description, "Collection of all documentation fragments found in the following assemblies: " + string.Join(", ", assemblies.Select(a => a.GetName().Name)));

            var namespaces = new System.Collections.Generic.HashSet<string>();

            assemblies.ForEach(a => {
                var assemblyName = a.GetName().Name;

                retval.IP.AddP(DocFrag.Create(DocFragType.Assembly, assemblyName));

                // Part 1, class documentation
                // ===============================
                var subTypesPerClass = new Dictionary<Type, List<Type>>(); /// Used in order to populate <see cref="ClassAttributeP.SubTypes"/>
                                                                           /// Build actual <see cref="DocFrag"/>s
                a.GetTypes().Where(t => !t.IsEnum).ForEach(t => {
                    var mas = t.GetMembers().Select(e => new Func<(ClassMemberAttribute a, bool isInherited, bool isInheritedDoc)?>(() => {
                        if ((e.MemberType & System.Reflection.MemberTypes.NestedType) == System.Reflection.MemberTypes.NestedType) {
                            // Nested type means a type declared inside this class (an inner type).
                            /// If we had tried to use it, it would most probably result in a <see cref="BaseAttribute.IncorrectAttributeTypeUsedException"/>
                            return null;
                        }
                        if (e.DeclaringType == t) {
                            // Not inherited
                            var ma = ClassMemberAttribute.GetAttribute(e);
                            if (ma.IsDefault) {
                                // Check if documented in a base type
                                // Note that this call is somewhat expensive, and is frequently executed (for every class member without an attribute)
                                ma = ClassMemberAttribute.GetAttributeFromBaseClassOfDeclaringType(e);
                                if (ma.IsDefault) {
                                    return null; // Documenting members without attributes is of little interest
                                } else {
                                    return (a: ma, isInherited: false, isInheritedDoc: true);
                                }
                            } else {
                                return (a: ma, isInherited: false, isInheritedDoc: false);
                            }
                        } else {
                            // Inherited
                            var ma = ClassMemberAttribute.GetAttribute(e.DeclaringType, e.Name);
                            if (ma.IsDefault) {
                                /// Note that we could also here use <see cref="ClassMemberAttribute.GetAttributeFromBaseClassOfDeclaringType"/>
                                /// in case of method not being documented in superclass, but ITS superclass again.
                                return null; // Documenting members without attributes is of little interest
                            } else {
                                return (a: ma, isInherited: true, isInheritedDoc: true);
                            }
                        }
                    })()).Where(ma => ma != null).Select(ma => ma!.Value).ToList();

                    var a = ClassAttribute.GetAttribute(t);

                    if (a.IsDefault && mas.Count == 0) {
                        // No attribute for class, and no attribute for any member, do not document class at all
                        return;
                    }

                    /// Populate <see cref="ClassAttributeP.SubTypes"/>
                    if (a.IP.TryGetPV<List<Type>>(ClassAttributeP.BaseTypes, out var baseTypes)) {
                        baseTypes.ForEach(baseType => {
                            if (!subTypesPerClass.TryGetValue(baseType, out var l)) {
                                subTypesPerClass[baseType] = l = new List<Type>();
                            }
                            l.Add(t);
                        });
                    }
                    if (a.IP.TryGetPV<List<Type>>(ClassAttributeP.Interfaces, out var interfaces)) {
                        interfaces.ForEach(_interface => {
                            if (!subTypesPerClass.TryGetValue(_interface, out var l)) {
                                subTypesPerClass[_interface] = l = new List<Type>();
                            }
                            l.Add(t);
                        });
                    }

                    if (t.Namespace != null) namespaces.Add(t.Namespace);

                    retval.IP.AddP(DocFrag.Create(DocFragType.Class, t, attribute: a));

                    mas.ForEach(ma => {
                        var maName = ma.a.IP.GetPV<string>(ClassMemberAttributeP.MethodName);
                        var overloads = mas.Where(c => c.a.IP.GetPV<string>(ClassMemberAttributeP.MethodName) == maName).ToList();
                        if (overloads.Count > 1) overloads = overloads.OrderBy(c => c.a.IP.GetPV<string>(ClassMemberAttributeP.MethodSignature)).ToList();
                        var memberName = ma.a.IP.GetPV<string>(ClassMemberAttributeP.MethodName) + (overloads.Count == 1 ?
                            "" : // Use only MethodName for key, only member with this name
                            (
                                // Use MethodName + space + Overload1 / Overload2 and so on.
                                " Overload" + (overloads.FindIndex(c => c.a.IP.GetPV<string>(ClassMemberAttributeP.MethodSignature) == ma.a.IP.GetPV<string>(ClassMemberAttributeP.MethodSignature)) + 1)
                            )
                        );
                        retval.IP.AddP(DocFrag.Create(DocFragType.ClassMember, t,
                            attribute: ma.a,
                            memberName,
                            shortMemberName: ma.a.IP.GetPV<string>(ClassMemberAttributeP.MethodName),
                            isInherited: ma.isInherited,
                            isInheritedDoc: ma.isInheritedDoc)
                        );
                    });
                });
                subTypesPerClass.ForEach(e => {
                    ClassAttribute.GetAttribute(e.Key).IP.SetPV(ClassAttributeP.SubTypes, e.Value.OrderBy(t => t.ToStringVeryShort()).ToList());
                });

                // Part 2, enum documentation
                // ===============================
                a.GetTypes().Where(t => t.IsEnum).ForEach(t => {
                    if (t.Namespace != null) namespaces.Add(t.Namespace);
                    var a = EnumAttribute.GetAttribute(t);
                    retval.IP.AddP(DocFrag.Create(DocFragType.Enum, t, attribute: a));

                    // Note that it is important to choose correct between PK.FromEnum and EnumMemberAttribute.GetAttribute (incorrect choice results in an exception being thrown)
                    var isPK = a.IP.GetPV<AREnumType>(EnumAttributeP.AREnumType) == AREnumType.PropertyKeyEnum;

                    UtilCore.EnumGetMembers(t).ForEach(e => {
                        // Note that for enums (in contrast to classes below), we document every value regardless of there
                        // being only a default attribute for it or not
                        retval.IP.AddP(isPK ?
                            DocFrag.Create(DocFragType.EnumMember, t, attribute: PK.FromEnum(e).IP, memberName: e.ToString()) :
                            DocFrag.Create(DocFragType.EnumMember, t, attribute: EnumMemberAttribute.GetAttribute(e).IP, memberName: e.ToString())
                        );
                    });
                });
            });
            namespaces.ForEach(n => retval.IP.AddP(DocFrag.Create(DocFragType.Namespace, n)));
            return retval;
        }
    }

    [Enum(
        Description =
            "Describes class -" + nameof(DocFrag) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum DocFragP {
        __invalid,

        [PKType(Type = typeof(DocFragType), IsObligatory = true)]
        DocFragType,

        [PKType(Description =
            "Name of the assembly in which this fragment resides."
        )]
        Assembly,

        [PKType(Description =
            "Namespace in which this fragment resides."
        )]
        Namespace,

        [PKType(Description =
            "Parent namespace of namespace in which this fragment resides."
        )]
        ParentNamespace,

        [PKType(
            Description = "Type of class or enum which this fragment describes (also relevant for class or enum member).",
            Type = typeof(Type)
        )]
        Type,

        [PKType(
            Description =
                "As simple a name as practical usable. Often not unique within a greater context.\r\n" +
                "\r\n" +
                "Normally class name, enum name or member name.\r\n" +
                "Only method name for class methods (without signature), maybe with ' Overload1' / ' Overload2' and so on appended.\r\n" +
                "\r\n" +
                "The first word if this name is used for -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "-.\r\n" +
                "\r\n" +
                "If type is -" + nameof(ARCDoc.DocFragType.Assembly) + "- then consists of only the assembly name.\r\n" +
                "If type is -" + nameof(ARCDoc.DocFragType.Namespace) + "- then consists of 'Namespace {namespace}', that is,\r\n" +
                "with the literal prefix 'Namespace' prepended to the actual namespace.",
            IsObligatory = true
        )]
        Name,

        [PKType(
            Description =
                "Relevant for class members. TRUE if method is inherited from a base type.",
            Type = typeof(bool)
        )]
        IsInherited,

        [PKType(
            Description =
                "Relevant for class members (methods).\r\n" +
                "TRUE if documentation is inherited from a base type.\r\n" +
                "Either because method -" + nameof(IsInherited) + "-, " +
                "or because method is override and documentation (-" + nameof(ClassMemberAttribute) + "- was only found in corresponding method in super-class).\r\n" +
                "(should always TRUE whenever -" + nameof(IsInherited) + "- is TRUE).",
            Type = typeof(bool)
        )]
        IsInheritedDoc,

        [PKType(
            Description =
                "Number of lines of text in -" + nameof(BaseAttributeP.Description) + "- and -" + nameof(BaseAttributeP.LongDescription) + "- for this fragment.\r\n" +
                "\r\n" +
                "Only set if description is explicit given for this -" + nameof(DocFrag) + "- instance, and not inherited / copied from elsewhere.\r\n" +
                "(in other words, sum of all lines should be equivalent to sum of all lines of orginal text.)\r\n" +
                "For isntance if type is -" + nameof(ARCDoc.DocFragType.Assembly) + "- or -" + nameof(ARCDoc.DocFragType.Namespace) + "- " +
                "then only an explicit given description is counted.\r\n" +
                "And for type -" + nameof(ARCDoc.DocFragType.ClassMember) + "- inherited descriptions are not counted.",
            Type = typeof(long)
        )]
        Lines,

        [PKType(
            Description =
                "The -" + nameof(BaseAttribute) + "- or -" + nameof(PK) + "- from which this -" + nameof(DocFrag) + "- originates.\r\n" +
                "May be NULL (not existing), typically for enum members.",
            Type = typeof(IP)
        )]
        Attribute
    }

    [Enum(Description = "Type of documentation fragment, like -" + nameof(Assembly) + "-, -" + nameof(Class) + "- or -" + nameof(ClassMember) + "-.")]
    public enum DocFragType {
        __invalid,
        Assembly,
        Namespace,
        [EnumMember(Description = "Does also include interfaces.")]
        Class,
        ClassMember,
        Enum,
        EnumMember,
    }
}
