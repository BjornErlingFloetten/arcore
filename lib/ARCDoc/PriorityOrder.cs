﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCDoc {
    [Enum(
        Description =
            "Used for general sorting / prioritizing when showing properties for an entity. " +
            "\r\n" +
            "A lower value (think like 1'st order of priority, 2'nd order of priority) will put object higher up (make more visible) " +
            "in a typical AgoRapide sorted list.\r\n" +
            "\r\n" +
            "Also used for making summaries of what an entity contains (used by -" + nameof(Extensions.DescribeValue) + "- for instance).\r\n" +
            "\r\n" +
            "Recommended values are:\r\n" +
            nameof(Important) + " (-1) for important,\r\n" +
            nameof(Neutral) + " (0) (default) for 'ordinary' and\r\n" +
            nameof(NotImportant) + " (1) for not important.\r\n" +
            "\r\n" +
            "In this manner it will be relatively easy to emphasize or deemphasize single properties " +
            "without having to give an explicit ordering for all properties.\r\n" +
            "Eventually expand to -2, -3 ... or 2, 3 ... as needed " +
            "(note there is no need for expanding this enum -" + nameof(PriorityOrder) + "- itself since any integer is a valid enum value).\r\n" +
            "NOTE: This is not possible as of May 2020 if -" + nameof(PK) + "- data is to be communicated over the -" + nameof(ARConcepts.PropertyStream) + "- " +
            "because values other than those defined here will fail validation",
        AREnumType = AREnumType.OrdinaryEnum
    )]
    public enum PriorityOrder {
        Important = -1,
        Neutral = 0,
        NotImportant = 1,

        // TODO: Consider introducing this
        //[ClassMember(
        //    Description =
        //        "Not used for -" + nameof(ARConcepts.TaggingOfPropertyKeys) + "- but can be used as a query-parameter " +
        //        "(all properties are requested)"
        //)]
        //Everything = int.MaxValue
    }
}