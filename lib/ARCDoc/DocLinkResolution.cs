﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Net;
using System.Linq;

/// <summary>
/// Contains
/// <see cref="ARCDoc.DocLinkResolution"/>
/// <see cref="ARCDoc.DocLinkResolutionP"/>
/// <see cref="ARCDoc.DocLinkResolutionCollection"/>
/// </summary>
namespace ARCDoc {

    [Class(Description = "Contains a resolution page for a link word which has duplicate 'targets' (pointing to multiple -" + nameof(DocLink) + "- instances).")]
    public class DocLinkResolution : PRich {

        /// <summary>
        /// NOTE: <see cref="PropertyStreamLine.TryStore"/> requires all IP implementations to have a constructor with no parameters.
        /// </summary>
        public DocLinkResolution() {
        }

        public DocLinkResolution(string linkWord, List<DocLink> docLinks) {
            IP.AddPV(DocLinkResolutionP.LinkWord, linkWord);
            IP.AddPV(DocLinkResolutionP.DocLinks, docLinks);
        }

        [ClassMember(Description =
            "Gives a friendly description about possible targets for " + nameof(DocLinkResolutionP.LinkWord) + "-.\r\n"
        )]
        public string ToHTMLSimpleSingle(bool prepareForLinkInsertion = false, List<IKCoded>? linkContext = null) {
            var retval = new System.Text.StringBuilder();
            retval.Append(UtilDoc.GetCurrentLevelInHierarchyAsHTML(linkContext, out var internalLinks, out var strLinkContext));

            // Note how some suggestions may be confusing, because the original link will reside within some local context, 
            // while the page we create now will have a global context. Ideally we should have had a separate resolution file 
            // for each local context, because then we could have picked the most local one as default. 

            var linkWord = IP.GetPV<string>(DocLinkResolutionP.LinkWord);
            retval.AppendLine("<p>The link \"" + WebUtility.HtmlEncode(linkWord) + "\" is ambigious, possible targets are:</p>\r\n");
            // Create links and sort alfabetically

            var docLinks = IP.GetPV<List<DocLink>>(DocLinkResolutionP.DocLinks);
            var commonLevels = 0;
            var min = docLinks.Select(docLink => docLink.Location.Count).Min();
            while (commonLevels < min && docLinks.Select(docLink => docLink.Location[commonLevels]).All(l => l == docLinks[0].Location[commonLevels])) {
                commonLevels++;
            }
            docLinks.
            Select(docLink => (
                url:
                    (linkContext == null ?
                        string.Join("", Enumerable.Repeat("../", Documentator.DocLinkResolutionLocation.Count)) : // HACK, will not necessarily work.
                        string.Join("", Enumerable.Repeat("../", linkContext.Count - 1))
                    ) +
                    string.Join("/", docLink.Location) + (docLink.Location.Count == 0 ? "" : "/") + docLink.Filename.Encoded,
                linkText:
                    // Create link text as short as possible
                    // 1) Skip common levels between all possible targets for instance                    
                    // 2) Remove _Member
                    string.Join(" / ",
                        docLink.Location.
                        Skip(commonLevels).
                        Select(level => {
                            var str = level.Unencoded.ToString();
                            return nameof(DummyEnumP._Member).Equals(str) ? "" : str;
                        }) /// _Member is just not necessary to show (it usually refers to collection of members in for instance a class or enum). See <see cref="Documentator.BuildDocumentationHierarchy"/> for where it originates.
                        ) +  // 
                    ((docLink.Location.Count - commonLevels) == 0 ? "" : " / ") + linkWord
            )).
            OrderBy(target => target.url).ForEach(target => {
                /// TODO: Insert description for each URL
                /// TODO: Now we only insert URLs, with no description about what is behind each URL
                /// TODO: Expand <see cref="HLocation"/> to include a Description-field
                /// TODO: and insert that here.
                /// TODO:
                /// TODO: Note, such an addition is useful also for adding description for all other links in the system.
                /// TODO: In this manner we can:
                /// TODO: 1) For non-ambigiuous links, insert description directly as 'tooltip'
                /// TODO: 2) For ambigious links, insert tooltip Ambigious links between 1,2,3.... 
                /// TODO: 3) On actual resolution page (here) add link directly visible as HTML (no tooltip)
                retval.Append("<p><a href=\"" +
                    target.url + // TODO: UrlEncode is absent. We can not encode now because of slashes. 
                    "\">" +
                    WebUtility.HtmlEncode(target.linkText) + /// TODO: HtmlEncode is absent. Probably OK since filename was originally coded by <see cref="IKCoded.CreateIKSafeString"/>
                    "</a></p>\r\n"
                );
            });
            return retval.ToString();
        }
    }

    [Enum(
        Description = "Describes class -" + nameof(DocLinkResolution) + "-",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum DocLinkResolutionP {
        __invalid,

        [PKType(IsObligatory = true)]
        LinkWord,

        [PKType(
            Description = "The possible links for -" + nameof(LinkWord) + "-",
            Type = typeof(DocLink),
            Cardinality = Cardinality.WholeCollection,
            IsObligatory = true)]
        DocLinks,
    }

    [Class(Description =
        "Contains a collection of -" + nameof(DocLinkResolution) + "- objects.\r\n"
    )]
    public class DocLinkResolutionCollection : PRich {

    }
}
