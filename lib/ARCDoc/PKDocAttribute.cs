﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCDoc {


    [Class(
        Description =
            "Describes documentation and presentation of a property." +
            "\r\n" +
            "Note: As of Mar 2021 only in use by -" + nameof(Extensions.DescribeValue) + "-.\r\n" +
            "\r\n" +
            "See also -" + nameof(PKDocAttributeP) + "-."
    )]
    public class PKDocAttribute : BasePKAttribute {

        /// <summary>
        /// TODO: Add AssertInitializationNotCompleteForAttributeAccess
        /// </summary>
        [ClassMember(Description = "See -" + nameof(PKDocAttributeP.PriorityOrder) + "-.")]
        public PriorityOrder PriorityOrder { get; set; }

        public override void Initialize() {
            IP.AddPV(PKDocAttributeP.PriorityOrder, PriorityOrder);
            base.Initialize();
        }
    }

    [Enum(
        Description =
            "Describes class -" + nameof(PKDocAttribute) + "-.\r\n" +
            "\r\n" +
            "See also -" + nameof(PKDocAttribute) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum PKDocAttributeP {
        __invalid,

        [PKType(
            Description = "Used by -" + nameof(Extensions.DescribeValue) + "-.",
            Type = typeof(PriorityOrder)
        )]
        PriorityOrder,
    }
}