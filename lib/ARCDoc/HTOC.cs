﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;
using System.Net;

namespace ARCDoc {

    [Class(Description =
        "HTOC = Hierarchical table of contents.\r\n" +
        "\r\n" +
        "Has two functions which may be used separately:\r\n" +
        "\r\n" +
        "1) Create a hierarchical table of contents based on a (usually) de-normalized table of content fragments.\r\n" +
        "This is done by method -" + nameof(Create) + "-.\r\n" +
        "For example of this, see -" + nameof(DocFrag) + "- and -" + nameof(DocFragCollection) + "-.-" + nameof(DocFragCollection.Create) + "-.\r\n" +
        "\r\n" +
        "2) Present a hierarchical table of content as HTML.\r\n" +
        "This is done by method -" + nameof(ToHTMLSimpleSingle) + "-.\r\n" +
        "\r\n" +
        "Note how 1) can be used to populate any -" + nameof(IP) + "- object, not necessarily -" + nameof(HTOC) + "-,\r\n" +
        "and 2) can be used to present any -" + nameof(IP) + "- object by using the static overload of -" + nameof(ToHTMLSimpleSingle) + "-."

    )]
    public class HTOC : PRich {

        [ClassMember(Description =
            "The key used for storing the actual content at this level.\r\n" +
            "\r\n" +
            "Should probably be filtered out by -" + nameof(HLocationCollection.Create) + "-\r\n" +
            "\r\n" +
            "NOTE: This key has a somewhat unusual name in order not to collide with any child key.\r\n" +
            "NOTE:\r\n" +
            "NOTE: This key is intentional not a -" + nameof(PK) + "- (contained in a HTOCP PropertyKey enum).1\r\n" +
            "NOTE: because it would then collide when we use -" + nameof(DocFragCollection) + "-.-" + nameof(DocFragCollection.Create) + "-.\r\n" +
            "NOTE: on \"ourselves\".\r\n"
        )]
        public static IK HTOCContentThisLevelKey = IKString.FromCache("__TOCDet");

        /// <summary>
        /// Note: As of Mar 2021 algorithm is fundamentally O(n^2), albeit with some attempts at reducing the impact.
        /// 
        /// Note: Works recursively.
        /// </summary>
        /// <param name="content">
        /// NOTE: This argument will be modified: For each item found, the corresponding value for its index will be set to null
        /// (remaining content are thus items not being null in this argument)
        /// 
        /// The list of content must have unique keys, at least for each parent
        /// </param>
        /// <param name="contentThisLevel">
        /// At initial call, set this to the initial description.
        /// </param>
        /// <param name="filterNode">
        /// TODO. Correct text here.<br>
        /// Each level is a function that compares parent against some content, and returns
        /// preferred key for that content if it is to be included.
        /// By preferred key is understood that key may not be unique for all children, in which
        /// case the original key will be used instead (an exception will then be thrown if keys are 
        /// still not uniqe)
        /// </param>
        /// <param name="possibleDuplicatePositions">
        /// TRUE means that some content may occur at multiple places in the resulting hierarchy.
        /// The practical consequence of setting this parameter TRUE is that content can not be deleted
        /// from <paramref name="content"/> as it is found, thus slowing down the creation process a little.
        /// (the algorithm's O(n^2) behaviour bites even harder).
        /// </param>
        /// <param name="parent">
        /// Do not set this at initial call
        /// </param>
        /// <typeparam name="T">
        /// Recommended type parameter is <see cref="HTOC"/>, as it has a suitable HTML-presenter for table of contents (<see cref="HTOC.ToHTMLSimpleSingle"/>)
        /// If you want the standard HTML-presenter in AgoRapide instead (<see cref="ARCDoc.Extensions.ToHTMLSimpleSingle"/>) use <see cref="PRich"/>.
        /// </typeparam>
        /// <returns></returns>
        [ClassMember(Description =
            "Structures content hierarchically according to the criteria for each level.\r\n" +
            "\r\n" +
            "A given piece of content can only be included at one level.\r\n" +
            "\r\n" +
            "Algorithm ends when no more content is found for a level, or all levels are checked.\r\n" +
            "\r\n" +
            "It is considered normal if some content is not included when algorithm ends.\r\n"
        )]
        public static T Create<T>(List<IKIP?> content, IP contentThisLevel, FilterNode? filterNode, bool possibleDuplicatePositions = false, ParentChain? parent = null) where T : class, IP?, new() {

            var retval = new T();

            retval.AddP(HTOCContentThisLevelKey, contentThisLevel);

            if (filterNode == null) {
                // We are finished, we do not have instructions to go deeper in the structure.
                return retval;
            }

            switch (filterNode) {
                case FilterSingle filterSingle: // Most common occurrence

                    var children = new List<(IKIP content, IK preferredKey)>();
                    for (var i = 0; i < content.Count; i++) {
                        if (content[i] == null) continue; // Was included earlier.
                        var preferredKey = filterSingle.Filter(parent, content[i]!);
                        if (preferredKey != null) {
                            children.Add((content[i]!, preferredKey));
                            if (possibleDuplicatePositions) {
                                // Careful, content may appear in another position also.
                            } else {
                                content[i] = null; // Remove in order to make for quicker comparision
                            }
                        }
                    }

                    // Decide use of keys:

                    /// Original approach was like this:
                    /// Check that the keys for this parent are unique, if not we will use the original keys instead.
                    ///   var useOriginalKey = children.Select(t => t.preferredKey).Distinct().Count() != children.Count;
                    ///   var key = useOriginalKey ? child.content.Key : child.preferredKey;
                    /// This would break <see cref="ARConcepts.LinkInsertionInDocumentation"/> however because
                    /// we would use an unexpected key like this:
                    ///   toc/ARCCore/Class/IP/ARCCore.IP.AddOrUpdateP0x0020Overload1.html
                    ///   toc/ARCCore/Class/IP/ARCCore.IP.AddOrUpdateP0x0020Overload2.html
                    // meaning that -AddOrUpdateP- would not point to anywhare.
                    // (and worse, ALL members of IP would be created with this long key, meaning that NONE
                    // of the links to it would work).

                    // Instead we now change the above structure into
                    //   toc/ARCCore/Class/IP/AddOrUpdateP/ARCCore.IP.AddOrUpdateP0x0020Overload1.html
                    //   toc/ARCCore/Class/IP/AddOrUpdateP/ARCCore.IP.AddOrUpdateP0x0020Overload2.html

                    /// Check that the keys for this parent are unique
                    var duplicatesExist = children.Select(t => t.preferredKey).Distinct().Count() != children.Count;
                    if (duplicatesExist) {
                        // Put duplicate content into subcontent with the original key as key instead.
                        bool foundDuplicates;
                        do { // while (foundDuplicates)    // NOTE: Algorithm is O(n^2), but with assumption that will run seldom and with only a few items.
                            foundDuplicates = false;
                            for (var i = 0; i < children.Count; i++) {
                                var key = children[i].preferredKey;
                                var duplicates = children.Where(child => child.preferredKey.Equals(key)).ToList();
                                if (duplicates.Count > 1) {
                                    foundDuplicates = true;
                                    var container = new T();
                                    container.AddP(CreateInitialContent(HTOCContentThisLevelKey.ToString(), duplicates.Count + " items"));
                                    duplicates.ForEach(d => {
                                        try {
                                            REx.Inc();
                                            var childrenHTOC = Create<T>(content, d.content.P, filterSingle.Next, possibleDuplicatePositions, ParentChain.Add(parent, new IKIP(key, d.content.P)));
                                            if (!container.TryAddP(d.content.Key, childrenHTOC, out var errorResponse)) {
                                                throw new HTOCException(
                                                    "Probable duplicate key (" + d.content.Key + ").\r\n" +
                                                    "The list of content must have unique keys, at least for each level in the hierarchical structure.\r\n" +
                                                    "Details: " + errorResponse + "\r\n");
                                            }
                                        } finally {
                                            REx.Dec();
                                        }
                                        children.Remove(d);
                                    });
                                    if (!retval.TryAddP(key, container, out var errorResponse)) {
                                        throw new HTOCException(
                                            "Probable duplicate key (" + key + ").\r\n" +
                                            "The list of content must have unique keys, at least for each level in the hierarchical structure.\r\n" +
                                            "Details: " + errorResponse + "\r\n");
                                    }
                                    break;
                                }
                            }
                        } while (foundDuplicates);
                        // Continue below with the rest of the children with keys that do not overlap
                    }


                    children.ForEach(child => {
                        // var key = useOriginalKey ? child.content.Key : child.preferredKey;
                        var key = child.preferredKey;
                        try {
                            REx.Inc();
                            var childrenHTOC = Create<T>(content, child.content.P, filterSingle.Next, possibleDuplicatePositions, ParentChain.Add(parent, new IKIP(key, child.content.P)));
                            if (!retval.TryAddP(key, childrenHTOC, out var errorResponse)) {
                                throw new HTOCException(
                                    "Probable duplicate key (" + key + ").\r\n" +
                                    "The list of content must have unique keys, at least for each level in the hierarchical structure.\r\n" +
                                    "Details: " + errorResponse + "\r\n");
                            }
                        } finally {
                            REx.Dec();
                        }
                    });
                    return retval;
                case FilterBranch filterBranch:
                    filterBranch.Branches.ForEach(branch => {
                        var i = branch.initialContent(parent);
                        var childrenHTOC = Create<T>(content, i.P, branch.firstNode, possibleDuplicatePositions, ParentChain.Add(parent, i));
                        var key = i.Key;
                        if (!retval.TryAddP(key, childrenHTOC, out var errorResponse)) {
                            throw new HTOCException(
                                "Probable duplicate key (" + key + ").\r\n" +
                                "When branching the branches must have unique keys (at point of branching).\r\n" +
                                "Details: " + errorResponse + "\r\n");
                        }
                    });
                    return retval;
                default: throw new NotImplementedException(filterNode.GetType().ToString());
            }
        }

        [ClassMember(Description =
            "Alternative ('override') to -" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "-.\r\n"
        )]
        public string ToHTMLSimpleSingle(bool prepareForLinkInsertion = false, List<IKCoded>? linkContext = null) =>
            ToHTMLSimpleSingle(this, prepareForLinkInsertion, linkContext);

        [ClassMember(Description =
            "Use this method in any class where a table-of-contents type of view is desireable"
        )]
        public static string ToHTMLSimpleSingle(IP ip, bool prepareForLinkInsertion = false, List<IKCoded>? linkContext = null) {
            var retval = new System.Text.StringBuilder();
            retval.Append(UtilDoc.GetCurrentLevelInHierarchyAsHTML(linkContext, out var internalLinks, out var strLinkContext));

            // Part 1), sub chapters (without any text, compact presentation)
            var subChapters = string.Join(", ", ip.Where(ikip => ikip.Key != HTOCContentThisLevelKey).OrderBy(ikip => ikip.Key.ToString()).Select(ikip =>
                  "<a href=\"" +
                  strLinkContext +
                  IKCoded.FromUnencoded(ikip.Key).Encoded +
                  ".html\">" +
                  ikip.Key.KeyToHTMLSimpleWithTooltip() + "</a>\r\n"
            ));
            if (!string.IsNullOrEmpty(subChapters)) {
                retval.Append(
                    "<p>" +
                    // "Sub chapters: " +
                    subChapters +
                    "</p>\r\n"
                );
            }

            if (ip.TryGetP<IP>(HTOCContentThisLevelKey, out var content)) {

                // TODO: Describe assumption about _Description, Description and LongDescription being what we are looking for here.

                // Part 2), Actual content text
                if (content.TryGetPV<string>(DocumentatorP._Description, out var d)) {
                    retval.Append("<hr>\r\n");
                    retval.Append("<p>" + WebUtility.HtmlEncode(d).Replace("\r\n", "<br>\r\n") + "</p>\r\n");
                }

                if (content.TryGetPV<string>(BaseAttributeP.Description, out d)) {
                    retval.Append("<hr>\r\n");
                    retval.Append("<p>" + WebUtility.HtmlEncode(d).Replace("\r\n", "<br>\r\n") + "</p>\r\n");
                }

                if (content.TryGetPV<string>(BaseAttributeP.LongDescription, out d)) {
                    retval.Append("<hr>\r\n");
                    retval.Append("<p>" + WebUtility.HtmlEncode(d).Replace("\r\n", "<br>\r\n") + "</p>\r\n");
                }

                // Part 3), sub chapters (with first line of text)                
                subChapters = string.Join("", ip.Where(ikip => ikip.Key != HTOCContentThisLevelKey).OrderBy(ikip => ikip.Key.ToString()).Select(ikip => {
                    ikip.P.TryGetP<IP>(HTOCContentThisLevelKey, out var subChapterContent);
                    return "<tr><td>" +
                        // (subChapterContent == null || subChapterContent.EnumeratorReturnsEmptyCollection ? // Will actually never happen, superfluous test
                        (subChapterContent == null || !subChapterContent.Any() ? // Will actually never happen, superfluous test
                            ikip.Key.KeyToHTMLSimpleWithTooltip() :
                            (
                                "<a href=\"" +
                                strLinkContext +
                                IKCoded.FromUnencoded(ikip.Key).Encoded +
                                ".html\">" +
                                ikip.Key.KeyToHTMLSimpleWithTooltip() + "</a>"
                            )
                        ) +
                        "</td><td>" + new Func<string>(() => {
                            if (
                                subChapterContent == null ||
                                (
                                    !subChapterContent.TryGetPV<string>(DocumentatorP._Description, out d) &&
                                    !subChapterContent.TryGetPV<string>(BaseAttributeP.Description, out d)
                                )
                            ) {
                                return "&nbsp;";
                            }
                            var pos = d.IndexOf("\r\n"); // Show first line of text only
                            return WebUtility.HtmlEncode(pos == -1 ? d : d[..pos]);
                        })() + "</td>" +
                        "</tr>\r\n";
                }));
                if (!string.IsNullOrEmpty(subChapters)) {
                    retval.Append("<hr>\r\n");
                    retval.Append(
                        // "<p>Sub chapters</p>\r\n" +
                        "<table>\r\n" +
                        subChapters +
                        "</table>\r\n"
                    );
                }

                retval.Append("<hr>\r\n");

                retval.Append(
                    "<a href=\"" +
                    strLinkContext +
                    IKCoded.FromUnencoded(HTOCContentThisLevelKey).Encoded +
                    ".html\">" +
                    "Details</a>\r\n"
                );
            }
            return retval.ToString();
        }

        public class HTOCException : ApplicationException {
            public HTOCException(string message) : base(message) { }
            public HTOCException(string message, Exception inner) : base(message, inner) { }
        }

        public abstract class FilterNode {
        }

        [Class(Description =
            "Used by -" + nameof(FilterNode) + "- in order to keep track of node's parents.\r\n" +
            "\r\n" +
            "This class is immutable.\r\n"
        )]
        public class ParentChain {
            public ParentChain? Parent { get; private set; }
            public IKIP IKIP { get; private set; }
            public ParentChain(IKIP ikip, ParentChain? parent = null) {
                IKIP = ikip;
                Parent = parent;
            }
            public static ParentChain Add(ParentChain? parent, IKIP ikip) => new ParentChain(ikip, parent);
        }

        public class FilterSingle : FilterNode {
            public Func<ParentChain?, IKIP, IK?> Filter { get; private set; }
            public FilterNode? Next { get; private set; }
            public FilterSingle(Func<ParentChain?, IKIP, IK?> filter, FilterNode? next = null) {
                Filter = filter;
                Next = next;
            }
        }

        public class FilterBranch : FilterNode {
            public List<(Func<ParentChain?, IKIP> initialContent, FilterNode firstNode)> Branches { get; private set; }
            public FilterBranch(List<(Func<ParentChain?, IKIP> initialContent, FilterNode firstNode)> branches) => Branches = branches;
        }

        [ClassMember(Description = "Helper method for creation of a -" + nameof(FilterBranch) + "-.")]
        public static IKIP CreateInitialContent(string key, string description) {
            var retval = new PRich();
            //var contentThisLevelc = new PRich();
            //contentThisLevelc.IP.AddPV(DocumentatorP._Description, description);
            //retval.IP[HTOCContentThisLevelKey] = contentThisLevelc;
            retval.IP.AddPV(DocumentatorP._Description, description);
            return new IKIP(IKString.FromString(key), retval);
        }
    }
}