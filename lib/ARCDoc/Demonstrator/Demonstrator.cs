﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using ARCCore;
using System.Linq;

namespace ARCDoc {

    [Class(Description =
        "Demonstrates various concepts from -" + nameof(ARComponents.ARCCore) + "-, especially -" + nameof(ARConcepts.PropertyAccess) + "-.\r\n" +
        "\r\n" +
        "See also classes -" + nameof(Apple) + "-, -" + nameof(Orange) + "- and -" + nameof(Banana) + "-.\r\n" +
        "\r\n" +
        "See also -" + nameof(ARComponents.ARCQuery) + "-, especially -QueryExpression-\r\n" +
        "\r\n" +
        "Used by for instance -" + nameof(ARComponents.ARAClient) + ".\r\n"
    )]
    public static class Demonstrator {

        public static void DemonstrateAll() {
            DemonstratePropertyAccessApple();

            DemonstratePropertyAccessOrange();

            DemonstratePropertyAccessBanana();

            DemonstrateITypeDescriber();

            DemonstratePRichHierarchicalStorage();

            DemonstrateExposingApplicationState();

            DemonstratePropertyStream();

            DemonstrateBareBonesClient();

            DemonstrateStreamProcessorAsClient();
        }

        public static void W(string s = "") => Console.WriteLine("DIRECT TO CONSOLE: " + s.Replace("\r\n", "\r\nDIRECT TO CONSOLE: ")); // Mark as direct written to console in order to distinguish from data originating more deeply from within AgoRapide system (IP.Log, property-stream and similar)

        [ClassMember(
            Description =
                "Demonstrates use of -" + nameof(ARConcepts.PropertyAccess) + "- through the -" + nameof(Apple) + "- class.\r\n" +
                "Note how " +
                "-" + nameof(DemonstratePropertyAccessApple) + "-,\r\n" +
                "-" + nameof(DemonstratePropertyAccessOrange) + "- and\r\n" +
                "-" + nameof(DemonstratePropertyAccessBanana) + "-\r\n" +
                "all contain idential code, that is, the corresponding classes used (Apple, Banana, Orange) behaves in much the same manner."

        )]
        public static void DemonstratePropertyAccessApple() {
            W("\r\n\r\nPress ENTER in order to " + System.Reflection.MethodBase.GetCurrentMethod()?.Name);
            Console.ReadLine();

            W("Create a new Apple");
            var apple = new Apple();

            W("Assert integrity, fails because no Name given (the Name is tagged as obligatory)");
            try {
                apple.IP.AssertIntegrity();
                W("ERROR! Exception should have been thrown, but was not");
            } catch (Exception ex) {
                W("Exception " + ex.GetType().ToStringShort() + " when asserting integrity");
            }

            W("Set property value");
            apple.IP.AddPV(AppleP.Name, "My big juicy apple");

            W("Assert integrity, now it succeeds");
            apple.IP.AssertIntegrity();

            W("Get property value");
            W(apple.IP.GetPV<string>(AppleP.Name));

            W("Get property value, alternative");
            W(apple.IP["Name"].GetV<string>());

            W("Get property value, convenience method, traditional getter");
            W(apple.Name);

            W("Set property value, but with wrong type. Exception is thrown");
            try {
                apple.IP.SetPV(AppleP.Name, 42);
                W("ERROR! Exception should have been thrown, but was not");
            } catch (Exception ex) {
                W("Exception " + ex.GetType().ToStringShort() + " when setting property value with wrong type");
            }

            W("Get property type (in this case a PValue<string>-object");
            W("(Would have been more relevant if property had been a more complex object)");
            W(apple.IP.GetP<IP>(AppleP.Name).GetType().ToStringShort());

            W("Get property value, supply default value");
            W(apple.IP.GetPV(AppleP.Colour, defaultValue: ConsoleColor.Green).ToString());

            W("Get property, exception is thrown because does not exist");
            try {
                W(apple.IP.GetPV<ConsoleColor>(AppleP.Colour).ToString());
                W("ERROR! Exception should have been thrown, but was not");
            } catch (Exception ex) {
                W("Exception " + ex.GetType().ToStringShort() + " when asking for non-existing property");
            }

            W("Try-pattern when asking for property (in this case it fails)");
            W(apple.IP.TryGetPV<ConsoleColor>(AppleP.Colour, out var c, out var errorResponse) ? c.ToString() : "No colour found. Details: " + errorResponse);

            W("Adding property");
            apple.IP.AddPV(AppleP.Colour, ConsoleColor.Green);

            W("Try-pattern when asking for property (now it succeeds)");
            W(apple.IP.TryGetPV<ConsoleColor>(AppleP.Colour, out c) ? c.ToString() : "No colour found");

            W("Ask for 'mapped' property");
            W(apple.IP.GetPVM<ConsoleColor>().ToString());

            W("Adding property value, exception is thrown because already exists");
            try {
                apple.IP.AddPV(AppleP.Colour, ConsoleColor.Red);
                W("ERROR! Exception should have been thrown, but was not");
            } catch (Exception ex) {
                W("Exception " + ex.GetType().ToStringShort() + " when setting property already set");
            }

            W("Set property value (Add or update)");
            apple.IP.SetPV(AppleP.Colour, ConsoleColor.Red);

            W("Set property, alternative");
            apple.IP["Colour"] = new PValue<ConsoleColor>(ConsoleColor.Red);

            W("Set property (supply property object instead of only value)");
            apple.IP.SetP(AppleP.Colour, new PValue<ConsoleColor>(ConsoleColor.Red));

            W("Show all properties");
            W(string.Join("\r\n", apple.IP.ToPropertyStream()));

            W("Creating a deep copy and showing its properties");
            W(string.Join("\r\n", apple.DeepCopy().ToPropertyStream()));
        }

        [ClassMember(
            Description =
                "Demonstrates use of -" + nameof(ARConcepts.PropertyAccess) + "- through the -" + nameof(Orange) + "- class.\r\n" +
                "Note how " +
                "-" + nameof(DemonstratePropertyAccessApple) + "-,\r\n" +
                "-" + nameof(DemonstratePropertyAccessOrange) + "- and\r\n" +
                "-" + nameof(DemonstratePropertyAccessBanana) + "-\r\n" +
                "all contain idential code, that is, the corresponding classes used (Apple, Banana, Orange) behaves in much the same manner."

        )]
        public static void DemonstratePropertyAccessOrange() {
            W("\r\n\r\nPress ENTER in order to " + System.Reflection.MethodBase.GetCurrentMethod()?.Name);
            Console.ReadLine();

            W("Create a new Orange");
            var orange = new Orange();

            W("Assert integrity, fails because no Name given (the Name is tagged as obligatory)");
            try {
                orange.IP.AssertIntegrity();
                W("ERROR! Exception should have been thrown, but was not");
            } catch (Exception ex) {
                W("Exception " + ex.GetType().ToStringShort() + " when asserting integrity");
            }

            W("Add property value");
            orange.IP.AddPV(OrangeP.Name, "My sweet Orange");

            W("Assert integrity, now it succeeds");
            orange.IP.AssertIntegrity();

            W("Get property value");
            W(orange.IP.GetPV<string>(OrangeP.Name));

            W("Get property value, convenience method, traditional getter");
            W(orange.Name ?? "[NULL]");

            W("Set property value, but with wrong type. Exception is thrown");
            try {
                orange.IP.SetPV(OrangeP.Name, 42);
                W("ERROR! Exception should have been thrown, but was not");
            } catch (Exception ex) {
                W("Exception " + ex.GetType().ToStringShort() + " when setting property value with wrong type");
            }

            W("Get property type (in this case a PValue<string>-object");
            W("(Would have been more relevant if property had been a more complex object)");
            W(orange.IP.GetP<IP>(OrangeP.Name).GetType().ToStringShort());

            W("Get property value, supply default value");
            W(orange.IP.GetPV(OrangeP.Colour, ConsoleColor.Green).ToString());

            W("Get property, exception is thrown because does not exist");
            try {
                W(orange.IP.GetPV<ConsoleColor>(OrangeP.Colour).ToString());
                W("ERROR! Exception should have been thrown, but was not");
            } catch (Exception ex) {
                W("Exception " + ex.GetType().ToStringShort() + " when asking for non-existing property");
            }

            W("Try-pattern when asking for property (in this case it fails)");
            W(orange.IP.TryGetPV<ConsoleColor>(OrangeP.Colour, out var c) ? c.ToString() : "No colour found");

            W("Adding property");
            orange.IP.AddPV(OrangeP.Colour, ConsoleColor.Green);

            W("Try-pattern when asking for property (now it succeeds)");
            W(orange.IP.TryGetPV<ConsoleColor>(OrangeP.Colour, out c) ? c.ToString() : "No colour found");

            W("Ask for 'mapped' property");
            W(orange.IP.GetPVM<ConsoleColor>().ToString());

            W("Adding property, exception is thrown because already exists");
            try {
                orange.IP.AddPV(OrangeP.Colour, ConsoleColor.Red);
                W("ERROR! Exception should have been thrown, but was not");
            } catch (Exception ex) {
                W("Exception " + ex.GetType().ToStringShort() + " when setting property already set");
            }

            W("Set property value (Add or update)");
            orange.IP.SetPV(OrangeP.Colour, ConsoleColor.Red);

            W("Set property, alternative");
            orange.IP["Colour"] = new PValue<ConsoleColor>(ConsoleColor.Red);

            W("Set property (supply property object instead of only value)");
            orange.IP.SetP(OrangeP.Colour, new PValue<ConsoleColor>(ConsoleColor.Red));

            W("Show all properties");
            W(string.Join("\r\n", orange.IP.ToPropertyStream()));

            W("Creating a deep copy and showing its properties");
            W(string.Join("\r\n", orange.DeepCopy().ToPropertyStream()));
        }

        [ClassMember(
            Description =
                "Demonstrates use of -" + nameof(ARConcepts.PropertyAccess) + "- through the -" + nameof(Banana) + "- class.\r\n" +
                "Note how " +
                "-" + nameof(DemonstratePropertyAccessApple) + "-,\r\n" +
                "-" + nameof(DemonstratePropertyAccessOrange) + "- and\r\n" +
                "-" + nameof(DemonstratePropertyAccessBanana) + "-\r\n" +
                "all contain idential code, that is, the corresponding classes used (Apple, Banana, Orange) behaves in much the same manner."
        )]
        public static void DemonstratePropertyAccessBanana() {
            W("\r\n\r\nPress ENTER in order to " + System.Reflection.MethodBase.GetCurrentMethod()?.Name);
            Console.ReadLine();

            W("Create a new Banana");
            var banana = new Banana();

            W("Assert integrity, fails because no Name given (the Name is tagged as obligatory)");
            try {
                banana.IP.AssertIntegrity();
                W("ERROR! Exception should have been thrown, but was not");
            } catch (Exception ex) {
                W("Exception " + ex.GetType().ToStringShort() + " when asserting integrity");
            }

            W("Add property value");
            banana.IP.AddPV(BananaP.Name, "My sweet banana");

            W("Assert integrity, now it succeeds");
            banana.IP.AssertIntegrity();

            W("Get property value");
            W(banana.IP.GetPV<string>(BananaP.Name));

            W("Get property value, convenience method, traditional getter");
            W(banana.Name ?? "[NULL]");

            W("Set property value, but with wrong type. Exception is thrown");
            try {
                banana.IP.SetPV(BananaP.Name, 42);
                W("ERROR! Exception should have been thrown, but was not");
            } catch (Exception ex) {
                W("Exception " + ex.GetType().ToStringShort() + " when setting property value with wrong type");
            }

            W("Get property type (in this case a PValue<string>-object");
            W("(Would have been more relevant if property had been a more complex object)");
            W(banana.IP.GetP<IP>(BananaP.Name).GetType().ToStringShort());

            W("Get property, supply default value");
            W(banana.IP.GetPV(BananaP.Colour, ConsoleColor.Green).ToString());

            W("Get property, exception is thrown because does not exist");
            try {
                W(banana.IP.GetPV<ConsoleColor>(BananaP.Colour).ToString());
                W("ERROR! Exception should have been thrown, but was not");
            } catch (Exception ex) {
                W("Exception " + ex.GetType().ToStringShort() + " when asking for non-existing property");
            }

            W("Try-pattern when asking for property (in this case it fails)");
            W(banana.IP.TryGetPV<ConsoleColor>(BananaP.Colour, out var c) ? c.ToString() : "No colour found");

            W("Adding property");
            banana.IP.AddPV(BananaP.Colour, ConsoleColor.Green);

            W("Try-pattern when asking for property (now it succeeds)");
            W(banana.IP.TryGetPV<ConsoleColor>(BananaP.Colour, out c) ? c.ToString() : "No colour found");

            W("Ask for 'mapped' property");
            W(banana.IP.GetPVM<ConsoleColor>().ToString());

            W("Adding property, exception is thrown because already exists");
            try {
                banana.IP.AddPV(BananaP.Colour, ConsoleColor.Red);
                W("ERROR! Exception should have been thrown, but was not");
            } catch (Exception ex) {
                W("Exception " + ex.GetType().ToStringShort() + " when setting property already set");
            }

            W("Set property (Add or update)");
            banana.IP.SetPV(BananaP.Colour, ConsoleColor.Red);

            W("Set property, alternative, exception is thrown because string-keys not understood");
            try {
                banana.IP["Colour"] = new PValue<ConsoleColor>(ConsoleColor.Red);
                W("ERROR! Exception should have been thrown, but was not");
            } catch (Exception ex) {
                W("Exception " + ex.GetType().ToStringShort() + " when setting property with string key.");
            }

            W("Set property, alternative");
            banana.IP[PK.FromEnum(BananaP.Colour)] = new PValue<ConsoleColor>(ConsoleColor.Red);

            W("Set property (supply property object instead of only value)");
            banana.IP.SetP(BananaP.Colour, new PValue<ConsoleColor>(ConsoleColor.Red));

            W("Show all properties");
            W(string.Join("\r\n", banana.IP.ToPropertyStream()));

            W("Creating a deep copy and showing its properties");
            W(string.Join("\r\n", banana.DeepCopy().ToPropertyStream()));
        }

        [ClassMember(Description =
            "Demonstrates use of -" + nameof(ITypeDescriber) + "- for describing single-property values."
        )]
        public static void DemonstrateITypeDescriber() {
            W("\r\n\r\nPress ENTER in order to " + System.Reflection.MethodBase.GetCurrentMethod()?.Name);
            Console.ReadLine();

            var parser = new Action<PK, List<string>>((key, list) => {
                W("\r\n\r\nPress ENTER in order to parse " + key.Type.ToStringShort());
                Console.ReadLine();
                list.ForEach(s => {
                    W("\r\nAttempting to parse '" + s + "' as a " + key.Type.ToStringShort() + ":");
                    W(key.TryCleanValidateAndParse(s, out var result) ?
                        (
                            "Success, result " +
                            "'" +
                            key.PackParseResultForStorageInEntityObject(result).GetV<string>() +  /// Note, by packing into (presumable a PValue<T>), we get smarter string-representation, especially if this is a collection of items <see cref="Cardinality"/>
                            "'"
                        ) :
                        (
                            "Error, details are: " + result.ErrorResponse
                        )
                    );
                });
            });

            parser(
                PK.FromEnum(AppleP.Colour),
                new List<string> {
                    "Random text",
                    "Red",
                    "\r\n Red", /// Demonstrates use of <see cref="PK.Cleaner"/>
                }
            );

            parser(
                PK.FromEnum(ActualConnectionP.ClientUpdatePosition),
                new List<string> {
                    "Random text",
                    "Filename;1234",
                    "\r\n Filename;1234", /// Demonstrates use of <see cref="PK.Cleaner"/>
                }
            );

            parser(
                PK.FromEnum(ConnectionInstructionP.Subscription),
                new List<string> {  // TODO: 18 Apr 2020. Fix this, everything is OK now and use of List<> is not shown
                    "Random text",
                    "+* ; -" + nameof(PSPrefix.app) + "/*", /// Demonstrates use of List<> because <see cref="Cardinality.WholeCollection"/>.
                }
            );
        }

        [ClassMember(Description =
            "Demonstrates use of -" + nameof(PRich) + "-, how it receives -" + nameof(ARConcepts.PropertyStream) + "--lines and " +
            "builds up a strongly typed hierarchical object storage.\r\n" +
            "\r\n" +
            "Also demonstrates how the original -" + nameof(ARConcepts.PropertyStream) + "- can be reconstructed, " +
            "and how everything can be turned into an HTML representation (" + nameof(HLocationCollection.Create) + ")."
        )]
        public static void DemonstratePRichHierarchicalStorage() {
            W("\r\n\r\nPress ENTER in order to " + System.Reflection.MethodBase.GetCurrentMethod()?.Name);
            Console.ReadLine();

            var p = new PRich();
            var parser = new Action<string>(s => PropertyStreamLine.ParseAndStore(p, s));
            parser("Apple/42/Name = MyApple");

            parser("Apple/42/Colour = Red");

            try {
                parser("Apple/42/Colour = Redd");
                W("ERROR! Exception should have been thrown, but was not");
            } catch (Exception ex) {
                W("Exception " + ex.GetType().ToStringShort() + " when setting an invalid data value (Colour = Redd)");
            }

            // Parse directly
            parser("Apple/42/RandomDatapoint = SomeValue"); // As of May 2020 no way to specify that only AppleP-keys may be added. Probably an OK feature.
            parser("Apple/42/+ComplicatedKey* = +SomeComplicatedValue*\r\n/"); /// Demonstrates encoding. Note more characters allowed in value part then key part (compare <see cref="PropertyStreamLine.AllowedKeyChars"/> and <see cref="PropertyStreamLine.AllowedValueChars"/>)

            // Parse directly
            parser("Apple/43/Name = MyOtherApple");
            parser("Apple/43/Colour = Blue");
            parser("Customer/42/FirstName = John");
            parser("Customer/42/LastName = Smith");
            parser("Customer/43/LastName = Willis");

            // Set directly using indexing
            p.IP["Customer"]["44"] = new PRich();
            p.IP["Customer"]["44"]["FirstName"] = new PValue<string>("Mary");

            // Lookup through GetP
            W("\r\nType of PRich[\"Apple\"]\"42\"] is: " +
                 p.IP.
                    GetP<IP>(IKType.FromType(typeof(Apple))). // Note: It does not matter if you use GetP(IKString.FromCache("Apple")) instead.
                    GetP<Apple>(IKString.FromString("42")).
                    GetType().ToString()); // Note that strongly typed (typeof(Apple))
            W();

            // Lookup through combination of IK and string-based indexing
            W("\r\nType of PRich[\"Apple\"]\"42\"] is: " +
                p.IP[IKType.FromType(typeof(Apple))]["42"].GetType().ToString()); // Note that strongly typed (typeof(Apple))
            W();

            // Lookup through only string-based indexing
            W("\r\nType of PRich[\"Apple\"]\"42\"] is: " +
                p.IP["Apple"]["42"].GetType().ToString()); // Note that strongly typed (typeof(Apple))
            W();


            W("\r\n\r\nPress ENTER in order to call ToPropertyStream for whole PRich");
            Console.ReadLine();
            W(string.Join("\r\n", p.IP.ToPropertyStream()));

            W("\r\n\r\nPress ENTER in order to call ToPropertyStream for PRich['Apple']");
            Console.ReadLine();
            W(string.Join("\r\n", p.IP.GetP<IP>(IKType.FromType(typeof(Apple))).ToPropertyStream()));

            W("\r\n\r\nPress ENTER in order to call ToPropertyStream for PRich['Apple']['42']");
            Console.ReadLine();
            W(string.Join("\r\n", p.IP.
                GetP<IP>(IKType.FromType(typeof(Apple))).
                GetP<Apple>(IKString.FromString("42")).
                IP.ToPropertyStream()));
            W();

            W("\r\n\r\nPress ENTER in order to call ToHTMLSimpleSingle for whole PRich");
            Console.ReadLine();
            W("\r\n" + p.ToHTMLSimpleSingle());

            W("\r\n\r\nPress ENTER in order to call ToHTMLSimpleSingle for PRich['Apple']");
            Console.ReadLine();
            W("\r\n" + p.IP.GetP<IP>(IKType.FromType(typeof(Apple))).ToHTMLSimpleSingle());
            W();

            W("\r\n\r\nPress ENTER in order to call ToHTMLSimpleSingle for PRich['Apple']['42']");
            Console.ReadLine();
            W("\r\n" + p.IP.
                GetP<IP>(IKType.FromType(typeof(Apple))).
                GetP<Apple>(IKString.FromString("42")).
                ToHTMLSimpleSingle());
            W();

            W("\r\n\r\nPress ENTER in order to generate complete HTML representation of PRich");
            Console.ReadLine();
            var locations = HLocationCollection.Create(p);
            W("\r\n\r\n" + locations.Count() + " files generated.");

            var d = Environment.CurrentDirectory + System.IO.Path.DirectorySeparatorChar + nameof(DemonstratePRichHierarchicalStorage) + System.IO.Path.DirectorySeparatorChar;
            W("Press ENTER in order to " + nameof(HLocationCollection.WriteAllContentHTMLToDisk));
            W("Files will be written to " + d);
            Console.ReadLine();
            locations.WriteAllContentHTMLToDisk(d); // Note: No link insertion is done now (irrelevant, will not contain text with links)
            W("Complete, written " + locations.Count() + " files");
        }

        public static void DemonstrateExposingApplicationState() {
            W("\r\n\r\nPress ENTER in order to " + System.Reflection.MethodBase.GetCurrentMethod()?.Name);
            Console.ReadLine();
            var p = new SomeAppComponent();
            p.IP.Logger = s => Console.WriteLine(PSPrefix.app + "/" + typeof(SomeAppComponent).ToStringVeryShort() + "/" + s);

            W("Changing some values will automatically get logged");
            p.IP.Inc(SomeAppComponentP.SomeCountValue);
            p.IP.Inc(SomeAppComponentP.SomeCountValueNotToBeLogged);
            p.IP.Dec(SomeAppComponentP.SomeCountValue);
            W();
            W(string.Join("\r\n", p.IP.ToPropertyStream()));
            W();

            W("Calling some methods and showing how they log their execution");
            p.MethodA();
            W();
            p.MethodB();
            W();
            p.MethodC(parameterA: "Value of A", parameterB: "Value of B");
            W();
            W("Result of call to " + nameof(p.MethodD) + " is: " + p.MethodD(parameterA: "Value of A", parameterB: "Value of B"));
            W();
        }

        [ClassMember(Description =
            "Demonstrates use of -" + nameof(ARConcepts.PropertyStream) + "- by generating local data (documentation data) " +
            "and writing locally to console.\r\n" +
            "\r\n" +
            "Also generates documentation, does -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "- and writes everything to disk as HTML."
        )]
        public static void DemonstratePropertyStream() {
            W("\r\n\r\nPress ENTER in order to " + System.Reflection.MethodBase.GetCurrentMethod()?.Name);
            Console.ReadLine();

            W("\r\n\r\nPress ENTER in order to call " + nameof(DocFragCollection) + "." + nameof(DocFragCollection.Create) + ".");
            Console.ReadLine();
            var docFrags = DocFragCollection.Create(UtilCore.Assemblies);

            W("\r\n\r\nPress ENTER in order to call " + nameof(Documentator) + "." + nameof(Documentator.CreateHTOCStandard) + ".");
            Console.ReadLine();
            var doc = Documentator.CreateHTOCStandard(docFrags);

            W("\r\n\r\nPress ENTER in order to call " + nameof(IP.ToPropertyStream));
            Console.ReadLine();
            var stream = doc.IP.ToPropertyStream();

            W("\r\n\r\nPress ENTER in order to show property stream");
            Console.ReadLine();
            W(string.Join("\r\n", stream));

            W("\r\n\r\nPress ENTER in order to " + nameof(HLocationCollection) + "." + nameof(HLocationCollection.Create));
            Console.ReadLine();
            var hLocations = HLocationCollection.Create(doc, excludeKey: HTOC.HTOCContentThisLevelKey);

            var d = Environment.CurrentDirectory + System.IO.Path.DirectorySeparatorChar + nameof(DemonstratePropertyStream) + System.IO.Path.DirectorySeparatorChar;
            W("Press ENTER in order to " + nameof(HLocationCollection.WriteAllContentHTMLToDisk) + " (with link insertion)");
            W("Files will be written to " + d);
            Console.ReadLine();
            hLocations.WriteAllContentHTMLToDisk(d, doLinkInsertion: true);
            W("Complete, written " + hLocations.Count() + " files");
        }

        static bool _stopCoreDBThreads = false;
        [ClassMember(
            Description =
                "Demonstrates adding -" + nameof(ARConcepts.PropertyStream) + "--lines to -" + nameof(ARNodeType.CoreDB) + "- " +
                "without using ANY AgoRapide component, only standard TCP/IP functionality in .NET.\r\n" +
                "(has no mechanism for getting data back from the core).\r\n" +
                "\r\n" +
                "Contains two methods, -" + nameof(DemonstrateBareBonesClient) + "- and -" + nameof(SendToCoreDB) + "- " +
                "plus some static variables at class-level (and of course -" + nameof(W) + "- used for writing to local console).\r\n" +
                "\r\n" +
                "The typical usage scenario here would be using AgoRapide for logging.\r\n" +
                "\r\n" +
                "You may copy-paste this code as template for feeding data from other applications into an AgoRapide based system.\r\n" +
                "\r\n" +
                "See -" + nameof(PropertyStreamLine.EncodeValuePart) + "- for an example of " +
                "a C# single-line encoder if you want to send any special characters (especially semicolons and linebreaks)."
        )]
        public static void DemonstrateBareBonesClient() {
            W("\r\n\r\nPress ENTER in order to " + System.Reflection.MethodBase.GetCurrentMethod()?.Name);
            Console.ReadLine();

            _stopCoreDBThreads = false;
            System.Threading.Tasks.Task.Factory.StartNew(() => {
                try {
                    System.Threading.Thread.CurrentThread.Name = nameof(DemonstrateBareBonesClient) + "_ContinousLoop";
                    System.Threading.Thread.CurrentThread.IsBackground = true; // TODO: In principle we should set this BEFORE thread is started
                    while (true) {
                        /// See <see cref="PropertyStreamLine.EncodeValuePart"/> for an example of
                        /// a C# single-line encoder if you want to send any special characters (especially semicolons and linebreaks)
                        SendToCoreDB("SomeData/FromClient = " + UtilCore.DateTimeNow.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        System.Threading.Thread.Sleep(1000);
                        if (_stopCoreDBThreads) {
                            W(System.Threading.Thread.CurrentThread.Name + ": EXITING");
                            break;
                        }
                    }
                } catch (Exception ex) {
                    // Your exception handler here.
                    W(UtilCore.GetExeptionDetails(ex));
                }
            });

            W("CoreDB is now continously being fed data. Press ENTER in order to stop feeding");
            Console.ReadLine();
            _stopCoreDBThreads = true;
        }

        private static int _coreDBTaskIsRunning = 0;
        private static readonly string _coreDBHostName = "localhost";
        private static readonly int _coreDBPort = 4246;
        private static readonly ConcurrentQueue<string> _coreDBQueue = new ConcurrentQueue<string>();
        [ClassMember(Description =
            "Sends data to -" + nameof(ARNodeType.CoreDB) + "-, opens and keeps open a TCP/IP connection as needed " +
            "(in a separate background thread)"
        )]
        private static void SendToCoreDB(string data) {
            _coreDBQueue.Enqueue(data);

            var wasRunning = System.Threading.Interlocked.Exchange(ref _coreDBTaskIsRunning, 1);
            if (wasRunning != 0) {
                // Only first call 
                return;
            }
            System.Threading.Tasks.Task.Factory.StartNew(() => {
                System.Threading.Thread.CurrentThread.Name = nameof(SendToCoreDB) + "_ContinousLoop";
                System.Threading.Thread.CurrentThread.IsBackground = true; // TODO: In principle we should set this BEFORE thread is started
                try {

                    do {
                        try {
                            W("Opening connection to " + _coreDBHostName + ": " + _coreDBPort);
                            var client = new System.Net.Sockets.TcpClient(_coreDBHostName, _coreDBPort);
                            using var networkStream = client.GetStream();
                            using var w = new System.IO.StreamWriter(networkStream, Encoding.UTF8) { AutoFlush = true };
                            do {
                                if (_coreDBQueue.TryPeek(out var line)) {
                                    if (_stopCoreDBThreads) {
                                        W(System.Threading.Thread.CurrentThread.Name + ": EXITING (inner loop)");
                                        break;
                                    }
                                    w.WriteLine(line);
                                    W("Successfully sent '" + line + "' to CoreDB");
                                    _coreDBQueue.TryDequeue(out _); // Note how we only empty from queue after presumed successful write
                                } else {
                                    System.Threading.Thread.Sleep(10); // TODO: Non-optimal solution
                                    if (_stopCoreDBThreads) {
                                        W(System.Threading.Thread.CurrentThread.Name + ": EXITING (inner loop)");
                                        break;
                                    }
                                }
                            } while (true);
                        } catch (Exception ex) {
                            // Your exception handler here.
                            W(UtilCore.GetExeptionDetails(ex));
                            W("\r\n\r\n\r\n");
                            W(System.Threading.Thread.CurrentThread.Name + ": Sleeping for 20 seconds, will attempt reconnect afterwards");
                            for (var i = 0; i < 100; i++) {
                                System.Threading.Thread.Sleep(200);
                                if (_stopCoreDBThreads) {
                                    break; // Message will be written below:
                                }
                            }
                        }
                        if (_stopCoreDBThreads) {
                            W(System.Threading.Thread.CurrentThread.Name + ": EXITING (outer loop)");
                            break;
                        }
                    } while (true);
                } catch (Exception ex) {
                    // This should "never" happen
                    // Your exception handler here.
                    W(UtilCore.GetExeptionDetails(ex));
                } finally {
                    _coreDBTaskIsRunning = 0;
                }
            });
        }

        [ClassMember(Description =
            "Demonstrates a quite simple log console.\r\n"
        )]
        public static void DemonstrateLogConsole(ConnectionInstruction connectionInstruction) {
            connectionInstruction.Initialize();
            W(nameof(connectionInstruction));
            W("\r\n" + string.Join("\r\n", connectionInstruction.IP.ToPropertyStream()) + "\r\n\r\n");

            W("\r\nCreating StreamProcessor");
            using var streamProcessor = StreamProcessor.CreateBareBonesInstance(IKString.FromString("LogConsole"), writeToConsole: true, doNotStoreLocally: true);
            streamProcessor.Initialize();
            streamProcessor.AddOrRemoveOutgoingConnections(new List<ConnectionInstruction> {
                connectionInstruction
            });
            /// Stream processor will now connect and subscribe according to <see cref="ConnectionInstructionP.Subscription"/>
            streamProcessor.StartTCPIPCommunication();

            W("StreamProcessor is now running. Press ENTER in order to stop StreamProcessor");
            Console.ReadLine();
        }

        [ClassMember(Description =
            "Demonstrates use of -" + nameof(StreamProcessor) + "- as a general client.\r\n" +
            "Demonstration includes thread-safe processing of data received from the -" + nameof(Subscription) + "-."
        )]
        public static void DemonstrateStreamProcessorAsClient() {
            W("\r\n\r\nPress ENTER in order to " + System.Reflection.MethodBase.GetCurrentMethod()?.Name);
            Console.ReadLine();

            W("\r\nCreating StreamProcessor");
            using var streamProcessor = StreamProcessor.CreateBareBonesInstance(
                IKString.FromString(nameof(ARNodeType.Client)),
                writeToConsole: true,
                cacheDiskWrites: true
            );
            streamProcessor.Initialize();

            W("\r\nPress ENTER in order to continue");
            W("Note: The server must be running now");
            Console.ReadLine();

            streamProcessor.AddOrRemoveOutgoingConnections(new List<ConnectionInstruction> {
                ConnectionInstruction.CreateAndInitialize("localhost", serverPortNo: 4246, new List<Subscription> { Subscription.Parse("+*") }), // Subscribe to everything (this is also default setting)
                ConnectionInstruction.CreateAndInitialize("localhost", serverPortNo: 4246, dataTransferDirection: DataTransferDirection.Send)
            });

            // NOTE: It is only relevant to have an incoming queue at all if you have some processing tasks like 
            // NOTE: DoTask1 and DoTask2 indicated below
            var incomingQueue = new ConcurrentQueue<string>();
            streamProcessor.OutsideLocalReceiver = s => incomingQueue.Enqueue(s);
            streamProcessor.StartTCPIPCommunication();

            var stopThread = false;
            var dataStorage = new PRich();
            System.Threading.Tasks.Task.Factory.StartNew(() => {
                try {
                    System.Threading.Thread.CurrentThread.Name = nameof(DemonstrateStreamProcessorAsClient) + "_ContinousLoop";
                    System.Threading.Thread.CurrentThread.IsBackground = true;
                    while (true) {

                        // If you want to do some local processing on the subscribed data being received continously
                        // you can for instance call methods like this
                        // DoTask1(dataStorage); // Task1 may freely access data storage (within calling thread)
                        // DoTask2(dataStorage); // Task1 may freely access data storage (within calling thread)

                        streamProcessor.SendFromLocalOrigin(nameof(PSPrefix.app) + "/Client/1/Heartbeat = " + UtilCore.DateTimeNow.ToStringDateAndTime());
                        W("Sleeping for 20 seconds");
                        streamProcessor.IP.LogText("Sleeping for 20 seconds (processing every 200 millisecond)");
                        for (var i = 0; i < 100; i++) {
                            // NOTE: It is only relevant to have an incoming queue at all if you have some processing tasks like 
                            // NOTE: DoTask1 and DoTask2 indicated above.
                            while (incomingQueue.TryDequeue(out var s)) PropertyStreamLine.ParseAndStore(dataStorage, s);
                            System.Threading.Thread.Sleep(200);
                            if (stopThread) break;
                        }
                        if (stopThread) {
                            W(System.Threading.Thread.CurrentThread.Name + ": EXITING");
                            break;
                        }
                        W("Finished sleeping");
                        streamProcessor.IP.LogText("Finished sleeping");
                    }
                } catch (Exception ex) {
                    W(UtilCore.GetExeptionDetails(ex));
                }
            });

            W("StreamProcessor is now running. Press ENTER in order to stop StreamProcessor");
            Console.ReadLine();
            stopThread = true;
        }
    }
}