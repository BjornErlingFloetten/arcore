﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCDoc {
    [Class(
        Description =
            "The most 'standard' way of implementing an entity-class in AgoRapide, by just inheriting -" + nameof(PRich) + "-.\r\n" +
            "\r\n" +
            "It is somewhat memory-hungry (due to inheriting -" + nameof(PRich) + "-) but very easy to implement.\r\n" +
            "Can store any property, not only just those contained in -" + nameof(AppleP) + "- enum.\r\n" +
            "\r\n" +
            "Note that apart from the convenience method -" + nameof(Name) + "- (which is not obligatory to implement), " +
            "the Apple class itself is totally empty.\r\n" +
            "\r\n" +
            "All the descriptions of this class (except this text) are found in -" + nameof(AppleP) + "- " +
            "something which is consistent with the principle of -" + nameof(ARConcepts.TaggingOfPropertyKeys) + "-.\r\n" +
            "\r\n" +
            "See also -" + nameof(ARConcepts.MemoryConsumption) + "- / -" + nameof(ARConcepts.PropertyAccess) + "-.\r\n" +
            "\r\n" +
            "Related examples of entity classes, all of which implement -" + nameof(IP) + "- and offer the properties 'Name' and 'Colour' " +
            "but which differ in their storage mechanisms:\r\n" + 
            "-" + nameof(Apple) + "-: The most 'standard' way of implementing an entity-class in AgoRapide, by just inheriting -" + nameof(PRich) + "-.\r\n" +
            "-" + nameof(Orange) + "- Inherits -" + nameof(PExact<TPropertyKeyEnum>) + "- for probably the best compromise between memory consumption and easy of development.\r\n" +
            "-" + nameof(Banana) + "-: Implements -" + nameof(IP) + "- directly and uses traditional property storage for optimum -" + nameof(ARConcepts.MemoryConsumption) + "- efficiency.\r\n" +
            ""
    )]
    public class Apple : PRich {

        [ClassMember(
            Description =
                "Convenience method, not obligatory.\r\n" +
                "\r\n" +
                "Traditional getter / setter.\r\n" +
                "\r\n" +
                "If you find AgoRapide's standard syntax for -" + nameof(ARConcepts.PropertyAccess) + "- too convoluted, " +
                "then you can just implement traditional setters / getters like this one, for every property.\r\n" +
                "(in this class, only the property -" + nameof(AppleP.Name) +" - has been implemented as such, not -" + nameof(AppleP.Colour) + "-).\r\n" +
                "\r\n" +
                "Note that in order to get all the rich functionality of -" + nameof(ARConcepts.PropertyAccess) + "- you have to " +
                "implement additional methods also, like " +
                "-" + nameof(AddName) + "-, " +
                "-" + nameof(GetName) + "- (two overloads, one potentially returning null, the other returning a specified default value) and " +
                "-" + nameof(TryGetName) + "-." +
                "\r\n" +
                "See also -" + nameof(PKTypeAttributeP.BoilerplateCodeProperties) + "- and -" + nameof(ClassAttributeP.BoilerplateCodePropertiesAndStorage) + "-."
        )]
        public string Name {
            get => IP.GetPV<string>(AppleP.Name);
            set => IP.SetPV(AppleP.Name, value);
        }

        [ClassMember(
            Description =
                "Convenience method, not obligatory.\r\n" +
                "\r\n" +
                "See property -" + nameof(Name) + "- for explanation"
        )]
        public void AddName(string name) => IP.AddPV(AppleP.Name, name);

        [ClassMember(
            Description =
                "Convenience method, not obligatory.\r\n" +
                "\r\n" +
                "See property -" + nameof(Name) + "- for explanation"
        )]
        public string? GetName() => TryGetName(out var retval) ? retval : null;
        // Note: Do not add [ClassMember(Description = "... for all overloads unless it actually differs. It will only lead to confusing compiled documentation.
        public string GetName(string defaultValue) => TryGetName(out var retval) ? retval : defaultValue;

        [ClassMember(
            Description =
                "Convenience method, not obligatory.\r\n" +
                "\r\n" +
                "See property -" + nameof(Name) + "- for explanation"
        )]
        public bool TryGetName(out string name) => IP.TryGetPV(AppleP.Name, out name);

    }

    [Enum(
        AREnumType = AREnumType.PropertyKeyEnum,
        Description = "Describes class -" + nameof(Apple) + "-."
    )]
    public enum AppleP {

        // All AgoRapide enum's start with __invalid (value 0) in order for a non-initialized value (default 0) not to be mistaken as a data-value.
        __invalid,

        [PKType(
            Description = "The name of the apple",
            IsObligatory = true
        )]
        Name,

        [PKType(Type = typeof(ConsoleColor))]
        Colour,
    }
}