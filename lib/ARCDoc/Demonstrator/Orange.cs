﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCDoc {

    [Class(
        Description =
            "Inherits -" + nameof(PExact<TPropertyKeyEnum>) + "- for probably the best compromise between memory consumption and easy of development.\r\n" +
            "\r\n" +
            "See also -" + nameof(ARConcepts.MemoryConsumption) + "- / -" + nameof(ARConcepts.PropertyAccess) + "-.\r\n" +
            "\r\n" +
            "Related examples of entity classes, all of which implement -" + nameof(IP) + "- and offer the properties 'Name' and 'Colour' " +
            "but which differ in their storage mechanisms:\r\n" +
            "-" + nameof(Apple) + "-: The most 'standard' way of implementing an entity-class in AgoRapide, by just inheriting -" + nameof(PRich) + "-.\r\n" +
            "-" + nameof(Orange) + "- Inherits -" + nameof(PExact<TPropertyKeyEnum>) + "- for probably the best compromise between memory consumption and easy of development.\r\n" +
            "-" + nameof(Banana) + "-: Implements -" + nameof(IP) + "- directly and uses traditional property storage for optimum -" + nameof(ARConcepts.MemoryConsumption) + "- efficiency.\r\n" +
            ""
    )]
    public class Orange : PExact<OrangeP> {
        public Orange() : base(capacity: 2) { } // Name + Colour => two properties. NOTE: Explicit value '2' here can be replaced with 'capacity' if you also create the field "private static readonly int capacity = Enum.GetNames(typeof(OrangeP)).Length - 1; // 1 being subtracted from the length since the first enum value (__invalid) is not used."

        [ClassMember(Description = "See -" + nameof(Apple.Name) + "- for more information.")]
        public string Name {
            get => IP.GetPV<string>(OrangeP.Name);
            set => IP.SetPV(OrangeP.Name, value);
        }
    }

    [Enum(
        AREnumType = AREnumType.PropertyKeyEnum,
        Description = "Describes class -" + nameof(Orange) + "-."
    )]
    public enum OrangeP {

        // All AgoRapide enum's start with '__invalid' (value 0) in order for a non-initialized value (default 0) not to be mistaken as a data-value.
        __invalid,

        [PKType(
            Description = "The name of the orange",
            IsObligatory = true
        )]
        Name,

        [PKType(Type = typeof(ConsoleColor))]
        Colour,
    }
}
