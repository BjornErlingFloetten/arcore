﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCDoc {

    [Class(
        Description =
            "Demonstrates how also your actual application classes, not only your data classes, should implement -" + nameof(IP) + "-.\r\n" +
            "The goal is -" + nameof(ARConcepts.ExposingApplicationState) + "-."
    )]
    public class SomeAppComponent : PConcurrent {

        public void MethodA() =>
            IP.LogText("Some unstructured log text. Do not include keys / values in such a text, see instead " + nameof(MethodC) + ".");

        public void MethodB() =>
            IP.LogExecuteTime(() => {
                // Do some processing here.
                System.Threading.Thread.Sleep(1000);
            });

        [ClassMember(Description =
            "Note structured format in logging.\r\n" +
            "\r\n" +
            "This makes it easier to parse this information at a later stage. For instance in order to create performance metrics.\r\n" +
            "\r\n" +
            "Example of output text:\r\n" +
            "log/SomeApplicationComponent/log = MethodC/MainThread/1/Start = 2020-06-03 13:20:59.760\r\n" +
            "log/SomeApplicationComponent/log = MethodC/MainThread/1/parameterA = Value of A\r\n" +
            "log/SomeApplicationComponent/log = MethodC/MainThread/1/parameterB = Value of B\r\n" +
            "log/SomeApplicationComponent/log = MethodC/MainThread/1/Finish = 2020-06-03 13:21:00.766\r\n" +
            "log/SomeApplicationComponent/log = MethodC/MainThread/1/ElapsedTime = 00:00:01.005\r\n"
        )]
        public void MethodC(string parameterA, string parameterB) =>
            IP.LogExecuteTime(
                () => {
                    // Do some processing here.
                    System.Threading.Thread.Sleep(1000);
                },
                new List<(string, string)> {
                    (nameof(parameterA), parameterA),
                    (nameof(parameterB), parameterB)
                }
            );

        [ClassMember(Description = "Same as " + nameof(MethodC) + "- but returns data ('Func<string>()' instead of 'Action()')")]
        public string MethodD(string parameterA, string parameterB) =>
            IP.LogExecuteTime<string>(
                () => {
                    // Do some processing here.
                    System.Threading.Thread.Sleep(1000);
                    return parameterA + " plus " + parameterB;
                },
                new List<(string, string)> {
                    (nameof(parameterA), parameterA),
                    (nameof(parameterB), parameterB)
                }
            );
    }

    [Enum(
        Description = "Describes class -" + nameof(SomeAppComponent) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum SomeAppComponentP {
        __invalid,

        [PKType(Type = typeof(long))]
        SomeCountValue,

        [PKType(Type = typeof(long))]
        [PKLog(DoNotLogAtAll = true)]
        SomeCountValueNotToBeLogged,
    }
}