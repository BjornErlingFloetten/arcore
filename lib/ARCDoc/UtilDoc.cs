﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;
using System.Net;

namespace ARCDoc {

    [Class(Description = 
        "Utility methods for -" + nameof(ARComponents.ARCDoc) + "-.\r\n" +
        "\r\n" +
        "See -" + nameof(UtilCore) + "- for documentation about what belongs in such a Utility class like this.\r\n" +
        "\r\n" + 
        "See also -" + nameof(Extensions) + "-."
    )]
    public static class UtilDoc {

        [ClassMember(Description =
            "Returns an HTML presentation of the given link context, with links pointing backwards to each level in the hierarchy.\r\n" +
            "\r\n" +
            "In addition, returns internal links (for linking internally on same page) and " +
            "a string representation of the link context.\r\n" +
            "TODO: Document this better.\r\n" +
            "\r\n" +
            "Used by -" + nameof(ARCDoc) + "-.-" + nameof(ARCDoc.Extensions) + "-.-" + nameof(Extensions.ToHTMLSimpleSingleInternal) + "-.\r\n" +
            "\r\n" +
            "Also useful for external use.")]
        public static string GetCurrentLevelInHierarchyAsHTML(
            List<IKCoded>? linkContext,
            out List<IKCoded>? internalLinks,
            out string strLinkContext
        ) {
            var retval = new System.Text.StringBuilder();

            List<IKCoded>? externalLinks = null;
            internalLinks = null;

            // Step 1, show current level in hierarchy with backwards links
            // Note how we understand both anchors (links within this page) and links to other 'pages'.
            if (linkContext != null) {
                if (linkContext.Count == 0) {
                    // TODO: Consider showing [ROOT] or similar (see also below).
                } else {
                    // Accompany wildcards in REST queries 
                    // Like RQ/Assembly/ARCCore/Class/ActualConnection/*.html
                    externalLinks = linkContext.TakeWhile(ik => !"*".Equals(ik.Unencoded.ToString())).ToList();
                    internalLinks = linkContext.Skip(externalLinks.Count).ToList();
                    var extraLevelUp = internalLinks.Count == 0 ? "" : "../";

                    if (internalLinks.Count > 0) {
                        // Create anchor for linking internally on same page, upwards, from content "deeper down" in hierarchical order
                        retval.Append("<a id=\"" + string.Join("_", internalLinks.Select(ik => ik.Encoded)) + "\"</a>\r\n");
                        // Same as above, but linking downwards ('filename' will end with .html)
                        retval.Append("<a id=\"" + string.Join("_", internalLinks.Select(ik => ik.Encoded)) + ".html\"</a>\r\n");
                    }

                    // Show current location (with links 'backwards' to each ancestor level)
                    if (externalLinks.Count == 0) {
                        // TODO: How do we link to root now?
                    } else {
                        retval.Append("<p>");
                        retval.Append("<a href=\"" +
                            string.Join("/", Enumerable.Repeat("..", externalLinks.Count - 1)) +
                            (externalLinks.Count == 1 ? "" : "/") +
                            extraLevelUp +
                            "Index.html\">[ROOT]</a>"
                        );
                    }

                    for (var i = 0; i < externalLinks.Count; i++) {
                        if ("".Equals(externalLinks[i].ToString())) continue; // Do not bother. Probably because an HTTP request ends with slash ('/')
                        // Link to all levels upwards in hierarchical structure
                        retval.Append(
                            " / " +
                            "<a href=\"" +
                                string.Join("/", Enumerable.Repeat("..", externalLinks.Count - i - 1)) + // Move 'up' as needed
                                (i == (externalLinks.Count - 1) ? "" : "/") +
                                extraLevelUp +
                                externalLinks[i].Encoded + ".html\">" +
                                WebUtility.HtmlEncode(externalLinks[i].Unencoded.ToString()) + "</a>"
                        );
                    }

                    for (var i = 0; i < internalLinks.Count; i++) {
                        // Link to all levels upwards in hierarchical structure, but on same page as this page.
                        retval.Append(
                            " / " +
                            "<a href=\"#" + string.Join("_", internalLinks.Take(i + 1).Select(ik => ik.Encoded)) +
                            "\">" +
                            WebUtility.HtmlEncode(internalLinks[i].ToString()) + "</a>"
                        );
                    }

                    retval.Append("</p>\r\n");
                }
            }

            strLinkContext =
                (internalLinks != null && internalLinks.Count > 0) ?
                    ("#" + string.Join("_", internalLinks.Select(ik => ik.Encoded)) + "_") :
                    ((linkContext == null || linkContext.Count == 0 || "".Equals(linkContext[^1].ToString())) ?
                        "" :
                        (linkContext[^1].Encoded + "/")
                    );

            return retval.ToString();
        }

        [ClassMember(Description ="Global header to be inserted at the top of every HTML page created by -" + nameof(GenerateHTMLPage) + "-.")]
        public static string GlobalHeaderHTML { get; set; } = "";

        /// <summary>
        /// </summary>
        /// <param name="bodyContentHTML">
        /// Without &ltbody&gt enclosing tags
        /// </param>
        /// <returns></returns>
        [ClassMember(Description =
            "Wraps the parameter inside a <html><head></head><body>...</body></html> enclosure with some rudimentare CSS.\r\n" +
            "\r\n" +
            "In other words, turns the parameter into a complete HTML page.\r\n"
        )]
        public static string GenerateHTMLPage(string bodyContentHTML) =>
            "<html>\r\n" +
            "<head>\r\n" +
            "<meta charset=\"UTF-8\">\r\n" +
            "<style>\r\n" +
            "  table,th,td {\r\n" +
            "    border-width: 1px;\r\n" +
            "    border-style: solid;\r\n" +
            "    border-collapse: collapse;\r\n" +
            "  }\r\n" +
            "  td {\r\n" +
            "    vertical-align: top;\r\n" +
            "  }\r\n" +
            "</style>\r\n" +
            "</head>\r\n" +
            "<body>\r\n" +
            GlobalHeaderHTML +
            bodyContentHTML + "\r\n" +
            "<br>" +
            "<p>Generated " + UtilCore.DateTimeNow.ToStringDateAndTime() + " " + (UtilCore.UseLocalTimeInsteadOfUTC ? "(Local time)" : "UTC") + "</p>\r\n" +
            "</body>\r\n" +
            "</html>";
    }
}
