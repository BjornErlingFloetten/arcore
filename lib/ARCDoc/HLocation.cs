﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Linq;

/// <summary>
/// Contains:
/// <see cref="ARCDoc.HLocation"/>
/// <see cref="ARCDoc.HLocationP"/>
/// <see cref="ARCDoc.HLocationCollection"/>
/// </summary>
namespace ARCDoc {

    [Class(Description =
        "Simple class describing the location of a single -" + nameof(IP) + "- instance within some hierarhical context.\r\n" +
        "\r\n" +
        "The hierarchical context can be a folder / file structure on disk, or some in-memory structure.\r\n" +
        "\r\n" +
        "Created in order to facilitate\r\n" +
        "1) -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "- and\r\n" +
        "2) Storing of static HTML files to disk.\r\n" +
        "\r\n" +
        "Used in conjunction with -" + nameof(HLocationCollection) + "-.\r\n" +
        "\r\n" +
        "Used by -" + nameof(ARComponents.ARCDoc) + "- in order to document application.\r\n" +
        "\r\n" +
        //"Can be initialized either with HTML content, or with a pointer to the content's origin, " +
        //"in the latter case -" + nameof(GetContentHTML) + "- will construct the HTML content " +
        //"with the help of -" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "-.\r\n" +
        //"\r\n" +
        "See -" + nameof(DocLink) + "- for information about how references to instances of this class " +
        "is expected to turn up as references in other instances of this class.\r\n" +
        "TODO: Clarify this sentence.\r\n" +
        "\r\n" +
        "This class is immutable."
    )]
    public class HLocation : PRich {

        public List<IKCoded> Location => IP.GetPV<List<IKCoded>>(HLocationP.Location);

        public IKCoded Filename => IP.GetPV<IKCoded>(HLocationP.Filename);

        [ClassMember(Description = 
            "The Content object is not stored within the standard -" + nameof(IP) + "--mechanism.\r\n" +
            "The reason is that the object may be anything, even something pointing back to this class.\r\n" +
            "(introducing a cycle and making -" + nameof(IP.ToPropertyStream) + "- dangerous to call for instance)"
        )]
        public IP? Content { get; private set; }

        /// <summary>
        /// NOTE: <see cref="PropertyStreamLine.TryStore"/> requires all IP implementations to have a constructor with no parameters.
        /// </summary>
        public HLocation() {
        }

        public HLocation(List<IKCoded> location, IKCoded filename, IP content) {
            // : this(location, filename) {
            IP.AddPV(HLocationP.Location, location);
            IP.AddPV(HLocationP.Filename, filename);
            IP.AddPV(HLocationP.ContentType, content.GetType());
            Content = content;
        }
    }

    [Enum(
        Description = "Describes class -" + nameof(HLocation) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum)]
    public enum HLocationP {
        __invalid,

        [PKType(
            Description = "Folder names / hierarchical levels.\r\n",
            IsObligatory = true, Type = typeof(IKCoded), Cardinality = Cardinality.WholeCollection
        )]
        Location,

        [PKType(
            Description = "Either the last hierarhical key for the content's location, or Index.html if no location.",
            Type = typeof(IKCoded), IsObligatory = true
        )]
        Filename,

        /// NOTE: Actual Content object is not stored within the standard <see cref="IP"/>-mechanism.
        /// NOTE: The reason is that the object may be anything, even something pointing back to this class.
        /// NOTE: (introducing a cycle and making <see cref="IP.ToPropertyStream"/> dangerous to call for instance)
        //[PKType(
        //    Description = "The -" + nameof(IP) + "- instance from which this location originated.\r\n",
        //    Type = typeof(IP),
        //    IsObligatory = true
        //)]
        //Content,

        [PKType(
            Description = "The type of the content object. The actual content object is stored as -" + nameof(HLocation.Content) + "-.",
            Type = typeof(Type)
        )]
        ContentType
    }

    [Class(Description =
        "Contains a collection of -" + nameof(HLocation) + "- objects.\r\n" +
        "\r\n" +
        "Instances of this class are usually ephemeral, this class can more accurately be described as a utility class than a class for data storage.\r\n" +
        "\r\n"
    )]
    public class HLocationCollection : PRich {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="excludeKey">
        /// HACK:
        /// Typical value for this key would be <see cref="HTOC.HTOCContentThisLevelKey"/>
        /// </param>
        /// <param name="currentIK">
        /// Do not set this parameter from 'outside'. It is only used internally for recursive calls.
        /// </param>
        /// <param name="retval">
        /// Do not set this parameter from 'outside'. It is only used internally for recursive calls.
        /// </param>
        /// <returns></returns>
        [ClassMember(
            Description =
                "Transforms the parameter (and recursively all contained objects) into a collection of -" + nameof(HLocation) + "- objects.\r\n" +
                "\r\n" +
                "This is usually the first step in order to find potential links (see -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "-).\r\n" +
                "\r\n" +
                "See also -" + nameof(Documentator) + "-.\r\n"
            )]
        public static HLocationCollection Create(IP ip,
            IK? excludeKey = null,
            List<IKCoded>? currentIK = null,
            HLocationCollection? retval = null
        ) {
            try {
                REx.Inc();
                if (currentIK == null) currentIK = new List<IKCoded>();
                if (retval == null) retval = new HLocationCollection();

                // Call recursively for all properties that have properties themselves.
                ip.
                    Where(ikip =>
                        // !ikip.P.EnumeratorReturnsEmptyCollection && /// No point in calling if no properties because <see cref="ToHTMLSimpleAsTableRow"/> will not link anyway
                        ikip.P.Any() && /// No point in calling if no properties because <see cref="ToHTMLSimpleAsTableRow"/> will not link anyway
                        (excludeKey == null || !ikip.Key.Equals(excludeKey))
                    ).
                    OrderBy(ikip => ikip.Key.ToString()).
                    ForEach(ikip => {
                        currentIK.Add(IKCoded.FromUnencoded(ikip.Key));
                        Create(ikip.P,
                            // TODO: Delete commented out code
                            // Simplified method 23 Mar 2021:
                            // calculateKeysOnly, prepareForLinkInsertion, 
                            excludeKey, currentIK, retval);
                        currentIK.RemoveAt(currentIK.Count - 1);
                    });

                retval.IP.AddP(
                    key: string.Join("/", currentIK.Select(ik => ik.Unencoded)) + (currentIK.Count == 0 ? "_Index" : ""), /// HACK: _Index instead of Index.html because the latter is hardcoded in <see cref="ARComponents.ARCAPI"/>
                    p: new HLocation(
                        // NOTE: Be careful here, do not pass currentIK as parameter whole, as it will change afterwards.
                        location: currentIK.Take(currentIK.Count - 1).ToList(),
                        filename: currentIK.Count == 0 ? IKCoded.FromEncoded("Index.html") : IKCoded.FromEncoded(currentIK[^1].Encoded + ".html"),
                        content: ip
                ));

                return retval; // Return value will now only be "consumed" if we were the first one to be called in the chain (see above)
            } finally {
                REx.Dec();
            }
        }

        [ClassMember(Description =
            "Writes all -" + nameof(HLocation) + "- results to a hierarhical folder structure " +
            "as static HTML files with (optionally) -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "-.\r\n" +
            "\r\n" +
            "If -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "- is requested " +
            "will call -" + nameof(DocLinkCollection) + "-.-" + nameof(DocLinkCollection.Create) + "- " +
            "in order to find potential links.\r\n" +
            "\r\n" +
            "Uses -" + nameof(UtilDoc.GenerateHTMLPage) + "- in order to wrap file content within <head> / <body> together with some rudimentary CSS."
        )]
        public void WriteAllContentHTMLToDisk(string rootFolder, bool doLinkInsertion = false) {

            /// Use a simpler docLinkResolutionLocation (instead of <see cref="DocLinkCollection.DefaultDocLinkResolutionLocation"/>)
            /// in order to be able to construct all pages leading to the resolution pages below
            /// (only relevant for doLinkInsertion = TRUE).
            var docLinkResolutionLocation = new List<IKCoded> { IKCoded.FromEncoded(nameof(DocLinkResolution)) };

            var (docLinks, docLinkResolutions) = doLinkInsertion ?
                DocLinkCollection.Create(this, docLinkResolutionLocation: docLinkResolutionLocation) :
                default;

            if (!System.IO.Directory.Exists(rootFolder)) {
                System.IO.Directory.CreateDirectory(rootFolder);
            }
            if (!rootFolder.EndsWith(System.IO.Path.DirectorySeparatorChar)) rootFolder += System.IO.Path.DirectorySeparatorChar;
            this.Concat(
                    !doLinkInsertion ? new List<IKIP>() :
                    docLinkResolutions.Select(ikip => {
                        if (!(ikip.P is DocLinkResolution d)) return null; /// Could have thrown exception here, but this allows for use of <see cref="DocumentatorP._Description"/> for instance within collection 
                        var filename = d.IP.GetPV<List<DocLink>>(DocLinkResolutionP.DocLinks)[0].Filename;
                        return new IKIP(
                            keyAsString: "",  // Key is irrelevant, it will not be used below anyway
                            new HLocation(
                                location: docLinkResolutionLocation,
                                filename: filename,
                                content: d
                            )
                         );
                    }).Where(ikip => ikip != null).
                    Select(ikip => ikip!).
                    Append(
                        /// It is important to also include the collection, 
                        /// if not <see cref="DocLinkResolution.ToHTMLSimpleSingle"/> 'backwards' link
                        /// (link up higher in the hierarchy) will not work.
                        /// This is the reason why we only use a one-level-down location for the resolution pages
                        /// because it would be difficult to construct more pages here than this single one.
                        new IKIP(
                            keyAsString: "",  // Key is irrelevant, it will not be used below anyway
                            new HLocation(
                                // location: DocLinkCollection.DocLinkResolutionLocation,
                                // location: new List<IKCoded> { IKCoded.FromEncoded(nameof(PropertyStreamLinePrefix.doc)) },
                                location: new List<IKCoded>(),
                                filename: IKCoded.FromEncoded(nameof(DocLinkResolution) + ".html"),
                                content: docLinkResolutions
                            )
                        )
                    )
                ).
                AsParallel(). // Added AsParallell 24 Mar 2021 (approximately doubles performance in a 4-core scenario).
                ForEach(ikip => {
                    if (!(ikip.P is HLocation hLocation)) return; /// Could have thrown exception here, but this allows for use of <see cref="DocumentatorP._Description"/> for instance within collection 
                    var currentFolder = new System.Text.StringBuilder();
                    currentFolder.Append(rootFolder);

                    var location = hLocation.Location;
                    location.ForEach(f => {
                        currentFolder.Append(f.Encoded + System.IO.Path.DirectorySeparatorChar);
                        if (!System.IO.Directory.Exists(currentFolder.ToString())) {
                            System.IO.Directory.CreateDirectory(currentFolder.ToString());
                        }
                    });

                    /// Attempt to "reverse" operation in <see cref="HLocationCollection.Create"/>
                    /// when the HLocation-instance was created:
                    /// TODO: Reconstruction is not "correct". Consider storing original context within HLocation instead.
                    var linkContext = location.Count == 0 && "Index.html".Equals(hLocation.Filename.Encoded) ?
                        location :
                        // HACK: Replace .html in filename
                        // TODO: Create filename-without-html somewhere, instead of replace like here.
                        location.Append(IKCoded.FromEncoded(hLocation.Filename.Encoded.Replace(".html", ""))).ToList();

                    var text = hLocation.Content?.ToHTMLSimpleSingle(doLinkInsertion, linkContext: linkContext) ??
                        // TODO: Analyze better how to react here. Ignore maybe?
                        throw new NullReferenceException(nameof(hLocation.Content));

                    System.IO.File.WriteAllText(
                        currentFolder +
                        hLocation.Filename.Encoded.ToString(),
                        UtilDoc.GenerateHTMLPage(
                            doLinkInsertion ?
                                docLinks.InsertLinks(hLocation.Location, text) :
                                text
                        ),
                        UtilCore.DefaultAgoRapideEncoding
                    );
                });
        }
    }

    public static class HLocationCollectionExtensions {
        public static HLocationCollection ToHLocationCollection(this IEnumerable<HLocation> hLocations) {
            var retval = new HLocationCollection();
            hLocations.ForEach(location => {
                retval.IP.AddP(
                    key: string.Join("/", location.Location.Select(ik => ik.Unencoded).Append(location.Filename.Unencoded)),
                    p: location
                );
            });
            return retval;
        }
    }
}
