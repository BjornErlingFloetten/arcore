﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;
using System.Net;
using System.Linq;

namespace ARCDoc
{

    [Class(Description =
        "Contains various extension methods for -" + nameof(IP) + "- and -" + nameof(IKIP) + "- related to documentating / reporting / HTML.\r\n" +
        "\r\n" +
        "See for instance -" + nameof(ToHTMLSimpleSingle) + "- which " +
        "transforms any -" + nameof(IP) + "- object into an HTML representation.\r\n"
    )]
    public static class Extensions
    {

        /// <summary>
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="prepareForLinkInsertion">
        /// Relevant for call to <see cref="Extensions.DescribeValue"/>
        /// </param>
        /// <param name="currentIK">
        /// Set this parameter from outside as needed. If outside call is made from the 'root' context, this parameter does not need to be set.
        /// </param>
        /// <param name="retval">
        /// Do not set this parameter from 'outside'. It is only used internally for recursive calls.
        /// </param>
        /// <returns></returns>
        [ClassMember(Description =
            "Calls -" + nameof(ToHTMLSimpleSingle) + "- recursively.\r\n" +
            "In other words, flattens out a hierarchical structure into one single 'flat' HTML file.\r\n" +
            "\r\n" +
            "TODO: See -" + nameof(ToHTMLSimpleSingle) + "-, add use of reflection in order to call any class implementation of this method instead."
        )]
        public static string ToHTMLSimpleSingleRecursive(this IP ip, bool prepareForLinkInsertion = false, List<IKCoded>? currentIK = null, StringBuilder? retval = null)
        {
            REx.Inc();
            try
            {
                if (currentIK == null) currentIK = new List<IKCoded>();
                if (retval == null) retval = new StringBuilder();

                // Step 1) Create page with any properties at this level
                retval.Append(ip.ToHTMLSimpleSingle(prepareForLinkInsertion, currentIK));

                // Step 2) Call recursively for all properties that have properties themselves.
                ip.
                    // Where(ikip => !ikip.P.EnumeratorReturnsEmptyCollection). /// No point in calling if no properties because <see cref="ToHTMLSimpleAsTableRow"/> will not link anyway
                    Where(ikip => ikip.P.Any()). /// No point in calling if no properties because <see cref="ToHTMLSimpleAsTableRow"/> will not link anyway
                    // TODO: Add integer / double ordering if all values parse as double
                    OrderBy(ikip => ikip.Key.ToString()).
                    ForEach(ikip =>
                    {
                        currentIK.Add(IKCoded.FromUnencoded(ikip.Key));
                        ToHTMLSimpleSingleRecursive(ikip.P, prepareForLinkInsertion, currentIK, retval);
                        currentIK.RemoveAt(currentIK.Count - 1);
                    });

                return retval.ToString(); // Return value will only be consumed if we were the first one to be called in the chain (see above)
            }
            finally
            {
                REx.Dec();
            }
        }

        /// <param name="linkContext">
        /// If not given then links will be generated as '/RQ/dt/...',  that is, assuming a specific
        /// hierarchical structure. This usually works well.
        /// </param>
        /// <param name="prepareForLinkInsertion">
        /// Relevant for call to <see cref="Extensions.DescribeValue"/>
        /// </param>
        [ClassMember(
            Description =
                "Creates a simple HTML representation of the given parameter.\r\n" +
                "\r\n" +
                "The properties are shown as a two column table with heading Key and Value.\r\n" +
                "\r\n" +
                "By simple we mean that no Javascript library is involved, only standard HTML, CSS and Javascript.\r\n" +
                "By single we mean that no recursion will be performed (for recursion see -" + nameof(HLocationCollection.Create) + "-)." +
                "\r\n" +
                "Links are attempted inserted using 'linkContext' in order to generate relative links.\r\n" +
                "\r\n" +
                "If 'linkContext' is given then\r\n" +
                "1) Its last value will be taken as the Key for what we is generated now.\r\n" +
                "2) Backlinking will be done (linking one step up in hierarchical structure, " +
                "for instance going from '.../Customer/42' to '.../Customer' (viewing all Customers).\r\n" +
                "\r\n" +
                "If 'linkContext' is not given then links will be generated as '/RQ/dt/...',  that is, assuming a specific " +
                "hierarchical structure. This usually works well.\r\n" +
                "\r\n" +
                // TODO: Delete commented out code (problem is probably fixed as of 1 Jan 2022
                //"See boolean parameter 'prepareForLinkInsertion' for more information about linking.\r\n" +
                //"TODO: As of Dec 2021 the parameter 'prepareForLinkInsertion' does not distinguish between\r\n" +
                //"TODO: 'do not link because of performance reasons' and\r\n" +
                //"TODO: 'do not link because links will not resolve correctly'.\r\n" +
                //"TODO: (The latter would typically be the case then calling this method from ESController or ASController\r\n" +
                //"TODO: in -" + nameof(ARComponents.ARCEvent) + "-.\r\n" +
                //"TODO: Change this boolean value into an enum distinguishing between these to.\r\n" +
                "\r\n" +
                "If the link context contains a '*', it is assumed that from that level and down the hierarchical structure " +
                "has been flattened into a single HTML page page, in other words, that the links are internally on the same page." +
                "This is in order to accompany wildcards in REST queries like 'RQ/Assembly/ARCCore/Class/ActualConnection/*.html'.\r\n" +
                "\r\n" +
                "If you have a specific implementation of -" + nameof(IP) + "- that needs a different HTML representation " +
                "than what is offered by this method, then you can just implement a method with this same signature for that implementation.\r\n" +
                "You may also reuse functionality from here by calling -" + nameof(ToHTMLSimpleSingleInternal) + "-.\r\n" +
                "\r\n" +
                "Note that -" + nameof(ARComponents.ARCAPI) + "- offers additional functionality for generating HTML views.\r\n" +
                "For instance this method is not very well suited to represent a uniform collection of objects, " +
                "something which -" + nameof(ARComponents.ARCAPI) + "- offers.\r\n" +
                "In other words, this method is very suitable for representing a single 'Customer' object, " +
                "but not a uniform collection of 'Customer' objects (a 'CustomerCollection').",
            LongDescription =
                "NOTE: If you actually implement this method (-" + nameof(ToHTMLSimpleSingle) + "-) in your own class\r\n" +
                "NOTE: (something which you quite naturally will want to do).\r\n" +
                "NOTE: then it would by default (in .NET) not be called if the (statically known) type of the object\r\n" +
                "NOTE: at the time of invocation was -" + nameof(IP) + "- (instead of the actual type).\r\n" +
                "NOTE: \r\n" +
                "NOTE: This extension method therefore uses reflection in order to look for a specific implementation " +
                "NOTE: in the (dynamically known) type of the -" + nameof(IP) + "- parameter " +
                "NOTE: and calls that method instead if found.\r\n" +
                "NOTE: (this is also the reason for having -" + nameof(ToHTMLSimpleSingleInternal) + "-.)\r\n" +
                "NOTE: \r\n" +
                "NOTE: See this article for more information\r\n" +
                "NOTE: https://stackoverflow.com/questions/30645441/extension-methods-with-base-and-sub-classes \r\n"
        )]
        public static string ToHTMLSimpleSingle(this IP ip, bool prepareForLinkInsertion = false, List<IKCoded>? linkContext = null)
        {

            var method = ip.GetType().GetMethod(nameof(ToHTMLSimpleSingle), System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
            if (method != null)
            {
                /// Known implementations of this metod as of Jan 2021 are:
                /// ARCQuery.QueryProgressDetails.ToHTMLSimpleSingle

                // TODO: Check that the method signature is actually identical to what is required.
                try
                {
                    return (string)method.Invoke(ip, new object?[] { prepareForLinkInsertion, linkContext });
                }
                catch (Exception ex)
                {
                    throw new System.Reflection.TargetInvocationException(
                        "Unable to call method " + method.Name + "\r\n" +
                        "(" + method.ToString() + ")\r\n" +
                        "for " + ip.GetType() + "\r\n" +
                        "Possible cause: Differing signature (no signature match was done)\r\n" +
                        "Possible resolution: Fix this code in AgoRapide or change signature of target method.\r\n",
                        ex
                    );
                }
            }
            return ToHTMLSimpleSingleInternal(ip, prepareForLinkInsertion, linkContext);
        }

        [ClassMember(Description =
            "Hack in order to escape check for direct implementation in actual class " +
            "of -" + nameof(ToHTMLSimpleSingle) + "- in -" + nameof(ToHTMLSimpleSingle) + "-.")]
        public static string ToHTMLSimpleSingleInternal(IP ip, bool prepareForLinkInsertion = false, List<IKCoded>? linkContext = null)
        {

            var retval = new StringBuilder();

            // Give up on this...
            //if (!prepareForLinkInsertion)
            //{
            //    /// Added 22 Dec 2021 because snapshots generated with ESController in <see cref="ARComponents.ARCEvent"/>
            //    /// do not generate correct links now.
            //    /// TODO: Consider if to become permanent or not.
            //}
            //else
            //{
            retval.Append(UtilDoc.GetCurrentLevelInHierarchyAsHTML(linkContext, out var internalLinks, out var strLinkContext));
            // }

            // Step 2, Information about this 'class'
            retval.Append("<p>" + new Func<string>(() =>
            {

                var a = ClassAttribute.GetAttribute(ip.GetType());
                if (a.IsDefault) return WebUtility.HtmlEncode(ip.GetType().ToStringShort());
                var helpTextHTML = a.GetHelptextHTML();
                if (string.IsNullOrEmpty(helpTextHTML)) return WebUtility.HtmlEncode(ip.GetType().ToStringShort());
                return "<span title=\"" + helpTextHTML + "\">" + ip.GetType().ToStringShort() + "</span>";
            })() + "</p>\r\n");

            // Step 3, Information about the key that this class instance is placed under
            if (linkContext != null)
            {
                if (linkContext.Count == 0)
                {
                    // TODO: Consider showing [ROOT] or similar (see also above).
                }
                else
                {
                    var ikSafe = linkContext[^1];
                    retval.Append("<h1>" +
                        (!("*".Equals(ikSafe.Unencoded.ToString()) && linkContext.Count > 1) ?
                            ikSafe.Unencoded.KeyToHTMLSimpleWithTooltip() :
                            (linkContext[^2].Unencoded.KeyToHTMLSimpleWithTooltip() + "/*") // Hack in order to accompany /* paths in API REST query.
                        ) +
                        "</h1>\r\n"
                   );
                }
            }

            // Step 4, The actual value (if relevant)
            if (ip.TryGetV<string>(out var s))
            {
                retval.Append("<p>" + WebUtility.HtmlEncode(s).Replace("\r\n", "<br>\r\n") + "</p>\r\n");
            }

            // Step 5, All properties (if relevant)
            var allP = ip.ToList();
            if (allP.Count > 0)
            {
                var retvalTable = new StringBuilder();
                retvalTable.Append("<table>\r\n<tr><th>Key</th><th>Value</th></tr>\r\n");
                (allP.AsParallel().All(ikip => // Note use of AsParallell here.
                      UtilCore.DoubleTryParse(ikip.Key.ToString(), out _)
                ) ?
                    // Use double comparision (because all values parse as doubles)
                    // Note performance issue here, if value was original integer or double it should be unnecessary to parse it again
                    allP.OrderBy(ikip => UtilCore.DoubleParse(ikip.Key.ToString())) :
                    allP.OrderBy(ikip => ikip.Key.ToString())
                ).Use(ordered =>
                {
                    var i = 0;
                    foreach (var ikip in ordered)
                    {
                        retvalTable.Append(ikip.ToHTMLSimpleAsTableRow(strLinkContext, prepareForLinkInsertion));
                        if (i++ >= 200)
                        { // Limit of 200 is hard coded here. We assume that an HTML page with more than 1000 items is not very relevant, even for static pages.
                            retvalTable.Append("</table>\r\n");
                            retvalTable.Insert(0,
                                "<p " +
                                    "style=\"color:red\"" +  // It is very important to emphasize this
                                ">" +
                                "NOTE: Only 200 elements of total " + ordered.Count() + " shown. Use -SKIP- and -TAKE-, or change -LIMIT-. Try also -SHUFFLE-.\r\n" +
                                "</p>\r\n");
                            return; // Return from inside 'Use' (see above)
                        }
                    }
                    retvalTable.Append("</table>\r\n");
                    retvalTable.Append("<p>" + allP.Count + " items</p>");
                });
                retval.Append(retvalTable);
            }

            return retval.ToString();
        }

        /// <summary>
        /// A forward-slash terminated string (if linking to another page) or
        /// A _ terminated string (if linking within same page) 
        /// indicating relative location of where content for this key will be located.
        /// </summary>
        /// <param name="ikip"></param>
        /// <param name="strLinkContext"></param>
        /// <param name="prepareForLinkInsertion">
        /// Relevant for call to <see cref="Extensions.DescribeValue"/>
        /// </param>
        /// <returns></returns>
        [ClassMember(
            Description =
                "Creates a simple HTML representation of the given parameter as a single two column row (Key, Value) in a table.\r\n" +
                "\r\n" +
                "By simple we mean that no Javascript library is involved, only standard HTML, CSS and Javascript.\r\n" +
                "\r\n" +
                "By single we mean that no recursion will be performed (for recursion see -" + nameof(HLocationCollection.Create) + "-." +
                "\r\n" +
                "It is assumed that the actual property can be linked to with the given link context and -" + nameof(IKIP.Key) + "- as reference.\r\n" +
                "\r\n" +
                "If the key ends with Id and value is in the form '(key minus Id) plus slash plus value', " +
                "like CustomerId = Customer/42, then will assume that can create link of value.\r\n" +
                "Note how this is totally independent of any predefined structure.\r\n" +
                "\r\n" +
                "Note that -" + nameof(ARComponents.ARCAPI) + "- offers additional functionality for generating HTML views."
        )]
        public static string ToHTMLSimpleAsTableRow(this IKIP ikip, string strLinkContext, bool prepareForLinkInsertion = false, string? keyToHTMLSimpleWithTooltip = null) =>
            "<tr>" +
                "<td>" + // Column 1, Key
                (
                    (
                        // ikip.P.EnumeratorReturnsEmptyCollection ||
                        !ikip.P.Any() ||
                        string.IsNullOrEmpty(strLinkContext) // Change 1 Jan 2022, do not link here we have no idea of structure
                    ) ?
                    (
                        (keyToHTMLSimpleWithTooltip ?? ikip.Key.KeyToHTMLSimpleWithTooltip())  // No point in linking, because will not contain more information, only the actual value
                    ) :
                    (
                        "<a href=\"" +
                            strLinkContext +
                            IKCoded.FromUnencoded(ikip.Key).Encoded +
                            ".html\">" + // Link to separate file describing Value.
                            (keyToHTMLSimpleWithTooltip ?? ikip.Key.KeyToHTMLSimpleWithTooltip()) + 
                        "</a>"
                    )
                ) +
                "</td>" +
                "<td>" + new Func<string>(() => // Column 2, Value
                {
                    var pk = ikip.Key as PK;
                    if (pk != null && pk.TryGetA<PKHTMLAttribute>(out var pkHTML))
                    {
                        return pkHTML.Encode(ikip.P.GetV<string>(""));
                    }
                    // Check for linked value
                    var key = ikip.Key.ToString();
                    Type? foreignEntityType = null;

                    if (
                        key.EndsWith("Id") &&
                        (
                            // Code was changed on 1 Jan 2022, original condition was only checking for 'Id'
                            // meaning that a weakly typed structure (without any schema at all) would automatically
                            // get links generated.
                            // Having 'pk == null' as a condition here _should_ ensure the same functionality
                            pk == null ||
                            (
                                /// For more strongly typed structures (with schema, that is with <see cref="PK"/>
                                /// the change from 1 Jan 2022 is that we are now stricter (checking that type actually
                                /// exists) and also with using <see cref="PKRelAttribute"/> so we can link
                                /// from foreign keys NOT having the same name as the type of the foreign entity.

                                IP.AllIPDerivedTypesDict.TryGetValue(key[0..^2], out foreignEntityType) ||
                                (
                                    pk != null && pk.TryGetA<PKRelAttribute>(out var pkrel) &&
                                    pkrel.IP.TryGetPV<Type>(PKRelAttributeP.ForeignEntity, out foreignEntityType)
                                )
                            )
                        )
                    )
                    {
                        // Insert link

                        // Insert link. Note how we are careful here to avoid any chance of malicious insertion of Javascript or similar.
                        // We assume that location of target is at same level.

                        if (pk != null)
                        {
                            // Check for cardinality, that is, check if we shall generate multiple links instead of a single link
                            /// NOTE: This is really support for <see cref="ARConcepts.ManyToManyRelations"/> without a 'third table'.
                            /// NOTE: One could ask if we go a little too far
                            /// NOTE: here, it might be more reasonable to just add a third entity type like in the traditional
                            /// NOTE: database manner in order to support many-to-many relations.
                            if (pk.Cardinality.IsMultiple())
                            {
                                if (ikip.P.TryGetV<List<string>>(out var list))
                                {
                                    return string.Join(", ", list.Select(s =>
                                        "<a href=\"" +
                                            (string.IsNullOrEmpty(strLinkContext) ?
                                               "/RQ/dt/" : // Change from 1 Jan 2022, link by starting with root and assume 'RQ/dt'
                                               "../" // Assume that foreign entity can be reached by going one level up
                                            ) +
                                            (foreignEntityType == null ? WebUtility.UrlEncode(key[0..^2]) : foreignEntityType.ToStringVeryShort()) + "/" +
                                            WebUtility.UrlEncode(s) + ".html\">" +
                                            WebUtility.HtmlEncode(s) +
                                        "</a>")
                                    );
                                }
                                else
                                {
                                    // This is very unexpected. But try to link as single string below instead.
                                }
                            }
                            else
                            {
                                // Assume single value only (see below)
                            }
                        }

                        if (ikip.P.TryGetV<string>(out var s))
                        {
                            return
                                "<a href=\"" +
                                    (string.IsNullOrEmpty(strLinkContext) ?
                                        "/RQ/dt/" : // Change from 1 Jan 2022, link by starting with root and assume 'RQ/dt'
                                        "../" // Assume that foreign entity can be reached by going one level up
                                    ) +
                                    (foreignEntityType == null ? WebUtility.UrlEncode(key[0..^2]) : foreignEntityType.ToStringVeryShort()) + "/" +
                                    WebUtility.UrlEncode(s) + ".html\">" +
                                    WebUtility.HtmlEncode(s) +
                                "</a>";
                        }
                    }
                    // 'Ordinary' value
                    var retval = WebUtility.HtmlEncode(ikip.DescribeValue(prepareForLinkInsertion)).Replace("\r\n", "<br>\r\n");
                    if ("".Equals(retval))
                    {
                        retval = "&nbsp;";
                    }
                    return retval;
                })() +
                "</td>" +
                "</tr>\r\n";

        [ClassMember(Description =
            "Describes the value contained withing an -" + nameof(IKIP) + "- (-" + nameof(IKIP.P) + "-).\r\n" +
            "\r\n" +
            "Useful in drill-down situations, where -" + nameof(IKIP.P) + "- has no intrinsic value itself but contains properties / links to new values. " +
            "In that case the description could for instance include list of keys found 'one level further down' (assuming not too may of them present).\r\n" +
            "\r\n" +
            "Can in a worst case scenario return an empty string, for instance if no intrinsic value, no description and noe keys to list.\r\n" +
            "\r\n" +
            "If parameter prepareForLinkInsertion is given then will attempt to do -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "- " +
            "if there is a high probability that the returned value is a link " +
            "This is somewhat experimental as of May 2020.\r\n" +
            "An example for when this is relevant is for a documentation page like\r\n" +
            "Assembly/ARCCore/AREnumType/DocumentationOnlyEnum/ARComponents/_Member/ARADB.html\r\n" +
            "Here values like EnumMember and EnumType should link to actual values,\r\n" +
            "so instead of a table-row (key value) listing like\r\n" +
            "  EnumMember   ARADB\r\n" +
            "  EnumType     ARCCore.ARComponents\r\n" +
            "This method describes the values as\r\n" +
            "  EnumMember   -ARADB-\r\n" +
            "  EnumType     -ARCCore.ARComponents-\r\n" +
            "for later link insertion.\r\n" +
            "\r\n" +
            "TODO: Add parameters describing when to list keys or not, and when to use linking." +
            "TODO: Also, ensure that can link values like Customer/42 (but calling method should do that?)\r\n"
        )]
        public static string DescribeValue(this IKIP ikip, bool prepareForLinkInsertion = false)
        {

            var prepareForLinkInsertionIfNoWhitespace = new Func<string, string>(s =>
            {
                /// See <see cref="ARConcepts.LinkInsertionInDocumentation"/> 
                if (!prepareForLinkInsertion) return s;
                if ("".Equals(s)) return s;
                if (!s.Any(c => char.IsWhiteSpace(c)))
                {
                    // We assume that it is a high probability that the value is linkable somewhere
                    // This test is somewhat experimental as of May 2020. It will have both false positives and false negatives.
                    // TODO: Add some property to PKTypeAttribute indicating if content is linkable.
                    return "-" + s + "-"; /// <see cref="ARConcepts.LinkInsertionInDocumentation"/>
                }
                return s;
            });

            /// Priority 1, the actual value
            /// (Either as List<string> or just string, according to <see cref="Cardinality"/>
            if (ikip.Key is PK pk)
            {
                if (pk.Cardinality == Cardinality.WholeCollection)
                {
                    if (ikip.P.TryGetV<List<string>>(out var list))
                    {
                        return string.Join("; ", list.Select(s => prepareForLinkInsertionIfNoWhitespace(s)));
                    }
                }
            }
            //if (ikip.P is PValue<string> pvs) {
            //    // NOTE: Added 13 Jul 2020. Delete if not useful.
            //    return string.Join("\r\n", pvs.Value);
            //}
            if (ikip.P.TryGetV<string>(out var s))
            {
                return prepareForLinkInsertionIfNoWhitespace(s);
            }

            /// Priority 2, use <see cref="PriorityOrder.Important"/>
            var allP = ikip.P.ToList(); // ToList = force evaluation (avoid deferred execution). Note: Expensive if huge number of properties.
            var important = allP.Where(ikip =>
            {
                if (!(ikip.Key is PK pk)) return false;
                if (!pk.TryGetA<PKDocAttribute>(out var a)) return false;
                if (!a.IP.TryGetPV<PriorityOrder>(PKDocAttributeP.PriorityOrder, out var o)) return false;
                return o <= PriorityOrder.Important;
            });
            if (important.Count() > 0)
            {
                return string.Join("; ", important.Select(ikip => ikip.P.TryGetV<string>(out var s) ? s : "").Where(s => !"".Equals(s)));
            }

            // Priority 3, any suitable description
            var retval = prepareForLinkInsertionIfNoWhitespace(new Func<string>(() =>
            {
                if (ikip.P.TryGetPV<string>(DocumentatorP._Description, out s)) return s; /// Priority 3a, <see cref="PP._Description"/>
                if (ikip.P.TryGetPV<string>(BaseAttributeP.Description, out s)) return s; /// Priority 3b, <see cref="BaseAttributeP.Description"/>
                if (ikip.Key is IKType iktype)
                { // Priority 3c, description of enum-attribute or class-attribute (when key is type)
                    if (iktype.Type.IsEnum) return EnumAttribute.GetAttribute(iktype.Type).IP.GetPV(BaseAttributeP.Description, "");
                    return ClassAttribute.GetAttribute(iktype.Type).IP.GetPV(BaseAttributeP.Description, "");
                }
                return ""; // Give up
            })());

            // For Priority 3, add also any keys found.
            // Add any keys found (note that not relevant for Priority 1, the actual value)
            var c = allP.Count;
            if (c == 0)
            {
                // Do not add anything
            }
            else if (c < 100)
            { // TODO: / NOTE: Limit of 100 is a quite arbitrary cut-off
                // TODO. Add as parameter.
                retval += ("".Equals(retval) ? "" : "\r\n\r\n") + string.Join(", ", allP.OrderBy(ikip => ikip.Key.ToString()).Select(
                    ikip => prepareForLinkInsertionIfNoWhitespace(ikip.Key.ToString()))); // There is a high probability that the key will also be a HTML file, therefore we turn into link
                if (c > 10)
                {
                    // Help with counting
                    retval += "\r\n" + c.ToString() + " items";
                }
            }
            else
            {
                retval += "\r\n" + c.ToString() + " items";
            }
            return retval;
        }

        [ClassMember(
            Description =
                "Creates a simple HTML representation of the Value with description as a 'tooltip'.\r\n" +
                "\r\n" +
                "Returned value is intended for location inside a <td>...</td> or similiar (returned value is not enclosed within <p>...</p> for instance.\r\n" +
                "\r\n" +
                "Returns '&nbsp;' if no value found.\r\n" +
                "\r\n" +
                "Note: The tooltip is only relevant when value itself has a -" + nameof(BaseAttribute) + "-, " +
                "as of Apr 2020 this is only checked for enum members / enum values."
        )]
        public static string ValueToHTMLSimpleWithTooltip(this IP value)
        {
            if (!value.TryGetV<string>(out var s)) return "&nbsp;";
            var sHTML = WebUtility.HtmlEncode(s);
            var o = value.GetV<object>();
            if (!o.GetType().IsEnum) return sHTML;
            // TODO: What about EnumAttribute.GetAttribute? Description on higher level?
            var a = EnumMemberAttribute.GetAttribute(o);
            if (a.IsDefault) return sHTML;
            var helpTextHTML = a.GetHelptextHTML();
            if (string.IsNullOrEmpty(helpTextHTML)) return sHTML;
            return "<span title=\"" + helpTextHTML + "\">" + sHTML + "</span>";
        }

        [ClassMember(
                Description =
                    "Creates a simple HTML representation of the Key with description as a 'tooltip'."
            )]
        public static string KeyToHTMLSimpleWithTooltip(this IK ik)
        {
            var helpTextHTML = !(ik is PK pk) ? "" : pk.GetA<PKTypeAttribute>().GetHelptextHTML();
            return string.IsNullOrEmpty(helpTextHTML) ?
                WebUtility.HtmlEncode(ik.ToString()) :
                (
                    "<span title=\"" + helpTextHTML + "\">" + WebUtility.HtmlEncode(ik.ToString()) + "</span>"
                );
        }

        [ClassMember(Description =
            "Returns content of -" + nameof(BaseAttributeP.Description) + "- and -" + nameof(BaseAttributeP.LongDescription) + "-.\r\n" +
            "\r\n" +
            "Returns text suitable for including within a <span title = \"...\" ... </span>\r\n" +
            "\r\n" +
            "Note how removes all hypens, that is, disables -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "- " +
            "because -" + nameof(DocLinkCollection.InsertLinks) + "- would have blindly destroyed the <span title = \"...\" ... </span> with <a href=\"...\"> inside.")]
        public static string GetHelptextHTML(this BaseAttribute a) =>
            (
                (!a.IP.TryGetPV<string>(BaseAttributeP.Description, out var s) ? "" : WebUtility.HtmlEncode(s)) +
                (!a.IP.TryGetPV<string>(BaseAttributeP.LongDescription, out s) ? "" : ("\r\n\r\n" + WebUtility.HtmlEncode(s)))
            ).Replace("-", "").Replace("\"", "'");
    }
}