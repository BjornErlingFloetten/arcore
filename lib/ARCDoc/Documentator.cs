﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using ARCCore;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Contains the following classes / enums:
/// <see cref="ARCDoc.Documentator"/>
/// <see cref="ARCDoc.Documentator.PotentialLinks"/>
/// <see cref="ARCDoc.DocumentatorP"/>
/// <see cref="ARCDoc.HTMLLinkWordWithLocation"/>
/// <see cref="ARCDoc.DummyEnum"/>
/// <see cref="ARCDoc.DummyEnumP"/>
/// </summary>
namespace ARCDoc {

    [Class(
        Description =
            "General class for documenting AgoRapide and also your own -" + nameof(ARConcepts.ApplicationSpecificCode) + "-.\r\n" +
            "\r\n" +
            "See also -" + nameof(DocumentatorP) + "-.\r\n" +
            "\r\n" +
            "HINT: If you want to test concepts like -" + nameof(ARConcepts.PropertyStream) + "- and -" + nameof(ARNodeType.CoreDB) + "- " +
            "then this class is a good generator of properties for the stream. " +
            "Just call -" + nameof(IP.ToPropertyStream) + "- on root storage " +
            "and you will have a quite sizeable chunk of property stream lines available for testing."
    )]
    public class Documentator : PRich {

        /// <summary>
        /// HACK: Will affect result of <see cref="ToHTMLSimpleSingle"/>
        /// 
        /// If TRUE then <see cref="ToHTMLSimpleSingle"/> will offer queries against documentation 
        /// using the query language of <see cref="ARComponents.ARCQuery"/>.<br>
        /// (Note: The queries are only textual in description, explaining why <see cref="ARComponents.ARCDoc"/> 
        /// does not have to link to the <see cref="ARComponents.ARCQuery"/> project).
        /// </summary>
        private bool _ARCQueryIsAvailable = false;

        [ClassMember(Description = "Set by -" + nameof(CreateAndInsert) + "-.")]
        public DocLinkCollection? DocLinks { get; private set; }

        ///// <param name="rootDataStorage">
        ///// The parameter into which this method will put an instance of <see cref="Documentator"/>
        ///// </param>
        ///// <param name="assemblies">

        /// <summary>
        /// Note type of return value, it is not <see cref="Documentator"/> but <see cref="DocLinkCollection"/>
        /// The created instance of <see cref="Documentator"/> will be inserted into <paramref name="rootDataStorage"/>
        /// </summary>
        /// <param name="assemblies">
        /// Assemblies into which to look for enums and classes. Normally taken from <see cref="UtilCore.Assemblies"/>
        /// </param>
        /// <param name="initialDescription">
        /// The 'getting started' text on the 'front-page' / root level of the documentation generated.
        /// If not given then description for <see cref="ARConcepts.GettingStarted"/> will be used.
        /// </param>
        /// <param name="specialPrefixes">
        /// Should contain prefixes for which links are also desired for potential links without the prefix.
        /// See <see cref="ARComponents.ARCQuery"/> class QUtil for example of use.
        /// </param>
        /// <param name="ARCQueryIsAvailable">
        /// If TRUE then <see cref="ToHTMLSimpleSingle"/> will offer queries against documentation using the query language of <see cref="ARComponents.ARCQuery"/>.<br>
        /// (Note: The queries are only textual in description, explaining why the <see cref="ARComponents.ARCDoc"/> project 
        /// does not need a reference to the <see cref="ARComponents.ARCQuery"/> project).
        /// </param>
        [ClassMember(
            Description =
                "Creates an instance of -" + nameof(Documentator) + "- with a natural hierarchical representation of documentation attributes found in the given assemblies.\r\n" +
                "\r\n" +
                "Inserts this representation in the given root data storage parameter under the -" + nameof(PSPrefix.doc) + "- key.\r\n" +
                "(this means that actual returned value is most probably not consumed by caller).\r\n" +
                "The reason for this somewhat curious behaviour is for -" + nameof(HLocationCollection) + "-.-" + nameof(HLocationCollection.Create) + "- to work.\r\n" +
                //"\r\n" +
                "Creates documentation for your application based on attributes like " +
                "-" + nameof(ClassAttribute) + "-, " +
                "-" + nameof(ClassMemberAttribute) + "-, " +
                "-" + nameof(EnumAttribute) + "-,\r\n" +
                "-" + nameof(EnumMemberAttribute) + "-,\r\n" +
                "-" + nameof(PKTypeAttribute) + "-,\r\n" +
                "-" + nameof(PKLogAttribute) + "- " +
                "-" + nameof(PKRelAttribute) + "- " +
                "-" + nameof(PKHTMLAttribute) + "- " +
                "and so on\r\n" +
                "\r\n" +
                "Note how documentation will include both -" + nameof(ARConcepts.ApplicationSpecificCode) + "- and -" + nameof(ARConcepts.StandardAgoRapideCode) + "- " +
                "(as long as parameter assemblies contain the relevant assemblies).\r\n" +
                "Hint: Limit parameter 'assemblies' to only those assemblies containing your -" + nameof(ARConcepts.ApplicationSpecificCode) + "- " +
                "in order to have documentation of only your application, without the AgoRapide library documentation 'coming in the way'.\r\n" +
                "\r\n" +
                "Note: If you want to publish the resulting data to -" + nameof(ARConcepts.PropertyStream) + "- " +
                "you should prefix the data with -" + nameof(PSPrefix.doc) + "-.\r\n" +
                "\r\n" +
                "The parameter specialPrefixes contains prefixes for which links are also desired for potential links without the prefix.\r\n" +
                "See -" + nameof(ARComponents.ARCQuery) + "-.-QUtil- for example of use.\r\n" +
                "\r\n"
        )]
        public static Documentator CreateAndInsert(IP rootDataStorage, IEnumerable<System.Reflection.Assembly> assemblies, List<string>? specialPrefixes = null, bool ARCQueryIsAvailable = false) {
            var retval = new Documentator();

            retval._ARCQueryIsAvailable = ARCQueryIsAvailable;

            var docFrags = DocFragCollection.Create(UtilCore.Assemblies);

            /// NOTE: Code in <see cref="CreateAndInsert"/> and <see cref="ToHTMLSimpleSingle"/> must correspond

            // Content with assembly divided up into class and enum
            retval.IP["toc"] = CreateHTOCStandard(docFrags);

            // We do now have the core documentation structure in place
            // Use that in order to build documentation links.

            // Insert core documentation into root storage,  before we find the locations.
            rootDataStorage[nameof(PSPrefix.doc)] = retval;
            var hLocations = HLocationCollection.Create(rootDataStorage, excludeKey: HTOC.HTOCContentThisLevelKey);
            var (docLinks, docLinkResolutions) = DocLinkCollection.Create(
                hLocations: hLocations,
                DocLinkResolutionLocation,
                specialPrefixes: specialPrefixes);

            retval.DocLinks = docLinks;

            /// Store other information. 
            /// Note that we had to wait with AFTER call to <see cref="DocLinkCollection.Create"/> above,
            /// in order for links not to be created for each and every item being stored now:

            /// Storing the resolution pages is essential for ambigious links to resolve.
            /// Note how this location corresponds to <see cref="DocLinkResolutionLocation"/>
            retval.IP[nameof(DocLinkResolution)] = docLinkResolutions;

            /// Store "nice to have" information 
            /// This helps with demonstrating <see cref="ARConcepts.ExposingApplicationState"/> and 
            /// may be useful to query against with help of <see cref="ARComponents.ARCQuery"/>
            // Create alternative table of contents
            retval.IP["tocNamespace"] = CreateHTOCNamespace(docFrags);
            retval.IP["tocInheritance"] = CreateHTOCInheritance(docFrags);
            retval.IP[nameof(DocLink)] = docLinks;
            retval.IP[nameof(DocFrag)] = docFrags;
            retval.IP[nameof(HLocation)] = hLocations;

            return retval;
        }

        [ClassMember(Description =
            "Alternative ('override') to -" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "-.\r\n" +
            "\r\n" +
            "Contains hand coded HTML of documentation overview.\r\n" +
            "\r\n" +
            "NOTE: The mere existence of this method is not sufficient in order to avoid " +
            "-" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- being called.\r\n" +
            "(extension method will be chosen anyway if instance is of type -" + nameof(IP) + "-.\r\n" +
            "See " + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- for more details about this issue.\r\n"
        )]
        public string ToHTMLSimpleSingle(bool prepareForLinkInsertion = false, List<IKCoded>? linkContext = null) =>
            // "<p><a href=\"..\">[ROOT]</a></p>" +
            UtilDoc.GetCurrentLevelInHierarchyAsHTML(linkContext, out _, out _) +
            "<h1>AgoRapide documentation</h1>\r\n" +
            "<table><tr><th>URL</th><th>Text</th></tr>" +
            /// NOTE: Code in <see cref="CreateAndInsert"/> and <see cref="ToHTMLSimpleSingle"/> must correspond
            string.Join("", new List<(string url, string text)> {
                ("toc", "Table of contents (standard structure)."),
                ("tocNamespace", "Alternative Table of contents, structured by namespace."),
                ("tocInheritance", "Alternative Table of contents, structured by inheritance."),
                ("https://bitbucket.org/BjornErlingFloetten/ARCore", "Git repository."),
            }.Concat(new Func<List<(string url, string text)>>(() => {
                if (_ARCQueryIsAvailable) return new List<(string url, string text)>(); // Wait with better queries below
                // Use simple key lookup
                return new List<(string url, string text)> {
                    (nameof(DocFrag), "Documentation fragments, useful for querying against."),
                    (nameof(DocLink), "All possible links"), // We can not use /All here because that is a key in the DocLink collection.
                    (nameof(DocLinkResolution), "Resolution for ambigious links."),
                    (nameof(HLocation), "Hierarchical locations (of little interest)."),
                };
            })()).Select(tuple =>
                "<tr><td>" +
                "<a href=\"" + (tuple.url.StartsWith("http") ?
                    tuple.url :
                    (
                        PSPrefix.doc + "/" +
                        tuple.url
                    )
                ) + "\">" +
                System.Net.WebUtility.HtmlEncode(tuple.url) +
                "</a>" +
                "</td><td>" +
                System.Net.WebUtility.HtmlEncode(tuple.text) +
                "</td></tr>"
            )) +
            "</table>" +
            "\r\n" +
            (!_ARCQueryIsAvailable ? "" : (
                "<h2>Some sample queries against documentation</h2>\r\n" +
                "<p>" +
                "Note how queries, although predefined here, do not have to be. " +
                "You can execute any query directly on-the-fly in your browser as long as you can construct the correct URL for it." +
                "</p>" +
                "<table><tr><th>Explanation</th><th>URL</th></tr>" +
                string.Join("", new List<(string url, string text)> {
                    /// TODO: <see cref="ARConcepts.LinkInsertionInDocumentation"/> currently not possible here because would get link in link-text
                    /// TODO: Use both Title and Description, and have links in Description instead
                    (nameof(DocFrag) + "/PIVOT Assembly BY DocFragType/ORDER BY _SUM DESC", "Overview of application complexity, number of classes per assembly."),
                    (nameof(DocFrag) + "/SELECT Assembly, DocFragType, Description.FirstLine()/ORDER BY DocFragId/LIMIT 2000", "Documentation fragments, all-in-one view."),
                    (nameof(DocFrag) + "/SELECT DocFragId/SELECT DocFragId.Length() AS length/ORDER BY length DESC", "Identifier lengths / naming complexity."),
                    (nameof(DocLink) + "/ORDER BY Location", "Documentation links"), 
                    (nameof(DocLinkResolution) + "/SELECT LinkWord, DocLinks.Length()0x002F7 AS references/ORDER BY references DESC", "Resolution for ambigious links, ordered by references ('references' is a hack)."),
                    (nameof(HLocation) + "/SELECT Location, Filename/ORDER BY Location/LIMIT 2000", "Hierarchical locations (of little interest)."),
                }.Select(tuple =>
                    "<tr><td>" +
                    "<a href=\"" + PSPrefix.doc + "/" + tuple.url + "\">" +
                    System.Net.WebUtility.HtmlEncode(tuple.text) +
                    "</a>" +
                    "</td><td>" +
                    System.Net.WebUtility.HtmlEncode(tuple.url).Replace("/", "<br>") +
                    "</td></tr>"
                )) +
                "</table>"
            )) +
            "\r\n" +
            "<h2>Other information</h2>\r\n" +
            "<p>Author: Bjørn Erling Fløtten, Trondheim, Norway. bef_agorapide@bef.no</p>\r\n";

        /// <summary>
        /// NOTE: Do not change this without also changing code in <see cref="CreateAndInsert"/>
        /// </summary>
        [ClassMember(Description =
            "Default position within the global storage context of doc link resolution pages.\r\n" +
            "\r\n" +
            "Value: 'doc/DocLinkResolution'."
        )]
        public static List<IKCoded> DocLinkResolutionLocation = new List<IKCoded> {
            IKCoded.FromEncoded(nameof(PSPrefix.doc)),
            IKCoded.FromEncoded(nameof(DocLinkResolution))
        };

        [ClassMember(Description =
            "Creates a 'standard' table of contents, structured by assembly and each assembly divided into 'Class' and 'Enum'.\r\n" +
            "\r\n" +
            "See also -" + nameof(CreateHTOCNamespace) + "- and -" + nameof(CreateHTOCInheritance) + "-."
        )]
        public static HTOC CreateHTOCStandard(DocFragCollection docFrags) => HTOC.Create<HTOC>(
            content: docFrags.Select(ikip => (IKIP?)ikip).ToList(),
            contentThisLevel: HTOC.CreateInitialContent(
                key: "dummy",
                    description:
                    "Table of contents.\r\n" +
                    "\r\n" +
                    EnumMemberAttribute.GetAttribute(ARConcepts.GettingStarted).IP.GetPV<string>(BaseAttributeP.Description)
            ).P,
            filterNode: new HTOC.FilterSingle(
                filter: DocFrag.HTOCFilterAllAssemblies,
                next: new HTOC.FilterBranch(new List<(Func<HTOC.ParentChain?, IKIP> initialContent, HTOC.FilterNode firstNode)> {
                    (
                        parent => HTOC.CreateInitialContent("Class",
                            "All classes with some AgoRapide specific attributes found in assembly " + parent!.IKIP.P.GetPV<string>(DocFragP.Name)),
                        new HTOC.FilterSingle(
                            filter: DocFrag.HTOCFilterAllClassesWithAssemblyEqualToParentsParent,
                            next: new HTOC.FilterSingle(
                                filter: DocFrag.HTOCFilterAllClassMembersWithTypeEqualToParent
                            )
                        )
                    ),
                    (
                        parent => HTOC.CreateInitialContent("Enum",
                            "All enums found in assembly " + parent!.IKIP.P.GetPV<string>(DocFragP.Name)),
                        new HTOC.FilterSingle(
                            filter: DocFrag.HTOCFilterAllEnumsWithAssemblyEqualToParentsParent,
                            next: new HTOC.FilterSingle(
                                filter: DocFrag.HTOCFilterAllEnumMembersWithTypeEqualToParent
                            )
                        )
                    )
                })
            )
        );

        [ClassMember(Description =
            "Creates a table of contents structured by namespace\r\n." +
            "\r\n" +
            "See also -" + nameof(CreateHTOCStandard) + "- and -" + nameof(CreateHTOCInheritance) + "-."
        )]
        public static HTOC CreateHTOCNamespace(DocFragCollection docFrags) => HTOC.Create<HTOC>(
            content: docFrags.Select(ikip => (IKIP?)ikip).ToList(),
            contentThisLevel: HTOC.CreateInitialContent(
                key: "dummy",
                    description: "Table of contents structured by namespace."
            ).P,
            filterNode: new HTOC.FilterSingle(
                filter: DocFrag.HTOCFilterAllRootNamespaces,
                next: new HTOC.FilterSingle(
                    filter: DocFrag.HTOCFilterNamespacesClassesAndEnumsWithNamespaceEqualToParent,
                    next: new HTOC.FilterSingle(
                        filter: DocFrag.HTOCFilterNamespacesClassesAndEnumsWithNamespaceEqualToParent,
                        next: new HTOC.FilterSingle(
                            filter: DocFrag.HTOCFilterNamespacesClassesAndEnumsWithNamespaceEqualToParent,
                            // Attempt to document additional levels should be superfluous for any sane 
                            // application because it should not have even deeper levels
                            next: new HTOC.FilterSingle(
                                filter: DocFrag.HTOCFilterNamespacesClassesAndEnumsWithNamespaceEqualToParent,
                                next: new HTOC.FilterSingle(
                                    filter: DocFrag.HTOCFilterNamespacesClassesAndEnumsWithNamespaceEqualToParent,
                                    next: new HTOC.FilterSingle(
                                        filter: DocFrag.HTOCFilterNamespacesClassesAndEnumsWithNamespaceEqualToParent
                                    )
                                )
                            )
                        )
                    )
                )
            )
        );

        [ClassMember(Description =
            "Creates a table of contents structured by inheritance (classes and interfaces only, not members).\r\n." +
            "\r\n" +
            "See also -" + nameof(CreateHTOCStandard) + "- and -" + nameof(CreateHTOCNamespace) + "-."
        )]
        public static HTOC CreateHTOCInheritance(DocFragCollection docFrags) => HTOC.Create<HTOC>(
            content: docFrags.
                // Reduce size of collection before starting, because Create kan no longer set to null each matched item
                // (each item may be matched multiple times since we have both interfaces and classes as parents)
                Where(ikip => ikip.P.GetPV<DocFragType>(DocFragP.DocFragType, DocFragType.__invalid) == DocFragType.Class).
                        Select(ikip => (IKIP?)ikip).ToList(),
            contentThisLevel: HTOC.CreateInitialContent(
                key: "dummy",
                    description: "Table of contents structured by inheritance (classes and interfaces only, not members)."
            ).P,
            possibleDuplicatePositions: true, // Because interfaces are also included, a class may appear in multiple positions now.
            filterNode: new HTOC.FilterSingle(
                filter: DocFrag.HTOCFilterAllSuperclasses,
                next: new HTOC.FilterSingle(
                    filter: DocFrag.HTOCFilterAllClassesWithBaseTypeOrInterfaceEqualToParent,
                    next: new HTOC.FilterSingle(
                        filter: DocFrag.HTOCFilterAllClassesWithBaseTypeOrInterfaceEqualToParent,
                        next: new HTOC.FilterSingle(
                            filter: DocFrag.HTOCFilterAllClassesWithBaseTypeOrInterfaceEqualToParent,
                            // Attempt to document additional levels should be superfluous for any sane 
                            // application because it should not have even deeper levels
                            next: new HTOC.FilterSingle(
                                filter: DocFrag.HTOCFilterAllClassesWithBaseTypeOrInterfaceEqualToParent,
                                next: new HTOC.FilterSingle(
                                    filter: DocFrag.HTOCFilterAllClassesWithBaseTypeOrInterfaceEqualToParent,
                                    next: new HTOC.FilterSingle(
                                        filter: DocFrag.HTOCFilterAllClassesWithBaseTypeOrInterfaceEqualToParent
                                    )
                                )
                            )
                        )
                    )
                )
            )
        );

        // 
        // public void BuildDocumentationHierarchy(IEnumerable<System.Reflection.Assembly> assemblies, string? initialDescription = null) {
        //    IP.AddPV(DocumentatorP._Description, initialDescription ?? EnumMemberAttribute.GetAttribute(ARConcepts.GettingStarted).IP.GetPV<string>(BaseAttributeP.Description)); /// This results in text from <see cref="ARConcepts.GettingStarted"

        //    var allAssemblies = new PRich();
        //    IP.AddP(IKString.FromString("Assembly"), allAssemblies);
        //    allAssemblies.IP.AddPV(DocumentatorP._Description, "All assemblies covered by this documentation.");

        //    assemblies.ForEach(a => {
        //        var assemblyName = a.GetName().Name;

        //        var thisAssembly = new PRich();
        //        allAssemblies.IP.AddP(IKString.FromCache(assemblyName), thisAssembly);
        //        if (UtilCore.EnumTryParse<ARComponents>(assemblyName, out var c)) {
        //            thisAssembly.IP.AddPV(DocumentatorP._Description, EnumMemberAttribute.GetAttribute(c).IP.GetPV<string>(BaseAttributeP.Description));
        //        }

        //        string TypeToString(Type t) {
        //            // Include namespace if different from assembly name (but not any part starting with assembly name).
        //            if (t.Namespace.Equals(assemblyName)) return t.ToStringShort();
        //            if (t.Namespace.StartsWith(assemblyName + ".")) return t.Namespace[(assemblyName + ".").Length..] + "." + t.ToStringShort();
        //            return t.Namespace + "." + t.ToStringShort();
        //        }

        //        // Part 1, class documentation
        //        // ===============================
        //        // Some example property stream lines resulting are:
        //        // "Class/Documentator/_Member/Document/MethodName = Document"
        //        // "Class/Documentator/_Member/Document/Description = Creates documentation for your application based on attributes ..."
        //        var allClasses = new PRich();
        //        allClasses.IP.AddPV(DocumentatorP._Description, "All classes with some AgoRapide specific attributes found in assembly -" + assemblyName + "-.");
        //        thisAssembly.IP["Class"] = allClasses; // Demonstrates us of indexing instead of AddP
        //        a.GetTypes().Where(t => !t.IsEnum).ForEach(t => {
        //            var mas = t.GetMembers().Select(e => {
        //                if ((e.MemberType & System.Reflection.MemberTypes.NestedType) == System.Reflection.MemberTypes.NestedType) {
        //                    // Nested type means a type declared inside this class (an inner type).
        //                    /// If we had tried to use it, it would most probably result in a <see cref="BaseAttribute.IncorrectAttributeTypeUsedException"/>
        //                    return null;
        //                }
        //                if (e.DeclaringType != t) {
        //                    // Inherited, see below for masInheritedNamesOnly
        //                    return null;
        //                }
        //                var ma = ClassMemberAttribute.GetAttribute(e);
        //                if (ma.IsDefault) {
        //                    return null; // Documenting members without attributes is of little interest
        //                }
        //                return ma;
        //            }).Where(ma => ma != null).Select(ma => ma!).ToList(); // ma => ma! in order to eliminate Intellisense / Compiler warnings.

        //            var a = ClassAttribute.GetAttribute(t);

        //            if (a.IsDefault && mas.Count == 0) {
        //                // No attribute for class, and no attribute for any member, do not document class at all
        //                return;
        //            }

        //            var thisClass = a;
        //            allClasses.IP.AddP(
        //                // Old key, before 12 Mar 2021
        //                // IKType.FromType(t)
        //                // New key, from 12 Mar 2021 (include namespace)
        //                IKString.FromCache(TypeToString(t)),
        //                thisClass
        //            );

        //            var tToStringShort = t.ToStringShort();

        //            if (mas.Count > 0) {
        //                var members = new PRich();
        //                mas.ForEach(ma => {
        //                    var maName = ma.IP.GetPV<string>(ClassMemberAttributeP.MethodName);
        //                    var overloads = mas.Where(c => c.IP.GetPV<string>(ClassMemberAttributeP.MethodName) == maName).ToList();
        //                    if (overloads.Count > 1) overloads = overloads.OrderBy(c => c.IP.GetPV<string>(ClassMemberAttributeP.MethodSignature)).ToList();
        //                    var memberName = ma.IP.GetPV<string>(ClassMemberAttributeP.MethodName) + (overloads.Count == 1 ?
        //                        "" : // Use only MethodName for key, only member with this name
        //                        (
        //                            // Use MethodName + space + Overload1 / Overload2 and so on.
        //                            " Overload" + (overloads.FindIndex(c => c.IP.GetPV<string>(ClassMemberAttributeP.MethodSignature) == ma.IP.GetPV<string>(ClassMemberAttributeP.MethodSignature)) + 1)
        //                        )
        //                    );
        //                    members.IP.AddP(
        //                        IKString.FromCache(memberName),
        //                        ma
        //                    );
        //                });
        //                thisClass.IP.AddP(
        //                    PK.FromEnum(DummyEnumP._Member),  // NOTE: / TODO: Using DummyEnum is a hack because BaseAttribute accepts only PK as key.
        //                    members                           // Careful, if already attribute contains key _Member, this will not execute
        //                );
        //            }

        //            // For inherited methods we store only the string-values (not the whole objects).
        //            // This reduces number of files / document fragments.
        //            var masInheritedNamesOnly = t.GetMembers().Select(e => {
        //                if ((e.MemberType & System.Reflection.MemberTypes.NestedType) == System.Reflection.MemberTypes.NestedType) {
        //                    // See comment above for mas.
        //                    return null;
        //                }
        //                if (e.DeclaringType == t) {
        //                    // Not inherited. Will ordinary having been listed already (see mas collection above)

        //                    // HOWEVER, if no attribute found for this method (meaning, not listed alreday),
        //                    // AND, if base-class has a non-default attribute for this, use that
        //                    // because that is where the documentation will probably exist.
        //                    if (!ClassMemberAttribute.GetAttribute(e).IsDefault) {
        //                        return null; // Not needed, found non-default attribute for this class, and it has already been listed
        //                    } else {
        //                        // Note that this call is somewhat expensive, and is frequently executed (for every class member without an attribute)
        //                        var ma = ClassMemberAttribute.GetAttributeFromBaseClassOfDeclaringType(e);
        //                        if (ma.IsDefault) {
        //                            return null; // Documenting members without attributes is of little interest
        //                        } else {
        //                            return e.Name;
        //                        }
        //                    }
        //                } else {
        //                    // NOTE: Since metod is inherited, it is for the declaring type that we must look after an attribute
        //                    var ma = ClassMemberAttribute.GetAttribute(e.DeclaringType, e.Name);
        //                    if (ma.IsDefault) {
        //                        return null; // Documenting members without attributes is of little interest
        //                    } else {
        //                        return e.Name;
        //                    }
        //                }
        //            }).Where(name => name != null).Select(name => name!).ToList(); // name => name! in order to eliminate Intellisense / Compiler warnings.                        
        //            if (masInheritedNamesOnly.Count > 0) {
        //                thisClass.IP.AddPV(
        //                    PK.FromEnum(DummyEnumP._MemberInherited),  // NOTE: / TODO: Using DummyEnum is a hack because BaseAttribute accepts only PK as key.
        //                    masInheritedNamesOnly                      // Careful, if already attribute contains key _MemberInherited, this will not execute
        //                );
        //            }
        //        });

        //        // Part 2, enum documentation
        //        // ===============================
        //        var allEnums = new PRich();
        //        allEnums.IP.AddPV(DocumentatorP._Description, "All enums found in assembly -" + assemblyName + "-.");
        //        thisAssembly.IP["Enum"] = allEnums; // Demonstrates us of indexing instead of AddP

        //        // Some example property stream lines resulting are:
        //        // Enum/ARConcepts/_Member/PropertyStream/EnumType = ARCCore.ARConcepts
        //        // Enum/ARConcepts/_Member/PropertyStream/EnumMember = PropertyStream
        //        // Enum/ARConcepts/_Member/PropertyStream/Description = The concept of how all data is broken down into single ...
        //        // Enum/ARConcepts/_Member/PropertyStream/LongDescription = Note how the format used is somewhat verbose but ...

        //        a.GetTypes().Where(t => t.IsEnum).ForEach(t => {
        //            var a = EnumAttribute.GetAttribute(t);
        //            var thisEnum = a;
        //            allEnums.IP.AddP(
        //                // Old key, before 12 Mar 2021
        //                // IKType.FromType(t)
        //                // New key, from 12 Mar 2021 (include namespace)
        //                IKString.FromCache(TypeToString(t)),
        //                thisEnum
        //            );

        //            var tToStringVeryShort = t.ToStringVeryShort();

        //            var members = new PRich();
        //            UtilCore.EnumGetMembers(t).ForEach(e => {
        //                // Note that for enums (in contrast to classes below), we document every value regardless of there
        //                // being a non-default attribute defined or not.
        //                var ma = a.IP.GetPV<AREnumType>(EnumAttributeP.AREnumType) == AREnumType.PropertyKeyEnum ?
        //                    PK.FromEnum(e).IP : // Note that it is important to choose correct between PK.FromEnum and EnumMemberAttribute.GetAttribute (incorrect choice results in an exception being thrown)
        //                    EnumMemberAttribute.GetAttribute(e).IP;
        //                members.IP.AddP(IKString.FromCache(e.ToString()), ma);
        //            });
        //            thisEnum.IP.AddP(
        //                PK.FromEnum(DummyEnumP._Member),  // NOTE: / TODO: Using DummyEnum is a hack because BaseAttribute accepts only PK as key.
        //                members                           // Careful, if already attribute contains key _Member, this will not execute
        //            );
        //        });
        //    });
        //}


        // TODO: Delete commented out code
        //[Class(Description =
        //    "Collection of potential HTML links.\r\n" +
        //    "\r\n" +
        //    "Result of calling -" + nameof(FindPotentialLinks) + "-.\r\n" +
        //    "\r\n" +
        //    "Building block for providing -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "-."
        //)]
        //public class PotentialLinks {

        //    public IEnumerable<DocLink> Links { get; private set; }

        //    [ClassMember(Description =
        //        "For duplicate links, shows possible targets from which the user browsing the structure can choose the desired link.\r\n" +
        //        "\r\n"
        //        //"These pages should be made available as [ROOT]/_TargetResolution/[targetResolutionPage].html " +
        //        //"in order for the links to work.\r\n"
        //    )]
        //    public IP DocLinkResolution { get; private set; }

        //    public PotentialLinks(IEnumerable<DocLink> links, IP docLinkResolutions) {
        //        Links = links;
        //        DocLinkResolution = docLinkResolutions;
        //    }

        //    [ClassMember(Description =
        //        "Transforms class into -" + nameof(IP) + "-.\r\n" +
        //        "(NOTE / TODO: One could argue for having this class implementing IP originally would have been better).\r\n"
        //    )]
        //    public IP ToIP() {
        //        var retval = (IP)new PRich();
        //        var links = (IP)new PRich();
        //        retval.AddP(nameof(Links), links);
        //        Links.ForEach(l => links.AddP(l.LinkWordHTML, l));
        //        retval.AddP(nameof(DocLinkResolution), DocLinkResolution);
        //        return retval;
        //    }
        //}

        public class DocumentatorException : ApplicationException {
            public DocumentatorException(string message) : base(message) { }
            public DocumentatorException(string message, Exception innerException) : base(message, innerException) { }
        }
    }

    [Enum(
        Description =
            "Describes class -" + nameof(Documentator) + "-.\r\n",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum DocumentatorP {
        [PKType(
        Description =
            "The general description of the given object.\r\n" +
            "\r\n" +
            "TODO: Consider replacing this with just BaseAttributeP.Description.\r\n" +
            "TODO: That is, just delete this enum altogether.\r\n" +
            "TODO: This may cause problems however when we have collections where key already is Description.\r\n" +
            "\r\n" +
            "Useful in tables, as link text and so on (see for instance -" + nameof(Extensions.DescribeValue) + "-).\r\n" +
            "\r\n" +
            "Examples could be like\r\n" +
            "FirstName + LastName for a Customer object, like 'John Smith' or " +
            "Quantity, Item and Sum for an Order line object like '200 x Fidget42 = USD 42.00.\r\n" +
            "\r\n" +
            "TODO: Improve on text here:\r\n" +
            "Note that in general, methods like -" + nameof(Extensions.DescribeValue) + "- is often able to describe an object without " +
            "needing an explicit description like offered by this attribute " +
            "(for instance by looking at -" + nameof(PKDocAttributeP.PriorityOrder) + "-).\r\n" +
            "\r\n" +
            "This key is often used in various collections throughout AgoRapide, in order to describe what the collection contains.\r\n" +
            "When iterating over the individual objects, this key is often excluced.\r\n" +
            "Example: With a collection of 'Customer' objectes, with keys, like '42', '43', '44' and so on, the key _Description is " +
            "not relevant when iterating over collection (because a uniform set of only 'Customer' objects is desired).\r\n" +
            "\r\n" +
            "See also -" + nameof(BaseAttributeP.Description) + "- and -" + nameof(BaseAttributeP.LongDescription) + "-.\r\n" +
            "\r\n" +
            "TODO: This concept is somewhat incompletely incorporated in AgoRapide, and the little use there is has been done in a haphazard manner."
        )]
        _Description,
    }

    [Class(Description = "TODO: Document better what this is for (and change name)")]
    public class DummyEnum : PRich {
    }

    [Enum(Description = "TODO: Document better what this is for (and change name)", AREnumType = AREnumType.PropertyKeyEnum)]
    public enum DummyEnumP {
        _Member,
        [PKType(
            Description = "Contains not only members which are inherited in the object-oriented sense, but also overrides for which the only documentation was found in base class",
            Cardinality = Cardinality.WholeCollection)]
        _MemberInherited
    }
}