﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ARCCore
{
    [Class(Description =
        "Container class for key (-" + nameof(IK) + "-), and value (-" + nameof(IP) + "-).\r\n" +
        "\r\n" +
        "The key could for instance be '42' and the value a Customer object, meaning that the Customer object has a primary key with value '42.\r\n" +
        // "Another example is Key 'typeof(Customer)' and value a collection of Customer objects.\r\n" +
        "\r\n" +
        "Note that entities in AgoRapide do not contain their own identities (their primary keys).\r\n" +
        "For instance, a Customer-object will not contain the field CustomerId.\r\n" +
        "This is in order to preserve memory because entities are always stored in a collection with a key, and that key is the primary key.\r\n" +
        "In other words, also storing it within the object would be to store a duplicate (at least a duplicate pointer) and thus use excessive memory."
    )]
    public class IKIP
    {
        public IK Key { get; }
        public IP P { get; }

        public IKIP(IK k, IP p)
        {
            Key = k;
            P = p;
        }
        public IKIP(string keyAsString, IP p) : this(IKString.FromString(keyAsString), p) { }

        // TODO: Verify that this was correct to add
        // public IKIP(Type keyAsType, IP p) : this(IKType.FromType(keyAsType), p) { }

        [ClassMember(Description = "See -" + nameof(TryAssertTypeIntegrity) + "- for documentation.")]
        public void AssertTypeIntegrity()
        {
            if (!TryAssertTypeIntegrity(out var errorResponse))
            {
                throw new IKIPTypeIntegrityException(errorResponse);
            }
        }

        [ClassMember(
            Description =
                "Asserts that value is of correct type as specified by the key.\r\n" +
                "\r\n" +
                "Note that not called from constructor because it might have some performance limitations. " +
                "\r\n" +
                "Typically called from implementators of -" + nameof(IP.TrySetP) + "-. " +
                "As of Apr 2020 only able to assert integrity when -" + nameof(PValue<TValue>) + "- is used for packing a value, or where type matches exact.\r\n" +
                "\r\n" +
                "This method is an attempt to mitigate the lack of static typing in the -" + nameof(ARConcepts.PropertyAccess) + "- mechanism " +
                "(AgoRapide is less static strong typed than C# itself is by default. " +
                "In other words type mistakes are not necessarily exposed at compile time)."
        )]
        public bool TryAssertTypeIntegrity(out string errorResponse)
        {
            if (!(Key is PK pk))
            {
                // We have no information to check against. The only thing we now is that value should inherit IP, something we know it does (P is IP here)
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }

            var t = P.GetType();

            if (UtilCore.CurrentlyStartingUp)
            {
                /// Hardcoded exceptional assertions for some specific properties only performed at startup

                /// Special case because of chicken-and-egg problem related to property keys containing property keys
                /// (leading to <see cref="PK.FromEnum"/> being called an excessive number of times)
                /// (the crux is when we ask for <see cref="PKTypeAttribute.Cardinality"/> below, this will trigger a whole 
                /// new series of calls to <see cref="PK.FromEnum"/>.
                /// 
                /// We have therefore hardcoded this method for some known properties.
                /// 
                /// Note that the initialization process is somewhat messy anyway, as demonstrated if you let 
                /// <see cref="PK.FromEnum"/> log every call at application startup.

                bool Asserter(Type requiredType, out string errorResponse)
                {
                    if (!requiredType.IsAssignableFrom(t))
                    {
                        errorResponse = "-" + nameof(TryAssertTypeIntegrity) + "-: " +
                            "The key -" + pk.__enum.GetType().ToStringShort() + "-.-" + pk.ToString() + "- requires a value of type -" + requiredType.ToStringShort() + "- " +
                            "but the type found was -" + t.ToStringShort() + "-.\r\n";
                        return false;
                    }
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
                }

                if (pk.__enum is BaseAttributeP baseAttribute)
                {
                    switch (baseAttribute)
                    {
                        case BaseAttributeP.Description: return Asserter(typeof(PValue<string>), out errorResponse);
                        case BaseAttributeP.LongDescription: return Asserter(typeof(PValue<string>), out errorResponse);
                        default: break; // Do normal assertion
                    }
                }
                else if (pk.__enum is EnumAttributeP enumAttribute)
                {
                    switch (enumAttribute)
                    {
                        case EnumAttributeP.EnumType: return Asserter(typeof(PValue<Type>), out errorResponse);
                        case EnumAttributeP.AREnumType: return Asserter(typeof(PValue<AREnumType>), out errorResponse);
                        case EnumAttributeP.CorrespondingClass: return Asserter(typeof(PValue<Type>), out errorResponse);
                        default: break; // Do normal assertion
                    }
                }
                else if (pk.__enum is EnumMemberAttributeP enumMemberAttribute)
                {
                    switch (enumMemberAttribute)
                    {
                        case EnumMemberAttributeP.EnumType: return Asserter(typeof(PValue<Type>), out errorResponse);
                        case EnumMemberAttributeP.EnumMember: return Asserter(typeof(PValue<string>), out errorResponse);
                        default: break; // Do normal assertion
                    }
                }
                else if (pk.__enum is PKTypeAttributeP pkTypeAttribute)
                {
                    switch (pkTypeAttribute)
                    {
                        case PKTypeAttributeP.Type: return Asserter(typeof(PValue<Type>), out errorResponse);
                        case PKTypeAttributeP.IsObligatory: return Asserter(typeof(PValue<bool>), out errorResponse);
                        case PKTypeAttributeP.Cardinality: return Asserter(typeof(PValue<Cardinality>), out errorResponse);
                        default: break; // Do normal assertion
                    }
                }
                else
                {
                    // Do normal assertion
                }
            }

            // Normal general assertion starts here

            if (
                !t.IsGenericType ||
                // Added 28 Jan 2022 in order to allow storing IP derived generic classes
                !t.GetGenericTypeDefinition().Equals(typeof(PValue<>))
                // TODO: Now we can only use PValue for packing final values
            )
            {
                // Compare directly with required type. 
                // This is the situation when we store IP-based values in other IP-based objects, that is, without 
                // packing inside PValue{T}
                // NOTE: This is actually the NON-NORMAL situation (remember that ordinary properties are normally 
                // NOTE: stored within a generic type like PValue<T>.

                // TODO: What about attempts to store any generic type directly (below only PValue<t> is accepted now)?

                if (t.Equals(typeof(PValueEmpty)))
                {
                    // TODO: Decide how to implement null values / empty values in general.
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
                }
                // TODO: Implement check for cardinality (List{RequiredType})
                if (!pk.Type.IsAssignableFrom(t))
                {
                    errorResponse = "-" + nameof(TryAssertTypeIntegrity) + "-: " +
                        "The key -" + pk.__enum.GetType().ToStringShort() + "-.-" + pk.ToString() + "- requires a value of type -" + pk.Type.ToStringShort() + "- " +
                        "but the type found was -" + t.ToStringShort() + "-.\r\n";
                    return false;
                }

                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;

            }

            // This is assumed to be the typical situation

            // Removed 28 Jan 2022, see text above
            //if (!t.GetGenericTypeDefinition().Equals(typeof(PValue<>))) {
            //    errorResponse = nameof(TryAssertTypeIntegrity) + ": " +
            //        "Only the generic type " + nameof(PValue<TValue>) + " is understood here.\r\n" +
            //        "Some background information:\r\n" +
            //        "TODO: Consider expanding for other types too.\r\n" +
            //        "In practice is probably does not matter as long as type found (" + t.ToStringShort() + ") " +
            //        "inherits " + nameof(IP) + " and have one generic argument, which is the argument we are checking against now\r\n" +
            //        "\r\n" +
            //        "See also comment in -" + nameof(PK.PackParseResultForStorageInEntityObject) + "- about using " +
            //        "other classes than PValue{T} for packing.\r\n" +
            //        "If ever introduced, it should also be reflected here.\r\n";
            //    return false;
            //}

            var l = t.GetGenericArguments();
            switch (l.Length)
            {
                case 1:
                    switch (pk.Cardinality)
                    {
                        case Cardinality.HistoryOnly:
                            if (!pk.Type.IsAssignableFrom(l[0]))
                            {
                                errorResponse = "-" + nameof(TryAssertTypeIntegrity) + "-: " +
                                    "The key -" + pk.__enum.GetType().ToStringShort() + "-.-" + pk.ToString() + "- requires a value of type -" + pk.Type.ToStringShort() + "- " +
                                    "but the type found was -" + l[0].ToStringShort() + "- " +
                                    "(as generic argument to -" + t.ToStringShort() + "-).\r\n" +
                                    (l[0].IsGenericType && l[0].GetGenericTypeDefinition().Equals(typeof(PValue<>)) ?
                                        "Possible cause: Did you by accident 'package' a PValue<TValue>-object inside a PValue<TValue>-object?" : "") +
                                    "\r\n";
                                return false;
                            }
                            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            return true;
                        case Cardinality.WholeCollection:
                            var rt = typeof(List<>).MakeGenericType(new Type[] { pk.Type });
                            if (!rt.Equals(l[0]))
                            {
                                errorResponse = "-" + nameof(TryAssertTypeIntegrity) + "-: " +
                                    "The key -" + pk.__enum.GetType().ToStringShort() + "-.-" + pk.ToString() + "- requires a value of type -" + pk.Type.ToStringShort() + "- and since " +
                                    "-" + nameof(pk.Cardinality) + "- = -" + pk.Cardinality + "-, the required type here now is " + rt.ToStringShort() + " " +
                                    "but the type found was " + l[0].ToStringShort() + " " +
                                    "(as generic argument to " + t.ToStringShort() + ").\r\n";
                                return false;
                            }
                            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            return true;
                        case Cardinality.IndividualItems:
                            throw new InvalidEnumException(pk.Cardinality, "Not supported yet / not decided how to support");
                        default:
                            throw new InvalidEnumException(pk.Cardinality);
                    }
                default:
                    throw new InvalidTypeException(t,
                        "Invalid number of generic arguments (" + l.Length + ") for " + typeof(PValue<>).ToStringShort() + ", 1 was expected. " +
                        "The generic arguments found where " + string.Join(", ", l.Select(t => t.ToStringShort())) + "\r\n");
            }
        }

        public override string ToString() => nameof(Key) + ": " + Key + ", " + nameof(P) + ": " + P.ToString(); // TODO: We should have a "short" representation here.

        public class IKIPTypeIntegrityException : ApplicationException
        {
            public IKIPTypeIntegrityException(string message) : base(message) { }
        }
    }
}