﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using System.Linq;

/// Note how this file contains multiple items, among them are:
/// <see cref="ARCCore.IP"/>       IP = IProperty, common central mechanism for implementing <see cref="ARCCore.ARConcepts.PropertyAccess"/>
/// <see cref="ARCCore.IKIP"/>     Container class for key and value (<see cref="ARCCore.IK"/> and <see cref="ARCCore.IP"/>)
/// <see cref="ARCCore.P"/>        Dummy-class, not necessarily in use
/// <see cref="ARCCore.PP"/>       Describes meta data for any class (property) implementing <see cref="ARCCore.IP"/>
/// <see cref="ARCCore.ICreated"/> Dummy interface signifying that implementing class has the ability to store metadata <see cref="ARCCore.PP.Created"/> 
namespace ARCCore
{

    // NOTE: "STRANGE" POSITION OF SOME ELEMENTS IN THIS INTERFACE IS CAUSED BY A BUG IN .NET / Compiler / ... ???
    // NOTE: Search POSITION 1 FOR MORE INFORMATION

    // TODO: As interface "stabilizes", repeat all documentation for all overloads, both as ClassMemberAttribute-tags and as XML-comments

    [Class(
        Description =
            "IP = IProperty.\r\n" +
            "\r\n" +
            "Common central mechanism for implementing -" + nameof(ARConcepts.PropertyAccess) + "-.\r\n" +
            "\r\n" +
            "'Everything' in your application, especially entity classes (like 'Customer', 'Order' and so on) " +
            "but also classes which are (in a hierarchical sense) over and under the entity level,\r\n" +
            "should implement this interface.\r\n" +
            "\r\n" +
            "Examples of classes implementing this interface as seen from a hierarchical storage viewpoint:\r\n" +
            "-" + nameof(PCollection) + "-: Collection of objects, like CustomerCollection.\r\n" +
            "-" + nameof(PRich) + "-: Customer object.\r\n" +
            "-" + nameof(PValue<TValue>) + "-: Customer.FirstName.\r\n" +
            "(note that the topmost level containing all collections should also implement this interface, " +
            "this can be accomplished with the universal -" + nameof(PRich) + "- class.)" +
            "\r\n" +
            "You should also let internal parts of your application implement this interface. " +
            "This will make logging and debugging much easier for instance (see -" + nameof(ARConcepts.ExposingApplicationState) + "-).\r\n" +
            "\r\n" +
            "Methods that must be implemented are:\r\n" +
            "-" + nameof(TrySetP) + "-\r\n" +
            "-" + nameof(TryGetP) + "-\r\n" +
            "-" + nameof(TryGetV) + "-\r\n" +
            "-" + nameof(GetEnumerator) + "-\r\n" +
            "and also:\r\n" +
            "-" + nameof(DeepCopy) + "-\r\n" +
            "-" + nameof(TryRemoveP) + "-\r\n" +
            "-" + nameof(OnTrySetP) + "-\r\n" +
            "-" + nameof(TrySetPP) + "-\r\n" +
            "-" + nameof(GetKeysEqualToValue) + "-\r\n" +
            "\r\n" +
            "Some of the implementing classes in -" + nameof(ARComponents.ARCCore) + "- are:\r\n" +
            "-" + nameof(PRich) + "- Fully flexible storage. Can store anything. Typical inherited for entity representations like 'Customer' or 'Order'.\r\n" +
            "-" + nameof(PExact<TPropertyKeyEnum>) + "- Very -" + nameof(ARConcepts.MemoryConsumption) + "- efficient alternative to -" + nameof(PRich) + "-.\r\n" +
            "-" + nameof(PConcurrent) + "- A (somewhat) thread-safe alternative to -" + nameof(PRich) + "- which also supports -" + nameof(ARConcepts.ExposingApplicationState) + "-.\r\n" +
            "-" + nameof(PValue<TValue>) + "- Single value storage.\r\n" +
            "-" + nameof(PValueEmpty) + "-.\r\n" +
            "-" + nameof(PCollection) + "- A class useful for storing a collection of entities (a table in RDBMS-terms) because it implements -" + nameof(ARConcepts.Indexing) + "-.\r\n" +
            "-" + nameof(PReadOnly) + "- Experimental as of Feb 2022.\r\n" +
            "\r\n" +
            "Use these as base classes for your own entities and properties in your -" + nameof(ARConcepts.ApplicationSpecificCode) + "-.\r\n" +
            "\r\n" +
            "In -" + nameof(ARComponents.ARCCore) + "- there is also -PCollectionES- which supports -" + nameof(ARConcepts.EventSourcing) + "-.\r\n" +
            "\r\n" +
            "NOTE: Some ideas for new classes implementing this interface could be:\r\n" +
            "1) PRichImmutable, a quasi-immutable class for which you could (after initialization) set a flag stopping further updates to the class.\r\n" +
            "\r\n" +
            "(The difference between -" + nameof(ITypeDescriber) + "- and -" + nameof(IP) + "- is that the former must be stored 'inside' an -" + nameof(IP) + "-.\r\n" +
            "In RDBMS-terms a -" + nameof(ITypeDescriber) + "- is the Field stored within a -" + nameof(IP) + "- which is the Post.)\r\n" +
            "\r\n" +
            "The more classes you have implementing this interface, the more classes you have that can participiate in the " +
            "AgoRapide world with flexible concepts like -" + nameof(ARConcepts.PropertyStream) + "-, -" + nameof(ARConcepts.TaggingOfPropertyKeys) + "-, " +
            "-" + nameof(ARConcepts.PropertyAccess) + "-, -" + nameof(ARConcepts.ExposingApplicationState) + "-." +
            "\r\n" +
            "The mechanism has been designed to be as flexible as possible. Experience shows that a great variety of objects can " +
            "with great advantage be represented through this mechanism. That is, not only traditional entities like 'Customer', 'Order' and so on " +
            "can be represented but also objects like internal parts of your application (classes, enums), \r\n" +
            "the different methods in an API and even reports and aggregations.\r\n" +
            "\r\n" +
            "(Even the mechanism for describing this mechanism (-" + nameof(PK) + "-) is an implementation of it).\r\n" +
            "\r\n" +
            "HINT: Whenever you are creating a new class in your application, instead of using traditional setters and getters / traditional properties, " +
            "let class inherit -" + nameof(IP) + "- and use this mechanism instead.\r\n" +
            "\r\n" +
            "Using -" + nameof(IP) + "- from the start makes the rich functionality of AgoRapide available without much work. " +
            "(You can always build traditional setters and getters and / or traditional property storage on top of that mechanism " +
            "at a later stage if you so prefer)\r\n" +
            "\r\n" +
            "Reflecting the fact that instances of this interface often contain a collection of objects, " +
            "this interface implements the following:\r\n" +
            "1) Indexing. Enables you to query more 'direct' in C# like for instance 'DataStorage[\"Customer\"][\"42\"][\"FirstName\"]'.\r\n" +
            "2) IEnumerable<IKIP>.\r\n" +
            "3) The Keys and Values pattern from ordinary .NET Dictionary (see -" + nameof(Keys) + "- and -" + nameof(Values) + "-.\r\n" +
            "This functionality is of course not relevant for value only implementations like -" + nameof(PValue<TValue>) + "- " +
            "but technically implemented nonetheless in all cases in order for any hierarchical structure to be 'probed' from the outside in " +
            "a standardized manner.\r\n" +
            "\r\n" +
            "TODO: Implement IDictionary<TKey, TValue> also..\r\n" +
            "\r\n" +
            "See also -" + nameof(ARConcepts.PropertyAccess) + "-.",
        LongDescription =
            "TODO: Regarding default interface methods defined here in this interface\r\n" +
            "TODO: and the need for implementing an 'IP' property in implementing classes like -" + nameof(PRich.IP) + "-.\r\n" +
            "TODO: As AgoRapide matures, considering implementing copies of the default interface methods in a common super class\r\n" +
            "TODO: for PRich, PConcurrent, PExact (not necessary for PValue, PEmpty and not possible for BaseAttribute).\r\n" +
            "\r\n" +
            "Note how AgoRapide internally uses this same mechanism ('eating its own dog food'), and also how you are encouraged " +
            "to document your application through attributes like -" + nameof(ClassAttribute) + "- and -" + nameof(ClassMemberAttribute) + "- " +
            "using the same mechanism.\r\n" +
            "\r\n" +
            "Note about packing of values: -" + nameof(ARConcepts.PropertyAccess) + "- uses packed values " +
            "(everything is sent and received as -" + nameof(IP) + "-) meaning for instance that " +
            "a string-value would typically be packed inside a -" + nameof(PValue<string>) + "- object.\r\n" +
            "Connected with this, classes implementing -" + nameof(IP) + "- have a choice between\r\n" +
            "1) Unpacking on receive, packing on send (like used by -" + nameof(PExact<TPropertyKeyEnum>) + "-) or\r\n" +
            "2) Storing in packed form (like used by -" + nameof(PRich) + "-).\r\n" +
            "The former is more memory efficient (see -" + nameof(ARConcepts.MemoryConsumption) + "-), " +
            "but less able to store metadata (see -" + nameof(PP) + "-) and hierarchical data.\r\n" +
            "(see also -" + nameof(PK.PackObjectForStorageInEntityObject) + "- and -" + nameof(IP.TryGetV) + "-.)" +
            "\r\n" +
            "See also -" + nameof(ITypeDescriber) + "- which provides a standardized mechanism for describing single-property values.\r\n" +
            "\r\n"
    )]
    public interface IP : IEnumerable<IKIP>
    {

        /// <summary>
        /// </summary>
        /// <param name="entity">
        /// May be null.<br>
        /// <br>
        /// Should be given if already known (eliminates one directory lookup here)
        /// </param>
        /// <param name="entityKey"></param>
        /// <param name="ikip"></param>
        /// <param name="errorResponse"></param>
        /// <returns></returns>
        [ClassMember(Description =
            "SetPP = set property of property. Sets property of entity contained within this class.\r\n" +
            "\r\n" +
            "Example: If this is a container class for 'Order' entities, " +
            "a call could correspond to a -" + nameof(PropertyStreamLine) + "- like 'Order/1968/CustomerId = 42, " +
            "that is entityKey = '1968' and ikip = 'CustomerId = 42'.\r\n" +
            "\r\n" +
            "Should be called if -" + nameof(ARConcepts.Indexing) + "- is desired for properties being stored.\r\n" +
            "-" + nameof(PropertyStreamLine.TryStore) + "- used this method for instance.\r\n" +
            "\r\n" +
            "If implementing class does not support -" + nameof(ARConcepts.Indexing) + "-, it can implement this method as follows:\r\n" +
            "   => (entity == null && !TryGetP(entityKey, out entity, out errorResponse)) ? false : entity.TrySetP(ikip, out errorResponse);\r\n" +
            // NOTE: If you wonder why the code above can not just be a default interface method, you can read here:
            // NOTE: https://daveaglick.com/posts/default-interface-members-and-inheritance
            // NOTE: The issue arises because most probably the actual implementation would reside in a sub-class 
            // NOTE: (typically something like Customer or Order) of the first class (typically PCollection) implementing IP,
            // NOTE: meaning that this first class would have to implement the method anyway, 
            // NOTE: in order for it to be called on the sub-class on an instance of IP
            // NOTE: so there would be little point in having a default interface method here.
            "\r\n" +
            "If implementing class supports -" + nameof(ARConcepts.Indexing) + "-, it should also implement -" + nameof(ITrySetPP) + "-, " +
            "that is, clearly stating that the class supports indexing.\r\n" +
            "\r\n" +
            "See also -" + nameof(GetKeysEqualToValue) + "-."
         )]
        bool TrySetPP(IK entityKey, IP? entity, IKIP ikip, out string errorResponse);

        [ClassMember(Description =
            "Returns properties which themselves again have key and value equal to the given key and value.\r\n" +
            "\r\n" +
            "Example: If this is a container class for 'Order' entities, " +
            "a call could correspond to query 'Order/WHERE CustomerId = 42', " +
            "that is key = 'CustomerId' and value = '42'.\r\n" +
            "\r\n" +
            "Should be called if -" + nameof(ARConcepts.Indexing) + "- is desired for properties being stored.\r\n" +
            "-ForeignKey- in -" + nameof(ARComponents.ARCQuery) + "- uses this method for instance.\r\n" +
            "\r\n" +
            "If implementing class does not support -" + nameof(ARConcepts.Indexing) + "-, it can implement this method in the following O(n) manner:\r\n" +
            "  return this.Where(ikip => ikip.P.TryGetPV<string>(key, out var v) && v.Equals(value)).Select(ikip => ikip);\r\n" +
            // NOTE: If you wonder why the code above can not just be a default interface method, you can read here:
            // NOTE: https://daveaglick.com/posts/default-interface-members-and-inheritance
            // NOTE: The issue arises because most probably the actual implementation would reside in a sub-class 
            // NOTE: (typically something like Customer or Order) of the first class (typically PCollection) implementing IP,
            // NOTE: meaning that this first class would have to implement the method anyway, 
            // NOTE: in order for it to be called on the sub-class on an instance of IP
            // NOTE: so there would be little point in having a default interface method here.
            "\r\n" +
            "If implementing class supports -" + nameof(ARConcepts.Indexing) + "-, it should also implement -" + nameof(IGetKeysEqualToValue) + "-, " +
            "that is, clearly stating that the class supports indexing.\r\n" +
            "\r\n" +
            "See also -" + nameof(TrySetPP) + "-."
        )]
        IEnumerable<IKIP> GetKeysEqualToValue(IK key, string value);

        [ClassMember(Description =
            "Event handler to be called by -" + nameof(TrySetP) + "- before it changes a property value " +
            "(but AFTER it has done any validity checks, that is, after it has 'decided' to return TRUE, " +
            "in other words, event handler should be called when change is guaranteed to go through, but before it actually has been done).\r\n" +
            "\r\n" +
            "Return value TRUE is to be treated as 'Cancel', that is, the change should not go through after all.\r\n" +
            "\r\n" +
            "If implementing class does not desire to support this event it can implement this method as follows:\r\n" +
            "  => false;\r\n" +
            // NOTE: If you wonder why the code above can not just be a default interface method, you can read here:
            // NOTE: https://daveaglick.com/posts/default-interface-members-and-inheritance
            // NOTE: The issue arises because most probably the actual implementation would reside in a sub-class 
            // NOTE: (typically something like Customer or Order) of the first class (typically PRich) implementing IP,
            // NOTE: meaning that this first class would have to implement the method anyway, 
            // NOTE: in order for it to be called on the sub-class on an instance of IP
            // NOTE: so there would be little point in having a default interface method here.
            "\r\n" +
            "Some uses for this event handler could be:\r\n" +
            "1) To record first instance of a value (the initial value).\r\n" +
            "For instance if a Customer can have subscriptions, you may want to note their initial subscription.\r\n" +
            "For instance, the first time a subscription is encountered, like 'Customer/42/Subscription = Premium', the event handler can\r\n" +
            "store 'Customer/42/InitialSubscription = Premium' in addition to the original change.\r\n" +
            "\r\n" +
            "NOTE: If you want more advanced capabilities you can use -" + nameof(ARConcepts.EventSourcing) + "- " +
            "NOTE: with the help of -" + nameof(ARComponents.ARCEvent) + "-."
        )]
        public bool OnTrySetP(IKIP ikip);

        /// <summary>
        /// Gets a single property for this object.
        /// 
        /// Note that not relevant for single-value properties like <see cref="PValue{T}"/>.
        /// </summary>
        /// <param name="key">The property key like CustomerP.FirstName.A()</param>
        /// <param name="p"></param>
        /// <param name="errorResponse"></param>
        /// <returns></returns>
        [ClassMember(Description =
            "Gets a single property for this object\r\n" +
            "Note that not relevant for single-value properties like -" + nameof(PValue<TValue>) + "-."
        )]
        bool TryGetP<T>(IK key, out T p, out string errorResponse) where T : IP;

        void RemoveP(IK key)
        {
            if (!TryRemoveP(key, out var errorResponse)) throw new RemovePException(errorResponse);
        }

        bool TryRemoveP(IK key, out string errorResponse);

        /// <summary>
        /// NOTE: Se "POSITION 1" FOR INFORMATION ABOUT CONSEQUENCES OF "MOVING" DECLARATIONS OF INTERFACE METHODS.
        /// </summary>
        /// <param name="recurseDepth">
        /// The number of recursive levels to go 'down' in the hierarchical structure.
        /// Use a value of 1 if only keys at start-level are needed.
        /// </param>
        /// <param name="currentIK">
        /// Usually set to null. 
        /// If you want some overarching context added to each line in the stream, like 'Node:42' for instance then you can 
        /// pass the corresponding value here.
        /// </param>
        /// <param name="retval">
        /// Usually set to null when called from 'outside' (only set internally by implementing methods)
        /// </param>
        /// <returns></returns>
        [ClassMember(
            Description =
                "Turns object into a -" + nameof(ARConcepts.PropertyStream) + "-\r\n" +
                "\r\n" +
                "Usually to be called initially (from 'outside') with no parameters.\r\n" +
                "\r\n" +
                "Set recursiveDepth to 1 if only keys at root-level are needed.\r\n" +
                "\r\n" +
                "This is AgoRapide's main serialization mechanism.\r\n" +
                "The main deserialization mechanism is -" + nameof(PropertyStreamLine.TryStore) + "- " +
                "(together with -" + nameof(PropertyStreamLineParsed.TryParse) + "-).\r\n" +
                "In addition -" + nameof(TryParseDtr) + "- can deserialize whole records.\r\n" +
                "\r\n" +
                "NOTE: 'final' implementations, that is, classes containing an actual value has to implement this method explicitly.\r\n" +
                "NOTE: For examples of this, see -" + nameof(PValue<TValue>.ToPropertyStream) + "- and -" + nameof(PValueEmpty.ToPropertyStream) + "-.\r\n" +
                // NOTE: Beware of this:
                // NOTE: https://daveaglick.com/posts/default-interface-members-and-inheritance
                "\r\n" +
                "TODO: Result now is sorted alphabeticaly within each level. Make sorting optional, and add a parameter specifying sorting\r\n" +
                "TODO: like mechanism in LINQ (" + nameof(System.Linq.Enumerable.OrderBy) + ")."
        )]
        public IEnumerable<string> ToPropertyStream(IEnumerable<IK>? currentIK = null, List<string>? retval = null, int recurseDepth = int.MaxValue)
        {
            try
            {
                REx.Inc();
                if (currentIK == null) currentIK = new List<IK>(); // We are the first one called in the chain, and also the one whose "return retval" actually will be consumed.
                if (retval == null) retval = new List<string>();   // We are the first one called in the chain, and also the one whose "return retval" actually will be consumed.
                if (recurseDepth == 0)
                {
                    // Requested recurse depth has now been reached.
                    return retval;
                }

                // Sort in order for keys as this level to appear before keys at next recursive level
                var isValue = new Func<IP, bool>(ip =>
                {
                    var t = ip.GetType();
                    return t.Equals(typeof(PValueEmpty)) || (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(PValue<>));
                });
                this.Where(e => isValue(e.P)).OrderBy(e => e.Key.ToString()).ForEach(e => e.P.ToPropertyStream(currentIK.Append(e.Key), retval, recurseDepth - 1));
                this.Where(e => !isValue(e.P)).OrderBy(e => e.Key.ToString()).ForEach(e => e.P.ToPropertyStream(currentIK.Append(e.Key), retval, recurseDepth - 1));
                return retval; // Return value will only be consumed if we were the first one to be called in the chain (see above)
            }
            finally
            {
                REx.Dec();
            }
        }

        // TODO: Probably unnecessary to add? Gives little of value?
        //public string ToPropertyStream() => string.Join("\r\n", ToPropertyStream());


        [ClassMember(Description =
        "Prepares the data into a format acceptable for System.Text.Json.JsonSerializer.Serialize.\r\n" +
        "(used by -" + nameof(ARComponents.ARCAPI) + "- (which again uses System.Text.Json version 4.7.2 as of Aug 2020)).\r\n" +
        "\r\n" +
        "This default implementation will for instance replace " + nameof(System.Type) + " with " + nameof(System.String) + ".\r\n" +
        "\r\n" +
        "Note that there is no corresponding JSON Deserialize-method " +
        "(as JSON is not a suitable serialization mechanism for -" + nameof(ARConcepts.PropertyStream) + "-).\r\n" +
        "See -" + nameof(ToPropertyStream) + "- for more information about serialization / deserialization.\r\n" +
        "\r\n" +
        "NOTE: Strictly speaking this functionality really belongs in -" + nameof(ARComponents.ARCQuery) + "- / -" + nameof(ARComponents.ARCAPI) + "-.\r\n"
    )]
        public Dictionary<string, object?> ToJSONSerializeable(int depthRemaining)
        {
            try
            {
                REx.Inc();
                var retval = new Dictionary<string, object?>();
                this.ForEach(ikip =>
                {
                    // As of Aug 2020 we only go one level down in the structure.
                    retval.Add(ikip.Key.ToString(),
                        IP.GetValueForJSONSerializer(ikip.P) ?? // If a value was found, use that one
                        (depthRemaining <= 1 ? // If no value found, continue 'deeper down' in hierarchy
                            null : // We have reached maximum depth
                            ikip.P.ToJSONSerializeable(depthRemaining - 1) // 
                    /// NOTE: If both a value and sub-properties exists, than only the value will be returned.
                    /// NOTE: If the value has metadata, like <see cref="PP.Created"/>, it will be returned 
                    /// NOTE: as <see cref="PP.Value"/> in list of sub-properties.
                    /// TODO: This is difficult for the JSON consumer to understand now / distinguish
                    )
                    );
                });
                return retval;
            }
            finally
            {
                REx.Dec();
            }
        }

        [ClassMember(Description =
            "Practical approach to serializing of -" + nameof(IP) + "- based values to JSON.\r\n" +
            "\r\n" +
            "Uses -" + nameof(PValue<TValue>.ConvertObjectToString) + "-.\r\n" +
            "Understands \"typeof(List<>)\" and returns as List<string>.\r\n" +
            "\r\n" +
            //"Makes the following modifications to the data:\r\n" +
            //"\r\n" +
            //"1) Replaces " + nameof(System.Type) + " with " + nameof(System.String) + "\r\n" +
            //"(in order to avoid exception being thrown by System.Text.Json.JsonSerializer.Serialize " +
            //"(issue in 4.7.2 and probably later versions also))\r\n" +
            //"\r\n" +
            //"2) Replaces enums with their string representation\r\n" +
            //"(in order to avoid int-representation being used by System.Text.Json.JsonSerializer.Serialize)." +
            "\r\n" +
            "NOTE: Strictly speaking this functionality really belongs in -" + nameof(ARComponents.ARCQuery) + "- / -" + nameof(ARComponents.ARCAPI) + "-.\r\n"
        )]
        public static object? GetValueForJSONSerializer(IP ip)
        {
            if (!ip.TryGetV<object>(out var o)) return null; // NOTE: / TODO: PValueEmpty comes out as "blank" by this method.

            // TODO: Delete commented out code (old approach from before 11 Jan 2022)
            //// NOTE: System.Text.Json.JsonSerializer.Serialize
            //// NOTE: is not able to serialize Type "out-of-the-box".
            //// NOTE: Some pointers about this:
            //// NOTE: https://github.com/dotnet/runtime/issues/31567
            //// NOTE: https://github.com/dotnet/runtime/pull/34249
            //if (s is Type) return s.ToString();
            //if (s is List<Type> l) return l.Select(t => t.ToString());
            //if (s.GetType().IsEnum) return s.ToString(); // Use string value instead of integer value.
            //// TODO: List of Enum is not taken into consideration for the time being

            // New approach from 11 Jan 2022
            // This basically works, but has the limitation of presenting everything as string in Json.
            // TODO: This just reuses PValue<TValue>.ConvertObjectToString
            // TODO: Create alternative to PValue<TValue>.ConvertObjectToString which can convert
            // TODO: known primitive types in a better manner

            var t = o.GetType();
            if (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(List<>)))
            {
                return ((System.Collections.IList)o).Cast<object>().Select(o => PValue<TValue>.ConvertObjectToString(o)).ToList();
            }
            return PValue<TValue>.ConvertObjectToString(o);
        }

        [ClassMember(
            Description =
                "Set property regardless of whether exists or not.\r\n" +
                "\r\n" +
                "Note that not relevant for single-value properties like -" + nameof(PValue<TValue>) + "-.\r\n" +
                "\r\n" +
                "Some reasons for FALSE being returned may be like the following:\r\n" +
                "\r\n" +
                "1) Parameter -" + nameof(IKIP) + "- found inconsistent at call to -" + nameof(IKIP.TryAssertTypeIntegrity) + "-.\r\n" +
                "(note that all implementing classes of -" + nameof(IP) + "- should call -" + nameof(IKIP.TryAssertTypeIntegrity) + "- " +
                "in their implementation of this method (" + nameof(TrySetP) + ")).\r\n" +
                "\r\n" +
                "2) This instance of -" + nameof(IP) + "- does not accept the given key. This again may have different reasons:\r\n" +
                "\r\n" +
                "2a) This instance is a 'value only' instance like -" + nameof(PValue<TValue>) + "-, simply not accepting properties at all " +
                "(see also -" + nameof(PP.Value) + "-).\r\n" +
                "\r\n" +
                "2b) This instance is a -" + nameof(PExact<TPropertyKeyEnum>) + "- which only accepts keys of a specific -" + nameof(AREnumType.PropertyKeyEnum) + " " +
                "(like the Banana-class in -" + nameof(ARComponents.ARCDoc) + "- only accepting keys from BananaP).\r\n" +
                "\r\n" +
                "2c) TODO: Not implemented. The key is not in a pre-defined collection of keys that this instance accepts " +
                "(Note: By default, classes like -" + nameof(PRich) + "- accepts any key)."
        )]
        bool TrySetP(IKIP ikip, out string errorResponse);

        [ClassMember(
            Description =
                "The actual 'unpacking' of the object.\r\n" +
                "\r\n" +
                "(corresponds to -" + nameof(PK.PackObjectForStorageInEntityObject) + "-.\r\n" +
                "\r\n" +
                "Relevant for -" + nameof(PValue<TValue>) + "- and -" + nameof(PValueEmpty) + "-.\r\n"
        )]
        bool TryGetV<T>(out T retval, out string errorResponse) where T : notnull;

        [ClassMember(
            Description =
                "Creates a deep copy of this object instance, that is, a copy with all new object instances (except for immutable objects).\r\n" +
                "Implementing classes are supposed to use this method recursively for a truly deep copy.\r\n" +
                "\r\n" +
                "Note that there may be some limitations in the implementing class for how truly deep the copy is. " +
                "For instance if the actual value (TValue) of -" + nameof(PValue<TValue>) + "- is not immutable, " +
                "then the copy returned will probably not be a truly deep copy"
        )]
        public IP DeepCopy();

        // Removed 28 Jan 2022. Use !IsAny instead (and implementation was also buggy)
        //[ClassMember(Description =
        //    "TODO: Could probably be replaced with !-" + nameof(System.Linq.Enumerable.Any) + "-.\r\n" +
        //    "\r\n" +
        //    "Hint for how to avoid calling enumerator.Count() when all you need is to decide if collection is empty or not.\r\n"
        //)]
        //bool EnumeratorReturnsEmptyCollection => this.FirstOrDefault() == default; // BUG: First element may be a 'default' element.


        // Approach before 8 Mar 2022, using generic type parameter for key (instead of for desired value)
        //bool TryGetP<TKey>(TKey keyAsEnum, out IP p) where TKey : Enum => TryGetP(PK.FromEnum(keyAsEnum), out p, out _);
        //bool TryGetP<TKey>(TKey keyAsEnum, out IP p, out string errorResponse) where TKey : Enum => TryGetP(PK.FromEnum(keyAsEnum), out p, out errorResponse);
        //bool TryGetP(string key, out IP p) => TryGetP(IKString.FromString(key), out p, out _);
        //bool TryGetP(string key, out IP p, out string errorResponse) => TryGetP(IKString.FromString(key), out p, out errorResponse);
        //bool TryGetP<TKey>(IK key, out IP p) where TKey : Enum => TryGetP(key, out p, out _);
        //bool TryGetP<TKey>(IK key, out IP p, out string errorResponse) where TKey : Enum => TryGetP(key, out p, out errorResponse);
        //bool TryGetP<T>(IK key, out T p, out string errorResponse) where T : IP
        //{
        //    if (!TryGetP(key, out var ip, out errorResponse))
        //    {
        //        p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
        //        return false;
        //    }
        //    if (!(ip is T temp))
        //    {
        //        p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
        //        errorResponse = "Key " + key + ": Found type " + ip.GetType().ToStringShort() + ", not " + typeof(T).ToStringShort();
        //        return false;
        //    }
        //    p = temp;
        //    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
        //    return true;
        //}

        // Approach after 8 Mar 2022, using generic type parameter for desired value (and object for key)
        bool TryGetP<T>(object keyAsEnum, out T p) where T : IP => TryGetP<T>(PK.FromEnum(keyAsEnum), out p, out _);
        bool TryGetP<T>(object keyAsEnum, out T p, out string errorResponse) where T : IP => TryGetP<T>(PK.FromEnum(keyAsEnum), out p, out errorResponse);
        bool TryGetP<T>(string key, out T p) where T : IP => TryGetP<T>(IKString.FromString(key), out p, out _);
        bool TryGetP<T>(string key, out T p, out string errorResponse) where T : IP => TryGetP<T>(IKString.FromString(key), out p, out errorResponse);
        bool TryGetP<T>(IK key, out T p) where T : IP => TryGetP<T>(key, out p, out _);
        //bool TryGetP<T>(IK key, out T p, out string errorResponse) where T : IP
        //{
        //    if (!TryGetP(key, out var ip, out errorResponse))
        //    {
        //        p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
        //        return false;
        //    }
        //    if (!(ip is T temp))
        //    {
        //        p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
        //        errorResponse = "Key " + key + ": Found type " + ip.GetType().ToStringShort() + ", not " + typeof(T).ToStringShort();
        //        return false;
        //    }
        //    p = temp;
        //    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
        //    return true;
        //}


        void SetP<TKey>(TKey keyAsEnum, IP p) where TKey : Enum
        {
            if (!TrySetP(keyAsEnum, p, out var errorResponse))
            {
                throw new SetPException(
                    "Setting of property with key " + keyAsEnum.GetType().ToStringShort() + "." + keyAsEnum + " failed.\r\n" +
                    nameof(errorResponse) + ": " + errorResponse
                );
            }
        }
        bool TrySetP<TKey>(TKey keyAsEnum, IP p, out string errorResponse) where TKey : Enum =>
            TrySetP(new IKIP(PK.FromEnum(keyAsEnum), p), out errorResponse);

        void SetP(string keyAsString, IP p)
        {
            if (!TrySetP(keyAsString, p, out var errorResponse))
            {
                throw new SetPException(
                    "Setting of property with key " + keyAsString + " failed.\r\n" +
                    nameof(errorResponse) + ": " + errorResponse + "\r\n"
                );
            }
        }

        void SetP(IK key, IP p)
        {
            if (!TrySetP(key, p, out var errorResponse))
            {
                throw new SetPException(
                    "Setting of property with key " + key.ToString() + " failed.\r\n" +
                    nameof(errorResponse) + ": " + errorResponse + "\r\n"
                );
            }
        }
        bool TrySetP(IK key, IP p, out string errorResponse) => TrySetP(new IKIP(key, p), out errorResponse);
        bool TrySetP(string keyAsString, IP p, out string errorResponse) => TrySetP(new IKIP(keyAsString, p), out errorResponse);

        void SetP(IKIP ikip)
        {
            if (!TrySetP(ikip, out var errorResponse))
            {
                throw new SetPException(
                    "Setting of property with key " + ikip.Key.ToString() + " failed.\r\n" +
                    nameof(errorResponse) + ": " + errorResponse + "\r\n"
                );
            }
        }

        void SetPVM<T>(T value) where T : notnull
        {
            if (!TrySetPVM(value, out var errorResponse))
            {
                throw new SetPException(
                    "Setting of property with value type " + typeof(T) + " failed.\r\n" +
                    nameof(errorResponse) + ": " + errorResponse + "\r\n"
                );
            }
        }
        bool TrySetPVM<T>(T value, out string errorResponse) where T : notnull => TrySetPV(
            PK.GetTypeMapping(GetType(), typeof(T)),  // TODO: Introduce PK.TryGetTypeMapping
            value, out errorResponse);

        void SetPV<T>(IK key, T value) where T : notnull
        {
            if (!TrySetPV(key, value, out var errorResponse))
            {
                throw new SetPException(
                    "Setting of property with key " + key + " failed.\r\n" +
                    nameof(errorResponse) + ": " + errorResponse + "\r\n"
                );
            }
        }

        bool TrySetPV<T>(IK key, T value, out string errorResponse) where T : notnull => TrySetP(new IKIP(
            key,
            // Note: The simplest alternative would be to just do
            //   new PValue<T>(value)));
            // here, but there is no rule in AgoRapide saying that only PValue may be used for packing.
            // Instead we do the more convoluted approach (which most probably ends with exact the same result)
            (key as PK)?.PackObjectForStorageInEntityObject(value) ??
            // If however we do not have a PK, just use PValue
            new PValue<T>(value)
        // That is, do not throw this exception:
        // throw new InvalidObjectTypeException(key, typeof(PK), "Attempt to set value '" + value + "'")
        ), out errorResponse);

        void SetPV(IK key, IP p)
        {
            if (!TrySetPV(key, p, out var errorResponse))
            {
                throw new SetPException(
                    "Setting of property with key " + key + " failed.\r\n" +
                    nameof(errorResponse) + ": " + errorResponse + "\r\n"
                );
            }
        }
        bool TrySetPV(IK key, IP p, out string errorResponse) => TrySetP(new IKIP(key, p), out errorResponse);

        void SetPV(string keyAsString, object value)
        {
            if (!TrySetPV(IKString.FromString(keyAsString), value, out var errorResponse))
            {
                throw new SetPException(
                    "Setting of property with key " + keyAsString + " failed.\r\n" +
                    nameof(errorResponse) + ": " + errorResponse + "\r\n"
                );
            }
        }
        void SetPV<TKey>(TKey keyAsEnum, object value) where TKey : Enum
        {
            if (!TrySetPV(keyAsEnum, value, out var errorResponse))
            {
                throw new SetPException(
                    "Setting of property with key " + typeof(TKey).ToStringShort() + "." + keyAsEnum + " failed.\r\n" +
                    nameof(errorResponse) + ": " + errorResponse + "\r\n"
                );
            }
        }
        bool TrySetPV<TKey>(TKey keyAsEnum, object value, out string errorResponse) where TKey : Enum =>
            TrySetP(
                PK.FromEnum(keyAsEnum).Use(pk =>  // TODO: Introduce PK.TryGetFromEnum.,
                    new IKIP(
                        pk,
                        // Note: The simplest alternative would be to just do
                        //   new PValue<T>(value)));
                        // here, but there is no rule in AgoRapide saying that only PValue may be used for packing.
                        // Instead we do the more convoluted approach (which most probably ends with exact the same result)
                        pk.PackObjectForStorageInEntityObject(value)
                    )
                ),
                out errorResponse
            );

        /// <summary>
        /// WARNING: AddP implies an assertion againts an already existing property (a TryGet will be performed)
        /// Use TrySetP for highest possible performance.
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="keyAsEnum"></param>
        /// <param name="value"></param>
        [ClassMember(Description =
            "NOTE: The different AddPV / AddP overloads should only be used if you want an assertion against the\r\n" +
            "NOTE: property already existing. This is because of performance reason (they will attempt a TryGetP (read) first).\r\n" +
            "NOTE: Use TrySetP for best performance.\r\n"
        )]
        void AddPV<TKey, T>(TKey keyAsEnum, T value) where TKey : Enum where T : notnull => AddPV(PK.FromEnum(keyAsEnum), value);
        bool TryAddPV<TKey, T>(TKey keyAsEnum, T value) where TKey : Enum where T : notnull => TryAddPV(PK.FromEnum(keyAsEnum), value, out _);
        bool TryAddPV<TKey, T>(TKey keyAsEnum, T value, out string errorResponse) where TKey : Enum where T : notnull => TryAddPV(PK.FromEnum(keyAsEnum), value, out errorResponse);

        /// <summary>
        /// WARNING: AddP implies an assertion againts an already existing property (a TryGet will be performed)
        /// Use TrySetP for highest possible performance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        [ClassMember(Description =
            "Adds a property value (as a -" + nameof(PValue<TValue>) + "-) for this object" +
            "Throws -" + nameof(KeyAlreadyExistsException) + "- when relevant.\r\n"
        )]
        void AddPV<T>(IK key, T value) where T : notnull => AddP(key, new PValue<T>(value));

        /// <summary>
        /// WARNING: AddP implies an assertion againts an already existing property (a TryGet will be performed)
        /// Use TrySetP for highest possible performance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="keyAsString"></param>
        /// <param name="value"></param>
        void AddPV<T>(string keyAsString, T value) where T : notnull => AddP(IKString.FromString(keyAsString), new PValue<T>(value));
        bool TryAddPV<T>(IK key, T value, out string errorResponse) where T : notnull => TryAddP(key, new PValue<T>(value), out errorResponse);

        /// <summary>
        /// WARNING: AddP implies an assertion againts an already existing property (a TryGet will be performed)
        /// Use TrySetP for highest possible performance.
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="keyAsEnum"></param>
        /// <param name="p"></param>
        void AddP<TKey>(TKey keyAsEnum, IP p) where TKey : Enum => AddP(PK.FromEnum(keyAsEnum), p);
        void TryAddP<TKey>(TKey keyAsEnum, IP p, out string errorResponse) where TKey : Enum => TryAddP(PK.FromEnum(keyAsEnum), p, out errorResponse);

        /// <summary>
        /// WARNING: AddP implies an assertion againts an already existing property (a TryGet will be performed)
        /// Use TrySetP for highest possible performance.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="p"></param>
        void AddP(string key, IP p) => AddP(new IKIP(IKString.FromString(key), p));

        bool TryAddP(string key, IP p, out string errorResponse) => TryAddP(new IKIP(IKString.FromString(key), p), out errorResponse);

        /// <summary>
        /// WARNING: AddP implies an assertion againts an already existing property (a TryGet will be performed)
        /// Use TrySetP for highest possible performance.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="p"></param>
        void AddP(IK key, IP p) => AddP(new IKIP(key, p));

        bool TryAddP(IK key, IP p, out string errorResponse) => TryAddP(new IKIP(key, p), out errorResponse);

        /// <summary>
        /// WARNING: AddP implies an assertion againts an already existing property (a TryGet will be performed)
        /// Use TrySetP for highest possible performance.
        /// </summary>
        [ClassMember(Description =
            "Adds a property for this object.\r\n" +
            "\r\n" +
            "Throws -" + nameof(KeyAlreadyExistsException) + "- when relevant.\r\n"
        )]
        void AddP(IKIP ikip)
        {
            if (TryGetP<IP>(ikip.Key, out _, out _))
            {
                throw new KeyAlreadyExistsException(ikip, ToString() + "\r\n");
            }
            if (!TrySetP(ikip, out var errorResponse))
            {
                throw new SetPException(
                    "Setting of property with key " + ikip.Key + " failed.\r\n" +
                    nameof(errorResponse) + ": " + errorResponse + "\r\n"
                );
            }
        }

        bool TryAddP(IKIP ikip, out string errorResponse)
        {
            if (TryGetP<IP>(ikip.Key, out _, out _))
            {
                errorResponse = "Key (" + ikip.Key + ") already exists";
                return false;
            }
            return TrySetP(ikip, out errorResponse);
        }

        [ClassMember(Description =
            "Duplicates functionality of SetP.\r\n" +
            "(offered in order to look similar to for instance ConcurrentDictionary.)")]
        void AddOrUpdateP<TKey>(TKey keyAsEnum, IP p) where TKey : Enum => SetP(keyAsEnum, p);
        // Note: Do not add [ClassMember(Description = "... for all overloads unless it actually differs. It will only lead to confusing compiled documentation.
        void AddOrUpdateP(IK key, IP p) => SetP(key, p);

        [ClassMember(Description = "Gets value. If not set then value will be added")]
        T GetOrAddPV<TKey, T>(TKey keyAsEnum, T value) where TKey : Enum where T : notnull => GetOrAddPV(PK.FromEnum(keyAsEnum), () => value);
        // Note: Do not add [ClassMember(Description = "... for all overloads unless it actually differs. It will only lead to confusing compiled documentation.
        T GetOrAddPV<TKey, T>(TKey keyAsEnum, Func<T> valueFactory) where TKey : Enum where T : notnull => GetOrAddPV(PK.FromEnum(keyAsEnum), valueFactory);
        // Note: Do not add [ClassMember(Description = "... for all overloads unless it actually differs. It will only lead to confusing compiled documentation.
        T GetOrAddPV<T>(IK key, T value) where T : notnull => GetOrAddPV(key, () => value);
        // Note: Do not add [ClassMember(Description = "... for all overloads unless it actually differs. It will only lead to confusing compiled documentation.
        T GetOrAddPV<T>(IK key, Func<T> valueFactory) where T : notnull
        {
            if (TryGetPV(key, out T retval)) return retval;
            retval = valueFactory();
            AddPV(key, retval);
            return retval;
        }

        // Approach before 8 Mar 2022, using generic type parameter for key (instead of for desired value)
        ///// <summary>
        ///// Gets a single property for this object.
        ///// 
        ///// Note that not relevant for single-value properties like <see cref="PValue{T}"/>.
        ///// </summary>
        ///// <param name="k">The property key like CustomerP.FirstName.A()</param>
        ///// <returns></returns>
        //[ClassMember(Description =
        //    "Gets a single property for this object.\r\n" +
        //    "Note that not relevant for single-value properties like -" + nameof(PValue<TValue>) + "-.\r\n" +
        //    "Throws exception if property not found.\r\n" +
        //    "TODO: Consider looking for a -" + nameof(PKTypeAttributeP.DefaultValue) + "- before throwing an exceptions.\r\n" +
        //    "TODO: (as of Apr 2020 system depends on -" + nameof(AssertIntegrity) + "- having being called in order to set default values)."
        //)]
        //IP GetP(IK k) => TryGetP(k, out var p, out var errorResponse) ? p : throw new KeyNotFoundException("Key " + k.ToString() + " not found in " + ToString() + ". " + nameof(errorResponse) + ": " + errorResponse + "\r\n");
        //IP GetP<TKey>(TKey keyAsEnum) where TKey : Enum => GetP(PK.FromEnum(keyAsEnum));
        //IP GetP(string keyAsString) => GetP(IKString.FromString(keyAsString));
        //IP GetP(string keyAsString, IP defaultValue) => TryGetP(IKString.FromString(keyAsString), out var retval) ? retval : defaultValue;
        //[ClassMember(Description = "Note that not relevant for single-value properties like -" + nameof(PValue<TValue>) + "-.")]
        //bool TryGetP(IK key, out IP p) => TryGetP(key, out p, out _);

        // Approach after 8 Mar 2022, using generic type parameter for desired value (and object for key)
        T GetP<T>(object keyAsEnum) where T : IP => GetP<T>(PK.FromEnum(keyAsEnum));
        T GetP<T>(string keyAsString) where T : IP => GetP<T>(IKString.FromString(keyAsString));
        T GetP<T>(IK k) where T : IP => TryGetP<T>(k, out var p, out var errorResponse) ? p : throw new KeyNotFoundException("Key " + k.ToString() + " not found in " + ToString() + ". " + nameof(errorResponse) + ": " + errorResponse + "\r\n");
        T GetP<T>(IK k, T defaultValue) where T : IP => TryGetP<T>(k, out var retval) ? retval : defaultValue;

        /// <summary>
        /// NOTE: Do not specify generic type parameter when attempting to call this method.
        /// NOTE: (Because you would then have to specify both of them, TKey and TValue)
        /// NOTE: Remember that TValue is inferred from parameter defaultValue
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="keyAsEnum"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        [ClassMember(Description =
            "Gets a single property's value for this object. Returns default value if not successful.\r\n" +
            "Note that default value returned is the actual parameter to this method, not any -" + nameof(PKTypeAttributeP.DefaultValue) + "-.\r\n" +
           "\r\n" +
            "Note that not relevant for single-value properties like -" + nameof(PValue<TValue>) + "-."
        )]
        TValue GetPV<TKey, TValue>(TKey keyAsEnum, TValue defaultValue) where TKey : struct, Enum where TValue : notnull =>
            TryGetPV<TValue>(PK.FromEnum(keyAsEnum), out var retval, out _) ? retval : defaultValue;
        /// <summary>
        /// Gets a single property's value for this object. 
        /// 
        /// Note that not relevant for single-value properties like <see cref="PValue{T}"/>.
        /// 
        /// NOTE: It might be tempting to change parameter 'object keyAsEnum' into 
        /// NOTE: {TKey} generic parameter, 'TKey keyAsEnum' and 'where TKey : Enum'
        /// NOTE: but it will complicate usage because then both type parameters would 
        /// NOTE: have to be specified explicit, instead of the one being explicit 
        /// NOTE: specified now.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="keyAsEnum">The property key like CustomerP.FirstName</param>
        /// <returns></returns>
        T GetPV<T>(object keyAsEnum) where T : notnull => TryGetPV<T>(keyAsEnum, out var retval, out var errorResponse) ? retval :
            throw new PropertyNotFoundException<T>(PK.FromEnum(keyAsEnum), ToString() + ", " + nameof(errorResponse) + ": " + errorResponse + "\r\n");
        T GetPV<T>(string keyAsString) where T : notnull => TryGetPV<T>(IKString.FromString(keyAsString), out var retval, out var errorResponse) ? retval :
            throw new PropertyNotFoundException<T>(IKString.FromString(keyAsString), ToString() + ", " + nameof(errorResponse) + ": " + errorResponse + "\r\n");

        //[ClassMember(Description = "HACK: The underscore in GetPV_ is there in order to avoid ambiguity with GetPV<T>(object keyAsEnym)")]
        //T GetPV_<T>(IK key) where T : notnull => TryGetPV<T>(key, out var retval, out var errorResponse) ? retval :
        //    throw new PropertyNotFoundException<T>(key, ToString() + ", " + nameof(errorResponse) + ": " + errorResponse + "\r\n");
        T GetPV<T>(object keyAsEnum, T defaultValue) where T : notnull =>
            TryGetPV<T>(PK.FromEnum(keyAsEnum), out var retval, out _) ? retval : defaultValue;
        T GetPV<T>(string keyAsString, T defaultValue) where T : notnull =>
            TryGetPV<T>(IKString.FromString(keyAsString), out var retval, out _) ? retval : defaultValue;
        //[ClassMember(Description = "HACK: The underscore in GetPV_ is there in order to avoid ambiguity with GetPV<T>(object keyAsEnym)")]
        //T GetPV_<T>(IK key, T defaultValue) where T : notnull =>
        //    TryGetPV<T>(key, out var retval, out _) ? retval : defaultValue;

        /// <summary>
        /// NOTE: It might be tempting to change parameter 'object keyAsEnum' into 
        /// NOTE: {TKey} generic parameter, 'TKey keyAsEnum' and 'where TKey : Enum'
        /// NOTE: but it will complicate usage because then both type parameters would 
        /// NOTE: have to be specified explicit, instead of the one being explicit 
        /// NOTE: specified now.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="keyAsEnum"></param>
        /// <param name="retval"></param>
        /// <returns></returns>
        bool TryGetPV<T>(object keyAsEnum, out T retval) where T : notnull => TryGetPV<T>(PK.FromEnum(keyAsEnum), out retval, out _);
        bool TryGetPV<T>(string keyAsString, out T retval) where T : notnull => TryGetPV<T>(IKString.FromString(keyAsString), out retval, out _);
        /// <summary>
        /// Gets a single property's value for the given property key
        /// 
        /// Note that not relevant for single-value properties like <see cref="PValue{T}"/>.
        /// 
        /// NOTE: It might be tempting to change parameter 'object keyAsEnum' into 
        /// NOTE: {TKey} generic parameter, 'TKey keyAsEnum' and 'where TKey : Enum'
        /// NOTE: but it will complicate usage because then both type parameters would 
        /// NOTE: have to be specified explicit, instead of the one being explicit 
        /// NOTE: specified now.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="keyAsEnum">The property key like CustomerP.FirstName</param>
        /// <param name="retval"></param>
        /// <param name="errorResponse"></param>
        /// <returns></returns>
        [ClassMember(Description =
            "Gets a single property's value for the given property key.\r\n" +
            "\r\n" +
            "TODO: BUG: As of Feb 2022: Compiler will choose this overload also when key is a string,\r\n" +
            "TODO: BUG: leading to assertion in PK.FromEnum failing. To avoid having this overload chosen convert key to IKString instead.\r\n" +
            "\r\n" +
            "Note that not relevant for single-value properties like -" + nameof(PValue<TValue>) + "-."
        )]
        bool TryGetPV<T>(object keyAsEnum, out T retval, out string errorResponse) where T : notnull => TryGetPV(PK.FromEnum(keyAsEnum), out retval, out errorResponse);

        T GetPV<T>(IK k, T defaultValue) where T : notnull => TryGetPV<T>(k, out var retval, out var _) ? retval : defaultValue;
        /// <summary>
        /// Gets a single property's value for the given property key
        /// 
        /// Do not confuse <see cref="GetV{T}"/> / <see cref="TryGetV{T}"/> with <see cref="GetPV{T}"/> / <see cref="TryGetPV{T}"/>. " +
        /// The former ask for the single value of this specific property 
        /// (which would then typically be an <see cref="PValue{T}"/>) " +
        /// while the latter asks for one of many possible properties within this specific property 
        /// (which could then for instance be <see cref="PRich"/> or similar).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="k"></param>
        /// <returns></returns>
        T GetPV<T>(IK k) where T : notnull => TryGetPV<T>(k, out var retval, out var errorResponse) ? retval : throw new PropertyNotFoundException<T>(k, ToString() + ", " + nameof(errorResponse) + ": " + errorResponse + "\r\n");
        bool TryGetPV<T>(IK key, out T retval) where T : notnull => TryGetPV(key, out retval, out _);
        bool TryGetPV<T>(IK key, out T retval, out string errorResponse) where T : notnull
        {
            if (!TryGetP<IP>(key, out var p, out var errorResponse2))
            {
                retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "-" + GetType().ToStringVeryShort() + "-: TryGetP failed with " + nameof(errorResponse) + ": " + errorResponse2;
                return false;
            }
            if (!p.TryGetV<T>(out retval, out var errorResponse3))
            {
                retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "-" + GetType().ToStringVeryShort() + "-: TryGetV on property failed with " + nameof(errorResponse) + ": " + errorResponse3;
                if (this is T)
                {
                    // TODO: Consider just returning 'this' instead? 
                    errorResponse +=
                        "\r\nNOTE: 'this' object (-" + GetType().ToStringShort() + "-) is of type requested (-" + typeof(T).ToStringShort() + "-).\r\n" +
                        "Did you mistakenly call TryGetPV instead of TryGetP? ";
                }
                return false;
            }
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }
        // ================
        // POSITION 1
        // ================

        // Ca April 2020: Bug discovered in ".NET". If you move AllP into POSITION 2 (MARKED BELOW) then you get
        // System.TypeLoadException at startup. 
        // Error message will be something like:
        // System.TypeLoadException
        //   HResult=0x80131522
        //   Message=Method 'get_AllP' in type 'ARCCore.EnumMemberAttribute' from assembly 'ARCCore, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null' does not have an implementation.
        //   Source=<Cannot evaluate the exception source>
        //   StackTrace:
        // <Cannot evaluate the exception stack trace>
        // 
        // You can also reposition TryGetV to POSITION 2 and see what happens.
        // 
        // You can also try to vary position of the two TryGetV-methods (one default implementation, the other an interface method)
        // to see what effects it gives. You can provocate either get_AllP missing, or TryGetV missing.
        //
        // February 2021: Problem still exists (.NET 5.0, Visual Studio 16.8.5 with corresponding tools)

        /// <summary>
        /// The actual value for this specific property. Only relevant to call when this objects is like a <see cref="PValue{T}"/>
        /// 
        /// Do not confuse <see cref="GetV{T}"/> / <see cref="TryGetV{T}"/> with <see cref="GetPV{T}"/> / <see cref="TryGetPV{T}"/>. " +
        /// The former ask for the single value of this specific property 
        /// (which would then typically be an <see cref="PValue{T}"/>) " +
        /// while the latter asks for one of many possible properties within this specific property 
        /// (which could then for instance be <see cref="PRich"/> or similar).
        /// </summary>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T GetV<T>() where T : notnull => TryGetV(out T retval, out var errorResponse) ? retval : throw new PropertyValueNotFoundException<T>(ToString() + ", " + nameof(errorResponse) + ": " + errorResponse + "\r\n");
        T GetV<T>(T defaultValue) where T : notnull => TryGetV(out T retval, out _) ? retval : defaultValue;
        [ClassMember(
            Description = "The actual value of the property. Mostly relevant for single-value properties like -" + nameof(PValue<TValue>) + "-.",
            LongDescription =
                "Note how the implementing method should be flexible when type-parameter T does not match the actual type stored.\r\n" +
                "\r\n" +
                "Asking for a string value should always be possible for instance.\r\n" +
                "\r\n" +
                "If it has to give up a good explanation should be given in the errorResponse out-parameter"
        )]
        bool TryGetV<T>(out T retval) where T : notnull => TryGetV<T>(out retval, out _);

        // ================
        // POSITION 2
        // ================

        public IP GetPM<T>() where T : notnull => GetP<IP>(PK.GetTypeMapping(GetType(), typeof(T)));
        public T GetPVM<T>(T defaultValue) where T : notnull => GetPV(PK.GetTypeMapping(GetType(), typeof(T)), defaultValue);
        public T GetPVM<T>() where T : notnull => GetPV<T>(PK.GetTypeMapping(GetType(), typeof(T)));
        public bool TryGetPM<T>(out IP retval) where T : notnull => TryGetP(PK.GetTypeMapping(GetType(), typeof(T)), out retval);
        public bool TryGetPVM<T>(out T retval) where T : notnull => TryGetPV(PK.GetTypeMapping(GetType(), typeof(T)), out retval);
        public void AddPM<T>(IP p) where T : notnull => AddP(PK.GetTypeMapping(GetType(), typeof(T)), p);
        public void AddPVM<T>(T value) where T : notnull => AddPV(PK.GetTypeMapping(GetType(), typeof(T)), value);

        [ClassMember(Description = "Increments given property key by 1. Sets to 1 if not found. Uses long, not integer.")]
        public void Inc<TKey>(TKey keyAsEnum) where TKey : Enum => PK.FromEnum(keyAsEnum).Use(k => SetPV(k, GetPV(k, 0L) + 1));
        public void Inc(string keyAsString) => SetPV(keyAsString, GetPV(keyAsString, 0L) + 1);
        [ClassMember(Description = "Decrements given property key by 1. Sets to -1 if not found. Uses long, not integer.")]
        public void Dec<TKey>(TKey keyAsEnum) where TKey : Enum => PK.FromEnum(keyAsEnum).Use(k => SetPV(k, GetPV(k, 0L) - 1));
        public void Dec(string keyAsString) => SetPV(keyAsString, GetPV(keyAsString, 0L) - 1);

        [ClassMember(
            Description =
                "Note that by default all properties are 'valid' but because of -" + nameof(ARConcepts.NoRealDeletion) + "-, " +
                "we must have a mechanism for invalidating properties. " +
                "This property returns FALSE if property has been invalidated and timestamp for invalidation is not '0001-01-01'.",
            LongDescription =
                "See also -" + nameof(PP.Invalid) + "-"
        )]
        public bool IsValid => !TryGetP<IP>(PP.Invalid, out var p) || p.GetV<DateTime>() == default;

        //// Indexer declaration:
        //IP this[string index] {
        //    get => throw new InvalidOperationException("Class " + GetType() + " does not implement indexing");
        //    set => throw new InvalidOperationException("Class " + GetType() + " does not implement indexing");
        //}

        [ClassMember(
            Description =
                "Offers string-based direct indexing.\r\n" +
                "\r\n" +
                "Enables you to query more 'direct' like for instance 'DataStorage[\"Customer\"][\"42\"][\"FirstName\"]'.\r\n" +
                "\r\n" +
                "This is especially useful in debugging situations.\r\n" +
                "(except that for the unfortunate situation as of May 2020 that you often get the " +
                "'The debugger is unable to evaluate this expression' " +
                "message when actually trying to use this in a debugging scenario)."
        )]
        IP this[string index]
        {
            get => GetP<IP>(IKString.FromString(index));
            set => SetP(IKString.FromString(index), value);
        }
        IP this[IK index]
        {
            get => GetP<IP>(index);
            set => SetP(index, value);
        }

        bool ContainsKey<TPropertyKeyEnum>(TPropertyKeyEnum key) where TPropertyKeyEnum : struct, Enum => TryGetP<IP>(PK.FromEnum(key), out var p) && !(p is PValueEmpty);
        bool ContainsKey(string key) => TryGetP<IP>(key, out var p) && !(p is PValueEmpty);
        bool ContainsKey(IK key) => TryGetP<IP>(key, out var p) && !(p is PValueEmpty);

        T As<T>() where T : class, IP => this as T ?? throw new InvalidObjectTypeException(this, typeof(T),
            "This sub class of IP (" + this.GetType().ToStringShort() + ") " +
            "was not of the expected type " + typeof(T).ToStringShort()
        );

        // public new IEnumerator<IKIP> GetEnumerator();
        // {
        //    foreach (var ikip in GetAllP()) {
        //        yield return ikip;
        //    }
        //}

        [ClassMember(Description = "Returns all the keys for this instance.")]
        IEnumerable<IK> Keys => this.Select(ikip => ikip.Key);

        [ClassMember(Description = "Returns all the values (all the properties) for this instance.")]
        IEnumerable<IP> Values => this.Select(ikip => ikip.P);

        System.Collections.IEnumerator
        System.Collections.IEnumerable.GetEnumerator() => GetEnumerator();

        [ClassMember(
            Description =
                "Describes a logger.\r\n" +
                "\r\n" +
                "NOTE: There is no effect in setting this property for classes that do not support it.\r\n" +
                "NOTE: As of Mar 2021 the only class supporting logging is -" + nameof(PConcurrent) + "-. See -" + nameof(PConcurrent.Logger) + "-.\r\n" +
                "\r\n" +
                "The logger is used by:\r\n" +
                "1) Methods like -" + nameof(Log) + "- (explicit logging).\r\n" +
                "2) Implementations of -" + nameof(IP) + "- that log -" + nameof(ARConcepts.PropertyAccess) + "-, " +
                "like for instance logging every call to -" + nameof(TrySetP) + "-.\r\n" +
                "\r\n" +
                "As of Mar 2021 the only class doing 1) or 2) is -" + nameof(PConcurrent) + "-. See -" + nameof(PConcurrent.Logger) + "-.\r\n" +
                "\r\n" +
                "Note how the default property is just a dummy property. " +
                "If not set, then logging will not be done.\r\n" +
                "\r\n" +
                "This is a special property not used by most implementations of -" + nameof(IP) + "-.\r\n" +
                "It is relevant for -" + nameof(ARConcepts.ExposingApplicationState) + "- to the -" + nameof(ARConcepts.PropertyStream) + "-.\r\n" +
                "\r\n" +
                // TODO: Delete commented out code
                //"Note that when set, parameter writeToConsole in calls to -" + nameof(Log) + "- will be ignored " +
                //"(because it is assumed that the data will be written eventually through the logger-mechanism anyway).\r\n" +
                //"\r\n" +
                "Tip: A logger in a member class within a containing class can chain together with the logger of the containing class.\r\n" +
                "Tip: See -" + nameof(StreamProcessor.AddOrRemoveOutgoingConnections) + "- for an example of how loggers are chained together.\r\n" +
                "Tip: In that example the logging for an outgoing connection uses the logger of the stream processor, but append 'ActualConnection/Outgoing/{id}/' to the context.\r\n" +
                "\r\n" +
                "See also -" + nameof(PSPrefix.app) + "-."
        )]
        public Action<string>? Logger
        {
            get => null;
            set { }
        }

        /// <summary>
        /// The simplest overload, just logging some text and be done with it.<br>
        /// Note: <see cref="LogText"/> and <see cref="LogKeyValue"/> are not named "Log" because
        /// most probably the C# compiler will pick the wrong overload (confusing 'Caller' with 'logText').
        /// </summary>
        /// <param name="logText"></param>
        /// <param name="caller"></param>
        public void LogText(string logText, [System.Runtime.CompilerServices.CallerMemberName] string caller = "") =>
            Log(new List<IK> { IKString.FromCache("Text") }, logText, caller);

        /// <summary>
        /// See also overload with "IEnumerable<(string Key, string Value)> properties" 
        /// for information about how you can log several datapoints at once.<br>
        /// Note: <see cref="LogText"/> and <see cref="LogKeyValue"/> are not named "Log" because
        /// most probably the C# compiler will pick the wrong overload (confusing 'Caller' with 'logText').
        /// </summary>
        /// <param name="key"></param>
        /// <param name="logText"></param>
        /// <param name="caller"></param>
        public void LogKeyValue(string key, string logText, [System.Runtime.CompilerServices.CallerMemberName] string caller = "") =>
            Log(new List<IK> { IKString.FromString(key) }, logText, caller);

        /// <summary>
        /// See also overload with "IEnumerable<(string Key, string Value)> properties" 
        /// for information about how you can log several datapoints at once.<br>
        /// Note: <see cref="LogText"/> and <see cref="LogKeyValue"/> are not named "Log" because
        /// most probably the C# compiler will pick the wrong overload (confusing 'Caller' with 'logText').
        /// </summary>
        /// <param name="key"></param>
        /// <param name="logText"></param>
        /// <param name="caller"></param>
        public void LogKeyValue(IK key, string logText, [System.Runtime.CompilerServices.CallerMemberName] string caller = "") =>
            Log(new List<IK> { key }, logText, caller);

        /// <summary>
        /// Overload with list of property keys (given as enum member / enum value) for which to log their values.
        /// </summary>
        /// <param name="keysAsEnum">
        /// The list of property keys (given as enum members / enum values) for which to log their values.
        /// </param>
        /// <param name="ip">
        /// The object instance whose key, values are to be logged.
        /// Defaults to instance of this class
        /// </param>
        /// <param name="caller"></param>
        public void Log(IEnumerable<object> keysAsEnum, IP? ip = null, [System.Runtime.CompilerServices.CallerMemberName] string caller = "") =>
            Log(keysAsEnum.Select(k => PK.FromEnum(k).Use(pk => (pk.ToString(), (ip ?? this).GetPV<string>(pk)))));

        /// <summary>
        /// Overload with list of key,value (list of datapoints)
        /// 
        /// Use this overload when you want to log several datapoints in one go
        /// (the general idea is to have the log look more like this:
        ///    [MyClass]/Log = [Caller]/[MyTread]/Filename = file1.txt
        ///    [MyClass]/Log = [Caller]/[MyTread]/Index = 1
        /// instead of this:
        ///    [MyClass]/Log = [Caller]/[MyTread]/Text = Reading filename 'file1.txt' at index 1
        /// </summary>
        /// <param name="properties"></param>
        /// <param name="caller"></param>
        public void Log(IEnumerable<(string Key, string Value)> properties, [System.Runtime.CompilerServices.CallerMemberName] string caller = "") =>
            Log(new List<IK>(), properties, caller);

        /// <summary>
        /// Overload with context plus list of key,value (list of datapoints)
        /// 
        /// Use this overload when you want to log several datapoints in one go
        /// 
        /// TODO: Correct this text example, most probably not correct:
        /// (the general idea is to have the log look more like this:
        ///    MyClass/Log = [Caller]/[MyTread]/[Context-1]/[Context-2]/.../[Context-n]/Filename = file1.txt
        ///    MyClass/Log = [Caller]/[MyTread]/[Context-1]/[Context-2]/.../[Context-n]/Index = 1
        /// instead of this:
        ///    MyClass/Log = [Caller]/[MyTread]/[Context-1]/[Context-2]/Text = Reading filename 'file1.txt' at index 1
        /// </summary>
        /// <param name="context">
        /// The overarching context which will be prepended to every key
        /// </param>
        /// <param name="properties"></param>
        /// <param name="caller"></param>
        public void Log(IEnumerable<IK> context, IEnumerable<(string Key, string Value)> properties, [System.Runtime.CompilerServices.CallerMemberName] string caller = "") =>
            properties.ForEach(t => Log(context.Append(IKString.FromString(t.Key)), t.Value, caller));

        // TODO: Delete commented out code
        ///// <param name="writeToConsole">
        ///// Useful for extra important information that should be shown locally immediately 
        ///// (normally, logging will only show up on the local console after a round-trip to <see cref="ARNodeType.CoreDB"/> and back,
        ///// (under the assumption that <see cref="LogContext"/> is used) 
        ///// and even then only if the relevant <see cref="Subscription"/> is set and the local <see cref="StreamProcessorP.WriteToConsole"/> is TRUE).
        ///// <br>
        ///// Note that will be ignored when <see cref="LogContext"/> is set.
        ///// <br>
        ///// Note that when using this option the logging may show up twice (first as immediately written to the console, 
        ///// then as data returned from <see cref="ARNodeType.CoreDB"/>.).
        ///// </param>

        /// <summary>
        /// See also overload with "IEnumerable<(string Key, string Value)> properties" and
        /// overload with "IEnumerable{object} keysAsEnum" for information about how you can log 
        /// several datapoints at once.
        /// </summary>
        /// <param name="valueContext">
        /// If just text, than overload without this parameter can also be used.
        /// </param>
        /// <param name="logText"></param>
        /// <param name="caller"></param>
        [ClassMember(Description =
                "The 'final' overload of -" + nameof(Log) + "- that is called by the other overloads.\r\n" +
                "\r\n" +
                "Will not do anything if -" + nameof(IP.Logger) + "- is not set.\r\n" +
                "\r\n" +
                "Will prepend [CallerMemberName]/[ThreadName]/[ThreadId] to the given context.\r\n" +
                "\r\n" +
                "See -" + nameof(PSPrefix.app) + "- for more details.")]
        public void Log(IEnumerable<IK> valueContext, string logText, [System.Runtime.CompilerServices.CallerMemberName] string caller = "") =>
            //if (writeToConsole && Logger == null) {
            //    var keysContext = new List<IK>() {
            //            IKString.FromCache("LOCAL") // Indicate that does not originate from the property stream itself (but locally logged)
            //        };
            //    keysContext.Add(IKType.FromType(GetType())); /// TODO: Consider a standardised way in AgoRapide to communicate Id of objects (like <see cref="PP.Id"/> for instance)
            //    keysContext.Add(IKString.FromCache(nameof(PSPrefix.app)));
            //    Console.WriteLine(PropertyStreamLine.EncodeWholeLine( // Encode in order to look as much like any other property stream line as possible
            //        keys: keysContext,
            //        value: logText
            //     ));
            //}
            Logger?.Invoke(
                // Note how the logged value (logText) in itself may also be in some key = value format
                PropertyStreamLine.EncodeKeyPart(caller) + "/" +
                PropertyStreamLine.EncodeKeyPart(System.Threading.Thread.CurrentThread.Name ?? "_") + "/" + // Note CurrentThread.Name may be null!
                System.Threading.Thread.CurrentThread.ManagedThreadId.ToString() + "/" +
                string.Join("/", valueContext.Select(c => PropertyStreamLine.EncodeKeyPart(c.ToString()))) +
                " = " +
                PropertyStreamLine.EncodeValuePart(logText)
            );

        /// <summary>
        /// Overload with list of property keys (given as enum members / enum values) for which to log their values.
        /// 
        /// See also overload with "IEnumerable<(string Key, string Value)> properties" for documentation
        /// </summary>
        /// <param name="action">
        /// The action whose execution time is to be logged.
        /// </param>
        /// <param name="keysAsEnum">
        /// The list of property keys (given as enum members / enum values) for which to log their values.
        /// </param>
        /// <param name="ip">
        /// The object instance whose key, values are to be logged.
        /// Defaults to instance of this class
        /// </param>
        /// <param name="caller"></param>
        public void LogExecuteTime(Action action, IEnumerable<object> keysAsEnum, IP? ip = null, [System.Runtime.CompilerServices.CallerMemberName] string caller = "") =>
            LogExecuteTime(action, keysAsEnum.Select(k => PK.FromEnum(k).Use(pk => (pk.ToString(), (ip ?? this).GetPV<string>(pk)))), caller);

        /// <summary>
        /// 
        /// </summary>
        /// The action whose execution time is to be logged.
        /// <param name="properties"></param>
        /// <param name="caller"></param>
        public void LogExecuteTime(Action action, IEnumerable<(string Key, string Value)>? properties = null, [System.Runtime.CompilerServices.CallerMemberName] string caller = "") =>
            LogExecuteTime(() => { action(); return true; }, properties, caller);

        /// <summary>
        /// Overload with list of property keys (given as enum members / enum values) for which to log their values.
        /// 
        /// See also overload with "IEnumerable<(string Key, string Value)> properties" for documentation
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="func"></param>
        /// The function whose execution time is to be logged.
        /// <param name="keysAsEnum">
        /// The list of property keys (given as enum members / enum values) for which to log their values.
        /// </param>
        /// <param name="ip">
        /// The object instance whose key, values are to be logged.
        /// Defaults to instance of this class
        /// </param>
        /// <param name="caller"></param>
        /// <returns></returns>
        public T LogExecuteTime<T>(Func<T> func, IEnumerable<object>? keysAsEnum, IP? ip, [System.Runtime.CompilerServices.CallerMemberName] string caller = "") =>
            LogExecuteTime(func, keysAsEnum.Select(k => PK.FromEnum(k).Use(pk => (pk.ToString(), (ip ?? this).GetPV<string>(pk)))), caller);

        [ClassMember(Description =
            "Times the execution of the given function and logs details about elapsed time.\r\n" +
            "Example of log output (with properties parameter containing two keys, 'propertiesKeyA' and 'propertiesKeyB'):\r\n" +
            "TODO: Correct this text example, most probably not correct:\r\n" +
            "  log/[LogContext]/log = [MethodName]/[ThreadName]/[ThreadId]/Start = 2020-06-03 13:20:59.760\r\n" +
            "  log/[LogContext]/log = [MethodName]/[ThreadName]/[ThreadId]/propertiesKeyA = Value of A\r\n" +
            "  log/[LogContext]/log = [MethodName]/[ThreadName]/[ThreadId]/propertiesKeyA = Value of B\r\n" +
            "  log/[LogContext]/log = [MethodName]/[ThreadName]/[ThreadId]/Finish = 2020-06-03 13:21:00.766\r\n" +
            "  log/[LogContext]/log = [MethodName]/[ThreadName]/[ThreadId]/ElapsedTime = 00:00:01.005\r\n"
        )]
        public T LogExecuteTime<T>(Func<T> func, IEnumerable<(string Key, string Value)>? properties = null, [System.Runtime.CompilerServices.CallerMemberName] string caller = "")
        {
            var start = UtilCore.DateTimeNow;
            // TODO: Consider turning 'Start' into a formal enum member / enum value?
            // TODO: Why do we not log with a stronger data type than string here?
            LogKeyValue("Start", start.ToStringDateAndTime(), caller);
            if (properties != null) Log(properties, caller);
            var retval = func();
            var end = UtilCore.DateTimeNow;
            // TODO: Consider turning 'Finish' into a formal enum member / enum value?
            // TODO: Why do we not log with a stronger data type than string here?
            LogKeyValue("Finish", end.ToStringDateAndTime(), caller);
            // TODO: Consider turning 'ElapsedTime' into a formal enum member / enum value?
            // TODO: Why do we not log with a stronger data type than string here?
            LogKeyValue("ElapsedTime", (end - start).ToStringWithMS(), caller);
            return retval;
        }

        [ClassMember(Description =
            "Note that most relevant to use for -" + nameof(PConcurrent) + "-. " +
            "\r\n" +
            "Using this method within an ordinary entity class like -" + nameof(PRich) + "- will only store the exception data 'out-of-sight' within the object instance, " +
            "it will not be sent to the -" + nameof(ARConcepts.PropertyStream) + "-.\r\n" +
            "\r\n" +
            "See -" + nameof(PP.ExceptionText) + " for more details.")]
        public void HandleException(Exception ex, [System.Runtime.CompilerServices.CallerMemberName] string caller = "")
        {
            try
            {
                REx.Inc(); // We are a possibly recursive function if new exception occurs 'below' (while writing / logging exception), therefore this guard.
                var text =
                    UtilCore.GetExceptionChainAsString(ex) + " occurred in " + GetType().ToStringShort() + "." + caller + ":\r\n" +
                    UtilCore.GetExeptionDetails(ex);
                Console.WriteLine(
                    "\r\n\r\n" +
                    text +
                    "\r\n\r\n");
                SetPV(PP.ExceptionText, text); /// Will be distributed over <see cref="ARConcepts.PropertyStream"/> if <see cref="LogContext"/> is set.
            }
            finally
            {
                REx.Dec();
            }
        }


        [ClassMember(Description =
            "Parses a " + nameof(PropertyStreamLine) + "- beginning with -" + nameof(PSPrefix.dtr) + "-.\r\n" +
            "\r\n" +
            "The parameter 'propertyStreamLine' should include the 'dtr/' prefix.\r\n" +
            "\r\n" +
            "See also -" + nameof(PropertyStreamLine.TryParseDirectToIP) + "-.\r\n" +
            "\r\n" +
            "See also (in -" + nameof(ARComponents.ARCEvent) + "-) IRegHandler.-TryParse-.\r\n"
        )]
        public static bool TryParseDtr(string propertyStreamLine, out IKIP ikip, out string errorResponse)
        {
            var t = propertyStreamLine.Split("/").Select(s => PropertyStreamLine.Decode(s)).ToList();

            if (t.Count < 3)
            {
                ikip = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = nameof(TryParseDtr) + ": Less than three items (" + t.Count + ")";
                return false;
            }

            if (!t[0].Equals(nameof(PSPrefix.dtr)))
            {
                ikip = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = nameof(TryParseDtr) + ": Not with prefix 'dtr'.";
                return false;
            }

            if (!AllIPDerivedTypesDict.TryGetValue(t[1], out var type))
            {
                ikip = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = nameof(TryParseDtr) + ": IP derived Type '" + t[1] + "' not found + '.";
                return false;
            }
            var ik = long.TryParse(t[2], out var l) ?
                (IK)(new IKLong(l)) : // IKLong takes up less memory and is preferred as key
                IKString.FromString(t[2]);

            var ip = (IP)System.Activator.CreateInstance(type);

            // Apply values in order
            var pks = PK.GetAllPKForEntityType(type).ToList(); // ToList makes for easier iteration
            var parameters = t.Skip(3).ToList(); // Make comparing easier (same indices)
            var i = 0;
            for (i = 0; i < parameters.Count && i < pks.Count; i++)
            {
                // TODO: Skip if blank and not IsObligatory
                var parameterValue = parameters[i];
                var pk = pks[i];
                if (
                    string.IsNullOrEmpty(parameterValue) &&
                    !pk.GetA<PKTypeAttribute>().IP.GetPV<bool>(PKTypeAttributeP.IsObligatory, defaultValue: false)
                )
                {
                    continue; // Blank and not obligatory, OK
                }
                if (!pk.TryCleanParseAndPack(parameterValue, out var property, out errorResponse))
                {
                    ikip = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                // TODO: Link to full documentation
                    errorResponse =
                        "Illegal value (" + parameterValue + ") given for parameter -" + pk.__enum.GetType().ToStringVeryShort() + "-.-" + pk.__enum + "-\r\n" +
                        "(Parameter no " + (i + 1) + " of " + parameters.Count + " given / " + pks.Count + " possible (counted from one))\r\n" +
                        "Details:\r\n" + errorResponse + "\r\n\r\n" +
                        (pk.IP.TryGetPV<string>(BaseAttributeP.Description, out var description) ?
                            ("Description of parameter:\r\n----------" + description) : ""
                        );
                    return false;
                }
                ip.SetP(new IKIP(pk, property));
            }

            if (parameters.Count > i)
            {
                ikip = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                // TODO: Link to full documentation
                errorResponse =
                    "Excess parameters given for -" + type.ToStringShort() + "-.\r\n" +
                    "The excess parameters are: " + string.Join(", ", parameters.Skip(i).Select(r => "'" + r + "'"));
                return false;
            }
            if (pks.Count > i && pks.Skip(i).Any(pk => pk.GetA<PKTypeAttribute>().IsObligatory))
            {
                ikip = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse =
                    "Missing obligatory parameters for -" + type.ToStringShort() + "-.\r\n" +
                    "The missing parameters are:\r\n" + string.Join("\r\n", pks.Skip(i).Where(pk => pk.GetA<PKTypeAttribute>().IsObligatory).Select(pk =>
                        "-----------\r\n-" + pk.__enum.GetType().ToStringVeryShort() + "-.-" + pk.__enum.ToString() + "-" + (pk.IP.TryGetPV<string>(BaseAttributeP.Description, out var description) ?
                        ("\r\n----------" + description) : ""
                    )));
                return false;
            }

            ikip = new IKIP(ik, ip);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Note that has a default implementation (doing nothing).\r\n" +
            "Any implementation should start with a call to -" + nameof(AssertIntegrity) + "-."
        )]
        public void Initialize() { }

        [ClassMember(Description = "See -" + nameof(TryAssertIntegrity) + "-.")]
        public void AssertIntegrity()
        {
            if (!TryAssertIntegrity(out var missingProperties))
            {
                throw new IntegrityAssertionException(
                    "This object (" + ToString() + ") does not pass integrity test because some " + nameof(PKTypeAttributeP.IsObligatory) + " properties are missing.\r\n" +
                    "Resolution: Ensure that the missing properties are set.\r\n" +
                    "The missing properties (" + missingProperties.Count + " in total) are:\r\n" +
                    string.Join("\r\n", missingProperties.Select(e => e.key.ToString() + ": " + e.errorResponse)) + "\r\n"
                );
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="missingProperties">Will always be set, regardless of TRUE or FALSE being returned</param>
        /// <returns></returns>
        [ClassMember(Description =
            "Asserts the integrity of this entity. " +
            "\r\n" +
            "As of Mar 2020 this consists of asserting that all obligatory values are set (based on -" + nameof(PKTypeAttributeP.IsObligatory) + "-.).\r\n" +
            "\r\n" +
            "Note: Will also add any missing missing obligatory values if a -" + nameof(PKTypeAttributeP.DefaultValue) + "- is found.\r\n" +
            "TODO: NOTE: This is not necessary smart, as it will consume more memory (at least for -" + nameof(PRich) + "-, " +
            "TODO: NOTE: not necessarily for -" + nameof(PExact<TPropertyKeyEnum>) + "-).\r\n" +
            "TODO: NOTE: Consider instead letting -" + nameof(IP.TryGetP) + "- look for default value instead of throwing exception " +
            "TODO: NOTE: (which will coincidentally also lead to AssertIntegrity not being as necessary).\r\n" +
            "TODO: NOTE: (you should also be aware of the concept of -EntityMethodKey-s in -" + nameof(ARComponents.ARCQuery) + "-.\r\n"
        )]
        public bool TryAssertIntegrity(out List<(PK key, string errorResponse)> missingProperties)
        {
            missingProperties = new List<(PK key, string errorResponse)>();
            foreach (var pk in PK.GetObligatoryValuesForEntityType(GetType()))
            {
                if (TryGetP<IP>(pk, out _, out var errorResponse))
                {
                    continue;
                }
                if (!pk.GetA<PKTypeAttribute>().IP.TryGetPV<object>(PKTypeAttributeP.DefaultValue, out var defaultValue))
                {
                    missingProperties.Add((pk, errorResponse));
                    continue;
                }
                // Note that naively doing 
                //   AddPV(pk, defaultValue);
                // will create a PValue<object>
                // Therefore we do
                AddP(pk, pk.PackObjectForStorageInEntityObject(defaultValue));
            };

            if (missingProperties.Count == 0)
            {
                // OK, all obligatory values are set
                return true;
            }

            return false;
        }

        [ClassMember(
            Description =
                "The ToString()-implementation in the classes implementing this interface is supposed to give a short concise representation of the object " +
                "for debugging and logging purposes, that is, intended for human consumption. " +
                "The most 'important' implementation is probably the one in -" + nameof(PRich) + "- (-" + nameof(PRich.ToString) + "-) " +
                "because that one is often the basis for your entity classes like 'Customer', 'Order' and so on",
            LongDescription =
                "NOTE: Having ToString() as an interface method is kind of pointless with regard to forcing and implementation, " +
                "because any object implements ToString() anyway, and therefore the compiler will not complain about " +
                "missing implementations in any classes implementing this interface." +
                "\r\n" +
                "For some explanation of the logic behind this, see (tldr \"class always win\") " +
                "http://www.lambdafaq.org/why-cant-default-methods-override-equals-hashcode-and-tostring/ "
        )]
        public string ToString() => GetType().ToString(); // NOTE, having a default implementation is a quick-fix due to bug in .NET (search for POSITION 1 for more information)

        private static HashSet<Type>? _allIPDerivedTypes;
        [ClassMember(
            Description =
                "All relevant -" + nameof(IP) + "--derived  types for the whole application.\r\n" +
                "\r\n" +
                "Note how -" + nameof(IP) + "- is NOT included in this collection. " +
                "The same goes for generic types, abstract types and other interfaces.\r\n" +
                "\r\n" +
                "TODO: Ascertain reason for why that is so (probably because must be able to instantiate type?).\r\n" +
                "TODO. Define more clearly purpose of this method.\r\n" +
                "\r\n" +
                "TODO: Probably this method is not in use as of May 2020.\r\n" +
                "\r\n" +
                "See also -" + nameof(AllIPDerivedTypesInludingGenericAndAbstract) + "-.\r\n" +
                "\r\n" +
                "Probable faster methods to call are one of\r\n" +
                "-" + nameof(AllIPDerivedTypesDict) + "- or\r\n" +
                "-" + nameof(AllIPDerivedTypesDictIKType) + "-."
        )]
        public static HashSet<Type> AllIPDerivedTypes => _allIPDerivedTypes ??= new Func<HashSet<Type>>(() =>
        {
            UtilCore.AssertCurrentlyStartingUp(); // TODO: Do we need this assertion? Probably not
            var retval = new HashSet<Type>();
            UtilCore.Assemblies.SelectMany(a => a.GetTypes()).Where(t =>
            {
                if (!typeof(IP).IsAssignableFrom(t)) return false;
                if (t.IsGenericTypeDefinition) return false; /// Typically <see cref="PExact{EnumType}"/>
                if (t.IsAbstract) return false; /// Typically <see cref="BasePKAttribute"/>
                if (t.IsInterface) return false; /// Excludes <see cref="IP"/> itself
                return true;
            }).ForEach(t =>
            {
                if (!retval.Add(t)) throw new InvalidTypeException(t, "Already contained in " + nameof(retval) + "\r\n");
            });
            return retval;
        })();


        private static HashSet<Type>? _allIPDerivedTypesWithPKEnum;
        [ClassMember(
            Description =
                "All relevant -" + nameof(IP) + "--derived  types for the whole application for which the also is defined a -" + nameof(AREnumType.PropertyKeyEnum) + "-.\r\n" +
                "\r\n" +
                "In other words, all entity classes defined, like 'Customer', 'Order' and so on.\r\n" +
                "Note that collection -" + nameof(AllIPDerivedTypesWithPKEnumApplicationSpecific) + "- is often better to use in a user context " +
                "because it attempts to exclude -" + nameof(ARConcepts.StandardAgoRapideCode) + "-."
        )]
        public static HashSet<Type> AllIPDerivedTypesWithPKEnum => _allIPDerivedTypesWithPKEnum ??= new Func<HashSet<Type>>(() =>
        {
            UtilCore.AssertCurrentlyStartingUp(); // TODO: Do we need this assertion? Probably not
            var retval = new HashSet<Type>();
            AllIPDerivedTypes.ForEach(t =>
            {
                var a = ClassAttribute.GetAttribute(t);
                if (!a.IP.TryGetP<IP>(ClassAttributeP.CorrespondingEnumType, out _)) return;
                if (!retval.Add(t)) throw new InvalidTypeException(t, "Already contained in " + nameof(retval) + "\r\n");
            });
            return retval;
        })();

        private static HashSet<Type>? _allIPDerivedTypesWithPKEnumApplicationSpecific;
        [ClassMember(
            Description =
                "All relevant -" + nameof(IP) + "--derived types for the -" + nameof(ARConcepts.ApplicationSpecificCode) + "- part of the application " +
                "for which also is defined a -" + nameof(AREnumType.PropertyKeyEnum) + "-.\r\n" +
                "\r\n" +
                "Useful in error message to user, when hinting about possible name of entity classes (tables) to use.\r\n" +
                "In other words contains all entity classes defined, like 'Customer', 'Order' and so on.\r\n" +
                "\r\n" +
                "Attempts to exclude -" + nameof(ARConcepts.StandardAgoRapideCode) + "- by exluding types residing in assemblies with names " +
                "corresponding to one of -" + nameof(ARComponents) + "-."
        )]
        public static HashSet<Type> AllIPDerivedTypesWithPKEnumApplicationSpecific => _allIPDerivedTypesWithPKEnumApplicationSpecific ??= new Func<HashSet<Type>>(() =>
        {
            UtilCore.AssertCurrentlyStartingUp(); // TODO: Do we need this assertion? Probably not
            var retval = new HashSet<Type>();
            var exclude = UtilCore.EnumGetMembers<ARComponents>().Select(e => e.ToString()).ToHashSet();
            AllIPDerivedTypesWithPKEnum.ForEach(t =>
            {
                if (exclude.Contains(t.Assembly.GetName().Name)) return;
                if (!retval.Add(t)) throw new InvalidTypeException(t, "Already contained in " + nameof(retval) + "\r\n");
            });
            return retval;
        })();

        private static Dictionary<Type, Type>? _allIPDerivedEntityCollectionClassesDict;
        [ClassMember(
            Description =
                "All -" + nameof(IP) + "--derived types for which is found a corresponding class with name like '{EntityType}Collection' " +
                "\r\n" +
                "Key is the type of the entity class, like 'Customer'. Value is the type of the container class, like 'CustomerCollection'.\r\n" +
                "\r\n" +
                "Note how -" + nameof(PropertyStreamLineParsed.TryParse) + "- choses a collection class " +
                "with help from " + nameof(IP.AllIPDerivedEntityCollectionClassesDict) + ".\r\n" +
                "A typical base class for such collections could be -" + nameof(PCollection) + "- " +
                "but other classes may also be used.\r\n" +
                "For instance in -" + nameof(ARComponents.ARCEvent) + "- there is a different base class PCollectionES " +
                "supporting event sourcing.\r\n" +
                "\r\n" +
                "Used by -" + nameof(PropertyStreamLineParsed.TryParse) + "- in order to support -" + nameof(ARConcepts.Indexing) + "-."
        )]
        public static Dictionary<Type, Type> AllIPDerivedEntityCollectionClassesDict => _allIPDerivedEntityCollectionClassesDict ??= new Func<Dictionary<Type, Type>>(() =>
        {
            UtilCore.AssertCurrentlyStartingUp(); // TODO: Do we need this assertion? Probably not
            var retval = new Dictionary<Type, Type>();
            AllIPDerivedTypes.ForEach(t =>
            {
                var s = t.ToStringVeryShort();
                if (AllIPDerivedTypesDict.TryGetValue(s + "Collection", out var collectionType))
                {
                    if (retval.ContainsKey(t)) throw new InvalidTypeException(t, "Already contained in " + nameof(retval) + "\r\n");
                    retval[t] = collectionType;
                }
            });
            return retval;
        })();

        private static Dictionary<string, Type>? _allIPDerivedTypesDict;
        [ClassMember(Description =
            "Provides lookup of a type in -" + nameof(AllIPDerivedTypes) + "- given its string representation.\r\n" +
            "\r\n" +
            "Keys are all the following possible string representations of type:\r\n" +
            "-" + nameof(Extensions.ToStringVeryShort) + "-\r\n" +
            "-" + nameof(Extensions.ToStringVeryShort) + "- in lower case\r\n" +
            "-" + nameof(Extensions.ToStringShort) + "-\r\n" +
            "-" + nameof(Extensions.ToStringDB) + "-\r\n" +
            "-" + nameof(Type.ToString) + "-\r\n"
        )]
        public static Dictionary<string, Type> AllIPDerivedTypesDict = _allIPDerivedTypesDict ??= new Func<Dictionary<string, Type>>(() =>
        {
            var retval = new Dictionary<string, Type>();
            AllIPDerivedTypes.ForEach(t =>
            {
                var add = new Action<string>(s =>
                {
                    if (retval.TryGetValue(s, out var duplicate))
                    {
                        if (duplicate.Equals(t)) return;
                        throw new UniquenessException(
                            "The types " + t.ToStringDB() + " and " + duplicate.ToStringDB() + " " +
                            "both resolve to the same string representation " +
                            "(namely '" + s + "')\r\n" +
                            "\r\n" +
                            "This makes it impossible to correctly use " + nameof(PropertyStreamLineParsed) + "." + nameof(PropertyStreamLineParsed.TryParse) +
                            "because types in the -" + nameof(PropertyStreamLine) + " " +
                            "are assumed to be in the " + nameof(Extensions.ToStringVeryShort) + " representation.\r\n" +
                            "\r\n" +
                            "Resolution:\r\n" +
                            "Either rename one or more of the types, " +
                            "or exclude one of them from the -" + nameof(AllIPDerivedTypes) + "- collection " +
                            "(for instance by not including the relevant assembly when setting " + nameof(UtilCore.Assemblies) + " at application startup).");
                    }
                    retval.Add(s, t);
                });
                add(t.ToStringVeryShort());
                add(t.ToStringVeryShort().ToLower());
                add(t.ToStringShort());
                add(t.ToStringDB());
                add(t.ToString());
            });
            return retval;
        })();

        private static Dictionary<string, IKType>? _allIPDerivedTypesDictIKType;
        [ClassMember(Description =
            "Same as -" + nameof(AllIPDerivedTypesDict) + "- except that value is -" + nameof(IKType) + "-, not string.\r\n" +
            "(saves the calling method from constructing an -" + nameof(IKType) + "- from the type returned.)"
        )]
        public static Dictionary<string, IKType> AllIPDerivedTypesDictIKType = _allIPDerivedTypesDictIKType ??= _allIPDerivedTypesDict.ToDictionary(e => e.Key, e => IKType.FromType(e.Value));

        private static HashSet<Type>? _allIPDerivedTypesInludingGenericAndAbstract;
        [ClassMember(
            Description =
                "All relevant -" + nameof(IP) + "--derived  types (except the interface -" + nameof(IP) + "- itself) known to the application.\r\n" +
                "\r\n" +
                "HACK: Hack because -" + nameof(AllIPDerivedTypes) + "- is too restrictive."
        )]
        public static HashSet<Type> AllIPDerivedTypesInludingGenericAndAbstract => _allIPDerivedTypesInludingGenericAndAbstract ??= new Func<HashSet<Type>>(() =>
        {
            UtilCore.AssertCurrentlyStartingUp(); // TODO: Do we need this assertion? Probably not
            var retval = new HashSet<Type>();
            UtilCore.Assemblies.SelectMany(a => a.GetTypes()).Where(t =>
            {
                if (!typeof(IP).IsAssignableFrom(t)) return false;
                if (t.IsInterface) return false; /// Excludes <see cref="IP"/> itself
                return true;
            }).ForEach(t =>
            {
                if (retval.Contains(t)) throw new InvalidTypeException(t, "Already contained in " + nameof(retval) + "\r\n");
                retval.Add(t);
            });
            return retval;
        })();

        public class IntegrityAssertionException : ApplicationException
        {
            public IntegrityAssertionException(string message) : base(message) { }
        }

        public class SetPException : ApplicationException
        {
            public SetPException(string message) : base(message) { }
        }

        public class RemovePException : ApplicationException
        {
            public RemovePException(string message) : base(message) { }
        }

        public class KeyAlreadyExistsException : ApplicationException
        {
            public KeyAlreadyExistsException(IKIP ikip, string? details = null) : base(
                "The key\r\n" +
                "\r\n" +
                ikip.Key + "\r\n" +
                "\r\n" +
                "already exists.\r\n" +
                "\r\n" +
                "Unable to add value\r\n" +
                "'" + ikip.P.ToString().Use(s => s[..Math.Min(s.Length, 100)] + (s.Length <= 100 ? "" : "...")) + "'.\r\n" +
                (details == null ? "" : ("\r\nDetails: " + details))
            )
            { }
        }

        public class PropertyNotFoundException<T> : ApplicationException
        {
            public PropertyNotFoundException(IK k, string? details = null) : base(
                    "No value found for " + k.ToString() + "\r\n" +
                    "Expected a value of type " + typeof(T) + ".\r\n" +
                    (details == null ? "" : ("\r\nDetails: " + details)))
            {
            }
        }

        public class PropertyValueNotFoundException<T> : ApplicationException
        {
            public PropertyValueNotFoundException(string? details = null) : base(
                    "Expected a value of type " + typeof(T) + ".\r\n" +
                    (details == null ? "" : ("\r\nDetails: " + details)))
            {
            }
        }

        public class EventHandlerException : ApplicationException
        {
            public EventHandlerException(string message, Exception inner) : base(message, inner) { }
        }

    }

    [Class(Description =
        "'Dummy'-class, not necessarily in use. See -" + nameof(PP) + "- for more information. " +
        "Created because of the concept of " + nameof(EnumAttributeP.CorrespondingClass) + " which is strictly enforced"
    )]
    public class P : PRich
    {
        public P() => throw new NotImplementedException("This class (" + GetType().ToStringShort() + ") is not expected to be used. If assumption turns out to be wrong, just delete this constructor.\r\n");
    }

    [Enum(
        Description =
            "Describes meta data for any class (property) implementing -" + nameof(IP) + "-.\r\n" +
            "\r\n" +
            "Not necessarily stored for every property.\r\n" +
            "\r\n" +
            "Can be added 'to any' property, but will result in higher memory consumption, because more complex objects will then have to be used. " +
            "(For instance having to use -" + nameof(PRich) + "- instead of -" + nameof(PValue<TValue>) + "-.\r\n" +
            "See -" + nameof(Value) + "- for more details about this.)\r\n" +
            "\r\n" +
            "See also -" + nameof(ARConcepts.MemoryConsumption) + "-.",
        LongDescription =
            "If stored as items in a Dictionary then values will be stored together with child-properties (if relevant) in the same dictionary. " +
            "Example: You might have 'Customer/42/Cid = Agent/43' which will be stored alongside 'Customer/42/Firstname = John'.\r\n" +
            "\r\n" +
            "Some values for this enum can be considered tags (like -" + nameof(Cid) + "-), " +
            "while other values can be considered actions taken on property (like -" + nameof(Invalid) + "-).",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum PP
    {
        __invalid,

        [PKType(
            Description =
                "Timestamp when property was created in database.\r\n" +
                "\r\n" +
                "When read from -" + nameof(ARConcepts.PropertyStream) + "- this value will usually originate from -" + nameof(StreamProcessorP.Timestamp) + "-, " +
                "not as a specific Created-property.\r\n" +
                "\r\n" +
                "Example: If the -" + nameof(ARConcepts.PropertyStream) + "- contains the following:\r\n" +
                "Timestamp = 2021-02-10 10:31:01\r\n" +
                "dt/Customer/42/Name = John\r\n" +
                "Then it is not necessary to also explicitly create in the property stream\r\n" +
                "dt/Customer/42/Name/Created = 2021-02-10 10:31:01\r\n" +
                "\r\n" +
                "NOTE: As of Feb 2021 there is no mechanism for deciding whether to store (in-memory) an implicit Created for a single property or not, " +
                "only for an entity class (see -" + nameof(ICreated) + "-).\r\n" +
                "TODO: Considering implement something corresponding to -" + nameof(ICreated) + "- also for single properties.\r\n" +
                "\r\n" +
                "See also -" + nameof(ICreated) + "- which (if implemented by the Customer-class in the example above) " +
                "will also store Created (in-memory) as a property of the Customer instance.",
            Type = typeof(DateTime))]
        Created,

        [PKType(Description =
            "Creator id (entity which created this property).\r\n" +
            "\r\n" +
            "NOTE: See -" + nameof(IK) + "- for note about more strongly typing of this property"

        )]
        Cid,

        [PKType(
            Description =
                "The actual value.\r\n" +
                "\r\n" +
                "Note that this is mostly relevant when storing values inside a -" + nameof(PRich) + "- object because we want to store some metadata associated with it, " +
                "like -" + nameof(PP.Cid) + "-, -" + nameof(PP.Created) + "- and similar.\r\n" +
                "\r\n" +
                "Normally, values are stored directly inside the entity object as a -" + nameof(PValue<TValue>) + "- object, not as a -" + nameof(PP.Value) + "-.\r\n" +
                "\r\n" +
                "TODO: Try to find a more suitable 'home' for this enum member. It is not really metadata which is all -" + nameof(PP) + "- is about",
            LongDescription =
                "Detailed explanation:\r\n" +
                "For 'Customer/42/FirstName = John', FirstName would normally be a -" + nameof(PValue<TValue>) + "- stored directly inside the Customer-entity, " +
                "but if we want to also do 'Customer/42/FirstName.Cid = Actor/43', then the -" + nameof(PValue<TValue>) + "- for storing FirstName " +
                "must be replaced with a -" + nameof(PRich) + "- instance which would have two properties, " +
                "-" + nameof(PP.Value) + "- as a -" + nameof(PValue<string>) + "- for storing 'John' and a -" + nameof(PP.Cid) + "- for storing 'Actor/43'",
            Type = typeof(object) // Note how string is the default type, therefore set explicit to object. Will normally be a PValue<T>
        )]
        Value,

        [PKType(
            Description = "Timestamp when property was last known to be (still) valid",
            Type = typeof(DateTime))]
        Valid,

        [PKType(Description =
            "Validator id, that is entity which set -" + nameof(Valid) + "- for this property.\r\n" +
            "\r\n" +
            "NOTE: See -" + nameof(IK) + "- for note about more strongly typing of this property"

        )]
        Vid,

        [PKType(
            Description =
                "Timestamp when property was invalidated / 'deleted'.\r\n" +
                "\r\n" +
                "Not necessary to use when a property with a given key is simply replaced by a new property (but with the same key), " +
                "but because of -" + nameof(ARConcepts.NoRealDeletion) + "-, " +
                "there has to be a mechanism which can at least simulate deletion of data.\r\n" +
                "\r\n" +
                "TOOD: Decide about how to do invalidation of invalid (making possible to recreate a key again).\r\n",

            // NOTE: =========================
            // NOTE: Dec 2020: Setting Invalid Invalid as suggested below is difficult for the in-memory storage, since it will most probably have deleted the
            // NOTE: relevant objects
            //"Note: If you need to remove or 'undo' an Invalid-operation, set Invalid to '0001-01-01' which is the default value for a DateTime in .NET " +
            //"(this is a practical approach because it is difficult to set Invalid the field Invalid (or something like that)).",
            // NOTE: =========================

            LongDescription =
                "See also -" + nameof(IP.IsValid) + "- and -" + nameof(ARConcepts.DataRetention) + "-.\r\n" +
                "\r\n" +
                "When received over -" + nameof(ARConcepts.PropertyStream) + "- -" + nameof(PropertyStreamLine.TryStore) + "- will usually delete the actual object from its object storage.\r\n" +
                "but keep the information about when it was invalidated and by whoom.\\r\n" +
                "Note importance of ordering, for instance for the following sequence:\r\n" +
                "  Customer/42/FirstName = John\r\n" +
                "  Customer/42/FirstName/Invalid = 2020-03-20\r\n" +
                "  Customer/42/FirstName/IId = Agent/42\r\n" + /// See <see cref="Iid"/>
                "The second line will delete the value of the key 'FirstName' but the third line will store key 'FirstName/IId' as a key.\r\n" +
                "\r\n" +
                "For the following sequence:\r\n" +
                "  Customer/42/FirstName = John\r\n" +
                "  Customer/42/LastName = Smith\r\n" +
                "  Customer/42/Invalid = 2020-03-20\r\n" +
                "the third line will delete all values of the key 'Customer/42'"
            ,
            Type = typeof(DateTime))]
        Invalid,

        [PKType(Description =
            "Invalidator id, that is entity which set -" + nameof(Invalid) + "- for this property (or removed / 'undid' the invalidation by setting the value to '0001-01-01').\r\n" +
            "\r\n" +
            "NOTE: See -" + nameof(IK) + "- for note about more strongly typing of this property"
        )]
        Iid,

        [PKType(
            Description =
                "General property for logging of exceptions, usually set through -" + nameof(IP.HandleException) + "-.\r\n" +
                "\r\n" +
                "See also -" + nameof(PSPrefix.app) + "-.\r\n" +
                "\r\n" +
                "TODO: Try to find a more suitable 'home' for this enum member. It is not really metadata which is all what -" + nameof(PP) + "- is about",
            LongDescription =
                "Usually used for -" + nameof(PConcurrent) + "- for sending exception-text over the -" + nameof(ARConcepts.PropertyStream) + "-. " +
                "(Note how -" + nameof(PConcurrent) + "-will (when LogContext is set) " +
                "actually send ALL changes to any property, not only " + nameof(ExceptionText) + ".)"
        )]
        ExceptionText,
    }

    [Class(Description =
        "A dummy interface signifying that implementing class has the ability to store (in-memory) metadata -" + nameof(PP.Created) + "-\r\n" +
        "\r\n" +
        "-" + nameof(PropertyStreamLine.TryStore) + "- will always store (in-memory) -" + nameof(PropertyStreamLine.CurrentTimestamp) + "- " +
        "as -" + nameof(PP.Created) + "- when it creates entity classes implementing this interface.\r\n" +
        "\r\n" +
        "TODO: Consider creating corresponding interfaces Valid and (possibly also) Invalid.\r\n" +
        "\r\n" +
        "TODO: Consider creating corresponding concept for single properties (like Customer's FirstName).\r\n"
    )]
    public interface ICreated
    {
    }
}