﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

/// <summary>
/// Contains the following classes / interfaces:
/// <see cref="ARCCore.PCollection"/>
/// <see cref="ARCCore.IGetKeysEqualToValue"/>
/// </summary>
namespace ARCCore {

    [Class(Description =
        "A class useful for storing a collection of entities (a table in RDBMS-terms) " +
        "because it implements -" + nameof(ARConcepts.Indexing) + "-.\r\n" +
        "\r\n" +
        "Uses -" + nameof(ARConcepts.Indexing) + "- for all keys ending with 'Id' (keys assumed to be foreign keys).\r\n" +
        "\r\n" +
        "Example, for an entity type 'Order', you can create an inheriting class like this:\r\n" +
        "  public class OrderCollection : PCollection {\r\n" +
        "  }\r\n" +
        "in order to utilize indexing when searching for foreign keys in the 'Order'-collection, like 'Order/WHERE CustomerId = 42' " +
        "\r\n" +
        "Note how -" + nameof(PropertyStreamLineParsed.TryParse) + "- choses a collection class " +
        "with help from -" + nameof(IP.AllIPDerivedEntityCollectionClassesDict) + "-, " +
        "but it does not necessarily have to inherit -" + nameof(PCollection) + "-.\r\n" +
        "\r\n"  +
        "TODO: Consider introducing a generic type-parameter for the class for which we are a collection for.\r\n" +
        "TODO: See -PCollectionES- in -" + nameof(ARComponents.ARCEvent) + "- for examples."
    )]
    public class PCollection : PRich, IGetKeysEqualToValue, ITrySetPP {

        [ClassMember(Description =
            "Outer key is the foreign key, like for 'Order' keys like 'CustomerId', 'EmployeeId', 'ShipperId'.\r\n" +
            "\r\n" +
            "Inner key is the actual values for the foreign key (like 'CustomerId = 42', with the inner value the key of the entities " +
            "containing this key (like '1968' and '1981' meaning that the entities Order/1968 and Order/1981 contains 'CustomerId = 42'"
        )]
        private readonly Dictionary<IK, Dictionary<string, HashSet<IK>>> _indices = new Dictionary<IK, Dictionary<string, HashSet<IK>>>();

        [ClassMember(Description =
            "Replaces default interface method -" + nameof(IP.TrySetPP) + "-.\r\n" +
            "\r\n" +
            "Uses -" + nameof(ARConcepts.Indexing) + "-.\r\n" +
            "\r\n" +
            "TODO: Implement support for -" + nameof(Cardinality.WholeCollection) + "-, that is, multiple foreign keys in one property."
        )]
        public override bool TrySetPP(IK entityKey, IP? entity, IKIP ikip, out string errorResponse) {
            if (entity == null) {
                if (!TryGetP<IP>(entityKey, out entity, out errorResponse)) {
                    // Entity not found
                    return false;
                }
            }
            if (
                !ikip.Key.ToString().EndsWith("Id")
            ) {
                // Do not use indexing, just set the property
                return entity.TrySetP(ikip, out errorResponse);
            }

            var strOldValue = entity.TryGetPV<string>(ikip.Key, out var t) ? t : null;

            if (!entity.TrySetP(ikip, out errorResponse)) {
                // No need to update index because setting of property failed anyway.
                return false;
            }

            if (!_indices.TryGetValue(ikip.Key, out var index)) {
                // Index for this key does not exist at all.
                // Like we are setting 'Order.CustomerId = 42' for the first time.
                _indices[ikip.Key] = (index = new Dictionary<string, HashSet<IK>>());
            }

            var strNewValue = ikip.P.TryGetV<string>(out t) ? t : null;

            if (strOldValue == null && strNewValue == null) {
                // No need to update index at all because property value did not change
                return true;
            }

            if (strOldValue != null && strNewValue != null && strOldValue.Equals(strNewValue)) {
                // No need to update index at all because property value did not change
                return true;
            }

            // Remove old value if relevant
            if (
                strOldValue != null && // If == null, then no need to remove from index
                index.TryGetValue(strOldValue, out var entityKeys) && // If fail, index was not up-to-date
                entityKeys.Contains(entityKey) // If fails, index was not up-to-date
             ) {
                entityKeys.Remove(entityKey);
            }

            // Set new value if relevant
            if (strNewValue != null) {
                if (!index.TryGetValue(strNewValue, out entityKeys)) {
                    // This value does not exist.
                    // Like for 'Order.CustomerId = 42' that we are seeing the value '42' for the first time.
                    index[strNewValue] = entityKeys = new HashSet<IK>();
                }
                if (!entityKeys.Add(entityKey)) {
                    throw new PCollectionException("Entity " + entityKey + " was already in index for '" + ikip.Key + " = " + strNewValue + "'.");
                }
            }

            return true;
        }

        [ClassMember(Description =
            "Returns properties which themselves again have key and value equal to the given key and value.\r\n" +
            "\r\n" +
            "Used indexing if possible (if key is found in index).\r\n" +
            "\r\n"
        )]
        public override IEnumerable<IKIP> GetKeysEqualToValue(IK key, string value) {
            if (!_indices.TryGetValue(key, out var index)) {
                // Index not found for this key. This is normal enough, as not all keys are indexed.

                // NOTE: If we have a huge collection, and the key should have been indexed, but by chance none of the objects contain the key, 
                // NOTE: and therefore the index did not exist, then we will now do a costly O(n)-search in vain:
                return this.Where(ikip => ikip.P.TryGetPV<string>(key, out var v) && v.Equals(value)).Select(ikip => ikip);
            }
            if (!index.TryGetValue(value, out var keys)) {
                // This will probably never happen, because either index will not exist at all (case above), or contain at least one value
                return new List<IKIP>(); // 
            }
            return keys.Select(k =>
                new IKIP(k, TryGetP<IP>(k, out var p, out var errorResponse) ? p : throw new PCollectionException(
                    "Object with key " + k + " was found in index for '" + key + " = " + value + "', but not in data storage.\r\n" +
                    "Index is most probably corrupt / not up-to-date.\r\n" +
                    "Possible cause: Object may have been deleted through " + nameof(PP.Invalid) + " without us knowing about it.\r\n" +
                    "Possible resolution: Restart application / check property stream storage.\r\n"))
            );
        }

        public class PCollectionException : ApplicationException {
            public PCollectionException(string message) : base(message) { }
            public PCollectionException(string message, Exception inner) : base(message, inner) { }
        }
    }

    [Class(Description =
            "Interface stating that implementing class does not use " +
            "the default implementation of -" + nameof(IP.GetKeysEqualToValue) + "- (which does not use -" + nameof(ARConcepts.Indexing) + "-) " +
            "but has replaced it with a more efficient implementation assumed to utilize -" + nameof(ARConcepts.Indexing) + "-.\r\n" +
            "\r\n" +
            "Can be taken as hint to the outside, that -" + nameof(IP.GetKeysEqualToValue) + "- should be used whenever relevant."
        )]
    public interface IGetKeysEqualToValue {
        IEnumerable<IKIP> GetKeysEqualToValue(IK key, string value);
    }

    public interface ITrySetPP {
        bool TrySetPP(IK entityKey, IP? entity, IKIP ikip, out string errorResponse);
    }
}