﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARCCore
{

    [Class(
        Description =
            "Fully flexible storage. Can store anything. Typical inherited for entity representations like 'Customer' or 'Order'.\r\n" +
            "\r\n" +
            "PRich = rich property. Rich in the sense that it can store a wide amount of information, including metadata.\r\n" +
            "\r\n" +
            "Inherently not thread-safe due to using Dictionary and not ConcurrentDictionary as storage.\r\n" +
            "See tips about locking in documentation for -" + nameof(StreamProcessor.OutsideLocalReceiver) + "- and " +
            "some practical code in -" + nameof(ARComponents.ARCDoc) + "-\r\n" +
            "\r\n" +
            "See -" + nameof(PConcurrent) + "- for a (somewhat more) thread-safe alternative.\r\n" +
            "See -" + nameof(PExact<TPropertyKeyEnum>) + "- for a more memory efficient alternative (typically used to store entities like 'Customer', 'Product', 'Order' and so on).\r\n" +
            "See -" + nameof(PValue<TValue>) + "- for how single property values are stored (without metadata), like customer's 'FirstName' and 'LastName'.\r\n" +
            "\r\n" +
            "Probably suitable when you want full flexibility about which metadata to store (see -" + nameof(PP) + "-) and / or want to store child properties.\r\n" +
            "Most probably the one to use for 'ordinary' entities like 'Customer', 'Product', 'Order' and so on (including the collections of such entities), " +
            "whereas -" + nameof(PValue<TValue>) + "- is often used for single values for these instances (like 'FirstName', 'Price' and so on).\r\n" +
            "\r\n" +
            "An instance of this class can also be used as the single total in-memory data storage in your application, " +
            "that is, as the dataStorage parameter to -" + nameof(PropertyStreamLine.ParseAndStore) + "-.\r\n" +
            // TODO: Delete commented out code, although technically correct it probably only confuses:
            //"A typical representation would then consist of dictionaries inside dictionaries, with -" + nameof(IK) + "--keys and either -" + nameof(PRich) + "--values " +
            //"or specific -" + nameof(IP) + "--subclass instances (like 'Customer') values inside those again.\r\n" +
            //"Such a usage would be very typical for a -" + nameof(ARNodeType.Client) + "- but also possible for -" + nameof(ARNodeType.ArmDB) + "- " +
            //"(enabling it to answer -" + nameof(ARConcepts.AdHocQuery) + "- over an TCP/IP connection.\r\n" +
            //"With such a usage the instance of this class would typically be populated by calling -" + nameof(PropertyStreamLine.ParseAndStore) + "-.\r\n" +
            "\r\n",
    LongDescription =
            "Note that in a '-" + nameof(ARConcepts.StandardAgoRapideCode) + "--only application' -" + nameof(IKString) + "- " +
            // TODO: Delete commented out code
            // Irrelevant from 11 Mar 2021, IKType no longer has a FromString method
            // "(or -" + nameof(IKType) + "- but generated from -" + nameof(IKType.FromString) + "-) " +
            "will be used more as key, " +
            "and -" + nameof(PRich) + "- more used as values (instead of more specific -" + nameof(IP) + "--subclass instances (like 'Customer')), " +
            "because the actual types and property keys are not known.\r\n" +
            "Although their names may be known because they can be announced over the property stream, " +
            "the definition for those types are unknown (the types are not present in the assemblies constituting the application).\r\n" +
            "\r\n" +
            "This means that for instance an -" + nameof(ARNodeType.ArmDB) + "- application may not be able to " +
            "validate all data, or answer the most complex queries (because it lacks the type information and has to operate more on strings).\r\n" +
            "\r\n" +
            "TODO: Implement storing of this collection to disk at regular intervals.\r\n" +
            "TOOD: Background: If data are received from -" + nameof(StreamProcessor) + "- through -" + nameof(StreamProcessor.OutsideLocalReceiver) + "-\r\n" +
            "TOOD: (and read by -" + nameof(StreamProcessor) + "- at application startup), then un-needed historical data will accumulate\r\n" +
            "TODO: (for instance if this object stores only the last temperature, but all temperatures are stored to disk).\r\n" +
            "TODO: Solution: Replace disk storage at regular intervals by serializing this object's data to disk with -" + nameof(IP.ToPropertyStream) + "-.\r\n"
    )]
    public class PRich : IP
    {

        [ClassMember(
            Description =
                "Note extreme flexibility when using -" + nameof(IK) + "- as key, although often only -" + nameof(PK) + "- is used for 'ordinary' cases",
            LongDescription =
                "Note how both -" + nameof(PP) + "- information and child-properties are stored in this same directory.\r\n" +
                "\r\n" +
                "NOTE: Reason for chosing Dictionary instead of ConcurrentDictionary is that the latter is much more memory-hungry.\r\n" +
                "NOTE: See -" + nameof(ARConcepts.SingleThreadedCode) + "-."
        )]
        protected Dictionary<IK, IP> Properties { get; } = new Dictionary<IK, IP>();

        /// <summary>
        /// Gives easy access to default interface methods inside of class
        ///  For some discussion about this, see:
        ///  https://stackoverflow.com/questions/57761799/calling-c-sharp-interface-default-method-from-implementing-class
        /// Note: Do not use <see cref="ClassMemberAttribute"/> here, it will only mess up the HTML documentation for AgooRapide with unnecessary links to IP-members.
        /// </summary>
        public IP IP => this;

        public virtual bool TrySetP(IKIP ikip, out string errorResponse)
        {
            if (!ikip.TryAssertTypeIntegrity(out errorResponse))
            {
                return false;
            }
            if (IP.OnTrySetP(ikip))
            {
                errorResponse = "Cancelled by " + nameof(IP.OnTrySetP);
                return false;
            }
            Properties[ikip.Key] = ikip.P;
            errorResponse = null!;
            return true;
        }

        public bool TryRemoveP(IK key, out string errorResponse)
        {
            if (!Properties.TryGetValue(key, out _))
            {
                errorResponse = "Key " + key.ToString() + " not found";
                return false;
            }
            Properties.Remove(key);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description = "Made virtual 13 Jan 2022. Possibly the same could be done for all other methods here since _properties is Protected anyway.")]
        public virtual bool TryGetP<T>(IK key, out T p, out string errorResponse) where T : IP
        {
            if (!Properties.TryGetValue(key, out var ip))
            {
                p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Key " + key.ToString() + " not found";
                return false;
            }

            if (!(ip is T temp))
            {
                p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Key " + key + ": Found type " + ip.GetType().ToStringShort() + ", not " + typeof(T).ToStringShort();
                return false;
            }

            p = temp;

            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        // public IEnumerable<IKIP> GetAllP() => Properties.Select(e => new IKIP(e.Key, e.Value));
        public IEnumerator<IKIP> GetEnumerator()
        {
            foreach (var e in Properties)
            {
                yield return new IKIP(e.Key, e.Value);
            }
        }

        [ClassMember(
            Description =
                "Note that 'normally' this method would be irrelevant for a -" + nameof(PRich) + "- object " +
                "because we will usually be an entity object (like 'Customer') not having an intrinsic value ourselves per se " +
                "(because we will instead be containing a list of values).\r\n" +
                "\r\n" +
                "See -" + nameof(PP.Value) + "- for information about when this method IS relevant (when storing metadata about a property)."
        )]
        public virtual bool TryGetV<TRequest>(out TRequest retval, out string errorResponse) where TRequest : notnull
        {
            if (GetType().Equals(typeof(TRequest)))
            {

                // Special case when not using PValue for packing object...
                // TODO: Ascertain that approach is correct

                // Note, the alternative
                // if (this is TRequest tRequest) { 
                // This would be a naïve approach compared 
                // GetType().Equals(typeof(TRequest)
                // but will not work because TRequest may be typeof(object) if calling method does not know what to ask for

                retval = (TRequest)(object)this;
                errorResponse = null!;
                return true;
            }

            if (!IP.TryGetP<IP>(PP.Value, out var p, out errorResponse))
            {
                errorResponse = this.GetType().ToStringVeryShort() + ": " + errorResponse;
                retval = default!;
                return false;
            }
            return p.TryGetV(out retval, out errorResponse);
        }

        public virtual bool OnTrySetP(IKIP ikip) => false;
        public virtual IEnumerable<IKIP> GetKeysEqualToValue(IK key, string value) => 
            this.Where(ikip => ikip.P.TryGetPV<string>(key, out var v) && v.Equals(value)).Select(ikip => ikip);
        public virtual bool TrySetPP(IK entityKey, IP? entity, IKIP ikip, out string errorResponse) => 
            (entity != null || TryGetP<IP>(entityKey, out entity, out errorResponse)) && entity.TrySetP(ikip, out errorResponse);

        public IP DeepCopy()
        {
            try
            {
                REx.Inc();
                var retval = (IP)System.Activator.CreateInstance(GetType());
                this.ForEach(e =>
                {
                    // Old approach before Jan 28 2022, using AddP (maybe it was chosen because it guards against duplicate inserts).
                    // But since we have just read from a guaranteed non-duplicate context, the guard is unnecessary.
                    // retval.IP.AddP(e.Key, e.P.DeepCopy());

                    // New approach from Jan 28 2022:
                    if (!retval.TrySetP(new IKIP(e.Key, e.P.DeepCopy()), out var errorResponse))
                    {
                        throw new PRichException(
                            "Failed to deep copy key '" + e.Key + "'.\r\n" +
                            "Could not set value of type '" + e.P.GetType().ToStringShort() + "'.\r\n" +
                            "Details:\r\n" +
                            errorResponse
                        );
                    }
                });
                return retval;
            }
            finally
            {
                REx.Dec();
            }
        }

        [ClassMember(Description =
            "TODO: Decide on what we actually want with ToString for this class.\r\n" +
            "TODO: For instance, a list of id-related fields would be useful, or tagging av properties which kind of define the entity"
        )]
        public override string ToString() => GetType().ToStringVeryShort();

        public class PRichException : ApplicationException
        {
            public PRichException(string message) : base(message) { }
            public PRichException(string message, Exception inner) : base(message, inner) { }
        }

    }
}