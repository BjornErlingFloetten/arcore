﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ARCCore
{

    /// Note that this file contains multiple classes, 

    [Class(
        Description =
            "Single value storage.\r\n" +
            "\r\n" +
            "Used for storing single property values (without metadata), like customer's 'FirstName' and 'LastName'.\r\n" +
            "\r\n" +
            "The most memory-efficient 'property'-object to use, when only the actual value of a property is needed.\r\n" +
            "(unable to store any sub-properties or metadata).\r\n" +
            "But see also -" + nameof(PExact<TPropertyKeyEnum>) + "- which stores values directly, " +
            "and uses this class -" + nameof(PValue<TValue>) + "- only for packing and unpacking the values.\r\n" +
            "\r\n" +
            "Immutable (assuming generic type is also immutable).\r\n" +
            "\r\n" +
            "Typically used for storing properties like 'Customer.FirstName', 'Product.Price' and so on.\r\n" +
            "\r\n" +
            "This class supports duck-typing, especially in -" + nameof(TryGetV) + "- which can return " +
            "a basic value type like int, long, double, DateTime, TimeSpan, enum based on a string value, or even convert between values, " +
            "like returning a double if the original -" + nameof(Value) + "- is an int and so on.\r\n" +
            "It is still however recommended to have a strongly typed schema, that is, using -" + nameof(PKTypeAttribute) + "- " +
            "in order to specify the original type of a value.\r\n" +
            "See also -" + nameof(ConvertObjectToString) + "-.\r\n" +
            "\r\n" +
            "When storing of metadata is desired, a more complicated approach is used. " +
            "See -" + nameof(PP.Value) + "- for a more detailed explanation about this" +
            "\r\n" +
            "Using this class should give an overhead per object of 24 bytes (16 for object header, 8 for -" + nameof(Value) + "-), " +
            "in addition to the space needed for the actual object (of type T) stored. " +
            "Note, when calculating space needed for the actual object (of type T) you must remember to include boxing-overhead for value types.\r\n" +
            "See also -" + nameof(ARConcepts.MemoryConsumption) + "-.\r\n" +
            "\r\n" +
            "TODO: Consider creating a PValueVal-class for storing value types. This should get rid of boxing...?\r\n" +
            "TODO: (However, no practical use-case has been seen as of Jul 2020 because of -" + nameof(PExact<TPropertyKeyEnum>) + "- " +
            "TODO: which stores objects directly within an Object[], that is, not using -" + nameof(PValue<TValue>) + "- at all.)\r\n" +
            "\r\n" +
            "See also -" + nameof(ARConcepts.MemoryConsumption) + "-."
    )]
    public class PValue<T> : IEquatable<PValue<T>>, IP where T : notnull
    {

        public T Value { get; private set; }

        private static readonly string Explanation =
            "The reason you are not allowed to do this is that this class only contains a single value (of type " + typeof(T).ToStringShort() + "). " +
            "It is therefore unable to handle more complex operations. ";

        [ClassMember(Description =
            "NOTE: -" + nameof(PropertyStreamLine.TryStore) + "- is explicit aware that -" + nameof(PValue<TValue>) + "- and -" + nameof(PValueEmpty) + "-\r\n" +
            "NOTE: are not able to do -" + nameof(IP.TrySetP) + "-, and will replace these with -" + nameof(PRich) + "- instead as needed.\r\n" +
            "NOTE: See -" + nameof(PP.Value) + "- for more information."
        )]
        public bool TrySetP(IKIP ikip, out string errorResponse)
        {
            errorResponse =
                "Illegal to call " + System.Reflection.MethodBase.GetCurrentMethod().Name + " for this class (" + GetType().ToStringShort() + "). " +
                "Parameters: " + nameof(ikip) + ": " + ikip + ". " + Explanation + "\r\n";
            return false;
        }

        public bool TryRemoveP(IK k, out string errorResponse)
        {
            errorResponse =
                "Irrelevant to call " + System.Reflection.MethodBase.GetCurrentMethod().Name + " for this class (" + GetType().ToStringShort() + "). " +
                "Parameters: " + nameof(k) + ": " + k + ". " + Explanation;
            return false;
        }

        public bool TryGetP<PT>(IK k, out PT p, out string errorResponse) where PT : IP
        {
            p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            errorResponse =
                "Irrelevant to call " + System.Reflection.MethodBase.GetCurrentMethod().Name + " for this class (" + GetType().ToStringShort() + "). " +
                "Parameters: " + nameof(k) + ": " + k + " (type " + typeof(PT).ToStringShort() + "). " + Explanation;
            return false;
        }

        //private static readonly List<IKIP> _emptyListInstance = new List<IKIP>();
        //public IEnumerable<IKIP> GetAllP() => _emptyListInstance;
        public IEnumerator<IKIP> GetEnumerator() { yield break; }

        [ClassMember(Description =
            "Supports duck typing.\r\n" +
            "\r\n" +
            "Attempts to convert value as far as possible, including " +
            "casting double to int or long, " +
            "upgrading int or long to double, or int to long, " +
            "and converting string to int, long, double, DateTime, TimeSpan and enum.\r\n" +
            "\r\n" +
            "See also -" + nameof(ConvertObjectToString) + "-.\r\n" +
            "\r\n" +
            "TODO: Make also able to convert string to -" + nameof(ITypeDescriber) + "-. (if ITypeDescriber.IsAssignableFrom(TRequest))"
        )]
        public bool TryGetV<TRequest>(out TRequest retval, out string errorResponse) where TRequest : notnull
        {

            // if (this is TRequest tRequest) { // Naïve approach will not work because TRequest may be object if calling method does not know what to ask for
            if (GetType().Equals(typeof(TRequest)))
            { // Better approach
                // Special case when not using PValue for packing object...
                // TODO: Ascertain that approach is correct
                retval = (TRequest)(object)this;
                errorResponse = null!;
                return true;
            }

            if (Value is TRequest)
            {
                retval = (TRequest)(object)Value;
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }

            // TODO: This is only a quick-fix used for developing. Involve ITypeDescriber and do it properly as in old AgoRapide!
            if (typeof(TRequest) == typeof(string))
            {
                var t = Value.GetType();
                if (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(List<>)))
                {
                    retval = (TRequest)(object)PropertyStreamLine.EncodeValues(((System.Collections.IList)Value).Cast<object>().Select(v => ConvertObjectToString(v)));
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
                }
                retval = (TRequest)(object)ConvertObjectToString(Value);
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }

            if (typeof(TRequest).Equals(typeof(IEnumerable<string>)) || typeof(TRequest).Equals(typeof(List<string>)))
            {
                // Similar to above but we do not have to encode the values
                var t = Value.GetType();
                if (t.IsGenericType && t.GetGenericTypeDefinition().Equals(typeof(List<>)))
                {
                    /// Typically used when turning <see cref="Cardinality.WholeCollection"/> into separate string items 
                    /// (for instance for an HTML presentation of value)
                    retval = (TRequest)(object)(((System.Collections.IList)Value).Cast<object>()).Select(v => ConvertObjectToString(v)).ToList();
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
                }
            }

            if (typeof(TRequest) == typeof(double))
            { // Upgrade from int or long, or parse from string
                switch (Value)
                {
                    case int _int:
                        retval = (TRequest)(object)System.Convert.ToDouble(_int);
                        errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        return true;
                    case long lng:
                        retval = (TRequest)(object)System.Convert.ToDouble(lng);
                        errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        return true;
                    case string str:
                        if (UtilCore.DoubleTryParse(str, out var dbl))
                        {
                            retval = (TRequest)(object)dbl;
                            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            return true;
                        }
                        retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        errorResponse = "Value '" + str + "' is invalid as double";
                        return false; // Bug fix 10 Mar 2022, was 'return true' before
                }
            }
            else if (typeof(TRequest) == typeof(long))
            { // Cast from double, upgrade from int or parse from string
                switch (Value)
                {
                    case double dbl:
                        try
                        {
                            retval = (TRequest)(object)(long)(dbl);
                            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            return true;
                        }
                        catch (Exception)
                        {
                            retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            errorResponse = "Unable to cast " + dbl + " to " + typeof(TRequest);
                            return false;
                        }
                    case int _int:
                        retval = (TRequest)(object)(long)_int;
                        errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        return true;
                    case string str:
                        if (long.TryParse(str, out var lng))
                        {
                            retval = (TRequest)(object)lng;
                            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            return true;
                        }
                        retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        errorResponse = "Value '" + str + "' is invalid as " + typeof(TRequest);
                        return false; // Bug fix 10 Mar 2022, was 'return true' before
                }
            }
            else if (typeof(TRequest) == typeof(int))
            { // Cast from double or parse from string
                switch (Value)
                {
                    case double dbl:
                        try
                        {
                            retval = (TRequest)(object)(int)(dbl);
                            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            return true;
                        }
                        catch (Exception)
                        {
                            retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            errorResponse = "Unable to cast " + dbl + " to " + typeof(TRequest);
                            return false;
                        }
                    case string str:
                        if (int.TryParse(str, out var _int))
                        {
                            retval = (TRequest)(object)_int;
                            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            return true;
                        }
                        retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        errorResponse = "Value '" + str + "' is invalid as " + typeof(TRequest);
                        return false; // Bug fix 10 Mar 2022, was 'return true' before
                }
            }
            else if (typeof(TRequest) == typeof(DateTime))
            { // Parse from string
                switch (Value)
                {
                    case string str:
                        if (UtilCore.DateTimeTryParse(str, out var dtm))
                        {
                            retval = (TRequest)(object)dtm;
                            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            return true;
                        }
                        retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        errorResponse = "Value '" + str + "' is invalid as " + typeof(TRequest);
                        return false; // Bug fix 10 Mar 2022, was 'return true' before
                }
            }
            else if (typeof(TRequest) == typeof(TimeSpan))
            { // Parse from string
                switch (Value)
                {
                    case string str:
                        if (UtilCore.TimeSpanTryParse(str, out var tsp))
                        {
                            retval = (TRequest)(object)tsp;
                            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            return true;
                        }
                        retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        errorResponse = "Value '" + str + "' is invalid as " + typeof(TRequest);
                        return false; // Bug fix 10 Mar 2022, was 'return true' before
                }
            }
            else if (typeof(TRequest).IsEnum)
            {
                switch (Value)
                {
                    case string str:
                        if (UtilCore.EnumTryParse(typeof(TRequest), str, out var _enum))
                        {
                            retval = (TRequest)(object)_enum;
                            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                            return true;
                        }
                        retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        errorResponse = "Value '" + str + "' is invalid as enum " + typeof(TRequest);
                        return false; // Bug fix 10 Mar 2022, was 'return true' before
                }
            }

            retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            errorResponse = "Unable to translate from " + typeof(T) + " to " + typeof(TRequest) + "\r\n";
            return false;
        }

        [ClassMember(Description =
            "Converts object to string.\r\n" +
            "\r\n" +
            "Uses decimal digits for double with full stop as decimal point, like '1.23'.\r\n" +
            "Uses -" + nameof(Extensions.ToStringDateAndTime) + "- for DateTime objects (but 'yyyy-MM-dd' if no time-of-day is 00:00:00.000').\r\n" +
            "Uses -" + nameof(Extensions.ToStringWithMS) + "- for DateTime objects.\r\n" +
            "Uses -" + nameof(Extensions.ToStringShort) + "- for Type objects.\r\n" +
            "For other object types the general " + nameof(Object.ToString) + " method is used.\r\n" +
            "\r\n" +
            "Note: See also the concept of -FunctionKey- in -" + nameof(ARComponents.ARCQuery) + "- which can also be used for formatting objects.\r\n" +
            "\r\n" +
            "TODO: Add more object types here (or (even better) find a more generic approach to the general issue of converting objects to string in AgoRapide)\r\n" +
            "TODO: AND / OR, Implement some kind of format-description in PK in order to decide string-format\r\n" +
            "TOOD: (the last idea would require PK as a key here).\r\n" +
            "\r\n" +
            "TODO: Move this method out of this generic class (PValue<T>), calling it requires a genetic type parameter now."
        )]
        public static string ConvertObjectToString(object o) =>
            (o) switch
            {
                string s => s,
                double d => d.ToString("0.00").Replace(",", "."), // From Dec 2021: Always use full stop as decimal point,
                DateTime dtm => (dtm.Ticks % TimeSpan.TicksPerDay) == 0 ?
                    dtm.ToString("yyyy-MM-dd") : // This is a pragmatic approach, but not 100% 'correct' in all cases.
                    dtm.ToStringDateAndTime(),
                TimeSpan tsp => tsp.ToStringWithMS(),
                Type typ => typ.ToStringShort(), // Added 9 Mar 2021
                _ => o.ToString()
            };

        [ClassMember(Description = "Note how this class does not have a parameter-less constructor (unlike all other -" + nameof(IP) + "--derived classes in AgoRapide)")]
        public PValue(T value)
        {
            if (value == null)
            {
                // Assertion added 16 Mar 2021.
                // NOTE: Even with <Nullable>enable</Nullable> for the project in which this code resides
                // NOTE: the compiler will still allow potential null value settings.
                throw new ArgumentNullException(nameof(value));
            }
            if (value is IP) throw new PValueException(
                "Illegal to use this class (" + GetType().ToStringShort() + ") for storing an " + nameof(IP) + "-derived value (in this case " + typeof(T).ToStringShort() + ").\r\n" +
                "Resolution: Instead of packing " + typeof(T).ToStringShort() + " inside " + GetType().ToStringShort() + ", store it directly instead.\r\n" +
                "Rationale for this limitation: It makes this class simpler, because it can then just assume that it is 'at-the-end' of a storage hierarchy " +
                "(implementation of -" + nameof(IP) + "- becomes  much less complex).\r\n"
            );
            Value = value;
        }

        [ClassMember(Description = "Do no cache this value, because memory consumption would then increase significantly in your applicationj")]
        public override string ToString() => ConvertObjectToString(Value);

        public IEnumerable<string> ToPropertyStream(IEnumerable<IK>? currentIK = null, List<string>? retval = null, int recurseDepth = int.MaxValue)
        {

            // NOTE: 04 Dec 2020: Limitation below removed. It made little sense and complicated functionality like ARCQuery.NewKeyPropertyStream
            //if (currentIK == null || retval == null) throw new PValueException(
            //    "Illegal to call " + System.Reflection.MethodBase.GetCurrentMethod().Name + " for this class (" + this.GetType().ToStringShort() + ") directly from 'outside' " +
            //    "(that is, as first member of the recursive chain resolving to a property stream item).\r\n" +
            //    "Explanation: This is due to this class not knowning its own key (it only has the " + nameof(Value) + "), due to memory conservation concerns.\r\n" +
            //    "(in other words, this class has no concept of the context within which the value belongs.)\r\n" +
            //    "It is therefore unable to give a meaningful representation as a " + nameof(ARConcepts.PropertyStream) + ".\r\n" +
            //    "Resolution: You must start with calling -" + System.Reflection.MethodBase.GetCurrentMethod().Name + "- from a containing class like -" + nameof(PRich) + "-.\r\n");

            if (currentIK == null)
            {
                // Note that what we return now will not be a "complete" property stream line (we will miss the key-part (the part before ' = '))
                /// NOTE: The problem typically manifests itself when the 'root' object subject to a ToPropertyStream evaluation
                /// NOTE: contains a mix of PRich and PValue.
                currentIK = new List<IK>();
            }
            if (retval == null)
            {
                retval = new List<string>();
            }

            // TODO: This is very inefficient (performance-wise), join'ing together all IK's for each and every value for an 'entity'
            // TODO: Consider passing the join-result as a parameter instead.
            /// TODO: Equivalent issue in both <see cref="PValue{T}"/> and <see cref="PValueEmpty"/>
            retval.Add(PropertyStreamLine.EncodeWholeLine(currentIK, ((IP)this).GetV<string>()));

            return retval; /// Will be ignored, real return-value is in <param name="retval"/>
        }

        public virtual bool OnTrySetP(IKIP ikip) => false;
        public virtual IEnumerable<IKIP> GetKeysEqualToValue(IK key, string value) =>
            this.Where(ikip => ikip.P.TryGetPV<string>(key, out var v) && v.Equals(value)).Select(ikip => ikip);
        public virtual bool TrySetPP(IK entityKey, IP? entity, IKIP ikip, out string errorResponse) =>
            (entity != null || TryGetP<IP>(entityKey, out entity, out errorResponse)) && entity.TrySetP(ikip, out errorResponse);

        [ClassMember(Description =
            "Note that if the generic type (T) is not immutable, then what we return will not be a truly deep copy.\r\n" +
            "\r\n" +
            "This is also noted as a general limitation in the documentation for -" + nameof(IP.DeepCopy) + "-.\r\n"
        )]
        public IP DeepCopy() => this; // We are assumed to be immutable, therefore no problem with returning ourselves

        public override int GetHashCode() => Value.GetHashCode();
        public bool Equals(PValue<T> other) => other.Value.Equals(Value);
        public override bool Equals(object other) => (other is PValue<T> p) && Equals(p);

        public class PValueException : ApplicationException
        {
            public PValueException(string message) : base(message) { }
            public PValueException(string message, Exception inner) : base(message, inner) { }
        }
    }

    [Class(
    Description =
        "Represents the 'no value'.\r\n" +
        "Immutable.\r\n" +
        "Relevant for -" + nameof(Cardinality.IndividualItems) + " where the values are actual keys (stored as -" + nameof(IKString) + "-) " +
        "and we just need a dummy representation where the value would ordinary be placed\r\n"
    )]
    public class PValueEmpty : IP
    {

        private static readonly string Explanation =
            "The reason you are not allowed to do this is that this class simple represents a 'no-value'";

        [ClassMember(Description =
            "NOTE: -" + nameof(PropertyStreamLine.TryStore) + "- is explicit aware that -" + nameof(PValue<TValue>) + "- and -" + nameof(PValueEmpty) + "-\r\n" +
            "NOTE: are not able to do -" + nameof(IP.TrySetP) + "-, and will replace these with -" + nameof(PRich) + "- instead as needed.\r\n" +
            "NOTE: See -" + nameof(PP.Value) + "- for more information."
        )]
        public bool TrySetP(IKIP ikip, out string errorResponse)
        {
            errorResponse =
                "Illegal to call " + System.Reflection.MethodBase.GetCurrentMethod().Name + " for this class (" + GetType().ToStringShort() + "). " +
                "Parameters: " + nameof(ikip) + ": " + ikip + ". " + Explanation + "\r\n";
            return false;
        }

        public bool TryRemoveP(IK k, out string errorResponse)
        {
            errorResponse =
                "Irrelevant to call " + System.Reflection.MethodBase.GetCurrentMethod().Name + " for this class (" + GetType().ToStringShort() + "). " +
                "Parameters: " + nameof(k) + ": " + k + ". " + Explanation;
            return false;
        }

        public bool TryGetP<T>(IK k, out T p, out string errorResponse) where T : IP
        {
            p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            errorResponse = "Irrelevant for this class (" + GetType().ToStringShort() + ")";
            return false;
        }

        public IEnumerator<IKIP> GetEnumerator() { yield break; }

        public bool TryGetV<TRequest>(out TRequest retval, out string errorResponse) where TRequest : notnull
        {
            retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            errorResponse = "Irrelevant for this class (" + GetType().ToStringShort() + ")";
            return false;
        }

        public virtual bool OnTrySetP(IKIP ikip) => false;
        public virtual IEnumerable<IKIP> GetKeysEqualToValue(IK key, string value) =>
            this.Where(ikip => ikip.P.TryGetPV<string>(key, out var v) && v.Equals(value)).Select(ikip => ikip);
        public virtual bool TrySetPP(IK entityKey, IP? entity, IKIP ikip, out string errorResponse) =>
            (entity != null || TryGetP<IP>(entityKey, out entity, out errorResponse)) && entity.TrySetP(ikip, out errorResponse);

        [ClassMember(
            Description =
                "Typicall return value could be something like\r\n" +
                "  Customer/42/PhoneNumber/90534333"
        )]
        public IEnumerable<string> ToPropertyStream(IEnumerable<IK>? currentIK = null, List<string>? retval = null, int recurseDepth = int.MaxValue)
        {
            // NOTE: 04 Dec 2020: Limitation below removed. It made little sense and complicated functionality like ARCQuery.NewKeyPropertyStream
            //if (currentIK == null || retval == null) throw new PValueEmptyException(
            //    "Illegal to call " + System.Reflection.MethodBase.GetCurrentMethod().Name + " for this class (" + GetType().ToStringShort() + ") directly from 'outside' " +
            //    "(that is, as first member of the recursive chain resolving to a property stream item). " +
            //    Explanation + "\r\n"); /// Do not bother with more complicated explanation as done by <see cref="PValue{T}.ToPropertyStream"/>, because it should be obvious why the call is not allowed.

            if (currentIK == null)
            {
                // Note that what we return now will not be a "complete" property stream line (we will miss the key-part, that is, we will just return an empty string now)
                /// NOTE: The problem typically manifests itself when the 'root' object subject to a ToPropertyStream evaluation
                /// NOTE: contains a mix of PRich and PValueEmpty.
                currentIK = new List<IK>();
            }
            if (retval == null)
            {
                retval = new List<string>();
            }

            /// TODO: This is very inefficient (performance-wise), join'ing together all IK's for each and every value for an 'entity'
            /// TODO: Consider passing the join-result as a parameter instead.
            /// TODO: Equivalent issue in both <see cref="PValue{T}"/> and <see cref="PValueEmpty"/>
            retval.Add(PropertyStreamLine.EncodeWholeLine(currentIK, "")); // NOTE: No value in this case.

            return retval; /// Will be ignored, real return-value is in <param name="retval"/>
        }

        private PValueEmpty() { }

        public static PValueEmpty Singleton = new PValueEmpty();

        public class PValueEmptyException : ApplicationException
        {
            public PValueEmptyException(string message) : base(message) { }
            public PValueEmptyException(string message, Exception inner) : base(message, inner) { }
        }

        public IP DeepCopy() => this; // We are immutable, no problem in just returning ourselves
    }
}
