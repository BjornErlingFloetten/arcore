﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

/// <summary>
/// TODO: DOCUMENT THESE FILES MUCH BETTER
/// 
/// Note that file contains:
/// <see cref="ARCCore.PII"/>
/// <see cref="ARCCore.IPII"/>
/// <see cref="ARCCore.IIKII"/>
/// <see cref="ARCCore.IKII{T}"/>
namespace ARCCore {

    [Class(Description =
        "PII = property for storing individual items.\r\n" +
        "\r\n" +
        "Attempt at solving -" + nameof(Cardinality.IndividualItems) + "- storage.\r\n" +
        "\r\n" +
        "Each item is stored as a key in the properties dictionary (with value as -" + nameof(PValueEmpty) + "-).\r\n" +
        "\r\n" +
        "Note that using only a HashSet would be more efficient memory-wise, but then we would loose a future ability to " +
        "store metadata for each key."
    )]
    public class PII : PRich, IPII {

        [ClassMember(Description =
            "Since value is stored in keys, we need to override this method.\r\n" +
            "Note that 'outside' must be aware of how to properly store a value (using -" + nameof(IIKII) + "-).\r\n" +
            "(like -" + nameof(PropertyStreamLine.TryStore) + "-.)"
        )]
        public override bool TryGetV<TRequest>(out TRequest retval, out string errorResponse) {

            if (typeof(TRequest).Equals(typeof(IEnumerable<string>)) || typeof(TRequest).Equals(typeof(List<string>))) {
                retval = (TRequest)(object)Properties.Keys.Select(ik => ik.ToString()).ToList();
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }

            if (!typeof(TRequest).IsGenericType) {
                if (!typeof(TRequest).Equals(typeof(string))) {
                    retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse = "Only generic type parameter TRequest as IEnumerable<> / List<> is supported (in addition to String)";
                    return false;
                }
                retval = (TRequest)(object)PropertyStreamLine.EncodeValues(Properties.Keys.Select(ik => ik.ToString()));
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }

            var genericTypeDefinition = typeof(TRequest).GetGenericTypeDefinition();
            if (!genericTypeDefinition.Equals(typeof(IEnumerable<>)) && !genericTypeDefinition.Equals(typeof(List<>))) {
                retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Only generic type parameter TRequest as IEnumerable<> / List<> is supported (in addition to String)";
                return false;
            }
            var genericTypeParameter = typeof(TRequest).GetGenericArguments()[0];

            // Create list of correct type
            var list = (System.Collections.IList)Activator.CreateInstance(
                typeof(List<>).GetGenericTypeDefinition().MakeGenericType(new Type[] { genericTypeParameter })
            );
            foreach (var ik in Properties.Keys) {
                if (!(ik is IIKII iikii)) {
                    retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse = "Key was not of type " + typeof(IIKII).ToStringShort() + " but " + ik.GetType().ToStringShort();
                    return false;
                }
                if (!genericTypeParameter.IsAssignableFrom(iikii.ObjValue.GetType())) {
                    /// Item does not match. 

                    /// TODO: Add automatic conversion like the one used in 
                    /// TODO: <see cref="PValue{T}.TryGetV"/>

                    retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    errorResponse = "Found item of type " + iikii.ObjValue.GetType().ToStringShort() + ", required type was " + genericTypeParameter.ToStringShort();
                    return false;
                }
                list.Add(iikii.ObjValue);
            }
            retval = (TRequest)list;
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }
    }

    [Class(Description =
        "Future expansion, using different classes for storing, not only -" + nameof(PII) + "-."
    )]
    public interface IPII {
    }

    [Class(Description = "Related to -" + nameof(PII) + "-, property for storing individual items")]
    public interface IIKII : IK {
        public object ObjValue { get; }

        [ClassMember(Description = "This method corresponds to -" + nameof(PK.PackParseResultForStorageInEntityObject) + "-.")]
        public static IIKII PackParseResultForStorageInIKII(ParseResult parseResult) =>
            PackObjectForStorageInIKII(parseResult.Result ?? throw new NullReferenceException(
                nameof(parseResult.Result) + ": This method can only be called for a valid " + nameof(parseResult) + "\r\n"));

        [ClassMember(Description = "This method corresponds to -" + nameof(PK.PackObjectForStorageInEntityObject) + "-.")]
        public static IIKII PackObjectForStorageInIKII(object o) =>
            (IIKII)Activator.CreateInstance(
                typeof(IKII<>).GetGenericTypeDefinition().MakeGenericType(new Type[] { o.GetType() }), // Create generic type IKII<T>
                new object[] { o } // Generic parameter to IKII<T>
        );
    }

    public class IKII<T> : IK, IIKII where T : notnull {
        public T Value { get; set; }
        public object ObjValue => Value;

        public IKII(T value) => Value = value;

        public override int GetHashCode() => ToString().GetHashCode();
        public bool Equals(IK other) => ToString().Equals(other.ToString());
        public override bool Equals(object other) => other is IK ik && Equals(ik);

        public override string ToString() => PValue<TValue>.ConvertObjectToString(Value);
    }
}
