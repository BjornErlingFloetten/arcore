﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using System.Linq;

namespace ARCCore {

    [Class(
        Description =
            "A (somewhat) thread-safe alternative to -" + nameof(PRich) + "- which also supports -" + nameof(ARConcepts.ExposingApplicationState) + "- (through -" + nameof(ARCCore.IP.Logger) + "-).\r\n" +
            "\r\n" +
            "With 'somewhat' thread-safe we mean that thread-safety is only guaranteed for writing / reading key and value pairs directly stored within this class, " +
            "not for writing / reading to objects further down in the hierarchical storage.\r\n" +
            "\r\n" +
            "Use this base class when you want to -" + nameof(ARConcepts.ExposingApplicationState) + "- " +
            "throughout the -" + nameof(ARConcepts.PropertyStream) + "-.\r\n" +
            "(in other words, use this class as a base class for all parts of your application that do some kind of processing.)\r\n" +
            "\r\n" +
            "By setting the -" + nameof(Logger) + "-, every call to -" + nameof(TrySetP) + "- will be logged.\r\n" +
            "\r\n" +
            "In addition the metods -" + nameof(IP.Log) + "- and -" + nameof(IP.HandleException) + "- get more meaningful, " +
            "as they will actually distribute the information somewhere.\r\n" +
            "\r\n" +
            "The practical consequence of this is that you do not have to write log-code like this:\r\n" +
            "  var c = new Connection()\r\n" +
            "  c.Id =\"My connection\"\r\n" +
            "  log(\"Created new connection with id \" + c.Id)\r\n" +
            "\r\n" +
            "See also -" + nameof(ARConcepts.ExposingApplicationState) + "-." +
            "\r\n" +
            "Note that more memory-hungry due to use of ConcurrentDictionary",
        LongDescription =
            "Note that this class is normally meaningless to use as base-class for data-objects like 'Customer', 'Order' or similar, because they are by their " +
            "nature meant to be distributed over the -" + nameof(ARConcepts.PropertyStream) + "- anyway, so changes to them will 'automatically' " +
            "be logged (that is, sent to the -" + nameof(ARConcepts.PropertyStream) + "-).\r\n" +
            "\r\n" +
            "Some inheriting classes in -" + nameof(ARComponents.ARCCore) + "- are -" + nameof(StreamProcessor) + "- and -" + nameof(ActualConnection) + "-."
    )]
    public class PConcurrent : IP {

        /// <summary>
        /// Gives easy access to default interface methods inside of class
        ///  For some discussion about this, see:
        ///  https://stackoverflow.com/questions/57761799/calling-c-sharp-interface-default-method-from-implementing-class
        /// Note: Do not use <see cref="ClassMemberAttribute"/> here, it will only mess up the HTML documentation for AgooRapide with unnecessary links to IP-members.
        /// </summary>
        public IP IP => this;

        [ClassMember(
            Description =
                "Note extreme flexibility when using -" + nameof(IK) + "- as key, although often only -" + nameof(PK) + "- is used for 'ordinary' cases",
            LongDescription =
                "Note how both -" + nameof(PP) + "- information and child-properties are stored in this same directory.\r\n" +
                "\r\n" +
                "NOTE: ConcurrentDictionary is very memory hungry. Consider using -" + nameof(PRich) + "- if you have a huge amount of instances."
        )]
        protected ConcurrentDictionary<IK, IP> Properties { get; } = new ConcurrentDictionary<IK, IP>();

        private Action<string>? _logger;
        public Action<string>? Logger {
            get => _logger;
            set => _logger = value;
        }

        public bool TrySetP(IKIP ikip, out string errorResponse) {
            if (!ikip.TryAssertTypeIntegrity(out errorResponse)) {
                return false;
            }
            if (IP.OnTrySetP(ikip)) {
                errorResponse = "Cancelled by " + nameof(IP.OnTrySetP);
                return false;
            }
            Properties[ikip.Key] = ikip.P;

            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            // Setting av property successful, continue with eventual logging

            if (Logger != null) {
                if (ikip.Key is PK pk) {
                    if (pk.TryGetA<PKLogAttribute>(out var l)) {
                        if (l.IP.GetPV(PKLogAttributeP.DoNotLogAtAll, false)) {
                            return true;
                        }
                        if (l.IP.TryGetPV<long>(PKLogAttributeP.CountInterval, out _)) throw new NotImplementedException(nameof(PKLogAttributeP.CountInterval) + " for " + ikip.Key);
                        if (l.IP.TryGetPV<long>(PKLogAttributeP.TimeInterval, out _)) throw new NotImplementedException(nameof(PKLogAttributeP.TimeInterval) + " for " + ikip.Key);
                    }
                    /// TODO: Implement <see cref="PKLogAttributeP.CountInterval"/> and <see cref="PKLogAttributeP.TimeInterval"/>
                }
                ikip.P.ToPropertyStream(
                    currentIK: new List<IK> { ikip.Key },
                    retval: new List<string>() // Necessary to initialize in order for PValue not to throw an exception...
                ).ForEach(s =>
                    /// Note that if the original logging call parameter constitued only a single string (the 'normal' situation) then 
                    /// only a single <see cref="PropertyStreamLine"/> will be generated now. 
                    /// If however the parameter was of an IP-type, like PRich, then multiple lines may result now.
                    Logger(s)
                );
            }
            return true;
        }

        public bool TryRemoveP(IK key, out string errorResponse) {
            if (!Properties.TryRemove(key, out _)) {
                errorResponse = "Key " + key.ToString() + " not found";
                return false;
            }            
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public virtual bool TryGetP<T>(IK key, out T p, out string errorResponse) where T : IP {
            if (!Properties.TryGetValue(key, out var ip)) {
                p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Key " + key.ToString() + " not found";
                return false;
            }
            if (!(ip is T temp))
            {
                p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Key " + key + ": Found type " + ip.GetType().ToStringShort() + ", not " + typeof(T).ToStringShort();
                return false;
            }
            p = temp;
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        // TODO: Implement other equivalent methods / overloads towards ConcurrentDictionary
        public IP GetOrAdd(IK key, Func<IK, IP> valueFactory) => Properties.GetOrAdd(key, valueFactory);
        // TODO: Implement other equivalent methods / overloads towards ConcurrentDictionary
        public IP GetOrAdd(IK key, IP p) => Properties.GetOrAdd(key, p);
        // TODO: Implement other equivalent methods / overloads towards ConcurrentDictionary
        public void AddOrUpdate(IK key, Func<IK, IP> addValueFactory, Func<IK, IP, IP> updateValueFactory) => Properties.AddOrUpdate(key, addValueFactory, updateValueFactory);

        // public IEnumerable<IKIP> GetAllP() => Properties.Select(e => new IKIP(e.Key, e.Value));
        public IEnumerator<IKIP> GetEnumerator() {
            foreach (var e in Properties) {
                yield return new IKIP(e.Key, e.Value);
            }
        }

        public bool TryGetV<TRequest>(out TRequest retval, out string errorResponse) where TRequest : notnull {
            /// Due to a desire for a 'safe' poking of properties, for instance by <see cref="ARComponents.ARCDoc"/>, we can not be this strict:
            // throw new PConcurrentException("Irrelevant for " + GetType().ToStringShort() + " because this class does not have a value of itself, it is a collection of values");
            retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            errorResponse = "Irrelevant for " + GetType().ToStringShort() + " because this class does not have a value of itself, it is a collection of values";
            return false;
        }

        public virtual bool OnTrySetP(IKIP ikip) => false;
        public virtual IEnumerable<IKIP> GetKeysEqualToValue(IK key, string value) => 
            this.Where(ikip => ikip.P.TryGetPV<string>(key, out var v) && v.Equals(value)).Select(ikip => ikip);
        public virtual bool TrySetPP(IK entityKey, IP? entity, IKIP ikip, out string errorResponse) => 
            (entity != null || TryGetP<IP>(entityKey, out entity, out errorResponse)) && entity.TrySetP(ikip, out errorResponse);

        public IP DeepCopy() {
            try {
                REx.Inc();
                var retval = (IP)System.Activator.CreateInstance(GetType());
                this.ForEach(e =>
                {
                    // Old approach before Jan 28 2022, using AddP (maybe it was chosen because it guards against duplicate inserts).
                    // But since we have just read from a guaranteed non-duplicate context, the guard is unnecessary.
                    // retval.IP.AddP(e.Key, e.P.DeepCopy());

                    // New approach from Jan 28 2022:
                    if (!retval.TrySetP(new IKIP(e.Key, e.P.DeepCopy()), out var errorResponse))
                    {
                        throw new PConcurrentException(
                            "Failed to deep copy key '" + e.Key + "'.\r\n" +
                            "Could not set value of type '" + e.P.GetType().ToStringShort() + "'.\r\n" +
                            "Details:\r\n" +
                            errorResponse
                        );
                    }
                });
                return retval;
            } finally {
                REx.Dec();
            }
        }

        [ClassMember(Description =
            "TODO: Decide on what we actually want with ToString for this class.\r\n" +
            "TODO: For instance, a list of id-related fields would be useful, or tagging av properties which kind of define the entity"
        )]
        public override string ToString() => GetType().ToStringVeryShort();

        public class PConcurrentException : ApplicationException {
            public PConcurrentException(string message) : base(message) { }
            public PConcurrentException(string message, Exception inner) : base(message, inner) { }
        }
    }

    [Enum(
        AREnumType = AREnumType.PropertyKeyEnum,
        Description = "Describes class -" + nameof(PConcurrent) + "-."
    )]
    public enum PConcurrentP {
        __invalid,

        [PKType(Type = typeof(DateTime))]
        Heartbeat
    }
}