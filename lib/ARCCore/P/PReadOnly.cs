﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARCCore
{

    [Class(Description =
        "Experimental (from Feb 2022) class suitable for classes that do not need to be deserialized, " +
        "or to accept write requests through -" + nameof(ARConcepts.PropertyAccess) + "-.\r\n" +
        "In addition, all read request are only offered through -" + nameof(GetEnumerator) + "-.\r\n" +
        "\r\n" +
        "This is both a low-memory usage and high-performance class.\r\n" +
        "(memory usage is the most promising advantage of this class, performance did not turn out to be significant changed).\r\n" +
        "\r\n" +
        "Abstract because else class would give little meaning.\r\n" +
        "\r\n" +
        "Not usable in conjunction with -" + nameof(PropertyStreamLine.TryParseAndStore) + "- (not usable for deserialization).\r\n" +
        "\r\n" +
        "Useful in conjunction with -" + nameof(ARComponents.ARCQuery) + "-s concept of -EntityMethodKey-.\r\n"
    )]
    public abstract class PReadOnly : IP
    {
        public IP DeepCopy()
        {
            throw new NotImplementedException();
        }

        public abstract IEnumerator<IKIP> GetEnumerator();

        public IEnumerable<IKIP> GetKeysEqualToValue(IK key, string value)
        {
            throw new NotImplementedException();
        }

        public bool OnTrySetP(IKIP ikip)
        {
            throw new NotImplementedException();
        }

        public virtual bool TryGetP<T>(IK key, out T p, out string errorResponse) where T : IP
        {
            p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            errorResponse = "Attempt to read key '" + key + "' (type " + typeof(T).ToStringShort() + "): TryGetP is not supported by PReadOnly (" + GetType().ToStringShort() + "), use GetEnumerator instead";
            return false;
        }

        public virtual bool TryGetV<T>(out T retval, out string errorResponse) where T : notnull
        {
            retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            errorResponse = "TryGetV is not supported by PReadOnly (" + GetType().ToStringShort() + "), use GetEnumerator instead";
            return false;
        }

        public bool TryRemoveP(IK key, out string errorResponse)
        {
            throw new NotImplementedException();
        }

        public bool TrySetP(IKIP ikip, out string errorResponse)
        {
            throw new NotImplementedException();
        }

        public bool TrySetPP(IK entityKey, IP? entity, IKIP ikip, out string errorResponse)
        {
            throw new NotImplementedException();
        }
    }
}
