﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using System.Linq;

namespace ARCCore
{

    [Class(
        Description =
            "Very -" + nameof(ARConcepts.MemoryConsumption) + "- efficient alternative to -" + nameof(PRich) + "-.\r\n" +
            "Typically used to store entities like 'Customer', 'Product', 'Order' and so on\r\n" +
            "\r\n" +
            "Has some limitations in the sense that can only store an exact number of properties " +
            "(assumed to come from one specific -" + nameof(AREnumType.PropertyKeyEnum) + "-), " +
            "and only use -" + nameof(PK) + "- as key.\r\n" +
            "\r\n" +
            "Values are stored 'directly', that is, without packing inside a -" + nameof(PValue<TValue>) + "-. " +
            "(in other words, values are unpacked as they are received (by call to -" + nameof(IP.GetV) + "-), " +
            "and packed again (by -" + nameof(PK.PackObjectForStorageInEntityObject) + "-) before being sent.\r\n" +
            "If -" + nameof(PValueEmpty) + "- is received from outside, it is treated as 'not-existing' (stored internally as null)\r\n" +
            "\r\n" +
            "Note that from 22 Jan 2022 " +
            "PExact contains the following high performance alternatives to standard -" + nameof(ARConcepts.PropertyAccess) + "-:\r\n" +
            "-" + nameof(SetPVDirect) + "-, -" + nameof(GetPVDirect) + "- and -" + nameof(TryGetPVDirect) + "-.\r\n" +
            "This avoids the performance overhead of unpacking and packing as described above.\r\n" +
            "\r\n" +
            "TODO: The high-performance alternatives describes above " +
            "TODO: are not described in -" + nameof(ARComponents.ARCDoc) + "- yet (see -Documentator-).\r\n" +
            "TODO: One could argue that they should make PExact perform faster than PRich (in addition to using less memory as already mentioned),\r\n" +
            "TODO: but this has not been tested properly as of Jan 2022.\r\n" +
            "TODO: (Initial evaluation did not show any dramatic change, or actually any change at all).\r\n" +
            "TODO: It is possible that the necessary boxing / unboxing is what impacts performance most. For value types\r\n" +
            "TODO: it could quite possibly be more efficient to actually store PValue directly, in order to avoid boxing / unboxing.\r\n" +
            "TODO: https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/types/boxing-and-unboxing \r\n" +
            "\r\n" +
            "Use this class as basis for your 'entity' classes when you know exact what you want to store in them and " +
            "when you do not need to store any meta-data like -" + nameof(PP.Cid) + "-, -" + nameof(PP.Created) + "- for each property.\r\n" +
            "\r\n" +
            "The only memory overhead of this class compared to a 'traditional' C# class (with ordinary properties) " +
            "should be the overhead for the -" + nameof(Storage) + "- array instance (24 extra bytes).\r\n" +
            "(with the exception of boxing / unboxing for value types of course.).\r\n" +
            "\r\n" +
            "See also -" + nameof(ARConcepts.MemoryConsumption) + "-.\r\n" +
            "\r\n" +
            "See -" + nameof(ARComponents.ARCDoc) + "-, class -Orange- for example implementation.\r\n" +
            "\r\n" +
            "NOTE: Metadata at the entity level, like -" + nameof(PP.Cid) + "- and -" + nameof(PP.Created) + "- is only \r\n" +
            "NOTE: allowed as long as corresponding enum-members for the specific generic type parameter EnumType exists.\r\n" +
            "\r\n" +
            "See also -" + nameof(ICreated) + ", a dummy interface signifying that implementing class has the ability to store (in-memory) metadata -" + nameof(PP.Created) + "-\r\n" +
            "\r\n" +
            "TODO: This class is currently (as of Mar 2021) unable to store properties of type IP, that is, when packing / unpacking is not needed.\r\n" +
            "TODO: Some workarounds could be to explicit recognize PValue<T> / PValueEmpty as the ones to be unpacked, and\r\n" +
            "TODO: store all other properties (of IP) directly.\r\n",
        LongDescription =
            "Some examples of advantages of this class compared to -" + nameof(PRich) + "-:\r\n" +
            "\r\n" +
            "Example 1: A small sample of about 832 000 -" + nameof(PropertyStreamLine) + "- " +
            "with data for about 16 000 customers (three properties, first name, last name and email) and 43 000 payments (about 21 properties each).\r\n" +
            "42% reduction in RAM usage and a 16% reduction in CPU time for initial reading of all data into memory.\r\n" +
            "Dataset occupied 43 MB on disk and 59 MB in RAM.\r\n" +
            "(Note: Example given is without any speed performance improvement through avoiding the pack / unpack cycle. " +
            "(see speed improvements done on Jan 17 2022))\r\n" +
            "\r\n"
    )]
    public abstract class PExact<TEnumType> : IP where TEnumType : struct, Enum
    {

        /// <summary>
        /// Gives easy access to default interface methods inside of class
        ///  For some discussion about this, see:
        ///  https://stackoverflow.com/questions/57761799/calling-c-sharp-interface-default-method-from-implementing-class
        /// Note: Do not use <see cref="ClassMemberAttribute"/> here, it will only mess up the HTML documentation for AgooRapide with unnecessary links to IP-members.
        /// </summary>
        public IP IP => this;

        [ClassMember(Description =
            "Uses int-value of -" + nameof(PK.__enum) + "- minus 1 as index.\r\n" +
            "\r\n" +
            "Use from outside with care."
        )]
        public object?[] Storage { get; private set; }

        [ClassMember(Description =
            "High performance alternative to -" + nameof(TrySetP) + "-, accessing -" + nameof(Storage) + "- more direct.\r\n" +
            "\r\n" +
            "Avoids unpacking of value.\r\n" +
            "\r\n" +
            "PExact contains the following high performance alternatives to standard -" + nameof(ARConcepts.PropertyAccess) + "-:\r\n" +
            "-" + nameof(SetPVDirect) + "-, -" + nameof(GetPVDirect) + "- and -" + nameof(TryGetPVDirect) + "-.\r\n"
        )]
        public void SetPVDirect(TEnumType key, object value)
        {
            Storage[(int)(object)key - 1] = value;
        }

        [ClassMember(Description =
            "High performance alternative to -" + nameof(TryGetP) + "-, accessing -" + nameof(Storage) + "- more direct.\r\n" +
            "\r\n" +
            "Avoids packing of value.\r\n" +
            "\r\n" +
            "PExact contains the following high performance alternatives to standard -" + nameof(ARConcepts.PropertyAccess) + "-:\r\n" +
            "-" + nameof(SetPVDirect) + "-, -" + nameof(GetPVDirect) + "- and -" + nameof(TryGetPVDirect) + "-.\r\n"
        )]
        public T GetPVDirect<T>(TEnumType key) where T : notnull =>
            Storage[(int)(object)key - 1] is T t ? t : (
            new Func<T>(() =>
            {
                var o = Storage[(int)(object)key - 1];
                if (o is null) throw new NullReferenceException("No value set for " + key.GetType().ToStringShort() + "." + key);
                throw new InvalidObjectTypeException(o, typeof(T), "Key: " + key.GetType().ToStringShort() + "." + key);
            })()
        );

        [ClassMember(Description =
            "High performance alternative to -" + nameof(TryGetP) + "-, accessing -" + nameof(Storage) + "- more direct.\r\n" +
            "\r\n" +
            "Avoids packing of value.\r\n" +
            "\r\n" +
            "PExact contains the following high performance alternatives to standard -" + nameof(ARConcepts.PropertyAccess) + "-:\r\n" +
            "-" + nameof(SetPVDirect) + "-, -" + nameof(GetPVDirect) + "- and -" + nameof(TryGetPVDirect) + "-.\r\n"
        )]
        public bool TryGetPVDirect<T>(TEnumType key, out T value, out string errorResponse) where T : notnull
        {
            if (Storage[(int)(object)key - 1] is T t)
            {
                value = t;
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
            var o = Storage[(int)(object)key - 1];
            if (o is null)
            {
                errorResponse = "No value set for " + key.GetType().ToStringShort() + "." + key;
            }
            else
            {
                errorResponse = "Can not convert " + o.GetType().ToStringShort() + " to " + typeof(T).ToStringShort() + ". Key: " + key.GetType().ToStringShort() + "." + key;
            }
            value = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return false;
        }


        [ClassMember(Description = "Caches the number of members in a given enum (minus the __invalid-value)")]
        private static readonly ConcurrentDictionary<Type, int> _capacities = new ConcurrentDictionary<Type, int>();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="capacity">
        /// Set capacity to the count of members in the <see cref="TEnumType"/> minus the __invalid value.<br>
        /// This parameter is optional but gives a slight performance increase if given.<br>
        /// (because it would eliminate a lookup in <see cref="_capacities"/> each and every time the constructor is called).
        /// </param>
        [ClassMember(Description = "" +
            "Set capacity to the number of members in the -" + nameof(AREnumType.PropertyKeyEnum) + "- for this class " +
            "(minus the __invalid-value)"
        )]
        public PExact(int? capacity)
        {
            if (capacity == null)
            {
                // Note a slight performance impact now. Therefore it is better to explicit state capacity in sub class.
                capacity = _capacities.GetOrAdd(typeof(TEnumType), t => UtilCore.EnumGetMembers<TEnumType>().Count);
            }
            Storage = new object[capacity.Value];
        }

        /// <summary>
        /// See constructor with int-parameter (<see cref="PExact{EnumType}.PExact(int?)"/>) for documentation.
        /// </summary>
        public PExact() : this(capacity: null) { }

        public IP DeepCopy()
        {
            try
            {
                REx.Inc();
                // Old inefficient approach before Jan 28 2022, 
                //var retval = (IP)System.Activator.CreateInstance(GetType());
                //this.ForEach(e =>
                //{
                //    // Using AddP (maybe it was chosen because it guards against duplicate inserts).
                //    // But since we have just read from a guaranteed non-duplicate context, the guard is unnecessary.
                //    retval.AddP(e.Key, e.P.DeepCopy());
                //});

                // New radically more efficient approach from 28 Jan 2022:
                var retval = (PExact<TEnumType>)System.Activator.CreateInstance(GetType());
                for (var i = 0; i < Storage.Length; i++)
                {
                    var o = Storage[i];
                    if (o is IP ip)
                    {
                        retval.Storage[i] = ip.DeepCopy();
                    }
                    else
                    {
                        /// Note that if the generic type (T) is not immutable, then what we return will not be a truly deep copy
                        /// This is also noted as a general limitation in the documentation for <see cref="IP.DeepCopy"/>
                        retval.Storage[i] = o;
                    }
                }
                return retval;
            }
            finally
            {
                REx.Dec();
            }
        }

        public IEnumerator<IKIP> GetEnumerator()
        {
            foreach (var i in Enumerable.Range(start: 0, count: Storage.Length).Where(i => Storage[i] != null))
            {
                var pk = PK.FromEnum((TEnumType)(object)(i + 1));
                yield return new IKIP(
                    k: pk,
                    p: pk.PackObjectForStorageInEntityObject(Storage[i]!) /// Note that null-check done in Where-part. Note how <see cref="PValueEmpty"/> does not get packed now.
                    );
            }
        }

        public bool TrySetP(IKIP ikip, out string errorResponse)
        {
            if (!TryConvertIKToValidStorageIndex(ikip.Key, out var index, out errorResponse))
            {
                return false;
            }
            if (!ikip.TryAssertTypeIntegrity(out errorResponse))
            {
                // Note: Assertion added 10 Feb 2021
                return false;
            }
            if (ikip.P is PValueEmpty)
            {
                if (IP.OnTrySetP(ikip))
                {
                    errorResponse = "Cancelled by " + nameof(IP.OnTrySetP);
                    return false;
                }
                Storage[index] = ikip.P;
            }
            else if (ikip.P is IPII)
            {
                if (IP.OnTrySetP(ikip))
                {
                    errorResponse = "Cancelled by " + nameof(IP.OnTrySetP);
                    return false;
                }
                Storage[index] = ikip.P;
            }
            else
            {
                // TODO: This is incompatible with when ikip.P's "type" is actually of type IP, that is, when there is no need for packing / unpacking
                if (!ikip.P.TryGetV<object>(out var obj, out errorResponse))
                {
                    // Change 29 nov 2021, assume that failed because of the comment above (P's "type" is actually of type IP).
                    obj = ikip.P;

                    //errorResponse = "TrySetP failed because TryGetV from " + ikip.P.GetType() + " failed with " + nameof(errorResponse) + ": " + errorResponse;
                    //return false;
                }
                if (IP.OnTrySetP(ikip))
                {
                    errorResponse = "Cancelled by " + nameof(IP.OnTrySetP);
                    return false;
                }
                Storage[index] = obj;
            }
            return true;
        }

        public bool TryRemoveP(IK key, out string errorResponse)
        {
            if (!TryConvertIKToValidStorageIndex(key, out var index, out errorResponse))
            {
                return false;
            }
            if (Storage[index] == null)
            {
                errorResponse = "Key " + key.ToString() + " not found";
                return false;
            }
            Storage[index] = null;
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public bool TryGetP<T>(IK key, out T p, out string errorResponse) where T : IP
        {
            if (!(typeof(IP).Equals(typeof(T))))
            {
                /// Since we use <see cref="PK.PackObjectForStorageInEntityObject"/>
                /// we are only able to return IP here.
                errorResponse = nameof(TryGetP) + ": Only relevant for IP, not " + typeof(T).ToStringShort();
            }

            if (!TryConvertIKToPK(key, out var pk, out errorResponse))
            {
                p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            if (!TryConvertIKToValidStorageIndex(pk, out var index, out errorResponse))
            {
                // TODO: Note that duplicates work already done by TryConvertIKToPK. 
                // TODO: Let TryConvertIKToValidStorageIndex also return pk
                p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            var retval = Storage[index];
            if (retval == null)
            {
                p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Not found (null value found in array)";
                return false;
            }

            var ip = pk.PackObjectForStorageInEntityObject(retval);
            if (!(ip is T t)) throw new InvalidObjectTypeException(ip, typeof(T), 
                "Generic type parameter T was checked at start of method that equals type of IP, so this should not have happened");
            p = t;
            return true;
        }

        public virtual bool TryGetV<T>(out T retval, out string errorResponse) where T : notnull
        {
            retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            errorResponse = "Irrelevant for " + GetType().ToStringShort() + " because this class does not have a value of itself, it is a collection of values";
            return false;
        }

        public virtual bool OnTrySetP(IKIP ikip) => false;
        public virtual IEnumerable<IKIP> GetKeysEqualToValue(IK key, string value) =>
            this.Where(ikip => ikip.P.TryGetPV<string>(key, out var v) && v.Equals(value)).Select(ikip => ikip);
        public virtual bool TrySetPP(IK entityKey, IP? entity, IKIP ikip, out string errorResponse) =>
            (entity != null || TryGetP<IP>(entityKey, out entity, out errorResponse)) && entity.TrySetP(ikip, out errorResponse);

        private static readonly string _convertIKToPKExplanation =
            "This class (" + typeof(PExact<TEnumType>).ToStringShort() + ") can only use -" + nameof(PK) + "- as keys (from enum -" + typeof(TEnumType).ToStringShort() + "-).\r\n" +
            "More specifically, one of the values\r\n" +
            string.Join(", ", UtilCore.EnumGetMembers<TEnumType>().Select(e => "-" + e.ToString() + "-")) + /// Note use of <see cref="ARConcepts.LinkInsertionInDocumentation"/>
            "\r\n";

        private PK ConvertIKToPK(IK ik) => TryConvertIKToPK(ik, out var retval, out var errorResponse) ? retval : throw new InvalidObjectTypeException(ik, typeof(PK), errorResponse);
        private bool TryConvertIKToPK(IK ik, out PK pk, out string errorResponse)
        {
            if (!(ik is PK pkTemp))
            {

                // See if can parse as required enum. This is not very efficient, but then calling method
                // may simply not have access to the enum. It could be a generic query expression for instance like "ORDER BY FirstName".
                if (UtilCore.EnumTryParse<TEnumType>(ik.ToString(), out var e))
                {
                    pk = PK.FromEnum(e);
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
                }

                pk = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse =
                    "Key given was of type " + ik.GetType().ToStringShort() + " with value '" + ik.ToString() + "'.\r\n" +
                    "\r\n" +
                    _convertIKToPKExplanation;
                //"But this class (" + GetType().ToStringShort() + ") can only use -" + nameof(PK) + "- as keys (from enum " + typeof(EnumType).ToStringShort() + "). " +
                //"More specifically, one of the values\r\n" +
                //string.Join(", ", Util.EnumGetMembers<EnumType>().Select(e => e.ToString()));
                return false;
            }
            if (!pkTemp.__enum.GetType().Equals(typeof(TEnumType)))
            {

                /// See if can translate (would typically mean translating from metadata <see cref="PP"/>)
                if (UtilCore.EnumTryParse<TEnumType>(pkTemp.__enum.ToString(), out var e))
                {
                    pk = PK.FromEnum(e);
                    errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return true;
                }
                pk = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse =
                    "Key given was of type " + pkTemp.GetType().ToStringShort() + " with value '" + pkTemp.__enum.GetType().ToStringShort() + "." + pkTemp.__enum + "'.\r\n" +
                    "\r\n" +
                    _convertIKToPKExplanation;
                //"But this class (" + GetType().ToStringShort() + ") can only use -" + nameof(PK) + "- as keys (from enum " + typeof(EnumType).ToStringShort() + "). " +
                //"More specifically, one of the values\r\n" +
                //string.Join(", ", Util.EnumGetMembers<EnumType>().Select(e => e.ToString()));
                return false;
            }
            pk = pkTemp;
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        private int ConvertIKToValidStorageIndex(IK ik) => TryConvertIKToValidStorageIndex(ik, out var retval, out var errorResponse) ? retval : throw new PExactException(errorResponse);
        [ClassMember(Description = "Converts to index valid for -" + nameof(Storage) + "-.")]
        private bool TryConvertIKToValidStorageIndex(IK ik, out int index, out string errorResponse)
        {
            if (!TryConvertIKToPK(ik, out var pk, out errorResponse))
            {
                index = -1;
                return false;
            }
            index = (int)(pk.__enum) - 1;
            if (index >= Storage.Length)
            {
                var m = UtilCore.EnumGetMembers(typeof(TEnumType));
                errorResponse =
                    "Invalid index (" + index + ") resulted for key " + pk.ToString() + ".\r\n" +
                    "Capacity (number of storage elements) is " + Storage.Length + " (index from 0 to " + (Storage.Length - 1) + ")\r\n" +
                    "while the actual enum " + typeof(TEnumType).ToStringShort() + "." + pk.__enum + " has a value  of " + index + ".\r\n" +
                    "Resolution: Change capacity value at call to constructor for this object.\r\n" +
                    "The correct capacity value to use is probably " + m.Count + "  " +
                    "since " + typeof(TEnumType).ToStringShort() + " has " + m.Count + " members (not counting the '__invalid' member).";
                index = -1; // Ensure index does not get used
                return false;
            }
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "TODO: Decide on what we actually want with ToString for this class.\r\n" +
            "TODO: For instance, a list of id-related fields would be useful, or tagging av properties which kind of define the entity"
        )]
        public override string ToString() => GetType().ToStringVeryShort();

        public class PExactException : ApplicationException
        {
            public PExactException(string message) : base(message) { }
            public PExactException(string message, Exception inner) : base(message, inner) { }
        }
    }
}