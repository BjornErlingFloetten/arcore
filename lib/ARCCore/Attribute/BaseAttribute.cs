﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Collections.Immutable;
using System.Linq;
using System.Text;

namespace ARCCore
{

    /// <summary>
    /// 
    /// </summary>
    [Class(
        Description =
            "Instances of this class are used as source of documentation and data for the API. " +
            "\r\n" +
            "Inherits -" + nameof(System.Attribute) + "- but does also implement -" + nameof(IP) + "-.\r\n" +
            "\r\n" +
            "Note how the information NORMALLY originates from within the C# code but as the class inherit " +
            "-" + nameof(IP) + "-, it is also possible to distribute the information over the property-stream.\r\n" +
            "\r\n" +
            "Some inheriting members in -" + nameof(ARComponents.ARCCore) + "- are:\r\n" +
            "-" + nameof(ClassAttribute) + "-,\r\n" +
            "-" + nameof(ClassMemberAttribute) + "-,\r\n" +
            "-" + nameof(EnumAttribute) + "-,\r\n" +
            "-" + nameof(EnumMemberAttribute) + "-,\r\n" +
            "-" + nameof(BasePKAttribute) + "- (which again is inherited by -" + nameof(PKTypeAttribute) + "-, -" + nameof(PKLogAttribute) + "-, -" + nameof(PKRelAttribute) + "- and -" + nameof(PKHTMLAttribute) + "-)"
    )]
    /// NOTE: Class should really have been marked "abstract" but then the strict enforcement of <see cref="EnumAttributeP.CorrespondingClass"/> will fail.
    public class BaseAttribute : Attribute, IP
    {

        [ClassMember(Description =
            "Note that we really only use -" + nameof(PK) + "- instances as keys, so we could theoretically have used instances of that type as key instead, " +
            "but there are situations where we will be passed the more general -" + nameof(IK) + "- as key " +
            "(like if we peek a data structure with a general query mechanism which does not care about distinctions of -" + nameof(IK) + "-.)\r\n" +
            "so therefore we follow the same approach as in -" + nameof(PRich) + "- (same key type in storage)."
        )]
        private Dictionary<IK, IP> Properties { get; } = new Dictionary<IK, IP>();

        /// <summary>
        /// Gives easy access to default interface methods inside of class
        ///  For some discussion about this, see:
        ///  https://stackoverflow.com/questions/57761799/calling-c-sharp-interface-default-method-from-implementing-class
        /// Note: Do not use <see cref="ClassMemberAttribute"/> here, it will only mess up the HTML documentation for AgooRapide with unnecessary links to IP-members.
        /// </summary>
        public IP IP => this;

        public bool TrySetP(IKIP ikip, out string errorResponse)
        {
            // This is too difficult as initialization because we have a chicken-and-egg issue with PK not being available yet
            // ikip.AssertTypeIntegrity();
            // TODO: IMPORTANT. Restore assertion here!
            Properties[ikip.Key] = ikip.P;
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description = "Should probably never be used.")]
        public bool TryRemoveP(IK key, out string errorResponse)
        {
            if (!Properties.TryGetValue(key, out _))
            {
                errorResponse = "Key " + key.ToString() + " not found";
                return false;
            }
            Properties.Remove(key);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public bool TryGetP<T>(IK k, out T p, out string errorResponse) where T : IP
        {
            if (!Properties.TryGetValue(k, out var ip))
            {
                p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Key " + k.ToString() + " not found";
                return false;
            }

            if (!(ip is T temp))
            {
                p = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Key " + k + ": Found type " + ip.GetType().ToStringShort() + ", not " + typeof(T).ToStringShort();
                return false;
            }

            p = temp;
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        // public IEnumerable<IKIP> GetAllP() => Properties.Select(e => new IKIP(e.Key, e.Value));
        public IEnumerator<IKIP> GetEnumerator()
        {
            foreach (var e in Properties)
            {
                yield return new IKIP(e.Key, e.Value);
            }
        }

        public virtual bool TryGetV<TRequest>(out TRequest retval, out string errorResponse) where TRequest : notnull
        {
            /// Due to a desire for a 'safe' poking of properties, for instance by <see cref="ARComponents.ARCDoc"/>, we can not be this strict:
            // throw new AttributeException(
            //"Illegal to call " + System.Reflection.MethodBase.GetCurrentMethod().Name + " for this class (" + ToString() + ") " +
            //"because has no concept of a single property value. " +
            //"Type parameter: " + typeof(TRequest).ToStringShort() + ".\r\n");
            retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            errorResponse = "Irrelevant for " + GetType().ToStringShort() + " because this class does not have a value of itself, it is a collection of values";
            return false;
        }

        public virtual bool OnTrySetP(IKIP ikip) => false;
        public virtual IEnumerable<IKIP> GetKeysEqualToValue(IK key, string value) => 
            this.Where(ikip => ikip.P.TryGetPV<string>(key, out var v) && v.Equals(value)).Select(ikip => ikip);
        public virtual bool TrySetPP(IK entityKey, IP? entity, IKIP ikip, out string errorResponse) =>
            (entity != null || TryGetP<IP>(entityKey, out entity, out errorResponse)) && entity.TrySetP(ikip, out errorResponse);

        [ClassMember(Description =
            "Does not have a corresponding -" + nameof(BaseAttributeP) + "- " +
            "since all attributes relevant for querying (for instance in the documentation) will be IsDefault = FALSE anyway. " +
            "(if we had included it in -" + nameof(BaseAttributeP) + "- it would just have led to a lot of confusing IsDefault = FALSE key-values showing up.)"
        )]
        public bool IsDefault;

        [ClassMember(Description = "See -" + nameof(BaseAttributeP.Description) + "-.")]
        private string? _description;
        public string? Description
        {
            get
            {
                AssertInitializationNotCompleteForAttributeAccess();
                return _description;
            }
            set => _description = value;
        }

        [ClassMember(Description = "See -" + nameof(BaseAttributeP.LongDescription) + "-")]
        private string? _longDescription;
        public string? LongDescription
        {
            get
            {
                AssertInitializationNotCompleteForAttributeAccess();
                return _longDescription;
            }
            set => _longDescription = value;
        }

        [ClassMember(Description =
            "Note how is guaranteed to always return a value (notnull-constraint on T).\r\n" +
            "Will return an instance with -" + nameof(IsDefault) + "- if no attribute found."
        )]
        protected static T GetAttributeThroughType<T>(Type type) where T : notnull, BaseAttribute, new() => FilterAndAssertAttribute<T>(
            AllAgoRapideAttributeTypes.Select(t =>
            {
                InvalidTypeException.AssertAssignable(t, typeof(System.Attribute), () =>
                    "Type " + t.ToStringShort() + " must inherit " + nameof(System.Attribute) + " (but it does not) " +
                    "in order to call " + nameof(Attribute.GetCustomAttribute) + " with it as second parameter. " +
                    "Possible cause: A bug in the C# AgoRapide code, especially in " + nameof(AllAgoRapideAttributeTypes) + "\r\n"
                );
                return GetCustomAttribute(
                    type,
                    t,
                    inherit: false // Added parameter inherit: false 11 Mar 2021 in order to NOT inherit attribute from base-class.
                );
            }).Where(a => a != null).Select(a => (BaseAttribute)a),
            type.ToStringShort()
        ) ??
        new T()
        {
            IsDefault = true
        };

        public IP DeepCopy()
        {
            try
            {
                REx.Inc();
                var retval = (IP)System.Activator.CreateInstance(GetType());
                this.ForEach(e =>
                {
                    retval.AddP(e.Key, e.P.DeepCopy());
                });
                return retval;
            }
            finally
            {
                REx.Dec();
            }
        }

        [ClassMember(Description =
             "To be overridden in sub-classes, but do note that sub-classes are supposed to call base.Initialize() at end of their own initialization."
        )]
        public virtual void Initialize()
        {
            // Note, IsDefault is not considered necessary to include as property key (it will only pollute a HTML presentation for instance)
            // (all attributes relevant for showing will be IsDefault = FALSE anyway).
            // IP.AddPV(BaseAttributeP.IsDefault, IsDefault);
            if (!string.IsNullOrEmpty(Description)) IP.AddPV(BaseAttributeP.Description, Description);
            if (!string.IsNullOrEmpty(LongDescription)) IP.AddPV(BaseAttributeP.LongDescription, LongDescription);
        }

        /// <summary>
        /// TODO: Create this automatically. Use current assembly, check for classes inheriting BaseAttribute.
        /// (in this manner we could support attributed added externally of AgoRapide too) 
        /// </summary>
        [ClassMember(
            Description =
                "Used for clarifying misunderstandings (using wrong attribute-class for a given element).\r\n" +
                "\r\n" +
                "This collection should only contain elements that are exclusive to each other, that is, when " +
                "asking for a given attribute, the presence of other attributes in this collection should signify " +
                "an error (that is, an exception should be thrown in order to inform the user).\r\n" +
                "\r\n" +
                "TODO: Text above does not correspond with actual content of collection,\r\n" +
                "TODO: because BasePKAttribute and PKTypeAttribute inherits EnumMemberAttribute",
            LongDescription = "See also -" + nameof(IncorrectAttributeTypeUsedGeneralExplanation) + "-."
        )]
        protected static readonly IEnumerable<Type> AllAgoRapideAttributeTypes = new List<Type> {
            typeof(ClassAttribute),
            typeof(ClassMemberAttribute),
            typeof(EnumAttribute),
            typeof(EnumMemberAttribute),
            // typeof(IPAttribute), // This does not inherit System.Attribute, put back if relevant
            typeof(BasePKAttribute), // Note that this is an abstract class (super class)
            typeof(PKTypeAttribute)
        };

        public static readonly string IncorrectAttributeTypeUsedGeneralExplanation =
            "One example of misunderstanding when using attributes would be confusing -" + nameof(EnumMemberAttribute) + "- " +
            "with a class inheriting -" + nameof(BasePKAttribute) + "- (like -" + nameof(PKTypeAttribute) + "-).\r\n" +
            "They are both used for tagging enum members / enum values, but -" + nameof(BasePKAttribute) + "- (like -" + nameof(PKTypeAttribute) + "-) " +
            "is special because it is used for -" + nameof(ARConcepts.TaggingOfPropertyKeys) + "-.\r\n" +
            "\r\n" +
            "Example 1:\r\n" +
            "   public class Customer {\r\n" +
            "   }\r\n" +
            "   [Enum(AgoRapideEnumType = EnumType.PropertyKey)]\r\n" +
            "   public enum CustomerP {\r\n" +
            "      [EnumMember(Description = \"Customer's first name / given name\")] // WRONG APPROACH\r\n" +
            "      FirstName,\r\n" +
            "      [PKType(Description = \"Customer's last name / familiy name\")] // CORRECT APPROACH\r\n" +
            "      LastName,\r\n" +
            "   }\r\n" +
            "\r\n" +
            "\r\n" +
            "\r\n" +
            "Another example of what could be wrong is just confusing names / typos, like this:\r\n" +
            "\r\n" +
            "Example 2:\r\n" +
            "   public class Prosessor {\r\n" +
            "      [Class(Description = \"Process A\")] // WRONG APPROACH\r\n" +
            "      public void ProcessA() {\r\n" +
            "         ...\r\n" +
            "      }\r\n" +
            "      [ClassMember(Description = \"Process B\")] // CORRECT APPROACH\r\n" +
            "      public void ProcessB() {\r\n" +
            "         ...\r\n" +
            "      }\r\n" +
            "   }\r\n" +
            "\r\n";

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="member"></param>
        /// <param name="detailer"></param>
        /// <param name="allowMultiple">
        /// Almost all attributes are supposed to exist only one of. 
        /// One exception is <see cref="BasePKAttribute"/> of which we may have multiple (stored in a <see cref="PK"/> instance).
        /// </param>
        /// <returns></returns>
        [ClassMember(Description = "Filter against type parameter and asserts that no other attributes are present in list")]
        protected static IEnumerable<T> FilterAndAssertAttributes<T>(IEnumerable<BaseAttribute> list, string member, Func<string>? detailer = null, bool allowMultiple = false) where T : notnull, BaseAttribute
        {
            var desired = list.Select(i => i as T).Where(i => i != null).Select(i => i!).ToImmutableList();
            if (!allowMultiple && desired.Count > 1) throw new BaseAttributeException(
                "Multiple attributes of type " + typeof(T).ToStringShort() + " found (0 or 1 expected, but found " + desired.Count + "). " +
                "The attributes found where: " + string.Join(", ", desired.Select(d => d.GetType().ToStringShort())) + ".\r\n" +
                "Member: " + member + "." + detailer.Result(" Details: ") + "\r\n");
            var undesired = list.Where(i => !(i is T)).ToImmutableList();
            if (undesired.Count > 0) throw new IncorrectAttributeTypeUsedException(undesired[0], typeof(T), member, detailer);
            return desired;
        }

        [ClassMember(Description = "See -" + nameof(FilterAndAssertAttributes) + "- for explanation. Note that may return null")]
        protected static T? FilterAndAssertAttribute<T>(IEnumerable<BaseAttribute> list, string member, Func<string>? detailer = null) where T : BaseAttribute =>
            FilterAndAssertAttributes<T>(list, member, detailer, allowMultiple: false).FirstOrDefault();

        [Class(Description = "Helps to clean up any confusion about which -" + nameof(BaseAttribute) + "- to use in a given concept.")]
        public class IncorrectAttributeTypeUsedException : ApplicationException
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="foundAttribute"></param>
            /// <param name="expectedType"></param>
            /// <param name="member">TODO: Should this be renamed 'classOrMemberName'? Check code!</param>
            /// <param name="detailer"></param>
            public IncorrectAttributeTypeUsedException(object foundAttribute, Type expectedType, string member, Func<string>? detailer = null) : base(
                "Incorrect attribute used for " + member + ".\r\n" +
                "Expected " + expectedType + " but found " + foundAttribute.GetType() + ".\r\n" +
                "Resolution: Change to " + expectedType + " for " + member + ".\r\n" +
                "Details for " + nameof(foundAttribute) + ":\r\n" + foundAttribute.ToString() +
                detailer.Result("\r\nDetails: ") + "\r\n\r\n" +
                nameof(IncorrectAttributeTypeUsedGeneralExplanation) + ": " + BaseAttribute.IncorrectAttributeTypeUsedGeneralExplanation)
            { }
        }

        public class BaseAttributeException : ApplicationException
        {
            public BaseAttributeException(string message) : base(message) { }
            public BaseAttributeException(string message, Exception inner) : base(message, inner) { }
        }

        public class AttributeAccessAfterInitialiationException : ApplicationException
        {
            public AttributeAccessAfterInitialiationException() : base(
                "You are not allowed to access -" + nameof(BaseAttribute) + "- 'ordinary' C# language properties (setters / getters) after application initialization. " +
                "This is a safeguard in order to ensure that the corresponding standard " + nameof(BaseAttribute.Properties) + " collection is used instead " +
                "(because these values can be updated remotely over the -" + nameof(ARConcepts.PropertyStream) + "-). " +
                "The 'ordinary' properties (setters / getters) are only to be used when 'tagging' components in the C# code")
            { }
        }

        protected void AssertInitializationNotCompleteForAttributeAccess()
        {
            if (!UtilCore.CurrentlyStartingUp) throw new AttributeAccessAfterInitialiationException();
        }
    }

    [Enum(
        AREnumType = AREnumType.PropertyKeyEnum,
        Description = "Describes class -" + nameof(BaseAttribute) + "-."
    )]
    public enum BaseAttributeP
    {
        __invalid,

        // TODO: Delete commented out code. Removed 2 May 2020 since all attributes relevant for showing will be IsDefault = FALSE anyway.
        //[PKType(Description =
        //    "TRUE means that the attribute is not defined with any values in the C# code, it is just an 'empty' class without any properties.\r\n" +
        //    "\r\n" +
        //    "Originates from -BaseAttribute.IsDefault- which again is set by -BaseAttribute.GetAttributeThroughType- if no attribute is found.",
        //    IsObligatory = true
        //)]
        //IsDefault,

        [PKType(Description =
            "The description for the attribute, like this description" +
            "\r\n" +
            "Originates from -" + nameof(BaseAttribute.Description) + "-."
        )]
        Description,

        [PKType(Description =
            "The long (detailed) description for the attribute.\r\n" +
            "\r\n" +
            "Originates from -" + nameof(BaseAttribute.LongDescription) + "-."
        )]
        LongDescription,

        // TODO: Delete comented out vode
        //[PKType(Description =
        //    "TODO: Is this needed?\r\n" +
        //    "\r\n" +
        //    "Constitutes -" + nameof(Description) + "- plus -" + nameof(LongDescription) + "-.")]
        //WholeDescription,
    }
}