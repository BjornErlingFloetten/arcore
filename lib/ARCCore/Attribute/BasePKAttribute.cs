﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using System.Linq;

namespace ARCCore {

    [Class(
        Description =
            "The class containing the actual attributes used for -" + nameof(ARConcepts.TaggingOfPropertyKeys) + "-. " +
            "\r\n" +
            "TODO: Rename into PKBaseAttribute?.\r\n" +
            "\r\n" +
            "Instances of this class are accessed through -" + nameof(PK) + "-.\r\n" +
            "\r\n" +
            "Some sub-classes included in -" + nameof(ARComponents.ARCCore) + "- are:\r\n" +
            "-" + nameof(PKTypeAttribute) + "-: Contains the most basic essential information about a property key.\r\n" +
            "-" + nameof(PKRelAttribute) + "-: Describes relationships between entities.\r\n" +
            "-" + nameof(PKHTMLAttribute) + "-: Contains pointer to HTML encoder for a property.\r\n" +
            "-" + nameof(PKLogAttribute) + "-: Explains how logging of changes to a property should be done.\r\n" +
            "\r\n" +
            "Other -" + nameof(ARComponents) + "- define their own sub-classes like:\r\n" +
            // "-" + nameof(ARComponents.ARCRet) + "-: -PKRetAttribute-,\r\n" +                        /// Note use of <see cref="ARConcepts.LinkInsertionInDocumentation"/> 
            // "-" + nameof(ARComponents.ARCMockValues) + "-: -PKMockValuesAttribute-,\r\n" +          /// without actually type being present "here".
            // "-" + nameof(ARComponents.ARCQuery) + "-: -PKRelAttribute-,\r\n" +                        /// Links will automatically resolve if types are present at run-time
            // "-" + nameof(ARComponents.ARCCalc) + "-: -PKCalcAttribute-,\r\n" +                      
            "-" + nameof(ARComponents.ARCDoc) + "-: -PKDocAttribute-,\r\n" +
            "\r\n" +
            "You are also encouraged to create your own instances in your -" + nameof(ARConcepts.ApplicationSpecificCode) + "- for special needs.\r\n" +
            "TODO: One idea is a TTL (Time-to-live) tag for a property when that property constitues a command to do something\r\n" +
            "TODO: for instance turning on a switch in a IoT system (in case of communications failure somewhere in the system, queuing a series\r\n" +
            "TODO: of turn-on, turn-off, turn-on attempts would have little meaning.\r\n" +
            "\r\n" +
            "NOTE / TODO: Having this class inherit EnumMemberAttribute makes for a complicated inheritance hierarchy. " +
            "It might therefore be a good idea to just duplicate the function of EnumMemberAttribute into this class and letting it inherit BaseAttribute directly.\r\n" +
            "\r\n" +
            "See also analogue -" + nameof(IPAttribute) + "- which describes classes (describes at class levels)."
    )]
    public abstract class BasePKAttribute : EnumMemberAttribute {

        private static readonly ConcurrentDictionary<Type, ConcurrentDictionary<string, List<BasePKAttribute>>> _PKAttributesCache = new ConcurrentDictionary<Type, ConcurrentDictionary<string, List<BasePKAttribute>>>();
        /// <summary>
        /// Note that we can not use our super-class' <see cref="EnumMemberAttribute.GetAttributeThroughFieldInfo"/> and <see cref="EnumMemberAttribute.GetAttribute"/>
        /// since it can be multiple instances of <see cref="BasePKAttribute"/> for a given value of an enum of type <see cref="AREnumType.PropertyKeyEnum"/>
        /// </summary>
        /// <param name="_enum"></param>
        /// <returns></returns>
        [ClassMember(Description =
            "Note that illegal to call for other than -" + nameof(AREnumType.PropertyKeyEnum) + "-.\r\n" +
            "Note how an instance of -" + nameof(PKTypeAttribute) + "- is added if not found (and -" + nameof(PKTypeAttribute.Type) + "- set to typeof(string)."
        )]
        public static List<BasePKAttribute> GetAttributes(object _enum) => _PKAttributesCache.
            GetOrAdd(_enum.GetType(), dummy => new ConcurrentDictionary<string, List<BasePKAttribute>>()).
            GetOrAdd(_enum.ToString(), dummy => {
                var type = _enum.GetType();
                NotOfTypeEnumException.AssertEnum(type, () => nameof(_enum) + ": " + _enum.ToString());

                if (type == typeof(EnumAttributeP)) {
                    // Important, do not ask for type.GetEnumAttribute now because that could lead to an infinite loop / stack overflow 
                    // because EnumAttributeP is just what is needed in order to GetEnumAttribute
                } else if (EnumAttribute.GetAttribute(type).IP.GetPV<AREnumType>(EnumAttributeP.AREnumType) != AREnumType.PropertyKeyEnum) {
                    throw new InvalidObjectTypeException(_enum,
                        "Only " + AREnumType.PropertyKeyEnum + " allowed here.\r\n" +
                        "Most common cause: " + nameof(PKTypeAttribute) + "." + nameof(PKTypeAttribute.GetAttributes) + " was incorrectly called instead of " + nameof(EnumMemberAttribute) + "." + nameof(GetAttribute) + " " +
                        "(the former (this method) is called only for " + nameof(AREnumType.PropertyKeyEnum) + " enums " +
                        "while the latter is called for enums like " + nameof(AREnumType.OrdinaryEnum) + " and " + nameof(AREnumType.DocumentationOnlyEnum) + ". " +
                        "Since the former may return multiple attributes you can not mix these concepts.)\r\n" +
                        "\r\n" +
                        "Possible resolution (if common cause explained above is not the case): " +
                        "Check that you tagged enum " + type.ToStringShort() + " with the correct " +
                        "[Enum(" + nameof(EnumAttribute.AREnumType) + " = " + nameof(AREnumType) + "." + nameof(AREnumType.PropertyKeyEnum) + ")]\r\n");
                }
                var field = type.GetField(_enum.ToString()) ?? throw new NullReferenceException(nameof(type.GetField) + "(): Cause: " + type + "." + _enum + " is most probably not defined.\r\n");
                var retval = GetAttributesThroughFieldInfo(field, () => type + "." + _enum);
                if (!retval.Any(a => a.GetType().Equals(typeof(PKTypeAttribute)))) retval.Add(new PKTypeAttribute { Type = typeof(string) });

                retval.ForEach(a => {
                    a.IP.AddPV(EnumMemberAttributeP.EnumType, _enum.GetType());
                    a.IP.AddPV(EnumMemberAttributeP.EnumMember, _enum.ToString());
                    a.Initialize();
                });
                return retval;
            });

        /// <summary>
        /// NOTE: Really not necessary to have as a separate method since only called from <see cref="GetAttributes"/>
        /// </summary>
        /// <param name="fieldInfo">The enum field for which to get attributes, like CustomerP.FirstName</param>
        /// <param name="memberInfo">Provides debug information in case an exception is thrown</param>
        /// <returns></returns>
        [ClassMember(
            Description =
                "Gets attributes of type -" + nameof(BasePKAttribute) + "\r\n" +
                "\r\n" +
                "Attempts to clarify incorrect attribute type used (through -" + nameof(BaseAttribute.IncorrectAttributeTypeUsedException) + "-).\r\n" +
                "\r\n" +
                "Note that we can not use our super-class' -" + nameof(EnumMemberAttribute.GetAttributeThroughFieldInfo) + "- and -" + nameof(EnumMemberAttribute.GetAttribute) + "- " +
                "since it can be multiple instances of -" + nameof(BasePKAttribute) + "- " +
                "for a given value of an enum of type -" + nameof(AREnumType) + "." + nameof(AREnumType.PropertyKeyEnum) + "-."
        )]
        private static List<BasePKAttribute> GetAttributesThroughFieldInfo(System.Reflection.FieldInfo fieldInfo, Func<string> memberInfo) {
            var retval = fieldInfo.GetCustomAttributes(
                // typeof(BasePKAttribute),  // Note that we can not specify type here because it must match exactly (sub-classes will not be included)
                // We therefore have to ask for all attributes and filter for those needed.
                inherit: false           // Inheritance is not relevant for enum's (enum's do not inherit each other) 
            ).Where(a => a is BasePKAttribute).Select(a => (BasePKAttribute)a).ToList();

            if (retval.Count == 0) {
                /// No attributes of type <see cref="BasePKAttribute"/> was found. 
                /// This is quite normal for some fields (for instance of type string with no other tags necessary). 
                /// BUT, check anyway for possible mis-tagging. For instance
                /// EnumMember(Description="Something")
                /// FirstName
                AllAgoRapideAttributeTypes.ForEach(t => {
                    var found = fieldInfo.GetCustomAttributes(t, inherit: true);
                    if (found != null && found.Length > 0) throw new IncorrectAttributeTypeUsedException(found[0], typeof(BasePKAttribute), memberInfo());
                });
            }
            return retval;
        }

        public override void Initialize() =>
            /// Add initialization code here as needed 
            /// Most probably not need here, but needed in sub-classes like <see cref="PKTypeAttribute.Initialize"/>
            /// ...
            base.Initialize();

        // TODO: Delete commented out code
        // NOTE: Removed 3 Sep 2020 because only used within PKTypeAttribute and "impossible" to use elsewhere.
        ///// <summary>
        ///// TODO: It might actually be 'impossible' for other classes to implement a value here because 
        ///// TODO: <see cref="PKTypeAttribute"/> is intimately connected with <see cref="PK"/>
        ///// TODO: (it is assumed to always exist one <see cref="PKTypeAttribute"/> for every <see cref="PK"/>)
        ///// </summary>
        ///// <param name="pkType">From <see cref="PK.Type"/></param>
        ///// <returns></returns>
        //[ClassMember(
        //    Description =
        //        "Standard validator and parser. " +
        //        "\r\n" +
        //        "Used by -" + nameof(PK) + "- when no -" + nameof(ITypeDescriber) + "- is available (or when it does not enrich key with a validator and parser)\r\n" +
        //        "\r\n" +
        //        "See example of implementation in -" + nameof(PKTypeAttribute) + "- (-" + nameof(PKTypeAttribute.StandardValidatorAndParser) + "-).",
        //    LongDescription =
        //        "See -" + nameof(PK.ValidatorAndParser) + "- for more information about the concept.\r\n" +
        //        "\r\n" +
        //        "Set return value to null if no validator and parser is relevant for your " + nameof(BasePKAttribute) + " implementation " +
        //        "(this is actually the 'normal' case, as only one -" + nameof(BasePKAttribute) + "- may offer a validator and parser, " +
        //        "if multiple are offered then an exception will be thrown by -" + nameof(PK.Initialize) + "-.).\r\n" +
        //        "\r\n" +
        //        "Note that within -" + nameof(ARConcepts.StandardAgoRapideCode) + "- only -" + nameof(PKTypeAttribute) + "- will offer a value here."
        //)]
        //public abstract Func<string, ParseResult>? StandardValidatorAndParser(Type pkType);

        // TODO: Delete commented out code
        // NOTE: Removed 3 Sep 2020 because only used within PKTypeAttribute and "impossible" to use elsewhere.
        ///// <summary>
        ///// TODO: It might actually be 'impossible' for other classes to implement a value here because 
        ///// TODO: <see cref="PKTypeAttribute"/> is intimately connected with <see cref="PK"/>
        ///// TODO: (it is assumed to always exist one <see cref="PKTypeAttribute"/> for every <see cref="PK"/>)
        ///// </summary>
        ///// <param name="pkType">From <see cref="PK.Type"/></param>
        ///// <returns></returns>
        //[ClassMember(
        //    Description =
        //        "Standard cleaner. Used by -" + nameof(PK) + "- when no -" + nameof(ITypeDescriber) + "- is available (or when it does not enrich key with a cleaner)\r\n" +
        //        "\r\n" +
        //        "See implementation in -" + nameof(PKTypeAttribute) + "- (-" + nameof(PKTypeAttribute.StandardCleaner) + "-).",
        //    LongDescription =
        //        "See -" + nameof(PK.Cleaner) + "- for more information about the concept.\r\n" +
        //        "\r\n" +
        //        "Set return value to null if no validator and parser is relevant for your " + nameof(BasePKAttribute) + " implementation " +
        //        "(this is actually the 'normal' case, as only one -" + nameof(BasePKAttribute) + "- may offer a validator and parser, " +
        //        "if multiple are offered then an exception will be thrown by -" + nameof(PK.Initialize) + "-.).\r\n" +
        //        "\r\n" +
        //        "Note that within -" + nameof(ARConcepts.StandardAgoRapideCode) + "- only -" + nameof(PKTypeAttribute) + "- will offer a value here."
        //)]
        //public abstract Func<string, string>? StandardCleaner(Type pkType);
    }
}