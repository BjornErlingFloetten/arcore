﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;

namespace ARCCore {

    [Class(Description =
        "Describes an enum's value." +
        "\r\n" +
        "The enum in itself is described by -" + nameof(EnumAttribute) + "-. " +
        "\r\n" +
        "TODO: Consider renaming class into EnumMemberAttribute, and correspondingly use term 'Member' also for enums, instead of 'Value' in AgoRapide.\r\n" +
        "\r\n" +
        "Note how enum members / enum values of type -" + nameof(AREnumType.PropertyKeyEnum) + "- " +
        "should be described by sub-class -" + nameof(BasePKAttribute) + "-, not this clas. "
    )]
    public class EnumMemberAttribute : BaseAttribute {

        private static readonly ConcurrentDictionary<Type, ConcurrentDictionary<string, EnumMemberAttribute>> _enumMemberAttributeCache = new ConcurrentDictionary<Type, ConcurrentDictionary<string, EnumMemberAttribute>>();
        /// <summary>
        /// Note how sub-class <see cref="BasePKAttribute"/> has its own methods (<see cref="BasePKAttribute.GetAttributes"/> and <see cref="BasePKAttribute.GetAttributesThroughFieldInfo"/> since it may 
        /// be multiple instances of this for a given value of an enum of type <see cref="AREnumType.PropertyKeyEnum"/>.
        /// </summary>
        /// <param name="_enum"></param>
        /// <returns></returns>
        [ClassMember(Description = "Note that illegal to call for -" + nameof(AREnumType.PropertyKeyEnum) + "-.")]
        public static EnumMemberAttribute GetAttribute(object _enum) => _enumMemberAttributeCache.
            GetOrAdd(_enum.GetType(), dummy => new ConcurrentDictionary<string, EnumMemberAttribute>()).
            GetOrAdd(_enum.ToString(), dummy => {
                var type = _enum.GetType();
                NotOfTypeEnumException.AssertEnum(type, () => nameof(_enum) + ": " + _enum.ToString());

                if (EnumAttribute.GetAttribute(type).IP.GetPV<AREnumType>(EnumAttributeP.AREnumType) == AREnumType.PropertyKeyEnum) {
                    throw new InvalidObjectTypeException(_enum,
                        nameof(AREnumType) + "." + AREnumType.PropertyKeyEnum + " not allowed here.\r\n" +
                        "Most common cause: " + nameof(EnumMemberAttribute) + "." + nameof(GetAttribute) + " was incorrectly called instead of " + nameof(PKTypeAttribute) + "." + nameof(PKTypeAttribute.GetAttributes) + " " +
                        "(the former (this method) is called for enums like " + nameof(AREnumType.OrdinaryEnum) + " and " + nameof(AREnumType.DocumentationOnlyEnum) + " " +
                        "while the latter is called only for " + nameof(AREnumType.PropertyKeyEnum) + " enums. " +
                        "Since the latter may return multiple attributes you can not mix these concepts.\r\n" +
                        "\r\n" +
                        "Possible resolution (if common cause explained above is not the case): " +
                        "Check that you tagged enum " + type.ToStringShort() + " with the correct\r\n" +
                        "[Enum(" + nameof(EnumAttribute.AREnumType) + " = " + nameof(AREnumType) + "." + nameof(AREnumType.OrdinaryEnum) + ")]\r\n" +
                        "or\r\n" +
                        "[Enum(" + nameof(EnumAttribute.AREnumType) + " = " + nameof(AREnumType) + "." + nameof(AREnumType.DocumentationOnlyEnum) + ")]\r\n" +
                        "\r\n" +
                        "\r\n" +
                        // TODO: Eliminating BasePKAttribute as subclass of EnumMemberAttribute would eliminate 'need' for explaining why this is not possible
                        "Reason for this exception message:\r\n" +
                        "It might look natural to allow this in C# code now (since -" + nameof(BasePKAttribute) + "- is a sub-class of the class requested now (-" + nameof(EnumMemberAttribute) + "-):\r\n" +
                        "\r\n" +
                        "   return BasePKAttribute.GetAttribute(_enum); // Quite OK since this is a sub class\r\n" +
                        "\r\n" +
                        "but there may be multiple -" + nameof(BasePKAttribute) + "- for a given value of an enum of type -" + nameof(AREnumType.PropertyKeyEnum) + "- " +
                        "because we want that system to be expandable since it is so central to AgoRapide, and then we would not know which of those to return now.\r\n" +
                        "In fact, method BasePKAttribute.GetAttribute does not even exits, only PKAttribute.GetAttributes\r\n\r\n" +
                        nameof(BaseAttribute.IncorrectAttributeTypeUsedGeneralExplanation) + BaseAttribute.IncorrectAttributeTypeUsedGeneralExplanation + "\r\n");

                }
                var field = type.GetField(_enum.ToString()) ?? throw new NullReferenceException(nameof(type.GetField) + "(): Cause: " + type + "." + _enum + " is most probably not defined.\r\n");
                var retval = GetAttributeThroughFieldInfo(field, type + "." + _enum);
                retval.IP.AddPV(EnumMemberAttributeP.EnumType, _enum.GetType());
                retval.IP.AddPV(EnumMemberAttributeP.EnumMember, _enum.ToString());
                retval.Initialize();
                return retval;
            });

        /// <summary>
        /// Gets attribute of type <see cref="EnumMemberAttribute"/> from <paramref name="fieldInfo"/>
        /// 
        /// NOTE: Really not necessary to have as a separate method since only called from <see cref="GetAttribute"/>
        /// 
        /// Attempts to clarify incorrect attribute type used (through <see cref="BaseAttribute.IncorrectAttributeTypeUsedException"/>)
        /// 
        /// Note how sub-class <see cref="BasePKAttribute"/> has its own methods (<see cref="BasePKAttribute.GetAttributes"/> and <see cref="BasePKAttribute.GetAttributesThroughFieldInfo"/> since it may 
        /// be multiple instances of this for a given value of an enum of type <see cref="AREnumType.PropertyKeyEnum"/>
        /// </summary>
        /// <param name="fieldInfo"></param>
        /// <param name="memberInfo">Provides debug information in case an exception is thrown</param>
        /// <returns></returns>
        protected static EnumMemberAttribute GetAttributeThroughFieldInfo(System.Reflection.FieldInfo fieldInfo, string memberInfo) =>
            FilterAndAssertAttribute<EnumMemberAttribute>(
                AllAgoRapideAttributeTypes.SelectMany(t => fieldInfo.GetCustomAttributes(t, inherit: false)).Where(a => a != null).Select(a => {
                    if (a is BasePKAttribute) {
                        // This assertion serves two purposes:
                        /// 1) Guards against duplicates of same attribute being found. Example: We ask once each for both EnumMemberAttribute, BasePKAttribute and PKTypeAttribute. A PKType attribute might satisfy all three searches and thus being returned thrice.
                        /// 2) No attributes inheriting <see cref="PKTypeAttribute"/> is expected here anyway. 
                        throw new EnumMemberAttributeException(
                            "Attribute inheriting " + typeof(BasePKAttribute).ToStringShort() + " " +
                            "(specifically " + a.GetType().ToStringShort() + ") found for " + memberInfo + "\r\n" +
                            "Possible causes:\r\n" +
                            "1) Use of [PKType(...] instead of [EnumMember(... when describing this member.\r\n" +
                            "2) Not setting [Enum(" + nameof(AREnumType) + " = " + nameof(AREnumType) + "." + nameof(AREnumType.PropertyKeyEnum) + "] for enum"
                        );
                    }
                    return (BaseAttribute)a;
                }),
                memberInfo
            ) ??
            new EnumMemberAttribute() {
                IsDefault = true
            };

        public override void Initialize() => base.Initialize();

        public class EnumMemberAttributeException : ApplicationException {
            public EnumMemberAttributeException(string message) : base(message) { }
        }
    }

    [Enum(
        AREnumType = AREnumType.PropertyKeyEnum,
        Description = "Describes class -" + nameof(EnumMemberAttribute) + "-."
    )]
    public enum EnumMemberAttributeP {
        __invalid,

        [PKType(
            Description = "The type of the enum (like type of 'CustomerP' in CustomerP.FirstName) that we are an attribute for",
            Type = typeof(Type)
        )]
        EnumType,

        [PKType(
            Description = "The actual enum member / enum value (like 'Firstname' in CustomerP.FirstName) that we are an attribute for",
            LongDescription = "Note that type is string"
        )]
        EnumMember
    }
}
