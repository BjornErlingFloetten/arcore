﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;

namespace ARCCore {

    [Class(Description =
            "Describes a member of a class (a method).\r\n" +
            "\r\n" +
            "The class itself is described by -" + nameof(ClassAttribute) + "-."
    )]    
    // This looks tempting but is too restrictive
    // [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field)]
    public class ClassMemberAttribute : BaseAttribute {

        private static ConcurrentDictionary<string, ClassMemberAttribute> _classMemberAttributeCache = new ConcurrentDictionary<string, ClassMemberAttribute>();
        /// <summary>
        /// NOTE: Use with caution. 
        /// NOTE: Throws an exception if no method found, or overloaded methods found and a multiple of them again have an attribute defined.
        /// NOTE: 
        /// NOTE: Overload <see cref="GetAttribute(System.Reflection.MemberInfo)"/> is preferred. 
        /// </summary>
        /// <param name="classType"></param>
        /// <param name="memberName"></param>
        /// <returns></returns>
        public static ClassMemberAttribute GetAttribute(Type classType, string memberName) => _classMemberAttributeCache.GetOrAdd(classType + "." + memberName, s => {
            var candidates = classType.GetMembers().Where(m => m.Name.Equals(memberName)).ToList();
            return (candidates.Count) switch {
                0 => throw new NullReferenceException(nameof(memberName) + ": " + memberName + ". Cause: " + classType + "." + memberName + " is most probably not defined.\r\n"),
                1 => GetAttribute(candidates[0]),
                _ => new Func<ClassMemberAttribute>(() => {
                    candidates = candidates.Where(c => GetAttribute(c).Use(a => !a.IsDefault)).ToList();
                    return (candidates.Count) switch {
                        /// This corresponds to code in <see cref="GetAttributeThroughMemberInfo"/>
                        0 => new ClassMemberAttribute() {
                            IsDefault = true,
                        },
                        1 => GetAttribute(candidates[0]),
                        _ => throw new InvalidCountException(
                            "Multiple versions (multiple overloads (total of " + candidates.Count + ")) found for " + classType + "." + memberName + " which have an attribute defined. " +
                            "You can only call this method with the name of a non-overloaded method, with only one overload having an attribute defined. " +
                            "The versions found where:\r\n" +
                            string.Join("\r\n", candidates.Select(c => c.ToString())) + "\r\n"
                        )
                    };
                })()
            };
        });

        /// <summary>
        /// Non-ordinary method used only from ARCDoc.Documentator.BuildDocumentationHierarchy
        /// Probably not the overload that you want to use.
        /// 
        /// Searches backwards up inheritance hierarchy until finds !<see cref="BaseAttribute.IsDefault"/> attribute
        /// Returns a <see cref="BaseAttribute.IsDefault"/> attribute if reaches end of hierarchy.
        /// </summary>
        /// <param name="memberInfo"></param>
        /// <returns></returns>
        public static ClassMemberAttribute GetAttributeFromBaseClassOfDeclaringType(System.Reflection.MemberInfo memberInfo) {
            var type = memberInfo.DeclaringType.BaseType;
            var signature = memberInfo.ToString();
            while (type != null) {
                var candidates = type.GetMembers().Where(m => m.ToString().Equals(signature)).ToList();
                if (candidates.Count == 0) {
                    // Try base type
                } else if (candidates.Count == 1) {
                    var a = GetAttribute(candidates[0]);
                    if (a.IsDefault) {
                        // Try base type
                    } else {
                        return a;
                    }
                } else {
                    throw new InvalidCountException( // Should never happen because all signatures should be different. 
                        "Unexpected number of methods (total of " + candidates.Count + ") found for " + type + " with signature " + memberInfo + ".  " +
                        "The versions found where:\r\n" +
                        string.Join("\r\n", candidates.Select(c => c.ToString())) + 
                        "\r\n\r\n" +
                        "This is most probably due to a bug in the C# code."
                    );
                }
                type = type.BaseType;
            }
            // Give up
            return new ClassMemberAttribute() { IsDefault = true };
        }

        /// <summary>
        /// Preferred overload
        /// </summary>
        /// <param name="classType"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        public static ClassMemberAttribute GetAttribute(System.Reflection.MemberInfo memberInfo) => _classMemberAttributeCache.GetOrAdd(
            /// Careful with cache key here. <see cref="System.Reflection.MemberInfo.Name"/> is not sufficient 
            /// (same name may exist in different classes, and there may also be overloads within one class)
            memberInfo.DeclaringType + "." + memberInfo.ToString(), s => {
                var retval = GetAttributeThroughMemberInfo<ClassMemberAttribute>(memberInfo);
                retval.IP.AddPV(ClassMemberAttributeP.DeclaringType, memberInfo.DeclaringType); // NOTE: Change from using ReflectedType to DeclaringType 11 Mar 2021.
                retval.IP.AddPV(ClassMemberAttributeP.MethodName, memberInfo.Name);
                retval.IP.AddPV(ClassMemberAttributeP.MethodSignature, memberInfo.ToString());
                retval.Initialize();
                return retval;
            });

        /// <summary>
        /// NOTE: Really not necessary to have as a separate method since only called from <see cref="GetAttribute"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="memberInfo"></param>
        /// <returns></returns>
        private static T GetAttributeThroughMemberInfo<T>(System.Reflection.MemberInfo memberInfo) where T : BaseAttribute, new() =>
            FilterAndAssertAttribute<T>(
                AllAgoRapideAttributeTypes.Select(t => GetCustomAttribute(
                    memberInfo,
                    t,
                    inherit: false // Added parameter inherit: false 11 Mar 2021 in order to NOT inherit attribute from base-class.
                )).Where(a => a != null).Select(a => (BaseAttribute)a),
                memberInfo.DeclaringType.ToStringShort() + "." + memberInfo.Name
            ) ??
            new T() {
                IsDefault = true,
            };

        public override void Initialize() => base.Initialize();
    }

    [Enum(
        AREnumType = AREnumType.PropertyKeyEnum,
        Description = "Describes class -" + nameof(ClassMemberAttribute) + "-."
    )]
    public enum ClassMemberAttributeP {
        __invalid,

        /// <summary>
        /// Will be added by <see cref="ClassMemberAttribute.GetAttribute(System.Reflection.MemberInfo)"/>
        /// NOTE: Change from using ReflectedType to DeclaringType 11 Mar 2021.
        /// </summary>
        [PKType(
            Description =
                "The actual " + nameof(System.Reflection.MemberInfo) + "." + nameof(System.Reflection.MemberInfo.DeclaringType) + ".\r\n" +
                "The class that actually declares the method.\r\n" +
                "This is relevant if method is declared in a base class and used in a derived class.",
            Type = typeof(Type)
        )]
        DeclaringType,

        /// <summary>
        /// Will be added by <see cref="ClassMemberAttribute.GetAttribute(System.Reflection.MemberInfo)"/>
        /// </summary>
        [PKType(Description = "The actual " + nameof(System.Reflection.MemberInfo) + "." + nameof(System.Reflection.MemberInfo.Name) + ".")]
        MethodName,

        /// <summary>
        /// </summary>
        [PKType(Description = "The actual " + nameof(System.Reflection.MemberInfo) + "." + nameof(System.Reflection.MemberInfo.ToString) + ".")]
        MethodSignature,

        // TODO: Delete commented out code
        //[PKType(Description = "TODO: Explain better name for this property. It is memberInfo.ToString(), so why this name?")]
        //ParentClassAttributeId,

        // TODO: Delete commented out code
        ///// Since only used for <see cref = "ClassMemberAttribute" /> for storing the full overload information, 
        ///// placed here instead of PP. But actually not needed since contained in MethodSignatur anyway?
        //[PKType(Description = "TODO: Do we need this")]
        //IdFriendlyDetailed
    }
}