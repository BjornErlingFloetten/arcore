﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;

namespace ARCCore {

    // TODO: Inherit EnumAttribute instead. It is better to tag the 'CustomerP'-enum, instead of the 'Customer'-class.

    // TODO: Will most probably be empty in ARCore. Could be useful in ARCSec, AccessLevelWrite / Read for instance.

    // TODO: Implement getting of attributes.

    [Class(Description =
        "Describes -" + nameof(IP) + "- classes (describes at class-level)\r\n" +
        "\r\n" +
        "See the analogue -" + nameof(BasePKAttribute) + "- which describes properties of classes (describes at property level).\r\n" +
        "\r\n" +
        "Note that no sub-classes are included in -" + nameof(ARComponents.ARCCore) + "-."
    )]
    // TODO: Do we really want to inherit ClassAttribute? Would not EnumAttribute be better? And reuse that functionality. 
    public abstract class IPAttribute { //
        // INHERITING OF 
        // : ClassAttribute {  // TODO: Inheriting EnumAttribute would be more consistent with how BasePKAttribute inherits EnumMemberAttribute.
        // REMOVED 2 MAY 2020 BECAUSE CORRESPONDING enum IPAttributeP not defined either.
       
        /// If putting back, check code in <see cref="BaseAttribute.AllAgoRapideAttributeTypes"/>
    }
}
