﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;

namespace ARCCore
{

    [Class(Description =
            "Describes a class.\r\n" +
            "\r\n" +
            "Individual class members are described by -" + nameof(ClassMemberAttribute) + "-."
    )]
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
    public class ClassAttribute : BaseAttribute
    {

        private static ConcurrentDictionary<Type, ClassAttribute> _classAttributeCache = new ConcurrentDictionary<Type, ClassAttribute>();
        [ClassMember(Description =
            "Throws exception if type implements -" + nameof(ARCCore.IP) + "- and " +
            "no corresponding enum found (enum ending with capital letter 'P' (like 'CustomerP' for enum describing class 'Customer' for instance)"
        )]
        public static ClassAttribute GetAttribute(Type type) => _classAttributeCache.GetOrAdd(type, t =>
        {
            try
            {
                REx.Inc();
                OfTypeEnumException.AssertNotEnum(type);
                var retval = GetAttributeThroughType<ClassAttribute>(type);

                retval.IP.AddPV(ClassAttributeP.AssemblyName, type.Assembly.GetName().Name);
                if (type.Namespace != null) retval.IP.AddPV(ClassAttributeP.ClassNamespace, type.Namespace);
                retval.IP.AddPV(ClassAttributeP.ClassType, type);

                var baseClasses = new List<Type>();

                var interfacesImplementedInBaseClasses = new HashSet<Type>();
                var it = type.BaseType; while (it != null && !typeof(object).Equals(it))
                {
                    baseClasses.Add(it);
                    it.GetInterfaces().ForEach(t => interfacesImplementedInBaseClasses.Add(t));
                    it = it.BaseType;
                }
                baseClasses.Reverse();
                if (baseClasses.Count > 0) retval.IP.AddPV(ClassAttributeP.BaseTypes, baseClasses);

                var interfaces = type.GetInterfaces().ToList();
                if (interfaces.Count > 0)
                {
                    retval.IP.AddPV(ClassAttributeP.Interfaces, interfaces);
                    var interfacesDirectlyImplemented = interfaces.Where(t => !interfacesImplementedInBaseClasses.Contains(t)).ToList();
                    if (interfacesDirectlyImplemented.Count > 0)
                    {
                        retval.IP.AddPV(ClassAttributeP.InterfacesDirectlyImplemented, interfacesDirectlyImplemented);
                    }
                }

                if (type.GetInterfaces().Contains(typeof(IP)))
                { /// Find <see cref="ClassAttributeP.CorrespondingEnumType"/> (if exists, if not just give up)
                    var strClassTypeWithPAppended = type.ToStringVeryShort() + "P";
                    var correspondingEnums = PK.AllPKEnums.Where(t =>
                        (t.ToStringVeryShort()).Equals(strClassTypeWithPAppended)).ToList();

                    switch (correspondingEnums.Count)
                    {
                        case 0:
                            // Restriction below is too hard / too restrictive. It does not give anything of value. 
                            // There are currently (May 2020) around 4 classes in AgoRapide which do not have 
                            // the corresponding enum declared, so enforcing restriction below only leads to unnecessary code.
                            //throw new ClassAttributeException(
                            //    "No corresponding enum found for class " + type.ToStringShort() + " which implements " + nameof(ARCCore.IP) + ".\r\n" +
                            //    "The class " + type.ToStringShort() + " implements " + nameof(ARCCore.IP) + ".\r\n" +
                            //    "A corresponding enum called " + strClassTypeWithPAppended + " is supposed to be declared alongside it, but was not found.\r\n" +
                            //    "\r\n" +
                            //    "Possible causes:\r\n" +
                            //    "1) Did you forget to implement enum " + strClassTypeWithPAppended + "?\r\n" +
                            //    "\r\n" +
                            //    "Resolution: Implement enum " + strClassTypeWithPAppended + "\r\n" +
                            //    "\r\n"
                            //);

                            // Instead we just give up
                            break;
                        case 1:
                            /// TOOD: Consider asserting that the enum is correctly tagged as <see cref="AREnumType.PropertyKeyEnum"/>
                            /// TODO: but beware of 'chicken-and-egg' problems, because if will quite possible lead to some
                            /// TODO: recursivity regarding reading of attributes.
                            retval.IP.AddPV(ClassAttributeP.CorrespondingEnumType, correspondingEnums[0]);
                            break; // OK
                        default:
                            throw new ClassAttributeException(
                                "Multiple corresponding enums found for class " + type.ToStringShort() + ", which implements " + nameof(ARCCore.IP) + ".\r\n" +
                                "\r\n" +
                                "The class " + type.ToStringShort() + " implements " + nameof(ARCCore.IP) + ".\r\n" +
                                "\r\n" +
                                "A single corresponding enum called " + strClassTypeWithPAppended + " is supposed to be declared alongside it, " +
                                "but multiple were found:\r\n" +
                                string.Join("\r\n", correspondingEnums.Select(t => t.ToStringDB())) + "\r\n" +
                                "\r\n" +
                                "Note that even if they reside in different namespaces this is not allowed because of a desire to keep the -" + nameof(ARConcepts.PropertyStream) + "- as compact as possible.\r\n" +
                                "Possible resolution: Rename duplicate enums.\r\n");
                    }
                }
                retval.Initialize();
                return retval;
            }
            finally
            {
                REx.Dec();
            }
        });

        public override void Initialize() => base.Initialize();

        [ClassMember(Description =
            "Generates typescript code corresponding to the class which this instance describes.\r\n" +
            "\r\n" +
            "Only relevant for classes implementing -" + nameof(IP) + "- (with -" + nameof(ClassAttributeP.CorrespondingEnumType) + "-.\r\n"
        )]
        public bool TryGetAsTypeScript(out string typeScript, out string errorResponse)
        {
            if (!IP.TryGetPV<Type>(ClassAttributeP.CorrespondingEnumType, out var t, out errorResponse))
            {
                typeScript = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }

            typeScript =
                "// Autogenerated code. Generated at " + UtilCore.DateTimeNow.ToStringDateAndTime() + " UTC.\r\n" +
                "\r\n" +
                (
                    !IP.TryGetPV<string>(BaseAttributeP.Description, out var description) ? "" :
                    (
                        "/*\r\n" +
                        description + "\r\n" +
                        "*/\r\n"
                    )
                ) +
                "public class " + IP.GetPV<Type>(ClassAttributeP.ClassType).ToStringVeryShort() + " {\r\n" +
                "\r\n" +
                string.Join("", UtilCore.EnumGetMembers(t).Select(e =>
                {
                    var pk = PK.FromEnum(e);
                    var pkType = pk.Type.ToStringVeryShort();
                    var propertyName = pk.ToString();
                    var pkta = (IP)pk.GetA<PKTypeAttribute>() ?? throw new NullReferenceException(
                        "pkta, should not happen, check implementation of " + nameof(PK.GetA));
                    var isList = pkta.GetPV<Cardinality>(PKTypeAttributeP.Cardinality, Cardinality.__invalid) == Cardinality.WholeCollection;
                    return
                        (
                            !pk.IP.TryGetPV<string>(BaseAttributeP.Description, out var description) ? "" :
                            (
                                "/*\r\n" +
                                description + "\r\n" +
                                "*/\r\n"
                            )
                        ) +
                        (pkta.GetPV<bool>(PKTypeAttributeP.IsObligatory, false) ? "// IsObligatory\r\n" : "") +
                        "  public " + (isList ? "List<" : "") + pkType + (isList ? ">" : "") + " " + propertyName + ";\r\n\r\n";
                })) +
                "}";

            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public class ClassAttributeException : ApplicationException
        {
            public ClassAttributeException(string message) : base(message) { }
        }
    }

    [Enum(
        AREnumType = AREnumType.PropertyKeyEnum,
        Description = "Describes class -" + nameof(ClassAttribute) + "-."
    )]
    public enum ClassAttributeP
    {
        __invalid,

        [PKType(Description =
            "The assembly in which this class resides."
        )]
        AssemblyName,

        [PKType(
            Description = "The namespace for -" + nameof(ClassType) + "-.",
            Type = typeof(Type)
        )]
        ClassNamespace,

        [PKType(
            Description = "The actual type that we are a -" + nameof(ClassAttribute) + "- for",
            Type = typeof(Type)
        )]
        ClassType,

        [PKType(
            Description =
                "The inheritance hierarchy for this class, starting with base type (excluding 'object').r\n" +
                "Not set if class does not inherit any class except 'object'",
            Type = typeof(Type),
            Cardinality = Cardinality.WholeCollection
        )]
        BaseTypes,

        [PKType(
            Description =
                "Direct and indirect sub types of this class.\r\n" +
                "\r\n" +
                "Not set if no sub type exists for this class.\r\n" +
                "\r\n" +
                "Populated from outside. Typically from -" + nameof(ARComponents.ARCDoc) + "- " +
                "when building documentation hierarchy (creating table of content).",
            Type = typeof(Type),
            Cardinality = Cardinality.WholeCollection
        )]
        SubTypes,

        [PKType(
            Description =
                "All interfaces that this class and its base classes implement\r\n" +
                "(as given by -" + nameof(Type.GetInterfaces) + "-.)\r\n" +
                "Not set if no interfaces implemented.\r\n",
            Type = typeof(Type),
            Cardinality = Cardinality.WholeCollection
        )]
        Interfaces,

        [PKType(
            Description =
                "All interfaces that this class directly implements.\r\n" +
                "(interfaces implemented by -" + nameof(BaseTypes) + "- are not included).",
            Type = typeof(Type),
            Cardinality = Cardinality.WholeCollection
        )]
        InterfacesDirectlyImplemented,

        [PKType(
            Description =
                "Only relevant when implements -" + nameof(ARCCore.IP) + "-. " +
                "\r\n" +
                "The corresponding -" + nameof(AREnumType.PropertyKeyEnum) + "- with values describing this class.\r\n" +
                "(like this enum -" + nameof(ClassAttributeP) + "- describes -" + nameof(ClassAttribute) + "-). " +
                "\r\n" +
                "The enum must have the same name, with capital letter 'P' appended (like CustomerP and Customer for instance). " +
                "This requirement is enforced by -" + nameof(EnumAttribute.GetAttribute) + "-.",
            LongDescription =
                "Automatically inserted by -" + nameof(ClassAttribute.GetAttribute) + "- if found.\r\n" +
                "See also -" + nameof(EnumAttributeP.CorrespondingClass) + "-.",
            Type = typeof(Type)
        )]
        CorrespondingEnumType,

        [PKType(Description =
                "Only relevant when implements -" + nameof(ARCCore.IP) + "- and when a -" + nameof(CorrespondingEnumType) + "- is found.\r\n" +
                "\r\n" +
                "Automatically generated boilerplate code that implements storage (and also traditional setters and getters) for properties of this class.\r\n" +
                "\r\n" +
                "Paste this code into your class if you find the default -" + nameof(ARCCore.IP) + "- implementations too memory hungry " +
                "(see -" + nameof(ARConcepts.MemoryConsumption) + "-).\r\n" +
                "\r\n" +
                "The code provided through this property will also eliminate the performance impact " +
                "due to the complicated -" + nameof(ARConcepts.PropertyAccess) + "- mechanism.\r\n" +
                "\r\n" +
                "Background: The typical -" + nameof(ARCCore.IP) + "- implementations are a bit memory hungry due the generic mechanism used. " +
                "This can be alleviated by implementing traditional object storage, " +
                "that is, by dispensing of the typical Dictionary<IK, IP> Properties collection like the one used in -" + nameof(PRich) + "-, " +
                "and just implement -" + nameof(ARCCore.IP) + "- on top of such a traditional structure. " +
                "\r\n" +
                "You can do this as needed for a few select 'entity' classes as your application grows " +
                "(that is, for the classes for which you see a huge number of instances consuming excessive memory).\r\n" +
                "(-" + nameof(ARComponents.ARCDoc) + "- gives some examples of this).\r\n" +
                "\r\n" +
                "The practical result of using this code is that your class turns into a quite traditional C# class " +
                "regarding property access and memory usage, but you still retain the general -" + nameof(ARConcepts.PropertyStream) + "- compatibility " +
                "(serialization / deserialization to / from the property stream)." +
                "(Note however, that you do NOT retain the possibility for storing neta-information for a property, " +
                "like who created it or how old it is (see -" + nameof(PP) + "- like -" + nameof(PP.Cid) + "- and -" + nameof(PP.Created) + "-).\r\n" +
                "\r\n" +
                "TODO: Not implemented as of May 2020.\r\n" +
                "\r\n" +
                "Note that in order to get all the rich functionality of -" + nameof(ARConcepts.PropertyAccess) + "- " +
                "a quite large number of methods have to be implemented.\r\n" +
                "\r\n" +
                "Automatically inserted by -" + nameof(ClassAttribute.GetAttribute) + "-.\r\n" +
                "\r\n" +
                "See also -" + nameof(PKTypeAttributeP.BoilerplateCodeProperties) + "-."
        )]
        BoilerplateCodePropertiesAndStorage,
    }
}