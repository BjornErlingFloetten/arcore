﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;

namespace ARCCore
{

    [Class(Description =
        "Describes an enum.\r\n" +
        "\r\n" +
        "Enum members (the individual values) are described by -" + nameof(EnumMemberAttribute) + "-."
    )]
    [AttributeUsage(AttributeTargets.Enum)]
    public class EnumAttribute : BaseAttribute
    {

        // TODO: Add AssertInitializationNotCompleteForAttributeAccess
        /// <summary>
        /// See <see cref="EnumAttributeP.AREnumType"/>
        /// </summary>
        public AREnumType AREnumType { get; set; }

        private static readonly ConcurrentDictionary<Type, EnumAttribute> _enumAttributeCache = new ConcurrentDictionary<Type, EnumAttribute>();

        [ClassMember(Description =
            "Throws exception if tagged as -" + nameof(AREnumType.PropertyKeyEnum) + "- and " +
            "name of type does not end with capital letter 'P' (like 'CustomerP' for enum describing class 'Customer' for instance).\r\n" +
            "Also throws exception (when tagged as -" + nameof(AREnumType.PropertyKeyEnum) + "-) if the corresponding class is not found."
        )]
        public static EnumAttribute GetAttribute(Type type) => _enumAttributeCache.GetOrAdd(type, t =>
        {
            NotOfTypeEnumException.AssertEnum(type);
            var retval = GetAttributeThroughType<EnumAttribute>(type);
            retval.IP.AddPV(EnumAttributeP.AssemblyName, type.Assembly.GetName().Name);
            retval.IP.AddPV(EnumAttributeP.EnumNamespace, type.Namespace);
            retval.IP.AddPV(EnumAttributeP.EnumType, type);
            retval.IP.AddPV(EnumAttributeP.AREnumType, retval.AREnumType);

            if (retval.AREnumType == AREnumType.PropertyKeyEnum)
            { /// Find <see cref="EnumAttributeP.CorrespondingClass"/>
                var enumTypeToStringVeryShort = type.ToStringVeryShort();

                if (!enumTypeToStringVeryShort.EndsWith("P")) throw new EnumAttributeException(
                    "Invalid name for " + nameof(AREnumType) + "." + nameof(AREnumType.PropertyKeyEnum) + " " + type.ToStringShort() + ".\r\n" +
                    "The enum " + type.ToStringShort() + " is tagged as being of " + nameof(AREnumType) + "." + nameof(AREnumType.PropertyKeyEnum) + " \r\n" +
                    "but its name does not end with a capital letter 'P'.\r\n" +
                    "Resolution: Either change the naming or remove the tagging.\r\n");

                var enumTypeToStringVeryShortWithoutLastP = enumTypeToStringVeryShort.Substring(0, enumTypeToStringVeryShort.Length - 1); // Remove last 'P'
                var correspondingClasses = IP.AllIPDerivedTypesInludingGenericAndAbstract.Where(t =>
                    t.ToStringVeryShort().Equals(enumTypeToStringVeryShortWithoutLastP)).ToList();
                switch (correspondingClasses.Count)
                {
                    case 0:
                        throw new EnumAttributeException(
                            "No corresponding 'entity'-class (" + nameof(ARCCore.IP) + " derived class) found for " + nameof(AREnumType) + "." + nameof(AREnumType.PropertyKeyEnum) + " " + type.ToStringShort() + ".\r\n" +
                            "The enum " + type.ToStringShort() + " is tagged as being of " + nameof(AREnumType) + "." + nameof(AREnumType.PropertyKeyEnum) + " \r\n" +
                            "A corresponding class called " + enumTypeToStringVeryShortWithoutLastP + " (implementing -" + nameof(IP) + "-) is supposed to be declared alongside it, but was not found.\r\n" +
                            "\r\n" +
                            "Possible causes:\r\n" +
                            "1) Did you maybe implement " + enumTypeToStringVeryShortWithoutLastP + " as an inner class to another class?\r\n" +
                            "2) Did you forget to declare that it inherits IP?\r\n" +
                            "\r\n" +
                            "Resolution: Either remove the tagging of " + type.ToStringShort() + " or implement the class " + enumTypeToStringVeryShortWithoutLastP + " like for instance\r\n" +
                            "'public class " + enumTypeToStringVeryShortWithoutLastP + " : " + nameof(ARCCore.IP) + " { }'\r\n" +
                            "or (much more typically):\r\n" +
                            "'public class " + enumTypeToStringVeryShortWithoutLastP + " : " + nameof(PRich) + " { }'\r\n" +
                            "." +
                            "\r\n"
                        );
                    case 1:
                        retval.IP.AddPV(EnumAttributeP.CorrespondingClass, correspondingClasses[0]);
                        break; // OK
                    default:
                        throw new EnumAttributeException(
                            "Multiple 'entity'-classes (" + nameof(ARCCore.IP) + " derived classes) found for " + nameof(AREnumType) + "." + nameof(AREnumType.PropertyKeyEnum) + " " + type.ToStringShort() + ".\r\n" +
                            "The enum " + type.ToStringShort() + " is tagged as being of " + nameof(AREnumType) + "." + nameof(AREnumType.PropertyKeyEnum) + " \r\n" +
                            "A single corresponding class called " + enumTypeToStringVeryShortWithoutLastP + " is supposed to be declared alongside it, but multiple were found " +
                            "(" + string.Join(" // ", correspondingClasses.Select(t => t.ToStringDB())) + ").\r\n" +
                            "Note that even if they reside in different namespaces this is not allowed because of a desire to keep the -" + nameof(ARConcepts.PropertyStream) + "- as compact as possible.\r\n" +
                            "Resolution: Either remove the tagging or remove superfluous classes.\r\n");
                }
            }
            retval.Initialize();
            return retval;
        });

        public override void Initialize() => base.Initialize();

        [ClassMember(Description =
            "Generates typescript code corresponding to the enum which this instance describes.\r\n" +
            "\r\n" +
            "Not relevant for -" + nameof(AREnumType.PropertyKeyEnum) + "-, those are described by -" + nameof(ClassAttribute.TryGetAsTypeScript) + "-.\r\n"
        )]
        public bool TryGetAsTypeScript(out string typeScript, out string errorResponse)
        {
            if (IP.GetPV<AREnumType>(EnumAttributeP.AREnumType, ARCCore.AREnumType.__invalid) == AREnumType.PropertyKeyEnum)
            {
                typeScript = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Irrelevant for " + AREnumType.PropertyKeyEnum + ", because those are described by -" + nameof(ClassAttribute.TryGetAsTypeScript) + "-";
                return false;
            }

            var t = IP.GetPV<Type>(EnumAttributeP.EnumType);
            typeScript =
                "// Autogenerated code. Generated at " + UtilCore.DateTimeNow.ToStringDateAndTime() + " UTC.\r\n" +
                "\r\n" +
                (
                    !IP.TryGetPV<string>(BaseAttributeP.Description, out var description) ? "" :
                    (
                        "/*\r\n" +
                        description + "\r\n" +
                        "*/\r\n"
                    )
                ) +
                "public enum " + t.ToStringVeryShort() + " {\r\n" +
                "\r\n" +
                string.Join("", UtilCore.EnumGetMembers(t).Select(e =>
                {
                    var a = EnumMemberAttribute.GetAttribute(e);
                    return
                        (
                            !a.IP.TryGetPV<string>(BaseAttributeP.Description, out var description) ? "" :
                            (
                                "/*\r\n" +
                                description + "\r\n" +
                                "*/\r\n"
                            )
                        ) +
                        "  " + e.ToString() + ";\r\n\r\n";
                })) +
                "}";
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public class EnumAttributeException : ApplicationException
        {
            public EnumAttributeException(string message) : base(message) { }
        }
    }

    [Enum(
        AREnumType = ARCCore.AREnumType.PropertyKeyEnum,
        Description = "Describes class -" + nameof(EnumAttribute) + "-."
    )]
    public enum EnumAttributeP
    {
        __invalid,

        [PKType(Description =
            "The assembly in which this class resides."
        )]
        AssemblyName,

        [PKType(
            Description = "The namespace for -" + nameof(EnumType) + "-.",
            Type = typeof(Type)
        )]
        EnumNamespace,

        [PKType(
            Description = "The type of the enum (like type of 'CustomerP' in CustomerP.FirstName) that we are an attribute for",
            Type = typeof(Type)
        )]
        EnumType,

        [PKType(
            Description = "Note that will default to -" + nameof(__invalid) + "- if not explicit set.",
            Type = typeof(ARCCore.AREnumType))]
        AREnumType,

        [PKType(
            Description =
                "Only relevant when -" + nameof(ARCCore.AREnumType.PropertyKeyEnum) + "-. " +
                "\r\n" +
                "The corresponding 'entity'-class (-" + nameof(IP) + "--derived class) that the enum describes " +
                "(like this enum -" + nameof(EnumAttributeP) + "- describes -" + nameof(EnumAttribute) + "-). " +
                "\r\n" +
                "The class must have the same name, except without the last capital letter 'P' (like CustomerP and Customer for instance). " +
                "This requirement is enforced by -" + nameof(EnumAttribute.GetAttribute) + "-.",
            LongDescription =
                "Automatically inserted by -" + nameof(EnumAttribute.GetAttribute) + "-.\r\n" +
                "See also -" + nameof(ClassAttributeP.CorrespondingEnumType) + "-.",
            Type = typeof(Type)
        )]
        CorrespondingClass,
    }
}
