﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using System.Linq;

namespace ARCCore {
    [Class(
        Description =
            "The actual connection, its internal state, queue for sending and so on.\r\n",
        LongDescription =
            "TODO: May 2020: The code in this class should be tidied up a bit before any more functionality is added.\r\n" +
            "\r\n" +
            "Note how this class inherits -" + nameof(PConcurrent) + "- which offers thread-safe access to properties " +
            "(and is also able to -" + nameof(ARConcepts.ExposingApplicationState) + "- to outside)."
    )]
    public class ActualConnection : PConcurrent {

        public System.Threading.Tasks.Task? Task { get; set; }

        public System.Net.Sockets.TcpClient? Client { get; set; }

        private System.IO.StreamWriter? Writer { get; set; } // TODO: Make this private set if possible!

        private readonly ConcurrentQueue<string> _queueSend = new ConcurrentQueue<string>();

        private Func<ClientUpdatePosition>? _myUpdatePositionGenerator;
        [ClassMember(Description =
            "Set by -" + nameof(StreamProcessor) + "-.\r\n" +
            "Not relevant for -" + nameof(ConnectionDirection.Incoming) + "-."
        )]
        public void SetMyUpdatePositionGenerator(Func<ClientUpdatePosition> r) => _myUpdatePositionGenerator = r;

        private Action<string>? _singlePropertyStreamLineReceiver;
        [ClassMember(Description =
            "Set by -" + nameof(StreamProcessor) + "-.\r\n" +
            "Relevant for both -" + nameof(ConnectionDirection.Incoming) + "- and -" + nameof(ConnectionDirection.Outgoing) + "-."
        )]
        public void SetSinglePropertyStreamReceiver(Action<string> r) => _singlePropertyStreamLineReceiver = r;

        private Func<ClientUpdatePosition>? _upToDatePositionGenerator;
        [ClassMember(Description =
            "Set by -" + nameof(StreamProcessor) + "-. Not used by -" + nameof(ARNodeType.Client) + "-.\r\n" +
            "Not relevant for -" + nameof(ConnectionDirection.Outgoing) + "-." +
            "\r\n" +
            "Will generate the up-to-date position for the property stream.\r\n" +
            "\r\n" +
            "Necessary in order for the server to turn -" + nameof(ARCCore.ClientUpdatePosition.OnlyNewDataIsRequested) + "- into correct position as soon as it is receive by client " +
            "(it would for instance be too late if -" + nameof(StreamProcessor) + "- tried to do it by itself when updating clients, " +
            "because new data may have been received in the meantime, meaning position would have been set 'too-far' into the property stream)."
        )]
        public void SetUpToDatePositionGenerator(Func<ClientUpdatePosition> s) => _upToDatePositionGenerator = s;

        [ClassMember(Description = "Note parameter-less constructor as desired for all classes inheriting -" + nameof(IP) + "-.")]
        public ActualConnection() { }

        [ClassMember(Description = "Relevant for -" + nameof(ConnectionDirection.Outgoing) + "-.")]
        public void InitializeOutgoingConnection() {
            IP.LogText("");
            System.Threading.Tasks.Task.Factory.StartNew(() => {
                try {
                    System.Threading.Thread.CurrentThread.Name = nameof(InitializeOutgoingConnection) + "_" + IP.GetPV<string>(ActualConnectionP.Id);
                    System.Threading.Thread.CurrentThread.IsBackground = true; // TODO: In principle we should set this BEFORE thread is started

                    IP.SetPV(ActualConnectionP.UnderClosure, false); // Relevant if called because value was TRUE
                    IP.SetPV(PP.Created, UtilCore.DateTimeNow);
                    var i = IP.GetPVM<ConnectionInstruction>().IP;
                    IP.LogText("AttemptingConnect");
                    IP.Log(new List<object> { ConnectionInstructionP.ServerHostName, ConnectionInstructionP.ServerPortNo }, i);
                    Client = new System.Net.Sockets.TcpClient(
                        i.GetPV<string>(ConnectionInstructionP.ServerHostName),
                        i.GetPV<int>(ConnectionInstructionP.ServerPortNo)
                        );
                    Task = Communicate();
                } catch (System.Net.Sockets.SocketException ex) {
                    IP.HandleException(ex);
                } finally {
                    // Was this misunderstood. What happens at 'Task = HandleConnectionAsync();' above?
                    // IP.SetPV(ActualConnectionP.UnderClosure, true); // Probably already set by HandleConnectionAsync
                }
            });
        }

        [ClassMember(
            Description =
                "Common for both -" + nameof(ConnectionDirection.Incoming) + "- and -" + nameof(ConnectionDirection.Outgoing) + "-.\r\n" +
                "-" + nameof(Client) + "- must be set.\r\n" +
                "Sets -" + nameof(Writer) + "- in order for -" + nameof(Send) + "- to work."
        )]
        public async System.Threading.Tasks.Task Communicate() {
            // TODO: Improve reading of IP-address.  
            IP.SetPV(ActualConnectionP.PartnerIPAddress, Client?.Client.RemoteEndPoint.ToString().Split(":")[0] ?? throw new NullReferenceException(nameof(Client) + "\r\n"));
            var closingReason = "UNKNOWN";
            try {
                using var networkStream = Client?.GetStream() ?? throw new NullReferenceException(nameof(Client) + "\r\n");
                using var r = new System.IO.StreamReader(networkStream, UtilCore.DefaultAgoRapideEncoding);
                using var w = new System.IO.StreamWriter(networkStream, UtilCore.DefaultAgoRapideEncoding) { AutoFlush = true };

                ///
                /// TODO: Note how both reader and writer works directly against the TCP/IP connection without any security at all.
                /// TODO: 
                /// TODO: In the future it must be possible to use <see cref="ARComponents.ARCSec"/> as an intermediate 
                /// TODO: for SSL, authentication and authorization. 
                /// TODO: In order to support this, we must add some abstraction mechanism in the form of an interface or similar, 
                /// TODO: that can be injected into this class. The interface can be defined in <see cref="ARComponents.ARCCore"/> 
                /// TODO: and the implementation can reside in <see cref="ARComponents.ARCSec"/> (keeping LOC down in the core).
                /// TODO: The default implementation (without any security) in <see cref="ARComponents.ARCCore"/> will probably
                /// TODO: just be a wrapper class around the .NET classes used now.
                ///
                
                Writer = w;

                if (IP.TryGetPVM<ConnectionInstruction>(out var ci) && ci.IP.GetPVM<DataTransferDirection>() == DataTransferDirection.Receive) {
                    /// We are an outgoing connection for receiving data 
                    /// (we are most probably a <see cref="ARNodeType.Client"/> or an <see cref="ARNodeType.ArmDB"/>. 
                    /// Send update status and subscription at once to server.

                    // TODO: Add some mechanism ensuring that this information will actually be received by server.
                    // TODO: Maybe some healtcheck functionality, where both parties (both sides of the connection) can assert that status is as expected.

                    ci.IP.GetP<IP>(ConnectionInstructionP.Subscription).
                        // TODO: What about IP default method for combination of GetP and ToPropertyStream?
                        ToPropertyStream(currentIK: new List<IK> {
                                IKString.FromCache("ThisConnection"), // TODO: Turn "ThisConnection" into something more strictly typed
                                PK.FromEnum(ActualConnectionP.SubscriptionAsRequestedByClient)
                        }, 
                        retval: new List<string>()).
                        // TODO: Replace complicated call to ToPropertyStream above with simpler
                        // TODO: s => Send("ThisConnection/SubscriptionAsRequestedByClient/" + s)) 
                        // TODO: here
                        ForEach(s => Send(s));

                    new PValue<ClientUpdatePosition>(_myUpdatePositionGenerator?.Invoke() ?? throw new NullReferenceException(nameof(_myUpdatePositionGenerator) + ": Should have been set by call to -" + nameof(SetMyUpdatePositionGenerator) + "- from -" + nameof(StreamProcessor) + "-.\r\n")).
                        // TODO: What about IP default method for combination of GetP and ToPropertyStream?
                        ToPropertyStream(
                        currentIK: new List<IK> {
                                IKString.FromCache("ThisConnection"), // TODO: Turn "ThisConnection" into something more strictly typed
                                PK.FromEnum(StreamProcessorP.ClientUpdatePosition)
                        }, 
                        retval: new List<string>()).
                        // TODO: Replace complicated call to ToPropertyStream above with simpler
                        // TODO: s => Send("ThisConnection/ClientUpdatePosition/" + s)) 
                        // TODO: here
                        ForEach(s => Send(s));
                }

                while (true) {
                    var readTask = r.ReadLineAsync();
                    var readOrTimeout = await System.Threading.Tasks.Task.WhenAny(readTask, GetDelayTask());
                    if (System.Threading.Thread.CurrentThread.Name == null) {
                        // TODO: Decide meaning of this. 
                        // TODO: Because this is not "our" thread, we assume that it is coming from the Thread pool...
                        // TODO: therefore, can it have other names set already?
                        System.Threading.Thread.CurrentThread.Name = nameof(readOrTimeout);
                    }
                    if (readOrTimeout != readTask) {
                        IP.LogText("Timeout");
                        closingReason = "TIMEOUT";
                        IP.SetPV(ActualConnectionP.UnderClosure, true);
                        break;
                    }
                    var dataReceived = await (System.Threading.Tasks.Task<string>)readOrTimeout;

                    // Old method
                    // System.Threading.Interlocked.Increment(ref CountReceiveMessage);
                    IP.Inc(ActualConnectionP.CountReceiveMessage); // Note: Not thread-safe

                    // This is too frequent, generating a heartbeat for EVERY message received.
                    // IP.SetPV(PP.Heartbeat, Util.DateTimeNow); // TODO: Is this the proper place to register heartbeat? Probably not!

                    if (dataReceived == null) { // Note that VS Intellisense 'lies' about dataReceived not being null here...
                        IP.LogText("No data received");
                        closingReason = "Connection assumed terminated by partner (in the form of 'no data received')";
                        break;
                    }

                    if ("quit".Equals(dataReceived.ToLower())) {
                        IP.LogText("CloseByPartner");
                        closingReason = "'quit' received from partner)";
                        break;
                    }

                    if (dataReceived.StartsWith("ThisConnection/")) { // TODO: Turn "ThisConnection" into something more strictly typed
                        /// Exceptional case, information is not meant to be sent to StreamProcessor. Instead it is meant for us.
                        /// 
                        /// ThisConnection/SubscriptionAsRequestedByClient) 
                        /// ThisConnection/ClientUpdatePosition
                        /// ThisConnection/ErrorCondition

                        if (!PropertyStreamLine.TryParseDirectToIP<ActualConnection>(dataReceived["ThisConnection/".Length..], out var instruction, out var errorResponse)) {
                            LogAndSendErrorConditionData(
                                logObject: this,
                                context: new List<string> {
                                    "InvalidThisConnection",
                                    "InvalidInstruction"
                                },
                                errorData: new List<(string, string)> { /// Adhere to AgoRapide <see cref="ARConcepts.Logging"/>-principle of logging data-points, not loose text
                                    ("Id", IP.GetPV<string>(ActualConnectionP.Id)),
                                    (nameof(errorResponse), errorResponse),
                                    (nameof(dataReceived), dataReceived)
                                }
                            );
                        } else {
                            InvalidObjectTypeException.AssertEquals(instruction, typeof(ActualConnection)); // Assertion is inserted for clarification. It does not matter what type is, as long as properties are present.

                            if (instruction.IP.TryGetPV<List<Subscription>>(ActualConnectionP.SubscriptionAsRequestedByClient, out var subscription)) {
                                IP.SetPV(ActualConnectionP.SubscriptionAsRequestedByClient, subscription);
                            } else if (instruction.IP.TryGetPV<ClientUpdatePosition>(ActualConnectionP.ClientUpdatePosition, out var p)) {
                                // It is important to set the correct value now at once, before new messages are being received, so that
                                // client receives from expected position
                                // "(it would for instance be too late if StreamProcessor tried to do it by itself when updating clients, " +
                                // "because new data may have been received in the meantime, meaning position would have been set 'too-far' into the property stream)."
                                if (p.OnlyNewDataIsRequested) {
                                    p = _upToDatePositionGenerator?.Invoke() ?? throw new NullReferenceException(nameof(_upToDatePositionGenerator) + ": Should have been set by call to -" + nameof(SetUpToDatePositionGenerator) + "- from -" + nameof(StreamProcessor) + "-.\r\n");
                                }
                                if (p.ClientDatastoreIsEmpty) {
                                    // This case can be handled by StreamProcessor without any problem
                                }
                                IP.SetPV(ActualConnectionP.ClientUpdatePosition, p);
                            } else if (instruction.IP.TryGetPV<string>(ActualConnectionP.ErrorConditionData, out var e)) {
                                /// Note that StreamProcessor will most probably receive this information anyway due to logging being done now
                                IP.LogKeyValue(PK.FromEnum(ActualConnectionP.ErrorConditionData), e);
                                IP.SetPV(ActualConnectionP.IsErrorCondition, true);

                                // Do not close now because there might be more data describing the error condition still to be received
                                //closingReason = nameof(ActualConnectionP.ErrorCondition) + " received by partner";
                                //break;
                                // TDOO: IMPLEMENT ELSEWHERE REINSTATEMENT OF CONNECTION NOW
                            } else {
                                LogAndSendErrorConditionData(
                                    logObject: this,
                                    context: new List<string> {
                                    "InvalidThisConnection",
                                    "UnknownInstruction"
                                    },
                                    errorData: new List<(string, string)> { /// Adhere to AgoRapide <see cref="ARConcepts.Logging"/>-principle of logging data-points, not loose text
                                    ("Id", IP.GetPV<string>(ActualConnectionP.Id)),
                                    (nameof(dataReceived), dataReceived),
                                    ("Details" , "Expected one of " + nameof(ActualConnectionP.SubscriptionAsRequestedByClient) + ", " + nameof(ActualConnectionP.ClientUpdatePosition) + " or " + nameof(ActualConnectionP.ErrorConditionData) + ".")
                                    }
                                );
                            }
                        }
                    } else {
                        // Ordinary case, 
                        if (_singlePropertyStreamLineReceiver == null) throw new NullReferenceException(nameof(_singlePropertyStreamLineReceiver) + ": Should have been set by call to -" + nameof(SetSinglePropertyStreamReceiver) + "- from -" + nameof(StreamProcessor) + "-.\r\n");
                        _singlePropertyStreamLineReceiver(dataReceived);
                    }
                } // while true
            } catch (Exception ex) {
                IP.LogKeyValue("Exception", ex.GetType().ToStringShort());
                closingReason = ex.GetType().ToString();

                // Unnecessary because will be logged anyway when setting closing reason below
                // IP.Log(nameof(closingReason), closingReason);

                if (ex is System.IO.IOException) { // Typically happens when the partner goes offline
                    IP.LogText("Not showing any details (assuming 'normal' exception)");
                    // IP.HandleException(ex);
                } else if ((ex is System.ObjectDisposedException) && IP.GetPV<bool>(ActualConnectionP.UnderClosure)) {
                    // Assumed no. TODO: Find out exactly why happens? Is it Connection.Send closing connections because exceptions happen?
                    IP.LogText(nameof(ActualConnectionP.UnderClosure) + " was already TRUE, therefore not showing any details (assuming 'normal' exception)");
                } else {
                    IP.LogText("Details are logged separately");
                    IP.HandleException(ex);
                }
            } finally {
                IP.LogText("Closing");
                IP.SetPV(ActualConnectionP.ClosingReason, closingReason);
                IP.SetPV(ActualConnectionP.UnderClosure, true);
                try { Client?.Close(); } catch (Exception) { }
            }
        }

        public void LogAndSendErrorConditionData(IP logObject, IEnumerable<string> context, IEnumerable<(string, string)> errorData, [System.Runtime.CompilerServices.CallerMemberName] string caller = "") =>
            LogAndSendErrorConditionData(logObject, context.Select(s => IKString.FromCache(s)), errorData, caller);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logObject">
        /// The object which "became aware" of the problem.
        /// Normally an instance of <see cref="StreamProcessor"/> or this actual connection.
        /// </param>
        /// <param name="context"></param>
        /// <param name="errorData"></param>
        /// <param name="caller"></param>
        [ClassMember(Description =
            "Called when a serious error condition has been discovered and connection should be terminated.\r\n" +
            "Performs the following functions:\r\n" +
            "1) Logs to the given log object parameter.\r\n" +
            "2) Informs client over TCP/IP connection about -" + nameof(ActualConnectionP.IsErrorCondition) + "- " +
            "by sending several -" + nameof(PropertyStreamLine) + "-.\r\n" +
            "3) Marks connection with -" + nameof(ActualConnectionP.IsErrorCondition) + "-. "
        )]
        public void LogAndSendErrorConditionData(IP logObject, IEnumerable<IK> context, IEnumerable<(string, string)> errorData, [System.Runtime.CompilerServices.CallerMemberName] string caller = "") {
            logObject.Log(context, errorData, caller: caller);

            if (IP.GetPV(ActualConnectionP.IsErrorCondition, false)) {
                // It would be dangerous to send more data now. We run the risk of client and server informing
                // each other over and over again in an infinite loop about the same problem.
            } else {
                errorData.ForEach(s => Send(PropertyStreamLine.EncodeWholeLine(
                    keys: new List<IK> {
                        IKString.FromString("ThisConnection"),
                        PK.FromEnum(ActualConnectionP.ErrorConditionData)
                    },
                    value: string.Join("/", context.Select(c => PropertyStreamLine.EncodeValuePart(c.ToString()))) +
                    "/" +
                    s.Item1 +
                    " = " +
                    s.Item2
                )));
            }

            // Do not close at once, give thread some time sending the error message
            // CloseConnection();
            // NOTE: We can not do this either, because sending thread will pick up this value also immedidately.
            // IP.SetPV(ActualConnectionP.UnderClosure, true);
            // This one however is OK
            IP.SetPV(ActualConnectionP.IsErrorCondition, true);
            // We expect client to close connection now
            // TODO: Implement (elsewhere) some monitoring mechanism for closing connections which have had ErrorCondition for some time
            // TODO: (remember to give time in order to send all the error data)
        }

        [ClassMember(Description = "This collection should at most only contain 1-2 tasks")]
        private static readonly ConcurrentDictionary<DateTime, System.Threading.Tasks.Task> delayTasks = new ConcurrentDictionary<DateTime, System.Threading.Tasks.Task>();
        [ClassMember(
            Description =
                "Returns a task that will complete between 60 and 120 minutes into the future.",
            LongDescription =
                "These 'delay' tasks er static (shared within the application) because if not we would create too many of them"
        )]
        private System.Threading.Tasks.Task GetDelayTask() {
            var now = UtilCore.DateTimeNow;
            // var key = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, 0).AddMinutes(1); // Timeout in 0-60 seconds
            var key = new DateTime(now.Year, now.Month, now.Day, now.Hour, 0, 0).AddHours(2);

            if (!delayTasks.TryGetValue(key, out var retval)) {
                IP.LogText("No task exists, adding to collection");
                IP.LogKeyValue("DelayUntil", key.ToStringDateAndTime());
                delayTasks[key] = System.Threading.Tasks.Task.Delay(key.Subtract(UtilCore.DateTimeNow));
                IP.LogKeyValue("DelayTasksCount", delayTasks.Count.ToString());
                if (delayTasks.TryRemove(key.AddHours(-1), out _)) {
                    IP.LogText("Successful remove");
                    IP.LogKeyValue("DelayTasksCount", delayTasks.Count.ToString());
                }
                retval = delayTasks[key]; // Note how we do not use TryAdd but it really does not matter if we create duplicate delay tasks once in a while
            }
            return retval;
        }

        public int QueueSendCount => _queueSend.Count;

        protected int _taskIsRunning = 0;
        [ClassMember(Description =
            "Sends all messages in queue.\r\n" +
            "1) Adds the given message to the queue and \r\n" +
            "2) Empties the queue asynchronously.\r\n" +
            "Emptying is done by ensuring that there is a thead running that continously send messages until the queue is empty " +
            "(not how this thread exists as soon as the queue is empty)" +
            "\r\n" +
            "TODO: If we are -" + nameof(ConnectionDirection.Outgoing) + "-, queue messages to disk also if many elements in queue." +
            "TODO: At application startup, put those messages into the queue again, before accepting new messages." +
            "TODO: This should all be managed autonomusly within this class."
        )]
        public void Send(string singlePropertyStreamLine) {
            if (string.Empty.Equals(singlePropertyStreamLine)) throw new ArgumentException("Parameter " + nameof(singlePropertyStreamLine) + ", empty string not allowed");

            // Old method:
            // System.Threading.Interlocked.Increment(ref CountSendMessage); 
            // NOTE: Put inside of single-threaded code instead
            // IP.Inc(ActualConnectionP.CountSendMessage); // Note: Not thread-safe

            _queueSend.Enqueue(singlePropertyStreamLine);
            if (Writer == null) {
                // Looks like we are not fully initialized yet. 

                /// TODO: Risk of outgoing data getting stuck now!
                /// TODO:
                /// TODO: Will typically happen if <see cref="StreamProcessor.AddOrRemoveOutgoingConnections"/> sends all connection
                /// TODO: data over the property stream, before connection is actually opened.
                /// TODO: The data will then reside in the queue, BUT, if will be stuck there until some NEW data is sent.
                /// TODO:
                /// TODO: Possible solution: Enter loop below, and loop until Writer gets set...
                return;
            }
            if (IP.GetPV<bool>(ActualConnectionP.UnderClosure)) return; // TODO: Throw exception now?
            try {
                var wasRunning = System.Threading.Interlocked.Exchange(ref _taskIsRunning, 1);
                if (wasRunning != 0) {
                    return;
                }
                System.Threading.Tasks.Task.Factory.StartNew(() => {
                    System.Threading.Thread.CurrentThread.Name = nameof(Send) + "_" + IP.GetPV<string>(ActualConnectionP.Id);
                    System.Threading.Thread.CurrentThread.IsBackground = true; // TODO: In principle we should set this BEFORE thread is started
                    do {
                        try {
                            if (IP.GetPV<bool>(ActualConnectionP.UnderClosure)) break;
                            while (_queueSend.TryPeek(out var m)) {
                                try {
                                    if (IP.GetPV<bool>(ActualConnectionP.UnderClosure)) break;
                                    // TODO: Removed 05 Apr 2020, check if correct approach
                                    //if (Writer == null || Writer.BaseStream == null) { // TODO: Is this test needed?
                                    //    IP.SetPV(ActualConnectionP.UnderClosure, true);
                                    //    break;
                                    //}

                                    Writer.AutoFlush = true;
                                    Writer.WriteLine(m); // writer.WriteLine is really meant to be synchronous!
                                    IP.Inc(ActualConnectionP.CountSendMessage);
                                    _queueSend.TryDequeue(out m);
                                } catch (ObjectDisposedException ex) {
                                    IP.LogKeyValue("Exception", ex.GetType().ToStringShort());
                                    IP.LogText("when sending message. Considered normal (for instance because client terminated connection while this thread was running)");
                                    CloseConnection();
                                    break;
                                } catch (Exception ex) {
                                    if ((ex is NullReferenceException) && (Task != null) && (Task.Status == System.Threading.Tasks.TaskStatus.RanToCompletion)) {
                                        IP.LogKeyValue("Exception", ex.GetType().ToStringShort());
                                        IP.LogText(
                                            "(ex is System.NullReferenceException) && (task != null) && (task.Status == TaskStatus.RanToCompletion) " +
                                            "when sending message '" + m[..Math.Min(100, m.Length)] + "'. " +
                                            "Considered normal, not calling HandleException."
                                        );
                                    } else {
                                        IP.LogKeyValue("Exception", ex.GetType().ToStringShort());
                                        IP.LogText("When sending message. Closing connection. See separate log for exception.");
                                        IP.HandleException(ex);
                                    }
                                    CloseConnection();
                                    break;
                                }
                            }
                        } finally {
                            _taskIsRunning = 0;
                        }
                        // Don't remove this: This will really help in emptying the queue if two threads are working on the outgoingMessages queue
                        // For example if one thread empties the queue and sets the taskIsRunning = 0, while the other thread didn't start emptying the queue since the previous thread was running at that time
                        // This remaining code is used to avoid those situations, so that the queue are really empty always.
                        if (_queueSend.Count == 0) { // TODO: THIS REALLY LOOKS MEANINGLESS???
                            break;
                        }
                        wasRunning = System.Threading.Interlocked.Exchange(ref _taskIsRunning, 1);
                        if (wasRunning != 0) {
                            break;
                        }
                    } while (true);
                });
            } catch (Exception ex) {
                IP.LogKeyValue("Exception", ex.GetType().ToString());
                IP.LogText("Closing connection. See separate log for exception.");
                IP.HandleException(ex);
                CloseConnection();
            }
        }

        public void CloseConnection() {
            IP.SetPV(ActualConnectionP.UnderClosure, true);
            try { Writer?.Close(); } catch (Exception) { }
            try { Client?.Close(); } catch (Exception) { }
        }

        public class ActualConnectionException : ApplicationException {
            public ActualConnectionException(string message) : base(message) { }
            public ActualConnectionException(string message, Exception inner) : base(message, inner) { }
        }
    }

    [Enum(
        AREnumType = AREnumType.PropertyKeyEnum,
        Description = "Describes class -" + nameof(ActualConnection) + "-."
    )]
    public enum ActualConnectionP {
        __invalid,

        [PKType(
            Description =
                "When connection is used with-" + nameof(StreamProcessor) + "-, the Id should be the same as the key used in the incoming / outgoing connection dictionary collection."
        )]
        Id,

        [PKType(
            Description = "Only relevant when we are an -" + nameof(ARCCore.ConnectionDirection.Outgoing) + "-.",
            Type = typeof(ConnectionInstruction)
        )]
        ConnectionInstruction,

        [PKType(Type = typeof(ConnectionDirection))]
        ConnectionDirection,

        [PKType(Type = typeof(long))]
        [PKLog(DoNotLogAtAll = true)] /// TODO: Implement <see cref="PKLogAttributeP.CountInterval"/> or <see cref="PKLogAttributeP.TimeInterval"/> instead.
        CountReceiveMessage,

        [PKType(Type = typeof(long))]
        [PKLog(DoNotLogAtAll = true)] /// TODO: Implement <see cref="PKLogAttributeP.CountInterval"/> or <see cref="PKLogAttributeP.TimeInterval"/> instead.
        CountSendMessage,

        [PKType(
            Description =
                "Note how this is stored both as -" + nameof(StreamProcessorP.ClientUpdatePosition) + "- (on the client side) " +
                "and -" + nameof(ActualConnectionP.ClientUpdatePosition) + "- (on the server side).\r\n" +
                "\r\n" +
                "-" + nameof(StreamProcessor) + "- (on the client side) also stores it locally 'on disk' and initializes from there.\r\n" +
                "\r\n" +
                "On the server side (stored as -" + nameof(ActualConnectionP.ClientUpdatePosition) + "-), it is first stored by the " +
                "connection itself as it is received from the client (once, for each connection). Afterwards, the server (-" + nameof(StreamProcessor) + "-) increments the " +
                "value and stores it successively as -" + nameof(ActualConnectionP.ClientUpdatePosition) + "- to the connection.",
            LongDescription =
                "Transferred from client to server as a special \"ThisConnection\" -" + nameof(PropertyStreamLine) + "-.", // TODO: Turn "ThisConnection" into something more strictly typed
            Type = typeof(ClientUpdatePosition)
        )]
        [PKLog(DoNotLogAtAll = true)] /// TODO: Implement <see cref="PKLogAttributeP.CountInterval"/> or <see cref="PKLogAttributeP.TimeInterval"/> instead.
        ClientUpdatePosition,

        [PKType(
            Description =
                "Originates from -" + nameof(ConnectionInstructionP.Subscription) + "-.\r\n" +
                "Only relevant for -" + nameof(ARCCore.ConnectionDirection.Incoming) + "-.\r\n" +
                "The subscription required by the client (as received over the -" + nameof(ARConcepts.PropertyStream) + "-).",
            LongDescription =
                "Transferred from client to server as a special \"ThisConnection\" -" + nameof(PropertyStreamLine) + "-.", // TODO: Turn "ThisConnection" into something more strictly typed
            Type = typeof(Subscription),
            Cardinality = Cardinality.WholeCollection
        )]
        SubscriptionAsRequestedByClient,

        [PKType(Description = "Reason why this connection was closed")]
        ClosingReason,

        [PKType(
            Description =
                "Some kind of error condition has occured regarding data going back and forth, but the TCP/IP connection itself is probably sound.\r\n" +
                "Used when some data still has to be sent in order to inform partner of problem (because using -" + nameof(UnderClosure) + "- could potentially " +
                "block sending this data to the partner.\r\n" +
                "When the partner receives a 'ThisConnection/-" + nameof(ErrorConditionData) + "-' message it should consider the connection invalid " +
                "(by setting -" + nameof(IsErrorCondition) + "-) and maybe reinstate the connection.\r\n",
            LongDescription =
                "TODO: Consider turning into an enum with some different possible error conditions (currently we have ThisConnection/FilenameNotFound, see StreamProcessor).\r\n" +
                "\r\n" +
                "TODO: Implement (elsewhere) some monitoring mechanism for closing connections which have had ErrorCondition for some time\r\n" +
                "TODO: (remember to give time in order to send all the error data)",
            Type = typeof(bool)
        )]
        IsErrorCondition,

        [PKType(Description = "Detailed explanation about -" + nameof(IsErrorCondition) + "-")]
        ErrorConditionData,

        [PKType(
            Description =
                "Set to TRUE if to be closed and reinitialized.\r\n +" +
                "\r\n" +
                "TODO: IMPROVE ON THIS TEXT.\r\n" +
                "\r\n" +
                "Happens at initialization, whenever an exception occurs, or when TIMEOUT (no data received from partner / client for an extended period of time).",
            Type = typeof(bool)
        )]
        UnderClosure,

        [PKType(Type = typeof(MonkeyWrench))]
        MonkeyWrench,

        [PKType(
            Description =
                "TODO: Use more strongly typing for this.\r\n" +
                "IP-address of partner at 'other end' of connection.\r\n" +
                "Mostly relevant for -" + nameof(ARCCore.ConnectionDirection.Incoming) + "- (will then mean client ip-address).\r\n" +
                "For -" + nameof(ARCCore.ConnectionDirection.Outgoing) + "- member -" + nameof(ConnectionInstruction) + "- will contain same information"
        )]
        PartnerIPAddress,
    }

    // TODO: Move into StreamProcessor folder if becomes permanent
    [Class(
        Description =
            "Describes level of 'spanner-throwing' this connection shall contibute with",
        LongDescription =
            "Not implemented as of Mar 2020. " +
            "The idea is to test the faul-tolerance of a multiple node setup by having the nodes deliberately acting weird.\r\n" +
            "For instance by refusing to accept messages, spurious timeouts, abruptly closing connections and so on."
    )]
    public class MonkeyWrench : ITypeDescriber {

#pragma warning disable IDE0079 // Remove unnecessary suppression
#pragma warning disable IDE0060 // Remove unused parameter
        public static bool TryParse(string value, out MonkeyWrench retval, out string errorResponse) =>
#pragma warning restore IDE0060 // Remove unused parameter
#pragma warning restore IDE0079 // Remove unnecessary suppression
            throw new NotImplementedException();

        /// <summary>
        /// 'Implementing' <see cref="ITypeDescriber"/>
        /// </summary>
        /// <param name="key"></param>
        public static void EnrichKey(PK k) =>
            k.SetValidatorAndParser(new Func<string, ParseResult>(value => 
                TryParse(value, out var retval, out var errorResponse) ?
                    ParseResult.CreateOK(retval) :
                    ParseResult.CreateError(errorResponse)
            ));

        public class InvalidMonkeyWrenchException : ApplicationException {
            public InvalidMonkeyWrenchException(string message) : base(message) { }
            public InvalidMonkeyWrenchException(string message, Exception inner) : base(message, inner) { }
        }
    }
}