﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ARCCore {

    [Class(
        Description =
            "A stream -" + nameof(Subscription) + "- is the concept of how nodes (see -" + nameof(ARNodeType) + "-) " +
            "subscribe to the -" + nameof(ARConcepts.PropertyStream) + "- " +
            "(either the whole stream or just part of it) in a distributed manner.\r\n" +
            "\r\n" +
            "This class -" + nameof(Subscription) + "- describes WHAT data is required from the -" + nameof(ARConcepts.PropertyStream) + "-.\r\n" +
            "Used in connection with -" + nameof(ClientUpdatePosition) + "- which describes from WHERE (from WHEN) " +
            "in the historic -" + nameof(ARConcepts.PropertyStream) + "- data is to be returned.\r\n" +
            "\r\n" +
            "This class describes a single subscription element. A complete query consists of one or more instances of this class. " +
            "See -" + nameof(IsMatch) + "- for how a query is evaluated.\r\n" +
            "\r\n" +
            "Some examples of subscriptions could be:\r\n" +
            "\r\n" +
            "1) A -" + nameof(ARNodeType.Client) + "-, for instance -" + nameof(ARComponents.ARCAPI) + "-, " +
            "subscribing (towards -" + nameof(ARNodeType.CoreDB) + "-) to those parts of the -" + nameof(ARConcepts.PropertyStream) + "- " +
            "that it needs for its functioning.\r\n" +
            "As this client receives the incoming properties (through -" + nameof(StreamProcessor.OutsideLocalReceiver) + "-) " +
            "they would typically be stored in a -" + nameof(PRich) + "- " +
            "for immediate in-memory evaluation of incoming queries to the client again.\r\n" +
            "\r\n" +
            "2) Subscribe to logging properties for setting up of a log-console (see -" + nameof(PSPrefix.app) + "-).\r\n" +
            "\r\n" +
            "3) Subscribe to whole database or part of database in different manners in order to set up load-balancing, fault-tolerance, caching and similar " +
            "(see -" + nameof(ConnectionInstructionP.MultipleConnectionUsage) + "- and -" + nameof(ConnectionInstructionP.Sharding) + "- for more information).\r\n" +
            "(in other words, synchronizing and coordinating between multiple AgoRapide based nodes, especially between -" + nameof(ARNodeType.CoreDB) + "- nodes.) " +
            "Note how the subscription instruction itself can very well be sent over the -" + nameof(ARConcepts.PropertyStream) + "-, configuring the different nodes " +
            "from a central entry point of administration.\r\n" +
            "\r\n" +
            "See -" + nameof(SyntaxHelp) + "-.\r\n" +
            "\r\n" +
            "TODO: Introduce '**' in order to signify any hierarchical level.\r\n" +
            "TODO: Like +**/Heartbeat/* in order to collect all -" + nameof(PConcurrentP.Heartbeat) + "- in the system, regardless of hierarchical level." +
            "\r\n" +
            "See -" + nameof(IsMatch) + "- (static method) for how a query consisting of multiple instances of -" + nameof(Subscription) + "- are evaluated." +
            "\r\n" +
            "This class is immutable.",
         LongDescription =
            "Note that this same 'query language' (multiple instances of -" + nameof(Subscription) + "-) can very well " +
            "be used by clients of -" + nameof(ARComponents.ARCAPI) + "- also.\r\n" +
            "(but more probably an API client will make GraphQL requests instead).\r\n" +
            "\r\n" +
            "Note / FAQ about IQueryable<>: May 2020: AgoRapide will probably not support " + nameof(IQueryable) + " because this interface is mostly about " +
            "avoiding local processing of data. With IQueryable the query will not be executed until it reaches the actual data storage. " +
            "Since a typical -" + nameof(ARNodeType) + "- actually has all data present locally, this concept becomes irrelevant.\r\n"
    )]
    public class Subscription : ITypeDescriber, IEquatable<Subscription> {

        /// <summary>
        /// See <see cref="ITypeDescriber.GetSyntaxHelp(Type)"/>
        /// </summary>
        [ClassMember(Description =
            "'+*'                         Subscribe to everything in database.\r\n" +
            "'+dt/Customer/*'             Subscribe to all Customer properties.\r\n" +
            "'-dt/Customer/*/Photo/*'     Exclude from subscription photos of customers.\r\n" +
            "'+dt/Customer/*/FirstName/*' Subscribe to all occurrences of Customer.FirstName.\r\n" +
            "'+dt/Customer/*/LastName/*'  Subscribe to all occurrences of Customer.LastName.\r\n" +
            "'+dt/Customer/42/*'          Subscribe to all properties related to customer with id 42.\r\n" +
            "'+dt/Order/*'                Subscribe to all Order properties.\r\n" +
            "'+log/*'                     Subscribe to everything beginning with log .\r\n" +
            "'-log/API/*'                 Do not include properties beginning with 'log/API/ in subscription.\r\n" +
            "'-*'                         Stop existing subscription.\r\n"
        )]
        public static string SyntaxHelp => ITypeDescriber.GetSyntaxHelp(typeof(Subscription));

        private string Value { get; }

        public bool IsAll => Value.Equals("+*");
        public bool IsNone => Value.Equals("-*");

        private bool? _isAdd = null;
        [ClassMember(Description = "Corresponds to whether value starts with a plus sign, '+' or not.")]
        public bool IsAdd => _isAdd ?? (bool)(_isAdd = Value.StartsWith("+"));

        private bool? _isRemove = null;
        [ClassMember(Description = "Corresponds to whether value starts with a minus sign, '+' or not.")]
        public bool IsRemove => _isRemove ?? (bool)(_isRemove = Value.StartsWith("-"));

        [ClassMember(Description =
            "TODO: Implement concept of specifying starting-point in TIME (often for an -" + nameof(ARConcepts.AdHocQuery) + "-?).\r\n" +
            "TODO: Clarify difference between -" + nameof(ClientUpdatePosition) + "- and -" + nameof(Subscription.FromTime) + "- / -" + nameof(Subscription.ToTime) + "-."
        )]
        public void FromTime() { }
        [ClassMember(Description =
            "TODO: Implement concept of specifying ending-point in TIME (often for an -" + nameof(ARConcepts.AdHocQuery) + "-?).\r\n" +
            "TODO: Clarify difference between -" + nameof(ClientUpdatePosition) + "- and -" + nameof(Subscription.FromTime) + "- / -" + nameof(Subscription.ToTime) + "-."
        )]
        public void ToTime() { }

        [ClassMember(
            Description =
                "Evaluates a query consisting of multiple instances of -" + nameof(Subscription) + "- against a single property stream line.\r\n" +
                "\r\n" +
                "TRUE is returned when\r\n" +
                "(any of the -" + nameof(IsAdd) + "- instances matches)\r\n" +
                "AND NOT\r\n" +
                "(any of the -" + nameof(IsRemove) + "- instances matches)\r\n" +
                "\r\n" +
                "TODO: Add support for RegEx and other formats"
        )]
        public static bool IsMatch(List<Subscription> list, string singlePropertyStreamLine) {
            // Preliminary naïve version
            // TODO: Add more professional parsing here.
            var pos = singlePropertyStreamLine.IndexOf(" = ");
            // Note how only matches up to actual assignment of property in property stream line
            // TODO: Add support for actual value too 
            if (pos > -1) singlePropertyStreamLine = singlePropertyStreamLine.Substring(0, pos);
            var singlePropertyStreamLineLevels = singlePropertyStreamLine.Split("/").ToList();
            return
                list.Where(s => s.IsAdd).Any(s => s.IsMatch(singlePropertyStreamLineLevels)) &&
                !list.Where(s => s.IsRemove).Any(s => s.IsMatch(singlePropertyStreamLineLevels));
        }

        [ClassMember(
            Description =
                "Evaluates this instance of -" + nameof(Subscription) + "- against a single property stream line " +
                "(which has been split up into its constituent elements).\r\n" +
                "\r\n" +
                "Does not take into consideration + or - (whether we are -" + nameof(IsAdd) + "- / -" + nameof(IsRemove) + "-), " +
                "considers only whether string matches")]
        public bool IsMatch(List<string> singlePropertyStreamLineLevels) {
            for (var i = 0; i < Levels.Count; i++) {
                if (i >= singlePropertyStreamLineLevels.Count) { // More levels specified than contained in single property stream line
                    if (i == Levels.Count - 1 && Levels[^1].IsAll) {
                        // It only remains one level in the criteria to match, and that one is '*'
                        // Regard as match
                        return true;
                    }
                    return false;
                }
                if (!Levels[i].IsMatch(singlePropertyStreamLineLevels[i])) return false;
            }
            if (singlePropertyStreamLineLevels.Count > Levels.Count && !Levels[^1].IsAll) {
                // All levels in the criteria matched, but more levels remain and last level in criteria was not '*'
                // Do not regard as match
                return false;
            }
            return true;
        }

        private List<SingleSubscriptonLevel>? _levels = null;
        private List<SingleSubscriptonLevel> Levels => _levels ?? (_levels = Value.Substring(1).Split("/").Select(s => new SingleSubscriptonLevel(s)).ToList());

        private Subscription(string value) => Value = value;

        public static Subscription AllInstance = new Subscription("+*");
        public static Subscription NoneInstance = new Subscription("-*");

        public static Subscription Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidSubscriptionException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse + "\r\n");
        public static bool TryParse(string value, out Subscription retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out Subscription retval, out string errorResponse) {
            if (string.IsNullOrEmpty(value)) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = "Null or Empty value";
                return false;
            }
            if (value.Equals("+*")) {
                retval = AllInstance;
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                return true;
            }
            if (value.Equals("-*")) {
                retval = NoneInstance;
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                return true;
            }
            if (!value.StartsWith("-") && !value.StartsWith("+")) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Must start with one of the characters + (plus) or - (minus), not '" + value.Substring(0, 1) + "'.";
                return false;
            }
            retval = new Subscription(value);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
            return true;
        }

        public override int GetHashCode() => Value.GetHashCode();
        public bool Equals(Subscription other) => Value.Equals(other.Value);
        public override bool Equals(object other) => other is Subscription s && Equals(s);

        public override string ToString() => Value;

        /// <summary>
        /// 'Implementing' <see cref="ITypeDescriber"/>
        /// </summary>
        /// <param name="key"></param>
        public static void EnrichKey(PK key) =>
            key.SetValidatorAndParser(new Func<string, ParseResult>(value =>
                TryParse(value, out var retval, out var errorResponse) ?
                    ParseResult.CreateOK(retval) :
                    ParseResult.CreateError(errorResponse)
            ));

        public class SingleSubscriptonLevel {
            public string Value { get; private set; }
            private bool? _isAll = null;
            public bool IsAll => _isAll ?? (bool)(_isAll = "*".Equals(Value));
            public SingleSubscriptonLevel(string value) => Value = value;
            public bool IsMatch(string singlePropertyStreamLineLevel) {
                if (IsAll) return true;
                // TODO: Make more advanced. Now can only match whole values.
                return Value.Equals(singlePropertyStreamLineLevel);
            }

            public override string ToString() => Value;
        }

        public class InvalidSubscriptionException : ApplicationException {
            public InvalidSubscriptionException(string message) : base(message) { }
            public InvalidSubscriptionException(string message, Exception inner) : base(message, inner) { }
        }
    }
}