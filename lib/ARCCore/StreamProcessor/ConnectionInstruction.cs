﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;

namespace ARCCore {
    [Class(
        Description =
            "The instruction about what connections to make (host, portnr, data transfer direction and so on).\r\n" +
            "Only relevant for -" + nameof(ConnectionDirection.Outgoing) + "-."
    )]
    public class ConnectionInstruction : PRich {

        public static ConnectionInstruction CreateAndInitialize(string serverHostName, int serverPortNo, List<Subscription>? subscription = null, DataTransferDirection dataTransferDirection = DataTransferDirection.Receive) {
            var retval = new ConnectionInstruction();
            retval.IP.AddPV(ConnectionInstructionP.ServerHostName, serverHostName);
            retval.IP.AddPV(ConnectionInstructionP.ServerPortNo, serverPortNo);
            if (subscription != null) retval.IP.AddPV(ConnectionInstructionP.Subscription, subscription);
            retval.IP.AddPV(ConnectionInstructionP.DataTransferDirection, dataTransferDirection);
            retval.Initialize();
            return retval;
        }

        [ClassMember(
            Description =
                "Initializes to sane values.\r\n" +
                "\r\n" +
                "Note that idempotent, in other words may be called multiple times without side effects.",
            LongDescription =
                "Due to the all-singing / all-dancing nature of this class (outgoing / incoming / send / receive) it is helpful " +
                "if the class can configure itself according to what is supposedly intended (therefore this method)."
        )]
        public void Initialize() {
            IP.AssertIntegrity();

            IP.GetOrAddPV(ConnectionInstructionP.ConnectionId, () =>
                    // TODO: How does this relate with use of id's over the property stream?
                    // TODO: (in general an entity does not know its own id, so why should it need it now?)
                    IP.GetPV<string>(ConnectionInstructionP.ClientId) + "_" +
                    IP.GetPV<string>(ConnectionInstructionP.ServerHostName) + "_" +
                    IP.GetPV<int>(ConnectionInstructionP.ServerPortNo) + "_" +
                    IP.GetPVM<DataTransferDirection>()
            );

            if (IP.GetPVM<DataTransferDirection>() == DataTransferDirection.Receive) {
                // If no subscription, assume that subscription to everything is desired.
                if (!IP.TryGetPV<List<Subscription>>(ConnectionInstructionP.Subscription, out var l) || l.Count == 0) {
                    IP.AddPV(ConnectionInstructionP.Subscription, new List<Subscription> { Subscription.Parse("+*") });
                }
            }
        }
    }

    [Enum(
        AREnumType = AREnumType.PropertyKeyEnum,
        Description = "Describes class -" + nameof(ConnectionInstruction) + "-."
    )]
    public enum ConnectionInstructionP {
        __invalid,

        [PKType(
            Description =
                "The AgoRapide node-id for which this connection is relevant. " +
                "Used for distributing over the -" + nameof(ARConcepts.PropertyStream) + "- information to the clients about which possible nodes they can connect to.\r\n" +
                "Not used as of Apr 2020 (distributing of -" + nameof(ConnectionInstruction) + "- has not been implemented.\r\n",
            DefaultValue = "Node1"

        )]
        ClientId,

        [PKType(
            Description = "The host (DNS or IP-address) for which to connect.",
            IsObligatory = true
        )]
        ServerHostName,

        [PKType(
            Description = "The port number for which to connect.",
            IsObligatory = true,
            Type = typeof(int)
        )]
        ServerPortNo,

        [PKType(Description =
            "Usually a combination of -" + nameof(ClientId) + "-, -" + nameof(ServerHostName) + "- and -" + nameof(ServerPortNo) + "-.\r\n" +
            "Will be set by -" + nameof(ConnectionInstruction.Initialize) + "- if not set originally"
        )]
        ConnectionId,

        [PKType(
            Type = typeof(DataTransferDirection),
            DefaultValue = ARCCore.DataTransferDirection.Receive
        )]
        DataTransferDirection,

        [PKType(
            Description =
                "The actual subscripton. This is what the connecting party / (downstream) client would send over its TCP/IP connection to the (upstream) server.\r\n" +
                "Only relevant for -" + nameof(ARCCore.DataTransferDirection.Receive) + "-.\r\n" +
                "For -" + nameof(ARCCore.DataTransferDirection.Send) + "- it is assumed that everything is always sent.\r\n" +
                "See also -" + nameof(ActualConnectionP.SubscriptionAsRequestedByClient) + "-." +
                "\r\n" +
                "Will be set to '+*' (everything) by -" + nameof(ConnectionInstruction.Initialize) + "- " +
                "for -" + nameof(ARCCore.DataTransferDirection.Receive) + "- if not set originally",
            Type = typeof(Subscription),
            Cardinality = Cardinality.WholeCollection
        )]
        Subscription,

        [PKType(Type = typeof(MultipleConnectionUsage))]
        MultipleConnectionUsage,

        [PKType(Type = typeof(Sharding))]
        Sharding
    }

    [Enum(
        Description = "Enables fault-tolerance by describing use of multiple connections\r\n",
        LongDescription = "See also -" + nameof(Sharding) + "- which enables load-balancing at the -" + nameof(ARNodeType.CoreDB) + "--level.",
        AREnumType = AREnumType.OrdinaryEnum
    )]
    public enum MultipleConnectionUsage {
        __invalid,

        [EnumMember(
            Description =
                "Connection is a special administrative connection to a central 'command-and-control' server which instructs which connections to make.\r\n" +
                "Accompanying -" + nameof(ConnectionInstructionP.Subscription) + "- will normally be something like 'ConnectionInstruction/*'.\r\n" +
                "\r\n" +
                "Note implemented as of May 2020."
        )]
        Admin,

        [EnumMember(Description =
            "This connection to be used in a turn based system, together with other connections with -" + nameof(MultipleConnectionUsage.RoundRobin) + "-.\r\n" +
            "\r\n" +
            "Whenever multiple -" + nameof(DataTransferDirection.Send) + "- connections are specified, " +
            "they are supposed to be used one-at-a-time (like in a round-robin fashion, with a single property sent to only one of them), " +
            "and any connection temporarily out-of-order shall be ignored.-\r\n" +
            "\r\n" +
            "Likewise, for -" + nameof(DataTransferDirection.Receive) + "-, only a single one should be subscribed to " +
            "at a given time.\r\n" +
            "\r\n" +
            "This is the natural setup for a -" + nameof(ARNodeType.ArmDB) + "- or -" + nameof(ARNodeType.Client) + "-.\r\n" +
            "\r\n" +
            "Note: All connections are supposed to be kept open at all times, and their health-status recorded, so that -" + nameof(StreamProcessor) + "- " +
            "is able to immediately switch connections\r\n" +
            "\r\n" +
            "NOTE: Not implemented as of Mar 2020.\r\n" +
            "\r\n"
        )]
        RoundRobin,

        [EnumMember(Description =
            "This connection to be used simultaneously with other connections with -" + nameof(MultipleConnectionUsage.Simultaneous) + "-\r\n" +
            "\r\n" +
            "TODO: Rename into something else. Mirroring for instance.\r\n" +
            "\r\n" +
            "Whenever multiple connections of the same -" + nameof(DataTransferDirection) + "- are specified, " +
            "they are supposed to be used simultaneously at all times.\r\n" +
            "\r\n" +
            "This is the natural setup for multiple -" + nameof(ARNodeType.CoreDB) + "- instances communicating and synchronizing among themselves.\r\n" +
            "(-" + nameof(ARNodeType.ArmDB) + "- / -" + nameof(ARNodeType.Client) + "- can send their data to 'wherever they wish' and then it is up to " +
            "the -" + nameof(ARNodeType.CoreDB) + "- to sort it out themselves.)" +
            "\r\n" +
            "NOTE: Not implemented as of Mar 2020."
        )]
        Simultaneous
    }

    [Class(
        Description =
            "Enables load-balancing by describing what data should be sent over a given connection.\r\n" +
            "If load-balancing is combined with fault-tolerance (see -" + nameof(MultipleConnectionUsage) + "-, then multiple connections " +
            "with the same value for -" + nameof(ConnectionInstructionP.Sharding) + "- are grouped together and considered separately for other groups",
        LongDescription =
            "Not implemented as of Mar 2020. The idea is to have some criteria like 'CustomerId % 5 = 3' for instance, " +
            "for a 5-node setup where load is shared out based on value of customerId."
    )]
    public class Sharding : ITypeDescriber {
        public static bool TryParse(string value, out Sharding retval, out string errorResponse) =>
            throw new NotImplementedException();

        /// <summary>
        /// 'Implementing' <see cref="ITypeDescriber"/>
        /// </summary>
        /// <param name="key"></param>
        public static void EnrichKey(PK k) =>
            k.SetValidatorAndParser(new Func<string, ParseResult>(value =>
                TryParse(value, out var retval, out var errorResponse) ?
                    ParseResult.CreateOK(retval) :
                    ParseResult.CreateError(errorResponse)
            ));

        public class InvalidShardingException : ApplicationException {
            public InvalidShardingException(string message) : base(message) { }
            public InvalidShardingException(string message, Exception inner) : base(message, inner) { }
        }
    }
}