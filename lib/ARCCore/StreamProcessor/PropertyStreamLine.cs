﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ARCCore
{

    [Class(
        Description =
            "Has two main functions:\r\n" +
            "\r\n" +
            "1) Encodes and decodes single lines in the -" + nameof(ARConcepts.PropertyStream) + "-.\r\n" +
            "See methods like -" + nameof(EncodeKeyPart) + "-, " +
            "-" + nameof(EncodeValuePart) + "- and " +
            "-" + nameof(Decode) + "-.\r\n" +
            "\r\n" +
            "2) Stores single lines in the -" + nameof(ARConcepts.PropertyStream) + "- into a data-storage. " +
            "See -" + nameof(TryStore) + "-.\r\n" +
            "\r\n" +
            "3) Parses a collection of -" + nameof(ARConcepts.PropertyStream) + "- lines into a single -" + nameof(IP) + "- instanse.\r\n" +
            "See methods like -" + nameof(TryParseDirectToIP) + "-.\r\n" +
            "\r\n" +
            "4) Parses a single -" + nameof(ARConcepts.PropertyStream) + "- line into a single property.\r\n" +
            "See methods like -" + nameof(TryParseDirectToProperty) + "-.\r\n" +
            "\r\n" +
            "A line consists of a key-part, the separator ' = ' (space, equals sign, space) and a value part.\r\n" +
            "(the value part may be empty, see -" + nameof(PValueEmpty) + "-).\r\n" +
            "\r\n" +
            "Examples are (syntax only approximate as of May 2020):\r\n" +
            "'// This is a comment (lines starting with // are to be ignored in any processing\r\n" +
            "'dt/Customer/42 (create new Customer object, actually this line is not necessary)\r\n" +
            "'dt/Customer/42/Cid = User/43' (See -" + nameof(PP.Cid) + "-)\r\n" +
            "'dt/Customer/42/FirstName = John'\r\n" +
            "'dt/Customer/42/PhoneNumber/+4790534333' (create new PhoneNumber)\r\n" +
            "'dt/Customer/42/PhoneNumber/+4790534333/Invalid' (Equivalent to 'delete', see -" + nameof(PP.Invalid) + ")\r\n" +
            "\r\n" +
            "'dt/Order/1968/CustomerId = Customer/42'\r\n" +
            "\r\n" +
            "'dt/OrderLine/2001/OrderId = 1968'\r\n" +
            "'dt/OrderLine/2001/ProductId = 2003'\r\n" +
            "'dt/OrderLine/2001/Quantity = 3'\r\n" +
            "\r\n" +
            "'dt/Product/2003/Name = Widget3'\r\n" +
            "'dt/Product/2003/Colour = Red'\r\n" +
            "\r\n" +
            "Note that timestamps are normally not needed because the property stream is sequentially organised, " +
            "and timestamps are inserted automatically (see -" + nameof(StreamProcessorP.TimestampResolution) + "-).\r\n" +
            "See " +
            "-" + nameof(EncodeKeyPart) + "- and " +
            "-" + nameof(EncodeValuePart) + "- " +
            "for how data is actually encoded (examples given above are only indicative).\r\n" +
            "\r\n" +
            "Note that this class is static, that is, no instances if this class are ever created.\r\n" +
            "Instead simple strings are used for representing property stream lines for transmission and storage. " +
            "This is considered to be more efficient (less processing required) and take up less memory (especially for -" + nameof(ARNodeType.CoreDB) + "-).",
        LongDescription =
            "FAQ: Is the property stream (and AgoRapide in general) strongly or weakly typed?\r\n" +
            "That is up to you.\r\n" +
            "For getting started, and just in order to experiment with some various data, you can start weakly typed, or just use strings everywhere.\r\n" +
            "A standard storage like -" + nameof(PRich) + "- and a generic -" + nameof(ARComponents.ARAAPI) + "- built on top of that " +
            "is not dependent on any pre-defined structure, it just accepts whatever is in the property stream.\r\n" +
            "In your C# code you can use multi-level indexing like this: 'DataStorage[\"Customer\"][\"42\"][\"FirstName\"] = \"John\"' without " +
            "bothering about types at all (or just send the corresponding -" + nameof(PropertyStreamLine) + "- " +
            "'dt/Customer/42/FirstName = John' to -" + nameof(PropertyStreamLineParsed.TryParse) + "-).\r\n" +
            "As your project matures and you see which classes and properties are more important, you can create the corresponding " +
            "'entity' classes and their -" + nameof(AREnumType.PropertyKeyEnum) + "-, and specify more exact types for the properties.\r\n" +
            "Taken into account concepts like -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "- this will also constitute documenting " +
            "your application.\r\n" +
            "Note that though you can choose the level of dynamic typing (as shown), static strong typing " +
            "is always a bit weaker with -" + nameof(ARConcepts.PropertyAccess) + "-. " +
            "See -" + nameof(IKIP.AssertTypeIntegrity) + "- and -" + nameof(PKTypeAttributeP.BoilerplateCodeProperties) + "- " +
            "for attempts at alleviating this.\r\n" +
            "\r\n" +
            "FAQ: How does the property stream format compare to the JSON format?\r\n" +
            "The short answer is that JSON is well suited when you want to transfer a whole object.\r\n" +
            "If you want to transfer parts of an object, for instance updating one single property, then the property stream format is better suited.\r\n" +
            "A JSON representation of some given data points could be seen as a 'compiled' variant of the same data points stored as property stream lines.\r\n" +
            "This is because the original property stream lines for a given JSON representation may be spread out (not being located close to each other).\r\n" +
            "Note however that if the actual property stream lines (for some given data points) reside consequently to each other in the data storage, " +
            "then JSON would be a much more effective format for representation because it is much less verbose.\r\n" +
            "TODO: Insert examples here in order to clarify.\r\n" +
            "\r\n" +
            "Some notes about compression:\r\n" +
            "Note how the format used is somewhat verbose but well suited for on-disk compression.\r\n" +
            "It is for instance inefficient (storage-wise) to repeat the same context line after line.\r\n" +
            "But on the other hand, the underlying OS can easily be instructed to compress the storage files instead (something it should be able to do " +
            "quite efficiently, leaving the size-issue to mostly constitute a network bandwidth problem.\r\n" +
            "Note that some optimizations are performed anyway, see for instance -" + nameof(StreamProcessorP.Timestamp) + "- " +
            "which describes how timestamps are not added to each and every property stream line but inserted as needed.\r\n" +
            "\r\n"
    )]
    public static class PropertyStreamLine
    {

        public static string EncodeWholeLine(IEnumerable<IK> keys, string value) => EncodeKeys(keys) + ("".Equals(value) ? "" : " = ") + EncodeValuePart(value);
        public static string EncodeWholeLine(IEnumerable<string> keys, string value) => EncodeKeys(keys) + ("".Equals(value) ? "" : " = ") + EncodeValuePart(value);
        public static string EncodeWholeLine(IEnumerable<IK> keys, IEnumerable<string> values) => EncodeKeys(keys) + " = " + EncodeValues(values);
        public static string EncodeWholeLine(List<string> keys, IEnumerable<string> values) => EncodeKeys(keys) + " = " + EncodeValues(values);

        [ClassMember(Description =
            "Encodes the key part of a -" + nameof(PropertyStreamLine) + "-.\r\n" +
            "Example result: Customer/42/FirstName"
        )]
        public static string EncodeKeys(IEnumerable<IK> keys) => EncodeKeys(keys.Select(k => k.ToString()));
        // Note: Do not add [ClassMember(Description = "... for all overloads unless it actually differs. It will only lead to confusing compiled documentation.
        public static string EncodeKeys(IEnumerable<string> keys) => string.Join("/", keys.Select(k => EncodeKeyPart(k)));

        [ClassMember(Description =
            "Typically used when turning -" + nameof(Cardinality.WholeCollection) + "- into a value part of a -" + nameof(PropertyStreamLine) + "-.\r\n" +
            "Example: Red;Yellow;Green.\r\n" +
            "TODO: Give better example with real need for encoding."
        )]
        public static string EncodeValues(IEnumerable<string> values) =>
            string.Join(";", values.Select(v => EncodeValuePart(v)));

        // TODO: Delete commented out code
        //[ClassMember(Description =
        //    "Encodes the single value part of a -" + nameof(PropertyStreamLine) + "-.\r\n"
        //)]
        //public static string EncodeValue(string value) => EncodeValuePart(value);

        [ClassMember(
            Description =
                "Converts a single key part of a -" + nameof(PropertyStreamLine) + "- to a format suitable for sending over a -" + nameof(ARConcepts.PropertyStream) + "-. " +
                "\r\n" +
                "The resulting format is supposed to be quite human-readable.\r\n" +
                "\r\n" +
                "Returns a string containing only characters in -" + nameof(AllowedKeyChars) + "-.\r\n" +
                "Other characters are represented by '0x' plus their UTF-16 code value as four hex characters (like '0x0020' for space).\r\n" +
                "(0x itself is changed into 0xoooo before conversion takes place.)\r\n" +
                "\r\n" +
                "Usually called from functions like -" + nameof(EncodeKeys) + "-.\r\n" +
                "\r\n" +
                "For instance for the following\r\n" +
                "key parts: 'Customer', '42', '+4790534333', 'Cid'\r\n" +
                "and value part: 'Actor/43'\r\n" +
                "the correct format is: " +
                "Customer/42/PhoneNumber/0x002B4790534333/Cid = Actor/43\r\n" +
                "('Customer/42/PhoneNumber/0x002B4790534333/Cid' is what this function encodes.)\r\n" +
                "\r\n" +
                "Note: -" + nameof(IKString) + "- used the same type of encoding but with another set of allowed characters.\r\n" +
                "\r\n" +
                "See also -" + nameof(Decode) + "- and -" + nameof(EncodeValuePart) + "-."
        )]
        public static string EncodeKeyPart(string s) =>
            // TODO: Note rooms for big improvements in processing speeds For instance using lookup-table for all ASCII-characters instead of a hashset, 
            // TODO: like: bool[] = new allowedChars[128]
            // TODO: This is only a naïve first implementation of encoding
            string.Join("", s.Replace("0x", "0xoooo").Select(c => AllowedKeyChars.Contains(c) ? c.ToString() : ("0x" + ((int)c).ToString("X4"))));

        [ClassMember(
            Description =
                "Allowed characters in the Key-part of a -" + nameof(PropertyStreamLine) + "-.\r\n" +
                "\r\n" +
                "The character set 'A...Za...z0...9-_().,' from -" + nameof(IKCoded.AllowedEncodedCharacters) + "- " +
                "plus ' ' (space), '\"' (double quote), ''' (single quote) and '|§!#%&=?+^¨~'.\r\n" +
                "\r\n" +
                "ASCII characters important to avoid are:\r\n" +
                "'/' (slash) (because used as a separator in the 'key' part of a property stream line,\r\n" +
                "';' (semicolon) because used as separator for a collection of values (see -" + nameof(Cardinality.WholeCollection) + "-).\r\n" +
                "and all control characters (especially CR and LF, ASCII 13 and 10).\r\n" +
                "\r\n" +
                "NOTE: The selection of characters are somewhat hastily chosen (as of May 2020) and may change in the future.\r\n" +
                "\r\n" +
                "See also -" + nameof(AllowedValueChars) + "-.\r\n"
        )]
        public static readonly HashSet<char> AllowedKeyChars = new Func<HashSet<char>>(() =>
        {
            var retval = IKCoded.AllowedEncodedCharacters.ToHashSet();
            " \"'|§!#%&?+^¨~:".ToCharArray().ForEach(c => retval.Add(c));
            return retval;
        })();

        [ClassMember(
            Description =
                "Converts a single value part of a -" + nameof(PropertyStreamLine) + "- to a format suitable for sending over a -" + nameof(ARConcepts.PropertyStream) + "-. " +
                "\r\n" +
                "The resulting format is supposed to be quite human-readable.\r\n" +
                "\r\n" +
                "Returns a string containing only characters in -" + nameof(AllowedValueChars) + "-.\r\n" +
                "Other characters are represented by '0x' plus their UTF-16 code value as four hex characters (like '0x0020' for space).\r\n" +
                "(0x itself is changed into 0xoooo before conversion takes place.)\r\n" +
                "\r\n" +
                "TODO: Change into disallowed chars instead (actually only ';' (semicolon) and '\\r' (carriage return) / '\\n' (new line) are disallowed).\r\n" +
                "TODO: And as UTF-8 encoding is used throughout AgoRapide (see -" + nameof(UtilCore.DefaultAgoRapideEncoding) + "-) other characters\r\n" +
                "TODO: in general can be represented.\r\n" +
                "\r\n" +
                "NOTE: You can typically replace this function with the following single liner in C#:\r\n" +
                "NOTE:\r\n" +
                "NOTE:   ...value.Replace(\"0x\",\"0xoooo\").Replace(\"\\r\", \"0x000D\").Replace(\"\\n\", \"0x000A\").Replace(\";\", \"0x003B\")\r\n" +
                "NOTE:\r\n" +
                "NOTE: That is, encode only line breaks and semicolon.\r\n" +
                "NOTE: (the semicolon is related to -" + nameof(Cardinality.WholeCollection) + "-).\r\n" +
                "\r\n" +
                "Note: -" + nameof(IKCoded) + "- used the same type of encoding but with another set of allowed characters that are not encoded.\r\n" +
                "\r\n" +
                "See also -" + nameof(Decode) + "-, -" + nameof(EncodeKeyPart) + "- and -" + nameof(EncodeValues) + "-.\r\n"
        )]
        public static string EncodeValuePart(string s) =>
            // TODO: Note rooms for big improvements in processing speeds
            // TODO: For instance using lookup-table for all ASCII-characters instead of a hashset, 
            // TODO: like: bool[] = new allowedChars[128]
            // TODO: This is only a naïve first implementation of encoding
            string.Join("", s.Replace("0x", "0xoooo").Select(c => AllowedValueChars.Contains(c) ? c.ToString() : ("0x" + ((int)c).ToString("X4"))));

        [ClassMember(Description =
            "Allowed characters in the Value-part of a -" + nameof(PropertyStreamLine) + "-.\r\n" +
            "\r\n" +
            "The character set -" + nameof(AllowedKeyChars) + "- " +
            "plus '=' (equals), '/' (forward slash), '*' (asterisk) and '\' (backslash).\r\n" +
            "(note that in general the value part is not as critical regarding encoding " +
            "because 'everything' after the key, value separator ' = ' is the actual value.)\r\n" +
            "\r\n" +
            "TODO: Change into 'DisallowedASCIIChars' instead of allowed chars.\r\n" +
            "TODO: See -" + nameof(EncodeValuePart) + "- for more information.\r\n" +
            "\r\n" +
            "ASCII characters important to avoid are:\r\n" +
            "';' (semicolon) because used as separator for a collection of values (see -" + nameof(Cardinality.WholeCollection) + "-).\r\n"
        )]
        public static readonly HashSet<char> AllowedValueChars = new Func<HashSet<char>>(() =>
        {
            var retval = AllowedKeyChars.ToHashSet();
            "=/*\\".ToCharArray().ForEach(c => retval.Add(c));
            return retval;
        })();

        [ClassMember(Description =
            "Decodes string as encoded by " +
            "-" + nameof(EncodeKeyPart) + "- or " +
            "-" + nameof(EncodeValuePart) + "-.\r\n" +
            "\r\n" +
            "\r\n" +
            "NOTE: Takes up a significant amount of processing time, for instance when called by -" + nameof(PropertyStreamLineParsed.TryParse) + "-\r\n" +
            "\r\n" +
            "TODO: A smarter encoding has great potential for quicker decoding. For instance a prefix in a -" + nameof(PropertyStreamLine) + "-\r\n" +
            "TODO: hinting about level of encoding necessary. Like '_' indicating no special encoding for instance.\r\n" +
            "TODO: In general AgoRapide application will read the property stream much more frequent than writing to it.\r\n" +
            "\r\n" +
            "Note how the decoder is common, used for both the key-part and the value-part of the -" + nameof(PropertyStreamLine) + "-.\r\n" +
            "Note that when the value-part constitutes of multiple items (-" + nameof(Cardinality.WholeCollection) + "-), " +
            "it is actually decoded twice, first by -" + nameof(PropertyStreamLineParsed.TryParse) + "- for the value-part as a whole, " +
            "and then again by the parser created by -" + nameof(PK.ModifyCleanerAndValidatorAndParserForCardinality) + "-.\r\n" +
            "\r\n" +
            "Single illegal instances of '0x (0x not followed by a valid 4-digit hex number (and not followed by 'oooo'))\r\n" +
            "are returned in-original.\r\n"
        )]
        public static string Decode(string s)
        {
            // 8 April 2021: Buggy implementation ('0x0020x....' would be decoded wrong)
            //string.Join("", ("[START]" + s).Split("0x").Select(e => {
            //    // TOOD: Note rooms for big improvements in processing speed.
            //    // TODO: This is only a naïve first implementation of decoding

            //    if (e.StartsWith("[START]")) return e.Substring("[START]".Length); // First element (contains no encoding)
            //    if (e == "") return e; // There is an error, s ends with '0x'
            //    if (e.Length < 4) return e; // There is an error, 0x should always be followed by at least four characters
            //    var first4 = e.Substring(0, 4);
            //    var rest = e.Substring(4);
            //    if ("oooo".Equals(first4)) return "0x" + rest; // 0x was actually part of the original string
            //    if (int.TryParse(first4, System.Globalization.NumberStyles.HexNumber, System.Globalization.CultureInfo.InvariantCulture, out var c)) {
            //        return ((char)c).ToString() + rest; // Ordinary situation
            //    }
            //    // Something is wrong (0x was followed by an non-integer number)
            //    return rest;
            //}));

            // 8 April 2021, better (and quicker) implementation
            var retval = new StringBuilder();
            var lastPos = -6;
            var pos = s.IndexOf("0x");
            while (pos > -1)
            {
                retval.Append(s.Substring(lastPos + 6, pos - lastPos - 6));
                if (pos > s.Length - 6)
                { // Something is wrong, return original string
                    lastPos = pos - 6; // Hack in order to pick up rest of string after loop terminates.
                    pos = -1; // Terminate loop
                }
                else
                {
                    var hex = s.Substring(pos + 2, 4);
                    if ("oooo".Equals(hex))
                    {
                        retval.Append("0x"); // 0x was actually part of the original string")
                    }
                    else if (!int.TryParse(hex, System.Globalization.NumberStyles.HexNumber, System.Globalization.CultureInfo.InvariantCulture, out var c))
                    {
                        // Something is wrong, return original string
                        retval.Append("0x" + hex);
                    }
                    else
                    {
                        retval.Append(((char)c).ToString());
                    }
                    lastPos = pos;
                    pos = s.IndexOf("0x", pos + 6);
                }
            }
            retval.Append(s[(lastPos + 6)..]);
            return retval.ToString();
        }

        /// <summary>
        /// TODO: Introduce proper unit-testing in AgoRapide!
        /// </summary>
        public static void DecodeUnitTest() =>
            new List<(string, string)> { 
                // TOOD: This data structure should be used in documentation examples.
                // TODO: (in general, unit-test data should be reused as documentation examples)
                ("GHIJ", "GHIJ"),
                ("0x0047HIJ", "GHIJ"), // Legal encodings
                ("G0x0048IJ", "GHIJ"),
                ("GH0x0049J", "GHIJ"),
                ("GHI0x004A", "GHIJ"),
                ("0xooooGHI", "0xGHI"),
                ("G0xooooHI", "G0xHI"),
                ("GH0xooooI", "GH0xI"),
                ("GHI0xoooo", "GHI0x"),
                ("0x004XGHI", "0x004XGHI"), // Illegal encodings
                ("G0x004XHI", "G0x004XHI"),
                ("GH0x004XI", "GH0x004XI"),
                ("GHI0x004X", "GHI0x004X"),
                ("GHIJ0x004", "GHIJ0x004"),
                ("GHIJK0x00", "GHIJK0x00"),
                ("GHIJKL0x0", "GHIJKL0x0"),
                ("GHIJKLM0x", "GHIJKLM0x"),
            }.ForEach(tOriginal =>
            {
                new List<Func<(string, string), (string, string)>> { // Create variations (especially longer strings)
                    t => (t.Item1, t.Item2),
                    t => (t.Item1 + t.Item1, t.Item2 + t.Item2),
                    t => ("XYZ" + t.Item1, "XYZ" + t.Item2),
                    t => (t.Item1 + "XYZ", t.Item2 + "XYZ"),
                    t => ("UVWXYZ" + t.Item1, "UVWXYZ" + t.Item2),
                    t => (t.Item1 + "UVWXYZ", t.Item2 + "UVWXYZ"),
                }.ForEach(f =>
                {
                    var t = f(tOriginal);
                    if (!Decode(t.Item1).Equals(t.Item2))
                    {
                        throw new PropertyStreamLineException(nameof(Decode) + ": String\r\n" +
                            "'" + t.Item1 + "'\r\n" +
                            "decoded as\r\n" +
                            "'" + Decode(t.Item1) + "'\r\n" +
                            "instead of\r\n" +
                            "'" + t.Item2 + "'");
                    }
                });
            });

        public static T ParseDirectToIP<T>(IEnumerable<string> singlePropertyStreamLines) where T : notnull, IP =>
            TryParseDirectToIP(singlePropertyStreamLines, out T retval, out var errorResponse) ? retval : throw new PropertyStreamLineException(
                nameof(errorResponse) + ": " + errorResponse + "\r\n" +
                "\r\n" +
                nameof(singlePropertyStreamLines) + ":\r\n" +
                string.Join("\r\n", singlePropertyStreamLines)
            );

        public static bool TryParseDirectToIP<T>(string singlePropertyStreamLine, out T retval) where T : notnull, IP =>
            TryParseDirectToIP(new List<string> { singlePropertyStreamLine }, out retval, out _);
        public static bool TryParseDirectToIP<T>(string singlePropertyStreamLine, out T retval, out string errorResponse) where T : notnull, IP =>
            TryParseDirectToIP(new List<string> { singlePropertyStreamLine }, out retval, out errorResponse);
        public static bool TryParseDirectToIP<T>(IEnumerable<string> singlePropertyStreamLines, out T retval) where T : notnull, IP =>
            TryParseDirectToIP(singlePropertyStreamLines, out retval, out _);
        /// <summary>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="singlePropertyStreamLines"></param>
        /// <param name="errorResponse"></param>
        /// <param name="nonStrict">
        /// NOTE: Should be used with caution as it will actually insert known invalid data.
        /// </param>
        /// <returns></returns>
        [ClassMember(
            Description =
                "Parses a series of -" + nameof(PropertyStreamLine) + "- directly to the given type of -" + nameof(IP) + "-.\r\n" +
                "\r\n" +
                "Typical usage would be parsing a series of configuration values.\r\n" +
                "\r\n" +
                "Example:\r\n" +
                "Given some values known to describe an -" + nameof(ConnectionInstruction) + "- like:\r\n" +
                nameof(ConnectionInstructionP.ServerHostName) + " = localhost\r\n" +
                nameof(ConnectionInstructionP.ServerPortNo) + " = 4246\r\n" +
                nameof(ConnectionInstructionP.DataTransferDirection) + " = Receive\r\n" +
                "this method can return a -" + nameof(ConnectionInstruction) + "- directly.\r\n" +
                "\r\n" +
                "See also -" + nameof(PSPrefix.dtr) + "- and -" + nameof(IP.TryParseDtr) + "-.\r\n"
        )]
        public static bool TryParseDirectToIP<T>(IEnumerable<string> singlePropertyStreamLines, out T retval, out string errorResponse) where T : notnull, IP
        {

            if (!TryParseDirectToIPNonGeneric(singlePropertyStreamLines, typeof(T), out var ip, out errorResponse))
            {
                retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            retval = (T)(ip);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Parses direct to a given property value.\r\n" +
            "The -" + nameof(PropertyStreamLine) + "- parameter is supposed to consist of only key = value, " +
            "like 'DataTransferDirection = Receive'.\r\n" +
            "\r\n" +
            "Not implemented as of May 2020"
        )]
        public static bool TryParseDirectToProperty<T>(object keyAsEnum, string singlePropertyStreamLine, out T retval, out string errorResponse) where T : notnull
        {
            throw new NotImplementedException();
            //retval = default!;
            //errorResponse = default!;
            //return false;
        }

        /// <summary>
        /// Introduced in order to have simpler code in <see cref="TryParseDirectToProperty"/> (no need to invoke through reflection)
        /// </summary>
        /// <param name="singlePropertyStreamLines"></param>
        /// <param name="typeRequired"></param>
        /// <param name="retval"></param>
        /// <param name="errorResponse"></param>
        /// <param name="nonStrict"></param>
        /// <returns></returns>
        private static bool TryParseDirectToIPNonGeneric(IEnumerable<string> singlePropertyStreamLines, Type typeRequired, out IP retval, out string errorResponse)
        {
            var ip = new PRich(); // TODO: Can we improve parsing, make code shorter?
            foreach (var a in singlePropertyStreamLines)
            {
                if (!TryParseAndStore(
                    dataStorage: ip,
                    propertyStreamLine:
                        typeRequired.ToStringShort() +
                        "/1/" + // Dummy line in order to parse correctly. TODO: Improve on this (May 2020)
                    a,
                    out errorResponse
                ))
                {
                    retval = default!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                    return false;
                }
            }
            retval = ip.IP. // TODO: Can we improve parsing, make code shorter?
                GetP<IP>(IKType.FromType(typeRequired)).
                GetP<IP>(IKString.FromCache("1")); // Dummy hierarchy level (see above)
            InvalidObjectTypeException.AssertEquals(retval, typeRequired);

            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        [ClassMember(Description =
            "Note that throws exception if -" + nameof(TryParseAndStore) + "- fails.\r\n" +
            "Use -" + nameof(ParseAndStoreFailSafe) + "- instead if this is not desired behaviour.\r\n"
        )]
        public static void ParseAndStore(IP dataStorage, string propertyStreamLine)
        {
            if (!TryParseAndStore(dataStorage, propertyStreamLine, out var errorResponse))
            {
                throw new PropertyStreamLineException(
                    nameof(errorResponse) + ": " + errorResponse + "\r\n" +
                    "\r\n" +
                    nameof(propertyStreamLine) + ": " + propertyStreamLine);
            }
        }

        [ClassMember(Description =
            "If either parsing or storing fails then this method will call -" + nameof(StoreFailure) + "- " +
            "in order to keep some track of why parsing or storing failed.\r\n" +
            "\r\n" +
            "Use -" + nameof(ParseAndStore) + "- instead if you want an exeption being thrown at failure."
        )]
        public static void ParseAndStoreFailSafe(IP dataStorage, string propertyStreamLine)
        {
            if (!TryParseAndStore(dataStorage, propertyStreamLine, out var errorResponse))
            {
                StoreFailure(dataStorage, propertyStreamLine, errorResponse);
            }
        }

        public static bool TryParseAndStore(IP dataStorage, string propertyStreamLine, out string errorResponse)
        {
            if (!PropertyStreamLineParsed.TryParse(propertyStreamLine, out var p, out errorResponse)) return false;
            return TryStore(dataStorage, p, out errorResponse);
        }

        public static void Store(IP dataStorage, PropertyStreamLineParsed propertyStreamLine)
        {
            if (!TryStore(dataStorage, propertyStreamLine, out var errorResponse))
            {
                throw new PropertyStreamLineException(
                    nameof(errorResponse) + ": " + errorResponse + "\r\n" +
                    "\r\n" +
                    nameof(propertyStreamLine) + ": " + propertyStreamLine);
            }
        }

        [ClassMember(Description =
            "Stores if possible and fails silently.\r\n" +
            "\r\n" +
            "If storing fails then this method will try to store a 'ParseOrStoreFailure'-object " +
            "at root-level in data storage, with detailed information about what exactly failed.\r\n" +
            "See -" + nameof(StoreFailure) + "- for more information."
        )]
        public static void StoreFailSafe(IP dataStorage, PropertyStreamLineParsed propertyStreamLine)
        {
            if (!TryStore(dataStorage, propertyStreamLine, out var errorResponse))
            {
                StoreFailure(
                    dataStorage,
                    propertyStreamLine.ToString(),
                    errorResponse);
            }
        }

        [ClassMember(Description =
            "Stores a 'ParseOrStoreFailure'-object. " +
            "\r\n" +
            "TODO: Create separate class for this.\r\n" +
            "\r\n" +
            "Stored at root-level in the given data storage, with detailed information about what exactly failed.\r\n" +
            "\r\n" +
            "Note that the 'ParseOrStoreFailure'-object will overwrite any older such object already written.\r\n" +
            "In other words it gives information about the last property stream line that failed to either parse or store.\r\n" +
            "\r\n" +
            "The 'CountStored' property tells how many such objects have been created since the application was initiated.\r\n" +
            "\r\n" +
            "'ParseOrStoreFailure' object gives an emergency indication " +
            "that something is wrong with for instance data received through " +
            "a -" + nameof(Subscription) + "- over the -" + nameof(ARConcepts.PropertyStream) + "- " +
            "without the calling method having to take any action there and then."
        )]
        public static void StoreFailure(IP dataStorage, string propertyStreamLine, string errorResponse, [System.Runtime.CompilerServices.CallerMemberName] string caller = "")
        {
            var parseOrStoreFailure = new PRich();
            // Note flexibility in AgoRapide for expressing any kind of data without any predefined schema.
            // TODO: Consider creating a more strongly typed object for this purpose.
            parseOrStoreFailure.IP.AddPV(IKString.FromString("_Description"),
                ClassMemberAttribute.GetAttribute(typeof(PropertyStreamLine), nameof(StoreFailure)).IP.GetPV<string>(BaseAttributeP.Description)
            );
            parseOrStoreFailure.IP.AddPV(PP.Created, UtilCore.DateTimeNow);
            parseOrStoreFailure.IP.AddPV("CreatingMethod", caller);
            parseOrStoreFailure.IP.AddPV(nameof(propertyStreamLine), propertyStreamLine);
            parseOrStoreFailure.IP.AddPV(nameof(errorResponse), errorResponse);
            var countStored = 0L;
            if (dataStorage.TryGetP<IP>("ParseOrStoreFailure", out var lastFailure) && lastFailure.TryGetPV<long>("CountStored", out var lastCount))
            {
                countStored = lastCount;
            }
            countStored++;
            parseOrStoreFailure.IP.AddPV("CountStored", countStored);
            if (!dataStorage.TrySetP("ParseOrStoreFailure", parseOrStoreFailure, out _))
            {
                // This is very unexpected, the given data storage is restrictive about which keys can be added.
                // Give up totally because this method is promised to be failsafe 
                // (therefore we do not throw any exception here).
            }
        }

        public static bool TryStore(IP dataStorage, PropertyStreamLineParsed propertyStreamLine) =>
            TryStore(dataStorage, propertyStreamLine, errorResponse: out _);


        public static DateTime CurrentTimestamp { get; private set; }
        private static readonly PK _PKCreated = PK.FromEnum(PP.Created);
        /// <summary>
        /// </summary>
        /// <param name="propertyStreamLine"></param>
        /// <param name="nonStrict">
        /// NOTE: Should be used with caution as it will actually insert known invalid data.
        /// </param>
        [ClassMember(
            Description =
                "Stores the given -" + nameof(PropertyStreamLineParsed) + "- in the given storage object.\r\n" +
                "\r\n" +
                "This is AgoRapide's main deserialization mechanism (together with -" + nameof(PropertyStreamLineParsed.TryParse) + "-).\r\n" +
                "The main serialization mechanism is -" + nameof(IP.ToPropertyStream) + "-\r\n" +
                "\r\n" +
                "Supposed to be called repeatedly by some mechanism listening to the -" + nameof(ARConcepts.PropertyStream) + "-, " +
                "resulting in a 'complete' object representation being built up.\r\n" +
                "That is, turning the very basic -" + nameof(PropertyStreamLine) + "- format " +
                "into a more usable object oriented format for in-memory processing of data.\r\n" +
                "\r\n" +
                "TODO: Build in support for transactions. See for -" + nameof(ARConcepts.PropertyStream) + "- " +
                "TODO: 'FAQ: Can the property stream format support transactions'\r\n" +
                "TODO: for an outline about how this can be done." +
                "\r\n" +
                "TODO: Consider having short-cut 'SetML' in IP (SetML = Set multi-level) which just calls this method.\r\n" +
                "\r\n" +
                "Parses the actual property-value (-" + nameof(PropertyStreamLineParsed.Value) + "-) with help of -" + nameof(PK.TryValidateAndParse) + "- " +
                "(if a corresponding -" + nameof(PK) + "- is found. " +
                "\r\n" +
                "Possible fail scenarios are:\r\n" +
                "\r\n" +
                "1) Creation of hierarchical keys. Call to -" + nameof(IP.TrySetP) + "- may fail.\r\n" +
                "Note that in general no validation is done for the keys, so for instance a misspelling of Customer as 'Customr/42/FirstName = John' " +
                "will be accepted and stored accordingly (unless the actual storage has implemented some restrictions for what keys to accept).\r\n" +
                "\r\n" +
                "2) The final value parsing (and then only if a -" + nameof(PK) + "- is found). Call to -" + nameof(PK.TryValidateAndParse) + "- may fail.\r\n" +
                "\r\n" +
                "3) The final value setting. Call to -" + nameof(IP.TrySetP) + "- may fail.\r\n" +
                "\r\n" +
                "The value is stored by call to -" + nameof(IP.TrySetP) + "-.\r\n" +
                "This means that repeated occurrences of the same value will overwrite older ones.\r\n" +
                "Example, for the following property stream lines:\r\n" +
                "'Customer/42/FirstName = John'\r\n" +
                "'...' (some other property stream lines)\r\n" +
                "'Customer/42/FirstName = Johnny'\r\n" +
                "the final value of Customer/42/FirstName will be 'Johnny'.\r\n" +
                "\r\n" +
                "Note that the parsing concept is inherently not thread-safe and " +
                "this method should therefore only be called within a single-threaded context " +
                "(see -" + nameof(ARConcepts.SingleThreadedCode) + "-).\r\n"
        )]
        public static bool TryStore(IP dataStorage, PropertyStreamLineParsed propertyStreamLine, out string errorResponse)
        {
            if (propertyStreamLine.IsComment)
            {
                // Ignore.
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }

            var keys = propertyStreamLine.Keys;
            var value = propertyStreamLine.Value; // Note: This might be null

            /// Loop through all keys, look for existing keys, when no key found at a specific level, create objects as needed (with IP.SetP).
            /// At end of loop, use ordinary IP.SetP on the last object. Use PK.TryValidateAndParse if available.
            var currentPointer = dataStorage;
            IP? lastPointer = null;
            int i;
            for (i = 0; i < (keys.Count - 1); i++)
            {
                if (!currentPointer.TryGetP<IP>(keys[i].key, out var newCurrentPointer))
                {
                    InvalidTypeException.AssertAssignable(
                        foundType: keys[i].valueType ?? throw new NullReferenceException(
                            "valueType is null:\r\n" +
                            "Since not at last level, should have been set by parser in -" + nameof(PropertyStreamLineParsed) + "-.\r\n" +
                            "\r\n" +
                            "At hierarchical level " + i + " (zero based) of " + keys.Count + " (key " + keys[i].ToString() + ").\r\n" +
                            "\r\n" +
                            nameof(propertyStreamLine) + ": " + propertyStreamLine
                        ),
                        expectedType: typeof(IP),
                        detailer: () =>
                            "At hierarchical level " + i + " (zero based) of " + keys.Count + " (key " + keys[i].ToString() + ").\r\n" +
                            "\r\n" +
                            nameof(propertyStreamLine) + ": " + propertyStreamLine
                    );
                    object obj;
                    try
                    {
                        // This is a vulnerable point in parsing. catch errors in order to give context in the final exception message
                        obj = System.Activator.CreateInstance(keys[i].valueType);
                    }
                    catch (Exception ex)
                    {
                        /// Exception may occur if the IP implementing class has not been properly implemented
                        /// like for instance not having a parameter-less constructor.
                        /// Note however that this applies for both <see cref="PValue{T}"/> and <see cref="PValueEmpty"/>
                        /// This is a programming error, not a user data error, therefore throw exception (do not set errorResponse and return false)
                        throw new PropertyStreamLineException(
                            "At hierarchical level " + i + " (zero based) of " + keys.Count + " (key " + keys[i].ToString() + ").\r\n" +
                            "Exception + " + ex.GetType() + " with message " + ex.Message + "\r\n" +
                            "\r\n" +
                            nameof(propertyStreamLine) + ": " + propertyStreamLine,
                            inner: ex
                        );
                    }
                    // }
                    newCurrentPointer = (IP)obj;
                    if (newCurrentPointer is ICreated)
                    { /// Signifies that it can store metadata <see cref="PP.Created"/> 
                        if (!newCurrentPointer.TrySetP(new IKIP(_PKCreated, new PValue<DateTime>(CurrentTimestamp)), out errorResponse))
                        { // Use most direct overload for setting of property (especially since this code is very performance sensitive)
                            throw new PropertyStreamLineException(
                                "At hierarchical level " + i + " (zero based) of " + keys.Count + " (key " + keys[i].ToString() + ").\r\n" +
                                "TrySetP of " + nameof(CurrentTimestamp) + " for " + nameof(ICreated) + " failed.\r\n" +
                                nameof(errorResponse) + ": " + errorResponse + "\r\n" +
                                "\r\n" +
                                nameof(propertyStreamLine) + ": " + propertyStreamLine
                            );
                        }
                    }
                    if (!currentPointer.TrySetP(new IKIP(keys[i].key, newCurrentPointer), out errorResponse))
                    { // Use most direct overload for setting of property (especially since this code is very performance sensitive)
                        throw new PropertyStreamLineException(
                            "At hierarchical level " + i + " (zero based) of " + keys.Count + " (key " + keys[i].ToString() + ").\r\n" +
                            "TrySetP failed.\r\n" +
                            nameof(errorResponse) + ": " + errorResponse + "\r\n" +
                            "\r\n" +
                            nameof(propertyStreamLine) + ": " + propertyStreamLine
                        );
                    }
                }

                if (i == (keys.Count - 2) && keys[i + 1].key.ToString().Equals(nameof(PP.Invalid)))
                {
                    // Prune object tree from this level

                    // Example:
                    // Customer/42/Invalid
                    // newCurrentPointer is the "42"-object, remove it from the 'Customer' object.
                    return currentPointer.TryRemoveP(keys[i].key, out errorResponse);
                }

                var t = newCurrentPointer.GetType();
                if (
                    t.Equals(typeof(PValueEmpty)) ||
                    (
                        t.IsGenericType &&
                        t.GetGenericTypeDefinition().Equals(typeof(PValue<>))
                    )
                )
                {
                    /// It is not possible to call <see cref="IP.TrySetP"/> for this object.
                    /// This will happen if the property stream looks something like this:
                    ///   Customer/42 
                    ///   Customer/42/FirstName = John
                    /// The first line will create a <see cref="PValueEmpty"/> object for key '42', which will have to be replaced 
                    /// when storing the second line (ths problem now) with a <see cref="PRich"/> instead.
                    /// Another example is
                    ///   Customer/42 = John Smith
                    ///   Customer/42/FirstName = John
                    /// The first line will create a <see cref="PValue{string}"/> object for key '42', which will have to be replaced 
                    /// when storing the second line (ths problem now) with a <see cref="PRich"/> instead.
                    /// The original <see cref="PValue{string}"/> object  will be stored as <see cref="PP.Value"/> inside the <see cref="PRich"/>
                    /// 
                    /// NOTE: If there actually exists an <see cref="IP"/>-implementing class called 'Customer' the
                    /// NOTE: issue is not relevant, as the object for key '42' would have been created as a 'Customer' object straight away.
                    /// NOTE. (In other words, the example above is only relevant without an explicit type called 'Customer'.)
                    /// NOTE: This also explains why we do not have to look for the proper object type to create now, we can just
                    /// NOTE. use <see cref="PRich"/>
                    var newObject = new PRich();
                    if (t.Equals(typeof(PValueEmpty)))
                    {
                        // There is no point in storing this. PValueEmpty was only a placeholder, which have now been replaced with a PRich
                    }
                    else
                    {
                        // Store original value, so it does not get lost
                        newObject.IP.AddP(PP.Value, newCurrentPointer);
                    }
                    if (!currentPointer.TrySetP(keys[i].key, newObject, out var replaceFailedErrorResponse))
                    {
                        throw new PropertyStreamLineException(
                            "Unable to replace object of type " + newCurrentPointer.GetType() + " with a " + nameof(PRich) + " object and key " + PP.Value + "\r\n" +
                            "as key " + keys[i].key + " inside " + currentPointer.GetType().ToString() + ".\r\n" +
                            "errorResponse was: " + replaceFailedErrorResponse + "\r\n"
                        );
                    }
                    newCurrentPointer = newObject;
                }
                lastPointer = currentPointer;
                currentPointer = newCurrentPointer;
            }

            // Wrong approach, do not delete keys, delete single key one level above (see above for final solution)
            //if (keys[i].key.ToString().Equals(nameof(PP.Invalid))) {
            //    // Delete all keys in currentPointer
            //    // Do not store Invalid as key
            //    var errors = new List<string>();
            //    var ikips = currentPointer.ToList();
            //    ikips.ForEach(ikip => {
            //        if (!currentPointer.TryRemoveP(ikip.Key, out var removeErrorResponse)) {
            //            errors.Append("Key: " + ikip.Key.ToString() + ": " + removeErrorResponse);
            //        }
            //    });
            //    if (errors.Count == 0) {
            //        errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            //        return true;
            //    } else {
            //        errorResponse =
            //            "Failed to remove " + errors.Count + " keys out of " + ikips.Count + "\r\n" +
            //            "errorResponse for each key was:\r\n" +
            //            string.Join("\r\n", errors);
            //    }
            //}

            if (i != (keys.Count - 1)) throw new PropertyStreamLineException(nameof(i) + " is " + i + " but was supposed to be " + (keys.Count - 1) + "\r\n");

            IKIP? ikip; // Will contain the final value to store

            if (currentPointer is IPII)
            {
                /// The key itself is the actual value. Parse it (based on PK on level up), and change the key
                /// (This was found out by <see cref="PropertyStreamLineParsed.TryParse"/> because of <see cref="Cardinality.IndividualItems"/>
                if (i == 0 || !(keys[i - 1].key is PK pk))
                {
                    throw new PropertyStreamLineException(
                        "At hierarchical level " + i + " (zero based) of " + keys.Count + " (key " + keys[i].ToString() + ").\r\n" +
                        "Unexpected find of " + currentPointer.GetType().ToStringShort() + "." +
                        "i = " + i + "\r\n" +
                        (
                            i == 0 ?
                                ("Key " + keys[i].key) :
                                (
                                    "Key at position " + (i - 1) + " (" + keys[i - 1].key.ToString() + ") was not of type " + typeof(PK).ToStringShort() + " but " + keys[i - 1].GetType().ToStringShort() + "\r\n" +
                                    "Key " + keys[i - 1].key + "/" + keys[i].key
                                )
                        ) + "\r\n" +
                        ""
                    );
                }
                if (!pk.TryValidateAndParse(keys[i].key.ToString(), out var result))
                {
                    errorResponse = "For key " + pk.ToString() + ": " + result.ErrorResponse;
                    return false;
                }

                // return currentPointer.TrySetP(IIKII.PackParseResultForStorageInIKII(result), PValueEmpty.Singleton, out errorResponse);
                ikip = new IKIP(IIKII.PackParseResultForStorageInIKII(result), PValueEmpty.Singleton);
            }
            else if (keys[i].key is PK pk)
            {
                if (value == null)
                {
                    // TODO: Add concept in PKTypeAttribute for whether to allow setting value to null

                    /// Note how this is quite a common scenario.
                    /// We may for instance have "deletion" of properties done in this manner:
                    ///   'Customer/42/FirstName = '
                    /// (not actually recommended (use <see cref="PP.Invalid"/> instead) but quite plausible)
                    /// or
                    /// we may actually have keys only functioning as markers, for instance
                    ///  'Payment/42/Complete = '
                    /// which marks that payment is completed, but without any corresponding value.

                    // return currentPointer.TrySetP(pk, PValueEmpty.Singleton, out errorResponse);
                    ikip = new IKIP(pk, PValueEmpty.Singleton);
                }
                else if (!pk.TryValidateAndParse(value, out var result))
                {
                    errorResponse = "For key " + pk.ToString() + ": " + result.ErrorResponse;
                    return false;
                }
                else
                {
                    // First, update current timestamp if relevant
                    if (pk.__enum is StreamProcessorP e && e == StreamProcessorP.Timestamp)
                    {
                        CurrentTimestamp = result.Result as DateTime? ?? throw new InvalidObjectTypeException(result.Result!, typeof(DateTime), pk.ToString());
                    }

                    // return currentPointer.TrySetP(pk, pk.PackParseResultForStorageInEntityObject(result), out errorResponse); // Result is assumed set now, therefore use of !
                    ikip = new IKIP(pk, pk.PackParseResultForStorageInEntityObject(result));
                }
            }
            else if (value == null)
            {
                /// Key is either a known type (IKType), or just a string (IKString)

                // return currentPointer.TrySetP(keys[i].key, PValueEmpty.Singleton, out errorResponse);
                ikip = new IKIP(keys[i].key, PValueEmpty.Singleton);
            }
            else
            {
                /// We can only store the value as string because we have no <see cref="PK"/> 
                /// 
                /// Most probable because we are <see cref="ARConcepts.StandardAgoRapideCode"/>

                /// TODO: Text below is probably irrelevant now (Feb 2021), as a semicolon should be taken care
                /// TODO: of in the decoding process.
                /// TOOD: 
                /// NOTE: Theoretically, cardinality <see cref="Cardinality.WholeCollection"/> might now have been intended
                /// NOTE: value might be "Red;Green" for instance. It is however best to totally ignore this 
                /// NOTE: issue here. Presence of ';' for instance does not mean anything, it could be part of an 
                /// NOTE: actual single item value. Since we do not have have corresponding property key (<see cref="PK"/>) 
                /// NOTE: we are probably <see cref="ARConcepts.StandardAgoRapideCode"/>, and should we distribute 
                /// NOTE: this property out again, onto the <see cref="ARConcepts.PropertyStream"/> it will be properly 
                /// NOTE: formatted anyway.

                ikip = new IKIP(keys[i].key, new PValue<string>(value));
            }
            if (ikip == null) throw new NullReferenceException(nameof(ikip) + ": " + nameof(propertyStreamLine) + ": " + propertyStreamLine.ToString());
            return lastPointer != null ?
                lastPointer.TrySetPP(keys[^2].key, currentPointer, ikip, out errorResponse) : /// Ensures use of <see cref="ARConcepts.Indexing"/>
                currentPointer.TrySetP(ikip, out errorResponse); /// We are at root-level in datastorage, not possible to do indexing
        }

        public class PropertyStreamLineException : ApplicationException
        {
            public PropertyStreamLineException(string message) : base(message) { }
            public PropertyStreamLineException(string message, Exception inner) : base(message, inner) { }
        }
    }
}