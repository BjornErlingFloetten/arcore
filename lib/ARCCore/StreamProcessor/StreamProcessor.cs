﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using System.Linq;

/// <summary>
/// TODO: Move classes other than StreamProcessor to separate files.
/// Contains:
/// <see cref="ARCCore.StreamProcessor"/>
/// <see cref="ARCCore.PropertyStreamLineWithOrigin"/>
/// <see cref="ARCCore.StreamProcessorP"/>
/// <see cref="ARCCore.StorageFile"/>
/// <see cref="ARCCore.StorageFileP"/>
/// <see cref="ARCCore.ConnectionDirection"/>
/// <see cref="ARCCore.DataTransferDirection"/>
/// </summary>
namespace ARCCore {
    [Class(
        Description =
            "Implements the storage mechanism for AgoRapide.\r\n" +
            "\r\n" +
            "Coordinates the -" + nameof(ARConcepts.PropertyStream) + "- with other nodes.\r\n" +
            "\r\n" +
            "TODO: Implement discarding of data when not listening to incoming connections (saves memory usage).\r\n" +
            "TODO: (search for this TODO in code to see where relevant to make changes).\r\n" +
            "\r\n" +
            "Has the following functionality:\r\n" +
            "(note: Terminology is kept close to the one used for the MQTT protcol, although AgoRapide does not use MQTT in itself)\r\n" +
            "\r\n" +
            "1) Listen to incoming TCP/IP connections from other nodes. An incoming connections may be either for:\r\n" +
            "   a) Incoming data (from a publishing client), for us to distribute.\r\n" +
            "      (\r\n" +
            "         as a -" + nameof(ARNodeType.CoreDB) + "- we would then store the data locally and " +
            "         then distribute the data to all current downstream subscriber clients as relevant according to their respective subscriptions " +
            "         (Note: For incoming data on an incoming connection an implicit -" + nameof(Subscription.IsAll) + "-, '+*', is assumed), " +
            "         as a -" + nameof(ARNodeType.ArmDB) + "- we would then forward the data to a -" + nameof(ARNodeType.CoreDB) + "-" +
            "      )\r\n" +
            "      or for\r\n" +
            "   b) Outgoing data, downstream subscribing client requests subscription\r\n" +
            "      (\r\n" +
            "        we would then typically be a -" + nameof(ARNodeType.CoreDB) + "- or a -" + nameof(ARNodeType.ArmDB) + "-.\r\n" +
            "        After having reveived the -" + nameof(Subscription) + "- and -" + nameof(ClientUpdatePosition) + "-\r\n" +
            "        from the subscribing client we will then keep the client continously up-to-date with new data.\r\n" +
            "      ).\r\n" +
            "\r\n" +
            "2) Connect to other nodes. An outgoing connection may be either for\r\n" +
            "   a) Publishing properties. " +
            "      (" +
            "         As a publishing -" + nameof(ARNodeType.Client) + "- the data would be locally in-application generated properties.\r\n" +
            "         As an -" + nameof(ARNodeType.ArmDB) + "- the data would typically have been received over an incoming connection from a -" + nameof(ARNodeType.Client) + "-" +
            "      )\r\n" +
            "      or for\r\n" +
            "   b) Receiving (as downstream subscribing client) data from upstream server.\r\n" +
            "      (\r\n" +
            "        We would then typically be an -" + nameof(ARNodeType.ArmDB) + "- or -" + nameof(ARNodeType.Client) + "-\r\n" +
            "        We would send -" + nameof(Subscription) + "- and -" + nameof(ClientUpdatePosition) + "- as soon as connection is established.\r\n" +
            "      )\r\n" +
            "\r\n",
        LongDescription =
            "TODO: May 2020: The code in this class should be tidied up a bit before any more functionality is added.\r\n" +
            "\r\n" +
            "Note that the examples above do not take into scenarios like load-balancing and fault tolerance where -" + nameof(ARNodeType.CoreDB) + "- would be " +
            "connecting among themselves.\r\n" +
            "\r\n" +
            "Note that information about outgoing connection and their purpose should be possible to distribute over the -" + nameof(ARConcepts.PropertyStream) + "-, " +
            "therefore we have implemented the class -" + nameof(ActualConnection) + "- (distribution itself not implemented as of Mar 2020).\r\n" +
            "\r\n" +
            "Note: A vision for AgoRapide is that load-balancing / fault tolerance and similar should be mostly a configuration issue around already in-built " +
            "functionality in -" + nameof(ARComponents.ARCCore) + "- / -" + nameof(StreamProcessor) + "-. In other words the core functionality should provide " +
            "a language which is suffiently rich in order for you to describe these different setups, there should be no need for additional C# code.\r\n" +
            "\r\n" +
            "Note how this class inherits -" + nameof(PConcurrent) + "- which offers thread-safe access to properties " +
            "(and is also able to -" + nameof(ARConcepts.ExposingApplicationState) + "- to outside)."

    )]
    public class StreamProcessor : PConcurrent, IDisposable {

        [ClassMember(
            Description =
                "The outside (in the sense outside of this class) local mechanism which receives all -" + nameof(PropertyStreamLine) + "- registered by this class.\r\n" +
                "(except changes with -" + nameof(PropertyStreamLineWithOrigin.HasLocalOrigin) + "-.)\r\n" +
                "\r\n" +
                "The 'outside' implementation will typically store data to a -" + nameof(PRich) + "- instance " +
                "through -" + nameof(PropertyStreamLine.ParseAndStore) + "-.\r\n" +
                "\r\n" +
                "Set this method when you want an always-up-to-date in-memory object collection of your data.\r\n" +
                "Most relevant for -" + nameof(ARNodeType.Client) + "-.\r\n" +
                "Also relevant for -" + nameof(ARNodeType.ArmDB) + "- (see -" + nameof(ARConcepts.AdHocQuery) + "-).\r\n" +
                "\r\n" +
                "If this method is set, then incoming data will be stored both to disk (unless -" + nameof(StreamProcessorP.DoNotStoreLocally) + "- is TRUE) and sent to this method.\r\n" +
                "Also, at application startup (-" + nameof(StreamProcessor.StartTCPIPCommunication) + "-), all data from disk will be sent to this method.\r\n" +
                "\r\n" +
                "Rationale for not distributing -" + nameof(PropertyStreamLineWithOrigin.HasLocalOrigin) + "- via this method:\r\n" +
                "If local originated content also was sent to this method, it would most probably have been sent twice, after a round-trip to " +
                "-" + nameof(ARNodeType.CoreDB) + "- and back, resulting in duplicate updates to the (presumed -" + nameof(PRich) + "-) outside's storage. " +
                "The 'outside' can instead choose whether to update its presumed -" + nameof(PRich) + "- storage immediately or not, by just calling this " +
                "method itself whenever it also calls -" + nameof(ReceiveAndDistribute) + "-",
            LongDescription =
                "Remember thread-safety regarding use of this method.\r\n" +
                "\r\n" +
                "Possible implementions on outside could be:\r\n" +
                "\r\n" +
                "1) Approach suitable for a processor, running in a continous loop:\r\n" +
                "\r\n" +
                "   var queue = new System.Collections.Concurrent.ConcurrentQueue<string>();\r\n" +
                "   myStreamProcessorInstance.OutsideLocalReceiver = s => queue.Enqueue(s);\r\n" +
                "\r\n" +
                "You can then pick up from the queue inside your continous processing loop in a thread-safe manner, for instance like this:\r\n" +
                "\r\n" +
                "   var dataStorage = new PRich();\r\n" +
                "   while (true) { // Your continous processing loop\r\n" +
                "      DoTask1(dataStorage);  // Task1 may freely access dataStorage (within calling thread)\r\n" +
                "      DoTask2(dataStorage);  // Task2 may freely access dataStorage (within calling thread)\r\n" +
                "      while (queue.TryDequeue(out var s)) PropertyStreamLine.ParseAndStore(dataStorage, s);\r\n" +
                "   }\r\n" +
                "\r\n" +
                "\r\n" +
                "2) Approach suitable for an API with incoming requests occuring on multiple threads:\r\n" +
                "(simplified code)\r\n" +
                "\r\n " +
                "   var lock = new System.Threading.ReaderWriterLockSlim();\r\n" +
                "   var dataStorage = new PRich();\r\n" +
                "   myStreamProcessorInstance.OutsideLocalReceiver = s => {\r\n" +
                "      try {\r\n" +
                "         lock.EnterWriteLock();\r\n" +
                "         PropertyStreamLine.ParseAndStore(_storage, s);\r\n" +
                "      } finally {\r\n" +
                "         lock.ExitWriteLock();\r\n" +
                "      }\r\n" +
                "   };\r\n" +
                "\r\n" +
                "You must also remember to acquire the corresponding lock (lock.EnterReadLock or lock.EnterWriteLock) " +
                "when processing API methods, for instance like this:" +
                "\r\n" +
                "   public APIResponse ServeAPIMethod(APIRequest request) {\r\n" +
                "       try {\r\n" +
                "           lock.EnterReadLock();\r\n" +
                "           return dataStorage.Query(request);\r\n" +
                "       } finally {\r\n" +
                "           lock.ExitReadLock();\r\n" +
                "       }\r\n" +
                "   }\r\n" +
                "\r\n"
        )]
        public Action<string>? OutsideLocalReceiver { get; set; }

        private System.Net.Sockets.TcpListener? _listener;

        [ClassMember(
            Description =
                "Outgoing connections are made according to instructions given to us",
            LongDescription =
                "Instructions can also be given over the -" + nameof(ARConcepts.PropertyStream) + "-."
        )]
        private readonly ConcurrentDictionary<IK, ActualConnection> _outgoingConnections = new ConcurrentDictionary<IK, ActualConnection>();

        [ClassMember(Description =
            "Our incoming clients are themselves responsible for keeping track of their update status."
        )]
        private readonly ConcurrentDictionary<string, ActualConnection> _incomingConnections = new ConcurrentDictionary<string, ActualConnection>();

        [ClassMember(Description =
            "Each -" + nameof(ActualConnection) + "- stores its received data directly into this queue.\r\n" +
            "Locally originated data (in-application generated) is also stored in this queue."
        )]
        private readonly ConcurrentQueue<PropertyStreamLineWithOrigin> _queueReceive = new ConcurrentQueue<PropertyStreamLineWithOrigin>();

        [ClassMember(
            Description =
                "Note non-thread safe collections used and corresponding single-threaded access to this collection"
        )]
        private readonly List<StorageFile> _storage = new List<StorageFile>();

        public StreamProcessor() =>
            IP.SetPV(StreamProcessorP.UnderClosure, false); // Set at once because we later ask for it with GetPV (simplifies asking for it)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nodeId">
        /// </param>
        /// <param name="writeToConsole">
        /// See <see cref="StreamProcessorP.WriteToConsole"/><br>
        /// If TRUE then will also instruct <see cref="IP.Logger"/> to write to console.
        /// </param>
        /// <param name="doNotStoreLocally">
        /// See <see cref="StreamProcessorP.DoNotStoreLocally"/>
        /// </param>
        /// <param name="cacheDiskWrites">
        /// See <see cref="StreamProcessorP.CacheDiskWrites"/>
        /// Note: If TRUE remember that weakly implemented as of Apr 2020 (graceful shutdown mechanism for instance is missing)
        /// </param>
        /// <returns></returns>
        [ClassMember(Description =
            "Creates a bare-bones -" + nameof(StreamProcessor) + "- instance.\r\n" +
            "\r\n" +
            "Note: You are not obliged to use this method, you can call constructor directly instead and set up this instance 'manually'.\r\n" +
            "You can also call this method and add other properties afterwards.\r\n" +
            "\r\n" +
            "This method will (unless parameter doNotStoreLocally is TRUE):\r\n" +
            "Set -" + nameof(StreamProcessorP.LocalStorageFolder) + "- to a directory called 'Data' under the " + nameof(Environment.CurrentDirectory) + ",\r\n" +
            "Set -" + nameof(StreamProcessorP.IndividualStorageFileSize) + "- to 10 million (characters),\r\n" +
            "\r\n" +
            "This method will also:\r\n" +
            "Set -" + nameof(ARCCore.IP.Logger) + "- to call -" + nameof(SendFromLocalOrigin) + " (and write to console if that parameter is TRUE).\r\n" +
            "\r\n" +
            "\r\n" +
            "-" + nameof(StreamProcessorP.TimestampResolution) + "- will not be set (timestamps will not be inserted into the -" + nameof(ARConcepts.PropertyStream) + "-)\r\n" +
            "\r\n" +
            "This method will not set up any connections, nor call -" + nameof(Initialize) + "- or -" + nameof(StartTCPIPCommunication) + "-."
        )]
        public static StreamProcessor CreateBareBonesInstance(IK nodeId, bool writeToConsole = false, bool doNotStoreLocally = false, bool cacheDiskWrites = false) {
            var retval = new StreamProcessor();

            retval.IP.SetPV(StreamProcessorP.WriteToConsole, writeToConsole);

            retval.IP.SetPV(StreamProcessorP.DoNotStoreLocally, doNotStoreLocally);

            if (doNotStoreLocally) {
                // Do not set up storage and associated parameters.
            } else {
                retval.IP.SetPV(StreamProcessorP.LocalStorageFolder, Environment.CurrentDirectory + System.IO.Path.DirectorySeparatorChar + "Data" + System.IO.Path.DirectorySeparatorChar);
                retval.IP.SetPV(StreamProcessorP.CacheDiskWrites, cacheDiskWrites);
                retval.IP.SetPV(StreamProcessorP.IndividualStorageFileSize, 10000000L);
            }
            retval.IP.Logger = writeToConsole ?
                new Action<string>(s => {
                    s = PSPrefix.app + "/" + nodeId + "/" + nameof(StreamProcessor) + "/" + s;
                    Console.WriteLine(s);
                    retval.SendFromLocalOrigin(s);
                }) :
                s => retval.SendFromLocalOrigin(PSPrefix.app + "/" + nodeId + "/" + nameof(StreamProcessor) + "/" + s);
            return retval;
        }


        [ClassMember(Description =
            "Creates a new storage file and returns the complete path to the file created.\r\n" +
            "\r\n" +
            "Background information: As the current data storage file reaches the limit given in -" + nameof(StreamProcessorP.IndividualStorageFileSize) + " " +
            "a new one is created with filename corresponding to current timestamp."
        )]
        private string CreateNewStorageFile(bool doNotLog = false) {
            if (!doNotLog) IP.LogText(""); // If we are currently initializing, then we are unable to log (due to Receive being called in the end). TODO: Consider letting Receive accept logging...
            var f = IP.GetPV<string>(StreamProcessorP.LocalStorageFolder);
            var filename = UtilCore.DateTimeNow.ToString("yyyy-MM-dd HH.mm.ss") + ".txt";
            System.IO.File.Create(f + System.IO.Path.DirectorySeparatorChar + filename).Close();
            if (!doNotLog) IP.LogKeyValue(nameof(filename), filename);
            return f + System.IO.Path.DirectorySeparatorChar + filename;
        }

        [ClassMember(Description =
            "Initializes this instance of -" + nameof(StreamProcessor) + "-\r\n" +
            "\r\n" +
            "If -" + nameof(OutsideLocalReceiver) + "- has been set then this method will read all local storage files and send to this.\r\n" +
            "This method may therefore take some time.\r\n" +
            "\r\n" +
            "See also -" + nameof(StartTCPIPCommunication) + "-."

        )]
        public void Initialize() {
            IP.SetPV(StreamProcessorP.CurrentlyInitializing, true); // Delays logging to property stream (until storage file has been initialized for instance)
            IP.SetPV(StreamProcessorP.TimestampIsOld, true);
            OutsideLocalReceiver?.Invoke(nameof(StreamProcessorP.TimestampIsOld) + " = " + true);

            /// Important: We had to wait until now with AssertIntegrity, because it may add properties which again will
            /// call <see cref="SendFromLocalOrigin"/> before we are 'ready'.
            IP.AssertIntegrity();

            // Take memory snapshop
            var totalMemoryAtStart = GC.GetTotalMemory(forceFullCollection: true);
            IP.LogKeyValue(nameof(totalMemoryAtStart), totalMemoryAtStart.ToString());
            IP.LogExecuteTime(() => {

                if (IP.GetPV(StreamProcessorP.CopyFileStructureFromCore, false)) throw new NotImplementedException("Support for " + nameof(StreamProcessorP.CopyFileStructureFromCore) + " is not implemented");
                if (IP.GetPV(StreamProcessorP.CacheDiskWrites, false)) {
                    IP.LogKeyValue(nameof(StreamProcessorP.CacheDiskWrites), "WARNING: Support for " + nameof(StreamProcessorP.CacheDiskWrites) + " is weakly implemented");
                    IP.LogKeyValue(nameof(StreamProcessorP.CacheDiskWrites), "WARNING: There is for instance no graceful shutdown mechanism as of Apr 2020");
                    // throw new NotImplementedException("Support for " + nameof(StreamProcessorP.CacheDiskWrites) + " is not implemented");
                }

                if (IP.GetPV(StreamProcessorP.DoNotStoreLocally, false)) {
                    // Skip reading of storage files and client update position
                } else {
                    if (!IP.TryGetPV<string>(StreamProcessorP.LocalStorageFolder, out _)) {
                        // Give easy to understand explanation, instead of the somewhat cryptic one that results from call to GetPV below
                        throw new StreamProcessorException(
                            "Property " + nameof(StreamProcessorP.LocalStorageFolder) + " has not been set.\r\n" +
                            "Resolution: Set property before calling this method (" + nameof(Initialize) + ").\r\n" +
                            "NOTE: If you do not need local storage (for instance because you only need to set up a logging console), " +
                            "set " + nameof(StreamProcessorP.DoNotStoreLocally) + " to TRUE instead.\r\n"
                        );
                    }
                    IP.SetPV(StreamProcessorP.ClientUpdatePositionPath,
                        IP.GetPV<string>(StreamProcessorP.LocalStorageFolder) +
                        System.IO.Path.DirectorySeparatorChar +
                        nameof(ClientUpdatePosition) +
                        ".txt"
                    );

                    // Read all file names from storage path, read into memory latest file (the others can wait)
                    // TODO: Delete any old revisisions (which have been replaced by more compressed revisions?)
                    var f = IP.GetPV<string>(StreamProcessorP.LocalStorageFolder);
                    if (!System.IO.Directory.Exists(f)) {
                        IP.LogText("CreatingDirectory", f);
                        System.IO.Directory.CreateDirectory(f);
                    }
                    var l = System.IO.Directory.GetFiles(f).ToList();
                    l = l.Where(f => !f.EndsWith(nameof(ClientUpdatePosition) + ".txt")).ToList();
                    IP.LogKeyValue("StorageFiles.Count", l.Count.ToString());
                    if (l.Count == 0) {
                        var filename = CreateNewStorageFile(doNotLog: true);
                        l.Add(filename); // Initialize with empty file
                    }
                    l.Sort();
                    l.ForEach(i => _storage.Add(new StorageFile(i, new System.IO.FileInfo(i).Length)));

                    if (OutsideLocalReceiver == null) {
                        // Older files will be read as needed (when requested by clients)
                    } else {
                        /// We must read old files also now, and send them to <see cref="OutsideLocalReceiver"/> (done by read storage file)
                        for (
                            var i = 0;
                            i < _storage.Count - 1;  // Note that last (current) file is read below
                            i++
                        ) {
                            ReadStorageFile(storageIndexToUse: i, doNotStore: true); // Do not bother with storing in-memory
                        }
                    }
                    /// Read the current / latest file into memory now (will also be sent to <see cref="OutsideLocalReceiver"/>)
                    ReadStorageFile(storageIndexToUse: _storage.Count - 1, doNotStore: false);

                    f = IP.GetPV<string>(StreamProcessorP.ClientUpdatePositionPath);
                    if (!System.IO.File.Exists(f)) {
                        IP.AddPV(StreamProcessorP.ClientUpdatePosition, ClientUpdatePosition.ClientDatastoreIsEmptyInstance);
                    } else {
                        IP.AddPV(StreamProcessorP.ClientUpdatePosition, ClientUpdatePosition.Parse(
                            System.IO.File.ReadAllText(f, UtilCore.DefaultAgoRapideEncoding)
                        ));
                    }
                }

                IP.SetPV(StreamProcessorP.CurrentlyInitializing, false);
                /// Note that this will also kick-start processing of log messages which have been accumulated by <see cref="ReceiveAndDistribute"/>"/>
                // TODO: Probably all logging will be 'lost' anyway until an outgoing Send connection have been made.
            });
            var totalMemoryAtEnd = GC.GetTotalMemory(forceFullCollection: true);
            IP.LogKeyValue(nameof(totalMemoryAtEnd), totalMemoryAtEnd.ToString());
            IP.LogKeyValue("StorageMemory", (totalMemoryAtEnd - totalMemoryAtStart).ToString());
            // NOTE: If we have a outgoing connections with Receive, we should actually wait 
            // NOTE: for it to complete also, before setting this warning flag to FALSE
            IP.SetPV(StreamProcessorP.TimestampIsOld, false);
            OutsideLocalReceiver?.Invoke(nameof(StreamProcessorP.TimestampIsOld) + " = " + false);
        }

        public void AddOrRemoveOutgoingConnections(List<ConnectionInstruction> connectionInstructions) =>
            AddOrRemoveOutgoingConnections(connectionInstructions.Select(i => new IKIP(IKString.FromString(i.IP.GetPV<string>(ConnectionInstructionP.ConnectionId)), i)));

        /// <summary>
        /// </summary>
        /// <param name="connectionInstructions">
        /// The vague typing is due to preparations for receiving this information from a <see cref="IP"/> which 
        /// reads the instructions from an ordinary property-stream.
        /// At regular intervals we could be fed this collection, and act accordingly.
        /// </param>
        [ClassMember(
            Description =
                "Not relevant for -" + nameof(ARNodeType.CoreDB) + "-.\r\n" +
                "\r\n" +
                "Note how this method supports live updating of connecting instructions over the -" + nameof(ARConcepts.PropertyStream) + "- " +
                "because it can be called at regular intervals.\r\n" +
                "(TODO: Live updating is not implemented from 'outside' as of Mar 2020)." +
                "\r\n" +
                "Should be called after -" + nameof(Initialize) + ".\r\n" +
                "\r\n" +
                "For each outgoing connection not found in list of connection instructions, add it to list. " +
                "For each active (outgoing) connection not on list of outgoing connections, close it.\r\n" +
                "\r\n" +
                "Note how the actual objects are shared with the 'outside' after the call, meaning that changes to for instance the subscripton, " +
                "could be immediately reflected on the 'inside' (here). In practical terms this is however a bit difficult to achieve so in practice. " +
                "A work-around could be (when you want to change some parameters for a specific connection), that you set the connection Invalid, " +
                "call this metod, do the changes, set it valid-again (by setting Invalid to default(DateTime), and call this method a final time.\r\n" +
                "TODO: Find some better way than description above."
        )]
        public void AddOrRemoveOutgoingConnections(IEnumerable<IKIP> connectionInstructions) {
            // Keep only those that pass verification
            var l = connectionInstructions.
                Select(ikip => (ikip.Key, ConnectionInstruction: ikip.P as ConnectionInstruction)). // We might have been sent something else also, filter out 
                Where(ikip => ikip.ConnectionInstruction != null).
                Select(ikip => (ikip.Key, ConnectionInstruction: ikip.ConnectionInstruction!)). // Get rid of null warnings by compiler
                Where(ikip => ikip.ConnectionInstruction.IP.TryAssertIntegrity(out _)).
                Where(ikip => ikip.ConnectionInstruction.IP.IsValid). // If not valid then we are supposed to not connect.
                ToList(); // ToList in order to avoid deferred execution
            l.ForEach(ikip => ikip.ConnectionInstruction.Initialize());

            /// Add any new connection to list of active connection
            var change = false;
            l.ForEach(ikip => _outgoingConnections.GetOrAdd(ikip.Key, ik => { // Note value factory, only create when needed
                change = true;
                /// Note how continuous loop in <see cref="StartTCPIPCommunication"/> will follow up / monitor this connection
                var retval = new ActualConnection {
                    // Note how we chain together loggers here, by just adding a context to call to the logger in the containing class.
                    Logger = Logger == null ? (Action<string>?)null : s => Logger?.Invoke(nameof(ActualConnection) + "/Outgoing/" + ik + "/" + s) // Note how we keep key as given (maybe from PRich over the property stream)
                };

                /// TODO: See comment for <see cref="ActualConnectionP.ConnectionInstruction"/>
                /// TODO: Clarify how this will actually look when sent over the property stream due to LogContext being set above.
                retval.IP.AddP(ActualConnectionP.ConnectionInstruction, ikip.ConnectionInstruction);
                // See use of SetMyUpdatePositionGenerator below instead
                // retval.IP.AddPV(ActualConnectionP.ClientUpdatePosition, IP.GetPVM<ClientUpdatePosition>());

                // TODO: Setting of Id here is not correct. If the point is to keep the Id as received over the propertystream, then a single IK is possible not enough...
                retval.IP.SetPV(ActualConnectionP.Id, "Outgoing_" + ik.ToString()); // Note how we keep key as given (maybe from PRich over the property stream?)

                retval.IP.SetPVM(ConnectionDirection.Outgoing);

                // And initialize further:
                retval.IP.SetPV(ActualConnectionP.UnderClosure, true); /// Signals continous loop in <see cref="StartTCPIPCommunication"/> to reinstate connection

                if (IP.GetPV(StreamProcessorP.DoNotStoreLocally, false)) {
                    retval.SetMyUpdatePositionGenerator(() => ClientUpdatePosition.OnlyNewDataIsRequestedInstance);
                } else {
                    //// TODO: Note how this assumes that if we have multiple outgoing connections, that is, that we
                    //// TODO: have multiple CoreDB's to choose from, they are all compatible regarding clientUpdateStatus.
                    //// TODO: This would probably not be the case with how clientUpdateStatus is built up today
                    //// TODO: (Apr 2020) with filename + index.
                    retval.SetMyUpdatePositionGenerator(() => IP.GetPVM<ClientUpdatePosition>());
                }

                retval.SetSinglePropertyStreamReceiver(s => ReceiveAndDistribute(new PropertyStreamLineWithOrigin(
                    line: s,
                    hasLocalOrigin: false,
                    sendLocalReceiver: false,
                    receivedFromOutgoing: true,
                    doNotAddTimestamp: false
                )));
                /// Note: This call is not relevant, as we do not need to know this on an incoming connection
                // retval.SetUpToDatePositionGenerator(TheUpToDatePosition);

                /// Leave this to continous loop in <see cref="StartTCPIPCommunication"/>
                // retval.InitializeOutgoingConnection();
                return retval;
            }));
            // Close outgoing connections no longer on list of outgoing connections
            _outgoingConnections.Where(e => !l.Any(ikip => ikip.Key.Equals(e.Key))).ToList().ForEach(a => { // ToList in order to be able to remove inside ForEach
                change = true;
                a.Value.IP.SetPV(ActualConnectionP.UnderClosure, true); // Note how this will be 'logged' to the property-stream
                                                                        // TODO: It is easy to add actual connections, and use PRichWithLogging and let them log activity.
                                                                        // TODO: What is more difficult is cleaning up the log / communicating to the log actual connections that are no longer
                                                                        // TODO: active (absence of a connection as in-parameter here is not recorded, so we do not know which connection 
                                                                        // TODO: that is already in the PropertyStream (in CoreDB) to mark as no-longer-relevant.
                                                                        // TODO: We could however log a recency-field for this purpose (maybe with an injecter marking them as Irrelevant or similar).

                _outgoingConnections.TryRemove(a.Key, out _);
            });
            if (change) IP.SetPV(StreamProcessorP.CountOutgoingConnections, _outgoingConnections.Count); // Note how this will be 'logged' to the property-stream
        }

        [ClassMember(Description = 
            "Starts communicating over TCP/IP.\r\n" +
            "\r\n" +
            "-" + nameof(Initialize) + "- must be called before this method is called."
        )]
        public void StartTCPIPCommunication() {
            if (!IP.TryGetPV<bool>(StreamProcessorP.CurrentlyInitializing, out _)) throw new StreamProcessorException(
                "No initialization status found.\r\n" +
                "Did you forget to call method " + nameof(Initialize) + " before calling method " + nameof(StartTCPIPCommunication) + "?\r\n" +
                "Explanation: The reason why the latter can not itself call the former is that you may want to " +
                "put a call to " + nameof(AddOrRemoveOutgoingConnections) + " in between.\r\n");

            if (IP.TryGetPV<int>(StreamProcessorP.IncomingConnectionsPortNo, out var p)) {
                // Start listening on this port. Add any new connection to list of active connection
                Listen(System.Net.IPAddress.Any, p);
            }

            // Start a new thread for continous management of outgoing connections (currently at 10 seconds intervals)

            System.Threading.Tasks.Task.Factory.StartNew(() => {
                try { // Note two levels of try catch, the outermost in order to safeguard thread itself against unhandled exception
                    System.Threading.Thread.CurrentThread.Name = nameof(StartTCPIPCommunication) + "_ContinousLoop";
                    System.Threading.Thread.CurrentThread.IsBackground = true; // TODO: In principle we should set this BEFORE thread is started
                    IP.LogExecuteTime(() => {
                        try {
                            var i = 0L;
                            var firstIteration = true;
                            while (!IP.GetPV<bool>(StreamProcessorP.UnderClosure)) {
                                // Loop indefinitely. 
                                // Keep outgoing connections open as requested.

                                // TODO: Add some cleanup of incoming connections?

                                _outgoingConnections.Where(e => e.Value.IP.GetPV<bool>(ActualConnectionP.UnderClosure)).ForEach(e => {
                                    // Note that when connection fails, we will try again some seconds later

                                    if (!firstIteration) {
                                        // Just in case UnderClosure was just set, and cleanup not completed
                                        System.Threading.Thread.Sleep(TimeSpan.FromSeconds(3));
                                    }

                                    e.Value.InitializeOutgoingConnection();
                                });

                                _outgoingConnections.Where(e => e.Value.Task != null).ForEach(e => {
                                    /// TODO: Do some monitoring here, according to Task.Status
                                    /// TODO: Maybe set ActualConnectionP.UnderClosure = TRUE so we can reinitialize the connection
                                    switch (e.Value.Task!.Status) {
                                        case System.Threading.Tasks.TaskStatus.RanToCompletion:
                                        case System.Threading.Tasks.TaskStatus.Faulted:
                                        case System.Threading.Tasks.TaskStatus.Canceled:
                                        case System.Threading.Tasks.TaskStatus.Created:
                                        case System.Threading.Tasks.TaskStatus.Running:
                                        case System.Threading.Tasks.TaskStatus.WaitingForActivation:
                                        case System.Threading.Tasks.TaskStatus.WaitingForChildrenToComplete:
                                        case System.Threading.Tasks.TaskStatus.WaitingToRun:
                                            break;
                                        default:
                                            throw new InvalidEnumException(e.Value.Task.Status);
                                    }
                                });
                                if ((i++ % 60) == 0) IP.SetPV(PConcurrentP.Heartbeat, UtilCore.DateTimeNow); // Heartbeat every tenth minute
                                System.Threading.Thread.Sleep(TimeSpan.FromSeconds(10));
                                firstIteration = false;
                            }
                        } catch (Exception ex) {
                            // TODO: Catch thread abort exception maybe? That is, when application terminates.
                            IP.HandleException(ex);
                        }
                    },
                        caller: System.Threading.Thread.CurrentThread.Name
                    );
                } catch (Exception ex) {
                    IP.HandleException(ex);
                }
            });
        }

        [ClassMember(
            Description = "Listens for incoming connections",
            LongDescription = "The short method name is a conscious decision, because it often shows up in logs"
        )]
        private void Listen(System.Net.IPAddress address, int port) =>
            new System.Threading.Thread(new System.Threading.ThreadStart(async () => {
                try {
                    System.Threading.Thread.CurrentThread.Name = nameof(Listen);
                    System.Threading.Thread.CurrentThread.IsBackground = true; // TODO: In principle we should set this BEFORE thread is started
                    _listener = new System.Net.Sockets.TcpListener(address, port);
                    // TODO: Consider giving collection of datapoints to log here (one call to log)
                    IP.LogText("Listener.Start");
                    IP.LogKeyValue("Address", address.ToString());
                    IP.LogKeyValue("Port", port.ToString());
                    _listener.Start();
                    IP.LogText("Listening");
                    var id = 0;
                    while (!IP.GetPV<bool>(StreamProcessorP.UnderClosure)) { // Note: What happens if no new connections from listener.AcceptTcpClientAsync? How will this thread exit?
                        IP.LogText("Waiting for connections...");
                        try {
                            var tcpClient = await _listener.AcceptTcpClientAsync();
                            var connection = new ActualConnection { Client = tcpClient };
                            connection.Client.NoDelay = true;
                            /// TODO: Find some better method for assigning id's. Preferably wait until we now the
                            /// TODO: <see cref="DataTransferDirection"/> for instance, so we can
                            /// TODO: better discern between different incoming connections from "same" client (same ARNode)
                            id++;
                            // TODO: Improve reading of IP-address.
                            var strId = "Incoming_" + id.ToString() + "_" + connection.Client?.Client.RemoteEndPoint.ToString().Split(":")[0];
                            IP.LogKeyValue(nameof(strId), strId);
                            connection.Logger = Logger == null ? (Action<string>?)null : s => Logger?.Invoke(nameof(ActualConnection) + "/Incoming/" + strId + "/" + s);
                            connection.IP.SetPV(ActualConnectionP.Id, strId);
                            connection.IP.SetPV(ActualConnectionP.ConnectionDirection, ConnectionDirection.Outgoing);
                            connection.IP.SetPV(ActualConnectionP.UnderClosure, false); // Initialize value
                            /// Note: This call is not relevant, as we do not need to know this on an incoming connection
                            // connection.SetMyUpdatePositionGenerator(() => IP.GetPVM<ClientUpdatePosition>());
                            connection.SetSinglePropertyStreamReceiver(s => ReceiveAndDistribute(new PropertyStreamLineWithOrigin(
                                line: s,
                                hasLocalOrigin: false,
                                sendLocalReceiver: false,
                                receivedFromOutgoing: false,
                                doNotAddTimestamp: false
                            )));
                            connection.SetUpToDatePositionGenerator(TheUpToDatePosition);
                            connection.IP.SetPV(PP.Created, UtilCore.DateTimeNow);
                            connection.Task = connection.Communicate();

                            // Note how AcceptIncomingConnections can only be called once for each instance of StreamProcessor.
                            // if not, duplicate id's will be created.
                            if (!_incomingConnections.TryAdd(strId, connection)) throw new StreamProcessorException("!connections.TryAdd(strId (" + strId + "), connection).\r\n");

                            // TODO: Delete commented out code
                            // NOT NECESSARY, LogContext was set above, before anything added to object
                            // Note: This is irrelevant because we set LogContext at once after initialization, meaning all properties have already been sent
                            // connection.ToPropertyStream(connection.LogContext?.context.ToList(), retval: null).ForEach(s => Receive(s)); // Communicate to outside that we have reveived a new incoming connection

                        } catch (Exception ex) {
                            if (_listener == null) { // Probably because CloseConnections was called
                                IP.LogText("listener == null, terminating loop");
                                break;
                            }
                            IP.LogKeyValue("Exception", ex.GetType().ToStringShort());
                            IP.LogText("When waiting for (or accepting) a connection. Will now continue to wait for _connections (after sleeping for a while)");
                            IP.HandleException(ex);
                            IP.LogExecuteTime(() => System.Threading.Thread.Sleep(30000), null, nameof(Listen) + "_Sleeping");  // Precautionary measure. Needed?
                        }
                    }
                    IP.LogText("Exiting"); // Note: Not possible to use IP.LogExecuteTime here because of async thread.
                } catch (Exception ex) {
                    IP.LogKeyValue("Exception", ex.GetType().ToStringShort());
                    IP.LogText("TODO: Fix this. Restart listening, because now no more incoming connections will be accepted.");
                    IP.HandleException(ex);
                }
            })).Start();

        public void CloseConnections() =>
            IP.LogExecuteTime(() => {
                if (_listener != null) { try { _listener.Stop(); _listener = null; } catch (Exception) { } }
                _incomingConnections.Values.ForEach(e => {
                    IP.LogKeyValue("CloseConnection", e.IP.GetPV<string>(ActualConnectionP.Id));
                    e.CloseConnection();
                });
                _outgoingConnections.Values.ForEach(e => {
                    IP.LogKeyValue("CloseConnection", e.IP.GetPV<string>(ActualConnectionP.Id));
                    e.CloseConnection();
                });
            });

        // TODO: Delete commented out code
        ///// <summary>
        ///// </summary>
        ///// <param name="context">Most probably something like 'Customer/42/FirstName' (three items in list)</param>
        ///// <param name="property">Most probably of type <see cref="PValue{T}"/>?</param>
        //[ClassMember(Description =
        //    "Distributes an in-application generated property to the stream.\r\n" +
        //    "Called from 'outside'.\r\n" +
        //    "\r\n" +
        //    "Note how will not call back to -" + nameof(OutsideLocalReceiver) + "- if that one is defined " +
        //    "because data is marked as -" + nameof(PropertyStreamLineWithOrigin.HasLocalOrigin) + "-.\r\n"
        //)]
        //public void SendFromLocalOrigin(List<IK> context, IP property) =>
        //    property.ToPropertyStream(context, retval: null).ForEach(s => // Most probably only one line (one string) will be generated        
        //        ReceiveAndDistribute(new PropertyStreamLineWithOrigin(
        //            s,
        //            hasLocalOrigin: true,
        //            sendLocalReceiver: false,
        //            receivedFromOutgoing: false,
        //            doNotAddTimestamp: false))
        //    );

        /// <summary>
        /// Note how default value = FALSE for <paramref name="sendLocalReceiver"/> means that data will not be stored in-memory locally
        /// </summary>
        /// <param name="singlePropertyStreamLine"></param>
        /// <param name="doNotAddTimestampWhenStoring">
        /// See <see cref="PropertyStreamLineWithOrigin.DoNotAddTimestamp"/>
        /// </param>
        /// <param name="sendLocalReceiver">
        /// <see cref="PropertyStreamLineWithOrigin.SendToLocalReceiver"/><br>
        /// Note how default value = FALSE means that data will not be stored in-memory locally
        /// </param>
        public void SendFromLocalOrigin(string singlePropertyStreamLine, bool sendLocalReceiver = false, bool doNotAddTimestampWhenStoring = false) =>
            ReceiveAndDistribute(new PropertyStreamLineWithOrigin(
                singlePropertyStreamLine,
                hasLocalOrigin: true,
                sendLocalReceiver: sendLocalReceiver,
                receivedFromOutgoing: false,
                doNotAddTimestamp: doNotAddTimestampWhenStoring));

        protected int _taskIsRunning = 0;

        // TODO: Enable calling of this, WITHOUT any parameter, just to update connection with more data
        // TODO: (when connection receives UpdateStatus PLUS Subscription, it should call this method in order to kick-start
        // TODO: updates towards it).
        /// <summary>
        /// For each incoming RECEIVE connection, distribute to any (incoming or outgoing) SEND connection according to its subscription.
        /// TODO: What about round-robing use / load-balancing and so on?
        /// Should we have a flag for this? Outgoing round-robin, just choose one randomly for each sending?
        /// If 3 connections, create random number between 1 and 3, choose connection.
        /// </summary>
        /// <param name="singlePropertyStreamLine">
        /// TODO: ALLOW THIS TO BE NULL! Signifying kick-start of connection update instead.
        /// </param>
        [ClassMember(
            Description =
                "Process all messages in receive queue.\r\n" +
                "1) Adds the given message to the receive queue and \r\n" +
                "2) Empties the receive queue asynchronously.\r\n" +
                "3) Send data to -" + nameof(OutsideLocalReceiver) + "- (if defined).\r\n" +
                "4) Feed connections asynchronously with new data (including connections catching up with others).\r\n" +
                "Emptying queue and feeding connections is done by ensuring that there is a thread running that continously processes messages " +
                "until the queue is empty / all connections are fed " +
                "(not how this thread exists as soon as the queue is empty)",
            LongDescription =
                "TODO: At initialization, All logging destined for the property stream done BEFORE an Outgoing Send connection has been established, " +
                "TODO: will most probably be lost." +
                "TODO: Implement some form of queue or similar, until connection have been established."
        )]
        public void ReceiveAndDistribute(PropertyStreamLineWithOrigin singlePropertyStreamLine) {
            if (singlePropertyStreamLine.DoNotAddTimestamp) {
                // Hack available at application startup
            } else {
                AddTimestampAsRequired();
            }
            _queueReceive.Enqueue(singlePropertyStreamLine);

            if (IP.TryGetPV<bool>(StreamProcessorP.CurrentlyInitializing, out var c) && c) {
                // Special case at initialization.
                // We can not do anything yet. 
                // The first call to us after initialization has completed will kick-start processing from queue
                // of the property stream line received now.
                return;
            }

            // TODO: Decide what to do when closing (when Dispose has been called). For instance storing all incoming properties to disk first?
            if (IP.GetPV<bool>(StreamProcessorP.UnderClosure)) return;
            try {
                var wasRunning = System.Threading.Interlocked.Exchange(ref _taskIsRunning, 1);
                if (wasRunning != 0) {
                    return;
                }
                System.Threading.Tasks.Task.Factory.StartNew(() => {
                    try {
                        System.Threading.Thread.CurrentThread.Name = nameof(ReceiveAndDistribute);
                        System.Threading.Thread.CurrentThread.IsBackground = true; // TODO: In principle we should set this BEFORE thread is started
                        do {
                            try {

                                Receive(); /// Note that will also distribute to <see cref="ConnectionDirection.Outgoing"/> connections with <see cref="DataTransferDirection.Send"/>

                                if (IP.GetPV(StreamProcessorP.DoNotStoreLocally, false)) {
                                    // TODO: There is nothing principally wrong with distributing data without a local data store.
                                    // TODO: Receive just has to put data into a in-memory collection, which we can keep fairly small,
                                    // TODO: and then Distribute can Distribute from that (ignoring clients ClientUpdateStatus and
                                    // TODO: instead keeping track of them itself.
                                } else {
                                    if (_queueReceive.Count == 0) { // Note that queue will "always" be empty now, because Receive will always finish queue
                                        Distribute(); // Note how Distribute yields control as soon as it sees another message in the queue
                                    }
                                }

                            } catch (Exception ex) {
                                // This is a problematic. 
                                IP.LogKeyValue("Exception", ex.GetType().ToStringShort());
                                IP.LogText("Closing connections. See separate log for exception.");
                                IP.HandleException(ex);
                                // TODO: This is not right course of action! Most probably we should only close maximum one connection now (the one for which the exception occurred)
                                CloseConnections();
                            } finally {
                                _taskIsRunning = 0;
                            }
                            // Don't remove this: This will really help in emptying the queue if two threads are working on the receive queue
                            // For example if one thread empties the queue and sets the taskIsRunning = 0, 
                            // while the other thread didn't start emptying the queue since the previous thread was running at that time
                            // This remaining code is used to avoid those situations, so that the queue are really empty always.
                            if (_queueReceive.Count == 0) { // TODO: THIS REALLY LOOKS MEANINGLESS???
                                break;
                            }
                            wasRunning = System.Threading.Interlocked.Exchange(ref _taskIsRunning, 1);
                            if (wasRunning != 0) {
                                break;
                            }
                        } while (true);
                    } catch (Exception ex) {
                        IP.LogKeyValue("Exception", ex.GetType().ToStringShort());
                        IP.LogText("Closing connections. See separate log for exception.");
                        IP.HandleException(ex);
                        // TODO: This is not right course of action! Most probably we should only close maximum one connection now (the one for which the exception occurred)
                        CloseConnections();
                    }

                });
            } catch (Exception ex) {
                IP.LogKeyValue("Exception", ex.GetType().ToStringShort());
                IP.LogText("Closing connections. See separate log for exception.");
                IP.HandleException(ex);
                // TODO: This is not right course of action! Most probably we should only close maximum one connection now (the one for which the exception occurred)
                CloseConnections();
            }
        }

        private DateTime _lastTimestamp = default;
        [ClassMember(Description =
            "Adds -" + nameof(StreamProcessorP.Timestamp) + "- to the property stream (to -" + nameof(_queueReceive) + "-) " +
            "as specified by -" + nameof(StreamProcessorP.TimestampResolution) + "-.\r\n" +
            "\r\n" +
            "Note how format reflects resolution, for instance if resolution is in whole minutes, then timestamp will be in format yyyy-MM-dd HH:mm"
        )]
        public void AddTimestampAsRequired() { // Note, public in order to be able to reference with nameof(xxx) in documentation
            if (!IP.TryGetPV<TimeSpan>(StreamProcessorP.TimestampResolution, out var r)) {
                return;
            }
            var t = UtilCore.DateTimeNow;
            if (t.Subtract(_lastTimestamp) < r) {
                return;
            }
            // Note that it is important to insert the timestamp BEFORE the actual value.
            // NOTE: If CoreDB has been offline, and clients have cached data for CoreDB, the timestamp for the (old) cached data will all be wrong.
            // TODO: Add support for syncronization between different nodes (who gets to create timestamps for instance?)
            /// <see cref="ARConcepts.CoreDBSynchronization"/>
            _queueReceive.Enqueue(new PropertyStreamLineWithOrigin(
                line: PropertyStreamLine.EncodeWholeLine(
                    keys: new List<IK> {
                        PK.FromEnum(StreamProcessorP.Timestamp)
                    },
                    value: new Func<string>(() => {
                        // Important detail, let the timestamp format reflect the resolution
                        if (r.Milliseconds == 0 && r.Seconds == 0) return t.ToString("yyyy-MM-dd HH:mm");
                        if (r.Milliseconds == 0) return t.ToString("yyyy-MM-dd HH:mm:ss");
                        return t.ToStringDateAndTime(); // Use full resolution (down to milliseconds)
                    })()
                ),
                /// This is actually lying (setting hasLocalOrigin to false), but it ensures that timestamp get sent to <see cref="OutsideLocalReceiver"/> 
                /// (although the combination of TimestampResolution and OutsideLocalReceiver is probably invalid anyway, because the former is for CoreDB and the latter for Client / ArmDB)
                hasLocalOrigin: false,
                sendLocalReceiver: false,
                receivedFromOutgoing: false,
                doNotAddTimestamp: false
            ));

            // _queueReceive.Enqueue(new SinglePropertyStreamLine("TimestampDebugInfoToBeRemoved/" + t.ToStringDateAndTime(), hasLocalOrigin: false, receivedFromOutgoingConnection: false));

            // Important detail, ensture that next timestamps is generated at "exact" time according
            // to resolution. 
            /// For instance, with <see cref="StreamProcessorP.TimestampResolution"/> 1 minutes, we want to avoid the situation of generating 
            //   2020-04-26 11:25 at time 11:25:59 and
            //   2020-04-26 11:26 at time 11:26:59 and so on
            // because that would be very inaccurate.
            if (r.Milliseconds == 0 && r.Seconds == 0) {
                _lastTimestamp = new DateTime(t.Year, t.Month, t.Day, t.Hour, t.Minute, 0);
            } else if (r.Milliseconds == 0) {
                _lastTimestamp = new DateTime(t.Year, t.Month, t.Day, t.Hour, t.Minute, t.Second);
            } else {
                _lastTimestamp = t;
            }
        }


        [ClassMember(Description =
            "Contains data to be written to disk.\r\n\" +" +
            "Only relevant when -" + nameof(StreamProcessorP.CacheDiskWrites) + "- is TRUE")]
        private readonly ConcurrentQueue<(ClientUpdatePosition? ClientUpdatePosition, string Filepath, string Data)> _diskStorageQueue =
            new ConcurrentQueue<(ClientUpdatePosition? ClientUpdatePosition, string Filepath, string Data)>();

        private static int _diskStorageThreadIsRunning = 0;
        [ClassMember(Description =
            "Handle all incoming messages.\r\n" +
            "1) Write to Console\r\n" +
            "2) Save to disk and RAM (List<string>)\r\n" +
            "3) Inform -" + nameof(OutsideLocalReceiver) + "-.\r\n" +
            "\r\n" +
            "Note that will also distribute to -" + nameof(ConnectionDirection.Outgoing) + "- connections with -" + nameof(DataTransferDirection.Send) + "-.\r\n" +
            "(rest of distribution is done by -" + nameof(Distribute) + "-.)\r\n" +
            "\r\n" +
            "Called by -" + nameof(ReceiveAndDistribute) + "- in a thread-safe manner."
        )]
        private void Receive() {
            // TODO: Decide what to do when closing (when Dispose has been called). For instance storing all incoming properties to disk first?
            if (IP.GetPV<bool>(StreamProcessorP.UnderClosure)) return; // TODO: Throw exception now?
            while (_queueReceive.TryPeek(out var m)) {
                // TODO: Decide what to do when closing (when Dispose has been called). For instance storing all incoming properties to disk first?
                if (IP.GetPV<bool>(StreamProcessorP.UnderClosure)) break; // TODO: Throw exception now?
                try {
                    if (m.HasLocalOrigin) {
                        IP.Inc(StreamProcessorP.CountReceiveLocalOrigin);
                    } else {
                        // This will also include timestamps, even if they are generated because of something received with local origin
                        IP.Inc(StreamProcessorP.CountReceiveNonLocalOrigin);
                    }

                    if (
                        !m.HasLocalOrigin &&
                        IP.GetPV(StreamProcessorP.WriteToConsole, defaultValue: false) &&
                        !IP.GetPV(StreamProcessorP.CurrentlyInitializing, defaultValue: true)
                    ) {
                        var pos = m.PropertyStreamLine.IndexOf("::"); /// Skip any <see cref="ClientUpdatePosition"/> included in line.
                        // TODO: :: may be part of data "later" in line. Fix parsing!
                        Console.WriteLine(pos == -1 ? m.PropertyStreamLine : m.PropertyStreamLine[(pos + 2)..]);
                    }

                    if (m.HasLocalOrigin && !m.SendToLocalReceiver) {
                        // Do not send back again to "local". Local generator can decide for itself instead.
                    } else {
                        OutsideLocalReceiver?.Invoke(m.PropertyStreamLine);
                        IP.Inc(StreamProcessorP.CountOutsideLocalReceiver);
                    }

                    if (m.ReceivedFromOutgoing) {
                        // Do not send back again to our own outgoing connection
                        // TODO: Add BECAUSE here
                    } else {
                        _outgoingConnections.ForEach(e => {
                            if (e.Value.IP.GetPVM<ConnectionInstruction>().IP.GetPVM<DataTransferDirection>() == DataTransferDirection.Send) {
                                /// This is supposed to receive everything that we get
                                /// Typically a <see cref="ARNodeType.Client"/> node sending to an <see cref="ARNodeType.ArmDB"/> / <see cref="ARNodeType.CoreDB"/> node 
                                /// or an <see cref="ARNodeType.ArmDB"/> node sending to an <see cref="ARNodeType.CoreDB"/> node.
                                /// 
                                /// Note that in this case sending any <see cref="ClientUpdatePosition"/> is not relevant as everything is sent
                                /// as soon as possible (also not how the 'connection itself' is supposed to cache in a local storage any data
                                /// not sent immedidately).
                                e.Value.Send(m.PropertyStreamLine);
                            }
                        });
                    }

                    if (IP.GetPV(StreamProcessorP.DoNotStoreLocally, false)) {
                        // Do not store locally at all
                    } else if (
                        m.HasLocalOrigin &&

                        // TODO: Delete commented out code
                        // (change Jul 2020, using StreamProcessorP.DoNotStoreLocalOriginLocally instead)
                        //_outgoingConnections.Count > 0)
                        ///// TODO: Change check for "_outgoingConnections.Count > 0" to outgoing connections with
                        ///// <see cref="DataTransferDirection.Sende"/>?

                        IP.GetPV(StreamProcessorP.DoNotStoreLocalOriginLocally, false)
                    ) {
                        // Do not store locally
                    } else {
                        // Note scenario with no outgoing connections above (_outgoingConnections.Count == 0), signifying that this application is a 
                        // total standalone client, storing everything locally

                        /// NOTE: Potential bug. When implementing <see cref="StreamProcessorP.CopyFileStructureFromCore"/>
                        /// NOTE: ensure that absolutely NOTHING is stored locally, especially between 
                        /// NOTE: call to <see cref="Initialize"/> and <see cref="StartTCPIPCommunication"/>
                        /// NOTE: (that is, before call <see cref="AddOrRemoveOutgoingConnections"/>)

                        ClientUpdatePosition? clientUpdatePosition = null;

                        if (m.ReceivedFromOutgoing) {
                            /// Note that we are not able to check for <see cref="DataTransferDirection.Receive"/> but 
                            /// if it was <see cref="DataTransferDirection.Send"/> we will not receive any data? 
                            /// So this should be safe? Famous last words...
                            var pos = m.PropertyStreamLine.IndexOf("::");
                            if (pos == -1) {
                                // This looks like an exception.
                                // TODO: Throw exception?
                            } else if (!ClientUpdatePosition.TryParse(m.PropertyStreamLine[..pos], out clientUpdatePosition)) {
                                // This looks like an exception.
                                // TODO: Throw exception?
                            } else {
                                // Remove update position part before further processing
                                m = new PropertyStreamLineWithOrigin(
                                    line: m.PropertyStreamLine[(pos + 2)..],
                                    hasLocalOrigin: m.HasLocalOrigin,
                                    sendLocalReceiver: m.SendToLocalReceiver,
                                    receivedFromOutgoing: m.ReceivedFromOutgoing,
                                    doNotAddTimestamp: m.DoNotAddTimestamp
                                );
                            }
                        }

                        var store = new Action<ClientUpdatePosition?, string, string>((c, filename, data) => {
                            // Either called directly, or called for greater chunks of data
                            if (c != null) {
                                IP.SetPV(StreamProcessorP.ClientUpdatePosition, c);
                                /// Store to disk, will be read next time by <see cref="Initialize"/>
                                System.IO.File.WriteAllText(
                                    IP.GetPV<string>(StreamProcessorP.ClientUpdatePositionPath),
                                    c.ToString(),
                                    UtilCore.DefaultAgoRapideEncoding);
                            }
                            System.IO.File.AppendAllText(filename, data, UtilCore.DefaultAgoRapideEncoding);
                        });

                        var i = _storage.Count - 1;
                        if (i == -1) throw new StreamProcessorException(
                            "Storage is empty, but at least one file should have been created by -" + nameof(Initialize) + "- and read into memory.\r\n" +
                            "Possible cause: Incorrect use of " + nameof(StreamProcessorP.CurrentlyInitializing) + " " +
                            "(for instance, setting properties before calling -" + nameof(Initialize) + "-).\r\n"
                        );

                        // TODO: It is inefficient to lookup revision in dictionary each time (see [0] below)
                        if (_storage[i].Length > IP.GetPV<long>(StreamProcessorP.IndividualStorageFileSize)) {
                            // Create new file. 
                            // Note how it is important to perform this check at once, before any attempt at storing below,
                            // in order for new file to be created for instance at application startup if needed  
                            // (makes it easier to "debug" use of storage file)
                            _storage.Add(new StorageFile(CreateNewStorageFile(), 0));
                            i = _storage.Count - 1;
                            _storage[i].Properties[0] = new List<string>();
                        }

                        if (!IP.GetPV(StreamProcessorP.CacheDiskWrites, false)) {
                            // Store directly
                            store(clientUpdatePosition, _storage[i].Filepath, m.PropertyStreamLine + "\r\n");
                        } else {
                            // Wait for bigger chunks of data (store with 1 second intervals)
                            // Take into consideration change of files within queue
                            _diskStorageQueue.Enqueue((clientUpdatePosition, _storage[i].Filepath, m.PropertyStreamLine));
                            var wasRunning = System.Threading.Interlocked.Exchange(ref _diskStorageThreadIsRunning, 1);
                            if (wasRunning == 0) {
                                System.Threading.Tasks.Task.Factory.StartNew(() => {
                                    // Start separate thread for writing to disk (the idea is to reduce dramatically the number of disk I/O-operations needed)
                                    // Note: This thread will work continously for the lifetime of the application
                                    System.Threading.Thread.CurrentThread.Name = nameof(Receive) + "_DiskStorage";
                                    System.Threading.Thread.CurrentThread.IsBackground = true; // TODO: In principle we should set this BEFORE thread is started
                                    try {
                                        while (true) {
                                            // Empty queue continuously, use a separate StringBuilder for each file (the queue could potentially span multiple files)
                                            // This code is a bit hastily thrown together. We could most probably increase performance here
                                            // (building allData is a bit expensive)

                                            // allData contains data to be written for each separate file (usually there will be only one file)
                                            var allData = new Dictionary<string, StringBuilder>();
                                            ClientUpdatePosition? newestClientUpdatePosition = null;
                                            while (_diskStorageQueue.TryDequeue(out var dataTuple)) {
                                                if (!allData.TryGetValue(dataTuple.Filepath, out var dataThisFile)) {
                                                    allData.Add(
                                                        dataTuple.Filepath,
                                                        dataThisFile = new StringBuilder()
                                                    );
                                                }
                                                dataThisFile.Append(dataTuple.Data + "\r\n");
                                                if (dataTuple.ClientUpdatePosition != null) {
                                                    // Note that hypotetically, if not all single property stream lines received contain a valid
                                                    // client update position, the one we find now may not be related to the latest line found in the queue.
                                                    newestClientUpdatePosition = dataTuple.ClientUpdatePosition;
                                                }
                                            }
                                            // Do the actual writing to disk
                                            allData.ForEach(e => store(newestClientUpdatePosition, e.Key, e.Value.ToString()));
                                            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));
                                        }
                                    } catch (Exception ex) {
                                        /// This is quite a helpless situation. If we are connected to a <see cref="ARNodeType.CoreDB"/> it might be possible
                                        /// to signal the problem over the property-stream
                                        IP.HandleException(ex, nameof(Receive)); // Note that HandleException will also write to the console about the problem
                                        IP.LogText("Sleeping for 60 seconds in order not to overwhelm system with exception info in case problem repeats itself");
                                        IP.LogExecuteTime(() => System.Threading.Thread.Sleep(TimeSpan.FromSeconds(60)), null, nameof(Receive) + "_Sleeping");
                                        // throw new StreamProcessorException("Exception", ex);
                                    } finally {
                                        _diskStorageThreadIsRunning = 0;
                                    }
                                });
                            }
                        }

                        /// Note how in-memory storage is updated immediately, regardless of setting for <see cref="StreamProcessorP.CacheDiskWrites"/>

                        // TODO: It is inefficient to lookup revision in dictionary each time (see [0] below)
                        var list = _storage[i].Properties[0] /// [0] = revision 0. TODO: Implement <see cref="ARConcepts.DataRetention"/>
                            ?? throw new NullReferenceException("List within storage index " + i + ". Should have been set in connection with call to -" + nameof(CreateNewStorageFile) + "- / -" + nameof(Initialize) + "-.\r\n");
                        // TODO: Eliminate need for null-reference check like this (as of Apr 2020 occurs two places in code. Make smarter initialization of StorageFile

                        // TODO: Implement discarding of data when not listening to incoming connections (saves memory usage).
                        // if (IP.TryGetPV<int>(StreamProcessorP.IncomingConnectionsPortNo, out _)) for instance.
                        // TODO: (search for this TODO in code to see where relevant to make changes).
                        list.Add(m.PropertyStreamLine);

                        _storage[i].Length += m.PropertyStreamLine.Length;
                    }
                    _queueReceive.TryDequeue(out m);
                } catch (Exception ex) {
                    IP.LogKeyValue("Exception", ex.GetType().ToStringShort());
                    IP.LogText("When receiving message. Closing connections. See separate log for exception.");
                    IP.HandleException(ex);
                    // TODO: This is not right course of action!                                      
                    CloseConnections();
                    break;
                }
            }
        }

        private ClientUpdatePosition TheUpToDatePosition() => new ClientUpdatePosition(
            _storage[^1].Filename,
            _storage[^1].Properties[0]?. /// [0] = revision 0. TODO: Implement <see cref="ARConcepts.DataRetention"/>
                Count - 1 ?? throw new NullReferenceException("List within storage index " + (_storage.Count - 1) + ". Should have been set in connection with call to - " + nameof(CreateNewStorageFile) + " - / -" + nameof(Initialize) + " -.\r\n"),
            // TODO: Eliminate need for null-reference check like this (as of Apr 2020 occurs two places in code. Make smarter initialization of StorageFile
            isAtEndOfStream: true
        );

        [ClassMember(Description =
            "Feed incoming connections (with -" + nameof(DataTransferDirection.Send) + "-) with data.\r\n" +
            "\r\n" +
            "Note: If queue fills up (count > 0) while feeding connections, will terminate in order to let " +
            "-" + nameof(Receive) + "- being called instead.\r\n" +
            "\r\n" +
            "Called by -" + nameof(ReceiveAndDistribute) + "- in a thread-safe manner")]
        private void Distribute() {

            var theUpToDatePosition = TheUpToDatePosition();

            while (true) {
                var changes = false;

                /// NOTE: <see cref="ConnectionDirection.Outgoing"/> with <see cref="DataTransferDirection.Send"/> are being served by <see cref="Receive"/>

                foreach (var c in _incomingConnections.Values) {
                    if (c.IP.GetPV<bool>(ActualConnectionP.UnderClosure)) {
                        continue;
                    }

                    if (c.IP.GetPV(ActualConnectionP.IsErrorCondition, false)) {
                        continue;
                    }

                    /// This is typically relevant when we are a <see cref="ARNodeType.CoreDB"/>
                    /// and have received a property from a <see cref="ARNodeType.Client"/> that has to be sent to other <see cref="ARNodeType.Client"/>'s
                    /// (and probably back to that same client also, from which this property originated)

                    if (!c.IP.TryGetPVM(out ClientUpdatePosition clientUpdatePosition)) {
                        /// No update status received yet from client (or it may never be received because connection is not for requesting updates)
                        /// On client side this is probably a <see cref="DataTransferDirection.Send"/> connection, it expects to
                        /// send us data, not receive it.
                        /// (note that on "our side" we have no concept of <see cref="DataTransferDirection"/> for incoming connections)
                        continue;
                    }
                    if (clientUpdatePosition.Equals(theUpToDatePosition)) {
                        /// This connection is already up to date
                        /// This is actually quite strange, because <see cref="Distribute"/> was called due to new incoming messages being received
                        /// (most probably it is <see cref="ClientUpdatePosition.OnlyNewDataIsRequestedInstance"/>)
                        continue;
                    }
                    if (!c.IP.TryGetPV(ActualConnectionP.SubscriptionAsRequestedByClient, out List<Subscription> subscription)) {
                        // No subscription received yet. We are expecting it soon since client has sent us its update status.
                        continue;
                    }

                    // We are going to feed connection some data
                    // Ensure that we get called back again soon, if we do not complete feeding of connection this time
                    changes = true;

                    if (clientUpdatePosition.ClientDatastoreIsEmpty) {
                        clientUpdatePosition = new ClientUpdatePosition(_storage[0].Filename, index: -1, isAtEndOfStream: false);
                    } else if (clientUpdatePosition.OnlyNewDataIsRequested) {
                        throw new ClientUpdatePosition.InvalidClientUpdateStatusException(nameof(ClientUpdatePosition.OnlyNewDataIsRequestedInstance) + ": Should have been changed by call to -" + nameof(TheUpToDatePosition) + "- from -" + nameof(ActualConnection) + "- as soon as it was received.");
                        // It might be too late if we had tried to set it now (because new data may have been received in the meantime)
                        // clientUpdatePosition = new ClientUpdatePosition(_storage[i].Filename, _storage[i].Properties[0].Count - 1);
                    } else if (clientUpdatePosition.IsGoBack) {
                        throw new NotImplementedException(nameof(ClientUpdatePosition.IsGoBack));
                    }

                    var storageIndexToUse = new Func<int>(() => {
                        if (clientUpdatePosition.Filename.Equals(theUpToDatePosition.Filename)) {
                            // The most common situation, clients will be on the last / current file
                            return _storage.Count - 1;
                        }
                        for (var i = 0; i < _storage.Count; i++) {
                            if (_storage[i].Filename == clientUpdatePosition.Filename) {
                                return i;
                            }
                        }

                        c.LogAndSendErrorConditionData(
                            logObject: this,
                            context: new List<string> {
                                "Invalid" + nameof(ClientUpdatePosition),
                                "FilenameNotFound"
                            },
                            errorData: new List<(string, string)> { /// Adhere to AgoRapide <see cref="ARConcepts.Logging"/>-principle of logging data-points, not loose text
                                ("Id", c.IP.GetPV<string>(ActualConnectionP.Id)),
                                ("Filename", clientUpdatePosition.Filename),
                                ("Explanation", "Filename in " + nameof(ClientUpdatePosition) + " as given by client was not found within server storage."),
                                ("Resolution", "Try deleting all local data (at client side) and restart the client application")
                            }
                        );

                        // Signal to our self (below) that we are giving up on this connection
                        return -1;
                    })();

                    if (storageIndexToUse == -1) {
                        // We are giving up on this connection
                        break;
                    }

                    var storage = _storage[storageIndexToUse].Properties[0]; /// [0] = revision 0. TODO: Implement <see cref="ARConcepts.DataRetention"/>
                    if (storage == null) ReadStorageFile(storageIndexToUse, doNotStore: false);
                    storage = _storage[storageIndexToUse].Properties[0] ?? throw new NullReferenceException(nameof(_storage) + ". Should have been set after call to -" + nameof(ReadStorageFile) + "-");

                    var theUpToDatePositionForThisStorageFile = new Func<ClientUpdatePosition>(() => {
                        if (storageIndexToUse == _storage.Count - 1) {
                            return theUpToDatePosition;
                        } else {
                            return new ClientUpdatePosition(_storage[storageIndexToUse].Filename, index: storage.Count - 1, isAtEndOfStream: false);
                        }
                    })();

                    if (clientUpdatePosition.Index > theUpToDatePositionForThisStorageFile.Index) {
                        // NOTE: theUpToDateStatus.Index may be -1 for a totally empty data-store?
                        // NOTE: (on the other hand, we do not get called unless there is some new property for us, so will no longer be -1???)

                        // The connection should definitely not be ahead of us
                        c.LogAndSendErrorConditionData(
                            logObject: this,
                            context: new List<string> {
                                "Invalid" + nameof(ClientUpdatePosition),
                                "InvalidIndex"
                            },
                            errorData: new List<(string, string)> { /// Adhere to AgoRapide <see cref="ARConcepts.Logging"/>-principle of logging data-points, not loose text
                                ("Id", c.IP.GetPV<string>(ActualConnectionP.Id)),
                                (nameof(clientUpdatePosition) + ".Index", clientUpdatePosition.Index.ToString()),
                                (nameof(theUpToDatePositionForThisStorageFile) + ".Index", theUpToDatePositionForThisStorageFile.Index.ToString()),
                                ("Explanation", "Client index is higher than our index"),
                                ("Resolution", "Try deleting all local data (at client side) and restart the client application")
                            }
                        );
                    }

                    var index = clientUpdatePosition.Index;

                    var sizeSent = 0;
                    while (index < theUpToDatePositionForThisStorageFile.Index) {
                        if (c.QueueSendCount > 100) {
                            // No point in filling up queue
                            break;
                        }
                        if (_queueReceive.Count > 0) {
                            // See comment below (prioritize incoming messages)
                            break;
                        }
                        if (IP.GetPV<bool>(StreamProcessorP.UnderClosure)) {
                            break;
                        }
                        index++;

                        var newClientUpdatePosition = new ClientUpdatePosition(theUpToDatePositionForThisStorageFile.Filename, index, isAtEndOfStream: false);
                        if (newClientUpdatePosition.Equals(theUpToDatePosition)) {
                            newClientUpdatePosition = theUpToDatePosition; /// Important in order to end any <see cref="ARConcepts.AdHocQuery"/>
                        }

                        if (!Subscription.IsMatch(subscription, storage[index])) {
                            // This specific line is not to be sent (it is not requested by client)

                            // Update position at once, so we do not repeat calculation / subscription match from the same position
                            c.IP.SetPV(ActualConnectionP.ClientUpdatePosition, newClientUpdatePosition);
                        } else {
                            // Inform client about its new update status
                            c.Send(newClientUpdatePosition + "::" + storage[index]);

                            // Store new client update status at once, ensuring integrity
                            c.IP.SetPV(ActualConnectionP.ClientUpdatePosition, newClientUpdatePosition);
                            // (note that if connection is not able to actually send the data to the client, we expect the connection
                            // to be terminated, and the process started all over again, with the client sending its update status again).

                            sizeSent += storage[index].Length;
                            if (sizeSent > 20000) { // Approximately 20 KB already sent
                                                    // Send to other connections too
                                break;
                            }
                        }
                    }

                    if (clientUpdatePosition.Index == theUpToDatePositionForThisStorageFile.Index) {
                        // We managed to send all remaining data for this storage file to this client in one go
                        // Change to next storage file
                        if (storageIndexToUse == _storage.Count - 1) {
                            // Client is completely updated (with latest / current storage file)
                        } else {
                            // Continue sending in next round (simplifying code), will happen "instantenously" anyway
                            c.IP.SetPVM(new ClientUpdatePosition(_storage[storageIndexToUse + 1].Filename, index - 1, isAtEndOfStream: false)); ;
                            // Note: There is no need in informing client
                        }
                    }

                    if (_queueReceive.Count > 0) {
                        // See comment below (prioritize incoming messages)
                        break;
                    }
                    if (IP.GetPV<bool>(StreamProcessorP.UnderClosure)) {
                        break;
                    }
                } // for each connection

                if (!changes) {
                    // No connection had to be updated (had to be fed). We can exit thread for now
                    break;
                }
                if (_queueReceive.Count > 0) {
                    // Prioritize incoming messages
                    //
                    // More incoming messages has arrived while we where feeding connections. 
                    // Stop feeding, store these first before continuing feeding connections
                    // (in other words, we prioritize storing of data over sharing out data)
                    // NOTE: Regarding ACID-principles, when a client is doing a READ after a WRITE, it will not necessarily get back the written value.
                    // NOTE: because it has not propagated back to the clients data storage yet.
                    break;
                }
                if (IP.GetPV<bool>(StreamProcessorP.UnderClosure)) {
                    break;
                }

                // Continue for another round
            } // while true
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="storageIndexToUse"></param>
        /// <param name="doNotStore">
        /// Hint that file is only needed for sending to <see cref="OutsideLocalReceiver"/>
        /// </param>
        [ClassMember(Description =
            "Reads one storage file from disk\r\n" +
            "\r\n" +
            "Sends to -" + nameof(OutsideLocalReceiver) + "- if -" + nameof(StreamProcessorP.CurrentlyInitializing) + "-.\r\n" +
            "\r\n" +
            "TODO: Clean up code here and explain better"
        )]
        public void ReadStorageFile(int storageIndexToUse, bool doNotStore) {
            if (storageIndexToUse == _storage.Count - 1) {
                if (!IP.GetPV<bool>(StreamProcessorP.CurrentlyInitializing)) {
                    throw new StreamProcessorException(
                        nameof(StreamProcessorP.CurrentlyInitializing) + " = TRUE and " +
                        nameof(storageIndexToUse) + " = " + storageIndexToUse + " (which is the last / current index).\r\n" +
                        "It is illegal to read last / current storage file after initialization is completed.\r\n" +
                        "This is a guard in case -" + nameof(StreamProcessorP.CacheDiskWrites) + "- is used " +
                        "(" + IP.GetPV<bool>(StreamProcessorP.CacheDiskWrites) + " in this case) " +
                        "because then writing may be in process at the same time that we are reading the file.\r\n"
                  );
                }
            }
            if (storageIndexToUse == _storage.Count - 2) {
                // Note that this is also a dangerous situation if this application instance made the change from next-to-last to last index,
                // and discarded the (now) next-to-last data immedidately, forcing a theoreticaly immediately read now because of a client request,
                // and with a cached write still in progress (that is, before the next-to-last file was completely written to disk)
                // As of Apr 2020 it is assumed that next-to-last file is not discarded immediately.
            }

            List<string>? fileContent = null;
            if (doNotStore) {
                /// We are only going to send to <see cref="OutsideLocalReceiver"/>
                // Do not read everything at once, stream file instead.
            } else {
                var totalMemoryAtStart = GC.GetTotalMemory(forceFullCollection: true);
                fileContent = IP.LogExecuteTime(
                    () => System.IO.File.ReadAllLines(_storage[storageIndexToUse].Filepath, UtilCore.DefaultAgoRapideEncoding).ToList(),
                    new List<(string, string)> {
                    ("Index", storageIndexToUse.ToString()),
                    ("Filename", _storage[storageIndexToUse].Filename)
                    }
                );
                _storage[storageIndexToUse].Properties[0] =  /// [0] = revision 0. TODO: Implement <see cref="ARConcepts.DataRetention"/>
                    // TODO: Implement discarding of these data after we can safely assume that clients have been updated.
                    // "TODO: (search for this TODO in code to see where relevant to make changes).\r\n" +
                    fileContent;

                var totalMemoryAtEnd = GC.GetTotalMemory(forceFullCollection: true);
                IP.LogKeyValue("MemoryChangeReadingFile", (totalMemoryAtEnd - totalMemoryAtStart).ToString());
            }

            // TODO: Implement discarding of data when not listening to incoming connections (saves memory usage).\r\n" +
            // TODO: (search for this TODO in code to see where relevant to make changes).\r\n" +

            if (IP.GetPV<bool>(StreamProcessorP.CurrentlyInitializing) && OutsideLocalReceiver != null) {
                var totalMemoryAtStart = GC.GetTotalMemory(forceFullCollection: true);
                if (fileContent != null) {
                    // File has already been read for us
                    fileContent.ForEach(s => {
                        OutsideLocalReceiver(s);
                        // Do not increase this count now. It would be misleading.
                        // IP.Inc(StreamProcessorP.CountPropertyStreamLinesReceived);
                    });
                } else {
                    // Read file, but only as stream in order to conserve memory
                    using var streamReader = new System.IO.StreamReader(_storage[storageIndexToUse].Filepath, UtilCore.DefaultAgoRapideEncoding);
                    while (!streamReader.EndOfStream) {
                        OutsideLocalReceiver(streamReader.ReadLine());
                    }
                    streamReader.Close();
                }
                var totalMemoryAtEnd = GC.GetTotalMemory(forceFullCollection: true);
                IP.LogKeyValue("MemoryChangeOutsideLocalReceiver", (totalMemoryAtEnd - totalMemoryAtStart).ToString());
            }
        }

        public void Dispose() {
            IP.SetPV(StreamProcessorP.UnderClosure, true);
            CloseConnections();
        }

        public class StorageFile {

            [ClassMember(Description =
                "Complete path to file"
            )]
            public string Filepath { get; private set; }

            [ClassMember(Description =
                "Filename: Main file name, like 2020-04-03 08:42\r\n"
            )]
            public string Filename { get; private set; }

            [ClassMember(Description =
                "Wrongly placed. Belongs to each individual revision file.\r\n" +
                "TODO: Implement -" + nameof(ARConcepts.DataRetention) + "-."
            )]
            public long Length { get; set; }

            [ClassMember(Description =
                "Key = revision number of file (due to compression / -" + nameof(ARConcepts.DataRetention) + "-), like R00001, R00002 and so on.\r\n" +
                "Value (inner dictionary) = actual data (-" + nameof(PropertyStreamLine) + "-).\r\n" +
                "Note: Value may be null, signifying that file has not been read into memory yet"
            )]
            public Dictionary<int, List<string>?> Properties { get; private set; }
            public StorageFile(string filepath, long size) {
                Filepath = filepath;
                Filename = System.IO.Path.GetFileName(filepath);
                Length = size;
                Properties = new Dictionary<int, List<string>?> {
                    {
                        0, /// [0] = revision 0. TODO: Implement <see cref="ARConcepts.DataRetention"/> 
                        null
                    }
                };
            }
        }

        public class StreamProcessorException : ApplicationException {
            public StreamProcessorException(string message) : base(message) { }
            public StreamProcessorException(string message, Exception inner) : base(message, inner) { }
        }
    }

    [Class(
        Description =
            "The rationale for this class is that -" + nameof(StreamProcessor.ReceiveAndDistribute) + "- needs to distingush origin of data when routing it " +
            "(in general, in order to avoid sending it back to its source).\r\n" +
            "\r\n" +
            "This class is not to be confused with -" + nameof(PropertyStreamLine) + "-."
    )]
    public class PropertyStreamLineWithOrigin {
        public string PropertyStreamLine { get; private set; }

        [ClassMember(Description =
            "Locally generated data is not to be sent to -" + nameof(StreamProcessor.OutsideLocalReceiver) + "-). " +
            "We instead leave it to the 'outside' to decide if\r\n" +
            "\r\n" +
            "either\r\n" +
            "\r\n" +
            "A) Locally originated data should be put straight into the local 'outside' property store (typically an -" + nameof(PRich) + "-)\r\n" +
            "NOTE: This alternative, A), is dangerous because timestamps as generated by -" + nameof(StreamProcessor.AddTimestampAsRequired) + "-\r\n" +
            "NOTE: will not be sent to -" + nameof(StreamProcessor.OutsideLocalReceiver) + "- in a deterministic manner compared to\r\n" +
            "NOTE: when the data is pute into the 'outside' property store.\r\n" +
            "NOTE: (in simpler words, the timestamp may come AFTER the data, leading to errors in parsing).\r\n" +
            "NOTE: Use -" + nameof(SendToLocalReceiver) + "- in order to escape this problem.\r\n" +
            "\r\n" +
            "or\r\n" +
            "\r\n" +
            "B) We should wait until it gets returned back from the -" + nameof(ARNodeType.CoreDB) + "- through our subscription " +
            "(and being sent to the local property store through -" + nameof(StreamProcessor.OutsideLocalReceiver) + "-).\r\n" +
            "\r\n" +
            "See also -" + nameof(SendToLocalReceiver) + "- which can overrule this.\r\n"
        )]
        public bool HasLocalOrigin { get; private set; }

        [ClassMember(Description =
            "Means 'send to local receiver regardless of origin'.\r\n" +
            "\r\n" +
            "See -" + nameof(HasLocalOrigin) + "- for explanation."
        )]
        public bool SendToLocalReceiver { get; private set; }

        [ClassMember(Description =
            "Normally -" + nameof(StreamProcessor) + "- inserts the required -" + nameof(StreamProcessorP.Timestamp) + "-s in the AgoRapide storage.\r\n" +
            "\r\n" +
            "-" + nameof(DoNotAddTimestamp) + "- is a hack used when synchronizing an AgoRapide storage with other databases.\r\n" +
            "Normally the synchronization is supposed to always be up-to-date, but at startup of the AgoRapide application " +
            "some gap will naturally exist since the application was last shut down.\r\n" +
            "The initial synchronization at application startup can therefore utilize timestamp information from the " +
            "source database, and signal through this property (" + nameof(DoNotAddTimestamp) + ") " +
            "that -" + nameof(StreamProcessor) + " should NOT also insert timestamps itself (because the correct timestamps are much older).\r\n" +
            "\r\n" +
            "Beware that this is not an optimal solution because at application startup -" + nameof(StreamProcessor) + "- will " +
            "inevitably do some logging with up to date timestamps BEFORE synchronization starts, before older timestamps, meaning " +
            "that timestamps in the AgoRapide storage (in the -" + nameof(ARConcepts.PropertyStream) + "-) are not sequential in rising order.\r\n" +
            "\r\n" +
            "TODO: An alternative would be to just inject the new properties directly in the data storage BEFORE -" + nameof(StreamProcessor) + "- " +
            "TODO: is initialized (storing text files directly into the data storage directory without knowledge of the -" + nameof(StreamProcessor) + "-).\r\n" +
            "TODO: BUT, if the actual synchronization status is stored within the data storage, we have a chicken-and-egg problem " +
            "TODO: because then -" + nameof(StreamProcessor) + "- would be needed in order to read the storage into memory " +
            "TODO: BEFORE the synchronization takes place.\r\n"
        )]
        public bool DoNotAddTimestamp { get; private set; }

        [ClassMember(Description =
            "Data received from an outgoing connection, that is, data received based on 'this' application's subscription request (as subscribing client).\r\n" +
            "This data must NOT be sent back again over any outgoing connection.\r\n" +
            "It should only be distributed locally (through -" + nameof(StreamProcessor.OutsideLocalReceiver) + ", typical for an -" + nameof(ARNodeType.Client) + "-) " +
            "and to incoming connections (incoming connections from subscribing clients) " +
            "whenever it matches their -" + nameof(Subscription) + "- request (typical for an -" + nameof(ARNodeType.ArmDB) + "-"
        )]
        public bool ReceivedFromOutgoing { get; private set; }

        public PropertyStreamLineWithOrigin(
            string line,
            bool hasLocalOrigin,
            bool sendLocalReceiver,
            bool receivedFromOutgoing,
            bool doNotAddTimestamp
        ) {
            if (string.Empty.Equals(line)) throw new ArgumentException("Parameter " + nameof(line) + ", empty string not allowed");
            PropertyStreamLine = line;
            HasLocalOrigin = hasLocalOrigin;
            SendToLocalReceiver = sendLocalReceiver;
            ReceivedFromOutgoing = receivedFromOutgoing;
            DoNotAddTimestamp = doNotAddTimestamp;
        }
    }

    [Enum(
        AREnumType = AREnumType.PropertyKeyEnum,
        Description = "Describes class -" + nameof(StreamProcessor) + "-."
    )]
    public enum StreamProcessorP {
        __invalid,

        [PKType(
            Description = "The local path to which new properties are stored. Should NOT end with any trailing " + nameof(System.IO.Path.DirectorySeparatorChar)
        /// IsObligatory = true  // We can not set this to obligatory because irrelevant when <see cref="DoNotStoreLocally"/>
        )]
        LocalStorageFolder,

        [PKType(
            Description =
                "Max size (in bytes) for a single storage file before a new file is created by call to -StreamProcessor.CreateNewStorageFile-.\r\n" +
                "Note: The actual resulting file-size will vary depending on actual method used for measuring file-size.\r\n" +
                "\r\n" +
                "The value to be chosen will affect in the following manner:\r\n" +
                "\r\n" +
                "1) Memory usage for -" + nameof(ARNodeType.CoreDB) + "- / -" + nameof(ARNodeType.ArmDB) + "- (smaller value is better). " +
                "These nodes do not need to hold the whole database in memory, but they will as a minimum have to read the last " +
                "(the current) storage file into memory, and possible also some older files for subscribing clients which " +
                "do not yet have an updated copy of the data relevant for their -" + nameof(Subscription) + "-.\r\n" +
                "(note that memory used will under normal circumstances be more than double the size of the storage file.)\r\n" +
                "\r\n" +
                "2) Response time at startup (smaller value is better). For instance for -" + nameof(ARNodeType.CoreDB) + "-, if all clients are 'up-to-date', then having a small current " +
                "storage file is advantageous because only that (last) file have to be read into memory at startup.\r\n" +
                "\r\n" +
                "3) -" + nameof(ARConcepts.DataRetention) + "- (smaller value is better). Smaller filesize would mean each file gets processed quicker, meaning it should affect less " +
                "the performance of more critical processing (data retention is assumed not time-critical / performance critical).\r\n" +
                "\r\n" +
                "4) If a file-system (file copy) method of backup is used, then more copying will be needed the greater the file size chosen (smaller value is better).\r\n" +
                "NOTE: You can as well do backup through an -" + nameof(Subscription) + "-, instead of by file-copying at the -" + nameof(ARNodeType.CoreDB) + "- server.\r\n" +
                "5) Debugging / administrative tasks. Opening of a storage file in an editor (smaller value is maybe better). Changing content of storage values (higher value is maybe better, a lesser number of files to contend with).\r\n" +
                "\r\n" +
                "6) The actual number of files stored in the -" + nameof(LocalStorageFolder) + "- (higher value is better). All filenames are read by -" + nameof(StreamProcessor.StartTCPIPCommunication) + "- at startup " +
                "so having a huge amount of files may be less optimal.\r\n" +
                "\r\n" +
                "7) Coordination among different -" + nameof(ARNodeType.CoreDB) + "- instances (smaller value is better). " +
                "(currently not implemented, Apr 2020). It is assumed much easier to coordinate with smaller filesizes. Like calculating hash-values in order to " +
                "ensure identical files for instance, which in turn will ensure that -" + nameof(ClientUpdatePosition) + "- " +
                "between different -" + nameof(ARNodeType.CoreDB) + "- instances are compatible.\r\n",
            LongDescription =
                "Note that setting a really huge value could breach limits of object storage in RAM " +
                "(like number of items in a List<> (because index is 32 bit only) and similar).\r\n" +
                "A value of up to 1GB is considered quite safe though.\r\n\" +" +
                "TODO: Research further these limitations",
            Type = typeof(long),
            DefaultValue = 100000000L // 100 MBytes
        )]
        IndividualStorageFileSize,

        [PKType(
            Description =
                "Set to TRUE at initialization (signals for instance -" + nameof(StreamProcessor.ReceiveAndDistribute) + "- " +
                "that it can only queue messages, not process them).\r\n" +
                "\r\n" +
                "With a local data storage (that is, when -" + nameof(DoNotStoreLocally) + "- is false (default)), initialization can take " +
                "quite some time.\r\n" +
                "\r\n" +
                "See also -" + nameof(TimestampIsOld) + "-",
            Type = typeof(bool)
        )]
        CurrentlyInitializing,

        [PKType(Description =
            "Complete path to file containing -" + nameof(ClientUpdatePosition) + "-.\r\n" +
            "Only relevant if outgoing connections (with -" + nameof(DataTransferDirection.Receive) + "-) are to be made.\r\n" +
            "Set by -" + nameof(StreamProcessor.StartTCPIPCommunication) + "-."
        )]
        ClientUpdatePositionPath,

        [PKType(
            Description =
                "Note how this is stored both as -" + nameof(StreamProcessorP.ClientUpdatePosition) + "- (on the client side) " +
                "and -" + nameof(ActualConnectionP.ClientUpdatePosition) + "- (on the server side).\r\n" +
                "-" + nameof(StreamProcessor) + "- (on the client side) also stores it locally 'on disk' and initializes from there.",
            Type = typeof(ClientUpdatePosition))]
        [PKLog(DoNotLogAtAll = true)]
        ClientUpdatePosition,

        [PKType(
            Description =
                "If not set, then class will not listen for incoming connections " +
                "(meaning that our application is probably a -" + nameof(ARNodeType.Client) + "-).",
            Type = typeof(int)
        )]
        IncomingConnectionsPortNo,

        // TODO: Consider adding this somewhere (but not necessarily here in this class)
        //[PKType(Type = typeof(int))]
        //// Note: We do not want to log every change in this value. It might trigger new exceptions.
        //[PKLog(DoNotLogAtAll = true)] // TODO: Change in order to use TimeInterval for this value instead.
        //CountExceptions,

        // TODO: Consider adding this. Could be set by OutsideLocalReceiver.
        //[PKType(Type = typeof(int))]
        //// Note: We do not want to log every change in this value. It might trigger new exceptions.
        //[PKLog(DoNotLogAtAll = true)] // TODO: Change in order to use TimeInterval for this value instead.
        //CountInvalidPropertyStreamLinesReceived,

        [PKType(
            Description =
                "Set to TRUE if to be closed, whenever an exception occurs or when class goes out of scope",
            Type = typeof(bool)
        )]
        UnderClosure,

        [PKType(
            Description =
                "If true then incoming data (from other nodes will be written to console.\r\n" +
                "Note that data with -" + nameof(PropertyStreamLineWithOrigin.HasLocalOrigin) + "- will not be written to console " +
                "(the local originator can to that by 'itself' instead, if so desired)",
            Type = typeof(bool)
        )]
        WriteToConsole,

        [PKType(
            Description =
                "The timestamp inserted at regular intervals in the -" + nameof(ARConcepts.PropertyStream) + "- according to -" + nameof(TimestampResolution) + "-.\r\n" +
                "\r\n" +
                "Note how -" + nameof(StreamProcessor.AddTimestampAsRequired) + "- uses a format reflecting the resolution, " +
                "for instance if resolution is in whole minutes, then timestamp will be in format yyyy-MM-dd HH:mm.\r\n" +
                "\r\n" +
                "Specially understood by -" + nameof(PropertyStreamLine.TryStore) + "-.\r\n" +
                "\r\n" +
                "This property can always be assumed to be at the top hierarchical level in the property stream. " +
                "(outside can look for it at the top level in the hierarchical data storage).\r\n" +
                "\r\n" +
                "Gets specific treatmeant by -" + nameof(PropertyStreamLine.TryStore) + "- (see -" + nameof(PropertyStreamLine.CurrentTimestamp) + "-).",
            Type = typeof(DateTime)
        )]
        Timestamp,

        [PKType(
            Description =
                "A warning sent by -" + nameof(StreamProcessor) + "- to -" + nameof(StreamProcessor.OutsideLocalReceiver) + "- when it knows that data " +
                "as served is out of date.\r\n" +
                "\r\n" +
                "Will normally correspond to -" + nameof(CurrentlyInitializing) + "-, " +
                "or that application has initialized but reading from data storage is still in progress.\r\n" +
                "\r\n" +
                "TODO: Expand use of this to also include when Stream processor knows that it has connection problems with " +
                "TODO: an upstream source of data.\r\n" +
                "\r\n" +
                "Specially understood by -" + nameof(PropertyStreamLine.TryStore) + "-.\r\n" +
                "\r\n" +
                "This property can always be assumed to be at the top hierarchical level in the property stream. " +
                "(outside can look for it at the top level in the hierarchical data storage)",
            Type = typeof(bool)
        )]
        TimestampIsOld,

        [PKType(
            Description =
                "The time interval between each timestamp update in the -" + nameof(ARConcepts.PropertyStream) + "-.\r\n" +
                "\r\n" +
                "(See also -"+ nameof(TimestampResolution) +"- concept in -" + nameof(ARComponents.ARCEvent) + "-.-UtilEvent-.)\r\n" +
                "\r\n" +
                "Used by -" + nameof(StreamProcessor.AddTimestampAsRequired) + "-.\r\n" +
                "\r\n" +
                "Only relevant for -" + nameof(ARNodeType.CoreDB) + "- (there should only be one source of timestamps in the system).\r\n" +
                "(If this value is present, then -" + nameof(StreamProcessor) + "- will insert timestamps at the specified interval).\r\n" +
                "\r\n" +
                "Background: The -" + nameof(ARConcepts.PropertyStream) + "- principle lends itself well to keeping historical track of changes in " +
                "your database. A method for timestamping the updates is therefore also natural to include.\r\n" +
                "\r\n" +
                "But, by default, timestamps are not stored with each -" + nameof(ARConcepts.PropertyStream) + "--line. " +
                "(you might have expected each property stream line to start with a timestamp like '2020-04-26 11:02:37' but that is not the case.)\r\n" +
                "\r\n" +
                "Instead, timestamps are inserted at regular intervals in the stream as Timestamp = [yyyy-MM-dd HH:mm ...]. " +
                "This is a practical approach reflecting the fact that consecutive timestamps will often be identical anyway and therefore " +
                "just taking up space.\r\n" +
                "\r\n" +
                "Note that if this value is not set at all, then neither will timestamps be inserted into the -" + nameof(ARConcepts.PropertyStream) + "-.\r\n" +
                "\r\n" +
                "The higher value chosen, the less space timestamps will take up in storage and transfer.\r\n" +
                "\r\n" +
                "Set to the highest value acceptable according to your time-resolution needs. If you have a need for a one-minute accuracy for " +
                "timestamps, set this value to 1 minute.\r\n" +
                "\r\n" +
                "If you want the highest possible resolution for when properties are created (down to the millisecond), set a resolution of " +
                "one millisecond (ticks are not supported).\r\n",
            LongDescription =
                "Properties for which we want to store in-memory a -" + nameof(PP.Created) + "-, / -" + nameof(PP.Invalid) + "- field or similar " +
                "are given the latest timestamp value found.",
            Type = typeof(TimeSpan)
        // DefaultValue = xxx NOTE: TimeSpan-literals are not available in C#, so we are unable to specify a default value here. Anyway, there should not be a default value for this property because only DBCore is supposed to insert timestamps.
        )]
        TimestampResolution,

        // TODO: Clarify documentation text here as we get experience with using system.
        // TODO: Maybe not necessary at all. If LocalPath is not set, then do not store locally
        [PKType(
            Description =
                "For -" + nameof(ARNodeType.Client) + "-, incoming data (as received from a -" + nameof(ARCCore.ConnectionDirection.Outgoing) + "-) " +
                "is possible to store BOTH to disk, AND to (normally) a -" + nameof(PRich) + "-.\r\n" +
                "\r\n" +
                "At application startup the data stored to disk can then be read locally directly into the -" + nameof(PRich) + "-, " +
                "meaning that the application only has to ask for updated information, not the whole picture.\r\n" +
                "\r\n" +
                "Set to TRUE if this is not desired. That is, when no local storage is desired " +
                "(for instance if you only want an ephemeral live-stream log window for instance). " +
                "Be careful so your subscription correspondingly only asks for relatively new data, not historical data " +
                "(typical by setting -" + nameof(ActualConnectionP.ClientUpdatePosition) + "- to -" + nameof(ARCCore.ClientUpdatePosition.OnlyNewDataIsRequestedInstance) + "-). " +
                "(If not, you will receive the whole database every you connect to the -" + nameof(ARNodeType.CoreDB) + "-.)",
            LongDescription =
                "Note that for -" + nameof(ARNodeType.CoreDB) + "- incoming data (as received from a -" + nameof(ARCCore.ConnectionDirection.Incoming) + "-) " +
                "is supposed to always be stored locally, meaning this property is not relevant.\r\n" +
                "\r\n" +
                "Note that for -" + nameof(ARNodeType.ArmDB) + "- incoming data (as received from a -" + nameof(ARCCore.ConnectionDirection.Outgoing) + "-, that is, from a -" + nameof(ARNodeType.CoreDB) + "-) " +
                "is supposed to always be stored locally, meaning this property is not relevant.\r\n" +
                "\r\n" +
                "Note that for -" + nameof(ARNodeType.ArmDB) + "- incoming data (as received from a -" + nameof(ARCCore.ConnectionDirection.Incoming) + "-, that is, from a -" + nameof(ARNodeType.Client) + "-) " +
                "is supposed to always be cached locally until sent to -" + nameof(ARNodeType.CoreDB) + "-, meaning this property is not relevant.\r\n",
            Type = typeof(bool)
        )]
        DoNotStoreLocally,

        [PKType(
            Description =
                "Means 'do not store local origin locally but wait until it gets confirmed by core storage'.\r\n" +
                "\r\n" +
                "Set to TRUE if you do not want locally orginated data to be stored directly locally\r\n" +
                "\r\n" +
                "This is normally relevant to do when you have outgoing connections, one for sending to -" + nameof(ARNodeType.CoreDB) + "- and " +
                "one subscribing to the same content again.\r\n" +
                "Setting this value to TRUE will then ensure that locally originated data do not get stored twice " +
                "(and also ensure that data is not stored permanently on the client side (-" + nameof(ARNodeType.Client) + "-) " +
                "before it has been confirmed stored by -" + nameof(ARNodeType.CoreDB) + "-.)",
            Type = typeof(bool)
        )]
        DoNotStoreLocalOriginLocally,

        [PKType(
            Description =
                "If TRUE then will write the -" + nameof(ARConcepts.PropertyStream) + "- to disk in bigger chunks, thereby increasing performance.\r\n" +
                "\r\n" +
                "This entails some risk of loosing data (especially with -" + nameof(ARNodeType.CoreDB) + "-) because of ungraceful shutdown to application.\r\n" +
                "(-" + nameof(ARNodeType.Client) + "- / -" + nameof(ARNodeType.ArmDB) + "- is less vulnerable since -" + nameof(ClientUpdatePosition) + "- " +
                "is normally stored at the same time.)." +
                "\r\n" +
                "Make little sense to use for -" + nameof(ARNodeType.CoreDB) + "- unless storage-speed is too slow. " +
                "More relevant for -" + nameof(ARNodeType.Client) + "- / -" + nameof(ARNodeType.ArmDB) + "- since these will more often read big amounts of historical " +
                "data from -" + nameof(ARNodeType.CoreDB) + "-, especially the first time a new node is run.\r\n" +
                "\r\n" +
                "NOTE: Implementation will have to add some graceful application shutdown mechanism, so at least the write-to-disk thread does not suffer a " +
                "NOTE: ThreadAbortException at normal shutdowns. This is not implemented as of April 2020\r\n" +
                "r\n" +
                "NOTE: Value can probably be changed from FALSE to TRUE while running, but not the other way around " +
                "(could lead to wrong ordering of writing to disk at moment of change)",
            Type = typeof(bool)
        )]
        CacheDiskWrites,

        [PKType(
            Description =
                "If TRUE, will copy the exact file structure (through information given in -" + nameof(ClientUpdatePosition) + "-). " +
                "\r\n" +
                "That is, instead of independently decide when to add new storage files, -" + nameof(StreamProcessor) + "- will use the same filenames " +
                "as received from its 'partner'.\r\n" +
                "\r\n" +
                "This is useful in situations where you want to use -" + nameof(MultipleConnectionUsage.Simultaneous) + ".\r\n" +
                "\r\n" +
                "Note how this only gives meaning for -" + nameof(Subscription.IsAll) + "- starting from -" + nameof(ARCCore.ClientUpdatePosition.ClientDatastoreIsEmpty) + "-.\r\n" +
                "\r\n" +
                "TODO: Rename into something better, like 'CopyFileStructureFromPartner' or similar.\r\n" +
                "\r\n" +
                "NOTE: Support for TRUE not implemented as of Apr 2020",
            LongDescription =
                "By using -" + nameof(StorageFileP.Hash) + "-, exact copies of files can be maintained between servers"
        )]
        CopyFileStructureFromCore,

        [PKType(Type = typeof(int))]
        CountIncomingConnections,

        [PKType(Type = typeof(int))]
        CountOutgoingConnections,

        [PKType(
            Description =
                "Count of calls to -" + nameof(StreamProcessor.OutsideLocalReceiver) + "-.\r\n" +
                "Useful for debugging, especially for API's (ensuring that subscription is active).\r\n" +
                "Count of -" + nameof(PropertyStreamLine) + "- sent to -" + nameof(StreamProcessor.OutsideLocalReceiver) + "- (after initialization) " +
                "(because they were received from an 'outside' source, that is, from a -" + nameof(Subscription) + "- to -" + nameof(ARConcepts.PropertyStream) + "-).\r\n",
            Type = typeof(long))]
        [PKLog(DoNotLogAtAll = true)] // TODO: Change in order to use TimeInterval for this value instead.
        CountOutsideLocalReceiver,

        [PKType(
            Description =
                "Count of -" + nameof(PropertyStreamLine) + "- " +
                "with -" + nameof(PropertyStreamLineWithOrigin.HasLocalOrigin) + "-.",
            Type = typeof(long))]
        [PKLog(DoNotLogAtAll = true)] // TODO: Change in order to use TimeInterval for this value instead.
        CountReceiveLocalOrigin,

        [PKType(
            Description =
                "Count of -" + nameof(PropertyStreamLine) + "- " +
                "not with -" + nameof(PropertyStreamLineWithOrigin.HasLocalOrigin) + "-.",
            Type = typeof(long))]
        [PKLog(DoNotLogAtAll = true)] // TODO: Change in order to use TimeInterval for this value instead.
        CountReceiveNonLocalOrigin,
    }

    [Class(Description = "Implemented only in order to store some ideas about -" + nameof(StorageFileP.Hash) + "-.")]
    public class StorageFile : PRich {
    }

    [Enum(
        Description =
            "Describes class -" + nameof(StorageFile) + "-.\r\n" +
            "\r\n" +
            "Implemented only in order to store some ideas about -" + nameof(StorageFileP.Hash) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum StorageFileP {
        __invalid,

        Filename,

        [PKType(
            Description =
                "Hash of storage file. " +
                "Not implemented as of Apr 2020. " +
                "It is envisaged to store hash-value as last line in storage file. In this case the hash must be the hash of the file excluding the last line",
            LongDescription =
                "Can be used in connection with -" + nameof(StreamProcessorP.CopyFileStructureFromCore) + "- in order to ensure exact copies of storage structure"
        )]
        Hash
    }

    // TODO: Move into ARCore/Enum-folder if turns out to be permanent (or to separate file in StreamProcessor folder)
    [Enum(
        Description =
            "The concept of who took the initiative for the connections (who asked to open the TCP/IP session).\r\n" +
            "\r\n" +
            "Note that not actually used in the code (as of Apr 2020), only used for documentation.\r\n" +
            "(The code uses the distinction of -" + nameof(ActualConnectionP.ConnectionInstruction) + "- being present or not to determine " +
            "direction of connection (only outgoing connections have a connection instructions))." +
            "\r\n" +
            "Always seen from the perspective of the application itself. That is, for the same connection seen from the client-side and the server-side, " +
            "the value will differ. For instance if the client has made an Outgoing connection in order to receive data, the server will see the same connection as " +
            "Incoming",
        AREnumType = AREnumType.OrdinaryEnum
    )]
    public enum ConnectionDirection {
        __invalid,

        [EnumMember(
            Description = nameof(ActualConnectionP.ConnectionInstruction) + " will not be set for these.\r\n"
        )]
        Incoming,

        [EnumMember(Description =
            "There would normally exist two live outgoing connection at a given point in time for " +
            "-" + nameof(ARNodeType.Client) + "- / -" + nameof(ARNodeType.ArmDB) + "-:\r\n" +
            "1) A -" + nameof(DataTransferDirection.Receive) + "- connection, and " +
            "2) a -" + nameof(DataTransferDirection.Send) + "- connection.)\r\n"
        )]
        Outgoing
    }

    // TODO: Move into ARCore/Enum-folder if turns out to be permanent (or to separate file in StreamProcessor folder)
    [Enum(
        Description =
            "Direction for transfer of data.\r\n" +
            "\r\n" +
            "As of Apr 2020 only used for -" + nameof(ConnectionDirection.Outgoing) + "- connections. " +
            "For -" + nameof(ConnectionDirection.Incoming) + "- connections the server relies more passively on " +
            "the presence of -" + nameof(ActualConnectionP.ClientUpdatePosition) + "- / -" + nameof(ActualConnectionP.SubscriptionAsRequestedByClient) + "- to " +
            "determine a connection as -" + nameof(DataTransferDirection.Send) + "-.\r\n" +
            "(in general the server will accept anything SENT to it, and store it). " +
            "\r\n" +
            "Always seen from the perspective of the application itself. That is, for the same connection seen from the client-side and the server-side, " +
            "the value will differ. For instance if the client has made an outgoing connection in order to Receive data, the server will see the same connection as " +
            "Send",
        AREnumType = AREnumType.OrdinaryEnum
    )]
    public enum DataTransferDirection {
        __invalid,

        [EnumMember(
            Description =
            "Like -" + nameof(ARNodeType.Client) + "- sending in-application generated properties to -" + nameof(ARNodeType.ArmDB) + "- / -" + nameof(ARNodeType.CoreDB) + "- or " +
            "-" + nameof(ARNodeType.ArmDB) + "- sending data received over an incoming connection to -" + nameof(ARNodeType.CoreDB) + "-."
        )]
        Send,

        [EnumMember(Description =
            "Like -" + nameof(ARNodeType.Client) + "- receiving data from an upstream  -" + nameof(ARNodeType.ArmDB) + "- / -" + nameof(ARNodeType.CoreDB) + "- or " +
            "-" + nameof(ARNodeType.ArmDB) + "- receiving data from an upstream -" + nameof(ARNodeType.CoreDB) + "-."
        )]
        Receive
    }
}