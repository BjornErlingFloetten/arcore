﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;

namespace ARCCore {
    [Class(
        Description =
            "Describes from WHERE (from WHEN) in the historic -" + nameof(ARConcepts.PropertyStream) + "- new data is to be returned.\r\n" +
            "(or actually, it describes up to which point in time client already has updated data).\r\n" +
            "\r\n" +
            "Communicated between client and server as follows:\r\n" +
            "\r\n" +
            "1) At initialization of -" + nameof(ActualConnection) + "-:\r\n" +
            "Sent by client to server in order to inform the server about the client's update status.\r\n" +
            "\r\n" +
            "2) For every new -" + nameof(PropertyStreamLine) + "- sent from server to client:\r\n" +
            "Sent by server to client. Done by prepending to the -" + nameof(PropertyStreamLine) + "- being sent, with '::' (double colon) as a separator.\r\n" +
            "Explains from where in the -" + nameof(ARConcepts.PropertyStream) + "- this line originated.\r\n" +
            "The server also keeps track of the last value sent to the client as long as the -" + nameof(ActualConnection) + "- is active.\r\n" +
            "\r\n" +
            "Used in connection with -" + nameof(Subscription) + "- which describes WHAT data is required from the -" + nameof(ARConcepts.PropertyStream) + "-.\r\n" +
            "TODO: Clarify difference between -" + nameof(ClientUpdatePosition) + "- and -" + nameof(Subscription.FromTime) + "- / -" + nameof(Subscription.ToTime) + "-.\r\n" +
            "\r\n" +
            "TODO: Build revision number into this? (-" + nameof(ARConcepts.DataRetention) + "- or keep in filename?\r\n" +
            "\r\n" +
            "TODO: Ensure that the format being used suits the encoding mechanism well " +
            "(see " + nameof(PropertyStreamLine.EncodeValuePart) + ") " +
            "(no special encoding should be necessary as want a human readable format also after encoding)\r\n" +
            "\r\n" +
            "Describes status of a client's up-to-date level.\r\n" +
            "Note that client in this case not only means -" + nameof(ARNodeType.Client) + "- but any -" + nameof(ARNodeType) + "-.\r\n" +
            "r\n" +
            "Examples:\r\n" +
            "\r\n" +
            "1) 'ClientDatastoreIsEmpty' (see -" + nameof(ClientDatastoreIsEmpty) + "-).\r\n" +
            "\r\n" +
            "2) 'OnlyNewDataIsRequested' (see -" + nameof(OnlyNewDataIsRequested) + "-).\r\n" +
            "\r\n" +
            "3) 'GoBack|1000' (see -" + nameof(IsGoBack) + "-) means start from 1000 elements back.\r\n" +
            "\r\n" +
            "4) 'StorageFile0042|42042': The common scenario. Last update was index 42042 in 'StorageFile0042', " +
            "next update should be at least 42043 (or index 0 in next file)\r\n" +
            "\r\n" +
            "An 'IsEndOfStream' marker may be added, like this:\r\n" +
            "'StorageFile0068|42042042|IsEndOfStream' (see -" + nameof(IsAtEndOfStream) + "-).\r\n" +
            "\r\n" +
            "This class is immutable.\r\n",
        LongDescription =
            "\r\n" +
            "TODO: Note how we assume that if we have multiple outgoing connections, that is, if we\r\n" +
            "TODO: have multiple CoreDB's to choose from, they are all compatible regarding clientUpdateStatus.\r\n" +
            "TODO: This would probably not be the case with how clientUpdateStatus is built up today\r\n" +
            "TODO: (Apr 2020) with filename + index.\r\n" +
            "TODO: Consider adding timestamp, actual property line or similar, in order for different CoreDB instances to more " +
            "TODO: accurately decide from where to 'pick-up' from when receiving a new incoming connection with a subscription request." +
            "TODO: OR, alternatively, ensure that CoreDB instances coordinate among themselves so that they always have the exact same " +
            "TODO. storage-layout (having a master instance at any given time for instance).\r\n" +
            "\r\n" +
            "TODO: If we add timestamp, it can then be up to the client to keep track of it, by 'listening' to Timestamps / 'Heartbeats' inserted in the " +
            "TODO: property stream. The client can then transmit it to the server, as a backup, in case the ordinary filename + index " +
            "TODO: information makes no sense. Using only Timestamp would of course lead to some overlap (duplicate property stream lines being sent), " +
            "TOOD: since it is probably quite coarse." +
            "\r\n" +
            "TODO: Other ideas for keeping clients correctly in sync:\r\n" +
            "TODO: Server can inject the current position at regular intervals in the property stream. This serves the same purpose as injecting a " +
            "TODO: Timestamp / 'Heartbeat' but the position becomes exact instead of approximate. When the client understands that there is some" +
            "TODO: corruption present, it can look for the last such value, delete stored data after that position, and update from server from this " +
            "TODO: exact known position. (actually the client can inject the position itself, since it receives the position with any value sent)"
    )]
    public class ClientUpdatePosition : ITypeDescriber, IEquatable<ClientUpdatePosition> {

        [ClassMember(Description = "Filename like '2020-04-03 08:42:22_R001'")]
        public string Filename { get; }
        [ClassMember(
            Description =
                "Index (zero-based) into file. Value of -1 means blank update status / no data received at all (or no data received for this specific filename)",
            LongDescription =
                "32 bit because List<string> which is used for storage do not support 64 bit indexing anyway"
        )]
        public int Index { get; }

        [ClassMember(Description =
            "Used by the subscribing client to signify that it has no data stored locally, " +
            "and it therefore needs an update from the beginning of the -" + nameof(ARConcepts.PropertyStream) + "-, " +
            "that is, 'from the beginning of time'.\r\n" +
            "\r\n" +
            "This will usually be the case when a new node is introduced, and it needs to build up its own local cache.\r\n" +
            "\r\n" +
            "See also -" + nameof(ClientDatastoreIsEmptyInstance) + "-."
        )]
        public bool ClientDatastoreIsEmpty =>
            // Not possible to do, because when server switches to next file, the index will be -1
            // Index.Equals(-1);
            // Correct approach:
            Filename.Equals("ClientDatastoreIsEmpty");

        [ClassMember(Description =
            "Used by the subscribing client to signify that it does not care about historical data, " +
            "the server may start with sending only new data.\r\n" +
            "\r\n" +
            "A typical example is starting up a log-console in order to monitor some action.\r\n" +
            "\r\n" +
            "Note that this indication is only approximate because data gets continously added to the -" + nameof(ARConcepts.PropertyStream) + "-.\r\n" +
            "\r\n" +
            "About the internal implementation: Note how -" + nameof(ActualConnection.Communicate) + "- immediately replaces this value with the actual up-to-date position " +
            "(instead of waiting for -" + nameof(StreamProcessor) + "- to decide from where to start sending data, because then some new data may be lost. " +
            "One would then not risk loosing a few tenths of seconds of -" + nameof(ARConcepts.PropertyStream) + "- data only " +
            "because it takes some time for -" + nameof(StreamProcessor) + "- to start processing the client request)\r\n" +
            "\r\n" +
            "See also -" + nameof(OnlyNewDataIsRequestedInstance) + "-."
        )]
        public bool OnlyNewDataIsRequested => Index.Equals(int.MaxValue);

        [ClassMember(Description =
            "Has to be translated by -" + nameof(StreamProcessor) + "- into a concrete filename / index.\r\n" +
            "Useful when subscribing client wants (in general) new log-data, but also data for the last few minutes, for instance when you see a special situation and " +
            "want to look into the latest log data.\r\n" +
            "TODO: 'GoBack' is not implemented as of May 2020.\r\n" +
            "TODO: Implement GoBack also as relative time, like 'minus 30 seconds' or absolute time like 'from 08:52'\r\n" +
            "TODO: Consider also necessity for Subscription.StartAt / EndAt, which are similar concepts"
        )]
        public bool IsGoBack => Filename.Equals("GoBack");

        [ClassMember(Description =
            "Necessary in order to implement -" + nameof(ARConcepts.AdHocQuery) + "-. " +
            "\r\n" +
            "Only sent by server to client. Signals to the client that it has now received all data available (for the time being, of course).\r\n" +
            "Used by subscribing client in order decide when an -" + nameof(ARConcepts.AdHocQuery) + "- query is complete.\r\n" +
            "Usually ignored by client for continous on-going -" + nameof(Subscription) + "-.\r\n" +
            "\r\n" +
            "NOTE: Not implemented as of May 2020.\r\n" +
            "\r\n" +
            "Note that fundamentally different from -" + nameof(OnlyNewDataIsRequested) + " " +
            "(the latter is communicated from client to server, this flag is communicated from server to client)"
        )]
        public bool IsAtEndOfStream { get; private set; }

        public override string ToString() =>
            Filename +
            ((Index == -1 || Index == int.MaxValue) ? "" : ("|" + Index)) +
            (!IsAtEndOfStream ? "" : ("|" + nameof(IsAtEndOfStream)));

        public ClientUpdatePosition(string filename, int index, bool isAtEndOfStream) {
            Filename = System.IO.Path.GetFileName(filename);
            Index = index;
            IsAtEndOfStream = isAtEndOfStream;
        }

        public static ClientUpdatePosition Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new InvalidClientUpdateStatusException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse + "\r\n");
        public static bool TryParse(string value, out ClientUpdatePosition retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out ClientUpdatePosition retval, out string errorResponse) {
            if (string.IsNullOrEmpty(value)) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = "Null or Empty value (" + value + ")";
                return false;
            }
            if (value.Equals(nameof(ClientDatastoreIsEmpty))) {
                retval = ClientDatastoreIsEmptyInstance;
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                return true;
            }

            if (value.Equals(nameof(OnlyNewDataIsRequested))) {
                retval = OnlyNewDataIsRequestedInstance;
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                return true;
            }

            var t = value.Split("|");
            switch (t.Length) {
                case 2:
                    break; // OK
                case 3:
                    if (!nameof(IsAtEndOfStream).Equals(t[2])) {
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                        errorResponse = "Third element (index 2) not '" + nameof(IsAtEndOfStream) + "' but '" + t[2] + "'";
                        return false;
                    }
                    break; // OK
                default:
                    retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                    errorResponse = "Not two or three elements separated by '|' (vertical bar) but " + t.Length + " elements";
                    return false;
            }

            if (!int.TryParse(t[1], out var index)) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = "Second element (index 1) not a valid int (" + t[1] + ")";
                return false;
            }
            if (t[0].ToLower().Equals("goback")) t[0] = "GoBack";
            retval = new ClientUpdatePosition(t[0], index, isAtEndOfStream: t.Length == 3 && nameof(IsAtEndOfStream).Equals(t[2]));
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
            return true;
        }

        [ClassMember(Description = "See also -" + nameof(ClientDatastoreIsEmpty) + ".")]
        public static ClientUpdatePosition ClientDatastoreIsEmptyInstance = new ClientUpdatePosition(nameof(ClientDatastoreIsEmpty), index: -1, isAtEndOfStream: false);

        [ClassMember(Description = "See also -" + nameof(OnlyNewDataIsRequested) + ".")]
        public static ClientUpdatePosition OnlyNewDataIsRequestedInstance = new ClientUpdatePosition(nameof(OnlyNewDataIsRequested), index: int.MaxValue, isAtEndOfStream: false);

        public override int GetHashCode() => Filename.GetHashCode() + Index + (IsAtEndOfStream ? 1 : 0); // TODO: Not necessarily a good hash-code. NOTE: Int-overflows are 'OK' in C# by default.
        public bool Equals(ClientUpdatePosition other) => Filename.Equals(other.Filename) && Index.Equals(other.Index);
        public override bool Equals(object other) => other is ClientUpdatePosition c && Equals(c);

        // IMPORTANT POINT, leave out this flag. equality holdes anyway
        // && IsAtEndOfStream.Equals(other.IsAtEndOfStream);

        /// <summary>
        /// 'Implementing' <see cref="ITypeDescriber"/>
        /// </summary>
        /// <param name="key"></param>
        public static void EnrichKey(PK k) =>
            k.SetValidatorAndParser(new Func<string, ParseResult>(value => 
                TryParse(value, out var retval, out var errorResponse) ?
                    ParseResult.CreateOK(retval) :
                    ParseResult.CreateError(errorResponse)
            ));

        public class InvalidClientUpdateStatusException : ApplicationException {
            public InvalidClientUpdateStatusException(string message) : base(message) { }
            public InvalidClientUpdateStatusException(string message, Exception inner) : base(message, inner) { }
        }
    }
}