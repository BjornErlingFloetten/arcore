﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ARCCore {
    [Class(
        Description =
            "Parses the keys part of a line from -" + nameof(ARConcepts.PropertyStream) + "-, like 'Customer/42/FirstName = John.\r\n" +
            "\r\n" +
            "Should only be used in ephemeral contexts as class is assumed to be somewhat memory hungry.\r\n" +
            "\r\n" +
            "In general, property stream lines should be stored as string only by -" + nameof(ARNodeType.CoreDB) + "- " +
            "(and possible also -" + nameof(ARNodeType.ArmDB) + "-).\r\n" +
            "-" + nameof(ARNodeType.Client) + "- will normally use -" + nameof(PropertyStreamLineParsed) + "- as a middle step in order to " +
            "to translate the string representation of a -" + nameof(PropertyStreamLine) + "- into a hierarhical data storage.\r\n"
    )]
    public class PropertyStreamLineParsed {

        [ClassMember(
            Description =
                "Signifies that property stream line given for parsing started with '//' (two forward slashes).\r\n" +
                "\r\n" +
                "If TRUE then this object contains no data"
        )]
        public bool IsComment { get; private set; }

        [ClassMember(Description =
            "The keys part of the -" + nameof(PropertyStreamLine) + "-, that is, everything before ' = '.\r\n" +
            "Note that tuple item valueType will always be of an -" + nameof(IP) + "- type.\r\n" +
            "\r\n" +
            "valueType will not be set for the last List item.\r\n" +
            "valueType is not in that position because calling method will use -" + nameof(PK.TryValidateAndParse) + "- " +
            "which picks up the key from the -" + nameof(PK) + "-.\r\n" +
            "And also, because we guarantee that valueType is only an IP-based type and pk.Type can be any type, it can not be set anyway."
        )]
        public List<(IK key, Type? valueType)> Keys { get; private set; }

        [ClassMember(Description =
            "The original (non-parsed, non-validated)) value part of the -" + nameof(PropertyStreamLine) + "-, that is, everything after ' = '.\r\n" +
            "Will be null if nothing found after ' = '."
        )]
        public string? Value { get; private set; }

        private PropertyStreamLineParsed(bool isComment, List<(IK key, Type? valueType)> keys, string? value) {
            IsComment = isComment;
            Keys = keys;
            Value = value;
        }

        [ClassMember(Description =
            "Note relatively complex operation of 'restoring' original property stream line.\r\n" +
            "Should probably only be used in debugging or failure scenarios."
        )]
        public override string ToString() =>
            string.Join("/", Keys.Select(e => PropertyStreamLine.EncodeKeyPart(e.key.ToString()))) + " = " +
            (Value == null ? "" : PropertyStreamLine.EncodeValuePart(Value));

        public static PropertyStreamLineParsed Parse(string propertyStreamLine) => TryParse(propertyStreamLine, out var retval, out var errorResponse) ? retval :
            throw new PropertyStreamLineParsedException(
                nameof(propertyStreamLine) + ": " + propertyStreamLine + "\r\n" +
                nameof(errorResponse) + ": " + errorResponse
            );

        [ClassMember(Description =
           "Generate list of <(IK key, Type valueType)> together with value from value part.\r\n" +
           "\r\n" +
           "Has very few fail-scenarios (that is will almost always return TRUE).\r\n" +
            "Note that the value itself will not be parsed (it is just returned in -" + nameof(PropertyStreamLineParsed.Value) + "- as a 'string?').\r\n" +
           "\r\n" +
           "the valueType returned will always be an -" + nameof(IP) + "- type.\r\n" +
           "(but null for last key in hierarchy).\r\n" +
           "\r\n" +
           "This is the first step in parsing a -" + nameof(PropertyStreamLine) + "-, typically used by more higher level methods like\r\n" +
           "-" + nameof(PropertyStreamLine.TryParseAndStore) + "-,\r\n" +
           "-" + nameof(PropertyStreamLine.TryParseDirectToIP) + "-,\r\n" +
           "-" + nameof(PropertyStreamLine.TryParseDirectToProperty) + "-.\r\n" +
           "\r\n" +
           "Note that if the value part consists of individual items, like in -" + nameof(Cardinality.WholeCollection) + "-, " +
           "each of these has to be decoded individually again by the calling method (by calling -" + nameof(PropertyStreamLine.Decode) + "-).\r\n" +
           "\r\n" +
           "Details: For each key, decide which instance type of -" + nameof(IK) + "- to use, that is, once of " +
           "-" + nameof(PK) + "-, -" + nameof(IKType) + "-, -" + nameof(IKString) + "- (or -" + nameof(IKLong) + "- if an integer) " +
           "in prioritized order).\r\n" +
           "\r\n" +
           "The following priority is used for deciding -" + nameof(IK) + "--keys:\r\n" +
           "1) -" + nameof(PK) + "- if a -" + nameof(PK) + "- is recognized " +
           "(like FirstName in 'Customer/42/FirstName:', based on a combination of found type 'Customer' and field 'FirstName')\r\n" +
           "\r\n" +
           "2) -" + nameof(IKType) + "- if a -" + nameof(IP) + "--derived type is recognized.\r\n" +
           "(like 'Customer' in 'Customer/42/FirstName:')\r\n" +
           "\r\n" +
           "3) -" + nameof(IKString) + "- for other cases (-" + nameof(IKLong) + "- if an integer) .\r\n" +
           "(like 42 in 'Custome/42/FirstName:'\r\n" +
           "\r\n" +
           "For each key, decide what type of value-object to create if no object exists for that key " +
           "(dedicated type like 'CustomerCollection' or 'Customer' if possible, if not -" + nameof(PRich) + "-)\r\n" +
           "\r\n" +
           "        For 'Customer/42/FirstName = John', the tuple generated will look like this:\r\n" +
           "        (which of the two alternative values are returned (IKType or IKString) depends on the actual type being known to the application (see -" + nameof(IP.AllIPDerivedTypes) + "-).\r\n" +
           "        ---------------------------------------------------------------------------------------------------------\r\n" +
           "                            key:                                                          valueType\r\n" +
           "        Index 0 Customer    IKType(typeof(Customer)) / IKString('Customer')               CustomerCollection, alternatively PRich\r\n" +
           "        Index 1 42          IKLong(42) (since integer, else IKString('42'))               Customer / PRich\r\n" +
           "        Index 2 FirstName   PK (from CustomerP.FirstName) / IKString('FirstName')         Null. Left to IP to decide at call to SetP.\r\n" +
           "\r\n" +
           "        For 'Customer/42/PhoneNumber/90534333'  ('strange' syntax is related to -" + nameof(Cardinality.IndividualItems) + "-), \r\n" +
           "        the tuple generated will look like this:\r\n" +
           "        (which of the two alternative values are returned (IKType or IKString) depends on the actual type being known to the application (see -" + nameof(IP.AllIPDerivedTypes) + "-).\r\n" +
           "        ---------------------------------------------------------------------------------------------------------\r\n" +
           "                            key:                                                          valueType\r\n" +
           "        Index 0 Customer    IKType(typeof(Customer)) / IKString('Customer')               CustomerCollection, alternatively PRich\r\n" +
           "        Index 1 42          IKLong(42) (since integer, else IKString('42'))               Customer / PRich\r\n" +
           "        Index 2 PhoneNumber PK (from CustomerP.PhoneNumber) / IKString('PhoneNumber')     Null. Left to IP to decide at call to SetP.\r\n" +
           "\r\n" +
           "Note that more complex representations like:\r\n" +
           "  Customer/42/Car/1/Colour = Red\r\n" +
           "is also understood.\r\n" +
           "So if CustomerP.Car exists then for 'Car' that corresponding PK will be used, while for '1' the type to create will be the the PK's Type.\r\n" +
           "\r\n" +
           "The above would be relevant for cardinality -" + nameof(Cardinality.IndividualItems) + "- for CustomerP.Car).\r\n" +
           "For cardinality -" + nameof(Cardinality.HistoryOnly) + "-, as in\r\n" +
           "  Customer/42/Car/Colour = Red\r\n" +
           "then for 'Car' the corresponding CustomerP.Car will be used (as above), and for Colour the corresponding PK for CarP.Colour.\r\n" +
           "\r\n" +
           "See also -" + nameof(IP.TryParseDtr) +"- which can deserialize whole records.\r\n" 
       )]
        public static bool TryParse(string propertyStreamLine, out PropertyStreamLineParsed retval, out string errorResponse) {
            //if (propertyStreamLine.Contains("Timestamp")) {
            //    var a = 1;
            //}
            if (propertyStreamLine.StartsWith("//")) {
                retval = new PropertyStreamLineParsed(isComment: true, keys: new List<(IK key, Type? valueType)>(), value: null);
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }

            var split = propertyStreamLine.IndexOf(" = ");
            List<string> listKeys;
            string? value;
            if (split == -1) {
                listKeys = propertyStreamLine.Split("/").ToList();
                value = null;
            } else {
                listKeys = propertyStreamLine[..split].Split("/").ToList();
                value = PropertyStreamLine.Decode(propertyStreamLine[(split + 3)..]); // This might result in an empty string.
                if ("".Equals(value)) value = null;
            }

            var keys = new List<(IK key, Type? valueType)>(); // Type? because not relevant for last level
            (Type type, int hierarchicalLevel)? lastTypeFound = null;

            for (var hierarchicalLevel = 0; hierarchicalLevel < listKeys.Count; hierarchicalLevel++) {
                var s = listKeys[hierarchicalLevel];

                /// Priority 1, use a <see cref="PK"/>
                if (hierarchicalLevel == 0) {
                    if (nameof(StreamProcessorP.Timestamp).Equals(s)) {
                        // Special exception for Timestamp, in order for it to be strongly typed (not stored as string in data storage)
                        keys.Add((
                            key: PK.FromEnum(StreamProcessorP.Timestamp),
                            valueType: null
                        ));
                        continue;
                    }
                    if (nameof(StreamProcessorP.TimestampIsOld).Equals(s)) {
                        // Special exception for TimestampIsOld, in order for it to be strongly typed (not stored as string in data storage)
                        keys.Add((
                            key: PK.FromEnum(StreamProcessorP.TimestampIsOld),
                            valueType: null
                        ));
                        continue;
                    }
                }
                if (
                    lastTypeFound != null &&
                    (hierarchicalLevel - lastTypeFound.Value.hierarchicalLevel <= 2) && // Max two levels apart, either Customer/42/FirstNameo = xx or Customer/FirstName
                    PK.TryGetFromTypeAndFieldName(lastTypeFound.Value.type, s, out var pk)) {
                    // For instance Customer/42/FirstName = Bjørn.
                    // We have already found the type Customer, and now the corresponding property key (two levels further 'down' in this example)

                    Type? valueType;
                    if (hierarchicalLevel == (listKeys.Count - 1)) {
                        /// We are at the last level (deepest down in the hierarchy):
                        /// See documentation here <see cref="PropertyStreamLineParsed.Keys"/> for explanation.
                        valueType = null;
                    } else if (typeof(IP).IsAssignableFrom(pk.Type)) {
                        valueType = pk.Type;
                    } else if (pk.Cardinality == Cardinality.IndividualItems) {
                        // TODO: Should this check be before check for 'typeof(IP).IsAssignableFrom(pk.Type)' above?
                        // TOOD: If so, pk.Cardinality-result should be cached inside pk as it is somewhat expensive.
                        valueType = typeof(PII); // TODO: Added 27 Jul 2020, check that correct approach
                    } else {
                        retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                        errorResponse =
                            "At hierarchical level " + hierarchicalLevel + " (zero based) of " + listKeys.Count + "\r\n" +
                            "Property key " + pk.ToString() + " specifies type " + pk.Type + " which does not inherit " + nameof(IP) + ".\r\n" +
                            nameof(propertyStreamLine) + ": " + propertyStreamLine +
                            "\r\n";
                        return false;
                    }

                    keys.Add((
                        key: pk, valueType));
                    continue;
                }

                /// Priority 2, use a <see cref="IKType"/>
                if (
                    hierarchicalLevel < (listKeys.Count - 1) &&  // Avoid creating IKType for last item (because we know it must be IKString now, since not found as PK above)
                    IP.AllIPDerivedTypesDictIKType.TryGetValue(s, out var ikType)
                ) {
                    lastTypeFound = (type: ikType.Type ?? throw new NullReferenceException("IK." + nameof(ikType.Type) + ", not expected since coming from " + nameof(IP.AllIPDerivedTypesDictIKType)), hierarchicalLevel);
                    keys.Add((
                        key: ikType,
                        // Note that valueType now is the type holding the collection of objects for the found type.
                        // This holds for 'Customer/42/Car/1/Colour = Red' but not for 'Customer/42/Car/Colour = Red'
                        // but only if CustomerP.Car does not exist (which it should, and then the code above using PK holds (key: pk, valueType: pk.Type)
                        valueType: IP.AllIPDerivedEntityCollectionClassesDict.TryGetValue(ikType.Type, out var collectionType) ?
                            collectionType : /// Specific collection type found. We are probably able to support <see cref="ARConcepts.Indexing"/> now for this collection.
                            typeof(PRich)
                    ));
                    continue;
                }

                // TODO: Add something like:
                // TODO: if string is recognized as a Type (through PropertyKeys distributed over the PropertyStream) then we should ideally
                // TODO: create an IKType but with string as parameter to constructor.

                /// Priority 3, use a <see cref="IKString"/> (<see cref="IKLong"/> if an integer)

                /// Assumed to be a sequential number like '42' in 'Customer/42/FirstName = John' (or any general identifier)
                /// May also be actual value, like '+4790534333' in 'Customer/42/PhoneNumber/+4790534333'.
                keys.Add((
                    // Note about chosing of key. It does not matter if collections of entities have different type of IK
                    // (like IKLong or IKString) because IK always compares against its string-representation.
                    // (But beware of leading zeroes in an otherwise valid integer, which must demand that we use IKString)
                    key: long.TryParse(s, out var lng) ?
                        (s[0] != '0' ? (IK)new IKLong(lng) : IKString.FromString(s)) : // Note how IKLong is more memory efficient, but do not use if leading zeroes (because may be referred in other places as string, in foreign fields for instance)
                        IKString.FromString( // Do not use cache as we might have lots of these.
                            /// Decode s before creating IKString
                            /// (decoding is expensive, therefore best to wait with until really needed)
                            /// Note that <see cref="IKType"/> and <see cref="PK"/> (checked for above) do not 'need' encoding
                            /// (their ToString-representation will never contain any special characters 
                            /// (note that <see cref="IKType"/> uses the <see cref="Extensions.ToStringVeryShort"/> for the type, so no special characters there either)).
                            /// (and for <see cref="IKLong"/> there is also no special characters)
                            PropertyStreamLine.Decode(s)
                        ),
                    valueType:
                        lastTypeFound == null || (lastTypeFound.Value.hierarchicalLevel != (hierarchicalLevel - 1)) ?
                            typeof(PRich) : // Only use lastTypeFound if was found exact one level up.
                            lastTypeFound.Value.type
                ));
                continue;
            }
            retval = new PropertyStreamLineParsed(isComment: false, keys: keys, value: value);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public class PropertyStreamLineParsedException : ApplicationException {
            public PropertyStreamLineParsedException(string message) : base(message) { }
            public PropertyStreamLineParsedException(string message, Exception inner) : base(message, inner) { }
        }
    }
}