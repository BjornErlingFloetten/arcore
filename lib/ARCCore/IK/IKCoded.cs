﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Concurrent;
using System.Linq;

namespace ARCCore {

    [Class(Description =
        "Understands coded and unencoded variants of -" + nameof(IK) + "-.\r\n" +
        "\r\n" +
        "See -" + nameof(Encoded) + "- for further documentation.\r\n" +
        "\r\n" +
        // Removed length restriction 14 Apr 2021 because caused unnecessary limitations in query length over HTTP
        //"An exception is being thrown if this class is attempted created with a longer string length " +
        //"than the maximum of 250 characters guaranteed by -" + nameof(Encoded) + "-.\r\n" +
        //"\r\n" +
        "Note how constructor allows an empty string, but -" + nameof(TryParse) + "- does not.\r\n" +
        "\r\n" +
        "TOOD: As of 22 Mar 2021 documentation is a bit unordered.\r\n" +
        "\r\n" +
        "This class is immutable."
    )]
    public class IKCoded :

        /// Note: This class does intentionally not implement <see cref="IK"/> although it would have been very easy to implement
        /// (just compare against <see cref="Unencoded"/>) 
        /// The reason is that it could have encouraged general use of this class as key in the 
        /// in-memory datastorage, something for which this class not suitable due to a relatively high memory and performance impact.
        /// (like storing two strings, performing assertions and encoding / decoding in constructor)
        // IK, 

        ITypeDescriber,
        IEquatable<IKCoded> {

        /// Note: This class does intentionally not implement <see cref="IK"/> although it would have been very easy to implement
        //public bool Equals(IK other) => ToString().Equals(other.ToString());
        //public override bool Equals(object other) => other is IK ik && Equals(ik);

        public IK Unencoded { get; private set; }
        [ClassMember(Description =
            "This string value can be used directly in folder names, file names and HTTP URL's without any further encoding being necessary.\r\n" +
            "\r\n" +
            "Contains only characters from the set 'A...Za...z0...9-_().,='.\r\n" +
            "Since this class also applies to HTTP URL encoding / decoding, space, ' ' is not allowed, nor is '+'.\r\n"
        // Removed length restriction 14 Apr 2021 because caused unnecessary limitations in query length over HTTP
        //"\r\n" +
        //"Maximum length is guaranteed to be at or below 250 characters.\r\n"
        )]
        public string Encoded { get; private set; }

        [ClassMember(Description =
            "The character set 'A...Za...z0...9-_().,='.\r\n" +
            "Since this class also applies to HTTP URL encoding / decoding, space, ' ' is not allowed, nor is '+'.\r\n"
        )]
        public static readonly HashSet<char> AllowedEncodedCharacters =
            ("ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
            "abcdefghijklmnopqrstuvwxyz" +
            "0123456789" +
            "-_().,=").
            ToCharArray().ToHashSet();

        /// <summary>
        /// NOTE: DO NOT MAKE THIS METHOD PUBLIC! Outside must not be allowed to skip assertion by setting <paramref name="alreadyValidated"/>
        /// NOTE: (and also not allowed to bypass cache)
        /// </summary>
        /// <param name="s"></param>
        /// <param name="alreadyValidated"></param>
        private IKCoded(IK? unencoded = null, string? encoded = null, bool alreadyValidated = false) {
            if (unencoded == null && encoded != null) {
                unencoded = IKString.FromString(PropertyStreamLine.Decode(encoded));
            } else if (encoded == null && unencoded != null) {
                encoded = Encode(unencoded.ToString());
            }

            if (encoded == null) {
                throw new ArgumentNullException(nameof(encoded));
            }

            if (unencoded == null) {
                throw new ArgumentNullException(nameof(unencoded));
            }

            if (alreadyValidated) {
                /// Typically because called from / via <see cref="TryParse"/>
            } else {
                if (!TryCheckValidEncodedValue(encoded, out var errorResponse)) {
                    throw new InvalidIKSafeException(
                        "Invalid " + nameof(encoded) + " value: '" + encoded + "'.\r\n" +
                        nameof(errorResponse) + ": " + errorResponse +
                        "\r\n" +
                        string.Join("", encoded.Where(c => !AllowedEncodedCharacters.Contains(c)).Select(c =>                        
                            "\r\nPossible resolution: The character '" + c + "' may be encoded as '0x" + ((int)c).ToString("X4") + "' instead."
                        ))
                    );
                }
            }
            Encoded = encoded;
            Unencoded = unencoded;
        }

        [ClassMember(Description =
            "Checks if encoded value is valid.\r\n" +
            "\r\n" +
            "Note: Check does not include checking whether all 0x occurences are followed by 'oooo' or four hex digits."
        )]
        public static bool TryCheckValidEncodedValue(string encodedValue, out string errorResponse) {
            // Removed length restriction 14 Apr 2021 because caused unnecessary limitations in query length over HTTP
            //if (encodedValue.Length > 250) {
            //    errorResponse = "More than 250 characters in length (" + encodedValue.Length + ").";
            //    return false;
            //}
            if (!encodedValue.All(c => AllowedEncodedCharacters.Contains(c))) {
                errorResponse = "Contains characters not in '" + string.Join("", AllowedEncodedCharacters) + "'.";
                return false;
            }
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        private static readonly ConcurrentDictionary<string, IKCoded> _cacheEncoded = new ConcurrentDictionary<string, IKCoded>();
        [ClassMember(Description =
            "Reduces number of objects generated by caching those known to be within a limited range\r\n" +
            "\r\n" +
            "Use this method when you know that the given parameter is within a limited range.\r\n" +
            "\r\n" +
            "Throws -" + nameof(InvalidIKSafeException) + "- if string contains characters not in -" + nameof(AllowedEncodedCharacters) + "-."
        )]
        public static IKCoded FromCacheEncoded(string encoded) => _cacheEncoded.GetOrAdd(encoded, e => new IKCoded(encoded: e));

        private static readonly ConcurrentDictionary<IK, IKCoded> _cacheUnencoded = new ConcurrentDictionary<IK, IKCoded>();
        [ClassMember(Description =
            "Reduces number of objects generated by caching those known to be within a limited range\r\n" +
            "\r\n" +
            "Use this method when you know that the given parameter is within a limited range.\r\n" +
            "\r\n" +
            "Throws -" + nameof(InvalidIKSafeException) + "- if unable to encode."
        )]
        public static IKCoded FromCacheUnencoded(IK unencoded) => _cacheUnencoded.GetOrAdd(unencoded, u => new IKCoded(unencoded: u));

        [ClassMember(Description =
            "Note how uses instance cache if already exists there.\r\n" +
            "\r\n" +
            "Throws -" + nameof(InvalidIKSafeException) + "- if parameter is invalid."
        )]
        public static IKCoded FromEncoded(string encoded) => _cacheEncoded.TryGetValue(encoded, out var retval) ? retval : new IKCoded(encoded: encoded);
        /// <summary>
        /// NOTE: DO NOT MAKE THIS METHOD PUBLIC! Outside must not be allowed to skip assertion by setting <paramref name="alreadyValidated"/>
        /// </summary>
        /// <param name="s"></param>
        /// <param name="alreadyValidated"></param>
        /// <returns></returns>
        private static IKCoded FromEncoded(string encoded, bool alreadyValidated) => _cacheEncoded.TryGetValue(encoded, out var retval) ? retval : new IKCoded(unencoded: null, encoded: encoded, alreadyValidated);

        public static IKCoded FromUnencoded(IK unencoded) => _cacheUnencoded.TryGetValue(unencoded, out var retval) ? retval : new IKCoded(unencoded);

        public static string Encode(string unencoded) => TryEncode(unencoded, out var retval, out var errorResponse) ? retval : throw new InvalidIKSafeException(
            "Invalid unencoded value: '" + unencoded.ToString() + "'.\r\n" +
            nameof(errorResponse) + ": " + errorResponse
        );

        [ClassMember(Description =
            "Returns a string containing only characters in -" + nameof(AllowedEncodedCharacters) + "-.\r\n" +
            "\r\n" +
            "Other characters are represented by '0x' plus their UTF-16 code value as four hex characters (like '0x0020' for space).\r\n" +
            "(0x itself is changed into 0xoooo before conversion takes place.)\r\n" +
            "\r\n" +
            "The encoding is the same as used by -" + nameof(PropertyStreamLine.EncodeKeyPart) + "- / -" + nameof(PropertyStreamLine.EncodeValuePart) + "- " +
            "except with a different set of allowed characters.\r\n" +
            "\r\n"
        )]
        public static bool TryEncode(string unencoded, out string encoded, out string errorResponse) {
            encoded = string.Join("", unencoded.Replace("0x", "0xoooo").Select(c => AllowedEncodedCharacters.Contains(c) ? c.ToString() : ("0x" + ((int)c).ToString("X4"))));
            // Removed length restriction 14 Apr 2021 because caused unnecessary limitations in query length over HTTP
            //if (encoded.Length > 250) {
            //    errorResponse = "Encoded result too long. Unencoded length was " + unencoded.Length + ", encoded length was " + encoded.Length;
            //    encoded = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            //}
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public override int GetHashCode() => ToString().GetHashCode();

        public bool Equals(IKCoded other) => ToString().Equals(other.ToString());
        public override bool Equals(object other) => other is IKCoded ikSafe && Equals(ikSafe);
        [ClassMember(Description =
            "Returns -" + nameof(Encoded) + "-'s string representation.\r\n" +
            "\r\n" +
            "-" + nameof(Encoded) + "- is chosen (instead of -" + nameof(Unencoded) + "-) " +
            "out of the principle that we implement -" + nameof(ITypeDescriber) + "- and therefore ToString should always match TryParse.\r\n" +
            "\r\n" +
            "Should be used with caution due to possible confusing about its meaning"
        )]
        public override string ToString() => Encoded;

        // Parse / TryParse according to AgoRapide standard. Looks like overkill of course in our case
        public static IKCoded Parse(string encodedValue) => TryParse(encodedValue, out var retval, out var errorResponse) ? retval : throw new InvalidIKSafeException(nameof(encodedValue) + ": " + encodedValue + ", " + nameof(errorResponse) + ": " + errorResponse + "\r\n");
        public static bool TryParse(string encodedValue, out IKCoded retval) => TryParse(encodedValue, out retval, out _);
        public static bool TryParse(string encodedValue, out IKCoded retval, out string errorResponse) {
            if (string.IsNullOrEmpty(encodedValue)) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = "Null or Empty value (" + encodedValue + ")";
                return false;
            }
            if (!TryCheckValidEncodedValue(encodedValue, out errorResponse)) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                return false;
            }
            retval = IKCoded.FromEncoded(encodedValue, alreadyValidated: true); // "skipAssertion: true" because we did check just now.
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
            return true;
        }

        [ClassMember(Description = "'Implementing' -" + nameof(ITypeDescriber) + "-.")]
        public static void EnrichKey(PK k) =>
            k.SetValidatorAndParser(new Func<string, ParseResult>(value =>
                TryParse(value, out var retval, out var errorResponse) ?
                    ParseResult.CreateOK(retval) :
                    ParseResult.CreateError(errorResponse)
            ));

        public class InvalidIKSafeException : ApplicationException {
            public InvalidIKSafeException(string message) : base(message) { }
            public InvalidIKSafeException(string message, Exception inner) : base(message, inner) { }
        }
    }
}