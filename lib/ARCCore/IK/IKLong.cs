﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCCore {
    [Class(
        Description =
            "A more memory efficient alternative to -" + nameof(IKString) + "-.\r\n" +
            "\r\n" +
            "In general it is preferred in AgoRapide to have integers as primary keys, " +
            "in order for this class (-" + nameof(IKLong) + "-) to be used for -" + nameof(IK) + "-.\r\n" +
            "(see for instance how -" + nameof(PropertyStreamLineParsed.TryParse) + "- and -" + nameof(IP.TryParseDtr) + "- " +
            "choose this class if possible).\r\n" +
            "\r\n" +
            "See -" + nameof(IKString) + "- for documentation.",
        LongDescription =
            "Note that in contrast to -" + nameof(IKString) + "-, this class does not offer any cache.\r\n" +
            "Use cached -" + nameof(IKString) + "- instead (-" + nameof(IKString.FromCache) + "-) for values that you know are limited in range."
    )]
    public class IKLong : IK, ITypeDescriber {

        public long Value { get; private set; }
        public IKLong(long value) => Value = value;

        /// <summary>
        /// NOTE: A naïve implementation would be this:
        /// NOTE: public override int GetHashCode() => (int)(Value % int.MaxValue);
        /// NOTE: But that would be a bug because hashcode must be identical between IK-instances, regardless of their type (IKString or IKLong for instance)
        /// NOTE: Correct implementation is thus like the one given below:
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode() => ToString().GetHashCode();
        public bool Equals(IK other) => other is IKLong l ? 
            l.Value.Equals(Value) : // Direct comparision against long Added 1 Mar 2022, should be much more efficient
            ToString().Equals(other.ToString());
        public override bool Equals(object other) => other is IK ik && Equals(ik);

        [ClassMember(Description =
            "It is quite natural in the code to write code like \"if (a.CustomerId == b.CustomerId)\", " +
            "therefore the need to implement == and != operators here."
        )]
        public static bool operator ==(IKLong a, IKLong b)
            => a.Equals(b);

        public static bool operator !=(IKLong a, IKLong b)
            => !a.Equals(b);

        // Parse / TryParse according to AgoRapide standard. Looks like overkill of course in our case
        public static IKLong Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new IKLongException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse + "\r\n");
        public static bool TryParse(string value, out IKLong retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out IKLong retval, out string errorResponse) {
            if (string.IsNullOrEmpty(value)) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = "Null or Empty value (" + value + ")";
                return false;
            }
            if (!long.TryParse(value, out var lng)) {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = "Invalid long (" + value + ")";
                return false;
            }
            retval = new IKLong(lng);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
            return true;
        }


        [ClassMember(Description = "'Implementing' -" + nameof(ITypeDescriber) + "-.")]
        public static void EnrichKey(PK k) =>
            k.SetValidatorAndParser(new Func<string, ParseResult>(value =>
                TryParse(value, out var retval, out var errorResponse) ?
                    ParseResult.CreateOK(retval) :
                    ParseResult.CreateError(errorResponse)
            ));

        public override string ToString() => Value.ToString();

        public class IKLongException : ApplicationException {
            public IKLongException(string message) : base(message) { }
            public IKLongException(string message, Exception inner) : base(message, inner) { }
        }
    }
}
