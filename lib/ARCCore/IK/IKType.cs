﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;

namespace ARCCore {

    [Class(
        Description =
            "Index key used when storing a collection of a given type, like 'Customer'.\r\n" +
            "\r\n" +
            "The key will normally point to a collection of all objects of that type.\r\n" +
            "(in RDBMS-terms pointing to a table like 'Customer')\r\n" +
            "\r\n" +
            "Note: Although the key will normally be like 'Customer', " +
            "the actual type of entity which it points to will be like 'CustomerCollection'.\r\n" +
            "\r\n" +
            "This class is immutable.",
        LongDescription =
            "Note that it is would be possible to let this class implement -" + nameof(ITypeDescriber) + "- " +
            "(by using -" + nameof(IP.AllIPDerivedTypesDict) + "- in a TryParse method) " +
            "but as of Mar 2021 no use case has been seeen yet " +
            "(for instance -" + nameof(IKString) + "- can be used instead)."
    )]
    public class IKType : IK {

        public Type Type { get; private set; }
        private IKType(Type type) => Type = type;

        private static readonly ConcurrentDictionary<Type, IKType> _cache = new ConcurrentDictionary<Type, IKType>();
        public static IKType FromType(Type type) => _cache.GetOrAdd(type, t => new IKType(t));

        public override int GetHashCode() => ToString().GetHashCode();
        public bool Equals(IK other) => ToString().Equals(other.ToString());
        public override bool Equals(object other) => other is IK ik && Equals(ik);

        private string? _toString;
        [ClassMember(Description =
            "Caching is supposed to be useful because may be called quite often.\r\n" +
            "Caching is acceptable because few instances of this will be created (little memory overhead in total)"
        )]
        public override string ToString() => _toString ??= Type.ToStringShort();
    }
}