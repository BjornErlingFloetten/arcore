﻿using System;
using System.Collections.Generic;
using System.Text;
using ARCCore;

namespace ARCCore
{
    [Class(
        Description =
            "A more memory efficient alternative to -" + nameof(IKString) + "- (together with -" + nameof(IKLong) + "-).\r\n" +
            "\r\n" +
            "Limited to whole days, string representation has format \"yyyy-MM-dd\" " +
            "(see -" + nameof(ValidDateTimeFormats) + "-).\r\n" +
            "\r\n" +
            "Note: Not recognized by -" + nameof(PropertyStreamLineParsed.TryParse) + "- or -" + nameof(IP.TryParseDtr) + "-.\r\n" +
            "\r\n" +
            "See -" + nameof(IKString) + "- for documentation.",
        LongDescription =
            "Note that in contrast to -" + nameof(IKString) + "-, this class does not offer any cache.\r\n" +
            "Use cached -" + nameof(IKString) + "- instead (-" + nameof(IKString.FromCache) + "-) for values that you know are limited in range."
    )]
    public class IKDate : IK, ITypeDescriber
    {

        public DateTime Value { get; private set; }
        public IKDate(DateTime value) => Value = value;

        /// <summary>
        /// NOTE: A naïve implementation would be this:
        /// NOTE: public override int GetHashCode() => (int)(Value.Ticks % int.MaxValue);
        /// NOTE: But that would be a bug because hashcode must be identical between IK-instances, regardless of their type (IKString or IKDate for instance)
        /// NOTE: Correct implementation is thus like the one given below:
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode() => ToString().GetHashCode();
        public bool Equals(IK other) => other is IKDate d ?
            d.Value.Equals(Value) :
            ToString().Equals(other.ToString());
        public override bool Equals(object other) => other is IK ik && Equals(ik);

        [ClassMember(Description =
            "It is quite natural in the code to write code like \"if (a.CustomerId == b.CustomerId)\", " +
            "therefore the need to implement == and != operators here."
        )]
        public static bool operator ==(IKDate a, IKDate b)
            => a.Equals(b);

        public static bool operator !=(IKDate a, IKDate b)
            => !a.Equals(b);

        [ClassMember(Description =
            "Note: Be very careful about expanding this array. ToString must be considered, and also how Equals should work " +
            "with other sub classes of IK."
        )]
        public static string[] ValidDateTimeFormats = new string[] {
            "yyyy-MM-dd"
        };

        // Parse / TryParse according to AgoRapide standard. Looks like overkill of course in our case
        public static IKDate Parse(string value) => TryParse(value, out var retval, out var errorResponse) ? retval : throw new IKDateException(nameof(value) + ": " + value + ", " + nameof(errorResponse) + ": " + errorResponse + "\r\n");
        public static bool TryParse(string value, out IKDate retval) => TryParse(value, out retval, out _);
        public static bool TryParse(string value, out IKDate retval, out string errorResponse)
        {
            if (string.IsNullOrEmpty(value))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = "Null or Empty value (" + value + ")";
                return false;
            }
            if (!DateTime.TryParseExact(value, ValidDateTimeFormats, UtilCore.Culture, System.Globalization.DateTimeStyles.None, out var d))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                errorResponse = "Invalid Date (" + value + "). Supported formats: " + string.Join(", ", ValidDateTimeFormats);
                return false;
            }
            retval = new IKDate(d);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
            return true;
        }


        [ClassMember(Description = "'Implementing' -" + nameof(ITypeDescriber) + "-.")]
        public static void EnrichKey(PK k) =>
            k.SetValidatorAndParser(new Func<string, ParseResult>(value =>
                TryParse(value, out var retval, out var errorResponse) ?
                    ParseResult.CreateOK(retval) :
                    ParseResult.CreateError(errorResponse)
            ));

        public override string ToString() => Value.ToString(ValidDateTimeFormats[0]);

        public class IKDateException : ApplicationException
        {
            public IKDateException(string message) : base(message) { }
            public IKDateException(string message, Exception inner) : base(message, inner) { }
        }
    }
}
