﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;

namespace ARCCore {

    [Class(
        Description =
            "IK = Interface Key / Index key. Key used to lookup 'inside' an instance of -" + nameof(IP) + "-.\r\n" +
            "\r\n" +
            "TODO: Consider renaming into IIK (Interface index key).\r\n" +
            "\r\n" +
            "Imlementing classes in -" + nameof(ARConcepts.StandardAgoRapideCode) + "- are:\r\n" +
            "-" + nameof(IKString) + "-: Index key for any general id, like primary-key,\r\n" +
            "-" + nameof(IKLong) + "-: A more memory efficient alternative to -" + nameof(IKString) + "-,\r\n" +
            "-" + nameof(IKType) + "-: Used when storing a collection of a given type,\r\n" +
            "-" + nameof(IIKII) + "-: Related to -" + nameof(PII) + "-, property for storing individual items,\r\n" +
            "-" + nameof(PK) + "-: PK = AgoRapide PropertyKey. Describes properties related to an object (an entity),\r\n" +
            "-NewKey-: (In -" + nameof(ARComponents.ARCQuery) + "-), A function creating new keys (new fields).\r\n" +
            "\r\n" +
            "The key is what describes the different elements in a -" + nameof(PropertyStreamLine) + "- line.\r\n" +
            "\r\n" +
            "Example for how different implementing classes are chosen for a specific -" + nameof(PropertyStreamLine) + "- like\r\n" +
            "'dt/Customer/42/PhoneNumber = 90534333':\r\n" +
            "\r\n" +
            "'dt' would correspond to -" + nameof(PSPrefix.dt) + "- (data). Parsed as -" + nameof(IKString) + "-\r\n" +
            "(if the data storage is implicitly storing only data then this prefix will often have been removed before parsing the rest of the keys).\r\n" +
            "\r\n" +
            "'Customer' would be a -" + nameof(IKType) + "-, that is a key pointing to a collection of all objects of type 'Customer'.\r\n" +
            "(in RDBMS-terms pointing to a table called Customers)\r\n" +
            "\r\n" +
            "'42' would be a -" + nameof(IKLong) + "- (-" + nameof(IKString) + "- if it had not been an integer), that is, a general identifier " +
            "for a specific entity.\r\n" +
            "(in RDBMS-terms a primary key)\r\n" +
            "\r\n" +
            "'PhoneNumber' would be a -" + nameof(PK) + "-, describing this specific field, and how the value '90534333' should be validated and parsed.\r\n" +
            "(in RDBMS-terms the Field in the schema for the Table)\r\n" +
            "\r\n" +
            "Instances implementing this interface is suitable for use as key in hierarchical object stores like -" + nameof(PRich) + "-.\r\n" +
            "\r\n" +
            "See -" + nameof(PropertyStreamLineParsed.TryParse) + "- for details about about how the different sub-classes of IK are utilizied " +
            "for a given -" + nameof(PropertyStreamLine) + "-.\r\n",
        LongDescription =
            "All implementations are supposed to be immutable.\r\n" +
            "\r\n" +
            "Some notes about IEquatable<IK>:\r\n" +
            "\r\n" +
            "Note how IEquatable<IK> must be implemented in the different implementing classes\r\n" +
            "(like -" + nameof(IKString.Equals) + "-, -" + nameof(PK.Equals) + "- and -" + nameof(IKLong.Equals) + "-).\r\n" +
            "\r\n" +
            "Equality is always based on the string representation (-" + nameof(IK.ToString) + "-) " +
            "because -" + nameof(IKString) + "- may be used instead of -" + nameof(IKType) + "- or -" + nameof(PK) + "- " +
            "(for instance depending on whether we have -" + nameof(ARConcepts.StandardAgoRapideCode) + "- or -" + nameof(ARConcepts.ApplicationSpecificCode) + "-).\r\n" +
            "(in other words, a -" + nameof(IKString) + "- must be seen as equal to a -" + nameof(IKType) + "- or even -" + nameof(PK) + "-.)\r\n" +
            "\r\n" +
            "Note how GetHashCode is also essential to implement. If not then dictionaries with this class as key will simply not work \r\n" +
            "(see -" + nameof(IKString.GetHashCode) + "-, -" + nameof(PK.GetHashCode) + "-, and -" + nameof(IKLong.GetHashCode) + "-).\r\n" +
            "\r\n" +
            "Like Equality, GetHashCode is also based on the string representation (-" + nameof(IK.ToString) + "-). Same reason as above.\r\n" +
            "\r\n" +
            "Note that default implementation of Equals(IK other) and GetHashCode() in this interface is not possible (because these methods are " +
            "implemented for any class anyway (as -" + nameof(Object.Equals) + "- / -" + nameof(object.GetHashCode) + "-), " +
            "so that would take precedence over a default interface method).\r\n" +
            "\r\n" +
            "Note that although this interface does not specify any maximum length of the string representation of the key (especially -" + nameof(IKString) +"-) " +
            "keys should be kept reasonable short in order to be practical in URL's and as file and folder names when storing on disk.\r\n" +
            "See -" + nameof(IKCoded) + "- which specifies a maximum of 250 characters in an encoded string representation of a key."
    )]
    public interface IK :
        /// Inheriting IEquatable for use by <see cref="IP"/>, using as key in dictionaries.
        IEquatable<IK>,
        // IComparer<IK>, TODO: Do we need to implement IComparer<IK>?

        /// Inheriting ITypeDescriber in order for <see cref="PP.Cid", <see cref="PP.Vid"/> and <see cref="PP.Iid"/>> and so on to parse correctly.
        /// NOTE: As of Feb 2021 this is appearantly not in use. Cid, Vid and Iid are still of type string.
        /// NOTE: and we can not use typeof(IK) because that can no be instantiated. We could use IKString, but then it would
        /// NOTE: be sufficient to let IKString implement ITypeDescriber.
        ITypeDescriber {

        /// <summary>
        /// This looks strange, but gets rid of compiler warnings against possible use default object's "string? ToString()" method.
        /// </summary>
        /// <returns></returns>
        public string ToString();

        T As<T>() where T : class, IK => this as T ?? throw new InvalidObjectTypeException(this, typeof(T),
            "This sub class of IK (" + this.GetType().ToStringShort() + ") " +
            "was not of the expected type " + typeof(T).ToStringShort()
        );

        // TODO: Do we need to implement IComparer<IK>?
        // int IComparer<IK>.Compare(IK x, IK y) => x.ToString().CompareTo(y.ToString());    
    }
}
