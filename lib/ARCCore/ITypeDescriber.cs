﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;

namespace ARCCore {

    [Class(
        Description =
            "Provides a standardized mechanism for describing single-property values (like 'PhoneNumber' of 'Customer' for instance).\r\n" +
            "\r\n" +
            "(The difference between -" + nameof(ITypeDescriber) + "- and -" + nameof(IP) + "- is that the former must be stored 'inside' an -" + nameof(IP) + "-.\r\n" +
            "In RDBMS-terms a -" + nameof(ITypeDescriber) + "- is the Field stored within a -" + nameof(IP) + "- which is the Post.)\r\n" +
            "\r\n" +
            "The most important functionality usually provided is validation and parsing in a standardized manner.\r\n" +
            "\r\n" +
            "This is done by a static method call 'EnrichKey' which is supposed to be available in a class 'implementing' this interface.\r\n" +
            "In this manner, types implementing this 'interface' is by this principle able to describe themselves to AgoRapide).\r\n" +
            "For a typical example, see -" + nameof(Subscription.EnrichKey) + "-.\r\n" +
            "\r\n" +
            "Note how this interface in itself is empty \r\n" +
            "(apart from -" + nameof(GetSyntaxHelp) + "- which is just a suggestion to use in your implementing classes.)\r\n" +
            "\r\n" +
            "See -" + nameof(PK.Initialize) + "- for where this 'interface' is actually used.\r\n" +
            "\r\n" +
            "EnrichKey should have the following signature:\r\n" +
            "  public static void EnrichKey(PK k)\r\n" +
            "\r\n" +
            "A typical method will set -" + nameof(PK.ValidatorAndParser) + "- although other attributes of -" + nameof(PK) + "- may also be changed " +
            "(which gives rise to the somewhat strange name for the method 'EnrichKey').\r\n" +
            "\r\n" +
            "(note that for very common types like string, int, long, bool, DateTime, TimeSpan, Type, Uri and so on " +
            "-" + nameof(PKTypeAttribute) + "- provides a -" + nameof(PKTypeAttribute.StandardValidatorAndParser) + "-.)\r\n" +
            "\r\n" +
            "A typical implementation can look like this:\r\n" +
            "\r\n" +
            "   public static void EnrichKey(PK k) {\r\n" +
            "      k.SetValidatorAndParser(new Func<string, ParseResult>(value =>\r\n" +
            "        TryParse(value, out var retval, out var errorResponse) ?\r\n" +
            "          ParseResult.CreateOK(retval) :\r\n" +
            "          ParseResult.CreateError(errorResponse);\r\n" +
            "      ));\r\n" +
            "  }\r\n" +
            "\r\n" +
            "The typical TryParse method in the implementation class should follow the ordinary .NET pattern " +
            "with the addition of a 'out string errorResponse' parameter which " +
            "can communicate WHY the value was not valid for parsing. " +
            "This helps a lot with error messages, making an API for instance much more user friendly.\r\n" +
            "\r\n" +
            "Note the the implementation class should also overrride object.ToString() to a format actually understood by the " +
            "ValidatorAndParser returned. This is necessary for the storage mechanism, like -" + nameof(ARConcepts.PropertyStream) + "- to work " +
            "(serialization and de-serialization of objects).",
        LongDescription =
            "See also -" + nameof(IP) + "- which describes multiple value properties (that is, 'entities' like Customer, Product, Order). " +
            "\r\n" +
            "Note that the use of a static method is a practical choice since it avoids AgoRapide having to instantiate the " +
            "class for what is essential a static one-off operation done only once (for instance at application initialization, or on-demand).\r\n" +
            "This however means that the compiler is unable to check if your class really implements 'EnrichKey' as 'promised' by the fact of it " +
            "being marked as implementing this interface. Missing implementations will show up at runtime with a helpful and explanatory error messages.\r\n" +
            "\r\n" +
            "Note that for some attributes you actually have a choice of where to specify them.\r\n" +
            "(example below assumes a PKDataAttribute-class with a property called 'InvalidValueExample')\r\n" +
            "TODO: FIND ETTER EXAMPLES WHEN SOME REAL BasePKAttribute subclasses are defined in AgoRapide\r\n" +
            "\r\n" +
            "You can specify either by\r\n" +
            "\r\n" +
            "1) Tagging directly the corresponding 'enum' field (enums of type -" + nameof(AREnumType.PropertyKeyEnum) + "-) with some -" + nameof(PKTypeAttribute) + "- " +
            "like:\r\n" +
            "\r\n" +
            "enum CustomerP {\r\n" +
            "  ...\r\n" +
            "  [PKType(Type=DateOfBirth)]\r\n" +
            "  [PKData(InvalidValueExample = \"1968-13-09\")]\r\n" +
            "  DateOfBirth,\r\n" +
            "  ...\r\n" +
            "}\r\n" +
            "\r\n" +
            "(but then you will have to do that everywhere you are using the implementing class (in this example the type 'DateOfBirth') because there will be a " +
            "separate -" + nameof(PK) + "- generated for each 'enum' field).  " +
            "\r\n" +
            "or " +
            "\r\n" +
            "2) by setting them when the resulting -" + nameof(PK) + "- is passed to EnrichKey (from wherever 'DateOfBirth' is used) " +
            "like (note, code is somewhat simplified):\r\n" +
            "\r\n" +
            "   public class DateOfBirth : ITypeDescriber {\r\n" +
            "      ...\r\n" +
            "      public static void EnrichKey(PK k) {\r\n" +
            "          var pkData = new PKDataAttribute();\r\n" +
            "          pkData.AddPV(PKDataAttributeP.InvalidValueExample.A(), \"1968-13-09\");\r\n" +
            "          k.AddP(IKType.FromType(pkData.GetType()), pkData);\r\n" +
            "      });\r\n" +
            "   }\r\n" +
            "\r\n" +
            "The latter method is supposedly better.\r\n" +
            "\r\n" +
            "Note that attributes like -" + nameof(PK.ValidatorAndParser) + "- can only be set through EnrichKey because they are " +
            "too complex for the -" + nameof(Attribute) + "- mechanism"
    )]
    public interface ITypeDescriber {
        // Implement public static method EnrichKey in your class. 

        // TODO: With default interface methods in C# 8.0, is it possible to actually have some code here in the interface now?

        public const string EnrichKeyMethodName = "EnrichKey";

        [ClassMember(Description =
            "Enables explanation of syntax in different contexts from one source:\r\n" +
            "\r\n" +
            "The actual syntax information is placed in a SyntaxHelp member like -" + nameof(Subscription.SyntaxHelp) + "-\r\n" +
            "\r\n" +
            "The syntax can then be published through different contexts like:\r\n" +
            "\r\n" +
            "1) In ordinary documentation as created by -" + nameof(ARComponents.ARCDoc) + "-\r\n" +
            "(this method is not involved)\r\n" +
            "\r\n" +
            "2) In run time error messages given to for instance API clients.\r\n" +
            "(involving this method)\r\n" +
            "\r\n" +
            "For variant 2) this method is typically called from a property method like -" + nameof(Subscription.SyntaxHelp) + "-. " +
            "This method (" + nameof(GetSyntaxHelp) + ") " +
            "will return the -" + nameof(BaseAttributeP.Description) + "- " +
            "of that member's -" + nameof(ClassAttribute) + ".\r\n" +
            "\r\n" +
            "Returns an empty string if no information found.\r\n"
        )]
        public static string GetSyntaxHelp(Type type, [System.Runtime.CompilerServices.CallerMemberName] string caller = "") {
            // TODO: Consider caching here, but remember that ClassMemberAttribute is cached anyway, so maybe of little value.
            var a = ClassMemberAttribute.GetAttribute(type, caller);
            if (a.IsDefault) return "";
            if (!a.IP.TryGetPV<string>(BaseAttributeP.Description, out var retval)) return "";
            if ("".Equals(retval)) return "";
            return "\r\nSyntax:\r\n" + retval;
        }
    }
}
