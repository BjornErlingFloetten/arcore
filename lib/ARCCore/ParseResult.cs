﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;

namespace ARCCore {

    /// <summary>
    /// Contains either <see cref="Result"/> or <see cref="ErrorResponse"/>
    /// </summary>
    [Class(Description =
        "NOTE: Concept is very old, from the dark ages of AgoRapide's very first inception. There might be room for improvement.\r\n" +
        "NOTE: (for instance by using a super-class and inheriting classes for OK and ERROR)."
    )]
    public class ParseResult {

        [ClassMember(Description =
            "Will be null if ErrorResponse is set.\r\n" +
            "If not derived from -" + nameof(IP) + "- then it will normally be packed inside a -" + nameof(PValue<TValue>) + "- afterwards (by mechanism using this class), " +
            "before final storage into an 'entity' object like -" + nameof(PRich) + "-"
        )]
        public object? Result { get; private set; }

        [ClassMember(Description = "Will be null if Result is set")]
        public string? ErrorResponse { get; private set; }

        public static ParseResult CreateOK(object objResult) => new ParseResult(objResult, null);
        public static ParseResult CreateError(string errorResponse) => new ParseResult(null, errorResponse);
        private ParseResult(object? objResult, string? errorResponse) {
            ErrorResponse = errorResponse;
            Result = objResult;
        }
    }
}