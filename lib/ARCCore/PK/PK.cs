﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using System.Linq;

// TODO: Why is all the parsing-functionality placed here? Is that natural?

namespace ARCCore
{
    [Class(
        Description =
            "PK = AgoRapide PropertyKey.\r\n" +
            "\r\n" +
            "TODO: Implement ToPropertyStream for this class since implements IP (like in -" + nameof(PValue<TValue>) + "-).\r\n" +
            "\r\n" +
            "Describes properties related to an object (an entity).\r\n" +
            "In RDBMS-terms it describes the field in the schema for the table, like 'Customer.FirstName' or 'Car.Colour')r\n" +
            "\r\n" +
            "Constitutes the basic element of -" + nameof(ARConcepts.TaggingOfPropertyKeys) + "-, " +
            "and gives the -" + nameof(ARCCore.IP) + "- mechanism the information it needs in order to implement -" + nameof(ARConcepts.PropertyAccess) + "-.\r\n" +
            "\r\n" +
            //"Generated either\r\n" +
            //"1) Directly at application startup through attribute tags written directly in the C# code, or\r\n" +
            //"2) Dynamically at run-time through mechanisms like -" + nameof(ARComponents.ARCCalc) + "-.\r\n" +
            //"\r\n" +
            "Contains collection of -" + nameof(BasePKAttribute) + "- instances which in turn contain the actual attributes like:\r\n" +
            "-" + nameof(PKTypeAttribute) + "-: Contains the most basic essential information about a property key.\r\n" +
            "-" + nameof(PKRelAttribute) + "-: Describes relationships between entities.\r\n" +
            "-" + nameof(PKHTMLAttribute) + "-: Contains pointer to HTML encoder for a property.\r\n" +
            "-" + nameof(PKLogAttribute) + "-: Explains how logging of changes to a property should be done.\r\n" +
            "These are accessed through -" + nameof(GetA) + "- / -" + nameof(TryGetA) + "-\r\n" +
            "\r\n" +
            "See also -" + nameof(ARConcepts.TaggingOfPropertyKeys) + "-.\r\n" +
            "\r\n" +
            "NOTE: This class is itself an instance of -" + nameof(ARCCore.IP) + "- " +
            "because we want to communicate contents of keys in a standardised manner. They can for instance be communicated to other " +
            "-" + nameof(ARNodeType) + "- through the -" + nameof(IP.ToPropertyStream) + "- mechanism.\r\n" +
            "\r\n" +
            "See also -" + nameof(AREnumType.PropertyKeyEnum) + "-."
    )]
    public class PK : PRich, IK
    {

        private readonly string _whyEnumIsNeededInConstructorExplanation =
                "The reason for why the constructor with 'object _enum' as parameter needs to be used is that " +
                "we have a chicken-and-egg problem related to the -" + nameof(ToString) + "- method.\r\n" +
                "ToString is the basis for -" + nameof(Equals) + "- and -" + nameof(GetHashCode) + "-.\r\n" +
                "This again is necessary for -" + nameof(TryGetA) + "- when looking for a -" + nameof(PKTypeAttribute) + "- " +
                "with the -" + EnumMemberAttributeP.EnumMember + "- which is what -" + nameof(ToString) + "- would use if 'object _enum' was not " +
                "passed as a parameter to the constructor...\r\n" +
                "\r\n" +
                "NOTE: This gives limitations regarding the possibility of distributing property keys over the -" + nameof(ARConcepts.PropertyStream) + "- " +
                "(if we ever should want to do that).";

        [ClassMember(Description =
            "This information is also found as " + nameof(EnumMemberAttributeP.EnumMember) + ".\r\n" +
            "\r\n" +
            "TODO: Rename if found to be permanently desired exposed as public.\r\n" +
            "\r\n" +
            "See also -" + nameof(TryGetEnum) + "-."
        )]
        public readonly object __enum;

        public T GetEnum<T>() where T : struct, Enum => TryGetEnum<T>(out var retval, out var errorResponse) ? retval : throw new PKException(errorResponse);
        [ClassMember(
            Description =
                "Note that usually you will not have to ask for this value, " +
                "usually in AgoRapide it goes the other way around, you have the enum member / enum value and want the corresponding property key.\r\n" +
                "Method is made generic in order to provide an informative exception message for any type inconsistencies.\r\n" +
                "\r\n" +
                "See also -" + nameof(__enum) + "-."
        )]
        public bool TryGetEnum<T>(out T retval, out string errorResponse) where T : struct, Enum
        {
            if (__enum is T t)
            {
                retval = t;
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
            retval = default;
            errorResponse =
                "Unable to return enum of type " + typeof(T).ToStringShort() + ".\r\n" +
                "This property key is of type " + __enum.GetType().ToStringShort() + " (" + __enum.GetType().ToStringShort() + "." + __enum.ToString() + ")\r\n";
            return false;
        }

        public PK() =>
            throw new PKException(
                "Illegal to call parameterless constructor for this class (" + nameof(PK) + ").\r\n" +
                "You need to use the constructor with 'object _enum' as parameter.\r\n" +
                "\r\n" +
                _whyEnumIsNeededInConstructorExplanation + "\r\n"
            );

        /// <summary>
        /// </summary>
        /// <param name="_enum">
        /// The actual enum member / enum value (like CustomerP.FirstName) that we are a collection of <see cref="BasePKAttribute"/>s for
        /// </param>
        [ClassMember(Description =
            "Constructor with 'object _enum' is necessary in order for ToString to work.\r\n" +
            "See " + nameof(_whyEnumIsNeededInConstructorExplanation) + " for more information.\r\n"
        )]
        public PK(object _enum)
        {
            NotOfTypeEnumException.AssertEnum(_enum.GetType(), () => "Parameter " + nameof(_enum) + " passed to constructor of " + nameof(PK) + ".");
            __enum = _enum;
        }

        [ClassMember(Description =
            "Only type of PK, string or type of __enum is allowed to ask for (as TRequest generic type parameter).\r\n" +
            "\r\n" +
            "This is a special override necessary when this class occurs as a property in a query table for instance, and " +
            "one wants the string representation. Our base class -" + nameof(PRich) + "- implements TryGetV by " +
            "looking for -" + nameof(PP.Value) + "- and will therefore never succeeed."
        )]
        public override bool TryGetV<TRequest>(out TRequest retval, out string errorResponse)
        {
            if (typeof(TRequest).Equals(typeof(string)))
            {
                retval = (TRequest)(object)ToString();
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
            else if (GetType().Equals(typeof(TRequest)))
            {
                // Special case, made in order for this override to work like base class
                // Will typically happen when the caller "should" have asked with TryGetP instead of TryGetPV against the containing entity.
                /// <see cref="PRich.TryGetV"/>
                retval = (TRequest)(object)this;
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
            else if (typeof(TRequest).Equals(__enum.GetType()))
            {
                retval = (TRequest)(object)__enum;
                errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return true;
            }
            else
            {
                errorResponse = "Invalid TRequest type parameter (" + typeof(TRequest).ToStringVeryShort() + "), use <string> or <" + __enum.GetType().ToStringVeryShort() + "> or <" + GetType().ToStringVeryShort() + ">";
                retval = default!;
                return false;
            }
        }

        [ClassMember(Description = "Convenience method for easy access to -" + nameof(BasePKAttribute) + "- with corresponding explaining exception if fails")]
        public T GetA<T>() where T : BasePKAttribute => TryGetA<T>(out var retval) ? retval : throw new BasePKAttributeNotFoundException<T>(this);
        [ClassMember(Description = "Convenience method for easy access to -" + nameof(BasePKAttribute) + "-.\r\n")]
        public bool TryGetA<T>(out T retval) where T : BasePKAttribute
        {
            if (!Properties.TryGetValue(IKType.FromType(typeof(T)), out var temp))
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/> 
                return false;
            }
            retval = (T)temp;
            return true;
        }

        private Type? _type;
        [ClassMember(
            Description =
                "Returns type from -" + nameof(PKTypeAttribute) + "-'s -" + nameof(PKTypeAttributeP.Type) + "-.\r\n" +
                "Convenience method because original syntax is just too complex (also, improves performance)."
        // TODO: Delete commented out code (introduced caching 28 Aug 2020, improves performance)
        //LongDescription =
        //    "Note how we still support potential update of value over -" + nameof(ARConcepts.PropertyStream) + "- (because we do not cache value). " +
        //    "NOTE: An alternative would be to remove AssertInitializationNotCompleteForAttributeAccess in PKTypeAttribute.Type, we could then just use " +
        //    "GetA<PKTypeAttribute>().Type (but then updating over -" + nameof(ARConcepts.PropertyStream) + "- would not be possible."
        )]
        public Type Type => _type ??= GetA<PKTypeAttribute>().IP.GetPV<Type>(PKTypeAttributeP.Type);

        private Cardinality? _cardinality;
        [ClassMember(
            Description =
                "Returns cardinality from -" + nameof(PKTypeAttribute) + "-'s -" + nameof(PKTypeAttributeP.Cardinality) + "-.\r\n" +
                "Convenience method because original syntax is just too complex (also, improves performance)."
        // TODO: Delete commented out code (introduced caching 28 Aug 2020, improves performance)
        //LongDescription =
        //    "Note how we still support potential update of value over -" + nameof(ARConcepts.PropertyStream) + "- (because we do not cache value). " +
        //    "NOTE: An alternative would be to remove AssertInitializationNotCompleteForAttributeAccess in PKTypeAttribute.IsObligatory, we could then just use " +
        //    "GetA<PKTypeAttribute>().IsObligatory (but then updating over -" + nameof(ARConcepts.PropertyStream) + "- would not be possible."
        )]
        public Cardinality Cardinality =>
            /// GetA<PKTypeAttribute>().IP.GetPVM<Cardinality>(); // Do not use PVM because we may be called from <see cref="Initialize"/>, that is, before mappings are ready for use.
            _cardinality ??= GetA<PKTypeAttribute>().IP.GetPV<Cardinality>(PKTypeAttributeP.Cardinality);

        private Type? _correspondingClass;
        [ClassMember(
             Description =
                 "Returns -" + nameof(EnumAttributeP.CorrespondingClass) + "- based on -" + nameof(__enum) + "-.\r\n" +
                 "\r\n" +
                 "TODO: Possible not needed. May be deleted."
        )]
        public Type CorrespondingClass => _correspondingClass ??= EnumAttribute.GetAttribute(__enum.GetType()).IP.GetPV<Type>(EnumAttributeP.CorrespondingClass);

        [Class(Description = "Explains how to implement a given -" + nameof(BasePKAttribute) + "- for an enum")]
        public class BasePKAttributeNotFoundException<T> : ApplicationException
        {
            public BasePKAttributeNotFoundException(PK pk) : base(
                "Attribute " + typeof(T).ToStringShort() + " not found for " + pk.ToString() + ".\r\n" +
                (
                    (typeof(T).Equals(typeof(PKTypeAttribute))) ?
                    (
                        "Actual enum member / enum value is " + pk.__enum.GetType().ToStringVeryShort() + "." + pk.__enum + ".\r\n" +
                        "\r\n" +
                        "Most probable cause (as of Jun 2020, due to weakness in AgoRapide):\r\n" +
                        "The assembly " + pk.__enum.GetType().Assembly.GetName().FullName + " was not referred to " +
                        "at call to -" + nameof(UtilCore.Assemblies) + "- at application initialization.\r\n" +
                        "\r\n" +
                        "Another probable cause is a bug in the AgoRapide framework, because this attribute should always be present.\r\n" +
                        "Note: Initialization of AgoRapide is a bit tricky because property keys contain property keys " +
                        "meaning that " + nameof(FromEnum) + " is inherently recursive.\r\n" +
                        "A good starting point for debugging would be to let " + nameof(FromEnum) + " log every call."
                    //+
                    //string.Join("\r\n", DebugSequence)
                    ) :
                    (
                        "Resolution: " +
                        "For the actual enum member / enum value " + pk.__enum.GetType().ToStringVeryShort() + "." + pk.__enum + " " +
                        "add '[" + typeof(T).ToStringShort().Replace("Attribute", "") + "( ... )]'."
                    )
                ) +
                "\r\n"
            )
            { }
        }

        private bool _isInitializing = false;
        public void Initialize()
        {
            _isInitializing = true;
            BasePKAttribute.GetAttributes(__enum).ForEach(a =>
            { /// Populate with <see cref="BasePKAttribute"/>
                IP.AddP(IKType.FromType(a.GetType()), a);
            });

            if (typeof(ITypeDescriber).IsAssignableFrom(Type))
            {
                /// Call EnrichKey method. 
                /// As a result <see cref="_validatorAndParser"/>, <see cref="_cleaner"/> and other values may be set
                /// through calls back to methods like <see cref="SetValidatorAndParser"/> and <see cref="SetCleaner"/>

                var methodName = ITypeDescriber.EnrichKeyMethodName; /// Note that <see cref="ITypeDescriber"/> itself is "empty", 
                try
                {
                    var method = Type.GetMethod(methodName, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static) ?? throw new InvalidTypeException(Type,
                        "The class " + Type.ToStringShort() + " does not implement the method\r\n" +
                        "   public static void " + methodName + "(" + typeof(PK).ToStringShort() + " key)\r\n" +
                        "but it should have according to the explanation (documentation) for interface " + typeof(ITypeDescriber).ToStringShort() + ".\r\n" +
                        "\r\n" +
                        "Resolution: Either add the method, or do not mark " + Type.ToStringShort() + " as implementing " + typeof(ITypeDescriber).ToStringShort() + ".\r\n" +
                        "Note: The compiler was unable to catch this problem because the actual method is supposed to be static and therefore really not part " +
                        "of the interface mechanism.\r\n" +
                        "\r\n" +
                        "Details: Problem originated from the tagging done for enum " + __enum.GetType().ToStringShort() + "." + __enum.ToString() + "\r\n"
                    );
                    // TODO: Implement signature check on method
                    method.Invoke(null, new object[] { this }); /// This will typically set <see cref="ValidatorAndParser"/> but other "enrichments" are also possible.
                }
                catch (Exception ex)
                {
                    throw new PKException(
                        "Unable to invoke for type " + Type.ToStringShort() + " the method\r\n" +
                        "   public static void " + methodName + "\r\n" +
                        "Resolution: Check that it exists and that it takes exactly one parameter of type " + typeof(PK).ToStringShort() + ".\r\n" +
                        "In other words it should look like\r\n\r\n" +
                        "   public static void " + methodName + "(" + typeof(PK).ToStringShort() + " key)\r\n" +
                        "\r\n" +
                        "Details: Problem originated from the tagging done for enum " + __enum.GetType().ToStringShort() + "." + __enum.ToString() + "\r\n",
                        inner: ex
                    );
                }
            }

            /// Set <see cref="_validatorAndParser"/>
            /// -------------------------------------------------------------------
            if (_validatorAndParser != null)
            {
                /// OK, was set by call to <see cref="ITypeDescriber"/>.EnrichKey above
            }
            else
            {

                // TODO: Delete commented out code
                //// Ask every BasePKAttribute about possible validator and parser.
                //var candidates = this.Select(ikip => ikip.P).Where(p => p is BasePKAttribute).Select(p => (type: p.GetType(), validatorAndParser: ((BasePKAttribute)p).StandardValidatorAndParser(Type))).Where(t => t.validatorAndParser != null).ToList();
                //switch (candidates.Count) {
                //    case 0: break; // None found
                //    case 1:

                //        // -----------------------------------------
                //        _validatorAndParser = candidates[0].validatorAndParser!; // Checked for null in where-clause above
                //        break;

                //    default:
                //        throw new PKException(
                //            "Multiple " + nameof(ValidatorAndParser) + " found for " + ToString() + ".\r\n" +
                //            "Several of the " + nameof(BasePKAttribute) + " implementations tagged for " + ToString() + " offered a " + nameof(ValidatorAndParser) + ".\r\n" +
                //            "Details: " + string.Join(", ", candidates.Select(t => t.type.ToStringShort())) + "\r\n" +
                //            "Possible resolution: Remove one or more of the " + nameof(BasePKAttribute) + " taggings.\r\n" +
                //            "Details: Problem originated from the tagging done for enum " + __enum.GetType().ToStringShort() + "." + __enum.ToString() + "\r\n"
                //        );
                //}

                _validatorAndParser = PKTypeAttribute.StandardValidatorAndParser(Type);

                if (_validatorAndParser == null)
                {
                    // Give up (return a validator and parser which throws an exception)

                    // TODO: Some ideas from old AgoRapide. Could be implemented in new BasePKAttribute-implementations.
                    //if (A.MustBeValidCSharpIdentifier) {
                    //    if (!typeof(string).Equals(A.Type)) throw new BaseAttribute.AttributeException(nameof(A.MustBeValidCSharpIdentifier) + " can only be combined with " + nameof(A.Type) + " string, not " + A.Type + ". Details: " + A.ToString());
                    //    ValidatorAndParser = value => InvalidIdentifierException.TryAssertValidIdentifier(value, out var errorResponse) ? ParseResult.Create(this, value) : ParseResult.Create(
                    //        "Invalid as C# identifier.\r\nDetails: " + errorResponse);
                    //} else 

                    //// TODO: Try something like a general TryParse through reflection
                    //// TODO: (See as TODO in PKTypeAttribute.StandardValidatorAndParser)

                    // Give up. Allow instantiation of PK (that is, do not throw exception now), but throw exception later if method tries to call ValidatorAndParser
                    _validatorAndParser = value =>
                    {
                        if (typeof(ITypeDescriber).IsAssignableFrom(Type))
                        {
                            throw new PKException(
                                nameof(ValidatorAndParser) + " for type " + Type.ToStringShort() + " not found.\r\n" +
                                "Possible cause: For type \r\n" + Type.ToStringShort() + " the method\r\n" +
                                "   public static void " + ITypeDescriber.EnrichKeyMethodName + "(PK key)\r\n" +
                                "did not set " +
                                "   key." + nameof(ValidatorAndParser) + "\r\n" +
                                "\r\n" +
                                "Details: Problem originated from the tagging done for enum " + __enum.GetType().ToStringShort() + "." + __enum.ToString() + "\r\n"
                            );
                        }
                        throw new PKException(
                            nameof(ValidatorAndParser) + " for type " + Type.ToStringShort() + " not found.\r\n" +
                            "Type was not a well-known type like int, long, Timespan ...\r\n" +
                            "\r\n" +
                            "Possible cause: Type '" + Type.ToStringShort() + "' does not implement -" + nameof(ITypeDescriber) + "-.\r\n" +
                            "\r\n" +
                            "Details: Problem originated from the tagging done for enum " + __enum.GetType().ToStringShort() + "." + __enum.ToString() + "\r\n"
                        );
                    };
                }
            }

            /// Set <see cref="_cleaner"/>
            /// -------------------------------------------------------------------
            if (_cleaner != null)
            {
                /// OK, was set by call to <see cref="ITypeDescriber"/>.EnrichKey above
            }
            else
            {

                // TODO: Delete commented out code
                // Ask every BasePKAttribute about possible cleaner
                //var candidates = this.Select(ikip => ikip.P).Where(p => p is BasePKAttribute).Select(p => (type: p.GetType(), cleaner: ((BasePKAttribute)p).StandardCleaner(Type))).Where(t => t.cleaner != null).ToList();
                //switch (candidates.Count) {
                //    case 0: break; // None found
                //    case 1:

                //        // -----------------------------------------
                //        _cleaner = candidates[0].cleaner!; // Checked for null in where-clause above
                //        break;

                //    default:
                //        throw new PKException(
                //            "Multiple " + nameof(Cleaner) + " found for " + ToString() + ".\r\n" +
                //            "Several of the " + nameof(BasePKAttribute) + " implementations tagged for " + ToString() + " offered a " + nameof(Cleaner) + ".\r\n" +
                //            "Details: " + string.Join(", ", candidates.Select(t => t.type.ToStringShort())) + "\r\n" +
                //            "Possible resolution: Remove one or more of the " + nameof(BasePKAttribute) + " taggings.\r\n" +
                //            "Details: Problem originated from the tagging done for enum " + __enum.GetType().ToStringShort() + "." + __enum.ToString() + "\r\n"
                //        );
                //}

                _cleaner = PKTypeAttribute.StandardCleaner(Type);

                if (_cleaner == null)
                {
                    // Set standard cleaner which just removes leading and trailing spaces
                    _cleaner = value => value.Trim();
                }
            }
            ModifyCleanerAndValidatorAndParserForCardinality();
            var description = GetA<PKTypeAttribute>().IP.TryGetPV<string>(BaseAttributeP.Description, out var retval) ? retval : "";
            if (!string.IsNullOrEmpty(description)) IP.AddPV(BaseAttributeP.Description, description); /// Note that taken from <see cref="PKTypeAttribute"/>. Practical approach, makes HTML drill-down easier to understand, because else description would be 'hidden' inside the PKTypeAttribute.
            var longDescription = GetA<PKTypeAttribute>().IP.TryGetPV<string>(BaseAttributeP.LongDescription, out retval) ? retval : "";
            if (!string.IsNullOrEmpty(longDescription)) IP.AddPV(BaseAttributeP.LongDescription, longDescription); /// Note that taken from <see cref="PKTypeAttribute"/>. Practical approach, makes HTML drill-down easier to understand, because else description would be 'hidden' inside the PKTypeAttribute.
            _isInitializing = false;
        }

        [ClassMember(
            Description =
                "Called from -" + nameof(Initialize) + "- after " + nameof(_cleaner) + " and " + nameof(_validatorAndParser) + " have been set.\r\n" +
                "This method modifies them according to -" + nameof(Cardinality) + "- for this -" + nameof(PK) + "-.\r\n" +
                "\r\n" +
                "Note how it makes sense for this class (-" + nameof(PK) + "-) to take care of -" + nameof(Cardinality) + "- " +
                "while the original -" + nameof(ITypeDescriber) + "- takes care of single values, " +
                "because -" + nameof(Cardinality) + "- is defined at -" + nameof(PK) + "--level and may differ between different -" + nameof(PK) + "-s.",
            LongDescription =
                "Note: Method is public only in order for nameof links to resolve in the documentation."
        )]
        public void ModifyCleanerAndValidatorAndParserForCardinality()
        {
            if (!_isInitializing) throw new PKException("!" + nameof(_isInitializing) + ". Method to be called only once, at initialization stage");
            switch (Cardinality)
            {
                case Cardinality.HistoryOnly:
                    // 'Normal' case, no modifications necessary
                    break;
                case Cardinality.WholeCollection:
                    // Modify in order to read a list of values and return the corresponding List<> value
                    var singleVP = _validatorAndParser ?? throw new NullReferenceException(nameof(_validatorAndParser));
                    var singleC = _cleaner ?? throw new NullReferenceException(nameof(_cleaner));
                    _cleaner = value => string.Join(";", value.Split(";").Select(s => singleC(s)));
                    _validatorAndParser = value =>
                    {
                        var t = value.Split(";").Select(s => PropertyStreamLine.Decode(s)).Select(s => singleVP(s)).ToList();
                        if (t.All(r => r.ErrorResponse == null))
                        {
                            var at = new Type[] { Type };
                            var listType = typeof(List<>).MakeGenericType(at);
                            var list = Activator.CreateInstance(listType);

                            // Add each element to list created
                            // ---
                            // Discarded attempt (requires probably reference to CSharp and looks too complex)
                            //var dynamicList = (dynamic)list;
                            //t.ForEach(r => dynamicList.Add(r.Result?.GetV<object>())); // TODO: Probably very inefficient code. Does this work?
                            // ---
                            // Chosen method, dynamically invoke method:
                            var adder = GetType().GetMethod(nameof(Adder), System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static)?.MakeGenericMethod(at) ?? throw new NullReferenceException("Method '" + nameof(Adder) + "' not found inside " + GetType().ToStringShort());
                            t.ForEach(r =>
                            {
                                adder.Invoke(null, new object[] { list, r.Result ?? throw new NullReferenceException(nameof(r.Result)) });
                            });

                            var pValueType = typeof(PValue<>).MakeGenericType(new Type[] { listType });
                            var pValue = Activator.CreateInstance(pValueType, new object[] { list });
                            return ParseResult.CreateOK(pValue as IP ?? throw new InvalidObjectTypeException(pValue, typeof(IP)));
                        }
                        return ParseResult.CreateError(
                            (t.All(r => r.ErrorResponse != null) ?
                                "None of the " + t.Count + " values given where valid" :
                                (t.Count(r => r.ErrorResponse != null) + " of the " + t.Count + " values given where invalid")
                            ) +
                            "\r\n" +
                            "The error responses are:\r\n" +
                            string.Join("\r\n", t.Where(r => r.ErrorResponse != null).Select(r => r.ErrorResponse))
                        // Note how the returned message may be quite long now
                        // We could consider limiting to a set number here.
                        );
                    };
                    break;
                case Cardinality.IndividualItems:
                    // There is nothing to be done at this stage.
                    // At actual time of parsing though, we must take into account that the value is actually stored as a key.
                    break;
                // throw new InvalidEnumException(Cardinality, "Not supported yet / not decided how to support");
                default:
                    throw new InvalidEnumException(Cardinality);
            }
        }

        [ClassMember(Description = "Helper method used by -" + nameof(ValidatorAndParser) + " for -" + nameof(Cardinality.WholeCollection) + "-")]
        private static void Adder<T>(List<T> list, T item) => list.Add(item);

        private string? _toString;
        [ClassMember(Description =
            "Caching is supposed to be useful because may be called quite often.\r\n" +
            "Caching is acceptable because few instances of this will be created (little memory overhead in total).\r\n" +
            "\r\n" +
            "Note that " + nameof(ToString) + " is only based on " + nameof(__enum) + ", " +
            "meaning that two enum members / enum values with same name but different type, like CustomerP.Name and OrderP.Name, will be considered equal.\r\n" +
            "This corresponds to a general keep-it-simple approach in AgoRapide (as short keys as possible)."
        )]
        public override string ToString() => _toString ??= __enum.ToString();

        public void SetValidatorAndParser(Func<string, ParseResult> value) => _validatorAndParser = value;
        private Func<string, ParseResult>? _validatorAndParser;
        [ClassMember(
            Description =
                "Validates and parses a value.\r\n" +
                 "\r\n" +
                "The reason for having a (standard) validator and parser is to ensure consistency with handling of input values throughout your application. " +
                "This both enhances the security and the user friendliness of your application, and makes it easier to debug because any error conditions are clearly " +
                "stated and explained.\r\n" +
                "\r\n" +
                "Originates from either:\r\n" +
                "1) The EnrichKey-implementation in " + nameof(ITypeDescriber) + " (that is, as tailor made for the actual class) or \r\n" +
                "2) As a standard validator and parser from a -" + nameof(BasePKAttribute) + "- " +
                "(for instance " + nameof(PKTypeAttribute.StandardValidatorAndParser) + " offers one for standard types like " +
                "string, int, long, bool, DateTime, TimeSpan, Type, Uri, and similar).\r\n" +
                "\r\n" +
                "If not found neither from 1) nor 2), then -" + nameof(Initialize) + "- will set a dummy value which will throw an exception upon attempted use (the null check below is really irrelevant)",
            LongDescription =
                "Note how -" + nameof(PK.Initialize) + "- modifies the value to take into account -" + nameof(ARCCore.Cardinality) + "-.\r\n" +
                "\r\n" +
                "See also -" + nameof(Cleaner) + "-."
        )]
        public Func<string, ParseResult> ValidatorAndParser => _validatorAndParser ?? throw new NullReferenceException(
            nameof(_validatorAndParser) + ": Should have been set by " + nameof(Initialize) + " (at least it should have set a dummy value which will throw an exception upon attempted use).\r\n");

        public void SetCleaner(Func<string, string> value) => _cleaner = value;
        private Func<string, string>? _cleaner;
        [ClassMember(Description =
            "Cleanup of values, to be used before value is attempted validated.\r\n" +
            "\r\n" +
            "The reason for having a cleaner is to make the final user interface more user friendly. " +
            "A typical error like having leading or trailing spaces in a user entered value should not result in any error message being generated, but the offending " +
            "spaces should simply be string.Trim'ed away.\r\n" +
            "\r\n" +
            "Originates from either:\r\n" +
            "1) The EnrichKey-implementation in " + nameof(ITypeDescriber) + " (that is, as tailor made for the actual class) or \r\n" +
            "2) As a standard cleaner from a -" + nameof(BasePKAttribute) + "- (typically from " + nameof(PKTypeAttribute.StandardCleaner) + ").\r\n" +
            "If not found neither from 1) nor 2), then- -" + nameof(Initialize) + "- will set a standard cleaner which will use string.Trim() (the null check below is really irrelevant)",
            LongDescription =
                "Note how -" + nameof(PK.Initialize) + "- modifies the value to take into account -" + nameof(ARCCore.Cardinality) + "-.\r\n" +
                "\r\n" +
                "See also -" + nameof(ValidatorAndParser) + "-."
        )]
        public Func<string, string> Cleaner => _cleaner ?? throw new NullReferenceException(nameof(_cleaner) + ": Should have been set by " + nameof(Initialize) + " to 's => s.Trim()'.\r\n");

        public IP CleanParseAndPack(string value) => TryCleanParseAndPack(value, out var retval, out var errorResponse) ? retval : throw new ArgumentException("Parsing failed for value '" + value + "'.\r\nDetails: " + errorResponse);
        public bool TryCleanParseAndPack(string value, out IP ip, out string errorResponse)
        {
            if (!TryCleanValidateAndParse(value, out var retval))
            {
                ip = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = retval.ErrorResponse ?? throw new NullReferenceException(nameof(retval.ErrorResponse) + ": Should be guaranteed not null here since Try... failed");
                return false;
            }
            ip = PackParseResultForStorageInEntityObject(retval);
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public T CleanAndParse<T>(string value)
        {
            if (!TryValidateAndParse(value, out var result)) throw new ArgumentException("Parsing failed for value '" + value + "'.\r\nDetails: " + result.ErrorResponse);
            if (!typeof(T).IsAssignableFrom(result.Result!.GetType())) throw new InvalidObjectTypeException(result.Result, typeof(T), "Parsing succeeded for value '" + value + "' but value was not of type " + typeof(T).ToStringShort());
            return (T)result.Result;
        }

        [ClassMember(
            Description =
                "Parses the value according to specifications for this property key.\r\n" +
                "Note: Method is a bit cumbersome, -" + nameof(TryCleanParseAndPack) + "- or -" + nameof(CleanParseAndPack) + "- are probably better alternatives"
        )]
        public bool TryValidateAndParse(string value, out ParseResult result)
        {
            result = ValidatorAndParser(value);
            if (result.ErrorResponse != null) result = ParseResult.CreateError("Validation failed for " + ToString() + " = '" + value + "'.\r\nDetails: " + result.ErrorResponse);
            if (result.ErrorResponse == null)
            {
                if (result.Result == null) throw new NullReferenceException("Result must be set when ErrorResponse is not set. Details: " + ToString() + " = '" + value + "'.\r\n");
            }
            return result.ErrorResponse == null;
        }

        /// <summary>
        /// </summary>
        /// <param name="value"></param>
        /// <param name="result">Will always be set, regardless of TRUE / FALSE as return value</param>
        /// <returns></returns>
        [ClassMember(
            Description =
                "Cleans and parses the value according to specifications for this property key.\r\n" +
                "Note: Method is a bit cumbersome, -" + nameof(TryCleanParseAndPack) + "- or -" + nameof(CleanParseAndPack) + "- are probably better alternatives"
        )]
        public bool TryCleanValidateAndParse(string value, out ParseResult result)
        {
            var cleanedValue = Cleaner(value);
            var retval = TryValidateAndParse(cleanedValue, out result);
            if (!retval && !cleanedValue.Equals(value))
            {
                result = ParseResult.CreateError(result.ErrorResponse + ".\r\nValue was unsuccessfully first cleaned up from original value '" + value + "' (but to no avail)");
            }
            return retval;
        }

        [ClassMember(
            Description =
                "Packs result of parsing into a -" + nameof(IP) + "--derived class, suitable for sending to -" + nameof(ARCCore.IP.AddOrUpdateP) + "-.\r\n" +
                "Note that if result is already derived from -" + nameof(IP) + "- then it is used directly.\r\n" +
                "Note: Method is a bit cumbersome to call from outside, -" + nameof(TryCleanParseAndPack) + "- or -" + nameof(CleanParseAndPack) + "- are probably better alternatives.\r\n" +
                "\r\n" +
                "Note that currently -" + nameof(PValue<TValue>) + "- is used for packing (unless result is already -" + nameof(IP) + "- as already mentioned). " +
                "\r\n" +
                "Corresponding 'unpacking' is done by -" + nameof(IP.TryGetV) + "-.\r\n" +
                "\r\n" +
                "A possible future expansion of AgoRapide capabilities could be to introduce, through the -" + nameof(BasePKAttribute) + "- mechanism, " +
                "a flexible choice of how to pack the result, instead of always using -" + nameof(PValue<TValue>) + "-. " +
                "(We could for instance introduce a 'StandardPacker' in -" + nameof(BasePKAttribute) + "- " +
                "just like we today have -" + nameof(PKTypeAttribute.StandardValidatorAndParser) + "- and -" + nameof(PKTypeAttribute.StandardCleaner) + "-. " +
                "If no suck packer is found, we could use a default packer that packs inside -" + nameof(PValue<TValue>) + "-).\r\n" +
                "\r\n" +
                "(This is the reason this method is not made static, in order to hint about this possibility and not break existing code if it ever gets introduced)"
        )]
        public // Do not make this method static! (Read reason in Description above).
            IP PackParseResultForStorageInEntityObject(ParseResult parseResult) =>
            PackObjectForStorageInEntityObject(parseResult.Result ?? throw new NullReferenceException(
                nameof(parseResult.Result) + ": This method can only be called for a valid " + nameof(parseResult) + "\r\n"));

        [ClassMember(Description = "See -" + nameof(PackParseResultForStorageInEntityObject) + "- for documentation.")]
        public /// Do not make this method static! (Read reason in Description above for <see cref="PackParseResultForStorageInEntityObject"/>
            IP PackObjectForStorageInEntityObject(object o)
        {
            if (o is null) throw new NullReferenceException(nameof(o));
            if (o is IP ip) return ip;
            /// PValue is generic, but we do not have the generic type parameter available here. Create generic type now instead.
            /// NOTE: We would really like to have ParseResult.Result (given as o here) as a generic class, so we could just return
            /// NOTE: new PValue(o) or similar, but we can not transport the generic type parameter throughout the processs
            /// NOTE: so therefore we have to resort to the following:
            return (IP)Activator.CreateInstance(
                typeof(PValue<>).GetGenericTypeDefinition().MakeGenericType(new Type[] { o.GetType() }), // Create generic type PValue<T>
                new object[] { o } // Generic parameter to PValue<T>
            );
        }

        /// <summary>
        /// Key is enum type which is mapped from. 
        /// Value is dictionary with values for each enum member / enum value again. 
        /// </summary>
        private static readonly ConcurrentDictionary<Type, ConcurrentDictionary<int, PKBoolPair>> _fromEnumCache = new ConcurrentDictionary<Type, ConcurrentDictionary<int, PKBoolPair>>();

        //public static List<string> DebugSequence = new List<string>();
        //public static int DebugSequenceCounter = 0;
        /// <summary>
        /// First call to this method should be made in a single-threaded context
        /// </summary>
        /// <param name="_enum">
        /// This is of type 'object' instead of a generic type, in order to work with 
        /// <see cref="IP.TryGetPV"/>
        /// </param>
        /// <returns></returns>
        [ClassMember(Description =
            "Returns the corresponding -" + nameof(PK) + "- instance from the given -" + nameof(AREnumType.PropertyKeyEnum) + "-.\r\n" +
            "\r\n" +
            "TODO: Introduce TryGetFromEnum, and rename this FromEnum to GetFromEnum."
        )]
        public static PK FromEnum(object _enum)
        {
            // TODO: There are some unresolved threading issues here. Although we use concurrent dictionaries, 
            // TODO: the result of the value factory may be discarded, and since we create PK-objects when creating PK-objects,
            // TODO: it as assumed that multiple 'instances' of the same PK will be created (potential creating trouble
            // TODO: for later storage).
            // TODO: (not speaking of the inner / outer setup, and duplicate (two sets))

            //DebugSequenceCounter++;
            //DebugSequence.Add(DebugSequenceCounter.ToString("000") + ": Asking for " + _enum.GetType().ToStringVeryShort() + "." + _enum);

            NotOfTypeEnumException.AssertEnum(_enum.GetType());
            var retval = _fromEnumCache.GetOrAdd(_enum.GetType(), t => new ConcurrentDictionary<int, PKBoolPair>()).GetOrAdd((int)(object)_enum, i =>
            {
                return new PKBoolPair(new PK(_enum)); /// Note delayed initialiation, returned object is "empty" without any <see cref="BasePKAttribute" collection.
                //
                /// NOTE: We can not initialize "inside" this value factory, because when calling <see cref="BasePKAttribute.GetAttributes"/> 
                /// NOTE: new calls will be made to <see cref="FromEnum{T}(T)"/> 
                /// NOTE: (Because of the following code inside <see cref="BasePKAttribute.GetAttributes"/>
                /// NOTE:    a.IP.AddPV(EnumMemberAttributeP.EnumType, _enum.GetType());
                /// NOTE:    a.IP.AddPV(EnumMemberAttributeP.EnumMember, _enum.ToString());
                /// NOTE: )
                /// NOTE: And if the new call is the same as the one we are already serving (inside this value factory), 
                /// NOTE: then this will lead to a infinite loop / stack overflow (because the value has not been placed in the dictionary yet)
                /// NOTE: We therefore have to wait AFTER dictionary has been populated with the value with populating with <see cref="BasePKAttribute"/> collection.
            });
            if (!retval.IsInitialized)
            {
                try
                {
                    REx.Inc(); // Calling initialize is inherently recursive because property keys contain property keys.
                    /// Now retval has been placed into cache. 
                    /// Is is therefore safe to call <see cref="BasePKAttribute.GetAttributes(object)"/> again because
                    /// when code below results in another call being made to us, the requested value will be returned immediately from cache, 
                    /// (in other words, this code will not be called again in a infinite loop resulting in stack overflow).

                    // First we have to stop next call from also trying to initialize
                    retval.IsInitialized = true;
                    // DebugSequence.Add(DebugSequenceCounter.ToString("000") + ": Initialize START for " + _enum.GetType().ToStringVeryShort() + "." + _enum);
                    retval.Pk.Initialize(); /// Will call <see cref="BasePKAttribute.GetAttributes(object)"/>
                    retval.TrulyInitialized = true;
                    // DebugSequence.Add(DebugSequenceCounter.ToString("000") + ": Initialize FINISH for " + _enum.GetType().ToStringVeryShort() + "." + _enum);
                }
                finally
                {
                    REx.Dec();
                }
            }
            // DebugSequence.Add(DebugSequenceCounter.ToString("000") + ": Success for " + _enum.GetType().ToStringVeryShort() + "." + _enum + ", TrulyInitialized: " + retval.TrulyInitialized);
            return retval.Pk;
        }

        /// <summary>
        /// 
        /// Typically use
        /// </summary>
        [ClassMember(
            Description =
                "Key is string like 'Customer.FirstName' (entity-type plus enum member / enum value).\r\n" +
                "Note that also includes possible metadata from -" + nameof(PP) + "- like Customer.Created or Customer.Invalid.\r\n" +
                "\r\n" +
                "See -" + nameof(BuildFromStringCache) + "- for documentation.\r\n" +
                "\r\n" +
                "Whole cache is built at application startup by -" + nameof(BuildFromStringCache) + "-.\r\n" +
                "\r\n" +
                "Planned future functionality (as of Mar 2020) is to also add values by subscribing to -" + nameof(PK) + "- instances " +
                "being distributed over -" + nameof(ARConcepts.PropertyStream) + "-.\r\n" +
                "Therefore this object is of type ConcurrentDictionary, not Dictionary"
        )]
        private static readonly ConcurrentDictionary<string, PK> _fromStringCache = new ConcurrentDictionary<string, PK>();
        /// <summary>
        /// </summary>
        /// <param name="entityType">For instance typeof(Customer)</param>
        /// <param name="field">For instance "FirstName"</param>
        /// <param name="retval"></param>
        /// <returns></returns>
        [ClassMember(Description =
            "Returns property key based on type and field name.\r\n" +
            "\r\n" +
            "Will also return, in addition to the tightly coupled keys like how CustomerP belongs to Customer, " +
            "the metadata -" + nameof(PP) + "- property keys."
        )]
        public static bool TryGetFromTypeAndFieldName(Type entityType, string field, out PK retval) => _fromStringCache.TryGetValue(entityType.ToStringVeryShort() + "." + field, out retval);

        public static bool TryGetFromTypeAndFieldName(Type entityType, string field, out PK retval, out string errorResponse)
        {
            if (_fromStringCache.TryGetValue(entityType.ToStringVeryShort() + "." + field, out retval))
            {
                errorResponse = null!; /// <see cref="ARConcepts."/>
                return true;
            }
            retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            errorResponse = "Unable to find " + nameof(PK) + " for " + entityType.ToStringShort() + "." + field;
            return false;
        }

        /// <summary>
        /// </summary>
        /// <param name="strEntityType">
        /// For instance "Customer"
        /// Entity-type like given in <see cref="ARConcepts.PropertyStream"/> (as result of <see cref="Extensions.ToStringVeryShort(Type)"/>)
        /// </param>
        /// <param name="field">For instance "FirstName"</param>
        /// <param name="retval"></param>
        /// <returns></returns>
        [ClassMember(Description =
            "Returns property key based on type and field name.\r\n" +
            "\r\n" +
            "Will also return, in addition to the tightly coupled keys like how CustomerP belongs to Customer, " +
            "the metadata -" + nameof(PP) + "- property keys."
        )]
        public static bool TryGetFromTypeAndFieldName(string strEntityType, string field, out PK retval) => _fromStringCache.TryGetValue(strEntityType + "." + field, out retval);

        /// <summary>
        /// </summary>
        /// <param name="entityTypePlusFieldName">
        /// Use format like 'Customer.FirstName' 
        /// 
        /// Explanation:
        ///   Entity-type like given in <see cref="ARConcepts.PropertyStream"/> (as result of <see cref="Extensions.ToStringVeryShort(Type)"/>) 
        /// plus 
        ///   literally "." (fulll stop)
        /// plus
        ///   name of actual enum member / enum value
        /// </param>
        /// <param name="retval"></param>
        /// <returns></returns>
        [ClassMember(Description =
            "Returns property key based on type and field name.\r\n" +
            "\r\n" +
            "Will also return, in addition to the tightly coupled keys like how CustomerP belongs to Customer, " +
            "the metadata -" + nameof(PP) + "- property keys."
        )]
        public static bool TryGetFromTypeAndFieldName(string entityTypePlusFieldName, out PK retval) => _fromStringCache.TryGetValue(entityTypePlusFieldName, out retval);

        [ClassMember(Description =
            "Planned future functionality (as of Mar 2020) is to also add values by subscribing to -" + nameof(PK) + "- instances " +
            "being distributed over -" + nameof(ARConcepts.PropertyStream) + "-.\r\n" +
            "Therefore this object is of type ConcurrentDictionary, not Dictionary"
        )]
        private static readonly ConcurrentDictionary<Type, List<PK>> _allPKForEntityTypeCache = new ConcurrentDictionary<Type, List<PK>>();
        [ClassMember(
            Description =
                "Returns all property keys relevant for the given entity type, \r\n" +
                "that is, all -" + nameof(PK) + "- constructed from -" + nameof(ClassAttributeP.CorrespondingEnumType) + "- for a class.\r\n" +
                "\r\n" +
                "The keys are returned in the order they appear as enum member.\r\n" +
                "\r\n" +
                "Remember that in addition to the tightly coupled properties like how CustomerP belongs to Customer, " +
                "the metadata -" + nameof(PP) + "- keys are also relevant for adding to many objects.\r\n" +
                "This metadata keys does not apply here though (they are not included in the returned enumeration).\r\n" +
                "\r\n" +
                "Returns an empty collection if no property keys found.\r\n" +
                "\r\n" +
                "Note that depends on -" + nameof(BuildFromStringCache) + "- having been called already, " +
                "something which should have happened at application startup.\r\n"
        )]
        public static IEnumerable<PK> GetAllPKForEntityType(Type entityType) => _allPKForEntityTypeCache.TryGetValue(entityType, out var retval) ? retval :
            new List<PK>(); // Change 27 Aug 2020 (would for instance fail for PRich as was)
                            //throw new PKException(
                            //nameof(_allPKForEntityTypeCache) + " does not contain an entry for " + entityType + ".\r\n" +
                            //"Possible cause: The class " + entityType + " is not an AgoRapide -" + nameof(IP) + "--derived class  " +
                            //"with a corresponding enum called -" + entityType + "P\r\n"
                            //);

        [ClassMember(Description =
            "Planned future functionality (as of Mar 2020) is to also add values by subscribing to -" + nameof(PK) + "- instances " +
            "being distributed over -" + nameof(ARConcepts.PropertyStream) + "-.\r\n" +
            "Therefore this object is of type ConcurrentDictionary, not Dictionary"
        )]
        private static readonly ConcurrentDictionary<string, List<PK>> _allPKForEntityTypeStringCache = new ConcurrentDictionary<string, List<PK>>();
        /// <summary>
        /// </summary>
        /// <param name="entityType">Should be should be the result of <see cref="Extensions.ToStringVeryShort(Type)/></param>
        /// <returns></returns>
        [ClassMember(
            Description =
                "Returns all property keys relevant for the given entity type, \r\n" +
                "that is, all -" + nameof(PK) + "- constructed from -" + nameof(ClassAttributeP.CorrespondingEnumType) + "- for a class.\r\n" +
                "The string representation of the entity type (the entity type parameter) should be the result of -" + nameof(Extensions.ToStringVeryShort) + "-." +
                "\r\n" +
                "Remember that in addition to the tightly coupled properties like how CustomerP belongs to Customer, " +
                "the metadata -" + nameof(PP) + "- keys are also relevant for adding to many objects.\r\n" +
                "This metadata keys does not apply here though (they are not included in the returned enumeration).\r\n" +
                "\r\n" +
                "Returns an empty collection if no property keys found.\r\n" +
                "\r\n" +
                "Note that depends on -" + nameof(BuildFromStringCache) + "- having been called already, " +
                "something which should have happened at application startup.\r\n"
        )]
        public static IEnumerable<PK> GetAllPKForEntityTypeString(string entityType) => _allPKForEntityTypeStringCache.TryGetValue(entityType, out var retval) ? retval :
            new List<PK>(); // Change 27 Aug 2020 (would for instance fail for PRich as was)
                            //throw new PKException(
                            //nameof(_allPKForEntityTypeCache) + " does not contain an entry for " + entityType + ".\r\n" +
                            //"Possible cause: The class " + entityType + " is not an AgoRapide -" + nameof(IP) + "--derived class  " +
                            //"with a corresponding enum called -" + entityType + "P\r\n"
                            //);

        [ClassMember(
            Description =
                "Builds the content of -" + nameof(_fromStringCache) + "- and also -" + nameof(_allPKForEntityTypeCache) + "-.\r\n" +
                "Note also caches the metadata in -" + nameof(PP) + "- for all other classes.\r\n" +
                "TODO: Explain better.\r\n" +
                "\r\n" +
                "Should be called at application startup (See -" + nameof(UtilCore.Assemblies) + "-).")]
        public static void BuildFromStringCache()
        {
            // Do not store duplicates of metadata keys (duplicates of PP's) for each and every other enum
            // Therefore build collection now 
            // TODO: We still store two of each of them, one for PP (IP), one shared between all other classes.
            var PKPP = UtilCore.EnumGetMembers<PP>().Select(e => FromEnum(e)).ToList();
            // TODO: EXPLAIN THIS BETTER

            UtilCore.Assemblies.SelectMany(a => a.GetTypes()).Where(t =>
            {
                if (!t.IsEnum) return false;
                if (EnumAttribute.GetAttribute(t).IP.GetPV<AREnumType>(EnumAttributeP.AREnumType) != AREnumType.PropertyKeyEnum) return false;
                return true;
            }).ForEach(t =>
            {
                var correspondingClass = EnumAttribute.GetAttribute(t).IP.GetPV<Type>(EnumAttributeP.CorrespondingClass);
                /// Note that existence of this class has been asserted by <see cref="EnumAttribute.GetAttribute"/>
                UtilCore.EnumGetMembers(t).ForEach(e =>
                {
                    var key = correspondingClass.ToStringVeryShort() + "." + e.ToString();
                    if (_fromStringCache.ContainsKey(key)) throw new PKException(
                        "Duplicate key (" + key + ").\r\n" +
                        "Possible cause: Two classes both resolving to the same " + nameof(Extensions.ToStringVeryShort) + "-representation" +
                        "\r\n");
                    _fromStringCache[key] = FromEnum(e);
                });
                if (t.Equals(typeof(PP)))
                {
                    // No meaning in having metadata for class P itself (besides, names would collide below)
                }
                else
                {
                    PKPP.ForEach(e =>
                    {
                        var key = correspondingClass.ToStringVeryShort() + "." + e.__enum.ToString();
                        if (_fromStringCache.ContainsKey(key))
                        {
                            /// For <see cref="PExact{EnumType}"/> we have problems with this.
                            /// We need to duplicate metadata in the Exact-class' enum.
                            return;
                            //throw new PKException(
                            //"Duplicate key (" + key + ") found when attempting to cache metadata keys.\r\n" +
                            //"Possible cause: " + t.ToStringVeryShort() + "." + e.__enum.ToString() + " is defined.\r\n" +
                            //"Resolution: Delete " + t.ToStringVeryShort() + "." + e.__enum.ToString() + " and use " + typeof(PP).ToStringVeryShort() + "." + e.__enum.ToString() + " instead" +
                            //"\r\n");
                        }
                        _fromStringCache[key] = e;
                    });
                }
                _allPKForEntityTypeCache[correspondingClass] = UtilCore.EnumGetMembers(t).Select(e => FromEnum(e)).ToList();
            });

            _allPKForEntityTypeCache.ForEach(t =>
            {
                _allPKForEntityTypeStringCache.TryAdd(t.Key.ToStringVeryShort(), t.Value);
                _allPKForEntityTypeStringCache.TryAdd(t.Key.ToStringShort(), t.Value);
                _allPKForEntityTypeStringCache.TryAdd(t.Key.ToString(), t.Value);
                _allPKForEntityTypeStringCache.TryAdd(t.Key.ToStringDB(), t.Value);
            });
        }

        private static readonly ConcurrentDictionary<Type, ConcurrentDictionary<Type, PK>> _typeMappingsCache = new ConcurrentDictionary<Type, ConcurrentDictionary<Type, PK>>();

        [ClassMember(
            Description =
                "Returns the relevant property key for a given type entity-type and field-type.\r\n" +
                "\r\n" +
                "This enables the use of convenience 'mapping' methods like AddPM, AddPVM, GetPM, TryGetPM, GetPVM, TryGetPVM which have a simpler syntax.\r\n" +
                "\r\n" +
                "For instance if you have a class called DateOfBirth, and a corresponding field for your Customer-class (normally also called DateOfBirth), then you can use " +
                "Customer.GetPVM<DateOfBirth>() instead of Customer.GetPV<DateOfBirth>(CustomerP.DateOfBirth).\r\n" +
                "(The approach works only as long as there is only a single DateOfBirth-field in your Customer-class).\r\n" +
                "\r\n" +
                "Note that if you in this specific case did not use a specific class like DateOfBirth, but just the DateTime-type, " +
                "and there is ONLY one DateTime field for Customer, " +
                "this approach still works but the syntax is not as intuitive " +
                "(it becomes Customer.GetPVM<DateTime>() instead of Customer.<DateTime>(CustomerP.DateOfBirth).\r\n" +
                "\r\n" +
                "Note that throws an exception if exactly one type not found. In other words, caller is assume to know the proper use of the entity class.\r\n" +
                "\r\n" +
                "TODO: Turn into TryGetTypeMapping with corresponding method GetTypeMapping throwing an exception like this method does today.",
            LongDescription =
                "This is a convenience functionality, not strictly needed. It just reduces the somewhat verbose syntax of AgoRapide's -" + nameof(ARConcepts.PropertyAccess) + "-. " +
                "Note that it is also possible to define traditional setters / getters in your entity classes, " +
                "giving you 'normal' syntax back, like Customer.DateOfBirth."
        )]
        public static PK GetTypeMapping(Type entityType, Type propertyType) => _typeMappingsCache.GetOrAdd(entityType, t => new ConcurrentDictionary<Type, PK>()).GetOrAdd(propertyType, t =>
        {
            var candidates = PK.GetAllPKForEntityType(entityType).Where(p => propertyType.IsAssignableFrom(p.Type)).ToList();
            // TODO: Add support for List<T> with Cardinality WholeCollection!
            return candidates.Count switch
            {
                0 => throw new GetTypeMappingException(
                    "No mapping (no property key) found for " + nameof(entityType) + ": " + entityType.ToStringShort() + ", " + nameof(propertyType) + ": " + propertyType.ToStringShort() + ".\r\n" +
                    "Explanation. For the entity class " + entityType.ToString() + " there is no property key in " + entityType.ToStringVeryShort() + "P with type " + propertyType.ToStringShort() + " " +
                        "(in other words, there is no enum member / enum value in " + entityType.ToStringVeryShort() + "P tagged with '[PKType(Type=typeof(" + propertyType.ToStringVeryShort() + "))] or with a type which is assignable to " + propertyType.ToStringShort() + ")\r\n"
                    ),
                1 => candidates[0],
                _ => throw new GetTypeMappingException(
                    "Multiple mapping (multiple possible property keys) found for " + nameof(entityType) + ": " + entityType.ToStringShort() + ", " + nameof(propertyType) + ": " + propertyType.ToStringShort() + ".\r\n" +
                    "Explanation. For the entity class " + entityType.ToString() + " there are multiple property keys in " + entityType.ToStringVeryShort() + "P with type " + propertyType.ToStringShort() + " " +
                    "(in other words, there are multiple enum members / enum values in " + entityType.ToStringVeryShort() + "P tagged with '[PKType(Type=typeof(" + propertyType.ToStringVeryShort() + "))] or with a type which is assignable to " + propertyType.ToStringShort() + ").\r\n" +
                    "\r\n" +
                    "The different property keys found where: " + string.Join(", ", candidates.Select(p => p.ToString())) + ".\r\n"
                    )
            };
        });

        public class GetTypeMappingException : ApplicationException
        {
            public GetTypeMappingException(string message) : base(message) { }
        }

        private static readonly ConcurrentDictionary<Type, List<PK>> _obligatoryValuesCache = new ConcurrentDictionary<Type, List<PK>>();
        [ClassMember(Description =
            "Returns all obligatory values (based on -" + nameof(PKTypeAttributeP.IsObligatory) + "-)"
        )]
        public static IEnumerable<PK> GetObligatoryValuesForEntityType(Type entityType) => _obligatoryValuesCache.GetOrAdd(entityType, t => PK.GetAllPKForEntityType(entityType).Where(p =>
            p.GetA<PKTypeAttribute>().IP.GetPV<bool>(PKTypeAttributeP.IsObligatory)
        ).ToList());

        [Class(Description =
            "Used to signify that a given -" + nameof(PK) + "- returned from value factory inside -" + nameof(FromEnum) + "- has not been initialized " +
            "(has not yet been populated with -" + nameof(PKTypeAttribute) + "- collection).")]
        private class PKBoolPair
        {
            public PK Pk { get; private set; }
            public bool IsInitialized = false;
            [ClassMember(Description = "Not used currently (May 2020). Useful for debugging purposes")]
            public bool TrulyInitialized = false;
            public PKBoolPair(PK pk) => Pk = pk;
        }

        /// <summary>
        /// Implementing <see cref="ITypeDescriber"/>
        /// TODO: Explain why we do not add validator and parser here...
        /// TODO: Or maybe we should?
        /// </summary>
        /// <param name="key"></param>
        public static void EnrichKey(PK _) { }

        public override int GetHashCode() => ToString().GetHashCode();
        [ClassMember(Description =
            "Note that " + nameof(ToString) + " is only based on " + nameof(__enum) + ", " +
            "meaning that two enum members / enum values with same name but different type, like CustomerP.Name and OrderP.Name, will be considered equal.\r\n" +
            "This ensures as short keys as possible in AgoRapide, and it shown not to have any practical consequences."
        )]
        public bool Equals(IK other) => ToString().Equals(other.ToString());
        public override bool Equals(object other) => other is IK ik && Equals(ik);

        public class PKException : ApplicationException
        {
            public PKException(string message) : base(message) { }
            public PKException(string message, Exception inner) : base(message, inner) { }
        }

        private static HashSet<Type>? _allEnums;
        [ClassMember(
            Description =
                "All enums for the whole application.\r\n" +
                "TODO: Consider changing to -" + nameof(AREnumType.PropertyKeyEnum) + "--tagged enums only."
        )]
        public static HashSet<Type> AllEnums => _allEnums ??= new Func<HashSet<Type>>(() =>
        {
            UtilCore.AssertCurrentlyStartingUp(); // TODO: Do we need this assertion? Probably not
            var retval = new HashSet<Type>();
            UtilCore.Assemblies.SelectMany(a => a.GetTypes()).Where(t =>
            {
                return t.IsEnum;
            }).ForEach(t =>
            {
                if (retval.Contains(t)) throw new InvalidTypeException(t, "Already contained in " + nameof(retval) + "\r\n");
                retval.Add(t);
            });
            return retval;
        })();

        private static HashSet<Type>? _allPKEnums;
        [ClassMember(
            Description =
                "All -" + nameof(AREnumType.PropertyKeyEnum) + "--tagged enums for the whole application."
        )]
        public static HashSet<Type> AllPKEnums => _allPKEnums ??= new Func<HashSet<Type>>(() =>
        {
            UtilCore.AssertCurrentlyStartingUp(); // TODO: Do we need this assertion? Probably not
            var retval = new HashSet<Type>();
            AllEnums.ForEach(t =>
            {
                var a = EnumAttribute.GetAttribute(t);
                if (!a.IP.TryGetPV<AREnumType>(EnumAttributeP.AREnumType, out var et) || et != AREnumType.PropertyKeyEnum) return;
                if (retval.Contains(t)) throw new InvalidTypeException(t, "Already contained in " + nameof(retval) + "\r\n");
                retval.Add(t);
            });
            return retval;
        })();
    }
}