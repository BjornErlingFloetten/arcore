﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARCCore {
    [Class(Description =
        "Describes relationships between entities (between tables in RDBMS terms).\r\n" +
        "\r\n" +
        "Note than in general relationsships are picked up automatically in AgoRapide through standardized naming of keys.\r\n" +
        "\r\n" +
        "For instance, for 'Order', key 'CustomerId' is automatically assumed to point to a " +
        "-" + nameof(PKRelAttributeP.ForeignEntity) + "- of type 'Customer', so this " +
        "attribute does not have to be specified.\r\n" +
        "\r\n" +
        "However, for 'Employee', key 'SupervisorId' where the supervisor is another 'Employee', " +
        "-" + nameof(PKRelAttributeP.ForeignEntity) + "- has to be explicitly specified in order for the system to understand that " +
        "'SupervisorId' is actually a foreign key pointing to another 'Employee'.\r\n" +
        "(this is not necessarily limited to self-referencing tables, " +
        "another example could be 'Transaction' with 'FromAccountId' and 'ToAccountId' both pointing to an 'Account'.)\r\n" +
        "\r\n" +
        "Used by -" + nameof(ARComponents.ARCQuery) + "- (in -QueryExpressionRel- and -ForeignKey-) in order to enable queries like:\r\n" +
        "\"Employee/WHERE DepartmentId = 'Sales'/REL Supervisor\" (all Supervisors of Employees in Sales) and\r\n" +
        "\"Employee/WHERE DepartmentId = 'Sales'/SELECT Name, Supervisor.Name).\r\n" +
        "\r\n" +
        "In addition to explicit stating -" + nameof(PKRelAttributeP.ForeignEntity) + "-, " +
        "this attribute is also used to -" + nameof(PKRelAttributeP.EnforceReferentialIntegrity) + "-\r\n." +
        "\r\n" +
        "See also -IPRelationsKeysPointingTo- and -IPRelationsKeysPointingFrom- in -" + nameof(ARComponents.ARCQuery) + "-.\r\n"
    )]
    public class PKRelAttribute : BasePKAttribute {

        /// <summary>
        /// TODO: Add AssertInitializationNotCompleteForAttributeAccess
        /// </summary>
        [ClassMember(Description = "See -" + nameof(PKRelAttributeP.ForeignEntity) + "-.")]
        public Type? ForeignEntity;

        [ClassMember(Description = "See -" + nameof(PKRelAttributeP.OppositeTerm) + "-.")]
        public string? OppositeTerm;

        /// <summary>
        /// TODO: Add AssertInitializationNotCompleteForAttributeAccess
        /// </summary>
        [ClassMember(Description = "See -" + nameof(PKRelAttribute.EnforceReferentialIntegrity) + "-.")]
        public bool? EnforceReferentialIntegrity;
        
        public override void Initialize() {
            if (ForeignEntity != null) IP.AddPV(PKRelAttributeP.ForeignEntity, ForeignEntity);
            if (OppositeTerm != null) IP.AddPV(PKRelAttributeP.OppositeTerm, OppositeTerm);
            if (EnforceReferentialIntegrity != null) IP.AddPV(PKRelAttributeP.EnforceReferentialIntegrity, EnforceReferentialIntegrity.Value);

            base.Initialize();
        }

        public class PKHTMLAttributeException : ApplicationException {
            public PKHTMLAttributeException(string message) : base(message) { }
            public PKHTMLAttributeException(string message, Exception inner) : base(message, inner) { }
        }
    }

    [Enum(
            Description = "Describes class -" + nameof(PKRelAttribute) + "-.",
            AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum PKRelAttributeP {
        __invalid,

        [PKType(
            Description = "The type of foreign entity that this property points to.\r\n",
            Type = typeof(Type)
        )]
        ForeignEntity,

        [PKType(Description =
            "Example: For 'Employee', the key 'SupervisorId' where the supervisor is another 'Employee', will have another " +
            "meaning \"as seen from\" the Supervisor, namely \"Subordinates\" or similar.\r\n" +
            "\r\n" +
            "Used by -" + nameof(ARComponents.ARCQuery) + "- (in -QueryExpressionRel- and -ForeignKey-) in order to enable queries like:\r\n" +
            "\"Employee/42/REL Subordinate\" (all Subordinates of Employee 42) and\r\n" +
            "\"Employee/SELECT Name, Subordinate.Count()\" (number of Subordinates for each Employee)\r\n"
        )]
        OppositeTerm,

        [PKType(
            Description =
                "If TRUE then referential integrity will be enforced by -" + nameof(PropertyStreamLine.TryStore) + "-.\r\n" +
                "\r\n" +
                "-" + nameof(PropertyStreamLine.TryStore) + "- will then fail when:\r\n" +
                "1) At creation of key, corresponding foreign entity does not exist.\r\n" +
                "2i At deletion of foreign entity, if there still exist any foreign key pointing to the entity.\r\n" +
                "\r\n" +
                "NOTE: Use of this attribute is not active (not implemented?) as of Mar 2021.",
            Type = typeof(bool)
        )]
        EnforceReferentialIntegrity
    }
}