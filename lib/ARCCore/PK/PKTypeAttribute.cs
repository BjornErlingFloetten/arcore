﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ARCCore {

    [Class(
        Description =
            "Contains the most basic essential information about a property key, " +
            "like its description, -" + nameof(Type) + "-, -" + nameof(Cardinality) + "-, -" + nameof(IsObligatory) + "- and -" + nameof(DefaultValue) + "-.\r\n" +
            "\r\n" +
            "Does also contain " +
            "-" + nameof(StandardCleaner) + "- (for cleaning up input-values from the user) and " +
            "-" + nameof(StandardValidatorAndParser) + "- for validating and parsing common types like string, int, long, bool, DateTime, TimeSpan, Type, Uri.\r\n" +
            "\r\n" +
            "Will always be present within an instance of -" + nameof(PK) + "-, with default type set to 'typeof(string)'.\r\n" +
            "\r\n" +
            "See also -" + nameof(PKTypeAttributeP) + "-."
    )]
    public class PKTypeAttribute : BasePKAttribute {

        private Type? _type;
        [ClassMember(Description = "See -" + nameof(PKTypeAttributeP.Type) + "-.")]
        public Type Type {
            get {
                // TODO: Exception message is incorrect.
                if (_type == null) throw new NullReferenceException("TODO: THIS EXCEPTION MESSAGE IS INCORRECT! " + nameof(Type) + ". Supposed to always be set from corresponding " + nameof(BaseAttribute.Initialize) + ".\r\nDetails: " + ToString());

                /// NOTE: This assertion makes it (theoretically) possible to change a type for a live-system over <see cref="ARConcepts.PropertyStream"/> 
                /// NOTE: because we force use of <see cref="ARConcepts.PropertyAccess"/>
                /// NOTE: But we loose a convenience method of reading type (this property), so therefore we have implemented <see cref="PK.Type"/>
                AssertInitializationNotCompleteForAttributeAccess();

                return _type;
            }
            set => _type = value;
        }

        /// <summary>
        /// TODO: Add AssertInitializationNotCompleteForAttributeAccess
        /// </summary>
        [ClassMember(Description = "See -" + nameof(PKTypeAttributeP.Cardinality) + "-.")]
        public Cardinality Cardinality;

        [ClassMember(Description = "See -" + nameof(PKTypeAttributeP.IsObligatory) + "-.")]
        public bool IsObligatory;

        /// <summary>
        /// TODO: Add AssertInitializationNotCompleteForAttributeAccess
        /// </summary>
        [ClassMember(Description = "See -" + nameof(PKTypeAttributeP.DefaultValue) + "-.")]
        public object? DefaultValue;

        public override void Initialize() {

            var enumType = IP.GetPV<Type>(EnumMemberAttributeP.EnumType);
            var enumMember = IP.GetPV<string>(EnumMemberAttributeP.EnumMember);

            if (_type == null) {
                // Note how types default to typeof(string)
                _type = typeof(string);
            } else if (_type.IsGenericType) {
                if (_type.GetGenericTypeDefinition().Equals(typeof(List<>))) {
                    // Clarify use of List<> and cardinality
                    throw new PKTypeAttributeException(
                        "Invalid use of List<> generic type for " + enumType.ToStringShort() + "." + enumMember + "\r\n" +
                        "For " + enumType.ToStringShort() + "." + enumMember + " attribute Type = typeof(" + _type.ToStringShort() + ").\r\n" +
                        "You should not use the generic list concept here.\r\n" +
                        "\r\n" +
                        "Possible resolution:\r\n" +
                        "Set Type = typeof(" + _type.GenericTypeArguments[0].ToStringShort() + ") instead of " + _type.ToStringShort() + " and \r\n" +
                        "Set Cardinality = " + nameof(Cardinality) + "." + nameof(Cardinality.WholeCollection) + "\r\n" +
                        "(this will create " + _type.ToStringShort() + " automatically for you 'behind the scenes').\r\n");
                } else {
                    // Generic types should probably not be allowed.
                    // TODO: Throw exception here is we are sure about not allow generic types.
                }
            }

            IP.AddPV(PKTypeAttributeP.Type, _type);

            if (DefaultValue != null) {
                IP.AddPV(PKTypeAttributeP.DefaultValue, DefaultValue);
                IP.AddPV(PKTypeAttributeP.IsObligatory, true);
            } else {
                IP.AddPV(PKTypeAttributeP.IsObligatory, IsObligatory);
            }
            if (Cardinality == Cardinality.__invalid) {
                IP.AddPV(PKTypeAttributeP.Cardinality, Cardinality.HistoryOnly);
            } else {
                IP.AddPV(PKTypeAttributeP.Cardinality, Cardinality);
            }

            IP.AddPV(PKTypeAttributeP.BoilerplateCodeProperties, new Func<string>(() => {
                var pkType = _type.ToStringVeryShort();
                var enumDotValue = enumType.ToStringVeryShort() + "." + enumMember.ToString();
                var propertyName = enumMember.ToString();
                var propertyNameAsParameter = propertyName[..1].ToLower() + propertyName[1..];
                return
                    "// Autogenerated boilerplate code. Paste into your class as needed." +
                    "\r\n" +
                    "public " + pkType + " " + propertyName + "{\r\n" +
                    "  get => IP.GetPV<" + pkType + ">(" + enumDotValue + ");\r\n" +
                    "  set => IP.SetPV(" + enumDotValue + ", value);\r\n" +
                    "}\r\n" +
                    "public Add" + propertyName + "(" + pkType + " " + propertyNameAsParameter + ") => IP.AddPV(" + enumDotValue + ", " + propertyNameAsParameter + ");\r\n" +
                    (IP.GetPV<bool>(PKTypeAttributeP.IsObligatory) ?
                        (
                            "/// <see cref=\"PKTypeAttributeP.IsObligatory\"/> for " + enumDotValue + ", therefore Get as possible null, Get with default value and TryGet not offered here\r\n"
                        ) :
                        (
                            "public " + pkType + "? Get" + propertyName + "() => TryGet" + propertyName + "(out var retval) ? retval : null;\r\n" +
                            "public " + pkType + " Get" + propertyName + "(" + pkType + " defaultValue) => TryGet" + propertyName + "(out var retval) ? retval : defaultValue;\r\n" +
                            "public bool TryGet" + propertyName + "(out " + pkType + " " + propertyNameAsParameter + ") => IP.TryGetPV(" + enumDotValue + ", out " + propertyNameAsParameter + ");\r\n"
                        )
                    ) +
                    "\r\n" +
                    "\r\n";
            })());

            base.Initialize();
        }

        [ClassMember(Description =
            "Returns validator and parser for well known types like string, int, long, bool, DateTime, TimeSpan, Type, Uri.\r\n" +
            "\r\n" +
            "TODO: Add validator and parser through use of reflection, that is, use reflection to look for type containing\r\n" +
            "TODO: a static TryParse method.\r\n" +
            "\r\n" +
            "TODO: There is duplicate code in ARCQuery.QueryExpressionWhere.TryParse and ARCCore.PKTypeAttribute.StandardValidatorAndParser"
        )]
        public static Func<string, ParseResult>? StandardValidatorAndParser(Type type) {
            if (type.Equals(typeof(string)))
                return value => !string.IsNullOrEmpty(value) ? ParseResult.CreateOK(value) : ParseResult.CreateError(
                "Invalid as " + type.ToStringShort() + " (because " + (value == null ? "[NULL]" : "[EMPTY]") + ").");

            if (type.Equals(typeof(int)))
                return value => int.TryParse(value, out var temp) ? ParseResult.CreateOK(temp) : ParseResult.CreateError(
                "Invalid as " + type.ToStringShort() + ".");

            if (type.Equals(typeof(long)))
                return value => long.TryParse(value, out var temp) ? ParseResult.CreateOK(temp) : ParseResult.CreateError(
                "Invalid as " + type.ToStringShort() + ".");

            if (type.Equals(typeof(double)))
                return value => UtilCore.DoubleTryParse(value, out var temp) ? ParseResult.CreateOK(temp) : ParseResult.CreateError(
                    "Invalid as " + type.ToStringShort() + ".");

            if (type.Equals(typeof(bool)))
                return value => bool.TryParse(value, out var temp) ? ParseResult.CreateOK(temp) :
                (value) switch {
                    "0" => ParseResult.CreateOK(false),
                    "1" => ParseResult.CreateOK(true),
                    _ => ParseResult.CreateError(
                        "Invalid as " + type.ToStringShort() + ", use '" + true.ToString() + "' or '" + false.ToString() + "'")
                };

            if (type.Equals(typeof(DateTime)))
                // TODO: Add user configurable formats
                return value =>
                UtilCore.DateTimeTryParse(value, out var temp) ? ParseResult.CreateOK(temp) : ParseResult.CreateError(
                    "Invalid as " + type.ToStringShort() + ".\r\n" +
                    "Must be in one of the following -" + nameof(UtilCore.ValidDateTimeFormats) + "-:\r\n" +
                    string.Join(", ", UtilCore.ValidDateTimeFormats) + "\r\n");

            if (type.Equals(typeof(TimeSpan)))
                // TODO: Move allowed formats to Util-class
                return value => UtilCore.TimeSpanTryParse(value, out var temp) ? ParseResult.CreateOK(temp) : ParseResult.CreateError(
                    "Invalid as " + type.ToStringShort() + ".\r\n" +
                    "Must be in one of the following formats:\r\n" +
                    string.Join(", ", UtilCore.ValidTimeSpanFormats.Select(f => f.Replace(@"\", ""))) + "\r\n"); /// Note corresponding code in <see cref="PValue{T}"/> 

            if (type.Equals(typeof(Type)))
                return value => UtilCore.TryGetTypeFromStringFromCache(value, out var temp) ? ParseResult.CreateOK(temp) : ParseResult.CreateError(
                    "Invalid as " + type.ToStringShort() + " (must be in a format understood by -" + nameof(UtilCore) + "-.-" + nameof(UtilCore.TryGetTypeFromStringFromCache) + "-).");

            if (type.Equals(typeof(Uri)))
                return value => (Uri.TryCreate(value, UriKind.RelativeOrAbsolute, out var temp)) ? ParseResult.CreateOK(temp) : ParseResult.CreateError(
                    "Invalid as " + type.ToStringShort() + ".");

            // TODO: Add any additional desired supported types to this list.

            if (type.IsEnum) {
                return value =>
                    /// NOTE: <see cref="ParseResult.Result"/> now will be <see cref="PValue{object}"/>, not of the generic enum-type
                    /// TODO: This can surefly be rectified with some smart code here.
                    /// NOTE (the object "inside" will be of correct type though)
                    UtilCore.EnumTryParse(type, value, out var temp) ? ParseResult.CreateOK(temp) : ParseResult.CreateError(
                        "Invalid as " + type.ToStringShort() + ".\r\n" +
                        "Must be one of the following values:\r\n" +
                        string.Join(", ", UtilCore.EnumGetMembers(type)));
            }

            /// No standard validator and parser found by us. 
            return null;
        }

        [ClassMember(
            Description =
                "Returns a cleaner for well known types like bool",
            LongDescription =
                "TODO: If we see consistent user-errors for other (well known) types, that are not covered by the final standard cleaner set by " +
                nameof(PK.Initialize) + " (which just string.Trim's the input value), then attempt to create more cleaners should be done here."
        )]
        public static Func<string, string>? StandardCleaner(Type type) {
            if (type.Equals(typeof(bool)))
                return value => {
                    value = value.Trim();
                    switch (value) {

                        case "0":
                        case "false":
                        case "False":
                        case "FALSE": return false.ToString();

                        case "1":
                        case "true":
                        case "True":
                        case "TRUE": return true.ToString();
                        default: break;
                    }
                    return value;
                };

            /// No standard cleaner found by us. 
            /// If not, then a final standard cleaner will be set by <see cref="PK.Initialize(object)"/> which just string.Trim's the input value
            return null;
        }

        public class PKTypeAttributeException : ApplicationException {
            public PKTypeAttributeException(string message) : base(message) { }
            public PKTypeAttributeException(string message, Exception inner) : base(message, inner) { }
        }
    }

    [Enum(
        Description = "Describes class -" + nameof(PKTypeAttribute) + "-.",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum PKTypeAttributeP {
        __invalid,

        [PKType(
            Description =
                "The type of this Property.\r\n" +
                "\r\n" +
                "Originates from -" + nameof(PKTypeAttribute.Type) + "-.\r\n" +
                "\r\n" +
                "Default to typeof(string) (see -" + nameof(PKTypeAttribute.Initialize) + "-).\r\n" +
                "\r\n" +
                "Note that normally (in entity-objects like -" + nameof(PRich) + "-). " +
                "the actual stored value is packed inside an -" + nameof(IP) + "- instance, usually -" + nameof(PValue<TValue>) + "-, " +
                "so for instance if type = typeof(string) then what is actually stored will usually be a -" + nameof(PValue<string>) + "-. " +
                "\r\n" +
                "If the type is derived form -" + nameof(IP) + "- then it is stored directly. " +
                "See -" + nameof(ActualConnectionP.ConnectionInstruction) + "- for an example of this" +
                "\r\n" +
                "Typical examples are\r\n" +
                "1) typeof(string), typeof(long), typeof(DateTime),\r\n" +
                "2) typeof([AnyEnum]),\r\n" +
                "3) Can also be a type assignable to -" + nameof(ITypeDescriber) + ",\r\n" +
                "\r\n" +
                "Note: Easily accessed through convenience method -" + nameof(PK.Type) + "-.\r\n" +
                "\r\n" +
                "Note: If you want to store multiple instances of a type, do not set type to List<T> or similar. " +
                "You should instead set -" + nameof(Cardinality) + "- to -" + nameof(ARCCore.Cardinality.WholeCollection) + "- and " +
                "the corresponding List<T> collection will then be generated automatically for you 'behind-the-scenes'",
            Type = typeof(Type)
        )]
        Type,

        [PKType(
            Description =
                "The default value.\r\n" +
                "\r\n" +
                "Originates from -" + nameof(PKTypeAttribute.DefaultValue) + "-.\r\n" +
                "\r\n" +
                "Will be used by -" + nameof(IP.TryAssertIntegrity) + "- (actual property will be set to this value) if no value for actual property is given.\r\n" +
                "TODO: NOTE: This is not necessary smart, as it will consume more memory (at least for -" + nameof(PRich) + "-, " +
                "TODO: NOTE: not necessarily for -" + nameof(PExact<TPropertyKeyEnum>) + "-).\r\n" +
                "TODO: NOTE: Consider instead letting -" + nameof(IP.TryGetP) + "- look for default value instead of throwing exception " +
                "TODO: NOTE: (which will coincidentally also lead to TryAssertIntegrity not being as necessary).\r\n" +
                "TODO: NOTE: (you should also be aware of the concept of -EntityMethodKey-s in -" + nameof(ARComponents.ARCQuery) + "-.\r\n" +
                "\r\n" +
                "Note: If -" + nameof(DefaultValue) + "- is given then -" + nameof(IsObligatory) + "- will automatically be set to TRUE\r\n" +
                "\r\n",
            Type = typeof(object)
        )]
        DefaultValue,

        [PKType(
            Description =
                "Describes how multiple instances of a property is supposed to be handled.\r\n" +
                "\r\n" +
                "Originates from -" + nameof(PKTypeAttribute.Cardinality) + "-.\r\n" +
                "\r\n" +
                "See description of -" + nameof(ARCCore.Cardinality) + "- for details",
            LongDescription =
                "Note: Easily accessed through convenience method -" + nameof(PK.Cardinality) + "-",
            Type = typeof(Cardinality)
        )]
        Cardinality,

        [PKType(
            Description =
                "True means value is obligatory. " +
                "\r\n" +
                "Originates from -" + nameof(PKTypeAttribute.IsObligatory) + "-.\r\n" +
                "\r\n" +
                "Used for instance by -" + nameof(IP.AssertIntegrity) + "-.",
            //LongDescription =
            //    "Note: Easily accessed through convenience method -" + nameof(PK.IsObligatory) + "-",
            Type = typeof(bool)
        )]
        IsObligatory,

        [PKType(
            Description =
                "Automatically generated boilerplate code that implements traditional setters and getters for this key.\r\n" +
                "\r\n" +
                "In it simplest form this entails code similar to:\r\n" +
                "\r\n" +
                "   public string Name {\r\n" +
                "      get => IP.GetPV<string>(AppleP.Name);\r\n" +
                "      set => IP.SetPV(AppleP.Name, value);\r\n" +
                "   }\r\n" +
                "\r\n" +
                "Paste this code into your class if you find the default -" + nameof(ARConcepts.PropertyAccess) + "- syntax too verbose.\r\n" +
                "\r\n" +
                "If you are instead more concerned about memory coumsumption, use code provided through -" + nameof(ClassAttributeP.BoilerplateCodePropertiesAndStorage) + "- instead.\r\n" +
                "\r\n" +
                "Background: The -" + nameof(ARConcepts.PropertyAccess) + "--syntax is a bit convoluted. " +
                "This can be alleviated by implementing traditional setters / getters, something which is offered through this property.\r\n" +
                "\r\n" +
                "Note that the self-evident performance impact due to a more complicated access mechanism will of course remain " +
                "(unless you use -" + nameof(ClassAttributeP.BoilerplateCodePropertiesAndStorage) + " instead)-.\r\n" +
                "\r\n" +
                "Note that in order to get all the rich functionality of -" + nameof(ARConcepts.PropertyAccess) + "- " +
                "a quite large number of methods have to be implemented.\r\n" +
                "\r\n" +
                //"Automatically inserted by -" + nameof(ClassAttribute.GetAttribute) + "-.\r\n" +
                //"(but not for classes within -" + nameof(ARConcepts.StandardAgoRapideCode) + "-, " +
                //"only for -" + nameof(ARConcepts.ApplicationSpecificCode) + "-.)\r\n" +
                //"\r\n" +
                "See also -" + nameof(ClassAttributeP.BoilerplateCodePropertiesAndStorage) + "-."
        )]
        BoilerplateCodeProperties,
    }
}
