﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ARCCore {
    [Class(Description =
        "Contains pointer to HTML encoder for a property.\r\n" +
        "\r\n" +
        "The encoder replaces any standard mechanism used for HTML encoding a single property.\r\n" +
        "It is useful for instance for showing images, " +
        "where the property value can be for instance a Base64-encoding of the binary data " +
        "which the encoder converts into an HTML <IMG> tag.\r\n" +
        "\r\n" +
        "Used in -" + nameof(ARComponents.ARCCore) + "- by class -Extensions- method -ToHTMLSimpleAsTableRow-.\r\n" +
        "Used in -" + nameof(ARComponents.ARCQuery) + "- by class -QueryProgressDetails- method -ToHTMLSimpleSingle-.\r\n" +
        "\r\n" +
        "This attribute can be used in two distinct manners:\r\n" +
        "\r\n" +
        "1) Manually in C#, that is, at -" + nameof(ARConcepts.TaggingOfPropertyKeys) + "- like:\r\n" +
        "   [PKHtml(Encoder = typeof(PictureEncoder))]\r\n" +
        "   Picture,\r\n" +
        "for a specific property called 'Picture'.\r\n" +
        "or\r\n" +
        "2) Use a class implementing -" + nameof(ITypeDescriber) + "- which through the EnrichKey-principle in that interface, " +
        "supplies this attribute (" + nameof(PKHTMLAttribute) + ") with the necessary encoder.\r\n" +
        "In this manner (with a class called 'Picture', you only have to write:\r\n" +
        "  [PKType(type=typeof(Picture))]\r\n" +
        "  Picture,\r\n" +
        "Note: In the last case, the class 'Picture' also have to set -" + nameof(PK.ValidatorAndParser) + "- and implement ToString().\r\n"
    )]
    public class PKHTMLAttribute : BasePKAttribute {

        /// <summary>
        /// TODO: Add AssertInitializationNotCompleteForAttributeAccess
        /// </summary>
        [ClassMember(Description = "See -" + nameof(PKHTMLAttributeP.Encoder) + "-.")]
        public Type? Encoder;

        private System.Reflection.MethodInfo? _encoderMethod;

        [ClassMember(Description =
            "Encodes the given string to HTML format through the static method implemented in class -" + nameof(Encoder) + "-."
        )]
        public string Encode(string _string) {
            if (Encoder == null) throw new NullReferenceException(nameof(Encoder) + ". Should have been asserted at call to " + nameof(Initialize));
            if (_encoderMethod == null) throw new NullReferenceException(nameof(_encoderMethod) + ". Should have been set at call to " + nameof(Initialize));
            var methodName = IHTMLEncoder.EncodeMethodName;

            object? result;
            try {
                result = _encoderMethod.Invoke(null, new object[] { _string });
            } catch (Exception ex) {
                var enumType = IP.GetPV<Type>(EnumMemberAttributeP.EnumType);
                var enumMember = IP.GetPV<string>(EnumMemberAttributeP.EnumMember);

                throw new PKHTMLAttributeException(
                    "Unable to invoke for type " + Encoder.ToStringShort() + " the method\r\n" +
                    "   public static void " + methodName + "\r\n" +
                    "Resolution: Check that it exists and that it takes exactly one parameter of type " + typeof(string).ToStringShort() + ".\r\n" +
                    "In other words it should look like\r\n\r\n" +
                    "   public static string " + methodName + "(" + typeof(string).ToStringShort() + " _string)\r\n" +
                    "\r\n" +
                    "Details: Problem originated from the tagging done for enum " + enumType.ToStringShort() + "." + enumMember.ToString() + "\r\n",
                    inner: ex
                );
            }
            if (!(result is string retval)) {
                var enumType = IP.GetPV<Type>(EnumMemberAttributeP.EnumType);
                var enumMember = IP.GetPV<string>(EnumMemberAttributeP.EnumMember);

                throw new PKHTMLAttributeException(
                    "Invalid result from invocation for type " + Encoder.ToStringShort() + " the method\r\n" +
                    "   public static void " + methodName + "\r\n" +
                    "Resolution: Check that it exists and that it returns a string.\r\n" +
                    "In other words it should look like\r\n\r\n" +
                    "   public static string " + methodName + "(" + typeof(string).ToStringShort() + " _string)\r\n" +
                    "\r\n" +
                    "Details: Problem originated from the tagging done for enum " + enumType.ToStringShort() + "." + enumMember.ToString() + "\r\n"
                );
            }
            return retval;
        }

        public override void Initialize() {
            var enumType = IP.GetPV<Type>(EnumMemberAttributeP.EnumType);
            var enumMember = IP.GetPV<string>(EnumMemberAttributeP.EnumMember);

            if (Encoder == null) {
                throw new PKHTMLAttributeException(
                    "'Empty' -" + nameof(PKHTMLAttribute) + "- specified for " + enumType.ToStringShort() + "." + enumMember.ToString() + ".\r\n" +
                    "(Property '" + nameof(Encoder) + "' not set.)\r\n" +
                    "\r\n" +
                    "Resolution: Either add the property 'Encoder' or remove the use of [PKHTML(...)]\r\n" +
                    "\r\n" +
                    "Details: Problem originated from the tagging done for enum " + enumType.ToStringShort() + "." + enumMember.ToString() + "\r\n"
                );
            }

            if (!typeof(IHTMLEncoder).IsAssignableFrom(Encoder)) {
                throw new InvalidTypeException(
                    Encoder, typeof(IHTMLEncoder),
                    "Invalid type specified in -" + nameof(PKHTMLAttribute) + "- for 'Encoder' for " + enumType.ToStringShort() + "." + enumMember.ToString() + ".\r\n" +
                    "\r\n" +
                    "Resolution: Let type " + Encoder.ToStringShort() + " implement " + nameof(IHTMLEncoder) + " or use another type.\r\n" +
                    "\r\n" +
                    "Details: Problem originated from the tagging done for enum " + enumType.ToStringShort() + "." + enumMember.ToString() + "\r\n"
                );
            }

            var methodName = IHTMLEncoder.EncodeMethodName;
            _encoderMethod = Encoder.GetMethod(methodName, System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static) ?? throw new InvalidTypeException(Encoder,
                "The class " + Encoder.ToStringShort() + " does not implement the method\r\n" +
                "   public static void " + methodName + "(" + typeof(string).ToStringShort() + " key)\r\n" +
                "but it should have according to the explanation (documentation) for interface " + typeof(IHTMLEncoder).ToStringShort() + ".\r\n" +
                "\r\n" +
                "Resolution: Either add the method, or do not mark " + Encoder.ToStringShort() + " as implementing " + typeof(IHTMLEncoder).ToStringShort() + ".\r\n" +
                "Note: The compiler was unable to catch this problem because the actual method is supposed to be static and therefore really not part " +
                "of the interface mechanism.\r\n" +
                "\r\n" +
                "Details: Problem originated from the tagging done for enum " + enumType.ToStringShort() + "." + enumMember.ToString() + "\r\n"
            );
            // TODO: Do more detailed signatur check here.
            if (!typeof(string).Equals(_encoderMethod.ReturnType)) throw new InvalidTypeException(Encoder,
                "The class " + Encoder.ToStringShort() + " does not implement the method\r\n" +
                "   public static void " + methodName + "(" + typeof(string).ToStringShort() + " key)\r\n" +
                "correctly. It is implemented as returning " + _encoderMethod.ReturnType.ToStringShort() + " instead of " + typeof(string).ToStringShort() + ".\r\n" +
                "\r\n" +
                "Resolution: Either correct the method, or do not mark " + Encoder.ToStringShort() + " as implementing " + typeof(IHTMLEncoder).ToStringShort() + ".\r\n" +
                "Note: The compiler was unable to catch this problem because the actual method is supposed to be static and therefore really not part " +
                "of the interface mechanism.\r\n" +
                "\r\n" +
                "Details: Problem originated from the tagging done for enum " + enumType.ToStringShort() + "." + enumMember.ToString() + "\r\n"
            );

            var parameters = _encoderMethod.GetParameters();
            if (parameters.Length != 1) throw new InvalidTypeException(Encoder,
                "The class " + Encoder.ToStringShort() + " does not implement the method\r\n" +
                "   public static void " + methodName + "(" + typeof(string).ToStringShort() + " key)\r\n" +
                "correctly. It has " + parameters.Length + " parameters instead of 1.\r\n" +
                "\r\n" +
                "Resolution: Either correct the method, or do not mark " + Encoder.ToStringShort() + " as implementing " + typeof(IHTMLEncoder).ToStringShort() + ".\r\n" +
                "Note: The compiler was unable to catch this problem because the actual method is supposed to be static and therefore really not part " +
                "of the interface mechanism.\r\n" +
                "\r\n" +
                "Details: Problem originated from the tagging done for enum " + enumType.ToStringShort() + "." + enumMember.ToString() + "\r\n"
            );

            if (!typeof(string).Equals(parameters[0].ParameterType)) throw new InvalidTypeException(Encoder,
                "The class " + Encoder.ToStringShort() + " does not implement the method\r\n" +
                "   public static void " + methodName + "(" + typeof(string).ToStringShort() + " key)\r\n" +
                "correctly. Its parameter is of type " + parameters[0].ParameterType.ToStringShort() + " instead of " + typeof(string).ToStringShort() + ".\r\n" +
                "\r\n" +
                "Resolution: Either correct the method, or do not mark " + Encoder.ToStringShort() + " as implementing " + typeof(IHTMLEncoder).ToStringShort() + ".\r\n" +
                "Note: The compiler was unable to catch this problem because the actual method is supposed to be static and therefore really not part " +
                "of the interface mechanism.\r\n" +
                "\r\n" +
                "Details: Problem originated from the tagging done for enum " + enumType.ToStringShort() + "." + enumMember.ToString() + "\r\n"
            );

            IP.AddPV(PKHTMLAttributeP.Encoder, Encoder);

            base.Initialize();
        }

        public class PKHTMLAttributeException : ApplicationException {
            public PKHTMLAttributeException(string message) : base(message) { }
            public PKHTMLAttributeException(string message, Exception inner) : base(message, inner) { }
        }
    }

    [Enum(
            Description = "Describes class -" + nameof(PKRelAttribute) + "-.",
            AREnumType = AREnumType.PropertyKeyEnum
        )]
    public enum PKHTMLAttributeP {
        __invalid,

        [PKType(
            Description =
                "The class doing the HTML-encoding.\r\n" +
                "\r\n" +
                "It should implement -" + nameof(IHTMLEncoder) + "-, that is, have a static method " +
                "'public static string Encode(string _string)'.\r\n",
            Type = typeof(Type)
        )]
        Encoder
    }

    [Class(Description =
        "Pseudo-interface signifying that implementing class has a static metod called 'Encode' for encoding a string to HTML.\r\n" +
        "\r\n" +
        "See -" + nameof(PKHTMLAttribute) + "- for more information.\r\n" +
        "\r\n" +
        "Note how this interface in itself is empty \r\n" +
        "\r\n" +
        "'Encode' should have the following signature:\r\n" +
        "  public static string Encode(string _string)\r\n" +
        "\r\n"
    )]
    public interface IHTMLEncoder {

        public const string EncodeMethodName = "Encode";

        // Provide this method in your class implementing this interface.
        // public static string Encode(string _string);
    }
}