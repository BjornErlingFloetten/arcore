﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;

namespace ARCCore {

    [Class(
        Description =
            "Explains how logging of changes to a property should be done." +
            "\r\n" +
            "Describes mostly occasions where logging shall be restricted.\r\n" +
            "\r\n" +
            "Normally when a class like -" + nameof(PConcurrent) + "- is set up to log (through -" + nameof(ARCCore.IP.Logger) + "-), " +
            "then every property's changes will be logged. In some cases this is too much, " +
            "for instance if -" + nameof(ActualConnectionP.CountReceiveMessage) + "- was to be logged for every -" + nameof(ARConcepts.PropertyStream) + "--line received, that " +
            "would generate another property stream line which again would update the -" + nameof(ActualConnectionP.CountReceiveMessage) + "- value.\r\n" +
            "\r\n" +
            "Utilized by for instance -" + nameof(ActualConnection) + "-.\r\n" +
            "\r\n" +
            "See also -" + nameof(ARConcepts.ExposingApplicationState) + "-.\r\n" +
            "\r\n" +
            "See also -" + nameof(PKLogAttributeP) + "-."
    )]
    public class PKLogAttribute : BasePKAttribute {

        [ClassMember(Description = "See -" + nameof(PKLogAttributeP.DoNotLogAtAll) + "-.")]
        public bool DoNotLogAtAll;

        // Not implemented as of May 2020
        //[ClassMember(Description = "See -" + nameof(PKLogAttributeP.CountInterval) + "-.")]
        //public long CountInterval;

        // Not implemented as of May 2020
        //[ClassMember(Description = "See -" + nameof(PKLogAttributeP.CountInterval) + "-.")]
        //public TimeSpan TimeInterval;

        public override void Initialize() {
            if (DoNotLogAtAll) {
                // Note that we do no bother with storing FALSE
                IP.AddPV(PKLogAttributeP.DoNotLogAtAll, DoNotLogAtAll);
            }
            base.Initialize();
        }
    }

    [Enum(
        Description = "Describes class -" + nameof(PKLogAttribute) + ".",
        AREnumType = AREnumType.PropertyKeyEnum
    )]
    public enum PKLogAttributeP {
        __invalid,

        [PKType(
            Type = typeof(bool),
            Description =
                "Originates from -" + nameof(PKLogAttribute.DoNotLogAtAll) + "-.\r\n" +
                "\r\n" +
                "This alternative should not be used because it will negate the whole idea of " +
                "-" + nameof(ARConcepts.ExposingApplicationState) + "- to the -" + nameof(ARConcepts.PropertyStream) + "-. " +
                "It is better to use either -" + nameof(CountInterval) + "- or -" + nameof(TimeInterval) + "- " +
                "(however, as of Apr 2020 these two are not implemented)."
        )]
        DoNotLogAtAll,

        [PKType(
            Description =
                "For a value of n, log only every n'th time the property is changed.\r\n" +
                "\r\n" +
                "TODO: Not implemented as of Apr 2020. Will require some meta-storage in implementing classes, like PConcurrent.\r\n" +
                "\r\n" +
                "Suitable when you have a steady stream of changes for the actual property, and only need to 'sample' some of them in the log, " +
                "for instance as a progress indicator. Conceptually simpler than -" + nameof(TimeInterval) + "-.",
            Type = typeof(long)
        )]
        CountInterval,

        [PKType(
            Description =
                "Specifies a certain time interval for which the property's value shall be sent over the -" + nameof(ARConcepts.PropertyStream) + "-.\r\n" +
                "\r\n" +
                "TODO: Not implemented as of Apr 2020. Will for instance require a separate thread in implementing classes, like -" + nameof(PConcurrent) + "-.\r\n",
            Type = typeof(TimeSpan)
        )]
        TimeInterval,
    }
}
