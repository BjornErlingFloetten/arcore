﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using System.Linq;

namespace ARCCore
{

    [Class(
        Description =
            "Contains a few general nice to have extensions methods.\r\n" +
            "\r\n" +
            "TODO: If this class accumulates too many methods, consider splitting it into multiple classes in a separate folder.\r\n" +
            "TODO: For instance a TypeExtensions class and DictionaryExtensions class would be natural.\r\n" +
            "TODO: (together with maybe a StringExtensionClass).\r\n" +
            "\r\n" +
            "NOTE: Message to developers. Think through before you decide on adding anything to this class. It should really be kept as small as possible.\r\n" +
            "NOTE: Often a utility method in a related class is good enough.\r\n" +
            "\r\n" +
            "See also -" + nameof(UtilCore) + "-."
    )]
    public static class Extensions
    {

        private static readonly ConcurrentDictionary<Type, string> _toStringShortCache = new ConcurrentDictionary<Type, string>();
        /// <summary>
        /// TODO: ------- STILL RELEVANT:
        /// See also <see cref="ToStringDB(Type)"/> for a string representation compatible with <see cref="UtilCore.TryGetTypeFromStringFromCache"/>. 
        /// TODO: -------
        /// TODO: We would maybe also want to shorten OuterClass+InnerClass down to InnerClass here. 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        [ClassMember(
            Description =
                "Returns a shorter representation than -" + nameof(ToStringDB) + "-.\r\n" +
                "\r\n" +
                "TODO: INSERT EXAMPLE OF WHAT IS MEANT BY 'SHORTER'.\r\n" +
                "\r\n" +
                "Note how the representation is easier to read for generic types, returning for instance List<string> instead of List`1[string].\r\n" +
                "\r\n" +
                "Note how caches result since operation is somewhat complicated due do generic types.\r\n" +
                "Note how returns a string.Intern in order to make for quicker dictionary lookup as returned string will often be used for lookup in other caches.",
            LongDescription =
                "See also -" + nameof(ToStringDB) + "- ('complete' representation) and -" + nameof(ToStringVeryShort) + "- (shortest form)"
        )]
        public static string ToStringShort(this Type type) => _toStringShortCache.GetOrAdd(type, t =>
        {
            var retval = new System.Text.StringBuilder();
            var maybe = new System.Text.StringBuilder();
            t.ToString().
                Replace("`1[", "<").
                Replace("`2[", "<").
                Replace("`3[", "<").
                Replace("`1+", "+").  // Necessary for 
                Replace("`2+", "+").  // inner classes 
                Replace("`3+", "+").
                Replace("[", "<").
                Replace("]", ">").ForEach(c =>
                {
                    switch (c)
                    {
                        case '.': maybe.Clear(); break; // Remove everything found
                        case ',': // Comma is for multiple generic arguments
                        case '<':
                        case '>': retval.Append(maybe.ToString() + c); maybe.Clear(); break;
                        default: maybe.Append(c); break;
                    }
                });
            return string.Intern(retval.ToString() + maybe.ToString());
        });

        private static readonly ConcurrentDictionary<Type, string> _toStringVeryShortCache = new ConcurrentDictionary<Type, string>();
        [ClassMember(
            Description =
                "Returns same as -" + nameof(ToStringShort) + "- but without any generics information at all.\r\n" +
                "\r\n" +
                "One usage is to identify entity classes (like 'Customer', 'Order' and so on). We want those to be as simple as possible to refer to.\r\n" +
                "\r\n" +
                "NOTE: This method was probably created based on a misunderstanding about the fundamentals of generic classes.\r\n" +
                "NOTE: There is probably really little or no use for it.\r\n" +
                "NOTE: It is used throughout AgoRapide though.\r\n" +
                "\r\n" +
                "Note how caches result since operation is somewhat complicated due do generic types.\r\n" +
                "Note how returns a string.Intern in order to make for quicker dictionary lookup as returned string will often be used for lookup in other caches.",
            LongDescription =
                "See also -" + nameof(ToStringShort) + "- (somewhat longer form) and -" + nameof(ToStringDB) + "- ('complete' representation)"
        )]
        public static string ToStringVeryShort(this Type type) => _toStringVeryShortCache.GetOrAdd(type, t => string.Intern(t.ToStringShort().Split('<')[0]));

        private static readonly ConcurrentDictionary<Type, string> _toStringDBCache = new ConcurrentDictionary<Type, string>();
        [ClassMember(
            Description =
                "Gives the minimum representation of type as string suitable for later reconstruction by -" + nameof(Type.GetType) + "-.\r\n" +
                "(that is with namespace and including all relevant assembly names but nothing more)\r\n" +
                "The returned string even takes into consideration generics when the generic type resides in a different assembly.\r\n" +
                "\r\n" +
                "The return value is considered SQL-\"safe\" in the sense that it may be inserted directly into an SQL statement (not relevant for new AgoRapide (2020) though).\r\n" +
                "\r\n" +
                "Corresponding method for getting the type back is -" + nameof(UtilCore.TryGetTypeFromStringFromCache) + "-.\r\n" +
                "\r\n" +
                "Note how caches result since operation is somewhat complicated due do generic types.\r\n" +
                "Note how returns a string.Intern in order to make for quicker dictionary lookup as returned string will often be used for lookup in other caches.",
            LongDescription =
                "TODO: GIVE BETTER EXAMPLE below because ApplicationPart does not exist in new AgoRapide (2020).\r\n" +
                "\r\n" +
                "Example of return value is:\r\n" +
                "ApplicationPart<P>; : AgoRapide.ApplicationPart`1[AgoRapideSample.P,AgoRapideSample],AgoRapide\r\n" +
                "(the part before : is what is returned by -" + nameof(ToStringShort) + "- and serves as a human readable shorthand)\r\n" +
                "\r\n" +
                "Note complexities involved around generic types where the generic argument exist in a different assembly.\r\n" +
                "For instance for:\r\n" +
                "    typeof(AgoRapide.ApplicationPart{AgoRapideSample.P})\r\n" +
                " where ApplicationPart resides in AgoRapide and\r\n" +
                " P resides in AgoRapideSample a simple approach like\r\n" +
                "   -" + nameof(Type.ToString) + "- , -" + nameof(Type.Assembly) + "-.-" + nameof(System.Reflection.Assembly.GetName) + "-\r\n" +
                " would give\r\n" +
                "   AgoRapide.ApplicationPart`1[AgoRapideSample.P],AgoRapide\r\n" +
                " which is not enough information for -" + nameof(Type.GetType) + "-\r\n" +
                "\r\n" +
                " Therefore, for types where generic arguments exist in a different assembly we have to do like explained here:\r\n" +
                " http://stackoverflow.com/questions/2276384/loading-a-generic-type-by-name-when-the-generics-arguments-come-from-multiple-as\r\n" +
                "\r\n" +
                " In our case what is returned by this methdod is something like mentioned above\r\n" +
                "   AgoRapide.ApplicationPart`1[AgoRapideSample.P,AgoRapideSample],AgoRapide\r\n" +
                " that is, we insert the assembly name where the type of the generic parameter resides, into the string.\r\n"
        )]
        public static string ToStringDB(this Type type) => _toStringDBCache.GetOrAdd(type, t =>
        {
            var retval = t.ToString() + "," + t.Assembly.GetName().Name;
            if (!t.IsGenericType) return ToStringShort(t) + " : " + retval;
            var arguments = t.GetGenericArguments();
            if (arguments.Length != 1) throw new InvalidTypeException(type, nameof(t.GetGenericArguments) + ".Length != 1. Handling of this is just not implemented yet.\r\n");
            if (retval.Split(']').Length != 2) throw new InvalidTypeException(type, retval + " does not contain exactly one right bracket ].\r\n");
            return string.Intern(ToStringShort(t) + " : " + retval.Replace("]", "," + arguments[0].Assembly.GetName().Name + "]"));
        });

        [ClassMember(Description =
        "Useful when we want to write collection.ForEach( ... ) instead of \r\n" +
        "collection.ToList().ForEach ( ... )    (Bad idea performance-wise) \r\n" +
        "or\r\n" +
        "foreach (var t in collection)   which is more verbose \r\n" +
        "\r\n" +
        "Discussion about this at\r\n" +
        "http://blogs.msdn.com/b/ericlippert/archive/2009/05/18/foreach-vs-foreach.aspx\r\n" +
        "(short version http://stackoverflow.com/questions/10299458/is-the-listt-foreach-method-gone\r\n"
        )]
        public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            foreach (var t in collection) action(t);
        }

        [ClassMember(Description =
            "Convenience method that shortens down code in cases where an instance of an object must be " +
            "created first in order to use that same variable multiple times"
        )]
        public static void Use<T>(this T obj, Action<T> action) => action(obj);
        public static T2 Use<T1, T2>(this T1 obj, Func<T1, T2> func) => func(obj);

        public static void AddValue<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue value) where TKey : notnull => AddValue(dictionary, key, value, null);
        [ClassMember(Description = "Gives better error messages compared to 'original' Add when adding a value to a directory if key already exists")]
        public static void AddValue<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, TValue value, Func<string>? detailer) where TKey : notnull
        {
            var a = key.ToString();
            if (dictionary == null) throw new NullReferenceException(nameof(dictionary) + ", Key '" + key.ToString() + detailer.Result("\r\nDetails: ") + "\r\n");
            if (dictionary.ContainsKey(key)) throw new KeyAlreadyExistsException("Key '" + key.ToString() + "' does already exist in dictionary. Dictionary.Count: " + dictionary.Count + " " + dictionary.KeysAsString() + detailer.Result("\r\nDetails: ") + "\r\n");
            dictionary.Add(key, value);
        }

        public class KeyAlreadyExistsException : ApplicationException
        {
            public KeyAlreadyExistsException(string message) : base(message) { }
            public KeyAlreadyExistsException(object key, string collectionName) : base("The key '" + key + "' already exists in collection " + collectionName) { }
            public KeyAlreadyExistsException(object key, string collectionName, string details) : base("The key '" + key + "' already exists in collection " + collectionName + ".\r\nDetails: " + details) { }
        }

        [ClassMember(Description = "Gives a compressed overview of keys in a dictionary. Helpful for building exception messages.")]
        public static string KeysAsString<TKey, TValue>(this Dictionary<TKey, TValue> dictionary)
        {
            if (dictionary.Count > 100) return "Keys.Count: " + dictionary.Keys.Count;
            return "Keys:\r\n" + string.Join(",\r\n", dictionary.Keys);
        }

        /// <summary>
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="key"></param>
        /// <param name="detailer">
        /// May be null
        /// Used to give details in case of an exception being thrown
        /// </param>
        /// <returns></returns>
        [ClassMember(Description = "Gives better error messages when reading value from directory if key does not exist")]
        public static TValue GetValue<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, TKey key, Func<string>? detailer = null) where TKey : notnull
        {
            if (dictionary == null) throw new NullReferenceException(nameof(dictionary) + detailer.Result("\r\nDetails: ") + "\r\n");
            return dictionary.TryGetValue(key, out var retval) ? retval : throw new KeyNotFoundException(UtilCore.BreakpointEnabler + "Key '" + key.ToString() + "' not found in dictionary. Dictionary.Count: " + dictionary.Count + " " + dictionary.KeysAsString() + detailer.Result("\r\n---\r\nDetails: ") + "\r\n");
        }

        /// <summary>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="predicate"></param>
        /// <param name="predicateDescriber">
        /// Used to describe what the predicate matches against. Used to give details in case of an exception being thrown.
        /// </param>
        /// <returns></returns>
        [ClassMember(Description = "Same as inbuilt LINQ's Single except that gives a more friendly exception message")]
        public static T Single<T>(this IEnumerable<T> source, Func<T, bool> predicate, Func<string> predicateDescriber)
        {
            try
            {
                return source.Single(predicate);
            }
            catch (Exception ex)
            {
                throw new SingleObjectNotFoundOrMultipleFoundException(predicateDescriber.Result(nameof(predicateDescriber) + ": "), ex);
            }
        }

        /// <summary>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="predicate"></param>
        /// <param name="predicateDescriber">
        /// Used to describe what the predicate matches against. Used to give details in case of an exception being thrown.
        /// </param>
        /// <returns></returns>
        [ClassMember(Description = "Same as inbuilt LINQ's SingleOrDefault except that gives a more friendly exception message")]
        public static T SingleOrDefault<T>(this IEnumerable<T> source, Func<T, bool> predicate, Func<string> predicateDescriber)
        {
            try
            {
                return source.SingleOrDefault(predicate);
            }
            catch (Exception ex)
            {
                throw new MultipleObjectsFoundException(predicateDescriber.Result(nameof(predicateDescriber) + ": "), ex);
            }
        }

        [ClassMember(Description = "Returns heading plus return-value of function (calls function). If function == null then empty string is returned")]
        public static string Result(this Func<string>? function, string header)
        {
            if (function == null) return "";
            return header + function();
        }

        /// <summary>
        /// Extracts content between first occurence of <paramref name="start"/> and <paramref name="end"/>
        /// Returns false if does not find <paramref name="start"/> or <paramref name="end"/>
        /// </summary>
        /// <param name="text"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static bool TryExtract(this string text, string start, string end, out string retval)
        {
            if (string.IsNullOrEmpty(start)) throw new ArgumentNullException(nameof(start) + (start == null ? "" : ".IsEmpty.\r\n"));
            if (string.IsNullOrEmpty(end)) throw new ArgumentNullException(nameof(end) + (end == null ? "" : ".IsEmpty.\r\n"));
            var startPos = text?.IndexOf(start) ?? throw new ArgumentNullException(nameof(text) + "\r\n");
            if (startPos == -1)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            var endPos = text.IndexOf(end, startPos + start.Length);
            if (endPos == -1)
            {
                retval = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            retval = text.Substring(startPos + start.Length, endPos - startPos - start.Length);
            return true;
        }

        [ClassMember(Description =
            "Uses format 'yyyy-MM-dd HH:mm:ss.fff'.\r\n" +
            "\r\n" +
            "See also -" + nameof(UtilCore.DateTimeTryParse) + "-.\r\n"
        )]
        public static string ToStringDateAndTime(this DateTime dateTime) => dateTime.ToString("yyyy-MM-dd HH:mm:ss.fff"); // TODO: Create separate CONST for the format

        [ClassMember(Description =
            "Uses either format 'd\\.hh\\:mm\\:ss\\.fff' or 'hh\\:mm\\:ss\\.fff' depending on TotalHours >=24 hours or not." +
            "\r\n" +
            "See also -" + nameof(UtilCore.TimeSpanTryParse) + "-.\r\n"
        )]
        public static string ToStringWithMS(this TimeSpan timespan) => timespan.TotalHours >= 24 ?
            timespan.ToString("d\\.hh\\:mm\\:ss\\.fff") : // TODO: Create separate CONST for the format
            timespan.ToString("hh\\:mm\\:ss\\.fff");  // TODO: Create separate CONST for the format
    }
}