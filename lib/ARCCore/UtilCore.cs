﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using System.Linq;

/// <summary>
/// Contains:
/// <see cref="ARCCore.UtilCore"/>
/// <see cref="ARCCore.REx"/>
/// <see cref="ARCCore.InvalidEnumException"/>
/// <see cref="ARCCore.InvalidEnumException{T}"/>
/// <see cref="ARCCore.InvalidTypeException"/>
/// <see cref="ARCCore.InvalidObjectTypeException"/>
/// <see cref="ARCCore.NotOfTypeEnumException"/>
/// <see cref="ARCCore.OfTypeEnumException"/>
/// <see cref="ARCCore.InvalidCountException"/>
/// <see cref="ARCCore.NotNullReferenceException"/>
/// <see cref="ARCCore.UniquenessException"/>
/// <see cref="ARCCore.SingleObjectNotFoundOrMultipleFoundException"/>
/// <see cref="ARCCore.MultipleObjectsFoundException"/>
/// <see cref="ARCCore.TValue"/>
/// <see cref="ARCCore.TPropertyKeyEnum"/>
/// </summary>
namespace ARCCore
{

    [Class(
        Description =
            "Utility methods for -" + nameof(ARComponents.ARCCore) + "-.\r\n" +
            "\r\n" +
            "Methods and functionality that we are unable to find another natural home for.\r\n",
        LongDescription =
            "NOTE: Message to developers. Think through before you decide on adding anything to this class. It should really be kept as small as possible.\r\n" +
            "NOTE: Consider splitting up into multiple smaller classes if grows too big.\r\n" +
            "\r\n" +
            "See also -" + nameof(Extensions) + "-."
    )]
    public static class UtilCore
    {

        private static bool _useLocalTimeInsteadOfUTC = false;
        [ClassMember(Description =
            "By default AgoRapide operates only with UTC time.\r\n" +
            "\r\n" +
            "This is the recommended approach as UTC is always unambigious.\r\n" +
            "\r\n" +
            "This setting is for instance relevant to use if you synchronize data from data sources which do not support UTC, " +
            "in which case it would be difficult for -" + nameof(StreamProcessor) + "- to insert correct -" + nameof(StreamProcessorP.Timestamp) + "- " +
            "at initial synchronization.\r\n" +
            "\r\n" +
            "If you change this setting to TRUE then ALL uses of time in AgoRapide will be based on local time " +
            "(because -" + nameof(DateTimeNow) + "- is used).\r\n" +
            "\r\n" +
            "This setting can only be changed at application startup and only set to TRUE. Default value is FALSE."
        )]
        public static bool UseLocalTimeInsteadOfUTC
        {
            get => _useLocalTimeInsteadOfUTC;
            set
            {
                AssertCurrentlyStartingUp();
                if (!value) throw new UseLocalTimeInsteadOfUTCCanOnlyBeSetToTrueException();
                _useLocalTimeInsteadOfUTC = value;
            }
        }

        private class UseLocalTimeInsteadOfUTCCanOnlyBeSetToTrueException : ApplicationException { }
        [ClassMember(Description =
            "Returns DateTime.UtcNow unless -" + nameof(UseLocalTimeInsteadOfUTC) + "- has been set to TRUE in which case DateTime.Now is returned."
        )]
        public static DateTime DateTimeNow => _useLocalTimeInsteadOfUTC ? DateTime.Now : DateTime.UtcNow;

        // NOTE: Not sufficiently frequently used in order to defend a place in Util
        //[ClassMember(Description =
        //    "Returns -" + nameof(DateTimeNow) + "- appended with 'UTC' or '(Local time)' like '2020-12-09 12:30.21 UTC' or '2020-12-09 12:30.21 (Local time)'"
        //)]
        //public static string DateTimeNowWithDescription => DateTimeNow.ToStringDateAndTime() + " " + (_useLocalTimeInsteadOfUTC ? "(Local time)" : "UTC");

        private static bool _currentlyStartingUp = true;
        [ClassMember(Description =
            "Called once at application startup after all " + nameof(Attribute) + " instances in the C# code has been processed.\r\n" +
            "Afterwards attributes can only be accessed through the " + nameof(IP) + "-based -" + nameof(PKTypeAttribute) + "-.")]
        public static bool CurrentlyStartingUp
        {
            get => _currentlyStartingUp;
            set
            {
                if (value == true) throw new StartingUpException("Invalid value (" + value + "), you can only set value to FALSE.\r\n");
                if (_currentlyStartingUp == false) throw new StartingUpException(nameof(_currentlyStartingUp) + " already set (to " + _currentlyStartingUp + "). You can only set value once (to false).\r\n");
                // Bug-fix 02 Feb 2022: Added the following line:
                _currentlyStartingUp = value;
            }
        }

        public class StartingUpException : ApplicationException
        {
            public StartingUpException(string message) : base(message) { }
        }

        [ClassMember(
            Description =
                "Normally called from non-thread safe methods that should be run single-threaded at application startup only.",
            LongDescription =
                "NOTE: If you want to remove a call to this method somewhere in the AgoRapide code " +
                "then you must first make that code, and all corresponding collections and methods, thread-safe first.\r\n" +
                "NOTE: Most probably you should NEVER remove any such calls as there is also a performance advantage of finished initialization " +
                "in \"peace and quiet\" at application startup.")]
        public static void AssertCurrentlyStartingUp()
        {
            if (!CurrentlyStartingUp) throw new SomeCodeOnlyToBeRunAtStartupHasBeenCalledAfterStartupFinishedException();
        }
        private class SomeCodeOnlyToBeRunAtStartupHasBeenCalledAfterStartupFinishedException : ApplicationException { }

        public static void AssertNotCurrentlyStartingUp()
        {
            if (CurrentlyStartingUp) throw new SomeCodeNotToBeRunAtStartupHasBeenCalledBeforeStartupFinishedException();
        }
        private class SomeCodeNotToBeRunAtStartupHasBeenCalledBeforeStartupFinishedException : ApplicationException { }

        [ClassMember(Description = "Used by -" + nameof(DoubleTryParse) + "-.")]
        public static System.Globalization.CultureInfo Culture_en_US = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");

        public static double DoubleParse(string _string) => DoubleTryParse(_string, out var retval) ? retval : throw new InvalidDoubleException(_string);
        [ClassMember(Description = "Note that treats ',' (comma) as decimal point (not as thousands separator).")]
        public static bool DoubleTryParse(string _string, out double retval) => double.TryParse(_string.Replace(",", "."),
            /// TODO: Consider also allowing 
            /// <see cref="System.Globalization.NumberStyles.Float"/> (Scientific notation)
            /// but check performance impact first
            System.Globalization.NumberStyles.AllowDecimalPoint |
            System.Globalization.NumberStyles.Number |
            System.Globalization.NumberStyles.AllowLeadingSign,
            Culture_en_US,
            out retval);

        public class InvalidDoubleException : ApplicationException
        {
            public InvalidDoubleException(string message) : base(message) { }
            public InvalidDoubleException(string message, Exception inner) : base(message, inner) { }
        }

        public static T EnumParse<T>(string _string) where T : struct, Enum => EnumTryParse(_string, out T retval, out var errorResponse) ? retval : throw new InvalidEnumException<T>(_string + ". Details: " + errorResponse + "\r\n");
        public static bool EnumTryParse<T>(string _string, out T result) where T : struct, Enum => EnumTryParse(_string, out result, out _);
        [ClassMember(Description =
            "Same as the inbuilt .NET method -" + nameof(Enum.TryParse) + "- but with some additional checks.\r\n" +
            "\r\n" +
            "Will check that:\r\n" +
            "1) The value is defined (because -" + nameof(Enum.TryParse) + "- returns true for all integers, something which is deemed to lax and will lead to errors)\r\n" +
            "2) The int-value is not 0 / not '__invalid'.\r\n" +
            "Note: All AgoRapide enums start with '__invalid' (as 0) in order to catch missing setting of values " +
            "(the default integer value of 0 is by default an invalid AgoRapide enum member / enum value).\r\n"
        )]
        public static bool EnumTryParse<T>(string _string, out T result, out string errorResponse) where T : struct, Enum
        {
            if (!typeof(T).IsEnum) throw new NotOfTypeEnumException<T>();
            if (!System.Enum.TryParse(_string, out result))
            { // Duplicate code below
                errorResponse = "Not a valid " + typeof(T) + " (" + _string + ")";
                return false;
            }
            if (!System.Enum.IsDefined(typeof(T), result))
            { // Duplicate code below
                errorResponse = "!" + nameof(System.Enum.IsDefined) + " for " + typeof(T) + " (" + result + ")";
                return false;
            }
            if (((int)(object)result) == 0 && result.ToString().Equals("__invalid"))
            { // Duplicate code below
                errorResponse =
                    "0 ('__invalid') is not allowed for " + typeof(T) + " " +
                    "(Note: All AgoRapide enums start with '__invalid' in order to catch missing setting of values. " +
                    "This value is considered illegal in a parsing context).";
                return false;
            }
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        public static object EnumParse(Type type, string _string) => EnumTryParse(type, _string, out var retval, out var errorResponse) ? retval : throw new InvalidEnumException(type, _string + ". Details: " + errorResponse + "\r\n");
        public static bool EnumTryParse(Type type, string _string, out object result) => EnumTryParse(type, _string, out result, out _);
        /// <summary>
        /// See also generic overload <see cref="EnumTryParse{T}(string, out T, out string)"/> which most often is more practical.
        /// Also see that generic overload for documentation.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="_string"></param>
        /// <param name="result"></param>
        /// <param name="errorResponse"></param>
        /// <returns></returns>
        public static bool EnumTryParse(Type type, string _string, out object result, out string errorResponse)
        {
            NotOfTypeEnumException.AssertEnum(type);
            try
            {
                result = System.Enum.Parse(type, _string);
            }
            catch (Exception)
            {
                result = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                errorResponse = "Not a valid " + type + " (" + _string + ")"; // Duplicate code above
                return false;
            }
            if (!System.Enum.IsDefined(type, result))
            { // Duplicate code above
                errorResponse = "!" + nameof(System.Enum.IsDefined) + " for " + type + " (" + result + ")";
                return false;
            }
            if (((int)result) == 0 && result.ToString().Equals("__invalid"))
            { // Duplicate code above
                errorResponse =
                    "0 ('__invalid') is not allowed for " + type + " " +
                    "(Note: All AgoRapide enums start with '__invalid' in order to catch missing setting of values. " +
                    "This value is considered illegal in a parsing context).";
                return false;
            }
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        private static readonly ConcurrentDictionary<Type, List<object>> _enumMembersCache = new ConcurrentDictionary<Type, List<object>>();
        /// <summary>
        /// See <see cref="EnumGetMembers{T}(T)"/> for information about <paramref name="exclude"/>
        /// </summary>
        /// <param name="type"></param>
        /// <param name="exclude">See <see cref="EnumGetMembers{T}(T)"/> for information about this parameter</param>
        /// <returns></returns>
        public static List<object> EnumGetMembers(Type type, int exclude = 0) => _enumMembersCache.GetOrAdd(type, t =>
        {
            NotOfTypeEnumException.AssertEnum(t);
            var retval = new List<object>();
            foreach (var o in System.Enum.GetValues(type))
            {
                if (!(((int)o) == exclude)) retval.Add(o);
            }
            return retval;
        });

        private static readonly ConcurrentDictionary<Type, object> _enumMembersTCache = new ConcurrentDictionary<Type, object>();
        /// <summary>
        /// Note how all enums in AgoRapide start with None as first value, in order to not confuse default value (int 0) 
        /// with some presumed valid value. 
        /// Correspondingly, this first value (default value) is excluded by this method. 
        /// See <paramref name="exclude"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="exclude">
        /// Note how the default value is excluded (the int 0 value or '__invalid' in AgoRapide. 
        /// Set this parameter to (T)(object)(-1) if you want ALL values included 
        /// (assumed that you actually do not use the value -1 in your enum of course).
        /// Note: All AgoRapide enums start with '__invalid' (as 0) in order to catch missing setting of values.
        /// (the default integer value of 0 is by default an invalid AgoRapide enum member / enum value).
        /// </param>
        /// <returns></returns>
        public static List<T> EnumGetMembers<T>(T exclude = default) where T : struct, Enum
        {
            var temp = _enumMembersTCache.GetOrAdd(typeof(T), t =>
            {
                var type = typeof(T);
                NotOfTypeEnumException.AssertEnum(type);
                var retval = new List<T>();
                foreach (var value in System.Enum.GetValues(type))
                {
                    if (value.Equals(exclude)) continue;
                    retval.Add((T)value);
                }
                return retval;
            });
            return temp as List<T> ?? throw new InvalidObjectTypeException(temp, typeof(List<T>));
        }

        public static Type GetTypeFromStringNoCache(string strType) => TryGetTypeFromStringNoCache(strType, out var retval) ? retval : throw new InvalidTypeException(strType);
        [ClassMember(Description = "See -" + nameof(TryGetTypeFromStringFromCache) + "- for documentation.\r\n")]
        public static bool TryGetTypeFromStringNoCache(string strType, out Type type)
        {
            var temp = new Func<Type?>(() =>
            {
                if (IP.AllIPDerivedTypesDict.TryGetValue(strType, out var retval))
                {
                    return retval;
                }

                var t = strType.Split(':'); // Remove result of ToStringShort placed in front of strType. 
                switch (t.Length)
                {
                    case 1:
                        // Did not contain any ':'
                        break;
                    case 2:
                        strType = t[1].Trim(); break;
                    default:
                        /// Note: Old AgoRapide would throw exception here, but we may get called for all kind of values
                        /// (due to methods like <see cref="PropertyStreamLine.ParseAndStore"/>)
                        // throw new InvalidTypeException(s, "Invalid number of colons : (" + (t.Length - 1) + ").\r\n");
                        return null;
                }
                if (!strType.Contains('['))
                {
                    // Note that returns null if not found
                    return Type.GetType(strType);
                }
                if (!strType.TryExtract("[", "]", out var genericArguments)) throw new InvalidTypeException(strType, nameof(genericArguments) + " not found between left bracket [ and right bracket ]\r\n");
                var genericBaseTypeAndAssemblyName = strType.Replace("[" + genericArguments + "]", "");
                t = genericBaseTypeAndAssemblyName.Split(',');
                switch (t.Length)
                {
                    case 2:
                        var genericBaseType = Type.GetType(t[0].Trim() + "," + t[1].Trim());
                        if (genericBaseType == null) return null;
                        t = genericArguments.Split(',');
                        switch (t.Length)
                        {
                            case 2:
                                var genericArgument = Type.GetType(genericArguments);
                                if (genericArgument == null) return null;
                                return genericBaseType.MakeGenericType(new Type[] { genericArgument });
                            default: throw new InvalidTypeException(strType, "Invalid number of commas : (" + (t.Length - 1) + ") (assembly name not found in " + nameof(genericArguments) + " '" + genericArguments + "') (code only handles one generic argument at present).\r\n");
                        }
                    default: throw new InvalidTypeException(strType, "Invalid number of commas : (" + (t.Length - 1) + ") (assembly name not found in " + nameof(genericBaseTypeAndAssemblyName) + " '" + genericBaseTypeAndAssemblyName + "').\r\n");
                }
            })();
            if (temp == null)
            {
                type = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            type = temp;
            return true;
        }

        public static ConcurrentDictionary<string, Type?> _typeToStringCache = new ConcurrentDictionary<string, Type?>();
        public static Type GetTypeFromStringFromCache(string strType) => TryGetTypeFromStringFromCache(strType, out var retval) ? retval : throw new InvalidTypeException(strType);
        /// <summary>
        /// See <see cref="Extensions.ToStringDB"/> for documentation. 
        /// 
        /// Throws exception if invalid syntax for strType. 
        /// </summary>
        /// <param name="strType"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [ClassMember(
            Description =
                "Corresponding method for creating string-representation of type is -" + nameof(Extensions.ToStringDB) + "-.\r\n" +
                "(note that all -" + nameof(IP.AllIPDerivedTypes) + "- are understood also in short-hand form.\r\n" +
                "\r\n" +
                "NOTE: Note how result is cached. Use overload -" + nameof(TryGetTypeFromStringNoCache) + "- " +
                "NOTE: if input parameter is not necessarily a type / if it has a potential huge range of values."
        )]
        public static bool TryGetTypeFromStringFromCache(string strType, out Type type)
        {
            var temp = _typeToStringCache.GetOrAdd(strType, s =>
            {
                return TryGetTypeFromStringNoCache(s, out var retval) ? retval : null;
            });
            if (temp == null)
            {
                type = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
                return false;
            }
            type = temp;
            return true;
        }

        private static IEnumerable<System.Reflection.Assembly>? _assemblies;
        [ClassMember(Description =
            "All relevant assemblies for application.\r\n" +
            "\r\n" +
            "Should be set at startup of application in a single threaded context.\r\n" +
            "\r\n" +
            "Note that setting this value will also lead to other properties being constructed / methods being called like " +
            "-" + nameof(IP.AllIPDerivedTypes) + "-," +
            "-" + nameof(IP.AllIPDerivedTypesWithPKEnum) + "-," +
            "-" + nameof(IP.AllIPDerivedTypesWithPKEnumApplicationSpecific) + "-," +
            "-" + nameof(IP.AllIPDerivedTypesDict) + "-," +
            "-" + nameof(IP.AllIPDerivedTypesDictIKType) + "- and " +
            "-" + nameof(IP.AllIPDerivedTypesInludingGenericAndAbstract) + "- and " +
            "-" + nameof(PK.AllPKEnums) + "-.\r\n" +
            "-" + nameof(PK.BuildFromStringCache) + "-.\r\n" +
            "and -" + nameof(UtilCore.CurrentlyStartingUp) + "- will be set to FALSE.\r\n" +
            "In other words, this routine is something like a global init-method for the application"
        )]
        public static IEnumerable<System.Reflection.Assembly> Assemblies
        {
            get => _assemblies ?? throw new NullReferenceException(nameof(Assemblies) + ". Should have been set at startup of application.\r\n");
            set
            {
                NotNullReferenceException.AssertNotNull(_assemblies);
                AssertCurrentlyStartingUp();
                _assemblies = value.Distinct().ToList(); // ToList in order to escape eventual deferred execution / simplify later evaluation

                // Force other initialization also. 
                // This removes any doubt about thread safety at initialization.
                var _1 = IP.AllIPDerivedTypes;
                var _2 = IP.AllIPDerivedTypesWithPKEnum;
                var _3 = IP.AllIPDerivedTypesWithPKEnumApplicationSpecific;
                var _4 = IP.AllIPDerivedTypesDict;
                var _5 = IP.AllIPDerivedTypesDictIKType;
                var _6 = IP.AllIPDerivedTypesInludingGenericAndAbstract;
                var _7 = IP.AllIPDerivedEntityCollectionClassesDict;
                var _8 = PK.AllPKEnums;
                PK.BuildFromStringCache();
                
                // Removed 02 Feb 2022. Responsibility for setting value to FALSE has been moved to
                // each separate application
                // CurrentlyStartingUp = false;
            }
        }

        [ClassMember(Description =
            "Follows the chain of " + nameof(Exception.InnerException) + " and returns types as comma-separated string"
        )]
        public static string GetExceptionChainAsString(Exception ex)
        {
            var retval = new StringBuilder();
            while (ex != null)
            {
                if (retval.Length > 0) retval.Append(", ");
                retval.Append(ex.GetType().ToStringShort());
                ex = ex.InnerException;
            }
            return retval.ToString();
        }

        [ClassMember(Description = "Gives as much information about exception as possible.")]
        public static string GetExeptionDetails(Exception ex)
        {
            var retval = new StringBuilder();
            var timeStamp = UtilCore.DateTimeNow.ToString("yyyy-MM-dd HH:mm:ss");
            retval.AppendLine(GetExceptionChainAsString(ex));
            retval.AppendLine(timeStamp + ":" + "Thread " + System.Threading.Thread.CurrentThread.Name + "/" + System.Threading.Thread.CurrentThread.ManagedThreadId.ToString() + ":\r\n\r\n");
            while (ex != null)
            {
                retval.AppendLine("Exception: " + ex.GetType().ToStringShort() + "\r\n");
                retval.AppendLine("Message: " + ex.Message.ToString() + "\r\n");
                if (ex.Data != null && ex.Data.Count > 0)
                {
                    retval.Append("Data:\r\n");
                    foreach (var temp in ex.Data)
                    {
                        var e = (System.Collections.DictionaryEntry)temp; // Going through "var temp" in order to escape compilator warning (Dec 2016)
                        retval.Append(e.Key.ToString() + ": " + e.Value.ToString() + "\r\n");
                    }
                    retval.AppendLine();
                }
                retval.AppendLine("Source : " + ex.Source + "\r\n");

                if (ex is AggregateException ae)
                {
                    retval.AppendLine("=================");
                    retval.AppendLine(ex.GetType().ToStringShort() + ", details will only be shown for the first inner Exception. All InnerExceptions are:");
                    ae.InnerExceptions.ForEach(inner =>
                    {
                        retval.AppendLine(inner.GetType().ToStringShort() + ": " + inner.Message);
                    });
                    retval.AppendLine("=================");
                }

                var stackTrace = ex.StackTrace + "";

                // Idea from old AgoRapide giving shorter stack traces, nice but really not necessary
                // Configuration.C.SuperfluousStackTraceStrings.ForEach(s => stackTrace = stackTrace.Replace(s, ""));

                // Remove parameters. We assume that line numbers are sufficient.
                var start = stackTrace.IndexOf("(");
                while (start > -1)
                {
                    var slutt = stackTrace.IndexOf(")", start);
                    if (slutt == -1) break;
                    stackTrace = stackTrace[..start] + stackTrace[(slutt + 1)..];
                    start = stackTrace.IndexOf("(");
                }

                retval.AppendLine("Stacktrace: " + stackTrace.Replace(" at ", "\r\n\r\nat ").Replace(" in ", "\r\nin ") + "\r\n");

                ex = ex.InnerException;
                if (ex != null)
                {
                    retval.AppendLine();
                    retval.AppendLine("INNER EXCEPTION:");
                }
            }
            retval.AppendLine();
            retval.AppendLine("-----------------------------------------------------------");

            return retval.ToString();
        }

        [ClassMember(
            Description =
                "The standard encoding used for text (UTF8).\r\n" +
                "\r\n" +
                "TODO: Research optimal encoding. For instance if -" + nameof(ARConcepts.PropertyStream) + "- is encoded with only " +
                "ASCII-characters " +
                "(see " +
                "-" + nameof(PropertyStreamLine.EncodeKeyPart) + "- and " +
                "-" + nameof(PropertyStreamLine.EncodeValuePart) + "-" +
                ") " +
                "then maybe -" + nameof(Encoding.ASCII) + "- is good enough?"
        )]
        public static Encoding DefaultAgoRapideEncoding = Encoding.UTF8;

        [ClassMember(Description =
            "'yyyy-MM-dd HH:mm:ss.fff', " +
            "'yyyy-MM-dd HH:mm:ss', " +
            "'yyyy-MM-dd HH:mm', " +
            "'yyyy-MM-dd'\r\n" +
            "\r\n" +
            "See also -" + nameof(DateTimeTryParse) + "-.\r\n" +
            "\r\n" +
            "TODO: The formats are supposed to be used also for parsing human user input (through an API for instance).\r\n" +
            "TODO: Add support for user-configurable formats"
        )]
        public static string[] ValidDateTimeFormats = new string[] {
            "yyyy-MM-dd HH:mm:ss.fff",
            "yyyy-MM-dd HH:mm:ss",
            "yyyy-MM-dd HH:mm",
            "yyyy-MM-dd"
        };

        [ClassMember(Description =
            "Accepts on of the formats specified in  -" + nameof(ValidDateTimeFormats) + "-.\r\n" +
            "\r\n" +
            "See also -" + nameof(Extensions.ToStringDateAndTime) + "-."
        )]
        public static bool DateTimeTryParse(string s, out DateTime retval) =>
            DateTime.TryParseExact(s, ValidDateTimeFormats, Culture, System.Globalization.DateTimeStyles.None, out retval);

        [ClassMember(Description =
            "'" + @"hh\:mm" + "', " +
            "'" + @"hh\:mm\:ss" + "', " +
            "'" + @"d\.hh\:mm\:ss" + "', " +
            "'" + @"hh\:mm\:ss\.fff' // Milliseconds," +
            "'" + @"d\.hh\:mm\:ss\.fff" + "'\r\n" +
            "\r\n" +
            "See also -" + nameof(TimeSpanTryParse) + "-.\r\n"
        )]
        public static string[] ValidTimeSpanFormats = new string[] {
            @"hh\:mm",
            @"hh\:mm\:ss",
            @"d\.hh\:mm\:ss",
            @"hh\:mm\:ss\.fff", // Milliseconds 
            @"d\.hh\:mm\:ss\.fff"
        };

        [ClassMember(Description =
            "Accepts on of the formats specified in  -" + nameof(ValidDateTimeFormats) + "-.\r\n" +
            "\r\n" +
            "See also -" + nameof(Extensions.ToStringWithMS) + "-."
        )]
        public static bool TimeSpanTryParse(string s, out TimeSpan retval) =>
            TimeSpan.TryParseExact(s, ValidTimeSpanFormats, System.Globalization.CultureInfo.InvariantCulture, out retval);

        [ClassMember(Description =
            "The general culture used for parsing.\r\n" +
            "\r\n" +
            "As of Mar 2021 only used by -" + nameof(DateTimeTryParse) + "-.\r\n" +
            "\r\n" +
            "TODO: Add configurable support for other cultures, but beware how this culture is used currently."
        )]
        public static System.Globalization.CultureInfo Culture { get; } = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");


        /// <summary>
        /// Practical property that facilitates placement of breakpoints in expressions in addition to statements
        /// 
        /// Insert this wherever you throw an exception in expressions, especially when using the ? operator
        ///   {boolean expression} ? {some return value} : throw SomeException(Util.BreakpointEnabler + {Your original exception message} 
        /// 
        /// At regular intervals you can remove all uses of this method in the code.
        /// </summary>
        public static string BreakpointEnabler => ""; // <--- Place breakpoint here <---
    }

    [Class(
        Description =
            "REx = 'TooDeepRecursiveDepthException'\r\n" +
            "\r\n" +
            "Functionality that helps to guard against infinite recursion.\r\n" +
            "Usage:\r\n" +
            "\r\n" +
            "void RecursiveMethod() {\r\n" +
            "  try {\r\n" +
            "    REx.Inc()\r\n" +
            "    .... Some code calling RecursiveMethod again, either directly or indirectly ...\r\n " +
            "  } finally {\r\n" +
            "    REx.Dec()\r\n" +
            "  }\r\n" +
            "}\r\n" +
            "\r\n" +
            "Alternatively, you can just do:\r\n" +
            "   REx.Inc() + RecursiveMethod() + REx.Inc()\r\n" +
            "whenever RecursiveMethod returns a string.\r\n" +
            "This later approach is useful in expressions, meaning you do not have to build statements in order to use this class. If exceptions should occur " +
            "in RecursiveMethod however, the recursive counter may 'leak' upwards.",
        LongDescription =
            "Note how the class uses a recursion counter that is 1) static and 2) common for the whole application.\r\n" +
            "\r\n" +
            "1) Using a static counter is due to the fact that some methods may be recursive without directly calling itself. " +
            "For instance methodA could call B, which calls C, which then calls A without knowing about call coming from A " +
            "(This means that passing a recursion counter would not help because it would get lost on its way. " +
            "\r\n" +
            "2) Having a common counter for the whole application is just a pragmatic choice." +
            "\r\n" +
            "One has to be aware of threading of course, that is, the max recursion depth (default = 100) has to take into account other threads, and therefore be set " +
            "to a somewhat higher value than the one suitable for single-threaded environments.\r\n" +
            "\r\n" +
            "NOTE: Infinite recursion problems manifest themselves as a StackOverflowException (unless caught by safeguards like this)."
    )]
    public class REx : ApplicationException
    {
        private static int _currentRecursionDepth;
        public static int CurrentRecursionDepth => _currentRecursionDepth;
        public static void ResetRurrentRecursionDepth() => _currentRecursionDepth = 0;

        public REx(int recursionDepth, [System.Runtime.CompilerServices.CallerMemberName] string caller = "") : base(
            "The method '" + caller + "' is a recursive method and therefore suspectible to infinite recursion.\r\n" +
            "A recursion depth of " + recursionDepth + " has now been recorded, and that is suspected to be caused by a bug in the code."
        )
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentRecursionDepth"></param>
        /// <param name="maxRecursionDepth">
        /// Remember to take into account multi-threading issues when setting this parameter. 
        /// It must be somewhat bigger then what is needed for a single-threaded environeent. 
        /// If you experience spurious TooDeepRecursionExceptions in a heavy loaded multi-threaded environemnt, then just increase this value.
        /// 
        /// TODO: Make this possible to change by a configuration system. 
        /// TODO: In order to keep ARCore as simple a configuration system should not be part of ARCore. We can however, in ARCore
        /// TODO: define the interface necessary for receiving configuration information.
        /// </param>
        /// <param name="caller"></param>
        [ClassMember(Description = "Increases recursive count.")]
        public static string? Inc(int maxRecursionDepth = 100, [System.Runtime.CompilerServices.CallerMemberName] string caller = "")
        {
            System.Threading.Interlocked.Increment(ref _currentRecursionDepth);
            if (CurrentRecursionDepth > maxRecursionDepth)
            {
                var t = _currentRecursionDepth;
                // TODO: Should we use System.Threading.Interlocked for this?
                _currentRecursionDepth = 0; // Important, reset, because corresponding Dec may not be called if it is not in a finally-block. If Dec sees that it goes negative it will be set to 0.
                throw new REx(
                    // Purists would argue that currentRecursionDepth may now have changed its value through another thread, 
                    // (because we use a static counter) so the value shown is not necessarily maxRecursionDepth-1, it could be greater or less.
                    t,
                    caller
                );
            }
            return null;
        }
        [ClassMember(Description =
            "This method should be called from inside a 'finally' block, in order to secure its execution, " +
            "if not then your static recursion depth counter could slowly increase over time due to exceptions ocurring after " +
            "call to -" + nameof(Inc) + "- but before call to -" + nameof(Dec) + "-."
        )]
        public static string? Dec()
        {
            System.Threading.Interlocked.Decrement(ref _currentRecursionDepth);
            // TODO: Should we use System.Threading.Interlocked for this?
            if (CurrentRecursionDepth < 0) _currentRecursionDepth = 0; /// Will happen if <see cref="Inc"/> reset the value, but Dec was called anyway from finally-blocks as the stack unwinded.
            return null;
        }
    }

    public class InvalidEnumException : ApplicationException
    {
        public static void AssertDefined<T>(T _enum) where T : Enum
        {
            if (!System.Enum.IsDefined(typeof(T), _enum)) throw new InvalidEnumException(_enum, "Not defined.\r\n");
            if (((int)(object)_enum) == 0) throw new InvalidEnumException(_enum, "int value is 0. Note: All AgoRapide enums has '__invalid' as the first value / default value 0. This is considered an invalid value\r\n");
        }
        private static string GetMessage(object _enum, string? message) => "Invalid / unknown value for enum (" + _enum.GetType().ToString() + "." + _enum.ToString() + ")." + (string.IsNullOrEmpty(message) ? "" : ("\r\nDetails: " + message));
        public InvalidEnumException(object _enum) : base(GetMessage(_enum, null)) { }
        public InvalidEnumException(object _enum, string message) : base(GetMessage(_enum, message)) { }
        public InvalidEnumException(Type type, string _string) : base("Unable to parse '" + _string + "' as " + type) { }
        public InvalidEnumException(Type type, string _string, string details) : base("Unable to parse '" + _string + "' as " + type + ".\r\nDetails:\r\n" + details) { }
    }

    public class InvalidEnumException<T> : ApplicationException
    {
        public InvalidEnumException(string _string) : base("Value '" + _string + "' is not valid for Enum " + typeof(T).ToString()) { }
    }

    public class InvalidTypeException : ApplicationException
    {
        [ClassMember(Description = "Asserts that expectedType.IsAssignableFrom(foundType)")]
        public static void AssertAssignable(Type foundType, Type expectedType, Func<string>? detailer = null)
        {
            if (!expectedType.IsAssignableFrom(foundType)) throw new InvalidTypeException(foundType, expectedType, detailer.Result(""));
        }

        /// <summary>
        /// </summary>
        /// <param name="foundType"></param>
        /// <param name="expectedType"></param>
        /// <param name="detailer">
        /// May be null
        /// Used to give details in case of an exception being thrown
        /// </param>
        [ClassMember(Description = "Asserts that expectedType.Equals(foundType)")]
        public static void AssertEquals(Type foundType, Type expectedType, Func<string>? detailer)
        {
            if (!expectedType.Equals(foundType)) throw new InvalidTypeException(foundType, expectedType, detailer.Result("") + "\r\n");
        }

        public static bool TryAssertEquals(Type foundType, Type expectedType, out string errorResponse, Func<string>? detailer)
        {
            if (!expectedType.Equals(foundType))
            {
                errorResponse =
                    nameof(expectedType) + ": " + expectedType.ToString() + ",\r\n" + nameof(foundType) + ": " + foundType + ",\r\n" +
                    nameof(detailer) + ": " + detailer.Result("") + "\r\n";
                return false;
            }
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }


        public InvalidTypeException(string typeFound) : this(typeFound, null) { }
        public InvalidTypeException(string typeFound, string? details) : base(
            "Unable to reconstruct type based on " + nameof(typeFound) + " (" + typeFound + "). " +
            "Possible cause " +
            "(if " + nameof(typeFound) + " originates from " + nameof(ARConcepts.PropertyStream) + " " +
            "and is result of " + nameof(Extensions.ToStringDB) +
            "): " +
            "Assembly name may have changed since stored in database" + (string.IsNullOrEmpty(details) ? "" : (". Details: " + details)))
        { }

        public InvalidTypeException(Type type) : base("Type:" + type.ToStringShort()) { }

        public InvalidTypeException(Type type, string details) : base(
            "Type: " + type.ToStringShort() + ", " + nameof(details) + ": " + details)
        { }

        public InvalidTypeException(Type foundType, Type expectedType, string? details = null) : base(
            nameof(expectedType) + ": " + expectedType.ToString() + ",\r\n" +
            nameof(foundType) + ": " + foundType +
            (details is null ? "" : (",\r\n" + nameof(details) + ": " + details)))
        { }
    }

    public class InvalidObjectTypeException : ApplicationException
    {
        /// <summary>
        /// Asserts that expectedType.IsAssignableFrom(foundObject.GetType())
        /// TODO: Move this to somewhere else maybe?
        /// </summary>
        /// <param name="foundObject"></param>
        /// <param name="expectedType"></param>
        /// <param name="detailer">
        /// May be null
        /// Used to give details in case of an exception being thrown
        /// </param>
        public static void AssertAssignable(object foundObject, Type expectedType, Func<string>? detailer = null)
        {
            if (foundObject == null) throw new NullReferenceException(nameof(foundObject) + ". (" + nameof(expectedType) + ": " + expectedType + ")" + detailer.Result("\r\nDetails: ") + "\r\n");
            if (expectedType == null) throw new NullReferenceException(nameof(expectedType) + ". (" + nameof(foundObject) + ": " + foundObject + ")" + detailer.Result("\r\nDetails: ") + "\r\n");
            if (!expectedType.IsAssignableFrom(foundObject.GetType())) throw new InvalidObjectTypeException(foundObject, expectedType, detailer.Result("") + "\r\n");
        }

        /// <summary>
        /// Asserts that expectedType.Equals(foundObject.GetType())
        /// TODO: Move this to somewhere else maybe?
        /// </summary>
        /// <param name="foundObject"></param>
        /// <param name="expectedType"></param>
        /// <param name="detailer">
        /// May be null
        /// Used to give details in case of an exception being thrown
        /// </param>
        public static void AssertEquals(object foundObject, Type expectedType, Func<string>? detailer = null)
        {
            if (!expectedType.Equals(foundObject.GetType())) throw new InvalidObjectTypeException(foundObject, expectedType, detailer.Result("Details: ") + "\r\n");
        }

        public static bool TryAssertEquals(object foundObject, Type expectedType, out string errorResponse, Func<string>? detailer)
        {
            if (!expectedType.Equals(foundObject.GetType()))
            {
                errorResponse =
                    "Expected object of type " + expectedType + " but got object of type " + foundObject.GetType() + " instead.\r\n" +
                    nameof(detailer) + ": " + detailer.Result("") + "\r\n";
                return false;
            }
            errorResponse = null!; /// <see cref="ARConcepts.TryPatternAndNull"/>
            return true;
        }

        private static string GetMessage(object _object, string? message) => "Invalid / unknown type of object (" + _object.GetType().ToString() + "). Object: '" + _object.ToString() + "'." + (string.IsNullOrEmpty(message) ? "" : ("\r\nDetails: " + message));
        public InvalidObjectTypeException(object _object) : base(GetMessage(_object, null)) { }
        public InvalidObjectTypeException(object _object, Type typeExpected) : base(GetMessage(_object, "Expected object of type " + typeExpected + " but got object of type " + _object.GetType() + " instead")) { }
        public InvalidObjectTypeException(object _object, Type typeExpected, string message) : base(GetMessage(_object,
            "Expected object of type " + typeExpected + " but got object of type " + _object.GetType() + " instead.\r\nDetails: " + message))
        { }
        public InvalidObjectTypeException(object _object, string message) : base(GetMessage(_object, message)) { }
    }

    /// <summary>
    /// TODO: Mar 2020: Ensure that really needed in ARCCore
    /// </summary>
    public class NotOfTypeEnumException : ApplicationException
    {
        public static void AssertEnum(Type type)
        {
            if (!type.IsEnum) throw new NotOfTypeEnumException(type);
        }
        public static void AssertEnum(Type type, Func<string> detailer)
        {
            if (!type.IsEnum) throw new NotOfTypeEnumException(type, detailer());
        }
        public NotOfTypeEnumException(Type type) : base("Expected Type.IsEnum but got type " + type.ToString()) { }
        public NotOfTypeEnumException(Type type, string details) : base("Expected Type.IsEnum but got type " + type.ToString() + ".\r\nDetails: " + details) { }
    }

    /// <summary>
    /// TODO: Mar 2020: Ensure that really needed in ARCCore
    /// </summary>
    public class OfTypeEnumException : ApplicationException
    {
        public static void AssertNotEnum(Type type)
        {
            if (type.IsEnum) throw new OfTypeEnumException(type);
        }
        public static void AssertNotEnum(Type type, Func<string> detailer)
        {
            if (type.IsEnum) throw new OfTypeEnumException(type, detailer());
        }
        public OfTypeEnumException(Type type) : base("Expected !Type.IsEnum but got type " + type.ToString()) { }
        public OfTypeEnumException(Type type, string details) : base("Expected !Type.IsEnum but got type " + type.ToString() + ".\r\nDetails: " + details) { }
    }

    public class NotOfTypeEnumException<T> : ApplicationException
    {
        public NotOfTypeEnumException() : base("Expected Type.IsEnum but got type " + typeof(T).ToString()) { }
    }

    public class InvalidCountException : ApplicationException
    {
        public static void AssertCount(long found, long expected)
        {
            if (found != expected) throw new InvalidCountException(found, expected);
        }
        public InvalidCountException(string message) : base(message) { }
        public InvalidCountException(long found, long expected) : base(nameof(expected) + ": " + expected + ", " + nameof(found) + ": " + found) { }
        public InvalidCountException(long found, long expected, string details) : base(nameof(expected) + ": " + expected + ", " + nameof(found) + ": " + found + "\r\nDetails: " + details) { }
        public InvalidCountException(string message, Exception inner) : base(message, inner) { }
    }

    public class NotNullReferenceException : ApplicationException
    {
        public static void AssertNotNull(object? obj)
        {
            if (obj != null) throw new NotNullReferenceException(obj.GetType() + ". Details: " + obj.ToString() + "\r\n");
        }
        public NotNullReferenceException(string message) : base(message) { }
        public NotNullReferenceException(string message, Exception inner) : base(message, inner) { }
    }

    public class UniquenessException : ApplicationException
    {
        public UniquenessException(string message) : base(message) { }
        public UniquenessException(string message, Exception inner) : base(message, inner) { }
    }

    public class SingleObjectNotFoundOrMultipleFoundException : ApplicationException
    {
        public SingleObjectNotFoundOrMultipleFoundException(string message) : base(message) { }
        public SingleObjectNotFoundOrMultipleFoundException(string message, Exception inner) : base(message, inner) { }
    }

    public class MultipleObjectsFoundException : ApplicationException
    {
        public MultipleObjectsFoundException(string message) : base(message) { }
        public MultipleObjectsFoundException(string message, Exception inner) : base(message, inner) { }
    }

    /// <summary>
    /// <see cref="PValue{T}"/>
    /// </summary>
    [Class(Description =
        "Placeholder for any class type suitable as generic type parameter -" + nameof(PValue<TValue>) + "-.\r\n" +
        "\r\n" +
        "Used for documentation purposes only." +
        "\r\n" +
        "This class helps with referring to PValue<T> classes when using the 'nameof' keyword because it is not possible to simply do 'nameof(PValue<T>)'. " +
        "You have to specify a class like 'nameof(PValue<object>)' or 'nameof(PValue<string>)' but that would be misleading. " +
        "'nameof(PValue<TValue>)' is then better to use.\r\n" +
        "\r\n" +
        "Note that this issue does not exist with XML-comments where you can simply just write <see cref=\"PValue{T}\"/>"
    )]
    public class TValue
    {
    }

    /// <summary>
    /// <see cref="PValue{T}"/>
    /// </summary>
    [Enum(
        Description =
            "Placeholder for any enum type suitable as generic type parameter for -" + nameof(PExact<TPropertyKeyEnum>) + "-.\r\n" +
            "\r\n" +
            "Used for documentation purposes only." +
            "\r\n" +
            "This enum helps with referring to PExact<T> classes when using the 'nameof' keyword because it is not possible to simply do 'nameof(PExact<T>)'. " +
            "You have to specify a class like 'nameof(PExact<object>)' or 'nameof(PExact<string>)' but that would be misleading. " +
            "'nameof(PExact<TPropertyKeyEnum>)' is then better to use.\r\n" +
            "\r\n" +
            "Note that this issue does not exist with XML-comments where you can simply just write <see cref=\"PExact{T}\"/>",
        AREnumType = AREnumType.OrdinaryEnum
    )]
    public enum TPropertyKeyEnum
    {
    }

    [Class(Description =
        "Placeholder for any class type suitable as generic type parameter restricted to -" + nameof(IP) + ".\r\n" +
        "\r\n" +
        "Used for documentation purposes only." +
        "\r\n" +
        "See -" + nameof(TValue) + "- and -" + nameof(TPropertyKeyEnum) + "- for more information."
    )]
    public class TIP : PRich
    {
    }
}
