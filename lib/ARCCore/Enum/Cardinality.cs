﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;

namespace ARCCore {
    [Enum(
        AREnumType = AREnumType.OrdinaryEnum,
        Description =
            "Describes how multiple instances of a property is supposed to be handled.\r\n" +
            "\r\n" +
            "By a 'property' in this context we usually mean a PValue<T> object, in other words something that is part of a bigger entity like -" + nameof(PRich) + "- " +
            "(like how 'FirstName' or 'PhoneNumber' is part of a 'Customer' entity)" +
            "\r\n" +
            "Used by for instance " +
            "1) -" + nameof(PropertyStreamLine.ParseAndStore) + "- when building up a hierarchical object storage. " +
            "2) -" + nameof(IP) + "--implementations for internal arranging of properties\r\n" +
            "3) -" + nameof(ARComponents.ARCAPI) + "- in order to provide API-methods" +
            "\r\n"
    )]
    public enum Cardinality {
        __invalid,

        [EnumMember(Description =
            "The default value.\r\n" +
            "\r\n" +
            "Only a single instance is relevant for a given point of time.\r\n" +
            "Typical example would be 'Customer/Firstname'.\r\n" +
            "The -" + nameof(ARConcepts.PropertyStream) + "- / API-calls should look something like this:\r\n" +
            "Customer/42/Firstname = 'John'   // Set name\r\n" +
            "Customer/42/Firstname = 'John Martin'   // Change name\r\n" +
            "Customer/42/Firstname.SetInvalid = 2020-03-20   // 'Delete' FirstName\r\n" +
            "\r\n" +
            "Note that due to intrinsic traits of the -" + nameof(ARConcepts.PropertyStream) + "- concept, " +
            "history of values will always be available, therefore the name " + nameof(HistoryOnly) + ". " +
            "(availability of history may of course be limited by any -" + nameof(ARConcepts.DataRetention) + " mechanisms used though)."
        )]
        HistoryOnly,

        // TODO: DO WE ALSO NEED A HistoryOnlyKeepInRAM-value?
        // TODO: for data like Temperature for instance? Meaning that we want to keep data in memory?
        // TODO: What is the relevance for subscription in such cases? Does it make a difference?
        // TODO: Subscribing to HistoryOnly in a PRich would mean that the old temperature gets discarded each a new one is read
        // TODO: --
        // TODO: On the other hand, this is maybe a client-consumptiom choice, and not a data-related issue per se.
        // TODO: In other words, we could put the actual concept into Subscription instead (like "Subscribe to Temperature and keep old values in memory")
        // HistoryOnlyKeepInRAM,

        [EnumMember(
            Description =
                "Values are always set as a whole (there is no need or no meaning in setting or deleting individual items)\r\n" +
                "Typical example could be PizzaOrder/Extra\r\n" +
                "The -" + nameof(ARConcepts.PropertyStream) + "- / API-calls should look something like this:\r\n" +
                "PizzaOrder/42/Extra = Pepperoni;Cheese;Sauce // Set whole collection\r\n" +
                "PizzaOrder/42/Extra/Invalid // 'Delete' all items.\r\n" +
                "\r\n" +
                "Note that you can use -" + nameof(WholeCollection) + "- as a simple mechanism for supporting -" + nameof(ARConcepts.ManyToManyRelations) + "-.\r\n" +
                "\r\n" +
                "Note how semicolon (';') is used as a separating character, this is reflected in -" + nameof(PropertyStreamLine.EncodeValuePart) + "-.",
            LongDescription =
                "The internal representation in -" + nameof(IP) + "- is normally a List<PValue<T>> " +
                "(this would be more memory-efficient than -" + nameof(IndividualItems) + "-)\r\n" +
                "\r\n" +
                "Note that in principle all data that can be stored as -" + nameof(WholeCollection) + "- can also be stored as -" + nameof(IndividualItems) + "- " +
                "but the client interaction is probably easier to implement with the former. You should also consider how you want historical representations " +
                "of changes in the data to look when choosing between these two."
        )]
        WholeCollection,

        // TODO: Should we think more set-theory here? Expand on these concepts? Duplicate colletions found i .NET? Duplicates allowed? Ordered / unordered collection? 

        [EnumMember(
            Description =
                "Values are supposed to be set and 'deleted' individually.\r\n" +
                "\r\n" +
                "Typical example could be Customer/PhoneNumber\r\n" +
                "The -" + nameof(ARConcepts.PropertyStream) + "- / API-calls should look something like this:\r\n" +
                "Customer/42/PhoneNumber/90534333  // Add number\r\n" +
                "Customer/42/PhoneNumber/40178178  // Add number\r\n" +
                "Customer/42/PhoneNumber/90534333.Invalid = 2020-03-20  // 'Delete' individual number\r\n" +
                "Customer/42/PhoneNumber/Invalid = 2020-03-21  // 'Delete' all numbers\r\n" +
                "\r\n" +
                "Note how the actual values are used as identifisers (keys).\r\n" +
                "\r\n" +
                "The collection of items is stored in a -" + nameof(IPII) + "- instance (typical -" + nameof(PII) + "-).\r\n" +
                "(specially recognized by -" + nameof(PropertyStreamLineParsed.TryParse) + "- and -" + nameof(PropertyStreamLine.TryStore) + "-.\r\n)" +
                "\r\n" +
                "Note that in principle all data that can be stored as -" + nameof(WholeCollection) + "- can also be stored as -" + nameof(IndividualItems) + "- " +
                "but the client interaction is probably easier to implement with the former. You should also consider how you want historical representations " +
                "of changes in the data to look when choosing between these two.\r\n" +
                "\r\n" +
                "Note that you can use -" + nameof(IndividualItems) + "- as a simple mechanism for supporting -" + nameof(ARConcepts.ManyToManyRelations) + "-.\r\n"
        )]
        IndividualItems
    }

    public static class CardinalityExtensions {

        [ClassMember(Description =
            "Returns TRUE if given cardinality implies that multiple items are stored simultaneously in a typical in-memory data storage.\r\n" +
            "This is again taken as a hint of asking for a List of actual values, instead of a single value."
        )]
        public static bool IsMultiple(this Cardinality cardinality) => (cardinality) switch
        {
            Cardinality.WholeCollection => true,
            Cardinality.IndividualItems => true,
            _ => false
        };
    }
}