﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;

namespace ARCCore {

    /// NOTE: Some texts here could actually be updated over the property-stream by components other than ARCCore.
    /// NOTE: For instance, ARCDoc and ARCAPI could themselves update the description of themselves here over the property-stream 
    /// NOTE: (they should be better able to describe themselves as they are made)
    /// NOTE: This is a conceptual idea, reminding of the possibility of updating Attribute-generated documentation because 
    /// NOTE: "everything" is derived from IP in the application and distribued as a property stream.
    /// NOTE: Hypothetically, new components, unknown to this code, could present themselves in this manner by sending 
    /// NOTE: information about themselves (but as of Mar 2020 the current validation mechanism <see cref="PKTypeAttribute.StandardValidatorAndParser"/>) 
    /// NOTE: would probably hinder enum members / enum values unknown to this code)
    [Enum(
        AREnumType = AREnumType.DocumentationOnlyEnum,
        Description =
            "ARComponents = AgoRapide Components. Contains short descriptions of the different 'official' AgoRapide components.\r\n" +
            "\r\n" +
            "TODO: Rename into ARComponent?\r\n" +
            "\r\n" +
            "The core component is -" + nameof(ARCCore) + "-. It is demonstrated through the application -" + nameof(ARADB) + "-.\r\n" +
            "\r\n" +
            "The other components are optional.\r\n" +
            "If you want to implement an HTTP API however you are probably going to use most, " +
            "if not all, of the components (see -" + nameof(ARAAPI) + "-).\r\n" +
            "\r\n" +
            "This splitting of AgoRapide into separate components serves multiple purposes:\r\n" +
            "\r\n" +
            "1) To make it easy to get started.\r\n" +
            "\r\n" +
            "2) To keep discipline when developing AgoRapide, by clearly stating the interfaces and avoiding a monolithic structure.\r\n" +
            "\r\n" +
            "3) To avoid code bloat.\r\n" +
            "\r\n" +
            "4) To encourage other developers to participiate in the development of AgoRapide. " +
            "It should be quite easy to contribute within a limited and well defined problem space, like adding functionality to -" + nameof(ARCQuery) + "-.\r\n" +
            "\r\n" +
            "A component may be either \r\n" +
            "a library (.DLL in Windows terms, name starting with ARC... like -" + nameof(ARCCore) + "-), or\r\n" +
            "an application (.EXE in Windows terms, name starting with ARA... like -" + nameof(ARADB) + "-)\r\n"
        // TODO: Delete commented out code:
        //"\r\n" +
        //"Much of the functionality described in the additional components will be provided in the form of classes implementing -" + nameof(BasePKAttribute) + "-, " +
        //"that is keeping to the basic principle in AgoRapide of -" + nameof(ARConcepts.TaggingOfPropertyKeys) + "-, giving you at-a-glance information about " +
        //"everything essential to a given field." +
        //"\r\n"
    )]
    public enum ARComponents {
        __invalid,

        [EnumMember(
            Description =
                "A necessary library (.DLL in Windows terms) that every -" + nameof(ARNodeType) + "- should link to.\r\n" +
                "\r\n" +
                "Contains the basic structure of AgoRapide, like -" + nameof(BasePKAttribute) + "-, -" + nameof(IP) + "- and -" + nameof(StreamProcessor) + "-.\r\n" +
                "\r\n" +
                "Note that in some cases you do not need to even use this library.\r\n" +
                "This is the case if you are building a client application whose only need is " +
                "to SEND single -" + nameof(PropertyStreamLine) + "-s to a -" + nameof(ARNodeType.CoreDB) + "-.\r\n" +
                "You then just open a standard TCP/IP connection and send the lines straight away.\r\n" +
                "See -" + nameof(ARCDoc) + "- for an example of this and " +
                "-" + nameof(PropertyStreamLine.EncodeValuePart) + "- for the minimal encoding that is needed.\r\n" +
                "This is actually quite a unique capability of AgoRapide as a data storage system, " +
                "that no specific (to AgoRapide) client libraries are needed for storing data.\r\n"
        )]
        ARCCore,

        [EnumMember(
            Description =
                "An application constituting the full functionality of a -" + nameof(ARNodeType.CoreDB) + "- (linking to -" + nameof(ARComponents.ARCCore) + "-)." +
                "\r\n" +
                "TODO: Add reading of instructions from command-line (for outgoing connections).\r\n" +
                "TODO: This will enable use of this program also as -" + nameof(ARNodeType.ArmDB) + "-.\r\n" +
                "\r\n" +
                 "The standard version of " + nameof(ARADB) + " may also fully constitute the functionality of -" + nameof(ARNodeType.ArmDB) + "-, " +
                "or even a -" + nameof(ARNodeType.Client) + "- " +
                "but usually you will add some -" + nameof(ARConcepts.ApplicationSpecificCode) + "- to them, especially -" + nameof(ARNodeType.Client) + "-.\r\n" +
                "(when adding functionality, clone the repository for " + nameof(ARADB) + " and expand from there.",
            LongDescription =
                "Note: Almost all of the actual code resides in -" + nameof(ARCCore) + "- (which is the only AgoRapide component library that this program links to as standard). " +
                "-" + nameof(ARADB) + "- is only the minimum packaging needed in order to run as an independent application / executable."
        )]
        ARADB,

        [EnumMember(
            Description =
                "A template application for a -" + nameof(ARNodeType.Client) + "- (linking to -" + nameof(ARCCore) + "-).\r\n" +
                "\r\n" +
                "Contains some sample code for you to use as building blocks for your own -" + nameof(ARNodeType.Client) + "- application.\r\n",
            LongDescription =
                "Note that this application also links to -" + nameof(ARComponents.ARCDoc) + "-. " +
                "This is done in order to demonstrate some functionality of AgoRapide and have some properties available for generating a sizeable chunk of -" + nameof(ARConcepts.PropertyStream) + "-. " +
                "You do not have to do this in your own application, linking to -" + nameof(ARComponents.ARCCore) + "- is sufficient as a start."
        )]
        ARAClient,

        [EnumMember(
            Description =
                "A library supporting documentation and demonstrating usage of AgoRapide.\r\n" +
                "\r\n" +
                "Offers the following functionality:\r\n" +
                "\r\n" +
                "1) Transforming -" + nameof(IP) + "- instances into HTML.\r\n" +
                "Important method is -" + nameof(ARCDoc) +"-.-Extensions-.-ToHTMLSimpleSingle-\r\n" +
                "Can also transform recursively (transform hierarchical structures) " +
                "and store those structures to disk as static HTML files with -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "-.\r\n" +
                "Important class in this regard is -HLocationCollection-.\r\n" +
                "\r\n" +
                "2) Generation of documentation by extracting -" + nameof(BaseAttribute) + "- tags (like this one) for the different parts of your application. " +
                "Compiling everything together in a coherent HTML presentation with links between the different topics.\r\n" +
                "Important classes are\r\n" +
                "-DocFrag- (Documentation fragment),\r\n" +
                "-HTOC- (Hierarchical table of contents).\r\n" +
                "\r\n" +
                "The documentation can also be viewed with the help of -" + nameof(ARCQuery) + "-, just like any other -" + nameof(ARConcepts.PropertyStream) + "- based data.\r\n" +
                "\r\n" +
                "3) Demonstration of practical use of AgoRapide, through small easy to follow examples, like:\r\n" +
                "   a) 'Normal' AgoRapide classes and their usage.\r\n" +
                "   b) Demonstrate -" + nameof(PKTypeAttributeP.BoilerplateCodeProperties) + "-.\r\n" +
                "   c) Demonstrate -" + nameof(ClassAttributeP.BoilerplateCodePropertiesAndStorage) + "-.\r\n" +
                "\r\n" +
                "TODO: Extract the actual code examples from -" + nameof(ARCDoc) + "- into this documentation text.\r\n" +
                "\r\n" +
                "Note that you are not required to generate documentation / use this library (-" + nameof(ARComponents.ARCDoc) + "-), " +
                "your application works quite fine based on only -" + nameof(ARComponents.ARCCore) + "-.\r\n" +
                "\r\n" +
                "Note that the AgoRapide library -" + nameof(ARComponents.ARCQuery) + "- offers much richer functionality " +
                "for generating HTML views (and also JSON views). " +
                "Especially because it offers search and filtering functionality).\r\n"
        )]
        ARCDoc,

        [EnumMember(
            Description =
                "ARCQuery provides a query language that mimicks SQL but is usable through HTTP as GET URLs.\r\n" +
                "\r\n" +
                "The library supports fluent query expressions and entity-relations.\r\n" +
                "\r\n" +
                "Enables fluent queries like for instance\r\n" +
                "  'Order/WHERE Amount > 1000 EUR/REL Customer/SKIP 100/TAKE 50/SELECT FirstName, LastName' or \r\n" +
                "  'OrderLine/PIVOT Created.YearMonth() BY Product SUM Amount'\r\n" +
                "\r\n" +
                "Each query consist of a series of steps, each step is a transformation of the given dataset.\r\n" +
                "\r\n" +
                "The query language is supported by C# methods, defined globally or individually for each entity object class.\r\n" +
                "\r\n" +
                "Supports queries and linking between entities. Understands what properties like 'Order/43/CustomerId = 42' mean.\r\n" +
                "\r\n" +
                "Enables automatic suggestions in -" + nameof(ARCAPI) + "- for adding of related entities / navigating to related entities.\r\n" +
                "\r\n" +
                "Important classes in this library are:\r\n" +
                "-QueryExpression-,\r\n" +
                "-CompoundKey- (like -BinaryOperatorKey-, -ConstantKey-, -EntityMethodKey-, -NewKey-, -ForeignKey-, -MemberEntityKey-),\r\n" +
                "-FunctionKey-,\r\n" +
                "-QuantileKey- and\r\n" +
                "-ValueComparer- (like -ValueComparerDateTime-).\r\n" +
                "\r\n" +
                "Some online applications demonstrating -" + nameof(ARCQuery) + "- are:\r\n" +
                "http://ARNorthwind.AgoRapide.com and\r\n" +
                "http://ARAdventureWorksOLAP.AgoRapide.com \r\n"

        // TOOD: Delete commented out code
        //"\r\n" +
        //"Note also how extension method -TryGetP- (ARCQuery.Extensions.TryGetP) understands, through use of reflection, " +
        //"read-only properties of your entity classes."
        )]
        ARCQuery,

        [EnumMember(
            Description =
                "ARCEvent contains an -" + nameof(ARConcepts.EventSourcing) + "- engine offering a unique -" + nameof(ARConcepts.PropertyStream2D) + "- " +
                "concept where processes (of some business domain) can be expressed in very compact manner through the concept of -IRegHandler-.\r\n" +
                "\r\n" +
                "ARCEvent offers support for retroactive changes within the concept of -" + nameof(ARConcepts.EventSourcing) + "-.\r\n" +
                "\r\n" +
                "The -" + nameof(ARConcepts.PropertyStream2D) + "- (2D = Two dimensional) representation " +
                "has as first dimension an -" + nameof(ARConcepts.RegStream) +"- " +
                "and as second dimension an -" + nameof(ARConcepts.EventStream) + "-.\r\n" +
                "\r\n" +
                "ARCEvent offers state queries at any point within this two-dimensional space, also into the future " + 
                "(that is, it also supports planning in addition to history / recording).\r\n" +
                "\r\n" +
                "ARCEvent was introduced in ARCore (AgoRapide) in late 2021.\r\n" +
                "\r\n" +
                "From Feb 2022 ARCEvent does also offer handling if Commands (in addition to Registrations), " +
                "see -" + nameof(PSPrefix.cmd) + "-, -Cmd- and -ICmdHandler-.\r\n"
        )]
        ARCEvent,

        [EnumMember(
            Description =
                "(Not implemented as of March 2020)\r\n" +
                "\r\n" +
                "A library providing a basic framework for synchronizing with other data sources (data sources outside of AgoRapide).\r\n" +
                "\r\n" +
                "Background: One important use of AgoRapide is envisaged to be analyzing of existing databases " +
                "(with the help of for instance -" + nameof(ARCQuery) + "-). " +
                "In order to do the analysis the data has to be converted into the format of AgoRapide (-" + nameof(ARConcepts.PropertyStream) + "- " +
                "and continously kept synchronized in an efficient manner.\r\n" +
                "\r\n" +
                "-" + nameof(ARCSync) + "- is also what you use when you do not want AgoRapide to be your main database.\r\n" +
                "As long as your main database uses the 'key-value' principle and you can query data from it in chronological order, " +
                "it is usually quite easy to keep AgoRapide synchronized to it.\r\n" +
                "\r\n" +
                "Some database especially well suited in this regard are:\r\n" +
                "- Apache Kafka\r\n" +
                "- Amazon AWS DynamoDB\r\n" +
                "- Azure Cosmos DB\r\n" +
                "AgoRapide integrates quite naturally with these.\r\n" +
                "Note that using AgoRapide against these databases also reduces egress costs since downstream caching is inbuilt in AgoRapide.\r\n" +
                "In a typical situation you can have one AgoRapide node subscribing to the key-value stream, with all other nodes connecting to that one.\r\n" +
                "When the occasional need for an -" + nameof(ARConcepts.AdHocQuery) + "- arises " +
                "that would be O(n) in AgoRapide you can instead query directly against " +
                "the main database (which would more probably have a performance of O(1)).\r\n" +
                "If that query turns out to be permanently needed, then you set up a -" + nameof(Subscription) + "- for it " +
                "against -" + nameof(ARNodeType.CoreDB) + "- in order to cache it locally, " +
                "eliminating the egress cost and ensuring an even speedier response.\r\n" +
                "\r\n" +
                "-" + nameof(ARCSync) + "- is not to be confused with the concept of how different AgoRapide based nodes synchronize between themselves " +
                "(see -" + nameof(Subscription) + "-. That functionality is built into -" + nameof(ARCCore) + "-."
        )]
        ARCSync,

        [EnumMember(
            Description =
                "(Not implemented as of March 2020)\r\n" +
                "\r\n" +
                "A library providing security like secure communications (SSL), secure authentication and authorization (access rights), logging of access and similar.\r\n" +
                "\r\n" +
                "Will probably contain a -" + nameof(BasePKAttribute) + "- sub-class called -PKSecAttribute-."
        )]
        ARCSec,

        //[EnumMember(
        //    Description =
        //        "(Not implemented as of March 2020)\r\n" +
        //        "\r\n" +
        //        "A library providing language and translation functionality"
        //)]
        //ARCLang,

        [EnumMember(
            Description =
                "A library providing an API on top of the -" + nameof(ARConcepts.PropertyStream) + "-.\r\n" +
                "\r\n" +
                "The API can for instance be offered over HTTP / HTTPS with the help of Microsoft.AspNetCore.Mvc (see -" + nameof(ARAAPI) + "-)\r\n" +
                "but -" + nameof(ARCAPI) + "- itself is totally independent of any specific web-server technology.\r\n" +
                "\r\n" +
                "The API provided supports both\r\n " +
                "1) GraphQL (-GQController-) and\r\n" +
                "2) REST (-RQController-).\r\n" +
                "\r\n" +
                "In addition to JSON format the API also provides a rudimentary HTML / Javascript based interface " +
                "for direct user interaction (nominally for administrative users / power users).\r\n" +
                "\r\n" +
                "The HTML views are created automatically based on -" + nameof(ARConcepts.TaggingOfPropertyKeys) + "-, including Javascript forms for " +
                "creating new properties.\r\n" +
                "\r\n" +
                "The HTML interface automatically constructs views also for GraphQL queries, meaning that rich functionality " +
                "can be programmed from 'outside' the API in line with the GraphQL philosophy.\r\n" +
                "\r\n" +
                "This HTML interface is quite sufficient as an internal administrative tool / support department tool for your application.\r\n" +
                "\r\n" +
                "Depends on -" + nameof(ARCDoc) + "-, -" + nameof(ARCQuery) + "- and -" + nameof(ARCSec) + "-.\r\n" +
                "\r\n" +
                "See also -BaseController- and -AddController-.\r\n" +
                "\r\n"
        // "Contains a -" + nameof(BasePKAttribute) + "- sub-class called -PKAPIAttribute-."
        )]
        ARCAPI,

        [EnumMember(Description =
            "The corresponding template application for -" + nameof(ARCAPI) + "- demonstrating its usage, " +
            "providing an HTTP / HTTPS interface with the help of Microsoft.AspNetCore.Mvc."
        )]
        ARAAPI

    }
}