﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;

namespace ARCCore
{

    [Enum(
        AREnumType = AREnumType.DocumentationOnlyEnum,
        Description =
            "The overarching concepts upon which AgoRapide is built.\r\n" +
            "(and upon which your -" + nameof(ApplicationSpecificCode) + "- hopefully also will be built).\r\n" +
            "\r\n" +
            "Start here in order to familiarizing yourself with AgoRapide.\r\n" +
            "\r\n" +
            "See -" + nameof(GettingStarted) + "-.\r\n" +
            "\r\n" +
            "This enum describes the overarching general concepts in AgoRapide.\r\n",
        LongDescription =
            "Note that in general this enum only describes concepts not implemented explicit under the same name " +
            "(not implemented as classes / properties or similar).\r\n" +
            "(it is a -" + nameof(AREnumType.DocumentationOnlyEnum) + "- enum).\r\n" +
            "As AgoRapide matures we expect some values here to be removed gradually, as some explicit implementation is done " +
            "(and the corresponding text moved to that implementation).\r\n" +
            "\r\n" +
            "TODO: EXPAND WITH LoadBalancing (Sharding) / FaulTolerance. " +
            "TODO: (or just use -" + nameof(ConnectionInstructionP.Sharding) + "-, -" + nameof(ConnectionInstructionP.MultipleConnectionUsage) + "-).\r\n" +
            "TODO: Give examples of how these objectives may be achieved through only configuration (no code should be necessary).\r\n" +
            "\r\n" +
            "TODO: Consider creating new enum CodeLocation or similar,\r\n" +
            "TOOD: comprising -" + nameof(StandardAgoRapideCode) + "- and -" + nameof(ApplicationSpecificCode) + "-.\r\n"
    )]
    public enum ARConcepts
    {
        __invalid,

        [EnumMember(Description =
            "AgoRapide 2020 is an open source library for building data-oriented backend applications using .NET.\r\n" +
            "\r\n" +
            "With AgoRapide the impedance mismatch problem between database tables and in-memory objects is eliminated through the -" + nameof(ARConcepts.PropertyStream) + "- concept.\r\n" +
            "\r\n" +
            "Data is created, stored and transported at the key-value level.\r\n" +
            "\r\n" +
            "NOTE: This text is an 'inside-out' approach to documentation best suited for developers of AgoRapide.\r\n" +
            "NOTE: A better introductory top-down presentation is found at\r\n" +
            "NOTE: \r\n" +
            "NOTE: http://bef.no/AgoRapide \r\n" +
            "\r\n" +
            "The source code, including this documentation, is found at\r\n" +
            "\r\n" +
            "https://bitbucket.org/BjornErlingFloetten/arcore \r\n" +
            "\r\n" +
            "\r\n" +
            "AgoRapide 2020 can be used as:\r\n" +
            "\r\n" +
            "1) A distributed in-memory backend processor of events / listener to events.\r\n" +
            "The -" + nameof(ARConcepts.PropertyStream) + "--format lends itself well to distribution of loads. " +
            "Each processing node subscribes to sub-sets of the entire property stream.\r\n" +
            "The -" + nameof(Subscription) + "-s consists of the events to which the node has to respond, plus other necessary data in order to correctly process the events. \r\n" +
            "Any output from the node is placed back out in the property stream, either back upstream, or further downstream for other nodes to process.\r\n" +
            "See -" + nameof(ARComponents.ARCCore) + "-.\r\n" +
            "\r\n" +
            "2) A 'single-endpoint' API.\r\n" +
            "All data is exposed through a standardized API query mechanism (REST or GraphQL) with no need for writing application specific endpoints.\r\n" +
            "Since APIs should expose well defined DTO's and not entire entities, AgoRapide APIs are therefore normally established as separate nodes.\r\n" +
            "Each node has a well defined -" + nameof(Subscription) + "- to a sub-set of your entire database (also reducing the memory footprint).\r\n" +
            "The API will then in effect offer DTO's with very little coding needed.\r\n" +
            "See -" + nameof(ARComponents.ARCAPI) + "-.\r\n" +
            "\r\n" +
            "3) A reporting tool / query tool (\"Express BI\").\r\n" +
            "An API as in 2) combined with the query library. \r\n" +
            "The query library enables report generation directly from the web-browser (as a single bookmark-able URL).\r\n" +
            "See -" + nameof(ARComponents.ARCQuery) + "-.\r\n" +
            "\r\n" +
            "4) (New from December 2021) An -" + nameof(ARConcepts.EventSourcing) + "- engine offering a unique -" + nameof(ARConcepts.PropertyStream2D) + "- " +
            "concept where processes (of some business domain) can be expressed in very compact manner through the concept of -IRegHandler-.\r\n" +
            "See -" + nameof(ARComponents.ARCEvent) + "-.\r\n" +
            "\r\n" +
            "For all cases above the following also applies:\r\n" +
            "\r\n" +
            "A) An in-memory database which is continously updated through the property stream is provided by AgoRapide. \r\n" +
            "This ensures short response times and significantly reduces network traffic. \r\n" +
            "See -" + nameof(StreamProcessor) + "-.\r\n" +
            "\r\n" +
            "B) The core database storage is either provided directly by AgoRapide, " +
            "or you can use an established 'key-value' database like Apache Kafka, AWS DynamoDB or Azure Cosmos DB.\r\n" +
            "See -" + nameof(ARComponents.ARCSync) + "-.\r\n" +
            "\r\n" +
            "C) High performance thread-safe \"simultaneous\" read and writes is supported in all scenarios.\r\n" +
            "\r\n" +
            "D) AgoRapide works better the more strongly typed schema you specify (see -" + nameof(TaggingOfPropertyKeys) + "-). \r\n" +
            "You can however start out without any schema at all. Duck typing is then used instead. \r\n" +
            "Even relational queries are possible straight out-of-the-box without any schema, as long as some naming conventions are followed \r\n" +
            "(like Order.CustomerId for instance, in order to relate an Order to a Customer)\r\n" +
            "Strong typing is used by AgoRapide whenever specified by the schema.\r\n" +
            "\r\n" +
            "E) Scaling and redundancy is supported by flexible routing of the -" + nameof(PropertyStream) + "- to multiple nodes.\r\n" +
            "\r\n" +
            "F) No dependencies to other libraries apart from .NET itself.\r\n" +
            "\r\n" +
            "See -" + nameof(ARComponents.ARCDoc) + "- for practical examples for using AgoRapide, especially class -Demonstrator-.\r\n" +
            "\r\n" +
            "AgoRapide is the result of a strong wish of -" + nameof(AvoidingRepetition) + "-.\r\n" +
            "\r\n" +
            "Three very important concepts in AgoRapide are:\r\n" +
            "\r\n" +
            "1) -" + nameof(PropertyStream) + "-: The concept of how all data is broken down into single 'key and value' pairs " +
            "which are then stored as plain text format lines and then never changed.\r\n" +
            "\r\n" +
            "2) -" + nameof(TaggingOfPropertyKeys) + "-: The concept of specifying in a single place " +
            "all relevant information about a given property (data field).\r\n" +
            "\r\n" +
            "3) -" + nameof(PropertyAccess) + "-: The general mechanism for accessing properties of objects in AgoRapide.\r\n" +
            "\r\n" +
            "AgoRapide is split into different -" + nameof(ARComponents) + "-. " +
            "You can have a functional distributed storage mechanism " +
            "including -" + nameof(PSPrefix.app) + "--consoles by linking only to -" + nameof(ARComponents.ARCCore) + "-.\r\n" +
            "Choose from the other components based on your security needs (-" + nameof(ARComponents.ARCSec) + "-), " +
            "the complexity of your data (-" + nameof(ARComponents.ARCQuery) + "-) or other needs (see -" + nameof(ARComponents) + "- in general).\r\n" +
            "\r\n" +
            "See also -" + nameof(ARConcepts) + "- and -" + nameof(AgoRapideHistory) + "-.\r\n" +
            "\r\n" +
            "Some online applications demonstrating AgoRapide are:\r\n" +
            "http://ARNorthwind.AgoRapide.com and\r\n" +
            "http://ARAdventureWorksOLAP.AgoRapide.com \r\n"
        )]
        GettingStarted,

        [EnumMember(
            Description =
                "AgoRapide, the 'streams-of-water' database\".\r\n" +
                "\r\n" +
                "Property stream in AgoRapide is the concept of how all data is broken down into single 'key and value' pairs " +
                "(single -" + nameof(PropertyStreamLine) + "-s) " +
                "which are stored sequentially as plain text format lines and then never changed.\r\n" +
                "\r\n" +
                "See -" + nameof(PropertyStreamLine) + "- for the actual syntax used.\r\n" +
                "\r\n" +
                "The resulting 'property stream' is, by its own nature, easy to distribute.\r\n" +
                "\r\n" +
                "It is also easy to convert into an object oriented format, " +
                "lessening the 'impedance mismatch' issue between databases and object oriented languages " +
                "(see -" + nameof(PropertyStreamLineParsed.TryParse) + "- and -" + nameof(PropertyStreamLine.TryStore) + "-.\r\n" +
                "Note that conversion back again is also possible, see -" + nameof(IP.ToPropertyStream) + "-.\r\n" +
                "\r\n" +
                "Some examples of what the property stream concept makes easier are:\r\n" +
                "\r\n" +
                "1) Implementing the actual storage.\r\n" +
                "AgoRapide offers its own storage mechanism (see -" + nameof(StreamProcessor) + "-).\r\n" +
                "(Note that this is not obligatory, see -" + nameof(ARComponents.ARCSync) + "- for how to synchronize AgoRapide from other databases.)\r\n" +
                "\r\n" +
                "2) Preservation of history.\r\n" +
                "Storing data in traditional 'database tables' is normally only preserving the current status, " +
                "with special provisions having to be taken in order to also preserve history. " +
                "With the property stream concept on the other hand, history is automatically (intrinsically) preserved. " +
                "(and the traditional 'table' can always be created from the property stream, but not vice versa).\r\n" +
                "\r\n" +
                "3) Distribution of data.\r\n" +
                "Thanks to your data flowing like 'streams-of-water', " +
                "it can be piped wherever convenient (by a publish / subscribe mechanism), to caches, to sub-systems and so on " +
                "(see -" + nameof(StreamProcessor) + "-, -" + nameof(Subscription) + "- and -" + nameof(ClientUpdatePosition) + "-).\r\n" +
                "\r\n" +
                "4) Security.\r\n" +
                "Instead of a traditional API connecting to a 'complete' database backend (with the corresponding security " +
                "implications), you can have non-sensitive data streamed to dedicated API nodes. Because these nodes can only serve " +
                "non-sensitive data, they are easier to implement in a secure manner.\r\n" +
                "\r\n" +
                "5) Coherent representation throughout your application.\r\n" +
                "The same format is used 'everywhere' like:\r\n" +
                "a) In the storage file: 'dt/Customer/42/FirstName = John'\r\n" +
                "b) As an HTTP API call: 'yourapi.com/RQ/dt/Customer/42/FirstName = John'\r\n" +
                "c) As a query (through API or otherwise): 'yourapi.com/RQ/dt/Customer/42'\r\n" +
                "\r\n" +
                "6) Load-balancing.\r\n" +
                "(see -" + nameof(ConnectionInstructionP.Sharding) + "-).\r\n" +
                "\r\n" +
                "7) Fault-tolerance.\r\n" +
                "(see -" + nameof(ConnectionInstructionP.MultipleConnectionUsage) + "-).\r\n" +
                "\r\n" +
                "8) Always up-to-date incremental backup.\r\n" +
                "A backup node can simply subscribe to -" + nameof(Subscription.IsAll) + "-, '+*', and thereby keep an always current backup).\r\n" +
                "\r\n" +
                "Note especially how distributed systems are easy to implement with AgoRapide " +
                "because synchronizing data across different -" + nameof(ARNodeType) + "- is very easy " +
                "when properties 'flow like water'.\r\n" +
                "\r\n" +
                "For serialization of objects see -" + nameof(IP.ToPropertyStream) + "- and\r\n" +
                "for deserialization see -" + nameof(PropertyStreamLine.ParseAndStore) + "-.\r\n" +
                "\r\n" +
                "See also -" + nameof(EventSourcing) + "-, -" + nameof(RegStream) + "- and -" + nameof(EventStream) + "-.",
            LongDescription =
                "Some origins for how new lines in the property stream are generated are:\r\n" +
                "\r\n" +
                "1) An API-request like api/Add/Customer/42/FirstName = 'John' (note how this translates almost directly into a single property stream line)-\r\n" +
                "'Entry-point' into -" + nameof(ARComponents.ARCCore) + "- would typically be by direct call from API-mechanism to -" + nameof(StreamProcessor.SendFromLocalOrigin) + "-.\r\n" +
                "\r\n" +
                "2) Through -" + nameof(ExposingApplicationState) + "- via -" + nameof(IP.Logger) + "- like -" + nameof(PConcurrent) + "-.- " + nameof(PConcurrent.Logger) + "-.\r\n" +
                "'Entry-point' into -" + nameof(ARComponents.ARCCore) + "- would typically be -" + nameof(StreamProcessor.SendFromLocalOrigin) + "- through " +
                "-" + nameof(PConcurrent.TrySetP) + " or through direct call to -" + nameof(IP.Log) + "-.\r\n" +
                "\r\n" +
                "FAQ: Reading historical data from the property stream has inherently a performance of O(n). How does AgoRapide mitigate this?\r\n" +
                "In two ways:\r\n" +
                "1) Through the concept of -" + nameof(Subscription) + "- where an -" + nameof(ARNodeType) + "- " +
                "has a continously kept up-to-date locally cached version of the data it needs to process. " +
                "The only O(n) query would then happen in connection with the initial setup of the node.\r\n" +
                "2) By using an established 'key-value' database (with O(1) performance) as main database instead of AgoRapide " +
                "(see -" + nameof(ARComponents.ARCSync) + "- for more information).\r\n" +
                "\r\n" +
                "FAQ: Can the property stream format support transactions?\r\n" +
                "Yes. There is a TODO: for that, inserting SQL-style BEGIN, COMMIT / ABORT into the stream, together with a corresponding transaction id, " +
                "to signal start and end of transactions. Every property stream line (each data point) belonging to that transaction " +
                "would then be tag'ed with the transaction id.\r\n" +
                "A client seeing a BEGIN would then know to wait for a COMMIT / ABORT with the same transaction id, " +
                "before considering the belonging data points for further processing.\r\n" +
                "Example:\r\n" +
                "  Transaction/123abc456def/BEGIN\r\n" +
                "  Transaction/123abc456def/Account/42/Subtract = 1000 EUR\r\n" +
                "  Transaction/123abc456def/Account/43/Add = 1000 EUR\r\n" +
                "  Transaction/123abc456def/COMMIT\r\n" +
                "(note that in the actual stream, other data may be interspersed with the lines shown above.)\r\n" +
                "Note: Example above is admittedly somewhat naïve.\r\n" +
                "TODO: Implement transactions.\r\n" +
                "\r\n" +
                "FAQ: Can the property stream format support GraphQL?\r\n" +
                "Yes, and actually in a quite direct manner. And the way -" + nameof(Subscription) + " is structured it is very similar to a GraphQL query " +
                "meaning a translation between the two is relatively simple to implement.\r\n" +
                "(as a side-note, the -" + nameof(TaggingOfPropertyKeys) + "- concept should be sufficient rich " +
                "to translate into GraphQL SDL (with support from -" + nameof(ARComponents.ARCQuery) + "-).\r\n" +
                "TODO: Implement support for GraphQL.\r\n" +
                "\r\n" +
                "FAQ: Is not the property stream quite similar to MQTT messages?\r\n" +
                "Yes, definitely. The 'topic' concept in MQTT corresponds closely to the key part of a -" + nameof(PropertyStreamLine) + "- " +
                "while the 'payload' in a message corresponds the the value part.\r\n" +
                "A key difference with the AgoRapide philosophy compared to MQTT is however that with AgoRapide the complete datastorage IS actually " +
                "the sum of all -" + nameof(PropertyStreamLine) + "-. Therefore the value part in AgoRapide is usually broken down " +
                "into single values (that is, multiple -" + nameof(PropertyStreamLine) + "-), " +
                "whereas the 'payload' in a single MQTT message usually contains more complex data (for instance a JSON object).\r\n" +
                "An MQTT 'topic' is also more ephemeral than an AgoRapide key.\r\n" +
                "Note that is should be very easy to translate between an AgoRapide world and an MQTT world " +
                "(translate between an MQTT message and a -" + nameof(PropertyStreamLine) + "-).\r\n" +
                "\r\n" +
                "See -" + nameof(StreamProcessor.ReceiveAndDistribute) + "-, " + nameof(IP.ToPropertyStream) + "- and " +
                "-" + nameof(ARNodeType) + "- for more information."
        )]
        PropertyStream,

        [EnumMember(Description =
            "PropertyStream2D is an expansion of the -" + nameof(PropertyStream) + "- concept " +
            "supported by the library -" + nameof(ARComponents.ARCEvent) + "-.\r\n" +
            "\r\n" +
            "The 2D (Two dimensional) representation has\r\n" +
            "as the first dimension (usefully drawn as a vertical downwards pointing axis) -" + nameof(ARConcepts.RegStream) + "- and\r\n" +
            "as the second dimension (usefully draw as a horizontal rightwards pointing axis) -" + nameof(ARConcepts.EventStream) + "-.\r\n" +
            "\r\n" +
            "-" + nameof(ARComponents.ARCEvent) + "- offers state queries at any point within this two-dimensional space.\r\n" +
            "\r\n" +
            "The two main classes involved are -EventProcessor- and -PCollectionES-."
        )]
        PropertyStream2D,

        [EnumMember(Description =
            "(related to -" + nameof(ARComponents.ARCEvent) + "-).\r\n" +
            "\r\n" +
            "NOTE: As of Jan 2022 there is some confusion about what AgoRapide is really doing, 'Command Sourcing' or 'Event Sourcing'.\r\n" +
            "NOTE: And some people are actually calling Fowlers description below for 'Command Sourcing'.\r\n" +
            "NOTE: One possible clarification is to state that -" + nameof(ARComponents.ARCEvent) + "- persists only the\r\n" +
            "NOTE: -Reg-s (Registrations) performed by the user (in the -" + nameof(RegStream) + "-).\r\n" +
            "NOTE: That is, it does not persist events in the -" + nameof(TransientEventTree) + "-.\r\n" +
            "NOTE: BUT, at the same time, an -IRegHandler- can be seen as both a command and event, this especially applies to\r\n" +
            "NOTE: -CNER- and -SSPR- for instance.\r\n" +
            "NOTE:\r\n" +
            "NOTE: One way to distinguish an -IRegHandler- is to see whether it is an\r\n" +
            "NOTE: -IRegHandlerTGTE- (more like a Command) or -IRegHandlerTAERTE- (more like an Event).\r\n" +
            "NOTE: but there is nothing preventing it from being both.\r\n" +
            "\r\n" +
            "As described by Martin Fowler in\r\n" +
            "\r\n" +
            "https://martinfowler.com/eaaDev/EventSourcing.html \r\n" +
            "\r\n" +
            "\"Event Sourcing ensures that all changes to application state are stored as a sequence of events.\r\n" +
            "Not just can we query these events, we can also use the event log to reconstruct past states, \r\n" +
            "and as a foundation to automatically adjust the state to cope with retroactive changes.\"\r\n" +
            "\r\n" +
            "The concept of -" + nameof(PropertyStream) + "- covers the first part of this definition, for the\r\n" +
            "last part (retroactive changes), see -" + nameof(ARComponents.ARCEvent) + "- and -" + nameof(PropertyStream2D) + "-\r\n" +
            "\r\n" +
            "In AgoRapide the term Event Sourcing 2D is used in order to help understand the concept. " +
            "Event Sourcing 2D introduces two axes,\r\n" +
            "the downwards pointing -RegTime- and the rightwards pointing -EventTime-.\r\n" +
            "\r\n" +
            "The corresponding \"streams\" along these axes is called -RegStream- and -EventStream-.\r\n" +
            "\r\n" +
            "The API offers the -RSController- for operating on the -Reg- (Registration) side (which can also be called the \"input\"-side,\r\n" +
            "and the -ESController- for reading the result on the -Event- side (which can also be called the \"output\"-side.\r\n" +
            "\r\n" +
            "The concept of Event Sourcing 2D is useful in order to construct an optimal code base covering all this.\r\n"
        )]
        EventSourcing,

        // WAIT A LITTLE BIT WITH THIS
        // TODO: Now is the maximum value how far into the EventStream events have been processed, and the actual UTC "now" value on the server.
        //[EnumMember(Description =
        //    "Related to -" + nameof(EventSourcing) + "-"
        //)]
        //Now,

        [EnumMember(Description =
            "The concept in -" + nameof(ARComponents.ARCEvent) + "- of an \"axis\" (usefully referred to as the vertical axis) " +
            "within the concept of -" + nameof(PropertyStream2D) + "-.\r\n" +
            "\r\n" +
            "The RegStream is an immutable collection of user input, as generated in real-time, in order to attempt to " +
            "construct a correct -" + nameof(EventStream) + "-.\r\n" +
            "\r\n" +
            "Every -Reg- (Registration) in the RegStream has an associated -RegTime- (the time at which the user did the actual registration) and " +
            "an -EventTime- (the time index at which to insert the -Reg- into the resulting -" + nameof(EventStream) + "-).\r\n" +
            "\r\n" +
            "The RegStream is immutable but any registration in it can be corrected or deleted through another -Reg-, " +
            "meaning that as the user makes inputs, he / she can correct incorrect inputs by making new inputs.\r\n" +
            "\r\n" +
            "The RegStream is the only component that MUST be persisted in the -" + nameof(PropertyStream2D) + "- world, " +
            "as the -" + nameof(EventStream) + "- can always be reconstructed from the RegStream.\r\n" +
            "\r\n" +
            "See also -" + nameof(PSPrefix.RS) + "-."
        )]
        RegStream,

        [EnumMember(Description =
            "The concept in -" + nameof(ARComponents.ARCEvent) + "- of an \"axis\" (usefully referred to as the horizontal axis) " +
            "within the concept of -" + nameof(PropertyStream2D) + "-.\r\n" +
            "\r\n" +
            "This is the timeline of how -Event-s actually unfolded in real life, or how they are supposed to unfold into the future.\r\n" +
            "\r\n" +
            "The EventStream continues \"automatically\" into the future if the -" + nameof(RegStream) + "- contains -Reg-s (Registrations) with " +
            "an event time into the future.\r\n" +
            "\r\n" +
            "The EventStream is generated from an -" + nameof(RegStream) + "-." +
            "\r\n" +
            "See also -" + nameof(PSPrefix.ES) + "-."
        )]
        EventStream,

        [EnumMember(Description =
            "The concept in -" + nameof(ARComponents.ARCEvent) + "- of how an -Event- in the -" + nameof(EventStream) + "- generates transient events which in turn may generate\r\n" +
            "new transient events.\r\n" +
            "\r\n" +
            "The whole tree is a summary of all the changes caused by one root event.\r\n" +
            "(limited to the changes as seen at that time on the -" + nameof(EventStream) + "- timeline " +
            "not necessarily follow through changes to other transient events belong to other root events further in the future of the event timeline).\r\n" +
            "\r\n"
        )]
        TransientEventTree,

        // TODO: Move text to documentation for BasePKAttribute.
        [EnumMember(
            Description =
                "The concept of specifying in a single place all relevant information about a given property (data field).\r\n" +
                "\r\n" +
                "The purpose is to give 'at-a-glance' information about all relevant aspects of a given property, " +
                "without having to navigate through a complex code database. " +
                "And also with the goal of reducing the amount of -" + nameof(ApplicationSpecificCode) + "-.\r\n" +
                "\r\n" +
                "Note: You can start using AgoRapide without this concept.\r\n" +
                "AgoRapide as a storage system is usable without define ANY schema / tagging at all. " +
                "Just create -" + nameof(PropertyStreamLine) + "-s and see how they immediately gets understood by a standard " +
                "-" + nameof(ARComponents.ARAAPI) + "- application.\r\n" +
                "\r\n" +
                "See -" + nameof(PK) + "-, / -" + nameof(BasePKAttribute) + "- where the tagged attributes are stored.\r\n" +
                "\r\n" +
                "This central concept in AgoRapide (in addition to -" + nameof(ARConcepts.PropertyStream) + "-) " +
                "ensures that once you have decided that you need a field like for instance 'Customer.FirstName' " +
                "then all of the functionality (or as much as possible of it) for that field " +
                "can reside in the C# code where it was originally defined / declared.\r\n" +
                "\r\n" +
                "TODO: Give som practical examples of what is meant by this.\r\n" +
                "\r\n" +
                "In the rest of your application logic there should be less need for referring to 'FirstName'. " +
                "Like implementing setters and getters, reading from and storing to database, logging access, storing historical data, " +
                "UI-presentation (including transformations and joins), input-validation and so on.\r\n" +
                "\r\n" +
                "In other words, all application logic related to 'FirstName' should reside where 'FirstName' was originally defined, " +
                "enabling you to at-a-glance ascertain how FirstName is used throughout the application.\r\n" +
                "\r\n" +
                "If all this can be sewn together in a coherent manner then a huge amount of boilerplate code disappears. This again will help with " +
                "keeping the complexity of your application down as it matures. " +
                "\r\n" +
                "In practice, all this is accomplished by you (the user of AgoRapide) by defining enums tagged as -" + nameof(AREnumType.PropertyKeyEnum) + "-. " +
                "(Example: See how -" + nameof(StreamProcessorP) + "- is tagged as '[Enum(AREnumType = AREnumType.PropertyKey)]'.)\r\n" +
                "\r\n" +
                "The values of these enum definitions again are tagged through the -" + nameof(BasePKAttribute) + "- mechanism.\r\n" +
                "(Example: See how -" + nameof(StreamProcessorP.LocalStorageFolder) + "- is tagged with -" + nameof(PKTypeAttribute) + "-'s 'Description' and 'IsObligatory'.)\r\n " +
                "\r\n" +
                "The end-result of all this tagging ends up in a -" + nameof(PK) + "- instance (which may contain multiple -" + nameof(BasePKAttribute) + "- instances).\r\n" +
                "(Example: -" + nameof(ActualConnectionP.CountSendMessage) + "- is tagged by both a -" + nameof(PKTypeAttribute) + "- and a -" + nameof(PKLogAttribute) + "-.)\r\n" +
                "\r\n" +
                "-" + nameof(ARComponents.ARCCore) + "- contains some implementations of -" + nameof(BasePKAttribute) + "- like -" + nameof(PKTypeAttribute) + "-  " +
                "(which will always be available for a given property) " +
                "and also -" + nameof(PKLogAttribute) + "-, -" + nameof(PKHTMLAttribute) + "- and-" + nameof(PKRelAttribute) + "-. " +
                "\r\n" +
                "Other -" + nameof(ARComponents) + "- could also introduce more implementations of -" + nameof(BasePKAttribute) + "- in order to implement their functionality.\r\n" +
                "\r\n" +
                "You are also strongly encouraged to write your own classes inheriting -" + nameof(BasePKAttribute) + "- " +
                "whenever you have code implementing some specific aspects about a property key.\r\n" +
                "TODO: INSERT EXAMPLE HERE OF CUSTOM-BUILT BasePKAttribute.\r\n" +
                "\r\n" +
                "For some easy-to-follow examples of this, see -" + nameof(ARComponents.ARCDoc) + "-.",
            LongDescription =
                "FAQ: Why does AgoRapide tag enum members (enum values) instead of just directly tagging ordinary class properties?\r\n" +
                "Because it gives greater flexibility, especially with -" + nameof(PropertyAccess) + "-.\r\n" +
                "We need the simplest possible mechanism on which to 'hang' the tags, and enum's are perfectly suited for that purpose.\r\n" +
                "They are easy to refer to and it requires very little use of reflection (or even none at all).\r\n" +
                "It does however make the classes look strange, they may even be totally empty in many cases.\r\n" +
                "One example of flexibility is when a class can have lets say 50 members, but you only need to have a -" + nameof(Subscription) + "- " +
                "for 5 of those. Initializing the class with 5 properties instead of 50 then takes up correspondingly less memory " +
                "(because even null-values for ordinary properties will take up at least 8 bytes for each property).\r\n"
        )]
        TaggingOfPropertyKeys,

        // TODO: RENAME INTO StandardizedPropertyAccess or StandardizedPropertyStorage.
        // TODO: GO THROUGH THIS TEXT AGAIN AND MAKE SURE IT HAS MEANING!
        [EnumMember(
            Description =
                "The general mechanism for accessing properties of objects in AgoRapide.\r\n" +
                "\r\n" +
                "Implemented through the -" + nameof(IP) + "- (IProperty) interface mechanism.\r\n" +
                "\r\n" +
                "In applications with lot of different data fields it might be difficult to " +
                "1) Specify from the beginning exactly which fields you need, " +
                "2) To decide if they are actually available at the stage of processing, " +
                "3) To explain them in logs and debug-messages and " +
                "4) To implement storage for them.\r\n" +
                "\r\n" +
                "This can lead to a messy bug-prone implementation because you have to make assumptions about the data available, or you just leave out " +
                "desired functionality because of a constant need for generating boilerplate code just to accomplish what is really a trivial task " +
                "that should be inbuilt / automated somehow from the beginning." +
                "TODO: CLARIFY SENTENCE ABOVE.\r\n" +
                "\r\n" +
                "AgoRapide uses a standardized generic mechanism for accessing properties of objects. " +
                "It is implemented through the -" + nameof(IP) + "- (IProperty) interface mechanism\r\n" +
                "\r\n" +
                "This means that for every field / property you have concepts like the following ready for use:\r\n" +
                "\r\n" +
                "1) Both Get and TryGet are always available. " +
                "Get will throw a detailed exception if not successful (see default interface methods in -" + nameof(IP) + "- like -" + nameof(IP.TryGetV) + "-)\r\n" +
                "\r\n" +
                "2) Get has the choice of a default value to return if not successful (instead of throwing an exception) " +
                "(see default interface methods in -" + nameof(IP) + "- like -" + nameof(IP.GetV) + "-).\r\n" +
                "\r\n" +
                "3) Meta-information for a property, like who created it or how old it is " +
                "(see -" + nameof(PP) + "- like -" + nameof(PP.Cid) + "- and -" + nameof(PP.Created) + "-).\r\n" +
                "\r\n" +
                "4) Defining cardinality like single, multiple and so on (with corresponding validation and parsing) " +
                "(see -" + nameof(PKTypeAttributeP.Cardinality) + "-).\r\n" +
                "\r\n" +
                "5) Whether the field is obligatory or not (see -" + nameof(PKTypeAttributeP.IsObligatory) + "-).\r\n" +
                "\r\n" +
                "6) Default value (see -" + nameof(PKTypeAttributeP.DefaultValue) + "-).\r\n" +
                "\r\n" +
                "7) Guard against multiple setting of same property (see -" + nameof(IP.SetP) + "- versus -" + nameof(IP.AddP) + "-, " +
                "the latter throws a -" + nameof(IP.KeyAlreadyExistsException) + "- for multiple attempts to set a property) " +
                "\r\n" +
                "TODO: Regarding 7), introduce in PKTypeAttribute a setting for this (SetOnlyOnceAllowed or similar)\r\n" +
                "TODO: but remember that must then be honoured by all implementations of -" + nameof(IP.AddP) + "- and\r\n" +
                "TODO: you must also think through implications when updating object from the property stream\r\n" +
                "\r\n" +
                "At the entity level (-" + nameof(IP) + "--level), you get the following:\r\n" +
                "1) Automatic assertion of integrity " +
                "(assertion that -" + nameof(PKTypeAttributeP.IsObligatory) + "- values are set for instance, see -" + nameof(IP.AssertIntegrity) + "-).\r\n" +
                "\r\n" +
                "2) An automatic ToString() method for your objects " +
                "(not even with boilerplate auto-generated code but all automatically from within the framework itself).\r\n" +
                "TODO: Actually not implemented as of May 2020, but 'easily done'.\r\n" +
                "\r\n" +
                "3) A truly 'deep' copy is always possible (see -" + nameof(IP.DeepCopy) + "-).\r\n" +
                "\r\n" +
                "4) Changes are automatically logged (see -" + nameof(PSPrefix.app) + "- and -" + nameof(ExposingApplicationState) + "-).\r\n" +
                "\r\n" +
                "See also -" + nameof(ITypeDescriber) + "- which extends the idea of a standardized parsing and validation mechanism to single property-values also",
            LongDescription =
                "FAQ: How does the -" + nameof(PropertyAccess) + "- mechanism work with inheritance?\r\n" +
                "Classes can of course have inheritance but accessing the properties may be a little non-intuitive.\r\n" +
                "For instance, for -" + nameof(ClassAttribute) + "-, the property -" + nameof(BaseAttributeP.Description) + "- " +
                "from the base type -" + nameof(BaseAttribute) + "- is also present. " +
                "Unfortunately you get little help from tools like Intellisense in Visual Studio to remember this " +
                "(unless you implement -" + nameof(PKTypeAttributeP.BoilerplateCodeProperties) + "-).\r\n" +
                "However, all the information is contained within -" + nameof(TaggingOfPropertyKeys) + "- " +
                "so -" + nameof(ARComponents.ARCAPI) + "- for instance (with help from -" + nameof(ARComponents.ARCQuery) + "-) " +
                "can communicate the relationships clearly to the outside, for instance expressed in Graph QL SDL.\r\n" +
                "\r\n" +
                "In general this way of storing properties incidentally also helps with issues like\r\n" +
                "\r\n" +
                "1) Enabling the AgoRapide concept of -" + nameof(PropertyStream) + "- like " +
                "serialization (see -" + nameof(IP.ToPropertyStream) + "- and\r\n" +
                "deserialization (see -" + nameof(PropertyStreamLineParsed.TryParse) + "-),\r\n" +
                "\r\n" +
                "2) -" + nameof(TaggingOfPropertyKeys) + "-,\r\n" +
                "\r\n" +
                "etc. etc..\r\n" +
                "\r\n" +
                "Some drawbacks of this mechanism are of course:\r\n" +
                "\r\n" +
                "1) The risk of increased memory usage. See -" + nameof(MemoryConsumption) + "- for more details about this.\r\n" +
                "\r\n" +
                "2) Convoluted syntax when accessing properties.\r\n" +
                "This can be alleviated by implementing traditional setters / getters, " +
                "using the standardized property access mechanism 'behind the scene'.\r\n" +
                "\r\n" +
                "3) Less static strong typing than what is default in C# (see for instance -" + nameof(IKIP.AssertTypeIntegrity) + "-).\r\n" +
                "\r\n" +
                "All these drawbacks, 1), 2) and 3) can be alleviated by using traditional setters / getters, and also traditional storage. " +
                "See -" + nameof(ARComponents.ARCDoc) + "- for practical examples of this, especially classes -Demonstrator-, -Apple-, -Orange- and -Banana-.\r\n" +
                "\r\n" +
                "See also -" + nameof(PKTypeAttributeP.BoilerplateCodeProperties) + "- and -" + nameof(ClassAttributeP.BoilerplateCodePropertiesAndStorage) + "-.\r\n"
        )]
        PropertyAccess, // TODO: RENAME INTO StandardizedPropertyAccess or StandardizedPropertyStorage.

        [EnumMember(
            Description =
                "Avoiding repetition, the overarching philosophy that inspired the rest of AgoRapide.\r\n" +
                "\r\n" +
                "By repetition we mean all kinds of tasks that are similar to each other, both:\r\n" +
                "1) Within an application and \r\n" +
                "2) Across different applications.\r\n" +
                "\r\n" +
                "Some examples of 1) are documentation, logging, debugging and business reporting.\r\n" +
                "These are actually quite similar problem domains and therefore handled in AgoRapide by a lot of common code.\r\n" +
                "(see -" + nameof(ARComponents.ARCQuery) + "- for more information about reports / queries).\r\n" +
                "\r\n" +
                "Some examples of 2) are creation of database tables, creation of C# objects, implementing properties for objects, populating objects, " +
                "validating properties (and handling the unknown / null / maybe scenarios), traversing entity relations, " +
                "creating API-methods (building API endpoints), writing unit tests.\r\n" +
                "These are quite similar across applications and should not demand much -" + nameof(ApplicationSpecificCode) + "-\r\n" +
                "\r\n" +
                "Every time that we developers see such patterns of repetition we try to factor out the common component. " +
                "\r\n" +
                "AgoRapide has been created for the purpose of making this process easier.\r\n" +
                "\r\n" +
                "The advantages of this are self-evident: " +
                "Less code, less complexity, easier debugging, more meaningful workdays as a programmer and so on and so on.\r\n" +
                "\r\n" +
                "We do especially want to remove some of the friction resulting when working with a system where you know " +
                "that some common functionality for dealing with a problem really should have been implemented. " +
                "But since it was not available from the start it is too time-consuming to implement now, " +
                "meaning that instead a lot of kludges get scattered all over the code in order to at least cover some emergency needs. " +
                "It all ends up with an in-overall inefficient and costly to maintain system.\r\n" +
                "\r\n" +
                "To quote Paul Graham 'The shape of a program should reflect only the problem it needs to solve'\r\n" +
                "(Paul Graham - Revenge of the Nerds, http://www.paulgraham.com/icad.html ).\r\n" +
                "By utilizing AgoRapide your application will hopefully accomplish this (but maybe we should have written AgoRapide in Lisp though).\r\n" +
                "\r\n" +
                "Another quote:\r\n" +
                "'Fools ignore complexity. Pragmatists suffer it. Some can avoid it. Geniuses remove it.'\r\n" +
                "(Alan Perlis, Epigrams on Programming, 1982, ACM SIGPLAN Notices #58, https://en.wikiquote.org/wiki/Alan_Perlis#Epigrams_on_Programming,_1982 ).\r\n" +
                "\r\n" +
                "However, we also have this gem, not to be forgotten:\r\n" +
                "'Everyone knows that debugging is twice as hard as writing a program in the first place. " +
                "So if you're as clever as you can be when you write it, how will you ever debug it?'\r\n" +
                "(Brian Kernighan, The Elements of Programming Style, https://en.wikiquote.org/wiki/Brian_Kernighan ).\r\n" +
                "\r\n" +
                "AgoRapide's level of abstraction is already close to this territory (about debugging is twice as hard). " +
                "However, your resulting application will hopefully require much " +
                "less cleverness thanks to AgoRapide, so at least you can debug your own code very easily " +
                "(see also -" + nameof(AssertionsAndDetailedExceptionMessages) + "-).\r\n",
            LongDescription =
                "In -" + nameof(ARComponents.ARCAPI) + "- you will find a quite intruiging case with -BaseController- where " + // Note: nameof(BaseController) not possible since we do not link to ARCAPI from ARCCore. Link will resolve anyway if documenting application references it.
                "the API-routing information is exposed with the same mechanism that answers REST queries."
        )]
        AvoidingRepetition,

        [EnumMember(
            Description =
                "AgoRapide has roots back to 2011 when Bjørn Erling Fløtten invented panSL (SL = Schema language).\r\n" +
                "\r\n" +
                "Introduced in 2012 panSL (panSL.org) was an excercise in data modelling where the principle of -" + nameof(TaggingOfPropertyKeys) + "- orginated, " +
                "in that case, tagging each element of the schema in order to create a data-driven application with only 'one page' of code.\r\n" +
                "The implementation of panSL was called AgoRapide (AgoRapide.com).\r\n" +
                "It had litte practical use.\r\n" +
                "\r\n" +
                "In 2017 a new version of AgoRapide (https://github.com/AgoRapide/AgoRapide), not using panSL at all, was introduced, as a " +
                "'Lightweight pragmatic integrated .NET REST API offering JSON and HTML views. Written in C# 7.0'.\r\n" +
                "This version introduced -" + nameof(PropertyAccess) + "- and most other -" + nameof(ARConcepts) + "-.\r\n" +
                "It was 'halfway' to implementing -" + nameof(PropertyStream) + "-, in the sense that all data where stored as 'key, value' " +
                "but it was still dependent on an underlying storage mechanism, like PostgreSQL.\r\n" +
                "The 2017 version has had applications within some companies, " +
                "including a modestly successful IoT related company (Sikom Connect AS of Trondheim, Norway).\r\n" +
                "Its weaknesses where its monolitic structure and too much emphasis on being a REST API " +
                "(meaning it was an 'all-or-nothing package', without useful single components).\r\n" +
                "\r\n" +
                "The current 2020 version of AgoRapide (this version), uses .NET Core and .NET Standard.\r\n" +
                "It introduces its own storage mechanism (see -" + nameof(StreamProcessor) + "-).\r\n" +
                "It consists of multiple mostly independent components (see -" + nameof(ARComponents) + "-).\r\n" +
                "It is built outwards from the core component -" + nameof(ARComponents.ARCCore) + "-.\r\n" +
                "Each component is kept as simple as possible. -" + nameof(ARComponents.ARCCore) + "- for instance can be accessed " +
                "through the concept of a Console application and it does not link to any library outside of Microsoft .NET itself.\r\n" +
                "(in general the 2020 version of AgoRapide should be ideally suited for containerization)."
        )]
        AgoRapideHistory,

        // TODO: Delete commented out code
        //[EnumMember(
        //    Description =
        //        "AgoRapide does not give a unique identifier to every object / property (due to memory usage concerns). " +
        //        "\r\n" +
        //        "Instead each object / property has an id valid within a limited scope or context " +
        //        "described by a collection of -" + nameof(IK) + "- (as stored inside -" + nameof(IKIP.Key) + ").\r\n" +
        //        "\r\n" +
        //        "TOOD: This text is difficult to understand and does not actually give any useful information.\r\n" +
        //        "\r\n" +
        //        "TODO: This enum-member (-" + nameof(IdLooseStorage) + "-) has marginal value, try to move text elsewhere and delete member.\r\n" +
        //        "TODO: Move for instance text to -" + nameof(Cardinality.IndividualItems) + "-.\r\n" +
        //        "\r\n" +
        //        "TODO: Write something about keys not being stored inside object (CustomerId is not stored inside Customer typically).\r\n" +
        //        "\r\n" +
        //        "Example 1): If the scope is 'Customer' then an id for a specific customer could be '42'" +
        //        "\r\n" +
        //        "Example 2): If the scope is 'Customer/42' then an id for a PhoneNumber would be either:\r\n" +
        //        "a) For storing only single values (see -" + nameof(Cardinality.HistoryOnly) + "-):\r\n" +
        //        "'Customer/42/PhoneNumber = +90534333'\r\n" +
        //        "or\r\n" +
        //        "b) For storing multiple values (see -" + nameof(Cardinality.WholeCollection) + "-):" +
        //        "'Customer/42/PhoneNumber/+4790534333'\r\n" +
        //        "'Customer/42/PhoneNumber/+4740178178'\r\n" +
        //        "and so on.\r\n",
        //    LongDescription =
        //        "For more information, see -" + nameof(IKIP.Key) + "-, -" + nameof(IK) + "- and especially -" + nameof(IKString) + "- which is used for storing the " +
        //        "(usually as a sequential number) general id of everyday objects. See also -" + nameof(PRich) + "- which uses -" + nameof(IK) + "- for " +
        //        "indexing the whole object storage"
        //)]
        //IdLooseStorage,

        [EnumMember(
            Description =
                "The concept of a -" + nameof(ARNodeType.Client) + "- doing an ad-hoc query against (normally) a -" + nameof(ARNodeType.ArmDB) + "-." +
                "\r\n" +
                "Background: Ordinarily, -" + nameof(Subscription) + "- are 'permanently' on-going, " +
                "and ad-hoc queries are done in-memory locally by each client, " +
                "but since there is just some small differences conceptually in doing ad-hoc queries over a TCP/IP connection, " +
                "AgoRapide implements also this mechanism.\r\n" +
                "\r\n" +
                "-" + nameof(AdHocQuery) + "- is most relevant against an -" + nameof(ARNodeType.ArmDB) + "- storing its data in a -" + nameof(PRich) + "-, " +
                "(The query itself is sent as a collection of -" + nameof(Subscription) + "- elements, " +
                "and when server answers with -" + nameof(ClientUpdatePosition.IsAtEndOfStream) + "- the client can close the query connection)\r\n" +
                "\r\n" +
                "TODO: Not implemented as of May 2020.\r\n" +
                "TODO: Clarify difference between -" + nameof(ClientUpdatePosition) + "- and -" + nameof(Subscription.FromTime) + "- / -" + nameof(Subscription.ToTime) + "-.\r\n" +
                "\r\n" +
                "Note that there would be little meaning in doing an ad-hoc query against a -" + nameof(ARNodeType.CoreDB) + "- because " +
                "the core only stores single -" + nameof(PropertyStream) + "--lines, meaning it would have to read through the whole database " +
                "from start to end in order to ensure that all data is found (the query would be an O(n) one instead of O(1)).\r\n" +
                "\r\n" +
                "Hint: If you are concerned about the risk of having to do O(n) queries, you are recommended to use an established " +
                "key-value storage (like Apache Kafka, AWS DynamoDB or Azure Cosmos DB) as your main database, and sync AgoRapide to that database.\r\n" +
                "See -" + nameof(ARComponents.ARCSync) + "- for more information about this.\r\n" +
                "\r\n" +
                "TOOD: Could potentially be deleted as ARConcept when a Query-metod is implemented somewhere."
        )]
        AdHocQuery,

        [EnumMember(
            Description =
                "Code originating from the main official AgoRapide distribution " +
                "(which therefore does not include any end-application specific code)\r\n" +
                "\r\n" +
                "TODO: Consider creating new enum CodeLocation or similar,\r\n" +
                "TOOD: comprising -" + nameof(StandardAgoRapideCode) + "- and -" + nameof(ApplicationSpecificCode) + "-.\r\n" +
                "\r\n" +
                "Although AgoRapide is open-source with an extremely flexible license (and you may therefore mix its code with your own), " +
                "it is anyway recommended to keep core parts of the system, " +
                "like all -" + nameof(ARNodeType.CoreDB) + "- and as many of -" + nameof(ARNodeType.ArmDB) + "- " +
                "as possible, based on only the official AgoRapide distribution, that is, without any -" + nameof(ApplicationSpecificCode) + "-. " +
                "Following this recommendation will ensure greater stability for the critical core parts of your system.\r\n" +
                "\r\n" +
                "A node with only -" + nameof(StandardAgoRapideCode) + "- therefore most probably knows very little about end-application specific types. " +
                "Although by compiling it with only the standard AgoRapide code it will be able to receive some -" + nameof(PK) + "- information " +
                "(through the -" + nameof(ARConcepts.PropertyStream) + "-), it will not be able to do any type-specific operations. " +
                "\r\n" +
                "For example: With something like 'Customer/42/PhoneNumber = +4790534333' it will understand that 'Customer' is a " +
                "-" + nameof(IKType) + "-, '42' is a -" + nameof(IKString) + "-, and 'PhoneNumber' is a -" + nameof(ITypeDescriber) + "-, " +
                "but it will not be able to actually create a Customer-object nor a PhoneNumber because the definition for those types are unknown " +
                "(the types are not present in the assemblies constituting the application)."
        )]
        StandardAgoRapideCode,

        [EnumMember(
            Description =
                "Code specific for an end-application.\r\n" +
                "\r\n" +
                "Should be kept to a minimum (obviously).\r\n" +
                "\r\n" +
                "TODO: Consider creating new enum CodeLocation or similar,\r\n" +
                "TOOD: comprising -" + nameof(StandardAgoRapideCode) + "- and -" + nameof(ApplicationSpecificCode) + "-.\r\n" +
                "\r\n" +
                "Will usually be contained within a -" + nameof(ARNodeType.Client) + "- and some -" + nameof(ARNodeType.ArmDB) + "-.\r\n" +
                "\r\n" +
                "Through -" + nameof(AvoidingRepetition) + "-, AgoRapide strives to help you keep your -" + nameof(ApplicationSpecificCode) + "- to a minimum.\r\n" +
                "\r\n" +
                "See -" + nameof(StandardAgoRapideCode) + "- for more information",
            LongDescription =
                "Note that when running in the same process space, application specific elements like types for instance, will be recognized and " +
                "used also by -the " + nameof(StandardAgoRapideCode) + "-."
        )]
        ApplicationSpecificCode,

        [EnumMember(
            Description =
                "A naïve implementation of key-value storages like AgoRapide may lead to excessive memory (RAM) consumption.\r\n" +
                "\r\n" +
                "AgoRapide has been designed in order to be easy to start with, but still with inbuilt flexibility to reduce memory " +
                "consumption as your database grows and your application matures.\r\n" +
                "\r\n" +
                "-" + nameof(ARNodeType.CoreDB) + "- will mostly store data on disk and have little need for RAM. " +
                "Sidenote: Although the -" + nameof(PropertyStream) + "--format is somewhat verbose it is well suited for on-disk compression.\r\n" +
                "\r\n" +
                "-" + nameof(ARNodeType.ArmDB) + "- / -" + nameof(ARNodeType.Client) + "- using -" + nameof(PRich) + "- is more sensitive to RAM " +
                "but you have a great flexibility in how much data they actually have to subscribe to.\r\n" +
                "\r\n" +
                "This is because the implementations of -" + nameof(IP) + "- in -" + nameof(ARComponents.ARCCore) + "- " +
                "are chosen dynamically according to need.\r\n" +
                "See for instance use of the -" + nameof(PValue<TValue>) + "- class, which will be dynamically replaced " +
                "with a -" + nameof(PRich) + "- instance if a need for " +
                "storing meta-data like -" + nameof(PP.Cid) + "-, -" + nameof(PP.Created) + "- or similar arises.\r\n" +
                "\r\n" +
                "Example: If the -" + nameof(ARConcepts.PropertyStream) + "- contains the following:\r\n" +
                "\r\n" +
                "Customer/42/FirstName = John\r\n" +
                "Customer/42/FirstName.Cid = Agent/42 // Creator id (entity which created this property). See -" + nameof(PP.Cid) + "-\r\n" +
                "...\r\n" +
                "Customer/42/FirstName.Valid = 2021-02-10 // Timestamp when property was last known to be (still) valid. See -" + nameof(PP.Valid) + "-\r\n" +
                "\r\n" +
                "then a full subscription to the customer-object like '+dt/Customer/*' would initially result in a -" + nameof(PValue<TValue>) + "- object being created by " +
                "-" + nameof(PropertyStreamLine.TryStore) + "-, then it would be replaced with a -" + nameof(PRich) + "- in order to also stored metadata Cid and Valid.\r\n" +
                "\r\n" +
                "However, if a -" + nameof(Subscription) + "- does not include any of this meta-data, like '+dt/Customer/*/FirstName', '+dt/Customer/*/LastName" +
                "then only a -" + nameof(PValue<TValue>) + "- instance will be created at the subscriber side, because the metadata will not be received.\r\n" +
                "This means that single nodes in the system can have reduced memory consumption just by judiciously chosing what data is subscribed to.\r\n" +
                "\r\n" +
                "NOTE: You can use the memory efficient -" + nameof(PExact<TPropertyKeyEnum>) + "- class\r\n" +
                "NOTE: as basis for your 'entity' classes when you know exactly what you want to store in them and\r\n" +
                "NOTE: when you do not need to store any meta-data like -" + nameof(PP.Cid) + "- and -" + nameof(PP.Created) + "-.\r\n" +
                "\r\n" +
                "See -" + nameof(ARComponents.ARCDoc) + "-, especially classes -Apple-, -Orange- and -Banana- for different compromises between memory usage and easy of development.\r\n" +
                "\r\n" +
                "See also -" + nameof(Indexing) + "-."
        )]
        MemoryConsumption,

        [EnumMember(Description =
            "In-memory indexing of data is done 'behind-the-scenes' by some classes implementing -" + nameof(IP) + "-.\r\n" +
            "\r\n" +
            "One such class included in -" + nameof(ARComponents.ARCCore) + "- is -" + nameof(PCollection) + "-\r\n" +
            "which uses -" + nameof(ARConcepts.Indexing) + "- for all keys ending with 'Id' (keys assumed to be foreign keys).\r\n" +
            "\r\n" +
            "This class is useful as container for a collection of entities (a table in RBDMS-terms).\r\n" +
            "\r\n" +
            "Methods in -" + nameof(IP) + "- which should be used if indexing is assumed to be necessary, is:\r\n" +
            "-" + nameof(IP.TrySetPP) + "-: SetPP = set property of property. Sets property of entity contained within this class\r\n" +
            "-" + nameof(IP.GetKeysEqualToValue) + "-: Returns properties which themselves again have key and value equal to the given key and value.\r\n" +
            "Note that these are default interface methods, that is, the default implementation does not support indexing.\r\n" +
            "\r\n" +
            "-" + nameof(PropertyStreamLine.TryStore) + "- will look for classes called '{EntityType}Collection' to instantiate " +
            "as containers for collections of entities (classes with such names are assumed to be suitable for that purpose).\r\n" +
            "Example: If the application has a type 'Order', then a class called 'OrderCollection' (if found) will be used as storage " +
            "container for the collection of 'Order' objects (if not the 'ordinary' -" + nameof(PRich) + "- will be used).\r\n" +
            "\r\n" +
            "Such a class can be created just by the following code:\r\n" +
            "  public class OrderCollection : PCollection {\r\n" +
            "  }\r\n" +
            "\r\n" +
            "In this manner, indexing will be ensured for foreign keys like CustomerId, EmployeeId, ShipperId and so on.\r\n" +
            "\r\n" +
            "Note that other -" + nameof(ApplicationSpecificCode) + "- implementations does not necessarily have to " +
            "inherit -" + nameof(PCollection) + "- but they should implemented interfaces like -" + nameof(IGetKeysEqualToValue) + "- " +
            "in order to clearly state that they support indexing.\r\n" +
            "\r\n" +
            "Indexing will use more memory but is often necessary for big collections when querying across collections of entities (querying across tables).\r\n" +
            "\r\n" +
            "See also -" + nameof(MemoryConsumption) + "-.\r\n"
         )]
        Indexing,

        [EnumMember(
            Description =
                "The concept of how multiple -" + nameof(ARNodeType.CoreDB) + "- instances synchronizes their data.\r\n" +
                "\r\n" +
                "TODO: The inherent possibility in AgoRapide of having multiple -" + nameof(ARNodeType.CoreDB) + "- nodes\r\n" +
                "TODO: means that synchronization between these nodes must be considered.\r\n" +
                "TODO: As of Apr 2020 the most critical issue being identified is probably how to re-synchronize nodes after some\r\n" +
                "TODO: period of offline status / network outage and similar.\r\n" +
                "TODO: Also a mechanism for anointing a master-node and similar must be implemented.\r\n" +
                "\r\n" +
                "TODO: There is also the issue of what happens when the core itself is unavailable. In principle the clients are perfectly\r\n" +
                "TODO: capable of caching data, and sending it as soon as the core gets online again, but then there is the issue of ordering data\r\n" +
                "TODO: from the different client nodes correctly into the property stream.\r\n" +
                "TODO: One possible solution to this issue might be to just state that it is unsolved and that such cached data will be put into the\r\n" +
                "TODO: property stream without any coordination / ordering.\r\n" +
                "\r\n" +
                "See also -" + nameof(ConnectionInstructionP.MultipleConnectionUsage) + "- and -" + nameof(ConnectionInstructionP.Sharding) + "-."
        )]
        CoreDBSynchronization,

        [EnumMember(
            Description =
                "Some practical choices have been made in AgoRapide which requires object access to be done in a single-threaded manner, " +
                "or within a locking context (using for instance -" + nameof(System.Threading.ReaderWriterLockSlim) + "-).\r\n" +
                "\r\n" +
                "Examples are like\r\n" +
                "1) In objects like -" + nameof(PRich) + "-, using System.Collections.Dictionary instead of System.Collections.Concurrent.ConcurrentDictionary " +
                "due to memory concerns and (when building a hierchical object model) " +
                "due to the inherent dangers for bugs that a multi-threaded model would bring).\r\n" +
                "\r\n" +
                "Note hints in the documentation for how to write thread safe applications, " +
                "for instance documentation for -" + nameof(StreamProcessor.OutsideLocalReceiver) + "-. " +
                "Also note objects like -" + nameof(PConcurrent) + "- which may be used when you definitely need multi-threaded access.\r\n" +
                "\r\n" +
                "The inbuilt distributive possibilities in AgoRapide is supposed to be a good fit with this restriction. If you need more processing power than " +
                "what a single main-thread can give you, then it is very easy to distribute load over multiple -" + nameof(ARNodeType) + "- nodes. " +
                "Each of these nodes should anyway only have to do a simple easy to understand task, and then a single main thread should be no real restriction.\r\n" +
                "\r\n" +
                //"Actually, by forcing you (as the developer) to use a single-threaded model we actually assumed it makes your code easier to debug and follow. " +
                //"(it bears some resemblance to concepts used in embedded code (firmware), where there often is a single thread going through necessary actions in turn).\r\n" +
                //"\r\n" +
                "Note that where relevant AgoRapide does of course make extensive use of multithreaded code, ascynchronous code (async / await) and so on.\r\n"
        )]
        SingleThreadedCode,

        [EnumMember(
            Description =
                "The concept of when to delete old data (if deemed necessary).\r\n" +
                "\r\n" +
                "Because of inherent traits of the -" + nameof(PropertyStream) + "- concept, data will never be deleted by default.\r\n" +
                "\r\n" +
                "This might result in unacceptable storage requirements / low performance. " +
                "Some functionality for deletion is therefore envisaged to be necessary to implement.\r\n" +
                "See -" + nameof(NoRealDeletion) + "- for details.\r\n" +
                "\r\n" +
                "TODO: Not implemented as of Mar 2020. Not decided if should be in a specific -" + nameof(ARComponents) + "- or just part of -" + nameof(ARComponents.ARCCore) + "-.\r\n" +
                "Will probably be a specific -" + nameof(BasePKAttribute) + "- called PKRetAttribute, maybe also something added to -" + nameof(ClassAttribute) + "- " +
                "(or something added to -" + nameof(EnumAttribute) + "-. As of Apr 2020 we have not decided where / how to tag 'entity'-classes).\r\n" +
                "Note that retention at application level is also plausible. One scenario could be a high-availability log-collector delivering log-data to " +
                "more simple (cheaper) final storage nodes that are not necessarily high-availablity themselves. The log-collector only has to keep data for as long as " +
                "the final storage nodes are tolerated to be offline.",
            LongDescription =
                "Some ideas for how to do this:\r\n" +
                "Use a concept of 'revision number' in the actual storage file (as of Apr 2020 -" + nameof(StreamProcessor) + "- is prepared for this (see its _storage-member) " +
                "but use of it is not supported).\r\n" +
                "Example: We start with file 2020-04-13 14:19.R00001.txt and after the first compacting we store a new file called  2020-04-13 14:19.R00002.txt.\r\n" +
                "As long as there are clients 'using' the old file, it can be kept on disk (in order for -" + nameof(ClientUpdatePosition) + "- to be compatible.\r\n" +
                "OR, more probable, compact by leaving empty-lines, thereby making -" + nameof(ClientUpdatePosition) + "- unchanged.\r\n" +
                "We can also have a cut-off of for instance a month, and just not accept -" + nameof(ClientUpdatePosition) + "- older than this.\r\n" +
                "(A backup mechanism could also be to build a 'last Timestamp' feature into ClientUpdatePosition, " +
                "and use that as a last resort when file position can not be used).\r\n"
        // "\r\n" +
        //"Note: In order to keep -" + nameof(ARCCore) + "- as small as possible, the necessary interface for implementing data rentention can be declared as an interface " +
        //"in -" + nameof(ARCCore) + "-, and the details kept outside of it, for instance in a separate compoenent -" + nameof(ARComponents.ARCRet) + "-."
        )]
        DataRetention,

        [EnumMember(
            Description =
                "AgoRapide has no real concept for deletion of data (apart from -" + nameof(DataRetention) + "-, which is mostly relevant for 'old' data).\r\n" +
                "The closest concept is setting -" + nameof(PP.Invalid) + "- for a property.\r\n" +
                "\r\n" +
                "There might also be regulatory concerns demanding that you actually delete data as requested in your application.\r\n" +
                "(for instance deleting data for a specific customer. This will be more difficult though since AgoRapide does not have any concept of WHERE " +
                "in the property stream a given datapoint resides).\r\n" +
                "TODO: Implement 'Delete' as a command verb on the -" + nameof(ARConcepts.PropertyStream) + "-.\r\n"
        )]
        NoRealDeletion,

        [EnumMember(
            Description =
                "-" + nameof(ARComponents.ARCQuery) + "- supports many-to-many relations in a simple manner " +
                "without introducing a third entity type (a third table).\r\n" +
                "\r\n" +
                "This is done through having foreign keys with cardinality -" + nameof(CardinalityExtensions.IsMultiple) + "- " +
                "like -" + nameof(Cardinality.WholeCollection) + "- and -" + nameof(Cardinality.IndividualItems) + "-.\r\n" +
                "\r\n" +
                "Example 1), tagging PersonP.CarId with -" + nameof(Cardinality.IndividualItems) + "-:\r\n" +
                "  Person/42/CarId/1968\r\n" +
                "  Person/42/CarId/1981\r\n" +
                "  Person/43/CarId/1968\r\n" +
                "\r\n" +
                "Example 2), tagging PersonP.CarId with -" + nameof(Cardinality.WholeCollection) + "-:\r\n" +
                "  Person/42/CarId = 1968;1981\r\n" +
                "  Person/43/CarId/1968\r\n" +
                "\r\n" +
                "In both examples person 42 owns two cars (1968 and 1981), but shares car 1968 with person 43.\r\n" +
                "(you can of course also store many-to-many relations in the traditional RDBMS manner by using a third 'entity' type " +
                "called PersonCarOwnership or similar).\r\n"

        )]
        ManyToManyRelations,

        [EnumMember(
            Description =
                "Thanks to the tagging of enums and classes in AgoRapide (like this description), automatically generation of documentation is possible.\r\n" +
                "The actual functionality for that is not part of -" + nameof(ARComponents.ARCCore) + "- but is placed in -" + nameof(ARComponents.ARCDoc) + "." +
                "(apart from all the -" + nameof(BaseAttribute) + "- instances necessary for tagging the different classes and members of course)\r\n"
        )]
        Documentation,

        [EnumMember(
            Description =
                "The abstraction level in AgoRapide is very high.\r\n" +
                "\r\n" +
                "This means that if something breaks " +
                "(either due to a bug in the -" + nameof(StandardAgoRapideCode) + "- or " +
                "due to wrong use of the libraries from -" + nameof(ApplicationSpecificCode) + "-) " +
                "it can sometimes be difficult to grasp the issue involved.\r\n" +
                "\r\n" +
                "The -" + nameof(StandardAgoRapideCode) + "- therefore has lots of assertions (in order to catch the exception at its root origin) " +
                "and also very detailed exception messages (in order to clarify and educate).\r\n"
        )]
        AssertionsAndDetailedExceptionMessages,

        [EnumMember(
            Description =
                "The traditional debugging process usually entails reading through logs and querying about your application state through a debugging tool.\r\n" +
                "\r\n" +
                "AgoRapide instead encourages logs and application state to be distributed over the -" + nameof(PropertyStream) + "- by default, " +
                "thereby making debugging simpler (the information is always available, and not restricted to some local access).\r\n" +
                "\r\n" +
                "You are encouraged to expose your internal application state to the -" + nameof(PropertyStream) + "- through the use of " +
                "-" + nameof(IP.Logger) + "- (and possible also use -" + nameof(PKLogAttribute) + "-) " +
                "like in -" + nameof(PConcurrent) + "-.\r\n" +
                "It is recommended to prefix these data points by -" + nameof(PSPrefix.app) + "-.\r\n" +
                "\r\n" +
                "This has two implications (as implemented by -" + nameof(PConcurrent) + "-):\r\n" +
                "1) Any call to -" + nameof(IP.Log) + "- / -" + nameof(IP.HandleException) + "- ends up in the property stream.\r\n" +
                "2) Any change to internal properties will be reflected in the property stream automatically without even having " +
                "to make 'manual' calls to -" + nameof(IP.Log) + "- (because all calls to -" + nameof(IP.SetP) + "- will automatically be logged " +
                "to the property stream).\r\n" +
                "\r\n" +
                "Some example classes inheriting -" + nameof(PConcurrent) + "- and using this principle in AgoRapide are:\r\n" +
                "\r\n" +
                "-" + nameof(StreamProcessor) + "- (search for code like 'IP.SetPV(StreamProcessorP.ClientUpdatePositionPath ...)\r\n" +
                "-" + nameof(ActualConnection) + "- (search for code like 'IP.Inc(ActualConnectionP.CountReceiveMessage)')\r\n" +
                "\r\n" +
                "In summary, much of the inner working of these classes are automatically exposed to the property stream " +
                "without any special code having to be added.\r\n" +
                "Note how manual calls to -" + nameof(IP.Log) + "- are also automatically exposed to the property stream in these classes.\r\n" +
                "Note how threads are always given a descriptive name in these classes, making the logging even easier to follow.\r\n" +
                "\r\n" +
                "All this makes debugging and understanding the inner workings of your applications much easier " +
                "reducing the need for external logging and debugging tools.\r\n" +
                "\r\n" +
                "Note also that by exposing the internal application state to the -" + nameof(PropertyStream) + "- " +
                "it can be read 'from' wherever convenient. " +
                "\r\n" +
                "In other words, you do not have to limit use of -" + nameof(IP) + "- to only data-objects " +
                "like 'Customer', 'Order' and similar, but you can also use it for internal parts of your application.\r\n" +
                "\r\n" +
                "Note that calls to -" + nameof(IP.Log) + "- will by default not be exposed to the -" + nameof(ARConcepts.PropertyStream) + "-." +
                "\r\n" +
                "See also\r\n" +
                "-" + nameof(PSPrefix.app) + "-.\r\n" +
                "-" + nameof(PP.ExceptionText) + "-, -" + nameof(PConcurrentP.Heartbeat) + "-, \r\n" +
                "-" + nameof(IP.Log) + "-, -" + nameof(IP.HandleException) + "-"
        )]
        ExposingApplicationState,

        [EnumMember(
            Description =
                "Documentation with links is much easier to read.\r\n" +
                "\r\n" +
                "On the other hand, inserting links can often be quite tedious in a typical markup language.\r\n" +
                "\r\n" +
                "AgoRapide offers a very easy method for link insertion: " +
                "You just prepend and append the relevant word that you want a link to, with a minus sign / hyphen, '-' in the documentation.\r\n" +
                "\r\n" +
                "If the relevant word is a C# identifier, you should in addition use the 'nameof' keyword, " +
                "in order to catch renames and deletions of terms and also " +
                "in order to be able to easy navigate within your developing environment (like when working within Visual Studio, using F12).\r\n" +
                "\r\n" +
                "If this is done methodically, then any mechanism for creating for instance HTML documentation, " +
                "(like the one offered in -" + nameof(ARComponents.ARCDoc) + "-),  " +
                "can then first identify all terms (or files) that are created, " +
                "and then blindly replace in the text -[Filename]- with '<a href=\"[Filename]\">[Filename]</a>' " +
                "without actually knowing anything about the text's structure.\r\n" +
                "\r\n" +
                "This is a pragmatic approach that works surprisingly well.\r\n" +
                "See also -" + nameof(PSPrefix.doc) + "-.\r\n" +
                "\r\n" +
                "Some classes in -" + nameof(ARComponents.ARCDoc) + "- that support link insertion are " +
                "-HLocation-, -HLocationCollection-, -DocLink- and -DocLinkCollection- and also -DocLinkResolution- (which resolves ambigious links).\r\n",
            LongDescription =
                "FAQ: Why does AgoRapide not use XML comments (comments beginning with three slashes, '///')?\r\n" +
                "AgoRapide in general does not use XML comments.\r\n" +
                "That is because XML comments are not included in the C# language, nor are they included in the compiled executable, " +
                "that is, they are not available at run-time within the application.\r\n" +
                "They are also weakly referenced, meaning that if Intellisense or similar do not catch a rename or a deletion of some term, " +
                "then the references to it will become a dead link (in other words, XML comments are prone to link-rot).\r\n" +
                "(There is of course some use of XML comments in AgoRapide, especially when it is directly applicable to helping you as a developer to " +
                "understand how to use a specific method or a specific parameter to a method)."
        )]
        LinkInsertionInDocumentation,

        [EnumMember(Description =
            "AgoRapide uses non-nullable reference types as default throughout the system\r\n" +
            "\r\n" +
            "(through the setting <PropertyGroup><Nullable>enable</Nullable></PropertyGroup> in the .csproj-files).\r\n" +
            "\r\n" +
            "However, the TryGet- / TryParse-pattern does not work well with non-nullable reference types. " +
            "A compromise is being used in AgoRapide where actually \"null!\" is set for the out 'retval'-value when method return value is FALSE. " +
            "Assuming correct client-usage, this use of \"null!\" is considered to be an acceptable compromise.\r\n" +
            "\r\n" +
            "(Correspondingly, the out \"errorResponse\" value is set to \"null!\" when method return value is TRUE.)\r\n" +
            "\r\n" +
            "For more details, see\r\nhttps://softwareengineering.stackexchange.com/questions/387674/c-8-non-nullable-references-and-the-try-pattern"
        )]
        TryPatternAndNull,
    }
}