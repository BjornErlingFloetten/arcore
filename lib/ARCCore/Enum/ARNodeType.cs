﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System.Collections.Generic;
using System.Text;

namespace ARCCore {

    [Enum(
        // Maybe this will change to en ordinary Enum, if the node actually is configured with a NodeType.
        AREnumType = AREnumType.DocumentationOnlyEnum,
        Description =
            "Describes what function a node in an AgoRapide system serves. A given node may constitute of only -" + nameof(ARConcepts.StandardAgoRapideCode) + "- " +
            "or a combination of -" + nameof(ARConcepts.StandardAgoRapideCode) + "- and -" + nameof(ARConcepts.ApplicationSpecificCode) + "-.",
        LongDescription =
            "Thanks to -" + nameof(ARConcepts.PropertyStream) + "- it is very easy to create multiple components of your application, each which does a " +
            "limited number of tasks but easy understood and in an efficient manner with high performance.\r\n" +
            "\r\n" +
            "Note: As of Mar 2020 the actual code in AgoRapide makes little reference to this enum (" + nameof(ARNodeType) + "). " +
            "Instead, the type of node is decided by what kind of connections it is instructed to make\r\n" +
            "\r\n"
        )]
    public enum ARNodeType {
        __invalid,

        [EnumMember(
            Description =
                "A node containing the core or central database.\r\n" +
                "\r\n" +
                "Normally this is a -" + nameof(ARComponents.ARADB) + "-.\r\n" +
                "\r\n" +
                "A node which stores data as single -" + nameof(PropertyStreamLine) + "-s and keeps track of the entire database " +
                "(maybe in cooperation with other nodes of same type).\r\n" +
                "\r\n" +
                "End-application specific code is typically not included in the node. " +
                "The node will most probably accept -" + nameof(Subscription) + "- from other nodes (as subscribing clients).\r\n" +
                "\r\n" +
                "The general principle is to have the core nodes as simple ('as dumb') as possible, leaving to the edges to implement the rich " +
                "application specific functionality (just like the Internet is built up).",
            LongDescription =
                "The node should be compiled with only -" + nameof(ARConcepts.StandardAgoRapideCode) + "- because that code is assumed to be stable " +
                "and the core functionality is critical.\r\n" +
                "NOTE: There is actually nothing stopping you from having the core database node being part of your application. But it will lead to " +
                "longer startup times because the database is mostly in-memory and has to be read from disk at startup. Having the core database " +
                "running as a separate node eliminates this issue."
        )]
        CoreDB,

        [EnumMember(
            Description =
                "A node with a 'client -" + nameof(Subscription) + "-' towards -" + nameof(CoreDB) + "- to a (usually) subset of the entire database (acting as a cache and filtering mechanism), " +
                "or with a 'client -" + nameof(Subscription) + "-' towards -" + nameof(CoreDB) + "- to the whole database (acting as a cache mechanism only)\r\n" +
                "\r\n" +
                "and which stores data as either:\r\n" +
                "\r\n" +
                "1) -" + nameof(PropertyStreamLine) + "-s (enabling it to servere ordinary continous -" + nameof(Subscription) + "- requests) or\r\n" +
                "2) -" + nameof(IP) + "- (typical -" + nameof(PRich) + "-) (enabling it to answer -"+ nameof(ARConcepts.AdHocQuery) + "-) (the last one is not implemented as of May 2020), " +
                "\r\n" +
                "Note that since the data storage mechanism in -" + nameof(StreamProcessor) + "- works just as well inside a -" + nameof(Client) + "- node as " +
                "inside an -" + nameof(ArmDB) + "- node, in many scenarios the -" + nameof(Client) + "- can just subscribe directly to -" + nameof(CoreDB) + "-. " +
                "In a high-load scenario however it might be useful having a single -" + nameof(ArmDB) + "- node serving several -" + nameof(Client) + "- nodes " +
                "in order to relieve som load in -" + nameof(ARNodeType.CoreDB) + "-.",
            LongDescription =
                "The node will either be based on only -" + nameof(ARConcepts.StandardAgoRapideCode) + "- (be -" + nameof(ARComponents.ARADB) + "- configured correspondingly)" +
                "or also contain end-application specific code (being based on -" +  nameof(ARComponents.ARADB) + "-.\r\n" +
                "\r\n" +
                "Note that in principle this node could also cache incoming data to -" + nameof(CoreDB) + "-. This could be useful in high-load scenarios where " +
                "-" + nameof(Client) + "- wants to just 'send-and-forget' data. But since the data storage mechanism in -" + nameof(StreamProcessor) + "- works just as " +
                "well inside a -" + nameof(Client) + "- ndoe as inside an -" + nameof(ArmDB) + "- node, there would usually be nothing in the way of -" + nameof(Client) + "- caching " +
                "itself, without any need for -" + nameof(ArmDB) + "-."
        )]
        ArmDB,

        [EnumMember(
            Description =
                "An outer node / 'end' node with (usually) -" + nameof(ARConcepts.ApplicationSpecificCode) + "- but based on -" + nameof(ARComponents.ARADB) + "-.\r\n" +
                "\r\n" +
                "The node will either:\r\n" +
                "1) Use something like -" + nameof(PRich) + "- for data storage and have a -" + nameof(Subscription) + "- to -" + nameof(ArmDB) + "-\r\n" +
                "(always being up-to-date with cached information in RAM),\r\n" +
                "or\r\n" +
                "2) Not use any specific permanent storage but just -" + nameof(ARConcepts.AdHocQuery) + "- -" + nameof(ArmDB) + "- as needed " +
                "(similar to a traditional client / database setup). ",
            LongDescription =
                "Note that for instance 'end'-nodes which only do logging should be quite possible to set up with only " +
                "-" + nameof(ARConcepts.StandardAgoRapideCode) + "-, but apart from specific cases like that, all other " +
                "'end'-nodes are supposed to contain -" + nameof(ARConcepts.ApplicationSpecificCode) + "-."
        )]
        Client
    }
}