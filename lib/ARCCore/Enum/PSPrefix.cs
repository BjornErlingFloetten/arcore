﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Text;

namespace ARCCore
{

    [Enum(
        AREnumType = AREnumType.OrdinaryEnum,
        Description =
            "Describes some recommended prefixes to use in a -" + nameof(PropertyStreamLine) + "-.\r\n" +
            "\r\n" +
            "The most important one is -" + nameof(PSPrefix.dt) +"- (\"dt/\") for data.\r\n" +
            "\r\n" +
            "Other examples are \"doc/\" for documentation \"cmd/\" for commands, \"log/\" for logging.\r\n" +
            "\r\n" +
            "In -" + nameof(ARComponents.ARCAPI) + "-, the Controllers -RQController- and -AddController- usually work " +
            "against a top level structure identical to these prefixes, but there is no strict one-to-one mapping here.\r\n" +
            "\r\n" +
            "-" + nameof(PSPrefix.dtr) + "- is special and signifies that a whole record under \"dt/\" follows.\r\n" +
            "\r\n" +
            "-" + nameof(ARComponents.ARCEvent) + "- supports -" + nameof(PSPrefix.RS) + "- (-" + nameof(ARConcepts.RegStream) + "-, " +
            "Registration Stream) and -" + nameof(PSPrefix.ES) + "- (-" + nameof(ARConcepts.EventStream) + "-) " +
            "in order to do 2D -" + nameof(ARConcepts.EventSourcing) + "- " +
            "(with corresponding Controllers -RSController- and -ESController-).\r\n" +            
            "\r\n" +
            "These prefixes are not obligatory and not enforced by AgoRapide. Consider them as guidelines only.\r\n" +
            "Naming is mimicking best practices used in for instance MQTT messaging.\r\n" +
            "\r\n" +
            "Note the general principle of containing application data, logging and documentation within the same data structure.\r\n"
    )]
    public enum PSPrefix
    {
        __invalid,

        [EnumMember(Description =
            "Data, single key + value.\r\n" +
            "\r\n" +
            "Example: \"dt/Customer/42/FirstName = John\".\r\n" +
            "\r\n" +
            "This is the actual data that your application stores, that is entities like 'Customer', 'Order', 'Hub', 'Device' " +
            "or rather single properties like 'FirstName', 'Quantity', 'NetworkId' or 'Temperature'."
        )]
        dt,

        [EnumMember(Description =
            "Data, whole records.\r\n" +
            "\r\n" +
            "Example: \"dtr/Customer/42/John/Smith\".\r\n" +
            "\r\n" +
            "This will correspond to multiple lines of -" + nameof(dt) + "-, for instance the example above " +
            "would correspond to:\r\n" +
            "  \"dt/Customer/42/FirstName = John\"\r\n" +
            "  \"dt/Customer/42/LastName = John\"\r\n" +
            "\r\n" +
            "In other words, 'dtr' is a more compact format with higher performance, but since the parsing relies on " +
            "a given order in the corresponding schema it is more vulnerable against changes in the schema.\r\n" +
            "\r\n" +
            "'dtr' is understood by -" + nameof(IP.TryParseDtr) + "-.\r\n"
        )]
        dtr,

        [EnumMember(Description =
            "RS = Registration Stream (see -" + nameof(ARConcepts.RegStream) + "-).\r\n" +
            "\r\n" +
            "Related to -" + nameof(ARComponents.ARCEvent) + "- which implements 2D -" + nameof(ARConcepts.EventSourcing) + "-.\r\n" +
            "\r\n" +
            "A -" + nameof(PropertyStreamLine) + "- with this prefix should be parsed as an -IRegHandler- in " +
            "-" + nameof(ARComponents.ARCEvent) + "-."
        )]
        RS,

        [EnumMember(Description =
            "ES = Event Stream (see -" + nameof(ARConcepts.EventStream) + "-).\r\n" +
            "\r\n" +
            "Related to -" + nameof(ARComponents.ARCEvent) + "- which implements 2D -" + nameof(ARConcepts.EventSourcing) + "-.\r\n" +
            "\r\n" +
            ""
        )]
        ES,

        [EnumMember(Description =
            "Command.\r\n" +
            "\r\n" +
            "See in -" + nameof(ARComponents.ARCEvent) +"- -Cmd-, -ICmdHandler- and -CmdController-.\r\n" +
            "\r\n" +
            "Example: \"cmd/Device/42/TurnOn\".\r\n"
        )]
        cmd,

        [EnumMember(Description =
            "Application state, like app/\r\n" +
            "\r\n" +
            "Application state means the actual internal state of application, and also logging.\r\n" +
            "See -" + nameof(ARConcepts.ExposingApplicationState) + "-.\r\n" +
            "\r\n" +
            "These data points are normally not read from disk at application initialization.\r\n" +
            "They can however normally be accessed from the in-memory data storage as the application progresses.\r\n" +
            "\r\n" +
            "Note that logging is kind of inbuilt in AgoRapide.\r\n" +
            "The -" + nameof(ARConcepts.PropertyStream) + "- / (-" + nameof(ARConcepts.RegStream) +"- " +
            "if you use -"+ nameof(ARComponents.ARCEvent) + "-) 'is' the log and if you need to insert additional log\r\n" +
            "information they are just new properties to be added to the stream.\r\n" +
            "\r\n" +
            "A logging console is likewise just a -" + nameof(ARNodeType.Client) + "- which subscribes to " +
            "the whole or part of the -" + nameof(ARConcepts.PropertyStream) + "- " +
            "(for instance like Subscripton = 'as/*'). " +
            "\r\n" +
            "NOTE: You could have as many log-consoles as you prefer (each with different subscriptions), in order to follow specific parts of the system.\r\n" +
            "\r\n" +
            "In addition to the inbuilt log-characteristics of the -" + nameof(ARConcepts.PropertyStream) + "- " +
            "you can insert additional application state data points through two main methods:\r\n" +
            "\r\n" +
            "Method A) Call to -" + nameof(IP.Log) + "- (this can also be instructed to log to the local console). " +
            "Note overloads to this methods encouraging you to supply a set of data-points (set of (key, values) tuples), instead of just " +
            "some loose text.\r\n" +
            "TODO. GIVE EXAMPLE OF THIS.\r\n" +
            "\r\n" +
            "Method B) Use a class like -" + nameof(PConcurrent) + "- with concept of -" + nameof(IP.Logger) + "-, " +
            "this will log any changes to the class (especially useful for exposing -" + nameof(ARConcepts.ExposingApplicationState) + "-).\r\n" +
            "\r\n" +
            "See also -" + nameof(IP.Log) + "-, -" + nameof(IP.HandleException) + "-, -" + nameof(ARConcepts.ExposingApplicationState) + "-."
        )]
        app,

        [EnumMember(Description =
            "Documentation.\r\n" +
            "\r\n" +
            "Example: \"doc/toc\" for Documentation Table of Contents.\r\n" +
            "Example, full URL: \"http://arnorthwind.agorapide.com/RQ/doc/toc \".\r\n" +
            "\r\n" +
            "Note: Documentation is usually generated on the fly at application startup and not persisted to disk.\r\n" +
            "\r\n" +
            "See also -" + nameof(ARConcepts.LinkInsertionInDocumentation) + "-."
        )]
        doc,

        [EnumMember(Description =
            "Translations.\r\n" +
            "\r\n" +
            "Example: \"t/de_DE/Car/Wagen\"\r\n" +
            "\r\n" +
            "See also -Translations- and -TranslationSingle- in -" + nameof(ARComponents.ARCQuery) + "-.\r\n"
        )]
        t
    }
}