// Copyright (c) 2016-2020 Bj�rn Erling Fl�tten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARCCore {

    [Enum(
        AREnumType = AREnumType.DocumentationOnlyEnum,
        Description =
            "Categories different types of enum used in AgoRapide. The most important one is -" + nameof(PropertyKeyEnum) + "-.\r\n" +
            "Note how this enum -" + nameof(AREnumType) + "- is itself a -" + nameof(AREnumType.DocumentationOnlyEnum) + "-"
    )]
    public enum AREnumType {

        __invalid,

        [EnumMember(
            Description =
                "Designates enums that describe relevant values for an 'entity' object like 'Customer'.\r\n" +
                "Name is entity class appended with P like for instance 'enum CustomerP'.\r\n" +
                "\r\n" +
                "The most important type of enum in AgoRapide used to support -" + nameof(ARConcepts.TaggingOfPropertyKeys) + "-.\r\n" +
                "\r\n" +
                "One such enum member corresponds to one instance of -" + nameof(PK) + "-.",
            LongDescription =
                "Corresponding -" + nameof(BaseAttribute) + "- is -" + nameof(EnumAttribute) + "- / -" + nameof(BasePKAttribute) + "- (the last of which there may be multiple). "
        )]
        PropertyKeyEnum,

        [EnumMember(
            Description = "Designates \"Ordinary\" enums used for indicating range of valid values for a given key",
            LongDescription = "Corresponding -" + nameof(BaseAttribute) + "- is -" + nameof(EnumAttribute) + "- / -" + nameof(EnumMemberAttribute) + "-. "
        )]
        OrdinaryEnum,

        /// <summary>
        /// TODO: Not really much used as of Mar 2020. In "old" AgoRapide AccessLocation was one example of such an enum
        /// (maybe relevant to use when we expand the new AR world).
        /// Maybe used for <see cref="ARNodeType"/> now (9 Mar 2020) (And ARConcepts 11 Mar 2020)
        /// </summary>
        [EnumMember(
            Description =
                "Designates enums that provide a central repository of explanation of terms that are not present in the C# code.\r\n" +
                "\r\n" +
                "In other words, enums of this enum type provides a tag onto which we can hang documentation which would otherwise have " +
                "no place to go, because the actual concept is not 'coded' anywhere.",
            LongDescription =
                "Corresponding -" + nameof(BaseAttribute) + "- is -" + nameof(EnumAttribute) + "- / -" + nameof(EnumMemberAttribute) + "-. " +
                "\r\n" +
                "Note how experience suggests that concepts originally documented as documentation only tend to get implementet in C# code anyway " +
                "(as a kind of natural maturation of the code).\r\n" +
                "In other words, these enums becomes strong hint after some time passes " +
                "that there should actually be some corresponding implementing class of the same name.\r\n" +
                "This again means that some if these enums will disappear from AgoRapide (and your application) as time goes by, " +
                "replaced by concrete implementations."
        )]
        DocumentationOnlyEnum,
    }
}