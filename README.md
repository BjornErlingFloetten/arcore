# AgoRapide 2020

AgoRapide 2020 is a library for building data-oriented backend applications written with .NET Core / Standard. 

With AgoRapide the impedance mismatch problem between database tables and in-memory objects is eliminated through the [property stream](http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/ARConcepts/PropertyStream) concept.

Data is created, stored and transported at the key-value level.

For an introductory top-down presentation of AgoRapide as a concept see [bef.no/agorapide](http://bef.no/AgoRapide).

# Usage

AgoRapide 2020 can be used as: 

1) A distributed in-memory backend processor of events / listener to events. 

2) A 'single-endpoint' API. 

All data is exposed through a standardized API query mechanism (REST or GraphQL) with no need for writing application specific endpoints.

3) A reporting tool / query tool. 

An API as in 2) combined with the [query library](http://arnorthwind.agorapide.com/RQ/doc/toc/ARCQuery).

The query library enables report generation directly from the web-browser (as a single bookmark-able URL).

This is demonstrated with 

[ARNorthwind](https://bitbucket.org/BjornErlingFloetten/ARNorthwind) which can be tried online [here](http://ARNorthwind.AgoRapide.com) 

and 

[ARAdventureWorksOLAP](https://bitbucket.org/BjornErlingFloetten/ARAdventureWorksOLAP) which can be tried online [here](http://ARAdventureWorksOLAP.AgoRapide.com).

# Roadmap / FAQ

General information about issues that will probably be inquired about:

No NuGet package available. This is just a consequence of the simple fact that AgoRapide currently is virtually a one-man project, and the author has no use for a NuGet package. The current practice is to clone ARCore in parallell to your project and link from there. See [ARNorthwind](https://bitbucket.org/BjornErlingFloetten/ARNorthwind) for an example of this.

GraphQL is not implemented although parts of the documentation pretend otherwise. The need has just not arised yet for the author, as ARCore's own [query language](http://arnorthwind.agorapide.com/RQ/doc/toc/ARCQuery) turned out to be quite good and is also HTTP GET URL compatible.

TCP/IP connections do not currently use SSL. This means that currently only 'localhost' connections should be made, or connections over a VPN. See [StreamProcessor](http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Class/StreamProcessor).

Coarse grained security in [ARCAPI](http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/ARComponents/ARCAPI) / [ARAAPI](http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/ARComponents/ARAAPI). You can add a Microsoft.AspNetCore.Authorization.AuthorizeAttribute to the ControllerFacade.CatchAll method with a corresponding AuthenticationHandler but that is all. More fine grained security than that (individual object access rights) is currently not possible.

Note: For security in general, see [security library](http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/ARComponents/ARCSec).

Note: Communication between [nodes](http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/ARNodeType) (see [StreamProcessor](http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Class/StreamProcessor)) is implemented but not throroughly evaluated in real world situations. The other components in general are quite stable in the sense that they are used in production environments and the few (inevitable) bugs that occasionally arise are quickly fixed.

# Input wanted!

Disclaimer: As of March 2021 AgoRapide is virtually a one-man project. There is no business model, just a great urge to scratch an intellectual itch.

The author would really appreciate suggestions for how to explain AgoRapide in order to pique interest. Pull requests are also welcome.

Currently (March 2021) AgoRapide is used in a small Norwegian IoT company for reporting purposes, in addition to some private projects. 

The author is available for consultative work on new projects utilizing AgoRapide in order to test the concept on more varied problem domains.

# Documentation

For complete documentation, see [http://ARNorthwind.AgoRapide.com/RQ/doc/toc](http://ARNorthwind.AgoRapide.com/RQ/doc/toc).

You can create this documentation yourself by either: 

1) Running app/ARAAPI (access documentation from web-browser as localhost)

or by

2) Running app/ARAClient (create documentation as local HTML-files).

Please feel free to contact the author at bef at bef dot no for more information.
