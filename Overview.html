<html>
<head>
<meta charset="UTF-8">
<title>AgoRapide, eliminating impedance mismatch through the property stream</title>
<link rel="stylesheet" type="text/css" href="http://bef.no/bef.css">
<style>
p.code {
  font-family: monospace;
  color: DarkGreen;
  white-space: pre;
}
</style>
</head>
<body>

<h1>Title: AgoRapide, eliminating impedance mismatch through the property stream</h1>
<h2>Subtitle: Creating, storing and transporting data at the key-value level</h2>
<h2>Author: Bjørn Erling Fløtten (Trondheim, Norway November 2020)</h2>
<p>Updated 31 Mar 2021 with documentation links</p>

<h1>Abstract</h1>
<p>
A traditional RDBMS (Relational Database Management System) presents data at an abstraction level well suited for querying (It can serve "any" query). But it is not so flexible with regard to propagation of data and transformation to and from object oriented representations.<br>
We propose creating, storing and transporting data at the key-value level instead (using the term "property stream"), and to redelegate the RDBMS to a secondary level where it can still ensure data consistency and process queries.<br>
Advantages of our method are: Less impedance mismatch between data storage and object model, easier prototyping, inherent scalability, reduced egress costs of data
</p>

<p>
An actual working demonstration implementation of AgoRapide in .NET is found at<br>
<a href="https://bitbucket.org/BjornErlingFloetten/ARCore">https://bitbucket.org/BjornErlingFloetten/ARCore</a><br>
with documentation at<br>
<a href="http://ARNorthwind.AgoRapide.com/RQ/doc/toc">http://ARNorthwind.AgoRapide.com/RQ/doc/toc</a><br>
This document is found at<br>
<a href="http://bef.no/AgoRapide">http://bef.no/AgoRapide</a><br>
</p>

<p>
The author welcomes implementations on other platforms. The demonstration implementation linked to above can be used as a guideline and a foundation for a standardized approach among different implementations.
</p>


<h1>Text</h1>

<h2>1. Property stream</h2>

<p>
"Property stream" in AgoRapide is the concept of how all data is broken down into single 'key and value' pairs which are stored sequentially as plain text format lines and then never change.
</p>
<p>
This format is, by its very nature, easy to distribute and cache.
</p>
<p>
It is also easy to convert into an object oriented format, lessening the 'impedance mismatch' issue between databases and object oriented languages
</p>
<p>
This basic key-value level of storage (the "property stream") is considered flexible enough to not impede on any specific desired implementation on top of core data.
</p>
<p>
See also <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/ARConcepts/PropertyStream">PropertyStream</a>
</p>

<h3>1.1. Property stream, encoding of individual items</h3>
<p>
Each "line" in the property stream is independent of the other lines.
</p>
<p>Example:</p>
<p class="code">dt/Customer/42/FirstName = John
dt/Customer/42/LastName = Smith
dt/Order/43/CustomerId = 42
</p>
<p>
We propose a human readable format for the property stream with a minimum of encoding.
</p>
<p>
See also 
<a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Class/PropertyStreamLine">PropertyStreamLine</a>, 
<a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Class/PropertyStreamLine/EncodeKeyPart">Encode key</a>,
<a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Class/PropertyStreamLine/EncodeValuePart">Encode value</a> and
<a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Class/PropertyStreamLine/Decode">Decode</a>.
</p>

<h3>1.2. Timestamps</h3>
<p>
Timestamps are proposed inserted into the property stream by a suitably responsible node according to the time resolution needed.
</p>
<p>Example:</p>
<p class="code">Timestamp = 2020-10-29 11:53
dt/Customer/42/FirstName = John
dt/Customer/42/LastName = Smith
Timestamp = 2020-10-29 11:54
dt/Order/43/CustomerId = 42
</p>
<p>
A customer was created at time 11:53, an order placed at time 11:54. A resolution of 1 minute is used.
</p>
<p>
See also <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/StreamProcessorP/Timestamp">Timestamp</a>
</p>

<h2>2. Data flow</h2>
<p>
The proposed property stream is inherently flexible regarding distribution of data.
</p>
<p>
This means that data can flow in any manner between different nodes in a given implementation. 
</p>
<p>
One typical implementation could look like this:
</p>

<p>
[FIGURE]
</p>

<h3>2.1. Core node</h3>
<p>
The Core node is responsible for the actual data storage. The actual storage is simple files in text format of a suitable size. 
Storing new data is done by just appending to a storage file. Throughput is therefore quite high.
The read load on the Core node is low because each connecting node has to receive the same data only once (because each node caches the received data).
</p>
<p>
See also <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Class/StreamProcessor">StreamProcessor</a>
</p>
<p>
<h3>2.2. RDBMS node</h3>
The PostgreSQL node subscribe to all or part of the property stream and updates the corresponding database storage.
Clients connect to this database again in an ordinary fashion in order to query the database.
</p>
<h3>2.3. NoSQL node</h3>
<p>
MongoDB node: Our proposal also makes it possible to use alternatives to RDBMS, like "NoSQL", in parallell with a traditional RDBMS.
This is exemplified by the MongoDB node, which has access to the same data, with the same conditions, as the PostgreSQL node.
</p>
<h3>2.4. Processor node</h3>
<p>
The Processor node exemplifies how impedance mismatch is eliminated.<br>
It implements its own in-memory database which provides ordinary object oriented access to the data. That is, the property stream is converted into ordinary objects with properties. 
The in memory database is "always" up to date, that is, the Processor node has no need for making further queries towards the Core node once some data is received.<br>
This node may have a full or a partial subscription to the property stream.
</p>
<p>
See also 
<a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/ARConcepts/PropertyAccess">Property access</a> 
and
<a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Class/IP">IP</a>
</p>
<h3>2.5. API node</h3>
<p>
The API node also implements its own in-memory database. This means that it is always updated and can serve API queries directly from memory. The API clients receive ordinary objects, in for instance JSON format.
</p>
<h4>2.5.1. API node, reading</h4>
<p>
In our working demonstration implementation of AgoRapide the queries can<br>
<br>
1) Peek directly into the in-memory database like
</p>
<p class="code">http://yoursite.com/RQ/Customer/42
</p>
<p>
or<br>
<br>
2) They can be more SQL-like:
</p>
<p class="code">http://yoursite.com/RQ/Customer/WHERE FirstName = John/SELECT FirstName, LastName/ORDER BY LastName, FirstName
</p>
<p>
See also <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCQuery/Class/QueryExpression">Query expressions</a>
</p>
<p>
See also <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCAPI/Class/RQController">REST Query Controller</a>
</p>
<h4>2.5.2. API node, writing</h4>
<p>
The API node can also accept new data. In its simplest form this is just a property stream line given directly in the URL like:
</p>
<p class="code">http://yoursite.com/Add/Customer/42/FirstName = John
</p>
<p>
See also <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCAPI/Class/AddController">Add Controller</a>
</p>
<h4>2.5.3. API node, schema and security</h4>
<p>
Note how the API implementation can be constructed without ANY application specific code.
</p>
<p>
By using the proposed 
<a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/ARConcepts/PropertyAccess">Property access</a> 
(
<a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Class/IP">IP</a>
) mechanism and just mirroring the subscribed property stream, it automatically exposes the Property stream to the outside via HTTP and JSON / HTML.
</p>
<p>
This is ideal for prototyping. As the need for more granular client security arises sensitive data can be cut off from the API altogether by just changing the node's Subscription parameters.
</p>
<p>
An RDBMS node can then be introduced, which subscribes to any sensitive data, with correspondingly established security mechanisms used for client access.
</p>
<h3>2.6. Command node</h3>
<p>
The Command node communicates with external devices (for instance in an IoT (Internet of things) scenario).
</p>
<p>
It injects incoming data to the property stream through the Core node. It can also send commands to the external devices.
</p>
<p>
It only has to subscribe to a miniscule amount of the property stream in order to know how to route messages (in order to know basic mappings between internal and external ids for instance). Like the Processor node it has no need for making queries whenever it does some local processing.
</p>
<h3>2.7. Cache node</h3>
<p>
The Cache node is just a common collector used by further downstream nodes, in order to reduce load on the Core storage node.
</p>
<h3>2.8. Other nodes</h3>
<p>
Some examples of further downstream nodes are:
</p>
<p>
The Backup node subscribes to the whole property stream and just stores it locally (as a backup copy).
</p>
<p>
The Log node and the Management node are hints of how administrative tasks can be performed by listening to the property stream, do log filtering and issuing configuration orders for instance.
</p>
<p>
Note that multiple log nodes can be started ad-hoc whenever needed, that is, plugged into the property stream with the relevant subscription. 
</p>

<h2>3. Routing and caching of data</h2>
<p>
The property stream is inherently very easy to route ("as easy as routing water in a building"). 
</p>
<p>
Since it is also very easy to store locally, and therefore cache between node shutdowns, a typical node never has to ask twice for the same data. This drastically minimizes egress costs, something which is becoming more of a concern with modern cloud architecture.
</p>

<h2>4. Cardinality</h2>
<p>
In order to reduce number of relations we propose a cardinality concept in order to represent some common structures:
</p>
<p>
See also <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/Cardinality">Cardinality</a>
</p>
<h3>4.1 Cardinality, individual items</h3>
<p>
Example, individual items:<br>
Values are supposed to be set and 'deleted' individually.<br>
Typical example could be Customer/PhoneNumber<br>
The PropertyStream / API-calls should look something like this:
</p>
<p class="code">Customer/42/PhoneNumber/90534333                       // Add number
Customer/42/PhoneNumber/40178178                       // Add number
Customer/42/PhoneNumber/90534333.Invalid = 2020-03-20  // 'Delete' individual number
Customer/42/PhoneNumber/Invalid = 2020-03-21           // 'Delete' all numbers
</p>
<p>
See also <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/Cardinality/IndividualItems">Cardinality, individual items</a>
</p>
<h3>4.2 Cardinality, whole collection</h3>
<p>
Example, whole collection:<br>
Values are always set as a whole (there is no need or no meaning in setting or deleting individual items)<br>
Typical example could be PizzaOrder/Extra<br>
The PropertyStream / API-calls should look something like this:
</p>
<p class="code">PizzaOrder/42/Extra = Pepperoni;Cheese;Sauce         // Set whole collection.
PizzaOrder/42/Extra/Invalid = 2020-03-20             // 'Delete' all items.
</p>
<p>
See also <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/Cardinality/WholeCollection">Cardinality, whole collection</a>
</p>
<h2>5. Property stream line prefix</h2>
<p>
The proposed property stream format is very simple and low level. It actually looks a lot like an MQTT message.
</p>
<p>Since the format can be used to carry not only data but also communication in general, we propose the use of prefixes as follows:
</p>
<p>
We propose to use the prefix "dt" for data, like
</p>
<p class="code">dt/Customer/42/FirstName = John
</p>
<p>
and "cmd" (Command) for instructions, like
</p>
<p class="code">cmd/Device/43/TurnOn
</p>
<p>
This is already established industry practice, for instance in the MQTT world.
</p>
<p>
We also propose the prefixes 'app' (Application state / logging) and 'doc' (Documentation), that is, to include exposing of application state, logging and documentation in the basic stream propagation. 
</p>
<p>
This enables mixing of content in the same stream, meaning that communication infrastructure and storage mechanisms can be utilized for more than just storage and dissemination of the core applicatioin data.
</p>
<p>
Our working demonstration implementation of AgoRapide for instance exposes internal application state in the property stream. This simplifies debugging and management of individual nodes in a given system.
</p>
<p>
See also 
<a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/PSPrefix">PSPrefix</a> and
<a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/ARConcepts/ExposingApplicationState">ExposingApplicationState</a>
</p>

<h2>6. Subscription</h2>
<p>
The inherent flexibility of the property strema actual encourages multiple nodes, each processing a small subset of the total data.
</p>
<p>
We therefore propose a system for subscription
</p>
<p>
A subscription consists of one or more lines as follows:<br>
First character is either + (plus, add this data) or - (minus, filter out this data).<br>
Then the actual hierarchical level is specified, with * (asterix) used as wildcard.<br>
</p>
<p>
Example:
</p>
<p class="code">+*                          // Subscribe to everything
+dt/Customer/42/*           // Subscribe to all properties related to customer with id 42
+dt/Customer/*/FirstName/*  // Subscribe to all occurrences of Customer.FirstName
+log/*                      // Subscribe to everything beginning with log
-log/API:                   // Do not include properties beginning with 'log/API/ in subscription
</p>
<p>
See also <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Class/Subscription">Subscription</a> and <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Class/Subscription/SyntaxHelp">Subscription syntax</a>
</p>
<p>
Whenever the client connects to an upstream node, it sends its subscription request:
</p>
<p>
Example:
</p>
<p class="code">SubscriptionAsRequestedByClient/+*
SubscriptionAsRequestedByClient/-log/*
</p>
<p>
(Subscribe to everything except log data.)
</p>
<h3>6.1. Client update position</h3>
<p>
In order for the server to know from where (actually from when) in the property stream to start sending data, the client has to specify a "client update position". This is ordinarily just a key given by the server, and sent with every new property stream line of the connection.
</p>
<p>
We propose to separate the client update position and the actual property stream line with a :: (double colon)
</p>
<p>
Example:
</p>
<p class="code">File00042,414546::dt/Customer/42/FirstName = John
</p>
<p>
See also <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Class/ClientUpdatePosition">ClientUpdatePosition</a>
<p>
Whenever the client connects to an upstream node it sends its "client update position".
</p>
<p>
Example:
</p>
<p class="code">ThisConnection/ClientUpdatePosition = File00042,414546
</p>
<p>
We propose some special values that the client can send to the server
</p>
<p>
ClientDatastoreIsEmpty: Used by the subscribing client to signify that it has no data stored locally, and it therefore needs an update from the beginning of the PropertyStream, that is, 'from the beginning of time'. This will usually be the case when a new node is introduced, and it needs to build up its own local cache.
</p>
<p>
See also <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Class/ClientUpdatePosition/ClientDatastoreIsEmpty">ClientDatastoreIsEmpty</a>
</p>
<p>
OnlyNewDataIsRequested: Used by the subscribing client to signify that it does not care about historical data, the server may start with sending only new data. A typical example is starting up a log-console in order to monitor some action
</p>
<p>
See also <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Class/ClientUpdatePosition/OnlyNewDataIsRequested">OnlyNewDataIsRequested</a>
</p>
<p>
GoBack: Useful when subscribing client wants (in general) new log-data, but also data for the last few minutes, for instance when you see a special situation and want to look into the latest log data.
</p>
See also <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Class/ClientUpdatePosition/IsGoBack">GoBack</a>
</p>

<h2>7. Tagging of property keys (schema) and property access</h2>
<p>
The property stream itself and the corresponding storage mechanism is not aware of, nor dependent of, any schema. 
</p>
<p>
For in-memory consumption of data however, and exposing of data through APIs, some schema and standardized object structure is desired.
</p>
<p>
We propose the term "tagging of property keys" in order to specify a schema for the data in the property stream. We propose that all schema data related to a given key shall reside "together" with the definition of the key. This ensures at-a-glance assession of the key's usage throughout the application.
</p>
<p>
See also <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/ARConcepts/TaggingOfPropertyKeys">TaggingOfPropertyKeys</a>
</p>
<p>
We also propose a standardized property access mechanism. 
</p>
<p>
This enables for instance automatically generated API's (without specific endpoints).
</p>
<p>
See also 
<a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/ARConcepts/PropertyAccess">Property access</a> 
and
<a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Class/IP">IP</a>
</p>

<h2>8. Meta properties</h2>
<p>
In order to ensure tracking of changes we propose the use of meta properties as follows:<br>
Created: Timestamp when created in database.<br>
Cid: Creator id: Creator id (entity which created this property).<br>
Valid: Timestamp when last known valid.<br>
Vid: Validator id, that is entity which set Valid for this property.<br>
Invalid: Timestamp when invalidated / 'deleted'.<br>
IId: Invalidator id, that is entity which set Invalid for this property<br>
</p>
<p>
Note that "Invalid" is proposed to be equivalent to SQL DELETE or SQL UPDATE (set value to NULL).
Standard transformations for the other meta properties are not proposed in this document.
</p>
<p>
See also <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/PP">Meta properties</a> for more details.
</p>

<h2>9. Conversion from property stream format to SQL</h2>
<p>
A traditional RDBMS can be positioned as a "subscriber" to the property stream.
</p>
<p>
Each property stream line will be converted to SQL as follows:
</p>
<h3>9.1. Conversion, ordinary properties</h3>
<p>
Ordinary change of property:
</p>
<p class="code">Customer/42/FirstName = John
UPDATE Customer SET FirstName = John WHERE CustomerId = 42
</p>
<p>
(alternatively, "INSERT INTO Customer (CustomerId, Name) VALUES (42, 'John')" if this is a new customer)<br>
</p>
<h3>9.2. Conversion, meta properties</h3>
<p>
Meta-property [Invalid] result in DELETE or UPDATE of the corresponding key.
</p>
<p>
Example:
</p>
<p class="code">Customer/42/Invalid = 2020-10-28 15:11:00
DELETE FROM Customer WHERE CustomerId = 42

Customer/42/MiddleName/Invalid = 2020-10-28 15:11:00
UPDATE Customer SET MiddleName = NULL WHERE CustomerId = 42
</p>
<h2>10. Transactions</h2>
<p>
Transactions can be supported by inserting SQL-style BEGIN, COMMIT / ABORT into the property stream, together with a corresponding transaction id, to signal start and end of transactions. Every property stream line (each data point) belonging to that transaction would then be tag'ed with the transaction id.
</p>
<p>
A node (client) seeing a BEGIN would then know to wait for a COMMIT / ABORT with the same transaction id, before considering the belonging data points for further processing.
</p>
<p>
Example, moving money from one account to another:
</p>
<p class="code">Transaction/123abc456def/BEGIN
Transaction/123abc456def/Account/42/Subtract = 1000 EUR
Transaction/123abc456def/Account/43/Add = 1000 EUR
Transaction/123abc456def/COMMIT
</p>
<p>
(note that in the actual property stream, other data may be interspersed with the lines shown above.)
</p>

<h2>11. Security</h2>
<p>
This document does not describe security in any detail. 
</p>
<p>
If secure access to the basic property stream is necessary we envisage that off-the-shelf solutions can be used, like SSL with certificates for instance.
</p>
<p>
For secure client queries (when more than just a single set of common administrative credentials are needed) we have proposed to use established stacks, starting from an established RDBMS node (which subscribes to the Property stream).
</p>

<h2>12. Scalability and fault-tolerance (redundancy)</h2>
<p>
Note: Apache Kafka already offers a property stream based fault tolerant and scaleable storage mechanism.<br>
For advanced needs, as of the time of writing (2020) the author recommends to use Kafka as the storage mechanism, and connect AgoRapide nodes to Kafka (the subscription paradigm will be the same).<br>
Key-value databases like DynamoDB and Cosmos DB are also well suited as the storage mechanism for advanced needs.<br>
</p>
<p>
We envisage the AgoRapide concept as inherently scalable. Data can for instance be sharded easily, by using corresponding Subscription parameters. 
</p>
<p>
Creation of new processing nodes as the load increases, and corresponding termination as the load decreases, can be done through a dedicated management Node. The configuration commands can themselves be part of the property stream.
</p>
<p>
See also <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Class/Sharding">Sharding</a>
</p>
<p>
Fault-tolerance (redundancy) is likewise relatively easy to build on top of AgoRapide. The property stream can be duplicated, one can use multiple Core data storages, synchronized to each other, have multiple processing nodes and so on.
</p>
<p>
See also <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/MultipleConnectionUsage">MultipleConnectionUsage</a>

<h2>13. Containerization</h2>
<p>
Each node is very simple in its construction. Our demonstration implementation for instance uses no external functionality (libraries) except what is found in .NET Standard.
</p>
<p>This should make the AgoRapide concept ideal for containerization.
</p>

<h2>14. Other ideas</h2>
<p>
The documentation for our working implementation of AgoRapide contains some more ideas. 
</p>
<p>
See especially <a href="http://arnorthwind.agorapide.com/RQ/doc/toc/ARCCore/Enum/ARConcepts">AgoRapide concepts</a> 
or, start reading from 
<a href="http://arnorthwind.agorapide.com/RQ/doc/toc">documentation root level</a>
</p>
<p>
Note that the documentation referenced throughout in this document is itself created with the help of AgoRapide. 
In this case as static .HTML-files, but it could just as well have been an online API: 
If you download the AgoRapide working implementation, from 
<a href="https://bitbucket.org/BjornErlingFloetten/arcore">https://bitbucket.org/BjornErlingFloetten/arcore</a>
and start the application ARAAPI, you will see this same documentation exposed through an online API peeking directly into the documentation database.
</p>

</body>
</html>