﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using ARCCore;
using ARCDoc;
using System.Collections.Generic;
using System.Linq;
namespace ARAClient {

    [Class(Description = "See -" + nameof(ARComponents.ARAClient) + "- for description")]
    public class Program {

        public static void Main(string[] args) {
            Console.SetWindowSize(150, 30);
            System.Threading.Thread.CurrentThread.Name = "MainThread";
            W(ARNodeType.Client.ToString());

            /// Include all assemblies in which your controllers and <see cref="AgoRapide.BaseEntity"/>-derived classes resides.
            UtilCore.Assemblies = new List<System.Reflection.Assembly> {
                typeof(ARCCore.ClassAttribute).Assembly,  
                typeof(ARCDoc.Documentator).Assembly,    
                typeof(ARCQuery.QueryExpression).Assembly, // Note that only referred to in order to complete documentation. You may run ARAClient without referring to this component. 
                typeof(ARCAPI.BaseController).Assembly, // Note that only referred to in order to complete documentation. You may run ARAClient without referring to this component. 
                typeof(ARAClient.Program).Assembly         
                };

            // Parse any instructions as command line arguments:
            if (args.Length > 0) {
                W("Command line arguments given, will attempt to parse as " + nameof(ConnectionInstruction) + ".");
                // Example of arguments can be:
                // "ServerHostName=localhost ServerPortNo=4246 DataTransferDirection=Receive"
                Demonstrator.DemonstrateLogConsole(PropertyStreamLine.ParseDirectToIP<ConnectionInstruction>(
                    args.Select(a => a.Replace(" = ", "=").Replace("=", " = ")) // Allow both with and without spaces. Without spaces means easier construction from command line (no need to enclose parameters in double quotes)
               ));
                return;
            }

            // Hint, uncomment this line if you want a thorough demonstration of use of AgoRapide
            Demonstrator.DemonstrateAll();

            //// You can also call individual demonstrations, like             
            // Demonstrator.DemonstratePropertyStream();

            /// Or run this application as a <see cref="ARNodeType.Client"/> against an existing <see cref="ARNodeType.CoreDB"/>
            // Demonstrator.DemonstrateStreamProcessorAsClient();

            W("\r\n\r\nPress ENTER in order to EXIT application");
            Console.ReadLine();
        }

        public static void W(string s = "") => Console.WriteLine("DIRECT TO CONSOLE: " + s.Replace("\r\n", "\r\nDIRECT TO CONSOLE: ")); // Mark as direct written to console in order to distinguish from data originating more deeply from within AgoRapide system (IP.Log, property-stream and similar)

        // TODO: Move into ARCDoc
        private readonly System.Threading.ReaderWriterLockSlim _lock = new System.Threading.ReaderWriterLockSlim();
        private readonly PRich _storage = new PRich();
        public void Receiver1(string s) {
            try {
                _lock.EnterWriteLock();
                PropertyStreamLine.ParseAndStore(_storage, s);
            } finally {
                _lock.ExitWriteLock();
            }
        }

        // TODO: Move into ARCDoc
        public APIResponse ServeAPIMethod(APIRequest request) {
            try {
                _lock.EnterReadLock();
                // return _storage.Query(request);
                return new APIResponse();
            } finally {
                _lock.ExitReadLock();
            }
        }
        public class APIRequest { }
        public class APIResponse { }
    }
}
