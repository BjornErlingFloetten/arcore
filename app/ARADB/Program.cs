﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway

using System;
using ARCCore;
using System.Collections.Generic;
using System.Linq;

namespace ARADB {

    [Class(Description = "See -" + nameof(ARComponents.ARADB) + "- for description")]
    public class Program {

        public static void Main(string[] _) {
            Console.SetWindowSize(150,30);
            System.Threading.Thread.CurrentThread.Name = "MainThread";
            W(nameof(ARNodeType.CoreDB));
            /// Include all assemblies in which your controllers and <see cref="AgoRapide.BaseEntity"/>-derived classes resides.
            UtilCore.Assemblies = new List<System.Reflection.Assembly> {
                typeof(ClassAttribute).Assembly,  // ARCCore
            };

            W(Environment.CurrentDirectory);
            using var sp = StreamProcessor.CreateBareBonesInstance(
                IKString.FromString(nameof(ARNodeType.CoreDB)), 
                writeToConsole: true, 
                cacheDiskWrites: true
            );
            sp.Initialize();
            // Important, do not add other properties until initialize has been called.
            sp.IP.SetPV(StreamProcessorP.IncomingConnectionsPortNo, 4246);
            sp.IP.SetPV(StreamProcessorP.TimestampResolution, TimeSpan.FromMinutes(1));

            sp.StartTCPIPCommunication();
            W("\r\n\r\nPress ENTER in order to EXIT application");
            Console.ReadLine();
        }

        public static void W(string s = "") => Console.WriteLine("DIRECT TO CONSOLE: " + s.Replace("\r\n", "\r\nDIRECT TO CONSOLE: ")); // Mark as direct written to console in order to distinguish from data originating more deeply from within AgoRapide system (IP.Log, property-stream and similar)
    }
}